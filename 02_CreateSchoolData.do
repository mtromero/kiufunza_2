*******************************************************
*************BASE LINE YR1
********************************************************
use "$basein/1 Baseline/School/GradesHead_noPII.dta", clear
gen StudentsGr= s184+s185
keep StudentsGr SchoolID GradeID
collapse (sum) Students=StudentsGr, by(SchoolID)
save "$base_out/ConsolidatedYr34/SchoolBaseline.dta", replace


use "$basein/1 Baseline/School/School_noPII.dta", clear
keep DistrictID SchoolID s78 s68 committeeTeachers committeeParents committeeOthers commPolitician commReligious commLocalBus commCivilServant commNGO commOther s36 s42 s108 s109 s110 s111 s118 s120 s124 s126 s131 s133 s135 computersYN s172 s175 s188 s191 s198 s199 s200
foreach var in  s42 s108  s118 s120    computersYN s172 s175 s191 s198 s200 s188{
recode `var' (2=0)
}

gen TotalNumberTachers=s68
gen FractionAbsentHT=s78/s68
gen Rural=(s108==2)
gen ClassesOutside=(s118==1)
gen Electricity=(s120==1)
gen Latrines=s126

gen PipedWater=0
replace PipedWater=1 if s124==5
gen NoWater=0
replace NoWater=1 if s124==1
gen SingleShift=0
replace SingleShift=1 if s198==1

merge 1:1 SchoolID using  "$base_out/ConsolidatedYr34/SchoolBaseline.dta"
drop _merge

gen PTR=Students/s68
save "$base_out/ConsolidatedYr34/SchoolBaseline.dta", replace


use "$basein/1 Baseline/School/Facilities_noPII.dta", clear

keep s142 s143 SchoolID FacilityID
reshape wide s142 s143, i(SchoolID) j(FacilityID)


label variable s1421 "Km District education office"
label variable s1422 "Km Closest bank"
label variable s1423 "Km Closest post office"
label variable s1424 "Km Closest high school"
label variable s1425 "Km Closest govt primary school"

label variable s1431 "Min. District education office"
label variable s1432 "Min. Closest bank"
label variable s1433 "Min. Closest post office"
label variable s1434 "Min. Closest high school"
label variable s1435 "Min. Closest govt primary school"
merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/SchoolBaseline.dta"
drop _merge
save "$base_out/ConsolidatedYr34/SchoolBaseline.dta", replace

use "$basein/1 Baseline/School/SchoolFacilities_noPII.dta", clear
keep s145 SchoolID SchoolFacilityID
reshape wide s145, i(SchoolID) j(SchoolFacilityID)


label variable s1451 "School has Kitchen"
label variable s1452 "School has Library"
label variable s1453 "School has Playground"
label variable s1454 "School has Staff room"
label variable s1455 "School has Outer wall or fence"
label variable s1456 "School has Receive a newspaper"

foreach var in  s1451-s1456{
recode `var' (2=0)
}


merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/SchoolBaseline.dta"
drop _merge

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge

pca s1451 s1452 s1453 s1454 s1455 s1456
predict InfrastructureIndex,score
pca s1421- s1425
predict DistanceIndex,score
save "$base_out/ConsolidatedYr34/SchoolBaseline.dta", replace


use "$basein//9 Baseline 2015/Final Data/School/R7Grade_noPII.dta", clear
gen enrollment=s184+s185
drop s184 s185
reshape wide enrollment, i( SchoolID) j( R7GradeID)
egen enrollment2015=rowtotal(enrollment*)

merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/SchoolBaseline.dta"
drop if _merge==1
drop _merge



rename * =_T1
foreach var in SchoolID DistrictID StrataScore treatment2 treatarm2 treatment treatarm{
rename `var'_T1 `var'
}
save "$base_out/ConsolidatedYr34/School.dta", replace



*******************************************************
*************END LINE YR1
********************************************************
/*
use "$basein/11 Endline 2015/Final Data/School Data/R8School_nopii.dta", clear
keep SchoolID httmus_a- httmus_h httmas wchprf ztoppal
*/
use "$basein\11 Endline 2015\Final Data\Teacher Data\R8School_nopii.dta", clear
keep SchoolID DistrictID s68
save "$base_out/ConsolidatedYr34/School_T2.dta", replace 

use "$basein/11 Endline 2015/Final Data/Teacher Data/R8Teacher_nopii.dta", clear
recode attbok (2=0)
collapse (mean) attbok, by(SchoolID)
merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/School_T2.dta"
drop _merge
save "$base_out/ConsolidatedYr34/School_T2.dta", replace 

use "$basein/11 Endline 2015/Final Data/School Data/R8Grade_nopii.dta", clear

keep SchoolID R8GradeID s184 s185 gdrctb
reshape wide s184 s185 gdrctb, i(SchoolID) j(R8GradeID)

label variable s1841 "Boys Grade 1"
label variable s1842 "Boys Grade 2"
label variable s1843 "Boys Grade 3"
label variable s1844 "Boys Grade 4"
label variable s1845 "Boys Grade 5"
label variable s1846 "Boys Grade 6"
label variable s1847 "Boys Grade 7"

label variable s1851 "Girls Grade 1"
label variable s1852 "Girls Grade 2"
label variable s1853 "Girls Grade 3"
label variable s1854 "Girls Grade 4"
label variable s1855 "Girls Grade 5"
label variable s1856 "Girls Grade 6"
label variable s1857 "Girls Grade 7"

gen StudentsGr1=s1841+s1851
gen StudentsGr2=s1842+s1852
gen StudentsGr3=s1843+s1853
gen StudentsGr4=s1844+s1854
gen StudentsGr5=s1845+s1855
gen StudentsGr6=s1846+s1856
gen StudentsGr7=s1847+s1857


label variable StudentsGr1 "Students Grade 1"
label variable StudentsGr2 "Students Grade 2"
label variable StudentsGr3 "Students Grade 3"
label variable StudentsGr4 "Students Grade 4"
label variable StudentsGr5 "Students Grade 5"
label variable StudentsGr6 "Students Grade 6"
label variable StudentsGr7 "Students Grade 7"

label variable gdrctb1 "Expenditure in textbooks grade 1"
label variable gdrctb2 "Expenditure in textbooks grade 2"
label variable gdrctb3 "Expenditure in textbooks grade 3"
label variable gdrctb4 "Expenditure in textbooks grade 4"
label variable gdrctb5 "Expenditure in textbooks grade 5"
label variable gdrctb6 "Expenditure in textbooks grade 6"
label variable gdrctb7 "Expenditure in textbooks grade 7"


egen TotalNumberStudents=rowtotal(StudentsGr1 StudentsGr2 StudentsGr3 StudentsGr4 StudentsGr5 StudentsGr6 StudentsGr7)

forvalues i=1/7{
	gen gdrctb`i'_PS=gdrctb`i'/StudentsGr`i'
}

compress
merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/School_T2.dta"
drop _merge
save "$base_out/ConsolidatedYr34/School_T2.dta", replace 

gen PTR=TotalNumberStudents/s68 

use "$basein/11 Endline 2015/Final Data/School Data/R8Expenses_nopii.dta", clear

gen ExpenseGroup=R8ExpenseID
replace  ExpenseGroup=1 if R8ExpenseID==1 | R8ExpenseID==2 | R8ExpenseID==3 | R8ExpenseID==4 | R8ExpenseID==5
replace  ExpenseGroup=2 if R8ExpenseID==7 | R8ExpenseID==8 | R8ExpenseID==16
replace  ExpenseGroup=3 if R8ExpenseID==9 | R8ExpenseID==10 | R8ExpenseID==11 | R8ExpenseID==12
replace  ExpenseGroup=4 if R8ExpenseID==13 | R8ExpenseID==14 | R8ExpenseID==15
replace  ExpenseGroup=5 if R8ExpenseID==6
replace  ExpenseGroup=6 if R8ExpenseID==17

label define expensegroup 1 "Administrative" 2 "Student Aid" 3 "Teaching Aid" 4 "Teachers" 5 "Construction" 6 "Emergency Fund"
label values ExpenseGroup  expensegroup
label variable 	ExpenseGroup "Expense Category"

collapse (sum) s168 , by(ExpenseGroup SchoolID)

reshape wide s168, i(SchoolID) j(ExpenseGroup)



label variable 	s1681 "Administrative Expenses"
label variable 	s1682 "Student Aid Expenses"
label variable 	s1683 "Teaching Aid Expenses"
label variable 	s1684 "Teachers Expenses"
label variable 	s1685 "Construction Expenses"
label variable 	s1686 "Emergency Fund"


merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/School_T2.dta"
drop _merge

forvalues i=1/6{
	gen s168`i'_PS=s168`i'/TotalNumberStudents
}

compress
save "$base_out/ConsolidatedYr34/School_T2.dta", replace


use "$basein/11 Endline 2015/Final Data/School Data/R8Funding_nopii.dta", clear


recode inkdyn (2=0)
keep s155 SchoolID  R8FundingID inkdyn
reshape wide s155 inkdyn, i(SchoolID) j( R8FundingID)

label var s1551 "Government CG"
label var s1552 "Government Other"
label var s1553 "Local Government"
label var s1555 "NGOs"
label var s1556 "Parents"
label var s1557 "Other"

label var inkdyn1 "Government CG"
label var inkdyn2 "Government Other"
label var inkdyn3 "Local Government"
label var inkdyn5 "NGOs"
label var inkdyn6 "Parents"
label var inkdyn7 "Other"



merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/School_T2.dta"
drop _merge

forvalues i=1/3{
	gen s155`i'_PS=s155`i'/TotalNumberStudents
}
forvalues i=5/7{
	gen s155`i'_PS=s155`i'/TotalNumberStudents
}

compress
save "$base_out/ConsolidatedYr34/School_T2.dta", replace

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
save "$base_out/ConsolidatedYr34/School_T2.dta", replace


rename * =_T2
foreach var in SchoolID DistrictID StrataScore treatment2 treatarm2 treatment treatarm{
rename `var'_T2 `var'
}

merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/School.dta"
drop _merge

compress
save "$base_out/ConsolidatedYr34/School.dta", replace


*******************************************************
*************END LINE YR2
********************************************************

use "$basein\15 Endline 2016\Final Deliverable\Final Data\Teacher Data\R10School_nopii.dta", clear
keep SchoolID DistrictID s68
save "$base_out/ConsolidatedYr34/School_T5.dta", replace 

use "$basein/15 Endline 2016\Final Deliverable\Final Data\Teacher Data\R10Teacher_nopii.dta", clear
recode attbok (2=0)
collapse (mean) attbok, by(SchoolID)
merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/School_T5.dta"
drop _merge
save "$base_out/ConsolidatedYr34/School_T5.dta", replace 

use "$basein/15 Endline 2016\Final Deliverable\Final Data\School Data\R10Grade_nopii.dta", clear

keep SchoolID R10GradeID s184 s185 gdrctb
reshape wide s184 s185 gdrctb, i(SchoolID) j(R10GradeID)

label variable s1841 "Boys Grade 1"
label variable s1842 "Boys Grade 2"
label variable s1843 "Boys Grade 3"
label variable s1844 "Boys Grade 4"
label variable s1845 "Boys Grade 5"
label variable s1846 "Boys Grade 6"
label variable s1847 "Boys Grade 7"

label variable s1851 "Girls Grade 1"
label variable s1852 "Girls Grade 2"
label variable s1853 "Girls Grade 3"
label variable s1854 "Girls Grade 4"
label variable s1855 "Girls Grade 5"
label variable s1856 "Girls Grade 6"
label variable s1857 "Girls Grade 7"

gen StudentsGr1=s1841+s1851
gen StudentsGr2=s1842+s1852
gen StudentsGr3=s1843+s1853
gen StudentsGr4=s1844+s1854
gen StudentsGr5=s1845+s1855
gen StudentsGr6=s1846+s1856
gen StudentsGr7=s1847+s1857


label variable StudentsGr1 "Students Grade 1"
label variable StudentsGr2 "Students Grade 2"
label variable StudentsGr3 "Students Grade 3"
label variable StudentsGr4 "Students Grade 4"
label variable StudentsGr5 "Students Grade 5"
label variable StudentsGr6 "Students Grade 6"
label variable StudentsGr7 "Students Grade 7"

label variable gdrctb1 "Expenditure in textbooks grade 1"
label variable gdrctb2 "Expenditure in textbooks grade 2"
label variable gdrctb3 "Expenditure in textbooks grade 3"
label variable gdrctb4 "Expenditure in textbooks grade 4"
label variable gdrctb5 "Expenditure in textbooks grade 5"
label variable gdrctb6 "Expenditure in textbooks grade 6"
label variable gdrctb7 "Expenditure in textbooks grade 7"


egen TotalNumberStudents=rowtotal(StudentsGr1 StudentsGr2 StudentsGr3 StudentsGr4 StudentsGr5 StudentsGr6 StudentsGr7)

forvalues i=1/7{
	gen gdrctb`i'_PS=gdrctb`i'/StudentsGr`i'
}

compress
merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/School_T5.dta"
drop _merge
save "$base_out/ConsolidatedYr34/School_T5.dta", replace 

gen PTR=TotalNumberStudents/s68 

use "$basein/15 Endline 2016\Final Deliverable\Final Data\School Data/R10Expenses_nopii.dta", clear

gen ExpenseGroup=R10ExpenseID
replace  ExpenseGroup=1 if R10ExpenseID==1 | R10ExpenseID==2 | R10ExpenseID==3 | R10ExpenseID==4 | R10ExpenseID==5
replace  ExpenseGroup=2 if R10ExpenseID==7 | R10ExpenseID==8 | R10ExpenseID==16
replace  ExpenseGroup=3 if R10ExpenseID==9 | R10ExpenseID==10 | R10ExpenseID==11 | R10ExpenseID==12
replace  ExpenseGroup=4 if R10ExpenseID==13 | R10ExpenseID==14 | R10ExpenseID==15
replace  ExpenseGroup=5 if R10ExpenseID==6
replace  ExpenseGroup=6 if R10ExpenseID==17

label define expensegroup 1 "Administrative" 2 "Student Aid" 3 "Teaching Aid" 4 "Teachers" 5 "Construction" 6 "Emergency Fund"
label values ExpenseGroup  expensegroup
label variable 	ExpenseGroup "Expense Category"

collapse (sum) s168 , by(ExpenseGroup SchoolID)

reshape wide s168, i(SchoolID) j(ExpenseGroup)



label variable 	s1681 "Administrative Expenses"
label variable 	s1682 "Student Aid Expenses"
label variable 	s1683 "Teaching Aid Expenses"
label variable 	s1684 "Teachers Expenses"
label variable 	s1685 "Construction Expenses"
label variable 	s1686 "Emergency Fund"


merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/School_T5.dta"
drop _merge

forvalues i=1/6{
	gen s168`i'_PS=s168`i'/TotalNumberStudents
}

compress
save "$base_out/ConsolidatedYr34/School_T5.dta", replace


use "$basein/15 Endline 2016\Final Deliverable\Final Data\School Data/R10Funding_nopii.dta", clear

rename s155_FUNDING s155
recode inkdyn (2=0)
keep s155 SchoolID  R10FundingID inkdyn
reshape wide s155 inkdyn, i(SchoolID) j( R10FundingID)

label var s1551 "Government CG"
label var s1552 "Government Other"
label var s1553 "Local Government"
label var s1555 "NGOs"
label var s1556 "Parents"
label var s1557 "Other"

label var inkdyn1 "Government CG"
label var inkdyn2 "Government Other"
label var inkdyn3 "Local Government"
label var inkdyn5 "NGOs"
label var inkdyn6 "Parents"
label var inkdyn7 "Other"



merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/School_T5.dta"
drop _merge

forvalues i=1/3{
	gen s155`i'_PS=s155`i'/TotalNumberStudents
}
forvalues i=5/7{
	gen s155`i'_PS=s155`i'/TotalNumberStudents
}

compress
save "$base_out/ConsolidatedYr34/School_T5.dta", replace

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
save "$base_out/ConsolidatedYr34/School_T5.dta", replace



rename * =_T5
foreach var in SchoolID DistrictID StrataScore treatment2 treatarm2 treatment treatarm{
rename `var'_T5 `var'
}

merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/School.dta"
drop _merge

compress
save "$base_out/ConsolidatedYr34/School.dta", replace
