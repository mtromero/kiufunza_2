******** NORMAL *********8
  
use "$base_out/ConsolidatedYr34/Student.dta", clear
merge m:1 SchoolID using "$base_out/ConsolidatedYr34/School.dta"
drop _merge
gen upidst=upid
merge m:1 upidst using "$base_out/ConsolidatedYr34/Household_Baseline2016.dta"
drop _merge
merge m:1 upidst using "$base_out/ConsolidatedYr34/Household_Baseline2015.dta", update
drop _merge
merge m:1 upidst using "$base_out/ConsolidatedYr34/Household_Endline2014.dta", update
drop _merge
merge m:1 upidst using "$base_out/ConsolidatedYr34/Household_Baseline2014.dta", update
drop _merge
 merge m:1 upidst using "$base_out/ConsolidatedYr34/Household_Endline2013.dta", update
drop _merge
  merge m:1 upidst using "$base_out/ConsolidatedYr34/Household_Baseline2013.dta", update
drop _merge


gen StudentsGr_T1=enrollment1_T1 if GradeID_T2==1
replace StudentsGr_T1=enrollment2_T1 if GradeID_T2==2
replace StudentsGr_T1=enrollment3_T1 if GradeID_T2==3
replace StudentsGr_T1=enrollment4_T1 if GradeID_T2==4


gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "Gains"

*IndexPoverty IndexEngagement IndexKowledge
foreach var in  Z_hisabati Z_kiswahili Z_kiingereza Z_ScoreKisawMath  Z_ScoreFocal seenUwezoTests preSchoolYN Gender {
	gen Lag`var'=.
	replace Lag`var'=`var'_T1 if !missing(`var'_T1)
	replace Lag`var'=`var'_T4 if !missing(`var'_T4)
}



*gen LagExpenditure=.
*replace LagExpenditure=expn15_T1 if !missing(expn15_T1)
*replace LagExpenditure=Expenditure_FC_2013_T5 if !missing(Expenditure_FC_2013_T5)


  


gen LagAge=.
replace LagAge=Age_T1+1 if !missing(Age_T1)
replace LagAge=Age_T4 if !missing(Age_T4)
gen LagGrade=GradeID_T5
replace LagGrade=5 if GradeID_T2==4

foreach var in  Z_hisabati Z_kiswahili Z_kiingereza  Z_ScoreFocal seenUwezoTests preSchoolYN Gender Grade Age  {
	gen Missing`var'=!missing(Lag`var')
	replace Lag`var'=0 if Lag`var'==.
}

foreach var in  IndexPoverty IndexEngagement IndexKowledge {
	gen Missing`var'=!missing(`var')
	replace `var'=0 if `var'==.
}


*replace LagGrade=GradeID_T8 if LagGrade==.
 
*gen DiffDias=date_T8-date_T7
*gen DiffDias2=dateTWA-dateEDI
*replace DiffDias=. if DiffDias<-365 | DiffDias>365
*replace DiffDias2=. if DiffDias2<-365 | DiffDias2>365
	 
*merge m:1 SchoolID LagGrade using "$base_out/Consolidated/SchoolsGradeAverageScores.dta", keepus(MeanGrade_LagZ_kiswahili MeanGrade_LagZ_kiingereza MeanGrade_LagZ_hisabati)
fvset base default treatarm 
save "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", replace
	

/*
******** NORMAL+TWAWEZA *********8
  
use "$base_out/Consolidated/Student_TWA.dta", clear
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 upid using "$basein/3 Endline/Supplementing/R_EL_rHHData_noPII.dta", keepus(HHID)
drop _merge
merge m:1 SchoolID using "$base_out/3 Endline/School/TestTiming.dta", keepus(DiffIntvTestRestTest)
drop _merge
merge m:1 HHID using "$base_out/Consolidated/Household.dta"
drop _merge
merge m:1 SchoolID using "$base_out/3 Endline/School/TeacherAverage.dta"
drop _merge 
merge m:1 SchoolID using "$base_out/Consolidated/SchoolDatesTest.dta"
drop _merge 
foreach var in male t05 t07 t08 t10 t21 higher_degree t23 t24 t25{
rename `var' `var'Average
}
merge m:1 SchoolID GradeID_T7 stdgrp_T7 using "$base_out/Consolidated/TeachingSchedulle_T7.dta"
drop if _merge==2
drop _merge 


gen StudentsGr_T1=StudentsGr1_T1 if GradeID_T3==1
replace StudentsGr_T1=StudentsGr2_T1 if GradeID_T3==2
replace StudentsGr_T1=StudentsGr3_T1 if GradeID_T3==3


gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "CG"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD" | treatment_T8==2
label var TreatmentCOD "COD"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both" | treatment_T8==1
label var TreatmentBoth "Combo"

foreach var in  Z_hisabati Z_kiswahili Z_kiingereza seenUwezoTests preSchoolYN male{
gen Lag`var'=.
replace Lag`var'=`var'_T1 if !missing(`var'_T1)
replace Lag`var'=`var'_T5 if !missing(`var'_T5)
}

foreach var in  Z_hisabati Z_kiswahili Z_kiingereza{
gen SD_Lag`var'_pass=.
replace SD_Lag`var'_pass=SD_`var'_pass_T1 if !missing(`var'_T1)
replace SD_Lag`var'_pass=SD_`var'_pass_T5 if !missing(`var'_T5)
}



gen LagAge=.
replace LagAge=Age_T1+1 if !missing(Age_T1)
replace LagAge=Age_T5 if !missing(Age_T5)
gen LagGrade=GradeID_T7
replace LagGrade=4 if GradeID_T3==3
replace LagGrade=GradeID_T8 if LagGrade==.
 
gen DiffDias=date_T8-date_T7
gen DiffDias2=dateTWA-dateEDI
replace DiffDias=. if DiffDias<-365 | DiffDias>365
replace DiffDias2=. if DiffDias2<-365 | DiffDias2>365
	 
merge m:1 SchoolID LagGrade using "$base_out/Consolidated/SchoolsGradeAverageScores.dta", keepus(MeanGrade_LagZ_kiswahili MeanGrade_LagZ_kiingereza MeanGrade_LagZ_hisabati)
 
save "$base_out/Consolidated/Student_School_House_Teacher_Char_TWA.dta", replace
	

  

  
