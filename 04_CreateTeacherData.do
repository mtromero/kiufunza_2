use "$basein/15 Endline 2016/Final Deliverable/Final Data/ID Linkages/R10_Teacher_ID_Linkage_nopii.dta", clear
drop if TeacherID==.
keep DistrictID SchoolID TeacherID ETTeacherID upid tchsex upidold R6TeacherID R7TeacherID R8TeacherID R9TeacherID R10TeacherID
merge 1:1 DistrictID SchoolID TeacherID using "$basein/1 Baseline/Teacher/Teacher_noPII.dta", keepus(t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24) update
drop _merge
drop if substr(upid,1,2)!="R1"
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace

use "$basein/2 Midline/MTeacher_noPII.dta",clear
gen upidold=upid
save "$base_out/2 Midline/MTeacher_noPII.dta",replace


use "$basein/15 Endline 2016/Final Deliverable/Final Data/ID Linkages/R10_Teacher_ID_Linkage_nopii.dta", clear
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R6TeacherID R7TeacherID R8TeacherID R9TeacherID R10TeacherID
drop if upidold==""
merge 1:1 upidold using "$base_out/2 Midline/MTeacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24  )  update
drop _merge
drop if substr(upid,1,2)!="R2"

append using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta"
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 
merge 1:1 upidold using "$base_out/2 Midline/MTeacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24  )  update
drop _merge
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 



use "$basein/15 Endline 2016/Final Deliverable/Final Data/ID Linkages/R10_Teacher_ID_Linkage_nopii.dta", clear
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R6TeacherID R7TeacherID R8TeacherID R9TeacherID R10TeacherID
drop if ETTeacherID==.
merge 1:1 ETTeacherID using "$basein/3 Endline/Teacher/ETTeacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24  )  update
drop _merge
drop if substr(upid,1,2)!="R3"

append using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta"
rename t24 t25
rename t23 t24
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 
merge 1:1 ETTeacherID using "$basein/3 Endline/Teacher/ETTeacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24  )  update
drop _merge
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 


use "$basein/15 Endline 2016/Final Deliverable/Final Data/ID Linkages/R10_Teacher_ID_Linkage_nopii.dta", clear
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R6TeacherID R7TeacherID R8TeacherID R9TeacherID R10TeacherID
drop if upid==""
merge 1:1 upid using "$basein/6 Baseline 2014/Data/school/R4Teacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24  tdgtrc3 -tdgtrc11 ) update
drop _merge
drop if substr(upid,1,2)!="R4"
append using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta"
bys upid: gen dup=_n
drop if dup==2
drop dup
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

merge 1:1 upid using "$basein/6 Baseline 2014/Data/school/R4Teacher_noPII.dta", keepus(tdgtrc3 -tdgtrc11 ) update 
drop _merge
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 
merge 1:1 upid using "$basein/6 Baseline 2014/Data/school/R4Teacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24 tdgtrc3 -tdgtrc11) update
drop _merge
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 


use "$basein/15 Endline 2016/Final Deliverable/Final Data/ID Linkages/R10_Teacher_ID_Linkage_nopii.dta", clear
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R6TeacherID R7TeacherID R8TeacherID R9TeacherID R10TeacherID
drop if upid==""
merge 1:1 upid using "$basein/7 Monitoring 2014/Data/School and Teacher/R5Teacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11 )  update
drop _merge
drop if substr(upid,1,2)!="R5"
append using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta"
bys upid: gen dup=_n
drop if dup==2
drop dup
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

merge 1:1 upid  using"$basein/7 Monitoring 2014/Data/School and Teacher/R5Teacher_noPII.dta", keepus(tdgtrc3 -tdgtrc11 )  update 
drop _merge
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 
merge 1:1 upid using "$basein/7 Monitoring 2014/Data/School and Teacher/R5Teacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11)  update
drop _merge
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 





use "$basein/15 Endline 2016/Final Deliverable/Final Data/ID Linkages/R10_Teacher_ID_Linkage_nopii.dta", clear
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R6TeacherID R7TeacherID R8TeacherID R9TeacherID R10TeacherID
drop if R6TeacherID==.
merge 1:1 SchoolID R6TeacherID using "$basein/8 Endline 2014/Final Data/Teacher/R6Teacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11 ) update
drop _merge
drop if substr(upid,1,2)!="R6"
append using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta"
bys upid: gen dup=_n
drop if dup==2
drop dup
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

use "$basein/8 Endline 2014/Final Data/Teacher/R6Teacher_noPII.dta", clear
keep SchoolID R6TeacherID t23
drop if R6TeacherID==.
drop if t23==.
merge 1:m SchoolID R6TeacherID using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", update
drop if _merge==1
drop _merge
compress
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

merge m:1 SchoolID R6TeacherID using "$basein/8 Endline 2014/Final Data/Teacher/R6Teacher_noPII.dta", keepus(tdgtrc3 -tdgtrc11 ) update
drop _merge
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

merge m:1 SchoolID R6TeacherID using "$basein/8 Endline 2014/Final Data/Teacher/R6Teacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11) update
drop _merge
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 



use "$basein/15 Endline 2016/Final Deliverable/Final Data/ID Linkages/R10_Teacher_ID_Linkage_nopii.dta", clear
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R6TeacherID R7TeacherID R8TeacherID R9TeacherID R10TeacherID
drop if R7TeacherID==.
merge 1:1 SchoolID R7TeacherID using "$basein/9 Baseline 2015/Final Data/Teacher/R7Teacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11  fatcon1-fatcon8 trust1-trust9 tses1-tses5)  update
drop _merge
recode t16 t24 t25 fatcon* trust* tses* (-99=.) (-96=.) (-97=.) (-98=.) (-95=.)
drop if substr(upid,1,2)!="R7"
append using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta"
bys upid: gen dup=_n
drop if dup==2
drop dup
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

use "$basein/9 Baseline 2015/Final Data/Teacher/R7Teacher_noPII.dta", clear
keep SchoolID R7TeacherID t23
drop if R7TeacherID==.
drop if t23==.
merge 1:m SchoolID R7TeacherID using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", update
drop if _merge==1
drop _merge
compress
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

merge m:1 SchoolID R7TeacherID using "$basein/9 Baseline 2015/Final Data/Teacher/R7Teacher_noPII.dta", keepus(tdgtrc3 -tdgtrc11  fatcon1-fatcon8 trust1-trust9 tses1-tses5)  update
drop _merge
recode t16 t24 t25 fatcon* trust* tses* (-99=.) (-96=.) (-97=.) (-98=.) (-95=.)
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

merge m:1 SchoolID R7TeacherID using "$basein/9 Baseline 2015/Final Data/Teacher/R7Teacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11  fatcon1-fatcon8 trust1-trust9 tses1-tses5)  update
recode t16 t24 t25 fatcon* trust* tses* (-99=.) (-96=.) (-97=.) (-98=.) (-95=.)
drop _merge
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 


use "$basein/15 Endline 2016/Final Deliverable/Final Data/ID Linkages/R10_Teacher_ID_Linkage_nopii.dta", clear
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R6TeacherID R7TeacherID R8TeacherID R9TeacherID R10TeacherID
drop if R8TeacherID==.
merge 1:m SchoolID R8TeacherID using "$basein/11 Endline 2015/Final Data/Teacher Data/R8Teacher_nopii.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11  fatcon1-fatcon8 trust1-trust9 tses1-tses5 dictatorgame) update
recode t16 t24 t25 fatcon* trust* tses* (-99=.) (-96=.) (-97=.) (-98=.) (-95=.)
drop if R8TeacherID==.
drop _merge
drop if substr(upid,1,2)!="R8"
append using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta"
bys upid: gen dup=_n
drop if dup==2
drop dup
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

use "$basein/11 Endline 2015/Final Data/Teacher Data/R8Teacher_nopii.dta", clear
keep SchoolID R8TeacherID t23
drop if R8TeacherID==.
drop if t23==.
merge 1:m SchoolID R8TeacherID using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", update
drop if _merge==1
drop _merge
compress
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

mmerge SchoolID R8TeacherID using "$basein/11 Endline 2015/Final Data/Teacher Data/R8Teacher_nopii.dta", type(n:1) uif(R8TeacherID!=.) missing(nomatch)  ukeep(tdgtrc3 -tdgtrc11  fatcon1-fatcon8 trust1-trust9 tses1-tses5 dictatorgame) update
drop if _merge==2
drop _merge
recode t16 t24 t25 fatcon* trust* tses* (-99=.) (-96=.) (-97=.) (-98=.) (-95=.)
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

mmerge SchoolID R8TeacherID using "$basein/11 Endline 2015/Final Data/Teacher Data/R8Teacher_nopii.dta", type(n:1) uif(R8TeacherID!=.) missing(nomatch)  ukeep(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11  fatcon1-fatcon8 trust1-trust9 tses1-tses5 dictatorgame) update
recode t16 t24 t25 fatcon* trust* tses* (-99=.) (-96=.) (-97=.) (-98=.) (-95=.)
drop _merge
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 


use "$basein/15 Endline 2016/Final Deliverable/Final Data/ID Linkages/R10_Teacher_ID_Linkage_nopii.dta", clear
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R6TeacherID R7TeacherID R8TeacherID R9TeacherID R10TeacherID
drop if R9TeacherID==.
merge 1:m SchoolID R9TeacherID using "$basein/13 Baseline 2016/Final Data/Teacher Data_nopii/R9Teacher_nopii.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11  fatcon1-fatcon8 trust1-trust9 tses1-tses5 raven1- raven12 waex1- word15) update
recode t16 t24 t25 fatcon* trust* tses* (-99=.) (-96=.) (-97=.) (-98=.) (-95=.)
drop if R9TeacherID==.
drop _merge
drop if substr(upid,1,2)!="R9"
append using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta"
bys upid: gen dup=_n
drop if dup==2
drop dup
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

use "$basein/13 Baseline 2016/Final Data/Teacher Data_nopii/R9Teacher_nopii.dta", clear
keep SchoolID R9TeacherID t23
drop if R9TeacherID==.
drop if t23==.
merge 1:m SchoolID R9TeacherID using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", update
drop if _merge==1
drop _merge
compress
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

merge m:1 SchoolID R9TeacherID using "$basein/13 Baseline 2016/Final Data/Teacher Data_nopii/R9Teacher_nopii.dta", keepus(tdgtrc3 -tdgtrc11  fatcon1-fatcon8 trust1-trust9 tses1-tses5 raven1- raven12 waex1- word15) update
drop _merge
recode t16 t24 t25 fatcon* trust* tses* (-99=.) (-96=.) (-97=.) (-98=.) (-95=.)
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

merge m:1 SchoolID R9TeacherID using "$basein/13 Baseline 2016/Final Data/Teacher Data_nopii/R9Teacher_nopii.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11  fatcon1-fatcon8 trust1-trust9 tses1-tses5 raven1- raven12 waex1- word15) update
recode t16 t24 t25 fatcon* trust* tses* (-99=.) (-96=.) (-97=.) (-98=.) (-95=.)
drop _merge
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 



use "$basein/15 Endline 2016/Final Deliverable/Final Data/ID Linkages/R10_Teacher_ID_Linkage_nopii.dta", clear
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R6TeacherID R7TeacherID R8TeacherID R9TeacherID R10TeacherID
drop if R10TeacherID==.
merge 1:m SchoolID R10TeacherID using "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10Teacher_nopii.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11  fatcon1-fatcon8 trust1-trust9 tses1-tses5 raven1- raven12 waex1- word15 dictatorgame) update
drop if R10TeacherID==.
recode t16 t24 t25 fatcon* trust* tses* (-99=.) (-96=.) (-97=.) (-98=.) (-95=.)
drop _merge
merge 1:m SchoolID R10TeacherID using "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10Teacher_nopii.dta", keepus(upid ) update replace
drop if substr(upid,1,3)!="R10"
drop _merge
append using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta"
bys upid: gen dup=_n
drop if dup==2
drop dup
*This is just to update R10 that is incomplete in the linkage file!
merge m:1 upid using "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10Teacher_nopii.dta", keepus(R10TeacherID) update
drop _merge


save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

use "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10Teacher_nopii.dta", clear
keep SchoolID R10TeacherID t23
drop if R10TeacherID==.
drop if t23==.
merge 1:m SchoolID R10TeacherID using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", update
drop if _merge==1
drop _merge
compress
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 



merge m:1 SchoolID R10TeacherID using "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10Teacher_nopii.dta", keepus(tdgtrc3 -tdgtrc11  fatcon1-fatcon8 trust1-trust9 tses1-tses5 raven1- raven12 waex1- word15 dictatorgame) update
drop _merge
recode t16 t24 t25 fatcon* trust* tses* (-99=.) (-96=.) (-97=.) (-98=.) (-95=.)
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 

merge m:1 SchoolID R10TeacherID using "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10Teacher_nopii.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11  fatcon1-fatcon8 trust1-trust9 tses1-tses5 raven1- raven12 waex1- word15 dictatorgame) update
recode t16 t24 t25 fatcon* trust* tses* (-99=.) (-96=.) (-97=.) (-98=.) (-95=.)
drop _merge
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 




use "$basein/1 Baseline/School/Teacher_noPII.dta",clear
keep SchoolID TeacherID teacherGender teacherBorn teacherEdu teacherCert teacherYears1 teacherYears2
rename teacherGender tchsex
rename teacherBorn t05 
rename teacherEdu t16 
rename teacherCert t17 
rename teacherYears1 t07
rename teacherYears2 t08
save "$base_out/1 Baseline/School/Teacher_noPII.dta",replace

use "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", clear
merge m:1 SchoolID TeacherID using "$base_out/1 Baseline/School/Teacher_noPII.dta", keepus(tchsex t05  t07 t08  t16 t17 ) update
drop if _merge==2
drop _merge
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 


use "$basein/6 Baseline 2014/Data/school/R4Teacher_noPII.dta",clear
keep upid tchsex teacherBorn teacherEdu teacherCert teacherYears1 teacherYears2
rename teacherBorn t05 
rename teacherEdu t16 
rename teacherCert t17 
rename teacherYears1 t07
rename teacherYears2 t08
save "$base_out/6 Baseline 2014/R4Teacher_noPII.dta",replace

use "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", clear
merge m:1 upid using "$base_out/6 Baseline 2014/R4Teacher_noPII.dta", keepus(tchsex t05  t07 t08  t16 t17 ) update
drop if _merge==2
drop _merge
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace 



recode t10 (2=0)
*fatcon1 -fatcon8 trust1 -trust9
recode tdgtrc3- tdgtrc11   tses1-tses5 raven1- raven12 waex1- word15(2=0)
forvalues i=4/11{
recode tdgtrc`i' (.=0) if tdgtrc`=scalar(`i'-1)'==0
}
forvalues i=6/15{
recode word`i' (.=0) if word`=scalar(`i'-1)'==0
}



replace upid="R8TCH"+string(SchoolID,"%04.0f")+string(R8TeacherID,"%02.0f") if upid==""
bys upid: gen dup=_n
drop if dup==2
drop dup
save "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace

merge 1:1 upid using "$basein/15 Endline 2016/Final Deliverable/Final Data/ID Linkages/R10_Teacher_ID_Linkage_nopii.dta", keepus(intvr10 intvr9 intvr8 intvr7)
drop if _merge!=3
drop if intvr10!=1 & intvr9!=1 & intvr8!=1 & intvr7!=1
drop _merge

pca tdgtrc3- tdgtrc11
predict IndexRecall, score

pca fatcon1 -fatcon8
predict IndexFatal,score

pca trust1 -trust9
predict IndexTurst,score

pca tses1-tses5
predict IndexSelfAssesment,score

pca raven1- raven12
predict IndexRaven,score

recode waex1- word15 (3=.)
pca waex1- word15
predict IndexWord,score

irt	(2pl tdgtrc3- tdgtrc11 raven1- raven12 waex1- word15), difficult intpoints(10)	
predict IRTAbility, latent		
			
foreach var in IndexRecall IndexFatal IndexTurst IndexSelfAssesment IndexRaven IndexWord IRTAbility{
	sum `var'
	replace `var'=(`var'-r(mean))/r(sd)
}

*drop tdgtrc3- tdgtrc11  tmihwa_1-tmihwa_8 mtstst_a-mtstst_f fatcon1 -fatcon8 trust1 -trust9 tses1-tses5
label var IndexRecall "Memory (PCA)"
label var IndexFatal "Locus of Control (PCA)"
label var IndexTurst "Trust (PCA)"
label var IndexSelfAssesment "Self-esteem (PCA)"
label var IndexRaven "Raven (PCA)"
label var IndexWord "Word Association (PCA)"
label var IRTAbility "Ability (IRT)"
gen MaleTeacher=(tchsex==1)
saveold "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", replace version(12)




************ WHO TEACHES WHAT IN YR 1 ******************************

use "$basein/11 Endline 2015/Final Data/Teacher Data/R8TGrdSubGrp_nopii.dta", clear
drop if tchgrpyn!=1
mmerge R8TeacherID SchoolID  using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", uif(R8TeacherID!=.) type( n:1 ) ukeep(t23 upid tchsex t05- t25 IndexRecall- IndexWord IRTAbility dictatorgame)
drop if _merge!=3
drop _merge
gen MaleTeacher=(tchsex==1)
collapse (count) N=MaleTeacher (mean) t05 t07 t10 t23 t24 t25 IndexRecall IndexFatal IndexTurst IndexSelfAssesment IndexRaven IndexWord MaleTeacher IRTAbility dictatorgame, by(SchoolID R8TGradeID R8TGrdSub2ID)
tostring R8TGrdSub2ID, replace
replace R8TGrdSub2ID="_hisabati_T2" if R8TGrdSub2ID=="1"
replace R8TGrdSub2ID="_kiswahili_T2" if R8TGrdSub2ID=="2"
replace R8TGrdSub2ID="_kiingereza_T2" if R8TGrdSub2ID=="3"

reshape wide N- dictatorgame, i(SchoolID R8TGradeID) j(R8TGrdSub2ID) string
rename R8TGradeID GradeID_T2
save "$base_out/ConsolidatedYr34/AverageTeacher_T2.dta", replace

************ WHO TEACHES WHAT IN YR 2 ******************************

use "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10TGrdSubGrp_nopii.dta", clear
drop if tchgrpyn!=1
mmerge R10TeacherID SchoolID  using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", uif(R10TeacherID!=.) type( n:1 ) ukeep(t23 upid tchsex t05- t25 IndexRecall- IndexWord IRTAbility dictatorgame)
drop if _merge!=3
drop _merge
mmerge R10TeacherID SchoolID  using "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data//R10Teacher_nopii.dta", uif(R10TeacherID!=.) type( n:1 ) ukeep(rate_*)
drop if _merge==2
drop _merge
recode  rate_* (-99=.)
pca rate_*
predict HT_Rating,score

gen MaleTeacher=(tchsex==1)
collapse (count) N=MaleTeacher (mean) HT_Rating t05 t07 t10 t23 t24 t25 IndexRecall IndexFatal IndexTurst IndexSelfAssesment IndexRaven IndexWord MaleTeacher IRTAbility dictatorgame, by(SchoolID R10TGradeID R10TGrdSub2ID)
tostring R10TGrdSub2ID, replace
replace R10TGrdSub2ID="_hisabati_T5" if R10TGrdSub2ID=="1"
replace R10TGrdSub2ID="_kiswahili_T5" if R10TGrdSub2ID=="2"
replace R10TGrdSub2ID="_kiingereza_T5" if R10TGrdSub2ID=="3"

reshape wide N- dictatorgame, i(SchoolID R10TGradeID) j(R10TGrdSub2ID) string
rename R10TGradeID GradeID_T5
save "$base_out/ConsolidatedYr34/AverageTeacher_T5.dta", replace



************************************************************************************************************************************************************************
************************************************************************************************************************************************************************
************************************************************************************************************************************************************************
************************************************************************************************************************************************************************
************************************************************************************************************************************************************************
************************************************************************************************************************************************************************
************************************************************************************************************************************************************************
************************************************************************************************************************************************************************

use "$basein/11 Endline 2015/Final Data/Teacher Data/R8Time_nopii.dta", clear
drop if R8TeacherID==.
reshape wide t27, i(SchoolID R8TeacherID) j(R8TimeID)
label variable t271 "Preparing class (Mins)"
label variable t272 "Grading homework (Mins)"
label variable t273 "Grading tests (Mins)"
label variable t274 "Getting children to school (Mins)"
label variable t275 "Regular classes (Mins)"
label variable t276 "Extra classes (Mins)"
label variable t277 "Socializing (Mins)"
save "$base_out/ConsolidatedYr34/Teachers.dta", replace





use "$basein/11 Endline 2015/Final Data/Teacher Data/R8TGrdSub3_nopii.dta", clear
drop if R8TeacherID==.
recode txtavl notest (-99=.) (-96=.) (-95=.)
recode t73 tcosyn tcspyn tcspsal ttutyn tremyn tremac (2=0) 
collapse (mean) gsbhrs notest  tcspyn tcspsal ttutyn tremyn tremac txtavl t73 tcosyn natcur , by(SchoolID R8TeacherID )
pca gsbhrs notest  tcspyn tcspsal ttutyn tremyn tremac txtavl t73 tcosyn natcur 
predict TechniquesScore, score 
merge 1:1 SchoolID R8TeacherID using "$base_out/ConsolidatedYr34/Teachers.dta"
drop _merge
save "$base_out/ConsolidatedYr34/Teachers.dta", replace


use "$basein/11 Endline 2015/Final Data/Teacher Data/R8TFocGrdSub_nopii.dta", clear
drop if R8TeacherID==.
recode earnbns20151-earnbns20159 (.=0) (1/9 = 1)  if fgsbnsel2015==1
collapse (mean) earnbns20151-earnbns20159 , by(SchoolID R8TeacherID )
pca earnbns20151-earnbns20159 
predict ChangesScore, score 
merge 1:1 SchoolID R8TeacherID using "$base_out/ConsolidatedYr34/Teachers.dta"
drop _merge
save "$base_out/ConsolidatedYr34/Teachers.dta", replace


use "$basein/11 Endline 2015/Final Data/Teacher Data/R8Teacher_nopii.dta", clear
drop if R8TeacherID==.
recode missedClasses (2=0)
gen TimeAtSchool=(tdptme- tartme)*15/60
label variable 	TimeAtSchool "Time at school (Hrs)"
keep SchoolID R8TeacherID TimeAtSchool missedClasses 
merge 1:1 SchoolID R8TeacherID using "$base_out/ConsolidatedYr34/Teachers.dta"
drop _merge
save "$base_out/ConsolidatedYr34/Teachers.dta", replace


merge 1:m SchoolID R8TeacherID using "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta"
drop if _merge!=3
drop _merge
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge

rename * =_T2
foreach var in SchoolID R8TeacherID upid tchsex DistrictID TeacherID ETTeacherID R6TeacherID R7TeacherID upidold t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25 IndexRecall IndexFatal IndexTurst IndexSelfAssesment StrataScore treatment2 treatarm2 treatment treatarm{
rename `var'_T2 `var'
}

save "$base_out/ConsolidatedYr34/Teachers.dta", replace


****************************************
****************************************
****** OBSERVATION ENDLINE Yr1 *************

use "$basein/11 Endline 2015/Final Data/Observation Data/R8Obs_nopii.dta", clear


recode unobs_actvts1-unobs_actvts_96 (.=0) (1/100=1)
recode unobs_actvts1-unobs_actvts19 unobs_actvts_96 (0=.) if unobs_actvts18==1

/*
gen ObsNotInClass=(unobs_actvts18==1) & !missing(unobs_actvts18)
gen ObsActiveTeaching=(unobs_actvts1==1) & !missing(unobs_actvts1)
gen ObsRepeating=(unobs_actvts11==1) & !missing(unobs_actvts11)
*/

egen UnObsTeaching=rowtotal(unobs_actvts1 unobs_actvts2 unobs_actvts3 unobs_actvts6 unobs_actvts7 unobs_actvts8 unobs_actvts10 unobs_actvts11 unobs_actvts12)
egen UnObsClassManagement=rowtotal(unobs_actvts4 unobs_actvts5 unobs_actvts13 unobs_actvts19)
egen UnObsOffTask=rowtotal(unobs_actvts16 unobs_actvts17 unobs_actvts18)
egen UnObsStudentOffTask=rowtotal(unobs_actvts14 unobs_actvts15)

foreach var in UnObsTeaching UnObsClassManagement UnObsOffTask UnObsStudentOffTask{
	replace `var'=(`var'>0) if !missing(`var')
}
 
pca unobs_actvts1-unobs_actvts19
predict UnObsIndex,score

gen desk=numdesks
replace desk=numdesks*2 if typedesks==1
replace desk=numdesks*1.5 if typedesks==-96
sum desk,d
replace desk=. if desk>r(p99)

gen crowding=numpupils/desk
gen dirtfloor=(floormaterial==2) if !missing(floormaterial)
recode electricity (2/3=0)
recode  assignwork studentsfloor blackboard otherboard chalk workdisplay chartsdisplay trash hitting teachertextbook studentstextbook teacherblackboard studentsblackboard teachermaterials individuals grading registers callnames teachersmile teacherthreaten teacherkind teacherhit teacherphone teacherleave drill dictation systematic task (2=0)

gen blackboardandchalk=((blackboard==1 | otherboard==1 ) & chalk==1)

gen proptext=numtextbooks/numpupils
gen proppen=penbook/numpupils

gen time10plusgrading=(timegrading==3) if !missing(timegrading)
gen bestseatedCluster=(bestseated==1) if !missing(bestseated)

pca crowding studentsfloor dirtfloor  blackboardandchalk electricity 
predict ClasroomIndex, score

replace numcharts=0 if chartsdisplay==0
pca workdisplay chartsdisplay  numcharts
predict AmbientIndex, score

pca hitting  teacherthreaten  teacherhit 
predict TeachingIndex, score

pca proptext proppen 
predict InputsIndex, score

label var  UnObsStudentOffTask "Off task"
label var UnObsStudentOffTask "Student Off task"
label var  UnObsTeaching "Teaching"
label var  UnObsClassManagement "Classroom management"
label var  UnObsIndex "Index (PCA)"


label var  crowding "Students/desk"
label var  studentsfloor "Students sitting on the floor"
label var  dirtfloor "Dirt floor"
label var  blackboardandchalk "Blacboard and chalk"
label var  electricity "Electricity"
label var  ClasroomIndex "Classroom (PCA)"

label var  workdisplay "Student's work on walls"
label var  chartsdisplay "Charts on walls"
label var  trash "Trash"
label var  AmbientIndex "Ambient (PCA)"


label var  hitting "Students misbehaving"
label var  teachertextbook "Teacher uses textbook"
label var  studentstextbook "Students use textbook"
label var  teacherblackboard "Teacher uses blackboard"
label var  studentsblackboard "Students use blackboard"
label var  individuals "Teacher gives individual attention"
label var  grading "Teacher grades"
label var  registers "Teacher does admin work"
label var  callnames "Teacher calls a student by name"
label var  teachersmile "Teacher smiles"
label var  teacherthreaten "Teachers uses threatening language"
label var  teacherkind "Teacher is kind"
label var  teacherhit "Teacher hits students"
label var  teacherphone "Teachers uses his phone"
label var  teacherleave "Teacher leaves"
label var  drill "Repeating excercise"
label var  dictation "Dictation excercise"
label var  systematic "Teacher is systematic"
label var  task "Teacher assings task"
label var  TeachingIndex "Teacher (PCA)"

label var  proptext "Proportion of students with excercise book"
label var  proppen "Proportion of students with pen/pencil"
label var  InputsIndex "Inputs (PCA)"

keep SchoolID R8ObsID grade subject second_teacher $UnObs $PhysicalClassroom $AmbientClassroom $TeachingActivities $Inputs
recode $UnObs $PhysicalClassroom $AmbientClassroom $TeachingActivities $Inputs (-95=.) (-96=.) (-97=.) (-98=.) (-99=.)

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge

/*
rename * =_T2
foreach var in SchoolID R8ObsID grade subject second_teacher DistrictID StrataScore treatment2 treatarm2 treatment treatarm{
rename `var'_T2 `var'
}
*/
gen time="T2"

save "$base_out/ConsolidatedYr34/Obs_T2.dta", replace




****************************************
****************************************
****** OBSERVATION ENDLINE Yr2 *************


use "$basein/15 Endline 2016/Final Deliverable/Final Data/Observation Data/R10Obs_nopii.dta", clear
keep SchoolID R10ObsID grade subject teaching* grading* inclass_noteaching* noclass_noactivity* noclass_activity*
rename teachingTEN  teaching10
rename gradingTEN grading10
rename inclass_noteachingTEN inclass_noteaching10
rename noclass_noactivityTEN noclass_noactivity10
rename noclass_activityTEN noclass_activity10
reshape long teaching@ grading@ inclass_noteaching@ noclass_noactivity@ noclass_activity@, i(SchoolID R10ObsID grade subject) j(obs)
foreach var in teaching grading inclass_noteaching noclass_noactivity noclass_activity{
	replace `var'="0" if `var'==""
	replace `var'="0" if `var'=="N"
	replace `var'="1" if `var'=="Y"
}

destring, replace
replace teaching=0 if grading==1
egen T=rowtotal( teaching- noclass_activity)
drop if T==0
collapse (mean) teaching grading inclass_noteaching noclass_noactivity noclass_activity, by(SchoolID R10ObsID grade subject)
rename * =_T5
foreach var in SchoolID R10ObsID grade subject{
rename `var'_T5 `var'
}

save "$base_out/ConsolidatedYr34/Obs_T5.dta", replace


use "$basein/15 Endline 2016/Final Deliverable/Final Data/Observation Data/R10Obs_nopii.dta", clear


recode unobs_actvts1-unobs_actvts_96 (.=0) (1/100=1)
recode unobs_actvts1-unobs_actvts19 unobs_actvts_96 (0=.) if unobs_actvts18==1

/*
gen ObsNotInClass=(unobs_actvts18==1) & !missing(unobs_actvts18)
gen ObsActiveTeaching=(unobs_actvts1==1) & !missing(unobs_actvts1)
gen ObsRepeating=(unobs_actvts11==1) & !missing(unobs_actvts11)
*/

egen UnObsTeaching=rowtotal(unobs_actvts1 unobs_actvts2 unobs_actvts3 unobs_actvts6 unobs_actvts7 unobs_actvts8 unobs_actvts9 unobs_actvts11 unobs_actvts12), missing
egen UnObsClassManagement=rowtotal(unobs_actvts4 unobs_actvts5 unobs_actvts13 unobs_actvts19), missing
egen UnObsOffTask=rowtotal(unobs_actvts16 unobs_actvts17 unobs_actvts18), missing
egen UnObsStudentOffTask=rowtotal(unobs_actvts14 unobs_actvts15), missing

foreach var in UnObsTeaching UnObsClassManagement UnObsOffTask UnObsStudentOffTask{
	replace `var'=(`var'>0) if !missing(`var')
}

pca unobs_actvts1-unobs_actvts19
predict UnObsIndex,score

gen desk=numdesks
replace desk=numdesks*2 if typedesks==1
replace desk=numdesks*1.5 if typedesks==-96
sum desk,d
replace desk=. if desk>r(p99)

gen crowding=numpupils/desk
gen dirtfloor=(floormaterial==2) if !missing(floormaterial)
recode electricity (2/3=0)
recode   studentsfloor blackboard otherboard chalk workdisplay chartsdisplay trash hitting  teachermaterials teachersmile teacherthreaten teacherkind teacherhit teacherphone teacherleave (2=0)

gen blackboardandchalk=((blackboard==1 | otherboard==1 ) & chalk==1)

gen proptext=numtextbooks/numpupils
gen proppen=penbook/numpupils


gen bestseatedCluster=(bestseated==1) if !missing(bestseated)

pca crowding studentsfloor dirtfloor  blackboardandchalk electricity 
predict ClasroomIndex, score

replace numcharts=0 if chartsdisplay==0
pca workdisplay chartsdisplay  numcharts
predict AmbientIndex, score

pca hitting  teacherthreaten  teacherhit 
predict TeachingIndex, score

recode sleep (2=0) 
pca proptext proppen 
predict InputsIndex, score 

label var  UnObsStudentOffTask "Off task"
label var UnObsStudentOffTask "Student Off task"
label var  UnObsTeaching "Teaching"
label var  UnObsClassManagement "Classroom management"
label var  UnObsIndex "Index (PCA)"

label var  crowding "Students/desk"
label var  studentsfloor "Students sitting on the floor"
label var  dirtfloor "Dirt floor"
label var  blackboardandchalk "Blacboard and chalk"
label var  electricity "Electricity"
label var  ClasroomIndex "Classroom (PCA)"

label var  workdisplay "Student's work on walls"
label var  chartsdisplay "Charts on walls"
label var  trash "Trash"
label var  AmbientIndex "Ambient (PCA)"


label var  hitting "Students misbehaving"
label var  teachersmile "Teacher smiles"
label var  teacherthreaten "Teachers uses threatening language"
label var  teacherkind "Teacher is kind"
label var  teacherhit "Teacher hits students"
label var  teacherphone "Teachers uses his phone"
label var  teacherleave "Teacher leaves"
label var  TeachingIndex "Teacher (PCA)"

label var  proptext "Proportion of students with excercise book"
label var  proppen "Proportion of students with pen/pencil"
label var  InputsIndex "Inputs (PCA)"


keep SchoolID R10ObsID grade subject $UnObs $PhysicalClassroom $AmbientClassroom $TeachingActivities $Inputs sleep
recode $UnObs $PhysicalClassroom $AmbientClassroom $TeachingActivities $Inputs (-95=.) (-96=.) (-97=.) (-98=.) (-99=.)

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge



merge 1:1  SchoolID R10ObsID grade subject using  "$base_out/ConsolidatedYr34/Obs_T5.dta"
drop _merge
gen time="T5"
save "$base_out/ConsolidatedYr34/Obs_T5.dta", replace




use "$base_out/ConsolidatedYr34/Obs_T2.dta"
append using "$base_out/ConsolidatedYr34/Obs_T5.dta"
save "$base_out/ConsolidatedYr34/Obs.dta", replace

*common 
*studentsfloor workdisplay chartsdisplay trash hitting
*teachersmile teacherthreaten teacherkind teacherhit
*teacherphone teacherleave
*electricity
*UnObsTeaching UnObsClassManagement UnObsOffTask UnObsStudentOffTask
*crowding dirtfloor blackboardandchalk proptext proppen


*****************************************************************
*************** ATTENDANCE USING MONITORING DATA ****************
*****************************************************************

use "$basein/10 Monitoring 2015/Teacher Attendance Data/ML2015TeacherAttendance_nopii.dta", clear

foreach var in attbtok atttch attcls{
replace `var'="1" if `var'=="NDIO"
replace `var'="0" if `var'=="HAPANA"
replace `var'="" if `var'=="--blank--"
}
foreach var in attact attact2{
replace `var'="" if `var'=="--blank--"
}
destring, replace
replace attact=. if attact==4
recode attact (-99=.) (2=1) (3=0)
replace attcls=0 if atttch==0
replace attact=0 if atttch==0 | attcls==0
label var attact "Teaching"
gen OffTask=1-(attact==1 | attact2==2 | attact2==3 | attact2==4) if !missing(attact)

keep schoolid attbtok atttch attcls attact OffTask
rename schoolid SchoolID
collapse (mean) attbtok atttch attcls attact  OffTask,by(SchoolID)
label var attbtok "Attendance book"
label var atttch "Attendance (verified)"
label var attcls "In classroom"
label var attact "Teaching"

rename * =_T2
rename SchoolID_T2 SchoolID
save "$base_out/ConsolidatedYr34/TAttendace.dta", replace


use "$basein/14 Monitoring 2016/Teacher Attendance Data/Data/MLTAChecks_CLEAN_nopii.dta", clear
keep atttch attbtok SchoolID attcls_s1 attact_op1_s1  attact2_s1
rename attcls_s1 attcls
rename attact_op1_s1 attact
rename attact2_s1 attact2
foreach var in attbtok atttch attcls{
replace `var'="1" if `var'=="NDIO"
replace `var'="0" if `var'=="HAPANA"
replace `var'="" if `var'=="--blank--"
}

 
destring, replace
recode attact (-99=.) (10/23=1) (30/40=0)
replace attcls=0 if atttch==0
replace attact=0 if atttch==0 | attcls==0
label var attact "Teaching"
gen OffTask=1-(attact==1 | attact2==2 | attact2==3 | attact2==4) if !missing(attact)

keep SchoolID attbtok atttch attcls attact OffTask
collapse (mean) attbtok atttch attcls attact  OffTask,by(SchoolID)

label var attbtok "Attendance book"
label var atttch "Attendance (verified)"
label var attcls "In classroom"
label var attact "Teaching"


rename * =_T5
rename SchoolID_T5 SchoolID
merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/TAttendace.dta"
drop _merge
save "$base_out/ConsolidatedYr34/TAttendace.dta", replace

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
save "$base_out/ConsolidatedYr34/TAttendace.dta", replace


********************************************************************************************************
*************** ATTENDANCE USING MONITORING DATA  ONLY FOR FOCAL GRADE SUBJECT TEACHERS ****************
********************************************************************************************************
use "$basein/11 Endline 2015/Final Data/Teacher Data/R8TGrdSub2_nopii.dta", clear
keep if (R8TGradeID==1 | R8TGradeID==2 | R8TGradeID==3) & (R8TGrdSub2ID==1 | R8TGrdSub2ID==2) & tgrdyn==1
keep SchoolID- R8TGrdSub2ID tgrdyn
keep SchoolID R8TeacherID
duplicates drop
merge 1:m SchoolID R8TeacherID using "$basein/11 Endline 2015/ID Linkages/R8_Teacher_ID_Linkage_Version2_noPII.dta", keepus(upid TeacherID MNewTeacherID- R7TeacherID upidold)
drop if _merge==2
drop _merge
drop if R7TeacherID==.
tempfile temp1
save `temp1'


use "$basein/10 Monitoring 2015/Teacher Attendance Data/ML2015TeacherAttendance_nopii.dta", clear

foreach var in attbtok atttch attcls{
replace `var'="1" if `var'=="NDIO"
replace `var'="0" if `var'=="HAPANA"
replace `var'="" if `var'=="--blank--"
}
foreach var in attact attact2{
replace `var'="" if `var'=="--blank--"
}
destring, replace
replace attact=. if attact==4
recode attact (-99=.) (2=1) (3=0)
replace attcls=0 if atttch==0
replace attact=0 if atttch==0 | attcls==0
label var attact "Teaching"
gen OffTask=1-(attact==1 | attact2==2 | attact2==3 | attact2==4) if !missing(attact)

keep schoolid attbtok atttch attcls attact OffTask teachID teachID_s teachID_Unique
rename schoolid SchoolID
rename teachID R7TeacherID
merge 1:1 SchoolID R7TeacherID using `temp1'
drop if _merge!=3
drop _merge

collapse (mean) attbtok atttch attcls attact  OffTask,by(SchoolID)
label var attbtok "Attendance book"
label var atttch "Attendance (verified)"
label var attcls "In classroom"
label var attact "Teaching"

rename * =_T2
rename SchoolID_T2 SchoolID
save "$base_out/ConsolidatedYr34/TAttendace_Focal.dta", replace


use "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10TGrdSub2_nopii.dta", clear
keep if (R10TGradeID==1 | R10TGradeID==2 | R10TGradeID==3) & (R10TGrdSub2ID==1 | R10TGrdSub2ID==2) & tgrdyn==1
keep SchoolID- R10TGrdSub2ID tgrdyn
keep SchoolID R10TeacherID
duplicates drop
merge 1:m SchoolID R10TeacherID using "$basein/15 Endline 2016/Final Deliverable/Final Data/ID Linkages/R10_Teacher_ID_Linkage_nopii.dta", keepus(upid TeacherID MNewTeacherID- R9TeacherID upidold)
drop if _merge==2
drop _merge
drop if R9TeacherID==.
tempfile temp1
save `temp1'



use "$basein/14 Monitoring 2016/Teacher Attendance Data/Data/MLTAChecks_CLEAN_nopii.dta", clear
keep atttch attbtok SchoolID attcls_s1 attact_op1_s1  attact2_s1 R9TeacherID 
rename attcls_s1 attcls
rename attact_op1_s1 attact
rename attact2_s1 attact2
foreach var in attbtok atttch attcls{
replace `var'="1" if `var'=="NDIO"
replace `var'="0" if `var'=="HAPANA"
replace `var'="" if `var'=="--blank--"
}

 
destring, replace
recode attact (-99=.) (10/23=1) (30/40=0)
replace attcls=0 if atttch==0
replace attact=0 if atttch==0 | attcls==0
label var attact "Teaching"
gen OffTask=1-(attact==1 | attact2==2 | attact2==3 | attact2==4) if !missing(attact)
merge 1:1 SchoolID R9TeacherID using `temp1'
drop if _merge!=3
drop _merge


keep SchoolID attbtok atttch attcls attact OffTask
collapse (mean) attbtok atttch attcls attact  OffTask,by(SchoolID)

label var attbtok "Attendance book"
label var atttch "Attendance (verified)"
label var attcls "In classroom"
label var attact "Teaching"


rename * =_T5
rename SchoolID_T5 SchoolID
merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/TAttendace_Focal.dta"
drop _merge
save "$base_out/ConsolidatedYr34/TAttendace_Focal.dta", replace

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
save "$base_out/ConsolidatedYr34/TAttendace_Focal.dta", replace

*****************************************************************
*************** Teacher Understanding ****************
*****************************************************************
use "$basein/11 Endline 2015/Final Data/Teacher Data/R8Teacher_nopii.dta", clear
keep  SchoolID R8TeacherID ETeach_pp upid tcodhttpay tcodfar codort codops codoif codopt knwtwa2 trstwa sgnpet wchprg programprefer topbpm toppal passAll bonusSkills SkillsWorth HTEarn1 elgsub_levels1 elgsub_levels2 elgsub_levels3 GroupsAbility EarnFail StudentAB HTEarn2 elgsub_gain1 elgsub_gain2 elgsub_gain3

gen PreferLevels=(programprefer==1) & !missing(programprefer)
label var PreferLevels "Single test"

label define topbm_l 1 "Very favorable" 2 "Somewhat favorable" 3 "Neutral" 4 "Somewhat unfavorable" 5 "Very unfavorable"
label var topbpm topbm_l

gen FavorableBonusPayments=(topbpm==1 | topbpm==2) & !missing(topbpm)
label var PreferLevels "Bonus payments"

recode knwtwa2 (2=0)
recode sgnpet trstwa tcodhttpay (1/2=1) (3/5=0)

recode SkillsWorth 1=0 2=1 3=0
recode GroupsAbility 2=0
recode EarnFail 2=0
recode StudentAB 2=0
recode passAll 2=1 1=0
recode bonusSkills 2=0 
recode HTEarn1 HTEarn2 (2=1) (-99=0) (1=0) (3=0) (4=0)
recode elgsub_levels1 elgsub_levels3 elgsub_gain1  elgsub_gain3 (2=1) (1=0)
recode elgsub_levels2 elgsub_gain2 (2=0)
egen FracStadi=rowtotal(bonusSkills SkillsWorth elgsub_levels1 elgsub_levels2 elgsub_levels3), missing
egen FracMash=rowtotal(GroupsAbility StudentAB elgsub_gain1 elgsub_gain2 elgsub_gain3), missing
gen FracCorrect=.
replace FracCorrect=FracStadi/5 if FracStadi!=.
replace FracCorrect=FracMash/5 if FracMash!=.

drop FracStadi FracMash
egen FracStadi=rowtotal(bonusSkills SkillsWorth), missing
egen FracMash=rowtotal(GroupsAbility StudentAB), missing
gen FracCorrectCore=.
replace FracCorrectCore=FracStadi/2 if FracStadi!=.
replace FracCorrectCore=FracMash/2 if FracMash!=.
drop FracStadi FracMash

recode codort codops codoif codopt (-99=.) (8=0) (6/7=1)
egen MoneyEvenifFail=rowtotal(EarnFail passAll), missing

collapse (count) NumTeachers=R8TeacherID (mean) PreferLevels FavorableBonusPayments knwtwa2 sgnpet trstwa tcodhttpay FracCorrect FracCorrectCore codort codops codoif codopt MoneyEvenifFail, by(SchoolID)
label var NumTeachers "Number of teachers"
label var PreferLevels "Single test"
label var FavorableBonusPayments "Bonus payments"
label var knwtwa2 "Know Twaweza"
label var sgnpet "Support Twaweza"
label var trstwa "Trust Twaweza"
label var tcodhttpay "Trust payments"
label var FracCorrect "Understanding"
label var FracCorrectCore "Understanding (core)"
label var codort "Resources (taken away)"
label var codops "Share"
label var codoif "Interfered"
label var codopt "Parent Support"
label var MoneyEvenifFail "Can earn bonus even if student fails"
rename * =_T2
rename SchoolID_T2 SchoolID
save "$base_out/ConsolidatedYr34/TUnderstanding.dta", replace


use "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10Teacher_nopii.dta", clear
keep  SchoolID R10TeacherID ETeach_pp upid tcodhttpay tcodfar codort codops codoif codopt knwtwa2 trstwa sgnpet wchprg programprefer topbpm toppal passAll bonusSkills SkillsWorth tearnl elgsub_levels1 elgsub_levels2 elgsub_levels3 GroupsAbility EarnFail StudentAB tearng elgsub_gain1 elgsub_gain2 elgsub_gain3


gen PreferLevels=(programprefer==1) & !missing(programprefer)
label var PreferLevels "Single test"

gen FavorableBonusPayments=(topbpm==1 | topbpm==2) & !missing(topbpm)
label var PreferLevels "Bonus payments"

recode knwtwa2 (2=0)
recode sgnpet trstwa tcodhttpay (1/2=1) (3/5=0)

recode SkillsWorth 1=0 2=1 3=0
recode GroupsAbility 2=0
recode EarnFail 2=0
recode StudentAB 2=0
recode passAll 2=1 1=0
recode bonusSkills 2=0 
recode tearnl tearng (2=0) 
recode elgsub_levels1 elgsub_levels3 elgsub_gain1  elgsub_gain3 (2=1) (1=0)
recode elgsub_levels2 elgsub_gain2 (2=0)
egen FracStadi=rowtotal(bonusSkills SkillsWorth elgsub_levels1 elgsub_levels2 elgsub_levels3), missing
egen FracMash=rowtotal(GroupsAbility StudentAB elgsub_gain1 elgsub_gain2 elgsub_gain3), missing
gen FracCorrect=.
replace FracCorrect=FracStadi/5 if FracStadi!=.
replace FracCorrect=FracMash/5 if FracMash!=.

drop FracStadi FracMash
egen FracStadi=rowtotal(bonusSkills SkillsWorth), missing
egen FracMash=rowtotal(GroupsAbility StudentAB), missing
gen FracCorrectCore=.
replace FracCorrectCore=FracStadi/2 if FracStadi!=.
replace FracCorrectCore=FracMash/2 if FracMash!=.
drop FracStadi FracMash
egen MoneyEvenifFail=rowtotal(EarnFail passAll), missing

recode codort codops codoif codopt (-99=.) (8=0) (6/7=1)
collapse (count) NumTeachers=R10TeacherID (mean) PreferLevels FavorableBonusPayments knwtwa2 sgnpet trstwa tcodhttpay FracCorrect FracCorrectCore codort codops codoif codopt MoneyEvenifFail, by(SchoolID)
label var NumTeachers "Number of teachers"
label var PreferLevels "Single test"
label var FavorableBonusPayments "Bonus payments"
label var knwtwa2 "Know Twaweza"
label var sgnpet "Support Twaweza"
label var trstwa "Trust Twaweza"
label var tcodhttpay "Trust payments"
label var FracCorrect "Understanding"
label var FracCorrectCore "Understanding (core)"
label var codort "Resources (taken away)"
label var codops "Share"
label var codoif "Interfered"
label var codopt "Parent Support"
label var MoneyEvenifFail "Can earn bonus even if student fails"

rename * =_T5
rename SchoolID_T5 SchoolID

merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/TUnderstanding.dta"
drop _merge
save "$base_out/ConsolidatedYr34/TUnderstanding.dta", replace

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
save "$base_out/ConsolidatedYr34/TUnderstanding.dta", replace


*****************************************************************
*************** Teacher substitution of resouces ****************
*****************************************************************
use "$basein/11 Endline 2015/Final Data/Teacher Data/R8Teacher_nopii.dta", clear
keep  SchoolID R8TeacherID ETeach_pp upid mchtch14 mchtch21 whooth tatryn tainyn twkcon tptint- trtgpt
recode mchtch14 -trtgpt (-99=.) (-98=.) (-97=.) (-96=.) (-95=.)
sum mchtch14,d
replace mchtch14=r(p99) if mchtch14>r(p99) & !missing(mchtch14)
recode mchtch21 (1=0) (2/7=1)
replace whooth=(whooth>1) if !missing(whooth)
recode tatryn tainyn twkcon tptint (1=0) (2=1) (3=0)
recode trtgti trtgas trtght trtgot trtgpt (1/2=1) (3/6=0)
collapse (count) NumTeachers=R8TeacherID (mean) mchtch14 -trtgpt, by(SchoolID)
label var mchtch14 "HT meetings"
label var mchtch21 "Classroom obs"
label var whooth "Pedagogical help"
label var tatryn "Less training"
label var tainyn "Less inputs"
label var twkcon "Worse conditions"
label var tptint "Parents less interested"
label var trtgti "Resources"
label var trtgas "Assitance"
label var trtght "HT"
label var trtgot "Other teachers"
label var trtgpt "Parents/Community"

rename * =_T2
rename SchoolID_T2 SchoolID
save "$base_out/ConsolidatedYr34/TSupport.dta", replace


use "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10Teacher_nopii.dta", clear
keep  SchoolID R10TeacherID ETeach_pp upid mchtch14 mchtch21 whooth tatryn tainyn twkcon tptint- trtgpt
recode mchtch14 -trtgpt (-99=.) (-98=.) (-97=.) (-96=.) (-95=.)
sum mchtch14,d
replace mchtch14=r(p99) if mchtch14>r(p99) & !missing(mchtch14)
recode mchtch21 (1=0) (2/7=1)
replace whooth=(whooth>1) if !missing(whooth)
recode tatryn tainyn twkcon tptint  (1=0) (2=1) (3=0)
recode trtgti trtgas trtght trtgot trtgpt (1/2=1) (3/6=0)
collapse (count) NumTeachers=R10TeacherID (mean) mchtch14 -trtgpt, by(SchoolID)
label var mchtch14 "HT meetings"
label var mchtch21 "Classroom obs"
label var whooth "Pedagogical help"
label var tatryn "Less training"
label var tainyn "Less inputs"
label var twkcon "Worse conditions"
label var tptint "Parents less interested"
label var trtgti "Resources"
label var trtgas "Assitance"
label var trtght "HT"
label var trtgot "Other teachers"
label var trtgpt "Parents/Community"

rename * =_T5
rename SchoolID_T5 SchoolID


merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/TSupport.dta"
drop _merge
save "$base_out/ConsolidatedYr34/TSupport.dta", replace

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
save "$base_out/ConsolidatedYr34/TSupport.dta", replace


*****************************************************************
*************** Comprehension Data from intervention ****************
*****************************************************************

use "$basein/12 Intervention_KFII/0 Baseline 2015/TwaBL2015_SchData_nopii.dta", clear

egen Correct=rowtotal(SMTchrQuiz1 SMTchrQuiz2 SMTchrQuiz3 SMTchrQuiz4),missing
replace Correct=Correct/4
replace Correct=Correct/SMTchrQuiz
rename Correct Correct_Baseline2015
keep SchoolID Correct_Baseline2015
save "$base_out/ConsolidatedYr34/TComprehension_TWA.dta", replace


use "$basein/12 Intervention_KFII/1.5 Midline 2015/TwaML2015_TchrUnderstanding_nopii.dta", clear
egen Correct=rowtotal(SMTchrQuiz1 SMTchrQuiz2 SMTchrQuiz3),missing
replace Correct=Correct/3
replace Correct=Correct/SMTchrQuiz
rename Correct Correct_Midline2015
keep SchoolID Correct_Midline2015
merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/TComprehension_TWA.dta"
drop _merge
save "$base_out/ConsolidatedYr34/TComprehension_TWA.dta", replace


use "$basein/12 Intervention_KFII/3 Baseline 2016/TwaBL2016_TeacherQuizResults_nopii.dta", clear
gen SMTchrQuiz1=.
gen SMTchrQuiz2=.
gen SMTchrQuiz3=.
gen SMTchrQuiz4=.
gen SMTchrQuiz5=.

replace SMTchrQuiz1=(quizqu_1=="C") 
replace SMTchrQuiz2=(quizqu_2=="B") if  treatarm2==1
replace SMTchrQuiz2=(quizqu_2=="A") if  treatarm2==2
replace SMTchrQuiz3=(quizqu_3=="KWELI") if  treatarm2==1
replace SMTchrQuiz3=(quizqu_3=="SIOKWELI") if  treatarm2==2
replace SMTchrQuiz4=(quizqu_4=="SIOKWELI") if  treatarm2==1
replace SMTchrQuiz4=(quizqu_4=="KWELI") if  treatarm2==2
replace SMTchrQuiz5=(quizqu_5=="KWELI") 



egen Correct=rowtotal(SMTchrQuiz*),missing
replace Correct=Correct/5
rename schoolid SchoolID
collapse (mean) Correct, by(SchoolID)
rename Correct Correct_Baseline2016
merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/TComprehension_TWA.dta"
drop _merge
save "$base_out/ConsolidatedYr34/TComprehension_TWA.dta", replace

/*not really sure how to deal with this one*/
*use "$basein/12 Intervention_KFII/3.5 Midline 2016/1 EDI Back Checks/TW_MAIN_nopii.dta", clear

use "$basein/12 Intervention_KFII/3.5 Midline 2016/3 Twaweza Visits/TwaML2016_School_FINAL.dta", clear
foreach var of varlist smtchrquiz1_a smtchrquiz1_b smtchrquiz1_c smtchrquiz2_a smtchrquiz2_b smtchrquiz2_c smtchrquiz3_t smtchrquiz4_t smtchrquiz4_f smtchrquiz5_t smtchrquiz5_f smtchrquiz6_a smtchrquiz6_b smtchrquiz6_c smtchrquiz7_t smtchrquiz7_f smtchrquiz8_t smtchrquiz8_f smtchrquiz9_t smtchrquiz9_f smtchrquiz10_t smtchrquiz10_f smtchrquiz11_t {
	replace `var'="0" if `var'=="--blank--"
	destring `var', replace
	replace `var'=0 if `var'==.
	}
	
gen correct1=smtchrquiz1_c

gen correct2=smtchrquiz2_b if treatment2=="STADI"
replace correct2=smtchrquiz2_a if treatment2=="MASHINDANO"

gen correct3=smtchrquiz3_t if treatment2=="STADI"
replace correct3=smtchrquiz3_f if treatment2=="MASHINDANO"

gen correct4=smtchrquiz4_f if treatment2=="STADI"
replace correct4=smtchrquiz4_t if treatment2=="MASHINDANO"
gen correct5=smtchrquiz5_t
gen correct9=smtchrquiz9_f if treatment2=="STADI"
replace correct9=smtchrquiz9_t if treatment2=="MASHINDANO"
gen correct10=smtchrquiz10_t
gen correct11=smtchrquiz11_f




egen Correct=rowtotal(correct*), missing
replace Correct=Correct/11
replace Correct=Correct/smtchrquiz
rename schoolid SchoolID
keep Correct SchoolID
rename Correct Correct_Midline2016
merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/TComprehension_TWA.dta"
drop _merge
save "$base_out/ConsolidatedYr34/TComprehension_TWA.dta", replace


use "$base_out/ConsolidatedYr34/TUnderstanding.dta", clear
collapse (mean) FracCorrectCore_T5 FracCorrectCore_T2, by(SchoolID)
rename FracCorrectCore_T5 Correct_Research2016
rename FracCorrectCore_T2 Correct_Research2015
merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/TComprehension_TWA.dta"
drop _merge
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
save "$base_out/ConsolidatedYr34/TComprehension_TWA.dta", replace


