**********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
fvset base default treatarm 
gen Diff_T2=date_twa_T2-date_edi_T2
gen Diff_T5=date_twa_T5-date_edi_T5
gen Week_T2=week(date_edi_T2)
gen Week_T5=week(date_edi_T5)


global AggregateDep 	Z_hisabati Z_kiswahili Z_kiingereza Z_sayansi Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal /*this should be added in the future...*/
global AggregateDep_Karthik 	Z_hisabati Z_kiswahili Z_kiingereza   /*Z_kiingereza  Z_ScoreFocal this should be added in the future...*/
global AggregateDep_int 	Z_hisabati Z_kiswahili Z_kiingereza
 
global AggregateDep_NonEng 	Z_hisabati Z_kiswahili 
global AggregateDep_Eng 	Z_kiingereza

global treatmentlist TreatmentLevels TreatmentGains
gen TreatmentLevels2=TreatmentLevels
gen TreatmentGains2=TreatmentGains
global treatmentlist2 TreatmentLevels2 TreatmentGains2
*enrollment2015_T1 Rural_T1 ClassesOutside_T1 Electricity_T1

      
*c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza)##c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza) ///


global student2 shoes_T2 socks_T2 dirty_T2 uniformdirty_T2 uniformtorn_T2  ringworm_T2 CloseToeShoe_T2

recode bookshome_T2 (-98=.)
global studentoutcomes studentsfight_T2 sing_T2 bookshome_T2

bys SchoolID: egen Z_kiswahili_T0_G=mean(Z_kiswahili_T0)
bys SchoolID: egen Z_kiingereza_T0_G=mean(Z_kiingereza_T0)
bys SchoolID: egen Z_hisabati_T0_G=mean(Z_hisabati_T0)
pca Z_hisabati_T0  Z_kiingereza_T0 Z_kiswahili_T0
predict Z_ScoreFocal_T0, score
*First lets create the tables Karthik Wants

label var Z_kiswahili_T2 Swahili
label var Z_kiswahili_T5 Swahili
label var Z_kiingereza_T2 English
label var Z_kiingereza_T5 English
label var Z_hisabati_T2 Math
label var Z_hisabati_T5 Math
label var Z_sayansi_T2 Science
label var Z_sayansi_T5 Science
label var Z_ScoreFocal_T2 "Focal Subjects"
label var Z_ScoreFocal_T5 "Focal Subjects" 
drop if GradeID_T2==. & GradeID_T5==.
mmerge SchoolID GradeID_T2 using "$base_out/ConsolidatedYr34/AverageTeacherUnderstanding_T2.dta", ukeep(FracCorrectCore)
drop if _merge==2
drop _merge
mmerge SchoolID GradeID_T5 using "$base_out/ConsolidatedYr34/AverageTeacherUnderstanding_T5.dta", ukeep(FracCorrectCore)
drop if _merge==2
drop _merge
		
keep $treatmentlist LagGrade LagseenUwezoTests MissingseenUwezoTests LagpreSchoolYN MissingpreSchoolYN LagGender MissingGender LagAge MissingAge ///
IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement MissingIndexKowledge IndexKowledge ///
LagZ_kiswahili LagZ_hisabati LagZ_kiingereza MissingZ_kiswahili MissingZ_hisabati MissingZ_kiingereza $schoolcontrol GradeID_*  ///
SchoolID DistrictID StrataScore treatarm Z_hisabati* Z_kiswahili* Z_kiingereza* upid  FracCorrectCore*
drop if upid==""
drop *_T0
drop *_T0_G
*drop *_T1
drop *_T3
drop *_T4
drop *_T6
	
sum FracCorrectCore_T2, d
gen AboveMedian_T2=(FracCorrectCore_T2>r(p50)) if !missing(FracCorrectCore_T2)
sum FracCorrectCore_T5, d
gen AboveMedian_T5=(FracCorrectCore_T5>r(p50)) if !missing(FracCorrectCore_T5)


reshape long Z_hisabati@ Z_kiswahili@ Z_kiingereza@  GradeID@ ///
AboveMedian@ FracCorrectCore@, i(upid) j(time) string

replace Z_kiingereza=. if time=="_T5" & GradeID<=2
encode time, gen(time2)
drop if time!="_T2" & time !="_T5"

*replace AboveMedian=0 if TreatmentLevels==0 & TreatmentGains==0
*replace FracCorrectCore=0 if TreatmentLevels==0 & TreatmentGains==0

gen TreatmentLevelsAbove=(TreatmentLevels==1 & AboveMedian==1)
gen TreatmentLevelsBelow=(TreatmentLevels==1 & AboveMedian==0)
gen TreatmentLevelsNoInfo=(TreatmentLevels==1 & AboveMedian==.)

label var TreatmentLevelsAbove "Levels (high-understanding)"
label var TreatmentLevelsBelow "Levels (low-understanding)"
label var TreatmentLevelsNoInfo "Levels (no info on understanding)"

gen TreatmentGainsAbove=(TreatmentGains==1 & AboveMedian==1)
gen TreatmentGainsBelow=(TreatmentGains==1 & AboveMedian==0)
gen TreatmentGainsNoInfo=(TreatmentGains==1 & AboveMedian==.)

label var TreatmentGainsAbove "P4Pctile (high-understanding)"
label var TreatmentGainsBelow "P4Pctile (low-understanding)"
label var TreatmentGainsNoInfo "P4Pctile (no info on understanding)"

label var Z_kiswahili Swahili
label var Z_hisabati Math
label var Z_kiingereza English

gen Info=AboveMedian
replace Info=2 if AboveMedian==. & (TreatmentGains==1 | TreatmentLevels==1)

reghdfe Z_hisabati TreatmentLevelsAbove TreatmentLevelsBelow TreatmentLevelsNoInfo TreatmentGainsAbove TreatmentGainsBelow TreatmentGainsNoInfo   i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2)
reghdfe Z_hisabati c.TreatmentLevels##Info  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2)

eststo clear
foreach var in  hisabati kiswahili  kiingereza  {
		qui eststo:  reghdfe Z_`var' TreatmentLevelsAbove TreatmentLevelsBelow TreatmentLevelsNoInfo TreatmentGainsAbove TreatmentGainsBelow TreatmentGainsNoInfo   i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2)
		estadd ysumm
		test (_b[TreatmentLevelsAbove] - _b[TreatmentLevelsBelow]=0)
		estadd scalar p_levels=r(p)
		estadd scalar suma_levels=_b[TreatmentLevelsAbove] - _b[TreatmentLevelsBelow]
		test (_b[TreatmentGainsAbove] - _b[TreatmentGainsBelow]=0)
		estadd scalar p_gains=r(p)
		estadd scalar suma_gains=_b[TreatmentGainsAbove] - _b[TreatmentGainsBelow]
		
		test (_b[TreatmentGainsAbove] - _b[TreatmentLevelsAbove]=0)
		estadd scalar p_above=r(p)
		estadd scalar suma_above=_b[TreatmentGainsAbove] - _b[TreatmentLevelsAbove]
		test (_b[TreatmentGainsBelow] - _b[TreatmentLevelsBelow]=0)
		estadd scalar p_below=r(p)
		estadd scalar suma_below=_b[TreatmentGainsBelow] - _b[TreatmentLevelsBelow]


}
esttab  using "$latexcodes/Hetero_Teacher_Understanding.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  keep(*Above *Below) stats(N suma_levels p_levels suma_gains p_gains suma_above p_above suma_below p_below, ///
labels("N. of obs." "Levels:High-Low" "p-value (Levels:High-Low=0)" ///
"P4Pctile:High-Low" "p-value (P4Pctile:High-Low=0)" "P4Pctile:High-Levels:High" "p-value (P4Pctile:High-Levels:High=0)" ///
"P4Pctile:Low-Levels:Low" "p-value (P4Pctile:Low-Levels:Low=0)" ) fmt(%9.0fc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc)) ///
fragment collabels(none) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")


foreach t in _T2 _T5 {
	eststo clear
	foreach var in  hisabati kiswahili kiingereza  {
			qui eststo:  reghdfe Z_`var' TreatmentLevelsAbove TreatmentLevelsBelow TreatmentLevelsNoInfo TreatmentGainsAbove TreatmentGainsBelow TreatmentGainsNoInfo   i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3 & time=="`t'", vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm

	}
	esttab  using "$latexcodes/Hetero_Teacher_Understanding_`t'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace  keep(*Above *Below) stats(N suma_levels p_levels suma_gains p_gains suma_above p_above suma_below p_below, ///
	labels("N. of obs." "Levels:High-Low" "p-value (Levels:High-Low=0)" ///
	"P4Pctile:High-Low" "p-value (P4Pctile:High-Low=0)" "High(P4Pctile)-High(Levels)" "p-value (High(P4Pctile)-High(Levels)=0)" ///
	"Low(P4Pctile)-Low(Levels)" "p-value (Low(P4Pctile)-Low(Levels)=0)" ) fmt(%9.0fc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc)) ///
	fragment collabels(none) ///
	nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")
}
