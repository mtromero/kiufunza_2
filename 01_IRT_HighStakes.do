
*********** ERNDLINE 2015***********
******* twaweza test **********8
*****************************
use "$basein/12 Intervention_KFII/1 Endline 2015/AllStudentsTested_2015.dta", clear
*use "$basein/12 Intervention_KFII/1 Endline 2015/AllStudentsTested_2015.dta", clear
drop  studentid_merge set_merge stream_merge gender_merge FifthID updatedIDLength IDLength stream_missing2 name_numb pos_space 

rename districtid DistrictID
rename grade Grade
rename stream Stream
rename tested2015 TestedEL2015
replace TestedEL2015=0 if TestedEL2015==.
keep DistrictID SchoolID Grade Stream studentid gender TestedEL2015 grd1_kis_a_si_1-grd3_his_c_g_3 treatarm2 heldback set
drop *his_b_* *his_c_* *kis_b_* *kis_c_* *eng_b_* *eng_c_*
drop if TestedEL2015!=1
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge==1
drop _merge

foreach var of varlist grd1*{
	replace `var'=. if Grade!=1 | TestedEL2015==0
	replace `var'=0 if `var'==. & Grade==1 & TestedEL2015==1
}

foreach var of varlist grd2*{
	replace `var'=. if Grade!=2 | TestedEL2015==0
	replace `var'=0 if `var'==. & Grade==2 & TestedEL2015==1
}

foreach var of varlist grd3*{
	replace `var'=. if Grade!=3 | TestedEL2015==0
	replace `var'=0 if `var'==. & Grade==3 & TestedEL2015==1
}


*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in si ma se{
	 forvalues i=3/6{
		replace grd1_kis_a_`skill'_`i'=0 if grd1_kis_a_`skill'_1==0 & grd1_kis_a_`skill'_2==0 & Grade==1 & TestedEL2015==1
	}
}

foreach skill in l w se{
	forvalues i=3/6{
		replace grd1_eng_a_`skill'_`i'=0 if grd1_eng_a_`skill'_1==0 & grd1_eng_a_`skill'_2==0 & Grade==1 & TestedEL2015==1
	}
}

foreach skill in id uta bwa j t{
	replace grd1_his_a_`skill'_3=0 if grd1_his_a_`skill'_1==0 & grd1_his_a_`skill'_2==0 & Grade==1 & TestedEL2015==1
} 



*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in ma se{
	 forvalues i=3/6{
		replace grd2_kis_a_`skill'_`i'=0 if grd2_kis_a_`skill'_1==0 & grd2_kis_a_`skill'_2==0 & Grade==2 & TestedEL2015==1
	} 
}

foreach skill in w se{
	forvalues i=3/6{
		replace grd2_eng_a_`skill'_`i'=0 if grd2_eng_a_`skill'_1==0 & grd2_eng_a_`skill'_2==0 & Grade==2 & TestedEL2015==1
	}
}

foreach skill in  bwa j t z{
	replace grd2_his_a_`skill'_3=0 if grd2_his_a_`skill'_1==0 & grd2_his_a_`skill'_2==0 & Grade==2 & TestedEL2015==1
} 




*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_kis_a_m_1=0 if grd3_kis_a_h==0  & Grade==3 & TestedEL2015==1
replace grd3_kis_a_m_2=0 if grd3_kis_a_h==0  & Grade==3 & TestedEL2015==1


*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_eng_a_c_1=0 if grd3_eng_a_s==0 & Grade==3 & TestedEL2015==1
replace grd3_eng_a_c_2=0 if grd3_eng_a_s==0 & Grade==3 & TestedEL2015==1

*First data cleaning process for math is as before...
foreach skill in  j t z g{
	replace grd3_his_a_`skill'_3=0 if grd3_his_a_`skill'_1==0 & grd3_his_a_`skill'_2==0 & Grade==3 & TestedEL2015==1
} 

drop grd2_eng_a_se_5 /*March 3rd, 2017: for some reason this appears only for gains students... WTF?*/


foreach var in grd2_kis_a_a grd2_eng_a_p grd3_kis_a_h grd3_eng_a_s{
	tab `var', gen(`var'_)
	replace  `var'_3=1 if  `var'_4==1
	replace  `var'_2=1 if  `var'_3==1
	replace  `var'_1=1 if  `var'_2==1
	drop `var'_1
	drop  `var'
}


foreach subject in kis  his{
	forvalues val=1/3{
		irt (1pl grd`val'_`subject'_*),  difficult  intpoints(4) iterate(100)
		predict IRT_`subject'_`val' if e(sample), latent 
		replace IRT_`subject'_`val'=. if Grade!=`val'
		irtgraph tif, se
		graph export "$graphs/TIF_`subject'_`val'_T3.pdf", replace
		qui sum IRT_`subject'_`val' if Grade==`val' & treatarm2==3
		qui replace IRT_`subject'_`val' = (IRT_`subject'_`val' -r(mean))/r(sd) if Grade==`val'
	}
	egen IRT_`subject'=rowtotal(IRT_`subject'_*), missing
	qui sum IRT_`subject' if treatarm2==3
	qui replace IRT_`subject' = (IRT_`subject' -r(mean))/r(sd) 
	drop IRT_`subject'_*
}

foreach subject in  eng {
	forvalues val=1/3{
		irt (1pl grd`val'_`subject'_*), difficult  intpoints(15) iterate(100)
		predict IRT_`subject'_`val' if e(sample), latent 
		replace IRT_`subject'_`val'=. if Grade!=`val'
		irtgraph tif, se
		graph export "$graphs/TIF_`subject'_`val'_T3.pdf", replace
		qui sum IRT_`subject'_`val' if Grade==`val' & treatarm2==3
		qui replace IRT_`subject'_`val' = (IRT_`subject'_`val' -r(mean))/r(sd) if Grade==`val'
	}
	egen IRT_`subject'=rowtotal(IRT_`subject'_*), missing
	qui sum IRT_`subject' if treatarm2==3
	qui replace IRT_`subject' = (IRT_`subject' -r(mean))/r(sd) 
	drop IRT_`subject'_*
}


forvalues val=1/3{
		irt (1pl grd`val'_his_* grd`val'_kis_*),  difficult  intpoints(4)  iterate(100)
		predict IRT_ScoreKisawMath_`val' if e(sample), latent 
		replace IRT_ScoreKisawMath_`val'=. if Grade!=`val'
		irtgraph tif, se
		graph export "$graphs/TIF_IRT_ScoreKisawMath_`val'_T3.pdf", replace
		qui sum IRT_ScoreKisawMath_`val' if Grade==`val' & treatarm2==3
		qui replace IRT_ScoreKisawMath_`val' = (IRT_ScoreKisawMath_`val' -r(mean))/r(sd) if Grade==`val'
}
	egen IRT_ScoreKisawMath=rowtotal(IRT_ScoreKisawMath_*), missing
	qui sum IRT_ScoreKisawMath if treatarm2==3
	qui replace IRT_ScoreKisawMath = (IRT_ScoreKisawMath -r(mean))/r(sd)
	drop IRT_ScoreKisawMath_*

keep IRT*  DistrictID SchoolID studentid  StrataScore treatment2 treatarm2 treatment treatarm Grade
rename * =_T3
foreach var in DistrictID SchoolID studentid  StrataScore treatment2 treatarm2 treatment treatarm{
	rename `var'_T3 `var'
}
save "$base_out/11 Endline 2015/IRT_T3.dta", replace





use "$base_out/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_Mashindano_CLEAN.dta", clear
append using "$base_out/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_StadiandControl_CLEAN.dta"
keep studentid districtid schoolid grade gender grd1* grd2* grd3*  newstudentEL2016  testedEL2016  set

rename schoolid SchoolID
rename districtid DistrictID
rename grade Grade

rename testedEL2016 TestedEL2016
replace TestedEL2016=0 if TestedEL2016==.
drop *his_b_* *his_c_* *kis_b_* *kis_c_* *eng_b_* *eng_c_*

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge==1
drop _merge


foreach var of varlist grd1*{
replace `var'=. if Grade!=1 | TestedEL2016==0
replace `var'=0 if `var'==. & Grade==1 & TestedEL2016==1
}

foreach var of varlist grd2*{
replace `var'=. if Grade!=2 | TestedEL2016==0
replace `var'=0 if `var'==. & Grade==2 & TestedEL2016==1
}

foreach var of varlist grd3*{
replace `var'=. if Grade!=3 | TestedEL2016==0
replace `var'=0 if `var'==. & Grade==3 & TestedEL2016==1
}
*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in si ma se{
	 forvalues i=3/6{
		replace grd1_kis_a_`skill'_`i'=0 if grd1_kis_a_`skill'_1==0 & grd1_kis_a_`skill'_2==0 & Grade==1
	}
}

foreach skill in id uta bwa j t{
	replace grd1_his_a_`skill'_3=0 if grd1_his_a_`skill'_1==0 & grd1_his_a_`skill'_2==0 & Grade==1
} 

replace grd2_kis_a_h=0 if grd2_kis_a_h==. & Grade==2

*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd2_kis_a_m_1=0 if grd2_kis_a_h==0 & Grade==2
replace grd2_kis_a_m_2=0 if grd2_kis_a_h==0 & Grade==2

*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in ma se{
	 forvalues i=3/6{
		replace grd2_kis_a_`skill'_`i'=0 if grd2_kis_a_`skill'_1==0 & grd2_kis_a_`skill'_2==0 & Grade==2
	}
}
foreach skill in  bwa j t{
	replace grd2_his_a_`skill'_3=0 if grd2_his_a_`skill'_1==0 & grd2_his_a_`skill'_2==0 & Grade==2
} 


replace grd3_kis_a_h=0 if grd3_kis_a_h==. & Grade==3
*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_kis_a_m_1=0 if grd3_kis_a_h==0 & Grade==3
replace grd3_kis_a_m_2=0 if grd3_kis_a_h==0 & Grade==3


*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_eng_a_c_1=0 if grd3_eng_a_s==0 & Grade==3
replace grd3_eng_a_c_2=0 if grd3_eng_a_s==0 & Grade==3

*First data cleaning process for math is as before...
foreach skill in  j t z g{
	replace grd3_his_a_`skill'_3=0 if grd3_his_a_`skill'_1==0 & grd3_his_a_`skill'_2==0 & Grade==3

} 


foreach var of varlist  grd1_kis_a_se_*   grd2_kis_a_se_* grd2_kis_a_h   grd3_kis_a_h   grd3_eng_a_s{
	tab `var', gen(`var'_)
	replace  `var'_3=1 if  `var'_4==1
	replace  `var'_2=1 if  `var'_3==1
	replace  `var'_1=1 if  `var'_2==1
	drop `var'_1
	drop  `var'
}
drop if set>10

foreach subject in kis his{
	forvalues val=1/3{
		irt (1pl grd`val'_`subject'_*), difficult  intpoints(4)  iterate(100)
		predict IRT_`subject'_`val' if e(sample), latent 
		replace IRT_`subject'_`val'=. if Grade!=`val'
		irtgraph tif, se
		graph export "$graphs/TIF_`subject'_`val'_T6.pdf", replace
		qui sum IRT_`subject'_`val' if Grade==`val' & treatarm2==3
		qui replace IRT_`subject'_`val' = (IRT_`subject'_`val' -r(mean))/r(sd) if Grade==`val'
	}
	egen IRT_`subject'=rowtotal(IRT_`subject'_*), missing
	qui sum IRT_`subject' if treatarm2==3
	qui replace IRT_`subject' = (IRT_`subject' -r(mean))/r(sd)
	drop IRT_`subject'_*
}


foreach subject in eng {
		irt (1pl grd3_`subject'_*), difficult  intpoints(15)  iterate(100)
		predict IRT_`subject' if e(sample), latent 
		replace IRT_`subject'=. if Grade!=3
		irtgraph tif, se
		graph export "$graphs/TIF_`subject'_`val'_T6.pdf", replace
		qui sum IRT_`subject' if treatarm2==3
		qui replace IRT_`subject' = (IRT_`subject' -r(mean))/r(sd)

}

forvalues val=1/3{
		irt (1pl grd`val'_his_* grd`val'_kis_*), difficult  intpoints(4)  iterate(100)
		predict IRT_ScoreKisawMath_`val' if e(sample), latent 
		replace IRT_ScoreKisawMath_`val'=. if Grade!=`val'
		irtgraph tif, se
		graph export "$graphs/TIF_IRT_ScoreKisawMath_`val'_T6.pdf", replace
		qui sum IRT_ScoreKisawMath_`val' if Grade==`val' & treatarm2==3
		qui replace IRT_ScoreKisawMath_`val' = (IRT_ScoreKisawMath_`val' -r(mean))/r(sd) if Grade==`val'
}
	egen IRT_ScoreKisawMath=rowtotal(IRT_ScoreKisawMath_*), missing
	qui sum IRT_ScoreKisawMath if treatarm2==3
	qui replace IRT_ScoreKisawMath = (IRT_ScoreKisawMath -r(mean))/r(sd)
	drop IRT_ScoreKisawMath_*



keep IRT*  DistrictID SchoolID studentid  StrataScore treatment2 treatarm2 treatment treatarm Grade
rename * =_T6
foreach var in DistrictID SchoolID studentid  StrataScore treatment2 treatarm2 treatment treatarm{
	rename `var'_T6 `var'
}

save "$base_out/11 Endline 2015/IRT_T6.dta", replace