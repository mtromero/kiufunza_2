**********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
fvset base default treatarm 
global treatmentlist TreatmentLevels TreatmentGains
gen TreatmentLevels2=TreatmentLevels
gen TreatmentGains2=TreatmentGains
global treatmentlist2 TreatmentLevels2 TreatmentGains2
label var TreatmentLevels "Levels (\$\beta_1\$)"
label var TreatmentGains "Gains (\$\beta_2\$)"
keep ${schoolcontrol} *T3 *T6  SchoolID DistrictID StrataScore treat* Treat*
keep ${schoolcontrol} *Pass* Grade* SchoolID DistrictID StrataScore treat* Treat*


foreach var in Kis_Silabi_Pass_T3 Kis_Maneno_Pass_T3 Kis_Sentenci_Pass_T3 Kis_Aya_Pass_T3 Kis_Story_Pass_T3 Kis_Comp_Pass_T3  ///
			   Eng_Letter_Pass_T3 Eng_Word_Pass_T3 Eng_Sentences_Pass_T3 Eng_Paragraph_Pass_T3 Eng_Story_Pass_T3 Eng_Comp_Pass_T3 ///
			   Math_id_Pass_T3 Math_uta_Pass_T3 Math_bwa_Pass_T3 Math_j_Pass_T3 Math_t_Pass_T3 Math_z_Pass_T3 Math_g_Pass_T3{
	qui reg `var' i.Grade_T3 ${schoolcontrol} DistrictID##StrataScore##treatarm if treatment2=="Control", vce(cluster SchoolID)
	predict `var'_Pred, xb
	replace `var'_Pred=1 if `var'_Pred>1 & !missing(`var'_Pred)
	replace `var'_Pred=0 if `var'_Pred<1 & !missing(`var'_Pred)
	forvalues grd=1/3{
		qui sum `var' if Grade_T3==`grd'
		if r(N)==0{
			replace `var'_Pred=. if Grade_T3==`grd'
		}
	}
	
}

foreach var in Kis_Silabi_Pass_T6 Kis_Maneno_Pass_T6 Kis_Sentenci_Pass_T6 Kis_Aya_Pass_T6 Kis_Story_Pass_T6 Kis_Comp_Pass_T6  ///
			   Eng_Story_Pass_T6 Eng_Comp_Pass_T6 ///
			   Math_id_Pass_T6 Math_uta_Pass_T6 Math_bwa_Pass_T6 Math_j_Pass_T6 Math_t_Pass_T6 Math_z_Pass_T6 Math_g_Pass_T6{
	qui reg `var' i.Grade_T6 ${schoolcontrol} DistrictID##StrataScore##treatarm if treatment2=="Control", vce(cluster SchoolID)
	predict `var'_Pred, xb
	replace `var'_Pred=1 if `var'_Pred>1 & !missing(`var'_Pred)
	replace `var'_Pred=0 if `var'_Pred<1 & !missing(`var'_Pred)
	forvalues grd=1/3{
		qui sum `var' if Grade_T3==`grd'
		if r(N)==0{
			replace `var'_Pred=. if Grade_T3==`grd'
		}
	}
	
}




foreach time in T3 T6{
	preserve
	drop if Grade_`time'==.
	collapse Kis_*_`time' Eng_*_Pass_`time' Math_*_Pass_`time' Kis_*_`time'_Pred Eng_*_Pass_`time'_Pred Math_*_Pass_`time'_Pred, by( treatment2 treatarm2 Grade_`time')
	foreach var of varlist Kis_*_`time' Eng_*_Pass_`time' Math_*_Pass_`time'{
		gen Diff_`var'=(`var'_Pred)/`var'
		*replace Diff_`var'=1 if Diff_`var'>1 & !missing(Diff_`var')
	}



	drop if treatment2=="Control"

	egen Kis=rowmean(Diff_Kis*)
	egen Eng=rowmean(Diff_Eng*)
	egen Math=rowmean(Diff_Math*)

	collapse (mean)  Kis Eng Math, by(treatment2 treatarm2)

	foreach subject in Kis Eng Math{
		sum `subject' if treatment2=="Levels"
		scalar define tempm=r(mean)
		local tempm=round(100*tempm)
		file open newfile using "$latexcodes/WastedProp_`subject'_`time'_Levels.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		sum `subject' if treatment2=="Gains"
		scalar define tempm=r(mean)
		local tempm=round(100*tempm)
		file open newfile using "$latexcodes/WastedProp_`subject'_`time'_Gains.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
	restore
}
