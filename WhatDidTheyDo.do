*** Produces means by treatment arm, differences with pair FE, and p-values
	capt prog drop my_ptest
	*! version 1.0.0  14aug2007  Ben Jann
	program my_ptest, eclass
	*clus(clus_var)
	syntax varlist [if] [in], by(varname) clus_id(varname numeric) strat_id(varlist numeric) [ * ] /// clus_id(clus_var)  

	marksample touse
	markout `touse' `by'
	tempname mu_1 mu_2 mu_3 se_1 se_2 se_3 d_p2
	capture drop TD*
	tab `by' , gen(TD)
	foreach var of local varlist {
	 reg `var'  TD1 TD2   `if', nocons vce(cluster `clus_id')
	 matrix A=e(b)
	 lincom (TD1-TD2)
	 sum `var' if TD2==1 & e(sample)==1
	 mat `mu_1' = nullmat(`mu_1'), r(mean)
	 mat `se_1' = nullmat(`se_1'), r(sd)/sqrt(r(N))
	 sum `var' if TD1==1 & e(sample)==1
	 mat `mu_2' = nullmat(`mu_2'),r(mean)
	 mat `se_2' = nullmat(`se_2'), r(sd)/sqrt(r(N))
	 
	 reghdfe `var'  TD1 TD2   `if',  vce(cluster `clus_id') absorb(i.`strat_id')
	 test (_b[TD1]- _b[TD2]== 0)
	 mat `d_p2'  = nullmat(`d_p2'),r(p)
	 matrix A=e(b)
	 lincom (TD1-TD2)
	 mat `mu_3' = nullmat(`mu_3'), A[1,2]-A[1,1]
	 mat `se_3' = nullmat(`se_3'), r(se)
	 
	}
	foreach mat in mu_1 mu_2 mu_3   se_1 se_2 se_3  d_p2 {
	 mat coln ``mat'' = `varlist'
	}
	 local cmd "my_ptest"
	foreach mat in mu_1 mu_2 mu_3   se_1 se_2 se_3  d_p2 {
	 eret mat `mat' = ``mat''
	}
	end


use "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10TFocGrdSub_nopii.dta", clear
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "P4Pctile"

foreach var of varlist earnbns20161- earnbns20169{
	replace `var'=0 if `var'==. & earnbns2016!=""
	replace `var'=1 if `var'>0 & earnbns2016!=""
}



label var earnbns20161 "Offer remedial sessions"
label var earnbns20162 "Offer tutoring sessions"
label var earnbns20163 "More interactive teaching"
label var earnbns20164 "Use new teaching tools"
label var earnbns20165 "More homework"
label var earnbns20166 "More practice tests"
label var earnbns20167 "Track students"
label var earnbns20168 "Create a happier/safer environment"
label var earnbns20169 "Facilitate peer to peer learning"
drop if TreatmentLevels==0 & TreatmentGains==0

eststo clear
eststo: xi: my_ptest earnbns20161- earnbns20169, by(TreatmentGains) clus_id(SchoolID) strat_id(DistrictID) /*controls(DistrictID StrataScore treatarm)*/
esttab using "$latexcodes/summaryWhatDidTheyDo.tex", label replace fragment nogaps nolines ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc) star pvalue(d_p2))" "se_1( fmt(%9.2fc) par) se_2( fmt(%9.2fc) par) se_3(fmt(%9.2fc) par)") ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")



