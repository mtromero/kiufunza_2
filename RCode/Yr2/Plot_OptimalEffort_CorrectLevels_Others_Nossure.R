rm(list=ls())
# ###Assumptions
# Cost=function(x) x^2
# #all with error terms normal N(0,1)
library(nleqslv) #to solve non linear equation systems
scale=1
if(Sys.info()["user"]=="Mauricio") setwd("C:/Users/Mauricio/Dropbox/KF_Draft/KFII")
if(Sys.info()["user"]=="MROMEROLO") setwd("C:/Users/MROMEROLO/Dropbox/KF_Draft/KFII")

NumTeachers=1000
ClassSize=1
seq_intiail=seq(-4,4,0.5)
Prize=NumTeachers*ClassSize*length(seq_intiail)
seq_integrate=seq(-4,4,0.001)


#ClassSize=(4-abs(seq_intiail)+0.1)*15
#First order codnition ignoring own effect on pass rate, but taking into account in equilibrium pass rate depends on effort
#Note that this is correct as it incorporates the effect of all students on the pass rate
first_order_levels=function(effort,gamma,Initial_Level,PI_Levels=rep(N*CK,length(T_Levels)),T_Levels,N=1000,CK=30) {
  if(length(PI_Levels)!=length(T_Levels)) stop("Length of payoffs is not the same as thresholds")
  if(length(effort)!=length(gamma)) stop("Length of effort is not the same as productivity")
  if(length(effort)!=length(Initial_Level)) stop("Length of effort is not the same as initial levels")
  
  vector_total=rep(0,length(effort))
  for(level in 1:length(T_Levels)){
    vector_total=vector_total+CK*gamma*dnorm(T_Levels[level]-Initial_Level-gamma*effort)*PI_Levels[level]/sum(N*CK*(1-pnorm(T_Levels[level]-Initial_Level-gamma*effort)))
  }
  return(vector_total-2*effort)
}

#The equilibrium effort for levels is:
effort_gamma_levels=function(gamma,Initial_Level,PI_Levels=rep(N*CK,length(T_Levels)),T_Levels,N=1000,CK=30) {
  effort_noslack=nleqslv(rep(0,length(Initial_Level)), first_order_levels,gamma=gamma,Initial_Level=Initial_Level,PI_Levels=PI_Levels,T_Levels=T_Levels,N=N,CK=CK)
  if(effort_noslack$termcd!=1) stop("fuck")
  return(effort_noslack$x)
}



first_order_levels_2=function(effort,Initial_Level_interes,gamma,Initial_Level,PI_Levels=rep(N*CK,length(T_Levels)),T_Levels,N=1000,CK=30,effort_others_eq,effort_other) {
  if(length(PI_Levels)!=length(T_Levels)) stop("Length of payoffs is not the same as thresholds")
  index=which(Initial_Level==Initial_Level_interes)
  effort_others_eq[index]=effort_other
  vector_total=0
  for(level in 1:length(T_Levels)){
    vector_total=vector_total+CK*gamma[index]*dnorm(T_Levels[level]-Initial_Level[index]-effort)*PI_Levels[level]/sum(N*CK*(1-pnorm(T_Levels[level]-Initial_Level-gamma*effort_others_eq)))
  }
  res=vector_total-2*effort
  return(res)
}



T_Levels=c(0)
PI_Levels=rep(Prize/length(T_Levels),length(T_Levels))
effort_others=effort_gamma_levels(gamma=rep(1,length(seq_intiail)),Initial_Level=seq_intiail,PI_Levels=PI_Levels,T_Levels=T_Levels,N=NumTeachers,CK=ClassSize)

fun_plot=function(x) uniroot(first_order_levels_2, c(0,1),Initial_Level_interes=0,gamma=rep(1,length(seq_intiail)),Initial_Level=seq_intiail,PI_Levels=PI_Levels,T_Levels=T_Levels,N=NumTeachers,CK=ClassSize,effort_others_eq=effort_others,effort_other=x)$root
pdf("LaTeX/figures/BestResponse.pdf")
plot(seq(0,1,0.01),sapply(seq(0,1,0.01),fun_plot),ylim=c(0,1),type="l",
     xlab="Other's effort",ylab="Best response (effort)",cex.lab=1.5,cex.axis=1.5,
     lwd=3,bty="n")
abline(a=0,b=1,col=2,lwd=3)
dev.off()


for(i in seq(0.5,4,0.5)){
  T_Levels=c(-i,i)
  PI_Levels=rep(Prize/length(T_Levels),length(T_Levels))
  effort_others=effort_gamma_levels(gamma=rep(1,length(seq_intiail)),Initial_Level=seq_intiail,PI_Levels=PI_Levels,T_Levels=T_Levels,N=NumTeachers,CK=ClassSize)
  
  fun_plot=function(x) uniroot(first_order_levels_2, c(0,1),Initial_Level_interes=0,gamma=rep(1,length(seq_intiail)),Initial_Level=seq_intiail,PI_Levels=PI_Levels,T_Levels=T_Levels,N=NumTeachers,CK=ClassSize,effort_others_eq=effort_others,effort_other=x)$root
  lines(seq(0,1,0.01),sapply(seq(0,1,0.01),fun_plot),ylim=c(0,1),type="l",lwd=2,col=i*2)
  abline(a=0,b=1,col=2,lwd=2)
}


T_Levels=c(0)
PI_Levels=rep(Prize/length(T_Levels),length(T_Levels))
effort_others=effort_gamma_levels(gamma=rep(1,length(seq_intiail)),Initial_Level=seq_intiail,PI_Levels=PI_Levels,T_Levels=T_Levels,N=NumTeachers,CK=ClassSize)

fun_plot=function(x) uniroot(first_order_levels_2, c(0,1),Initial_Level_interes=2,gamma=rep(1,length(seq_intiail)),Initial_Level=seq_intiail,PI_Levels=PI_Levels,T_Levels=T_Levels,N=NumTeachers,CK=ClassSize,effort_others_eq=effort_others,effort_other=x)$root
plot(seq(0,1,0.01),sapply(seq(0,1,0.01),fun_plot),ylim=c(0,1),type="l",lwd=2)
abline(a=0,b=1,col=2,lwd=2)

for(i in seq(0.5,4,0.5)){
  T_Levels=c(-i,i)
  PI_Levels=rep(Prize/length(T_Levels),length(T_Levels))
  effort_others=effort_gamma_levels(gamma=rep(1,length(seq_intiail)),Initial_Level=seq_intiail,PI_Levels=PI_Levels,T_Levels=T_Levels,N=NumTeachers,CK=ClassSize)
  
  fun_plot=function(x) uniroot(first_order_levels_2, c(0,1),Initial_Level_interes=2,gamma=rep(1,length(seq_intiail)),Initial_Level=seq_intiail,PI_Levels=PI_Levels,T_Levels=T_Levels,N=NumTeachers,CK=ClassSize,effort_others_eq=effort_others,effort_other=x)$root
  lines(seq(0,1,0.01),sapply(seq(0,1,0.01),fun_plot),ylim=c(0,1),type="l",lwd=2,col=i*2)
  abline(a=0,b=1,col=2,lwd=2)
}

