**********************************************
**************Teacher Understanding ************************
************************************************
use "$base_out/ConsolidatedYr34/TUnderstanding.dta", clear
merge m:1 SchoolID using "$base_out/ConsolidatedYr34/School.dta",keepus($schoolcontrol)
drop _merge

gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "P4Pctile"

global TeacherGeneral PreferLevels FavorableBonusPayments knwtwa2 sgnpet trstwa 

preserve
reshape long PreferLevels FavorableBonusPayments knwtwa2 sgnpet trstwa NumTeachers, i(SchoolID) j(time) string
encode time, gen(time2)

label var PreferLevels "Single test"
label var FavorableBonusPayments "Bonus payments"
label var knwtwa2 "Know Twaweza"
label var sgnpet "Support Twaweza"
label var trstwa "Trust Twaweza"



eststo clear
foreach var in $TeacherGeneral {
eststo:  reghdfe `var' $treatmentlist time2##c.($schoolcontrol) [aw=NumTeachers], vce(cluster SchoolID)   a(time2##DistrictID##StrataScore##treatarm)
estadd ysumm
sum `var' if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
}  


esttab  using "$latexcodes/RegTeacher_Trust1.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace   ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\),\sym{***} \(p<0.01\) }")
restore


preserve
keep MoneyEvenifFail* NumTeachers* tcodhttpay* FracCorrect* FracCorrectCore* SchoolID DistrictID StrataScore treatarm treatment2  treatarm2 $treatmentlist NumTeachers*
reshape long MoneyEvenifFail NumTeachers tcodhttpay FracCorrect FracCorrectCore, i(SchoolID) j(time) string
encode time, gen(time2)
label var tcodhttpay "Trust payments"
label var FracCorrect "Understanding"
label var FracCorrectCore "Understanding (core)"
drop if treatarm2==3
label define time2 1 "2015" 2 "2016", replace
replace treatment2="P4Pctile" if treatment2=="Gains"

graph bar (mean) tcodhttpay  (mean) FracCorrect (mean) MoneyEvenifFail  [aw=NumTeachers], over(treatment2) over(time2) legend(order( 1 "Trust Payments" 2 "Quiz" 3 "Bonus=Pass"))
graph export "$graphs/Understanding.pdf", replace
restore

preserve
keep codort* codops* codoif* codopt* SchoolID DistrictID StrataScore treatarm treatment2  treatarm2 $treatmentlist  NumTeachers*
reshape long codort codops codoif codopt NumTeachers, i(SchoolID) j(time) string
encode time, gen(time2)
label var codort "Resources (taken away)"
label var codops "Share"
label var codoif "Interfered"
label var codopt "Parent Support"
drop if treatarm2==3
label define time2 1 "2015" 2 "2016", replace
replace treatment2="P4Pctile" if treatment2=="Gains"

graph bar (mean) codort  (mean) codops (mean) codoif (mean) codopt [aw=NumTeachers], over(treatment2) over(time2) legend(order( 1 "Less resources" 2 "Share" 3 "Interfered" 4 "Parent support"))
graph export "$graphs/Teacher_resources.pdf", replace
restore



use "$base_out/ConsolidatedYr34/TComprehension_TWA.dta", clear

reshape long Correct_@, i(SchoolID) j(Period) string
encode Period, gen(Period2)
label define Period2 2 "BL 2016", modify
label define Period2 1 "BL 2015", modify
label define Period2 3 "ML 2015", modify
label define Period2 4 "ML 2016", modify
label define Period2 5 "EL 2015", modify
label define Period2 6 "EL 2016", modify
drop if treatarm2==3
rename Correct_ Correct
replace treatment2="P4Pctile" if treatment2=="Gains"
graph bar (mean) Correct, over(treatment2) over(Period2) bar(1, color(cranberry)) bar(2, color(dknavy)) asyvars ytitle("% of correct answers")  graphregion(color(white))
graph export "$graphs/Teacherunderstanding.pdf", replace


**********************************************
**************Teacher Abseentisim ************************
************************************************
use "$base_out/ConsolidatedYr34/TAttendace.dta", clear
merge m:1 SchoolID using "$base_out/ConsolidatedYr34/School.dta",keepus($schoolcontrol)
drop _merge

gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "P4Pctile"
label var OffTask_T2 "% off task"
label var OffTask_T5 "% off task"
global TeacherAbs atttch attcls 
global treatmentlist TreatmentLevels TreatmentGains


label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

eststo clear
foreach time in T2 T5{
	foreach var in $TeacherAbs {
		eststo:  reghdfe `var'_`time' $treatmentlist $schoolcontrol , vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm)
		estadd ysumm
		sum `var'_`time' if treatment2=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	}  
}


esttab  using "$latexcodes/RegTeacherAbs.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment nogaps nolines ///
nomtitle nonumbers noobs nodep  collabels(none) substitute(\_ _)  ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

keep atttch* attcls* $treatmentlist $schoolcontrol SchoolID DistrictID StrataScore treatarm treatment2
reshape long atttch_@ attcls_@, i(SchoolID) j(time) string
encode time, gen(time2)



eststo clear
	foreach var in $TeacherAbs {
		eststo:  reghdfe `var'_ $treatmentlist c.(${schoolcontrol})#time2 , vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm##time2)
		estadd ysumm
		sum `var'_`time' if treatment2=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	}  


esttab  using "$latexcodes/RegTeacherAbs_Pooled.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment nogaps nolines ///
nomtitle nonumbers noobs nodep  collabels(none) substitute(\_ _)  ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

eststo clear
	foreach var in $TeacherAbs {
		eststo:  reghdfe `var'_ $treatmentlist c.(${schoolcontrol}) if time2==1 , vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm)
		estadd ysumm
		sum `var'_`time' if treatment2=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	}  


esttab  using "$latexcodes/RegTeacherAbs_T2.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment nogaps nolines ///
nomtitle nonumbers noobs nodep  collabels(none) substitute(\_ _)  ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")


eststo clear
	foreach var in $TeacherAbs {
		eststo:  reghdfe `var'_ $treatmentlist c.(${schoolcontrol}) if time2==2 , vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm)
		estadd ysumm
		sum `var'_`time' if treatment2=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	}  


esttab  using "$latexcodes/RegTeacherAbs_T5.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment nogaps nolines ///
nomtitle nonumbers noobs nodep  collabels(none) substitute(\_ _)  ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

gen AnyTreat=TreatmentLevels+TreatmentGains
reghdfe atttch AnyTreat c.($schoolcontrol) if  time2==2, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
reghdfe attcls AnyTreat c.($schoolcontrol) if  time2==2, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)




**********************************************
**************Teacher Abseentisim FOCAL GRADE ************************
************************************************
use "$base_out/ConsolidatedYr34/TAttendace_Focal.dta", clear
merge m:1 SchoolID using "$base_out/ConsolidatedYr34/School.dta",keepus($schoolcontrol)
drop _merge

gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "P4Pctile"
label var OffTask_T2 "% off task"
label var OffTask_T5 "% off task"
global TeacherAbs atttch attcls 
global treatmentlist TreatmentLevels TreatmentGains


label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

eststo clear
foreach time in T2 T5{
	foreach var in $TeacherAbs {
		eststo:  reghdfe `var'_`time' $treatmentlist $schoolcontrol , vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm)
		estadd ysumm
		sum `var'_`time' if treatment2=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	}  
}


esttab  using "$latexcodes/RegTeacherAbs_Focal.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment nogaps nolines ///
nomtitle nonumbers noobs nodep  collabels(none) substitute(\_ _)  ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

keep atttch* attcls* $treatmentlist $schoolcontrol SchoolID DistrictID StrataScore treatarm treatment2
reshape long atttch_@ attcls_@, i(SchoolID) j(time) string
encode time, gen(time2)



eststo clear
	foreach var in $TeacherAbs {
		eststo:  reghdfe `var'_ $treatmentlist c.(${schoolcontrol})#time2 , vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm##time2)
		estadd ysumm
		sum `var'_`time' if treatment2=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	}  


esttab  using "$latexcodes/RegTeacherAbs_Pooled_Focal.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment nogaps nolines ///
nomtitle nonumbers noobs nodep  collabels(none) substitute(\_ _)  ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

eststo clear
	foreach var in $TeacherAbs {
		eststo:  reghdfe `var'_ $treatmentlist c.(${schoolcontrol}) if time2==1 , vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm)
		estadd ysumm
		sum `var'_`time' if treatment2=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	}  


esttab  using "$latexcodes/RegTeacherAbs_T2_Focal.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment nogaps nolines ///
nomtitle nonumbers noobs nodep  collabels(none) substitute(\_ _)  ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")


eststo clear
	foreach var in $TeacherAbs {
		eststo:  reghdfe `var'_ $treatmentlist c.(${schoolcontrol}) if time2==2 , vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm)
		estadd ysumm
		sum `var'_`time' if treatment2=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	}  


esttab  using "$latexcodes/RegTeacherAbs_T5_Focal.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment nogaps nolines ///
nomtitle nonumbers noobs nodep  collabels(none) substitute(\_ _)  ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

gen AnyTreat=TreatmentLevels+TreatmentGains
reghdfe atttch AnyTreat c.($schoolcontrol) if  time2==2, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
reghdfe attcls AnyTreat c.($schoolcontrol) if  time2==2, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)

**********************************************
**************Student reports on teacher behavior
************************************************
use "$base_out/ConsolidatedYr34/StudentSubject.dta", clear
merge m:1 SchoolID using "$base_out/ConsolidatedYr34/School.dta",keepus($schoolcontrol)
drop _merge

drop knowname_T2 - presents_explain_T2
gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "P4Pctile"
keep SchoolID $schoolcontrol $treatmentlist teacherhelp* knowname* callname* harsh* teacherhit* teacherleave* missdays* presents* homework* havebook* booklarge* assignmentlength* assignmentgraded* bookfeedback1* bookfeedback2* bookfeedback3* bookfeedback4* GradeID* upid SubjectID DistrictID StrataScore treatment2 treatarm2 treatment treatarm
drop presents_explain_T5

pca knowname callname harsh teacherhit presents
predict indexBehavior, score
* callname teacherhit
global teacher_beh  teacherhelp homework  
label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

eststo clear
foreach time in T2 T5{
	foreach var in $teacher_beh{
		eststo: reghdfe `var'_`time' $treatmentlist c.(${schoolcontrol}) if SubjectID!=1, vce(cluster SchoolID) a(GradeID_`time'##SubjectID##DistrictID##StrataScore##treatarm)
		estadd ysumm
		sum `var'_`time' if treatment2=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		
		
		local tempm=string(abs(_b[TreatmentGains] - _b[TreatmentLevels]), "%9.2gc")
		file open newfile using "$latexcodes/`var'_`time'_coef_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		if r(p)<0.01 {
		di "peque"
		local tempm ="$<0.01$"
		file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		}
		if r(p)>0.01 {
		di "grande"
		local tempm=string(r(p), "%9.2gc")
		file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		}
		
		matrix tempm=e(b)
		local tempm=string(tempm[1,1], "%9.2gc")
		local tempm_effect=tempm[1,1]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_levels.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		local tempm=string(tempm[1,2], "%9.2gc")
		local tempm_effect=tempm[1,2]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_gains.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		
		test TreatmentLevels
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		test TreatmentGains
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		
	}
}

esttab  using "$latexcodes/RegBehavior_Student_NonEnglish_T2T5.tex", booktabs se ar2 label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment nogaps nolines ///
nomtitle nonumbers noobs nodep  collabels(none) substitute(\_ _)  ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\),\sym{***} \(p<0.01\) }")



reshape long teacherhelp@ knowname@ callname@ harsh@ teacherhit@ teacherleave@ missdays@ presents@ homework@ havebook@ booklarge@ assignmentlength@ assignmentgraded@ bookfeedback1@ bookfeedback2@ bookfeedback3@ bookfeedback4@ GradeID@, i(upid SubjectID) j(time) string
encode time, gen(time2)

foreach var in teacherhelp knowname callname harsh teacherhit teacherleave missdays presents homework havebook assignmentlength{
replace `var'=. if time=="_T5" & GradeID<3 & SubjectID==1
}

*pca knowname callname harsh teacherhit presents
*predict indexBehavior, score
*teacherhelp callname teacherhit
global teacher_beh  teacherhelp homework callname teacherhit
label var teacherhelp "Extra Help"
label var callname "Call by name"
label var teacherhit "Hit/Pinch"
label var missdays "Missed school"
label var homework "Homework"
label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

eststo clear
foreach var of varlist $teacher_beh{
	eststo: reghdfe `var' $treatmentlist time2##c.($schoolcontrol) if SubjectID!=1, vce(cluster SchoolID) a(time2##GradeID##SubjectID##DistrictID##StrataScore##treatarm)
	estadd ysumm
	sum `var' if treatment2=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	

}

esttab  using "$latexcodes/RegBehavior_Student_NonEnglish.tex", booktabs se ar2 label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment nogaps nolines ///
nomtitle nonumbers noobs nodep  collabels(none) substitute(\_ _)  ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\),\sym{***} \(p<0.01\) }")


global teacher_beh_short teacherhelp homework
eststo clear
foreach var of varlist $teacher_beh_short{
	eststo: reghdfe `var' $treatmentlist c.($schoolcontrol) if SubjectID!=1 & time2==1, vce(cluster SchoolID) a(GradeID##SubjectID##DistrictID##StrataScore##treatarm)
	estadd ysumm
	sum `var' if treatment2=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]	
}

esttab  using "$latexcodes/RegBehavior_Student_NonEnglish_T2.tex", booktabs se ar2 label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment nogaps nolines ///
nomtitle nonumbers noobs nodep  collabels(none) substitute(\_ _)  ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\),\sym{***} \(p<0.01\) }")


eststo clear
foreach var of varlist $teacher_beh{
	eststo: reghdfe `var' $treatmentlist c.($schoolcontrol) if SubjectID!=1 & time2==2, vce(cluster SchoolID) a(GradeID##SubjectID##DistrictID##StrataScore##treatarm)
	estadd ysumm
	sum `var' if treatment2=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	
			local tempm=string(abs(_b[TreatmentGains] - _b[TreatmentLevels]), "%9.2gc")
		file open newfile using "$latexcodes/`var'_`time'_coef_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		if r(p)<0.01 {
		di "peque"
		local tempm ="$<0.01$"
		file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		}
		if r(p)>0.01 {
		di "grande"
		local tempm=string(r(p), "%9.2gc")
		file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		}
		
		matrix tempm=e(b)
		local tempm=string(abs(100*tempm[1,1]), "%9.2gc")
		local tempm_effect=tempm[1,1]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_levels.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		local tempm=string(abs(100*tempm[1,2]), "%9.2gc")
		local tempm_effect=tempm[1,2]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_gains.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		
		test TreatmentLevels
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		test TreatmentGains
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
	
}

esttab  using "$latexcodes/RegBehavior_Student_NonEnglish_T5.tex", booktabs se ar2 label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment nogaps nolines ///
nomtitle nonumbers noobs nodep  collabels(none) substitute(\_ _)  ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\),\sym{***} \(p<0.01\) }")

gen AnyTreat=TreatmentLevels+TreatmentGains
reghdfe callname AnyTreat c.($schoolcontrol) if SubjectID!=1 & time2==2, vce(cluster SchoolID) a(GradeID##SubjectID##DistrictID##StrataScore##treatarm)
reghdfe teacherhit AnyTreat c.($schoolcontrol) if SubjectID!=1 & time2==2, vce(cluster SchoolID) a(GradeID##SubjectID##DistrictID##StrataScore##treatarm)
reghdfe teacherhelp AnyTreat c.($schoolcontrol) if SubjectID!=1 & time2==2, vce(cluster SchoolID) a(GradeID##SubjectID##DistrictID##StrataScore##treatarm)
reghdfe homework AnyTreat c.($schoolcontrol) if SubjectID!=1 & time2==2, vce(cluster SchoolID) a(GradeID##SubjectID##DistrictID##StrataScore##treatarm)


recode havebook (2=0)
*rescale appropiertly large books
replace assignmentlength=. if havebook==0 | havebook==.
replace assignmentgraded=. if havebook==0 | havebook==.
replace booklarge=. if havebook==0 | havebook==.
replace bookfeedback1=. if havebook==0 | havebook==.
replace bookfeedback2=. if havebook==0 | havebook==.
replace bookfeedback3=. if havebook==0 | havebook==.
replace bookfeedback4=. if havebook==0 | havebook==.

replace assignmentlength=0 if assignmentlength==. & havebook==1
replace assignmentlength=1.4*assignmentlength if booklarge==1
replace assignmentlength=0 if assignmentlength==. & havebook==1
gen assignment=(assignmentlength>0) if !missing(assignmentlength)
replace assignmentlength=. if assignment==0
replace assignmentgraded=. if assignment==0


replace bookfeedback1=0 if havebook==1 & bookfeedback1==.
replace bookfeedback2=0 if havebook==1 & bookfeedback2==.
replace bookfeedback3=0 if havebook==1 & bookfeedback3==.
replace bookfeedback4=0 if havebook==1 & bookfeedback4==.

replace bookfeedback1=. if assignment==0 | assignment==.
replace bookfeedback2=. if assignment==0 | assignment==.
replace bookfeedback3=. if assignment==0 | assignment==.
replace bookfeedback4=. if assignment==0 | assignment==.

eststo clear
foreach var of varlist havebook assignment assignmentlength assignmentgraded bookfeedback1 bookfeedback2 bookfeedback3 bookfeedback4{
	eststo: reghdfe `var' $treatmentlist c.($schoolcontrol) if SubjectID!=1 & time2==2, vce(cluster SchoolID) a(GradeID##SubjectID##DistrictID##StrataScore##treatarm)
	estadd ysumm
	sum `var' if treatment2=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	

}

esttab  using "$latexcodes/Reg_NotebookCheck.tex", booktabs se ar2 label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment nogaps nolines ///
nomtitle nonumbers noobs nodep  collabels(none) substitute(\_ _)  ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\),\sym{***} \(p<0.01\) }")


 

**************************************************
************** Teacher Observations **************
**************************************************
use "$base_out/ConsolidatedYr34/Obs.dta", clear
merge m:1 SchoolID using "$base_out/ConsolidatedYr34/School.dta",keepus($schoolcontrol)
drop _merge


gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "P4Pctile"
encode time, gen(time2)
drop if subject==4
label var UnObsOffTask  "Teacher off task"
label var UnObsStudentOffTask "Student off task"

replace UnObsTeaching=0 if UnObsOffTask==1
replace UnObsClassManagement=0 if UnObsOffTask==1
replace UnObsStudentOffTask=0 if UnObsOffTask==1
replace UnObsClassManagement=0 if UnObsTeaching==1

eststo clear
foreach var in UnObsTeaching UnObsClassManagement UnObsOffTask UnObsStudentOffTask {
		eststo:  reghdfe `var' $treatmentlist   time2##c.($schoolcontrol), vce(cluster SchoolID) a(grade##subject##time2##DistrictID##StrataScore##treatarm)
		estadd ysumm
		sum `var' if treatment2=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]


		
}  


label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

esttab  using "$latexcodes/RegUnObs.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment substitute(\_ _) ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Control mean" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\),\sym{***} \(p<0.01\) }")

eststo clear
foreach var in UnObsTeaching UnObsClassManagement UnObsOffTask UnObsStudentOffTask {
		eststo:  reghdfe `var' $treatmentlist   c.($schoolcontrol) if time2==1, vce(cluster SchoolID) a(grade##subject##DistrictID##StrataScore##treatarm)
		estadd ysumm
		sum `var' if treatment2=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		
}  


label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

esttab  using "$latexcodes/RegUnObs_T2.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment substitute(\_ _) ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Control mean" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\),\sym{***} \(p<0.01\) }")



eststo clear
foreach var in UnObsTeaching UnObsClassManagement UnObsOffTask UnObsStudentOffTask {
		eststo:  reghdfe `var' $treatmentlist   c.($schoolcontrol) if time2==2, vce(cluster SchoolID) a(grade##subject##DistrictID##StrataScore##treatarm)
		estadd ysumm
		sum `var' if treatment2=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		
				local tempm=string(100*abs(_b[TreatmentGains] - _b[TreatmentLevels]), "%9.2gc")
		file open newfile using "$latexcodes/`var'_`time'_coef_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		if r(p)<0.01 {
		di "peque"
		local tempm ="$<0.01$"
		file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		}
		if r(p)>0.01 {
		di "grande"
		local tempm=string(r(p), "%9.2gc")
		file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		}
		
		matrix tempm=e(b)
		local tempm=string(abs(100*tempm[1,1]), "%9.2gc")
		local tempm_effect=tempm[1,1]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_levels.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		local tempm=string(abs(100*tempm[1,2]), "%9.2gc")
		local tempm_effect=tempm[1,2]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_gains.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		
		test TreatmentLevels
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		test TreatmentGains
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
}  


label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

esttab  using "$latexcodes/RegUnObs_T5.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment substitute(\_ _) ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Control mean" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\),\sym{***} \(p<0.01\) }")

label var ClasroomIndex "Infrastructure"
label var AmbientIndex "Environment"
label var TeachingIndex "Teaching"
label var InputsIndex "Materials"


eststo clear
foreach var in  ClasroomIndex AmbientIndex TeachingIndex  InputsIndex {
eststo:  reghdfe `var' $treatmentlist   time2##c.($schoolcontrol) if time2==2, vce(cluster SchoolID) a(grade##subject##time2##DistrictID##StrataScore##treatarm)
estadd ysumm
sum `var' if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
}  

esttab  using "$latexcodes/RegObservationsIndex.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment substitute(\_ _) ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Control mean" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\),\sym{***} \(p<0.01\) }")






egen off_Task=rowtotal(inclass_noteaching_T5 noclass_noactivity_T5 noclass_activity_T5)
sum off_Task
eststo clear

label var off_Task "Off task"
label var teaching_T5 "Teaching"
label var grading_T5 "Grading"
foreach var in  teaching_T5 grading_T5  off_Task{
eststo:  reghdfe `var' $treatmentlist   time2##c.($schoolcontrol) if time2==2, vce(cluster SchoolID) a(grade##subject##time2##DistrictID##StrataScore##treatarm)
estadd ysumm
sum `var' if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
}  

esttab  using "$latexcodes/RegObservations_Time.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  fragment substitute(\_ _) ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Control mean" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\),\sym{***} \(p<0.01\) }")



