************************************************************
************************************************************
************************************************************
************************************************************
*This should really be the list of students I expect to test in 2016
use "$basein/12 Intervention_KFII/4 Endline 2016/StudentNameLists_06122016_nopii", clear 
bys studentid: gen cont=_n
drop if cont>=2
drop cont
rename SchoolID, lower
ds studentid districtid schoolid, not
merge 1:1 studentid using "$base_out/4 Intervention/TwaEL_2016/2016_GroupsStudent_Mash_Grade23.dta", keepus(schoolid KisLevel1- MathLevel2)
*Drop students not tested at EL last year 
*drop if TestedEL==0
*drop students who are in grade 4 last year
drop if grade2016==4
drop if _merge==2 /*this should be no one... */
gen Match_Original=_merge
drop _merge
merge m:1 schoolid using "$base_out/4 Intervention/TwaEL_2016/2016_GroupsStudent_Mash_Grade1.dta"
drop _merge
rename schoolid SchoolID
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2) update
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440

*136 is a split school
replace treatarm2=1 if SchoolID==136
replace treatment2="Levels" if SchoolID==136

foreach var of varlist LevelGrade0 KisLevel1 EngLevel1 MathLevel1 KisLevel2 EngLevel2 MathLevel2{
	replace `var'=. if treatment2=="Levels"
} 
/*
foreach var of varlist LevelGrade0{
	replace `var'=. if treatment2=="Gains" & grade2016!=1
	replace `var'=0 if treatment2=="Gains" & grade2016==1 & `var'==.
} 

foreach var of varlist KisLevel1 EngLevel1 MathLevel1{
	replace `var'=. if treatment2=="Gains" & grade2016!=2
	replace `var'=0 if treatment2=="Gains" & grade2016==2 & `var'==.
} 
foreach var of varlist KisLevel2 EngLevel2 MathLevel2{
	replace `var'=. if treatment2=="Gains" & grade2016!=3
	replace `var'=0 if treatment2=="Gains" & grade2016==3 & `var'==.
} 
*/


foreach archivo in "EL2016_Student_FinalRun_Mashindano_CLEAN" "EL2016_Student_FinalRun_StadiandControl_CLEAN"{
di "`archivo'"
merge 1:m studentid using "$base_out/4 Intervention/TwaEL_2016/`archivo'.dta", keepus(studentid schoolid grade) update
/*
replace studentid="" if studentid=="999"
replace studentid="" if studentid=="-999"
replace studentid="" if studentid=="99"
replace studentid="" if studentid=="-99"

replace studentid="" if studentid=="-  999"
replace studentid="" if studentid=="--BLANK--"
replace studentid="" if studentid=="-.999"
replace studentid="" if studentid=="-99.9"
replace studentid="" if studentid=="9.99"
replace studentid=string(_n) if studentid==""
*/
replace SchoolID=schoolid if _merge==2
drop _merge schoolid
duplicates drop studentid, force
}
replace grade2016=grade if grade2016==.
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2) update replace
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440

*136 is a split school
replace treatarm2=1 if SchoolID==136
replace treatment2="Levels" if SchoolID==136

drop if SchoolID==.
drop if treatment2=="" /*right now is school 1002*/
drop if grade2016==.
gen there=!missing(SchoolID)
collapse (count) Sutdents= there , by( treatment2 treatarm2 grade2016 SchoolID)
sort treatment2 treatarm2 grade2016 SchoolID
rename grade2016 Grade_T6
saveold "$base_out/4 Intervention/TwaEL_2016/CountTested_TWA_2016_Control.dta", replace

************************************************************
************************************************************
************************************************************
************************************************************

*This should really be the list of students I expect to test in 2015
use "$basein/12 Intervention_KFII/1 Endline 2015/1 Student Names List/2015_Student_Name_List_nopii", clear 
bys StuID_15: gen cont=_n
drop if cont==2
drop cont
ds StuID_15 DistrictID SchoolID, not
replace Grade15=Grade
merge 1:1 StuID_15 using "$base_out/Consolidated/TWA_StudentLevelsBatches.dta", keepus(SchoolID NoInfoBL TestedEL Grade KisLevel1- MathLevel2)
*Drop students not tested at EL last year + students who were in grade 3 last year
*drop if TestedEL==0
drop if Grade==3
drop if _merge==2 /*this should be no one... there is a school... whatever */
gen Match_Original=_merge
drop _merge
merge m:1 SchoolID using "$base_out/4 Intervention/TwaEL_2015/2015_GroupsStudent_Mash_Grade1.dta"
drop _merge
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2) update
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440


replace Grade15=Grade+1 if Grade15==.
drop if Grade15==.


foreach var of varlist LevelGrade0 KisLevel1 EngLevel1 MathLevel1 KisLevel2 EngLevel2 MathLevel2{
	replace `var'=. if treatment2=="Levels"
} 

foreach var of varlist LevelGrade0{
	replace `var'=. if treatment2=="Gains" & Grade15!=1
	replace `var'=0 if treatment2=="Gains" & Grade15==1 & `var'==.
} 

foreach var of varlist KisLevel1 EngLevel1 MathLevel1{
	replace `var'=. if treatment2=="Gains" & Grade15!=2
	replace `var'=0 if treatment2=="Gains" & Grade15==2 & `var'==.
} 
foreach var of varlist KisLevel2 EngLevel2 MathLevel2{
	replace `var'=. if treatment2=="Gains" & Grade15!=3
	replace `var'=0 if treatment2=="Gains" & Grade15==3 & `var'==.
} 

gen studentid=StuID_15
destring studentid, replace
foreach archivo in "EL2015_Student_FinalRun_Mashindano_CLEAN" "EL2015_Student_FinalRun_StadiandControl_CLEAN"{
di "`archivo'"
merge 1:m studentid using "$base_out/4 Intervention/TwaEL_2015/`archivo'.dta", keepus(studentid schoolid grade) update
replace studentid=. if studentid==999
replace studentid=. if studentid==-999
replace studentid=. if studentid==99
replace studentid=. if studentid==-99
replace studentid=_n if studentid==.
replace SchoolID=schoolid if _merge==2
drop _merge schoolid
duplicates drop studentid, force
}
replace Grade15=grade if Grade15==.
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2) update replace
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440

drop if SchoolID==.
drop if treatment2==""
drop if Grade15==.

gen there=!missing(SchoolID)
collapse (count) Sutdents= there , by( treatment2 treatarm2 Grade15 SchoolID)
sort treatment2 treatarm2 Grade15 SchoolID
rename Grade15 Grade_T3
saveold "$base_out/4 Intervention/TwaEL_2015/CountTested_TWA_2015_Control.dta", replace


*I actually think this is the right way
use "$basein/12 Intervention_KFII/1 Endline 2015/AllStudentsTested_2015.dta", clear
gen there=!missing(SchoolID)
collapse (count) Sutdents= there , by( treatment2 treatarm2 grade SchoolID)
rename grade Grade_T3
saveold "$base_out/4 Intervention/TwaEL_2015/CountTested_TWA_2015_Control.dta", replace

************************************************************
************************************************************
************************************************************
************************************************************


*********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
preserve
drop if Grade_T3==.
collapse (count)  TestTakes=Z_hisabati_T3, by(Grade_T3 SchoolID treatment treatarm DistrictID treatment2 treatarm2 StrataScore TreatmentLevels TreatmentGains)
merge 1:1 SchoolID Grade_T3 using "$base_out/4 Intervention/TwaEL_2015/CountTested_TWA_2015_Control.dta"
drop if _merge!=3
drop _merge
drop if TestTakes==0
replace TestTakes=TestTakes/Sutdents
replace TestTakes=1 if TestTakes>1 & !missing(TestTakes)
eststo clear
eststo test_takers_Y1: reghdfe TestTakes TreatmentLevels TreatmentGains, ab(Grade_T3 DistrictID##StrataScore##treatarm) 
estadd ysumm
sum TestTakes if TreatmentLevels==0 & TreatmentGains==0
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
matrix M=e(b)
local cutLevels_T3=M[1,1]
local cutGains_T3=M[1,2]


restore
preserve
drop if Grade_T6==.
collapse (count)  TestTakes=Z_hisabati_T6, by(Grade_T6 SchoolID treatment treatarm DistrictID treatment2 treatarm2 StrataScore TreatmentLevels TreatmentGains)
merge 1:1 SchoolID Grade_T6 using "$base_out/4 Intervention/TwaEL_2016/CountTested_TWA_2016_Control.dta"
drop if _merge!=3
drop _merge
drop if TestTakes==0
replace TestTakes=TestTakes/Sutdents
replace TestTakes=1 if TestTakes>1 & !missing(TestTakes)
eststo test_takers_Y2: reghdfe TestTakes TreatmentLevels TreatmentGains, ab(Grade_T6 DistrictID##StrataScore##treatarm) 
estadd ysumm
sum TestTakes if TreatmentLevels==0 & TreatmentGains==0
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
matrix M=e(b)
local cutLevels_T6=M[1,1]
local cutGains_T6=M[1,2]

label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"
esttab  using "$latexcodes/RegTestScores_highstakes_TestTakers.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
keep(TreatmentLevels TreatmentGains) stats(N ymean2 suma p, fmt(%9.0fc %9.2fc %9.2fc %9.2fc) labels("N. of obs." "Mean control group" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value(\$\alpha_3=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)
restore






eststo clear
foreach time in T3 T6{
	foreach var in Z_hisabati Z_kiswahili{
		eststo m_`var'_`time':  reghdfe `var'_`time' $treatmentlist i.Grade_`time' ${schoolcontrol} if Grade_`time'<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
		estadd ysumm
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		preserve
			gsort SchoolID Grade_`time' -`var'_`time'
			by SchoolID Grade_`time': gen conteo=_n
			drop if conteo<`cutLevels_`time'' & treatment2=="Levels"
			drop if conteo<`cutGains_`time'' & treatment2=="P4Pctile"
			reghdfe `var'_`time' $treatmentlist i.Grade_`time' ${schoolcontrol} if Grade_`time'<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
			lincom _b[TreatmentLevels]
			scalar CI_Levels_1=r(estimate)+1.96*r(se)
			scalar CI_Levels_2=r(estimate)-1.96*r(se)
			lincom _b[TreatmentGains]
			scalar CI_Gains_1=r(estimate)+1.96*r(se)
			scalar CI_Gains_2=r(estimate)-1.96*r(se)
			lincom (_b[TreatmentGains]-_b[TreatmentLevels] )
			scalar CI_Diff_1=r(estimate)+1.96*r(se)
			scalar CI_Diff_2=r(estimate)-1.96*r(se)
			
		restore
		preserve
			gsort SchoolID Grade_`time' `var'_`time'
			by SchoolID Grade_`time': gen conteo=_n
			drop if conteo<`cutLevels_`time'' & treatment2=="Levels"
			drop if conteo<`cutGains_`time'' & treatment2=="P4Pctile"
			reghdfe `var'_`time' $treatmentlist i.Grade_`time' ${schoolcontrol} if Grade_`time'<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
			lincom _b[TreatmentLevels]
			scalar CI_Levels_3=r(estimate)+1.96*r(se)
			scalar CI_Levels_4=r(estimate)-1.96*r(se)
			lincom _b[TreatmentGains]
			scalar CI_Gains_3=r(estimate)+1.96*r(se)
			scalar CI_Gains_4=r(estimate)-1.96*r(se)
			lincom (_b[TreatmentGains]-_b[TreatmentLevels] )
			scalar CI_Diff_3=r(estimate)+1.96*r(se)
			scalar CI_Diff_4=r(estimate)-1.96*r(se)
		restore
		estadd scalar Lee_Levels_1=min(`=scalar(CI_Levels_1)',`=scalar(CI_Levels_2)',`=scalar(CI_Levels_3)',`=scalar(CI_Levels_4)'): m_`var'_`time'
		estadd scalar Lee_Levels_2=max(`=scalar(CI_Levels_1)',`=scalar(CI_Levels_2)',`=scalar(CI_Levels_3)',`=scalar(CI_Levels_4)'): m_`var'_`time'
		estadd scalar Lee_Gains_1=min(`=scalar(CI_Gains_1)',`=scalar(CI_Gains_2)',`=scalar(CI_Gains_3)',`=scalar(CI_Gains_4)'): m_`var'_`time'
		estadd scalar Lee_Gains_2=max(`=scalar(CI_Gains_1)',`=scalar(CI_Gains_2)',`=scalar(CI_Gains_3)',`=scalar(CI_Gains_4)'): m_`var'_`time'
		estadd scalar Lee_Diff_1=min(`=scalar(CI_Diff_1)',`=scalar(CI_Diff_2)',`=scalar(CI_Diff_3)',`=scalar(CI_Diff_4)'): m_`var'_`time'
		estadd scalar Lee_Diff_2=max(`=scalar(CI_Diff_1)',`=scalar(CI_Diff_2)',`=scalar(CI_Diff_3)',`=scalar(CI_Diff_4)'): m_`var'_`time'
	}
}


label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

esttab  using "$latexcodes/RegTestScores_highstakes_Lee.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
keep(TreatmentLevels TreatmentGains) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value(\$\alpha_3=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)

esttab  using "$latexcodes/RegTestScores_highstakes_Lee_two.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines nogaps /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
drop(*) stats(Lee_Levels_1 Lee_Levels_2, fmt(a2 a2) labels("Lower 95\% CI (\$\alpha_1\$)" "Higher 95\% CI (\$\alpha_1\$)" )) ///
nonotes substitute(\_ _)

esttab  using "$latexcodes/RegTestScores_highstakes_Lee_three.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines nogaps /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
drop(*) stats(Lee_Gains_1 Lee_Gains_2, fmt(a2 a2) labels("Lower 95\% CI (\$\alpha_2\$)" "Higher 95\% CI (\$\alpha_2\$)" )) ///
nonotes substitute(\_ _)

esttab  using "$latexcodes/RegTestScores_highstakes_Lee_four.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines nogaps /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
drop(*) stats(Lee_Diff_1 Lee_Diff_2, fmt(a2 a2) labels("Lower 95\% CI (\$\alpha_3\$)" "Higher 95\% CI (\$\alpha_3\$)" )) ///
nonotes substitute(\_ _)
