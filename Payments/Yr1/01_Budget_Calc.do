capture log close
log using "CreatedData/4 Intervention/TwaEL_2015/BudgetCalcs.smcl", replace

************************************************************
****** Lets read JML data and destring/compress it.  *******
*******This is the data I'll use in the future *************
************************************************************
************************************************************

foreach archivo in "EL2015_Student_FinalRun_Mashindano_CLEAN" "EL2015_Student_FinalRun_StadiandControl_CLEAN"{
	di "`archivo'"
	use "RawData/12 Intervention_KFII/1 Endline 2015/9 FINAL DATA as of 3.11/`archivo'.dta", clear
	destring, replace
	compress
	replace school=lower(school)
	saveold "CreatedData/4 Intervention/TwaEL_2015/`archivo'.dta", replace
}


************************************************************
************************************************************
****** FIRS LETS CREATE SOME BASIC DATA ********************
************************************************************
************************************************************

**This IS KEY. This are the student groups based on last year's exam!
use "CreatedData/Consolidated/TWA_StudentLevelsBatches.dta", clear
drop if treatment2!="Gains"
drop if Grade==3
rename StuID_15 studentID
destring  studentID, replace
keep SchoolID studentID KisLevel1 EngLevel1 MathLevel1 KisLevel2 EngLevel2 MathLevel2
saveold "CreatedData/4 Intervention/TwaEL_2015/2015_GroupsStudent_Mash_Grade23.dta", replace version(12)

*These are the school groups based on last year's exam!
use "CreatedData/Consolidated/TWA_SchoolLevelsBatches.dta", clear
keep SchoolID TotalQ
rename TotalQ LevelGrade0
keep LevelGrade0 SchoolID
set obs `=_N+1'
replace SchoolID=736 in `=_N'
sort SchoolID
replace LevelGrade0 = LevelGrade0[42] in 44

saveold "CreatedData/4 Intervention/TwaEL_2015/2015_GroupsStudent_Mash_Grade1.dta", replace version(12)

************************************************************
************************************************************
****** Now lets get the number of students to be tested ***
************************************************************
************************************************************
use "RawData/12 Intervention_KFII/1 Endline 2015/1 Student Names List/2015_Student_Name_List_nopii", clear 
bys StuID_15: gen cont=_n
drop if cont==2
drop cont
ds StuID_15 DistrictID SchoolID, not
replace Grade15=Grade
merge 1:1 StuID_15 using "CreatedData/Consolidated/TWA_StudentLevelsBatches.dta", keepus(SchoolID NoInfoBL TestedEL Grade KisLevel1- MathLevel2)
*Drop students not tested at EL last year + students who were in grade 3 last year
drop if TestedEL==0
drop if Grade==3
drop if _merge==2 /*this should be no one... there is a school... whatever */
gen Match_Original=_merge
drop _merge
merge m:1 SchoolID using "CreatedData/4 Intervention/TwaEL_2015/2015_GroupsStudent_Mash_Grade1.dta"
drop _merge
merge m:1 SchoolID using "RawData/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2) update
drop _merge
merge m:1 SchoolID using "CommScripts/Kigoma Sample/KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440


replace Grade15=Grade+1 if Grade15==.
drop if Grade15==.


foreach var of varlist LevelGrade0 KisLevel1 EngLevel1 MathLevel1 KisLevel2 EngLevel2 MathLevel2{
	replace `var'=. if treatment2=="Levels"
} 

foreach var of varlist LevelGrade0{
	replace `var'=. if treatment2=="Gains" & Grade15!=1
	replace `var'=0 if treatment2=="Gains" & Grade15==1 & `var'==.
} 

foreach var of varlist KisLevel1 EngLevel1 MathLevel1{
	replace `var'=. if treatment2=="Gains" & Grade15!=2
	replace `var'=0 if treatment2=="Gains" & Grade15==2 & `var'==.
} 
foreach var of varlist KisLevel2 EngLevel2 MathLevel2{
	replace `var'=. if treatment2=="Gains" & Grade15!=3
	replace `var'=0 if treatment2=="Gains" & Grade15==3 & `var'==.
} 

gen studentid=StuID_15
destring studentid, replace
foreach archivo in "EL2015_Student_FinalRun_Mashindano_CLEAN" "EL2015_Student_FinalRun_StadiandControl_CLEAN"{
di "`archivo'"
merge 1:m studentid using "CreatedData/4 Intervention/TwaEL_2015/`archivo'.dta", keepus(studentid schoolid grade) update
replace studentid=. if studentid==999
replace studentid=. if studentid==-999
replace studentid=. if studentid==99
replace studentid=. if studentid==-99
replace studentid=_n if studentid==.
replace SchoolID=schoolid if _merge==2
drop _merge schoolid
duplicates drop studentid, force
}
replace Grade15=grade if Grade15==.
merge m:1 SchoolID using "RawData/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2) update replace
drop _merge
merge m:1 SchoolID using "CommScripts/Kigoma Sample/KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update replace
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440

drop if SchoolID==.
drop if treatment2=="Control"
drop if treatment2==""
drop if Grade15==.

saveold "CreatedData/4 Intervention/TwaEL_2015/StudentsTested_TWA_2015.dta", replace
collapse (count) Sutdents= SchoolID , by( treatment2 treatarm2 Grade15)
sort treatment2 treatarm2 Grade15
saveold "CreatedData/4 Intervention/TwaEL_2015/CountTested_TWA_2015.dta", replace
gen TotalStudents = sum( Sutdents)


scalar define BudgetGrd1_Mash=scalar(TotalBudget)*Sutdents[1]/TotalStudents[6]
scalar define BudgetGrd2_Mash=scalar(TotalBudget)*Sutdents[2]/TotalStudents[6]
scalar define BudgetGrd3_Mash=scalar(TotalBudget)*Sutdents[3]/TotalStudents[6] 

scalar define BudgetGrd1_Stadi=scalar(TotalBudget)*Sutdents[4]/TotalStudents[6]
scalar define BudgetGrd2_Stadi=scalar(TotalBudget)*Sutdents[5]/TotalStudents[6]
scalar define BudgetGrd3_Stadi=scalar(TotalBudget)*Sutdents[6]/TotalStudents[6] 

**MASHINDANO BUDGET PER SUBJECT ******
foreach subject in Kis Eng Math{
	foreach grade in 1 2 3{
		scalar define Budget`subject'_Grd`grade'_Mash=scalar(BudgetGrd`grade'_Mash)/3
	}
}


**STADI BUDGET PER SUBJECT ******


foreach subject in Kis Eng Math{
	foreach grade in 1 2 3{
		scalar define Budget`subject'_Grd`grade'_Stadi=scalar(BudgetGrd`grade'_Stadi)/3
	}
}

**STADI BUDGET PER SKILL ******

foreach skills in Silabi Maneno Sentenci{
	scalar define BudgetKis_`skills'_Grd1_Stadi=scalar(BudgetKis_Grd1_Stadi)/3
}

foreach skills in Letter Word Sentences{
	scalar define BudgetEng_`skills'_Grd1_Stadi=scalar(BudgetEng_Grd1_Stadi)/3
}

foreach skills in id uta bwa j t{
	scalar define BudgetMath_`skills'_Grd1_Stadi=scalar(BudgetMath_Grd1_Stadi)/5
}

foreach skills in Maneno Sentenci Aya{
	scalar define BudgetKis_`skills'_Grd2_Stadi=scalar(BudgetKis_Grd2_Stadi)/3
}

foreach skills in Word Sentences Paragraph{
	scalar define BudgetEng_`skills'_Grd2_Stadi=scalar(BudgetEng_Grd2_Stadi)/3
}

foreach skills in bwa j t z{
	scalar define BudgetMath_`skills'_Grd2_Stadi=scalar(BudgetMath_Grd2_Stadi)/4
}

foreach skills in Story Comp {
	scalar define BudgetKis_`skills'_Grd3_Stadi=scalar(BudgetKis_Grd3_Stadi)/2
}

foreach skills in Story Comp{
	scalar define BudgetEng_`skills'_Grd3_Stadi=scalar(BudgetEng_Grd3_Stadi)/2
}

foreach skills in j t z g{
	scalar define BudgetMath_`skills'_Grd3_Stadi=scalar(BudgetMath_Grd3_Stadi)/4
}

log close
