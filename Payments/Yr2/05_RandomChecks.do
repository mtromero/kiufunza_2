capture log close
log using "CreatedData/4 Intervention/TwaEL_2016/RandomControls.smcl", replace
use "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_Mashindano_CLEAN.dta", clear
replace stream = lower(stream)
rename schoolid SchoolID
rename studentid studentID
ds, has(type string)
foreach var of varlist `r(varlist)'{
	replace `var'="" if `var'=="--blank--" | `var'=="X" | `var'=="--unalignable--"
}
destring , replace
saveold "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_Mashindano_CLEAN_quality.dta", replace


use using "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_StadiandControl_CLEAN.dta", clear
replace stream = lower(stream)
rename schoolid SchoolID
rename studentid studentID
ds, has(type string)
foreach var of varlist `r(varlist)'{
	replace `var'="" if `var'=="--blank--" | `var'=="X" | `var'=="--unalignable--"
}
destring , replace
saveold "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_StadiandControl_CLEAN_quality.dta", replace


use "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_Mashindano_CLEAN_quality.dta", clear
append using "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_StadiandControl_CLEAN_quality.dta"


gen studentid=studentID
merge m:1 studentid using "RawData/12 Intervention_KFII/4 Endline 2016/StudentNameLists_06122016_nopii"
replace testedEL2016=0 if testedEL2016==.
drop _merge

******************************************************************************
******************************************************************************
******************************************************************************
**************** get randomization status
merge m:1 SchoolID using "RawData/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2) update replace
drop if _merge==2
drop _merge
merge m:1 SchoolID using "CommScripts/Kigoma Sample/KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update replace
drop if _merge==2
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440
*136 is a split school
replace treatarm2=1 if SchoolID==136
replace treatment2="Levels" if SchoolID==136
tab treatarm2, gen(TD) 

******************************************************************************
******************************************************************************
******************************************************************************
**CHECK FOR MISSINGS ****
sum testedEL2016 
scalar Tested=r(sum)
foreach var of varlist SchoolID grade stream district  gender set start startampm volname end endampm{
	qui tabmiss `var' if testedEL2016==1
	display "`=scalar(r(sum)/Tested)' missing in variable `var'"
}

******************************************************************************
******************************************************************************
******************************************************************************
*** CHECK FOR NOT MISSINGS IN FIRST 2 QUESTIONS OF "Sehemu  A" in grade 1
sum testedEL2016 if grade==1
scalar Tested=r(sum)
foreach skill in si ma se{
	qui tabmiss grd1_kis_a_`skill'_1 if grade==1
	display "Skills `skill' in Swahili/Qs1 has `=scalar(r(sum)/Tested)' missing"
	qui tabmiss grd1_kis_a_`skill'_2 if grade==1
	display "Skills `skill' in Swahili/Qs2 has `=scalar(r(sum)/Tested)' missing"
}

/*
foreach skill in l w se{
	qui tabmiss grd1_eng_a_`skill'_1 if grade==1
	display "Skills `skill' in English/Qs1 has `=scalar(r(sum)/Tested)' missing"
	qui tabmiss grd1_eng_a_`skill'_1 if grade==1
	display "Skills `skill' in English/Qs2 has `=scalar(r(sum)/Tested)' missing"
}
*/
foreach skill in id uta bwa j t{
	qui tabmiss grd1_his_a_`skill'_1 if grade==1
	display "Skills `skill' in Math/Qs1 has `=scalar(r(sum)/Tested)' missing"
	qui tabmiss grd1_his_a_`skill'_2 if grade==1
	display "Skills `skill' in Math/Qs2 has `=scalar(r(sum)/Tested)' missing"
}


******************************************************************************
******************************************************************************
*... if first two in skills are wrong, then don't ask the rest!
foreach skill in si ma se{
	 forvalues i=3/6{
		qui tabmiss grd1_kis_a_`skill'_`i' if grd1_kis_a_`skill'_1==0 & grd1_kis_a_`skill'_2==0 & grade==1
		display "Skills `skill' in Swahili/Qs`i' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
	}
}
/*
foreach skill in l w se{
	forvalues i=3/6{
		qui tabmiss grd1_eng_a_`skill'_`i' if grd1_eng_a_`skill'_1==0 & grd1_eng_a_`skill'_2==0 & grade==1
		display "Skills `skill' in English/Qs`i' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
	}
}
*/
foreach skill in id uta bwa j t{
		qui tabmiss grd1_his_a_`skill'_3 if grd1_his_a_`skill'_1==0 & grd1_his_a_`skill'_2==0 & grade==1
		display "Skills `skill' in Math/Qs`i' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
} 

capture drop Kis_*  Math_*
/*Total answers per skill*/
egen Kis_Silabi=rowtotal(grd1_kis_a_si_1 grd1_kis_a_si_2 grd1_kis_a_si_3 grd1_kis_a_si_4 grd1_kis_a_si_5 grd1_kis_a_si_6)
egen Kis_Maneno=rowtotal(grd1_kis_a_ma_1 grd1_kis_a_ma_2 grd1_kis_a_ma_3 grd1_kis_a_ma_4 grd1_kis_a_ma_5 grd1_kis_a_ma_6)
egen Kis_Sentenci=rowtotal(grd1_kis_a_se_1 grd1_kis_a_se_2 grd1_kis_a_se_3 grd1_kis_a_se_4 grd1_kis_a_se_5 grd1_kis_a_se_6)


/*
egen Eng_Letter=rowtotal(grd1_eng_a_l_1 grd1_eng_a_l_2 grd1_eng_a_l_3 grd1_eng_a_l_4 grd1_eng_a_l_5 grd1_eng_a_l_6)
egen Eng_Word=rowtotal(grd1_eng_a_w_1 grd1_eng_a_w_2 grd1_eng_a_w_3 grd1_eng_a_w_4 grd1_eng_a_w_5 grd1_eng_a_w_6)
egen Eng_Sentences=rowtotal(grd1_eng_a_se_1 grd1_eng_a_se_2 grd1_eng_a_se_3 grd1_eng_a_se_4 grd1_eng_a_se_5 grd1_eng_a_se_6)
*/

egen Math_id=rowtotal(grd1_his_a_id_1 grd1_his_a_id_2 grd1_his_a_id_3)
egen Math_uta=rowtotal(grd1_his_a_uta_1 grd1_his_a_uta_2 grd1_his_a_uta_3)
egen Math_bwa=rowtotal(grd1_his_a_bwa_1 grd1_his_a_bwa_2 grd1_his_a_bwa_3)
egen Math_j=rowtotal(grd1_his_a_j_1 grd1_his_a_j_2 grd1_his_a_j_3)
egen Math_t=rowtotal(grd1_his_a_t_1 grd1_his_a_t_2 grd1_his_a_t_3)

******************************************************************************
******************************************************************************
******************************************************************************
*Only goes to b section if NO more than 1 answer right in each section */
*Only go to c if everything is perfect! */
sum testedEL2016 if grade==1 & treatment2=="Gains"
scalar Tested=r(sum)
foreach var of varlist grd1_kis_b*{
	qui tabmiss `var' if Kis_Silabi>1 | Kis_Maneno>1 | Kis_Sentenci>1 & grade==1
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
}

foreach var of varlist grd1_kis_c*{
	qui tabmiss  `var' if Kis_Silabi<6 | Kis_Maneno<6 | Kis_Sentenci<18 & grade==1
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
}
/*
foreach var of varlist grd1_eng_b*{
	qui tabmiss  `var' if Eng_Letter>1 | Eng_Word>1 | Eng_Sentences>1  & grade==1
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
}

foreach var of varlist grd1_eng_c*{
	qui tabmiss  `var' if Eng_Letter<6 | Eng_Word<6 | Eng_Sentences<6  & grade==1
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
}
*/
foreach var of varlist grd1_his_c*{
	qui tabmiss  `var' if Math_id<3 | Math_uta<3 | Math_bwa<3 | Math_j<3 | Math_t<3  & grade==1
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
} 

******************************************************************************
******************************************************************************
*** CHECK FOR NOT MISSINGS IN FIRST 2 QUESTIONS OF "Sehemu  A" in grade 2
sum testedEL2016 if grade==2
scalar Tested=r(sum)

foreach skill in ma se{
	qui tabmiss grd2_kis_a_`skill'_1 if grade==2
	display "Skills `skill' in Swahili/Qs1 has `=scalar(r(sum)/Tested)' missing"
	qui tabmiss grd2_kis_a_`skill'_1 if grade==2
	display "Skills `skill' in Swahili/Qs2 has `=scalar(r(sum)/Tested)' missing"
}
/*
foreach skill in w se{
	qui tabmiss grd2_eng_a_`skill'_1 if grade==2
	display "Skills `skill' in English/Qs1 has `=scalar(r(sum)/Tested)' missing"
	qui tabmiss grd2_eng_a_`skill'_1 if grade==2
	display "Skills `skill' in English/Qs2 has `=scalar(r(sum)/Tested)' missing"
}
*/
foreach skill in  bwa j t {
	qui tabmiss grd2_his_a_`skill'_1 if grade==2
	display "Skills `skill' in Math/Qs1 has `=scalar(r(sum)/Tested)' missing"
	qui tabmiss grd2_his_a_`skill'_1 if grade==2
	display "Skills `skill' in Math/Qs2 has `=scalar(r(sum)/Tested)' missing"
}
******************************************************************************
******************************************************************************
*... if first two in skills are wrong, then don't ask the rest!
foreach skill in ma se{
	 forvalues i=3/6{
		qui tabmiss grd2_kis_a_`skill'_`i' if grd2_kis_a_`skill'_1==0 & grd2_kis_a_`skill'_2==0 & grade==2
		display "Skills `skill' in Swahili/Qs`i' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
	}
}

*.. if story is wrong, dont ask comprehensions
qui tabmiss grd2_kis_a_m_1 if grd2_kis_a_h==0 & grade==2
display "Skills `skill' in Swahili/Qs1 has `=scalar((r(N)-r(sum))/Tested)' nonmissing"

qui tabmiss grd2_kis_a_m_2 if grd2_kis_a_h==0 & grade==2
display "Skills `skill' in Swahili/Qs2 has `=scalar((r(N)-r(sum))/Tested)' nonmissing"


/*
foreach skill in w se{
	forvalues i=3/6{
		qui tabmiss grd2_eng_a_`skill'_`i' if grd2_eng_a_`skill'_1==0 & grd2_eng_a_`skill'_2==0 & grade==2
		display "Skills `skill' in English/Qs`i' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
	}
}
*/
foreach skill in  bwa j t {
		qui tabmiss grd2_his_a_`skill'_3 if grd2_his_a_`skill'_1==0 & grd2_his_a_`skill'_2==0 & grade==2
		display "Skills `skill' in Math/Qs`i' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
} 

drop Kis_* Math_*
*Calculcate how many correct questions per skill */
egen Kis_Maneno=rowtotal(grd2_kis_a_ma_1 grd2_kis_a_ma_2 grd2_kis_a_ma_3 grd2_kis_a_ma_4 grd2_kis_a_ma_5 grd2_kis_a_ma_6)
egen Kis_Sentenci=rowtotal(grd2_kis_a_se_1 grd2_kis_a_se_2 grd2_kis_a_se_3 grd2_kis_a_se_4 grd2_kis_a_se_5 grd2_kis_a_se_6)
gen Kis_Aya=grd2_kis_a_h
egen Kis_Comp=rowtotal(grd2_kis_a_m_1 grd2_kis_a_m_2)
 
/*
egen Eng_Word=rowtotal(grd2_eng_a_w_1 grd2_eng_a_w_2 grd2_eng_a_w_3 grd2_eng_a_w_4 grd2_eng_a_w_5 grd2_eng_a_w_6)
egen Eng_Sentences=rowtotal(grd2_eng_a_se_1 grd2_eng_a_se_2 grd2_eng_a_se_3 grd2_eng_a_se_4 grd2_eng_a_se_5 grd2_eng_a_se_6)
gen Eng_Paragraph=grd2_eng_a_p
*/

egen Math_bwa=rowtotal(grd2_his_a_bwa_1 grd2_his_a_bwa_2 grd2_his_a_bwa_3)
egen Math_j=rowtotal(grd2_his_a_j_1 grd2_his_a_j_2 grd2_his_a_j_3)
egen Math_t=rowtotal(grd2_his_a_t_1 grd2_his_a_t_2 grd2_his_a_t_3)
*egen Math_z=rowtotal(grd2_his_a_z_1 grd2_his_a_z_2 grd2_his_a_z_3)

******************************************************************************
******************************************************************************
/*You only do b if you get no more than 1 question correct per section
*/
/*You only do c if you get everything right beforehand
*/
sum testedEL2016 if grade==2 & treatment2=="Gains"
scalar Tested=r(sum)
foreach var of varlist grd2_kis_b*{
	qui tabmiss  `var' if Kis_Maneno>1 | Kis_Sentenci>1 | Kis_Aya>1 & grade==2
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
}

foreach var of varlist grd2_kis_c*{
	qui tabmiss  `var' if Kis_Maneno<6 | Kis_Sentenci<18 | Kis_Aya<3 & grade==2
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
}
/*
foreach var of varlist grd2_eng_b*{
	qui tabmiss  `var' if Eng_Word>1 | Eng_Sentences>1 | Eng_Paragraph>1 & grade==2
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
}

foreach var of varlist grd2_eng_c*{
	qui tabmiss  `var' if Eng_Word<6 | Eng_Sentences<6 | Eng_Paragraph<3 & grade==2
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
}
*/
foreach var of varlist grd2_his_b*{
	qui tabmiss  `var' if Math_bwa>1 | Math_j>1 | Math_t>1  & grade==2
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
} 
foreach var of varlist grd2_his_c*{
	qui tabmiss  `var' if Math_bwa<3 | Math_j<3 | Math_t<3 & grade==2
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
} 

******************************************************************************
******************************************************************************
*** CHECK FOR NOT MISSINGS IN FIRST 2 QUESTIONS OF "Sehemu  A" in grade 3
sum testedEL2016 if grade==3
scalar Tested=r(sum)
qui tabmiss grd3_kis_a_h  if grade==3
display "Skills Reading in Swahili/Qs1 has `=scalar(r(sum)/Tested)' missing"
qui tabmiss grd3_eng_a_s if grade==3
display "Skills English in Swahili/Qs1 has `=scalar(r(sum)/Tested)' missing"



foreach skill in  j t z g{
	qui tabmiss grd3_his_a_`skill'_1 if grade==3
	display "Skills `skill' in Math/Qs1 has `=scalar(r(sum)/Tested)' missing"
	qui tabmiss grd3_his_a_`skill'_1 if grade==3
	display "Skills `skill' in Math/Qs2 has `=scalar(r(sum)/Tested)' missing"
}

******************************************************************************
*.. if story is wrong, dont ask comprehensions
qui tabmiss grd3_kis_a_m_1 if grd3_kis_a_h==0 & grade==3
display "Skills `skill' in Swahili/Qs1 has `=scalar((r(N)-r(sum))/Tested)' nonmissing"

qui tabmiss grd3_kis_a_m_2 if grd3_kis_a_h==0 & grade==3
display "Skills `skill' in Swahili/Qs2 has `=scalar((r(N)-r(sum))/Tested)' nonmissing"


******************************************************************************
*... if story is wrong, dont ask comprehensions
qui tabmiss grd3_eng_a_c_1 if grd3_eng_a_s==0 & grade==3
display "Skills `skill' in English/Qs1 has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
qui tabmiss grd3_eng_a_c_2 if grd3_eng_a_s==0 & grade==3
display "Skills `skill' in English/Qs2 has `=scalar((r(N)-r(sum))/Tested)' nonmissing"

******************************************************************************
* for math is as before...
foreach skill in  j t z g{
		qui tabmiss grd3_his_a_`skill'_3 if grd3_his_a_`skill'_1==0 & grd3_his_a_`skill'_2==0 & grade==3
		display "Skills `skill' in Math/Qs`i' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"

} 


drop Kis_* Eng_* Math_*
gen Kis_Story=grd3_kis_a_h
egen Kis_Comp=rowtotal(grd3_kis_a_m_1 grd3_kis_a_m_2)
     
gen Eng_Story=grd3_eng_a_s
egen Eng_Comp=rowtotal(grd3_eng_a_c_1 grd3_eng_a_c_2)
 

egen Math_j=rowtotal(grd3_his_a_j_1 grd3_his_a_j_2 grd3_his_a_j_3)
egen Math_t=rowtotal(grd3_his_a_t_1 grd3_his_a_t_2 grd3_his_a_t_3)
egen Math_z=rowtotal(grd3_his_a_z_1 grd3_his_a_z_2 grd3_his_a_z_3)
egen Math_g=rowtotal(grd3_his_a_g_1 grd3_his_a_g_2 grd3_his_a_g_3)

******************************************************************************
******************************************************************************
/*You only do b if you get no more than 1 question correct per section
*/
/*You only do c if you get everything right beforehand
*/
sum testedEL2016 if grade==3 & treatment2=="Gains"
scalar Tested=r(sum)
foreach var of varlist grd3_kis_b*{
	qui tabmiss `var' if Kis_Story>1 | Kis_Comp>1 & grade==3
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
}

foreach var of varlist grd3_kis_c*{
	qui tabmiss `var' if Kis_Story<3 | Kis_Comp<2 & grade==3
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
}

foreach var of varlist grd3_eng_b*{
	qui tabmiss `var' if Eng_Story>1 | Eng_Comp>1 & grade==3
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
}

foreach var of varlist grd3_eng_c*{
	qui tabmiss `var' if Eng_Story<3 | Eng_Comp<2 & grade==3
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
}
foreach var of varlist grd3_his_b*{
	qui tabmiss `var' if  Math_j>1 | Math_t>1 | Math_z>1 | Math_g>1 & grade==3
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
} 
foreach var of varlist grd3_his_c*{
	qui tabmiss `var' if  Math_j<3 | Math_t<3 | Math_z<3 | Math_g<3 & grade==3
	display "`var' has `=scalar((r(N)-r(sum))/Tested)' nonmissing"
} 


******************************************************************************
******************************************************************************
******************************************************************************
**************** EVEN DISTRIBUTION OF SETS
tab set
ksmirnov set=1/5
*Reject... which is obvious from set
tab set treatarm2
ksmirnov set if treatarm2<=2, by(treatarm2)

**************** EVEN DISTRIBUTION OF missing students
gen D_missing=missing(studentID)
reg D_missing TD*, vce(cluster SchoolID) nocons
test (_b[TD1] == _b[TD3] )
test (_b[TD3] == _b[TD2] )
test (_b[TD1] == _b[TD2] )
test (_b[TD1] == _b[TD2] == _b[TD3])


**************** EVEN DISTRIBUTION OF non-tested students
reg testedEL2016 TD*, vce(cluster SchoolID) nocons
test (_b[TD1] == _b[TD3] )
test (_b[TD3] == _b[TD2] )
test (_b[TD1] == _b[TD2] )
test (_b[TD1] == _b[TD2] == _b[TD3])

bys studentID: gen Count=_N if studentID!=""
tab Count
**This tells you how many "repeated" there are

**See how long IDS are
*tostring studentID, gen(studentID_String)
gen IDLengthMR=strlen( studentID)
tab IDLengthMR

**See if fifth positions is 0,1,2,3
gen FifthIDMR=substr(studentID,5,1) 
tab FifthIDMR

**********8
************* TIME TAKEN ********

replace startampm="am" if startampm=="AS"
replace startampm="pm" if startampm=="MCH"
drop if startampm==""

replace endampm="am" if endampm=="AS"
replace endampm="pm" if endampm=="MCH"
drop if endampm==""

drop if start==""
split start, parse(:)

destring start2, force replace
drop if missing(start2)
drop if start2>=60
destring start1, force replace
drop if start1>=13


gen startFormat=start+" "+startampm
ntimeofday startFormat, gen(start_num) n(minutes) string(hours minutes) am(am) pm(pm)

drop if end==""
split end, parse(:)
destring end2, force replace
drop if missing(end2)
drop if end2>=60
destring end1, force replace
drop if end1>=13


gen endFormat=end+" "+endampm
ntimeofday endFormat, gen(end_num) n(minutes) string(hours minutes) am(am) pm(pm)

gen TiempoExamen=end_num-start_num
sum TiempoExamen,d
scalar Tested=r(sum)

qui sum TiempoExamen if TiempoExamen<0
display "`=scalar(r(N)/Tested)' have negative times"
display "`=scalar(r(N)/Tested)' more than 3 hrs times"

drop if TiempoExamen>60*3 | TiempoExamen<0

sum TiempoExamen,d
scalar Tested=r(sum)

qui sum TiempoExamen if TiempoExamen<0
display "`=scalar(r(N)/Tested)' have negative times"

qui sum TiempoExamen if TiempoExamen>15
display "`=scalar(r(N)/Tested)' spend more than 15 MINUTES"

qui sum TiempoExamen if TiempoExamen>20
display "`=scalar(r(N)/Tested)' spend more than 20 MINUTES"

qui sum TiempoExamen if TiempoExamen>25
display "`=scalar(r(N)/Tested)' spend more than 25 MINUTES"

qui sum TiempoExamen if TiempoExamen>30
display "`=scalar(r(N)/Tested)' spend more than 30 MINUTES"


sum TiempoExamen,d
display "`r(mean)' WAS THE AVERAGE TIME"
display "`r(p50)' WAS THE MEDIAN TIME"

reg TiempoExamen TD*, vce(cluster SchoolID) nocons
test (_b[TD1] == _b[TD3] )
test (_b[TD3] == _b[TD2] )
test (_b[TD1] == _b[TD2] )
test (_b[TD1] == _b[TD2] == _b[TD3])


drop grd1_kis_b* grd1_eng_b* 
drop grd1_kis_c* grd1_eng_c* grd1_his_c*

drop grd2_kis_b* grd2_eng_b* grd2_his_b*
drop grd2_kis_c* grd2_eng_c* grd2_his_c*

drop grd3_kis_b* grd3_eng_b* grd3_his_b*
drop grd3_kis_c* grd3_eng_c* grd3_his_c*

egen grd1Total=rowtotal(grd1*)


reg grd1Total TiempoExamen, vce(cluster SchoolID) nocons


egen grd2Total=rowtotal(grd2*)


reg grd2Total TiempoExamen, vce(cluster SchoolID) nocons


egen grd3Total=rowtotal(grd3*)

reg grd3Total TiempoExamen, vce(cluster SchoolID) nocons


log close

