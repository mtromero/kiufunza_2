\documentclass[pdf]{beamer}
\usepackage{booktabs}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{dsfont}
\usepackage{bbm}
\usepackage[flushleft]{threeparttable}      % Option: Table notes
\usepackage{floatpag}
\usepackage{wrapfig}
\usepackage{relsize}
\usepackage{setspace}  
\usepackage{apacite}   
\usepackage{xcolor,colortbl}
                  
\mode<presentation>{}
\usetheme{Warsaw}                  % Option: 2.0 and 1.5 spacing package.
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\let\oldtabular\tabular
\renewcommand{\tabular}{\tiny\oldtabular}
\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}
\title[Levels Vs Gains]{Teacher Performance Pay: Levels Vs Gains }

\author[Mbiti, Muralidharan \& Romero]{Isaac Mbiti \and Karthik Muralidharan \and Mauricio Romero }

\normalsize
\date{}
\begin{document}
\frame{\titlepage}


\section{Introduction}

\begin{frame}[plain]{Background/Motivation}
\begin{itemize}[<+->]
\vfill \item Teacher salaries are the largest component of education budgets
\vfill \item Usual determinants of teacher salaries are not correlated with student performance \cite{bettinger2004,Kane2008,Woessmann2011}
\vfill \item The evidence on teacher performance pay is at best mixed
\begin{itemize}[<+->]
\vfill \item Positive effects \cite{Lavy2002,Lavy2009,Glewwe2010,Muralidharan2011,Balch2015}
\vfill \item Little or no effects at all \cite{Springer2011,Goldhaber2012,Goodman2013}
\vfill \item Studies are not directly comparable
\end{itemize}
\end{itemize}
\end{frame}




\begin{frame}[plain]{This paper}
\begin{itemize}
\vfill \item Compare two incentive payment structures in the same context and with the same budget (both are set as tournaments)
\pause
\vfill \item  Performance based on levels (absolute learning levels)
\pause
\begin{itemize}
\vfill \item Easy to implement (scale up) and communicate
\vfill \item Not optimal
\end{itemize}
\pause
\vfill \item  Performance based on gains (Pay for percentile)
\pause
\begin{itemize}
\vfill \item Hard to implement (scale up) and communicate
\vfill \item ``Optimal''
\end{itemize}
\end{itemize}
\end{frame}


\section{Interventions}

\begin{frame}[plain]{Intervention - ``levels''}
\begin{itemize}
\vfill \item Teachers are rewarded for each student that pass grade-specific skills
\vfill \item Tournament: payment depends on total number of students that pass each skill (rewards for harder skills is higher)

\begin{equation}
P_j^s=\frac{X_s}{\sum\limits_{i \in T} 1_{T_i>b_s}} \sum\limits_{k \in J} 1_{T_k>b_s} 
\end{equation}
\vfill \item Focus on students that are ``in the money''
\end{itemize}
\end{frame}


\begin{frame}[plain]{Intervention - ``levels''}
\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{KFIISkills.pdf}
\end{figure}
\end{frame}

\begin{frame}[plain]{Intervention - ``gains''}
\begin{itemize}
\vfill \item Based on the work of \citeA{Barlevy2012} (induces teachers to exert socially optimal levels of effort)
\vfill \item Place students in bins according to initial levels of learning (handicapping)
\vfill \item Rank students at the end of the year, within each bin
\vfill \item Pay teachers according to the rank of their students
\vfill \item For a student in the top $1\%$ a teacher gets 99 points, and for a student in the bottom 1\% he gets no points
\begin{itemize}
\vfill \item For a student in the top 1\% a teacher receives \$1.77
\vfill \item For a student in the top 50\% a teacher receives \$0.89
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[plain]{Timeline}
\begin{figure}[H]
\centering
\includegraphics[height=\textheight]{KiuFunzaIITimeline.pdf}
\end{figure}
\end{frame}


\section{Design, data and validity}

\begin{frame}[plain]{Design}
\begin{figure}[H]
\centering
\caption{Districts in Tanzania from which schools are selected\label{fig:mapa}}
\includegraphics[width=0.7\textwidth]{map_rct_1.pdf}
\end{figure}
\end{frame}



\begin{frame}[plain,label=goback_design]{Design}
\begin{itemize}
\vfill \item In each district we pick 18 government primary schools
\vfill \item The sample of 180 schools was taken from a previous RCT (needed baseline scores from students) \hyperlink{previous_rct}{\beamergotobutton{*}}
\vfill \item From each district 6 schools were assigned to the ``levels'' treatment, 6 schools to the ``gains'' treatment and 6 schools were left as controls.
\end{itemize}
\end{frame}





\begin{frame}[plain,label=balance]{Design validity}
\begin{itemize}
\vfill \item No difference in baseline characteristics by 
\begin{itemize}
\vfill \item \hyperlink{balance_student}{\beamergotobutton{Students}}
\vfill \item \hyperlink{balance_school}{\beamergotobutton{School}}
\vfill \item \hyperlink{balance_hh}{\beamergotobutton{Household}}
\vfill \item \hyperlink{balance_teacher}{\beamergotobutton{Teachers}}
\end{itemize}
\vfill \item No differential attrition \hyperlink{attrition}{\beamergotobutton{*}} 
\end{itemize}
\end{frame}


\section{Test score results}

\begin{frame}[plain]{Test Scores - Two years}
\begin{center}
\include{RegKarthik_Pooled}
\end{center}
\end{frame}

\begin{frame}[plain]{Test Scores - Yr1 vs Yr2}
\begin{center}
\include{RegKarthik}
\end{center}
\end{frame}


\begin{frame}[plain]{Test Scores - Yr1 vs Yr2}
\begin{center}
\include{RegKarthik_HHControls}
\end{center}
\end{frame}


\begin{frame}[plain]{Test Scores - Yr1 vs Yr2}
\begin{center}
\include{RegKarthik_HHControls_EYOS}
\end{center}
\end{frame}

\begin{frame}[plain]{Heterogeneity baseline ability - Swahili}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.95\textwidth]{HeteroQuintile_Z_kiswahili.pdf} 
\end{center}
\end{figure}
\end{frame}


\begin{frame}[plain]{Heterogeneity baseline ability - Math}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.95\textwidth]{HeteroQuintile_Z_hisabati.pdf} 
\end{center}
\end{figure}
\end{frame}


\begin{frame}[plain]{Heterogeneity baseline ability - English}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.95\textwidth]{HeteroQuintile_Z_kiingereza.pdf} 
\end{center}
\end{figure}
\end{frame}


\section{So what happened? (i.e., Mechanisms)}


\begin{frame}[plain]{Do teachers understand/trust the intervention?}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.95\textwidth]{Teacher understanding.pdf} 
\end{center}
\end{figure}
\hyperlink{trust}{\beamergotobutton{Table}}
\end{frame}


\begin{frame}[plain]{Is there a shift in resources?}
\begin{center}
\input{RegTeacher_Support1}
\end{center}
\end{frame}

\begin{frame}[plain]{Is there a shift in resources?}
\begin{center}
\input{RegTeacher_Support2}
\end{center}
\end{frame}

\begin{frame}[plain]{Is there teaching to the test? - High-stakes exam}
\begin{center}
\input{RegKarthikTWA}
\end{center}
\end{frame}

\begin{frame}[plain,label=comparison_yr1]{Is there teaching to the test? - Yr1}
\begin{center}
\begin{tabular}{l*{3}{c}}
\textbf{Panel A: Math} & & & \\
\input{RegZ_hisabati_InvVsEDI_Comp_Yr1}
\\
\textbf{Panel B: Swahili} & & & \\
\input{RegZ_kiswahili_InvVsEDI_Comp_Yr1}
\\
\textbf{Panel C: English} & & & \\
\input{RegZ_kiingereza_InvVsEDI_Comp_Yr1}
\hline
\end{tabular}
\end{center}
\hyperlink{TwaEDIYr1}{\beamergotobutton{Full comparison}}
\end{frame}


\begin{frame}[plain,label=comparison_yr2]{Is there teaching to the test? - Yr2}
\begin{center}
\begin{tabular}{l*{3}{c}}
\textbf{Panel A: Math} & & & \\
\input{RegZ_hisabati_InvVsEDI_Comp_Yr2}
\\
\textbf{Panel B: Swahili} & & & \\
\input{RegZ_kiswahili_InvVsEDI_Comp_Yr2}
\\
\textbf{Panel C: English} & & & \\
\input{RegZ_kiingereza_InvVsEDI_Comp_Yr2}
\hline
\end{tabular}
\end{center}
\hyperlink{TwaEDIYr2}{\beamergotobutton{Full comparison}}
\end{frame}


\begin{frame}[plain]{Do we observe a change in teacher behavior?}
\include{RegTeacherAbs_Pooled}
\end{frame}


\begin{frame}[plain]{Do we observe a change in teacher behavior?}
\include{RegUnObs}
\end{frame}

\begin{frame}[plain]{Do we observe a change in teacher behavior?}
\include{RegObservationsIndex}
\end{frame}

\begin{frame}[plain]{Do students report a change in teacher behavior?}
\include{RegBehavior_Student}
\end{frame}





\section{Extra slides}

\renewcommand*{\refname}{} % This will define heading of bibliography to be empty, so you can...
\begin{frame}[allowframebreaks]{Bibliography}
	\bibliographystyle{apacite}
\bibliography{TanBib}
\end{frame}


\begin{frame}[plain,label=TwaEDIYr1]{Twaweza vs EDI -- Yr1}
\begin{center}
\resizebox*{!}{\dimexpr\textheight-2\baselineskip\relax}{%
\begin{tabular}{l*{5}{c}}
\toprule
\multicolumn{6}{l}{\textbf{Panel A: Math}} \\
\input{RegZ_hisabati_TwaVsEDI_Yr1}
\\
\multicolumn{6}{l}{\textbf{Panel B: Swahili}} \\
\input{RegZ_kiswahili_TwaVsEDI_Yr1}
\\
\multicolumn{6}{l}{\textbf{Panel C: English}} \\
\input{RegZ_kiingereza_TwaVsEDI_Yr1}
\bottomrule
\multicolumn{6}{l}{\footnotesize \specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{center}
\hyperlink{comparison_yr1}{\beamergotobutton{Back}}
\end{frame}


\begin{frame}[plain,label=TwaEDIYr2]{Twaweza vs EDI -- Yr2}
\begin{center}
\resizebox*{!}{\dimexpr\textheight-2\baselineskip\relax}{%
\begin{tabular}{l*{5}{c}}
\toprule
\multicolumn{6}{l}{\textbf{Panel A: Math}} \\
\input{RegZ_hisabati_TwaVsEDI_Yr2}
\\
\multicolumn{6}{l}{\textbf{Panel B: Swahili}} \\
\input{RegZ_kiswahili_TwaVsEDI_Yr2}
\\
\multicolumn{6}{l}{\textbf{Panel C: English}} \\
\input{RegZ_kiingereza_TwaVsEDI_Yr2}
\bottomrule
\multicolumn{6}{l}{\footnotesize \specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{center}
\hyperlink{comparison_yr2}{\beamergotobutton{Back}}
\end{frame}



\begin{frame}[plain,label=balance_student]{Baseline descriptive/balance: students}
\begin{center}
\input{summaryStudentBase}
\hyperlink{balance}{\beamergotobutton{Back}}
\end{center}
\end{frame}

\begin{frame}[plain,label=balance_school]{Baseline descriptive/balance: schools and households}
\begin{center}
\input{summarySchoolBase}
\hyperlink{balance}{\beamergotobutton{Back}}
\end{center}
\end{frame}

\begin{frame}[plain,label=balance_hh]{Baseline descriptive/balance: households}
\begin{center}
\input{summaryHHBase}
\hyperlink{balance}{\beamergotobutton{Back}}
\end{center}
\end{frame}

\begin{frame}[plain,label=balance_teacher]{Baseline descriptive/balance: teachers}
\begin{center}
\input{summaryTeachersBase}
\hyperlink{balance}{\beamergotobutton{Back}}
\end{center}
\end{frame}

\begin{frame}[plain,label=attrition]{Attrition}
\begin{center}
\input{RegattendanceEDI}
\hyperlink{balance}{\beamergotobutton{Back}}
\end{center}
\end{frame}

\begin{frame}[plain,label=trust]{Twaweza}
\begin{center}
\input{RegTeacher_Trust1}
\hyperlink{trust_graph}{\beamergotobutton{Back}}
\end{center}
\end{frame}



\begin{frame}[plain,label=previous_rct]{Design}
\begin{table}[H]
  \centering
  \caption{}
    \begin{tabular}{rrrrrr}
    \toprule
          &       & \multicolumn{4}{c}{Current treatment} \\
    \midrule
          &       & Levels   & Gains & Control & Total \\
    Previous RCT & C1   & 40    & 20    & 10    & 70 \\
          & C2 & 10    & 30    & 30    & 70 \\
          & C3 & 10    & 10    & 20    & 40 \\
          & Total & 60    & 60    & 60    & 180 \\
    \bottomrule
    \end{tabular}%
  \label{tab:addlabel}%
\end{table}%
\hyperlink{goback_design}{\beamergotobutton{Back}}

\end{frame}

\end{document}

