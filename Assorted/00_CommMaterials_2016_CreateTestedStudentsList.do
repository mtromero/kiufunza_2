
cd "C:/Users/Mauricio/Box Sync/01_KiuFunza/"
use "RawData/12 Intervention_KFII/1 Endline 2015/1 Student Names List/2015_Student_Name_List_nopii", clear
keep StuID_15 DistrictID SchoolID Grade Stream Grade_14
rename Grade grade
rename Stream stream
rename SchoolID schoolid
rename DistrictID districtid
rename Grade_14 grade2014
gen studentid=StuID_15
gen TestedEL=0
destring grade2014, replace

merge 1:m studentid using "RawData/12 Intervention_KFII/1 Endline 2015/9 FINAL DATA as of 3.11/EL2015_Student_FinalRun_Mashindano_CLEAN.dta", update
replace TestedEL=1 if _merge>=2
drop _merge
gen grade20142=substr(studentid,5,1)
destring grade20142, replace
replace grade2014=grade20142 if grade2014==.
replace grade2014=0 if grade==1 & grade2014==.
replace grade2014=1 if grade==2 & grade2014==.
replace grade2014=2 if grade==3 & grade2014==.
drop grade20142
replace studentid="99999999" if studentid=="-999"
sort schoolid grade2014 studentid
gen studnumOld=substr(studentid,6,.) if studentid!="99999999"
destring studnumOld, replace
egen max_studnumOld=max(studnumOld), by(schoolid grade2014)
replace max_studnumOld=0 if max_studnumOld==.
bys schoolid grade2014 studentid: gen studnum_temp=_n
bys schoolid grade2014: gen studnum=studnum_temp+max_studnumOld if studentid=="99999999"
drop studnum_temp max_studnumOld studnumOld
tostring studnum, format(%03.0f) generate(studnum_s)
tostring grade2014, generate(grade2014_s)
tostring schoolid, format(%04.0f) generate (schoolid_s)
gen newstudentid=schoolid_s+grade2014_s+studnum_s
replace studentid=newstudentid if studentid=="99999999"
isid studentid
drop newstudentid dup studnum_s studnum grade2014_s schoolid_s

merge 1:m studentid using "RawData/12 Intervention_KFII/1 Endline 2015/9 FINAL DATA as of 3.11/EL2015_Student_FinalRun_StadiandControl_CLEAN.dta", update
replace TestedEL=1 if _merge>=2
drop _merge
gen grade20142=substr(studentid,5,1)
destring grade20142, replace
replace grade2014=grade20142 if grade2014==.
replace grade2014=0 if grade==1 & grade2014==.
replace grade2014=1 if grade==2 & grade2014==.
replace grade2014=2 if grade==3 & grade2014==.
drop grade20142
replace studentid="99999999" if studentid=="-999"
sort schoolid grade2014 studentid
gen studnumOld=substr(studentid,6,.) if studentid!="99999999"
destring studnumOld, replace
egen max_studnumOld=max(studnumOld), by(schoolid grade2014)
replace max_studnumOld=0 if max_studnumOld==.
bys schoolid grade2014 studentid: gen studnum_temp=_n
bys schoolid grade2014: gen studnum=studnum_temp+max_studnumOld if studentid=="99999999"
*drop studnum_temp max_studnumOld studnumOld
tostring studnum, format(%03.0f) generate(studnum_s)
tostring grade2014,  generate(grade2014_s)
tostring schoolid, format(%04.0f) generate (schoolid_s)
gen newstudentid=schoolid_s+grade2014_s+studnum_s
replace studentid=newstudentid if studentid=="99999999"
isid studentid
drop newstudentid dup studnum_s studnum grade2014_s schoolid_s


rename schoolid SchoolID
merge m:1 SchoolID using "RawData/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2) update replace
drop _merge
merge m:1 SchoolID using "CommScripts/Kigoma Sample/KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update replace
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440

**Control schools in district 11 it seems
*drop if districtid==.
gen districtid2=substr(studentid,1,2)
destring districtid2, replace
replace districtid=districtid2 if districtid==.
replace districtid=11 if SchoolID==1119
gen length=strlen(studentid)
tab length
drop length

saveold "RawData/12 Intervention_KFII/1 Endline 2015/AllStudentsTested_2015.dta", replace


merge m:1 StuID_15 using  "RawData/4 Intervention/TwaEL_2014/TwaTestData_2014_allstudents.dta", keepus(StuID_15)
drop if _merge==2
gen TestedEL2014=(_merge==3)
drop _merge
drop if TestedEL2014==0 & TestedEL==0 & (grade2014==1 | grade2014==2)
rename TestedEL TestedEL2015
drop tested2015 StuID_15
rename grd3_kis_c_h1 grd3_eng_c_s
rename grd3_eng_c_c_11 grd3_eng_c_c_2
saveold "RawData/12 Intervention_KFII/1 Endline 2015/AllStudentsTested_2015.dta", replace


gen StuID_15= studentid
merge 1:1 StuID_15 using "CreatedData\4 Intervention\TwaEL_2015\StudentsTested2014_NotIn2015.dta", keepus(Grade)

replace grade=Grade+1 if _merge==3
drop _merge
drop Grade
saveold "RawData/12 Intervention_KFII/1 Endline 2015/AllStudentsTested_2015.dta", replace
