*Trying to find missing 2014 students via fuzzy merge on student name
*March 10, 2016

cd "C:\Users\jmahoney\Desktop\Stata\Intervention Reclink"

/*
*Mashindano
use "D:\Box Sync\01_KiuFunza\CreatedData\4 Intervention\TwaEL_2015\StudentsTested2014_NotIn2015.dta" 
keep if treatarm2==2
merge 1:1 StuID_15 using "C:\Users\jmahoney\Dropbox (TWAWEZA)\Dropbox KiuFunza II Intervention\2 Intervention\2015\3 Endline\2 Data Collection Forms\Student Data Forms\Student Names\TwaTestData_2014_allstudents.dta", keepus (StuName StuID_15)
drop if _merge!=3
drop _merge
save "Reclink Missing 2014 Students_withName_MASHINDANO.dta", replace
clear

use "Reclink Missing 2014 Students_withName_MASHINDANO.dta"
rename StuID_15 studentid
rename StuName stuname
rename SchoolID schoolid
reclink2 stuname schoolid using "C:\Users\jmahoney\Dropbox (TWAWEZA)\Dropbox KiuFunza II Intervention\7 Data\Intervention data\TwaEL_2015\1 processed data\9_Final Student Data_CLEAN\EL2015_Student_FinalRun_Mashindano_CLEAN.dta", idm(studentid) idu(name) gen(Stu_Name_Fuzzy) required(stuname schoolid)
*ug, only 62 are linked.
rename studentid studentid_reclink
save "Reclink Missing 2014 Students_MASHINDANO_linked.dta", replace
sort Stu_Name_Fuzzy DistrictID schoolid
order Stu_Name_Fuzzy DistrictID schoolid studentid_reclink stuname Ustuname 
browse
export excel using "Reclink Missing 2014 Students_MASHINDANO_linked.xlsx", firstrow(var)

*Stadi + Control
use "D:\Box Sync\01_KiuFunza\CreatedData\4 Intervention\TwaEL_2015\StudentsTested2014_NotIn2015.dta" 
keep if treatarm2!=2
merge 1:1 StuID_15 using "C:\Users\jmahoney\Dropbox (TWAWEZA)\Dropbox KiuFunza II Intervention\2 Intervention\2015\3 Endline\2 Data Collection Forms\Student Data Forms\Student Names\TwaTestData_2014_allstudents.dta", keepus (StuName StuID_15)
drop if _merge!=3
drop _merge
save "Reclink Missing 2014 Students_withName_STADICONTROL.dta", replace
clear

import delimited using "C:\Users\jmahoney\Desktop\Stata\Intervention Reclink\Reclink Missing 2014 Students_MASHINDANO_linked.csv", varnames (1)
rename stuname stuname_2014
rename ustuname stuname
tostring studentid_reclink, replace
gen IDLength=strlen( studentid_reclink)
replace studentid_reclink="0"+studentid_reclink if IDLength==7
gen IDLength_2=strlen( studentid_reclink)
tab IDLength_2
replace schoolid=736 if v8==1
save "Reclink Missing 2014 Students_MASHINDANO_linked.dta", replace

use "Reclink Missing 2014 Students_withName_STADICONTROL.dta"
rename StuID_15 studentid
rename StuName stuname
rename SchoolID schoolid
reclink2 stuname schoolid using "C:\Users\jmahoney\Dropbox (TWAWEZA)\Dropbox KiuFunza II Intervention\7 Data\Intervention data\TwaEL_2015\1 processed data\9_Final Student Data_CLEAN\EL2015_Student_FinalRun_StadiandControl_CLEAN_3.8.dta", idm(id) idu(name) gen(Stu_Name_Fuzzy) required(schoolid) wmatch(20 1) wnomatch(1 20) minscore (0.9977)
drop if Stu_Name_Fuzzy==.
rename studentid studentid_reclink
save "Reclink Missing 2014 Students_STADICONTROL_linked.dta", replace
sort Stu_Name_Fuzzy DistrictID schoolid
order Stu_Name_Fuzzy DistrictID schoolid studentid_reclink stuname Ustuname 
browse
export excel using "Reclink Missing 2014 Students_STADICONTROL_linked.xlsx", firstrow(var)
*/

import delimited using "C:\Users\jmahoney\Desktop\Stata\Intervention Reclink\Reclink Missing 2014 Students_STADICONTROL_linked.csv", varnames (1)
rename stuname stuname_2014
rename ustuname stuname
tostring studentid_reclink, replace
gen IDLength=strlen( studentid_reclink)
replace studentid_reclink="0"+studentid_reclink if IDLength==7
gen IDLength_2=strlen( studentid_reclink)
tab IDLength_2
save "Reclink Missing 2014 Students_STADICONTROL_linked.dta", replace
