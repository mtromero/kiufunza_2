*Cleaning Final Student Data: Stadi & Control Schools
*Jessica Mahoney
*February 18, 2016
*Last Edit: March 1, 2016

clear
set more off

cd "C:\Users\jmahoney\Dropbox (TWAWEZA)\Dropbox KiuFunza II Intervention\7 Data\Intervention data\TwaEL_2015\1 processed data\4_Final Submission"
use "EL2015_Student_FinalRun_StadiandControl.dta"

*Check # of schools -- should be 6 per district except for 7 in Lushoto & Kinondoni, 5 in Kigoma
	*clean schoolIDs to get actual number of unique schools
replace districtid=9 if districtid==69
replace districtid=4 if districtid==402
replace districtid=7 if districtid==708
replace districtid=7 if districtid==716
replace districtid=4 if name=="0417_2_STD.pdf p140"
replace districtid=6 if name=="0622_1_STD.pdf p32"
replace districtid=1 if name=="0135_3_STD.pdf p12"
replace districtid=9 if name=="0905_1_STD.pdf p60"
replace districtid=9 if name=="0929_3_STD.pdf p44"
replace districtid=4 if name=="0412_1_STD.pdf p46"
replace districtid=4 if name=="0412_1_STD.pdf p205"
replace districtid=4 if school=="MBEZI NDUMBWI" | school=="MBEZINDUMBWI"
tab districtid

	*fixing Geita
replace schoolid=129 if schoolid==12
replace schoolid=135 if schoolid==7135
	*fixing Karagwe
replace schoolid=319 if schoolid==3
replace schoolid=315 if schoolid==31
drop if schoolid==309
	*fixing Kinondoni
replace schoolid=417 if schoolid==4
replace schoolid=402 if schoolid==40
replace schoolid=426 if schoolid==420
replace schoolid=419 if schoolid==459
replace schoolid=417 if name=="0417_1_STD.pdf p54"
replace schoolid=417 if name=="0417_1_STD.pdf p202"
replace schoolid=419 if schoolid==2119
replace schoolid=414 if schoolid==4114
	*fixing Kondoa
replace schoolid=534 if schoolid==5
	*fixing Korogwe
replace schoolid=610 if schoolid==620
replace schoolid=610 if schoolid==640
	*fixing Lushoto
replace schoolid=708 if schoolid==70
replace schoolid=719 if schoolid==219
replace schoolid=716 if name=="0716_1_STD.pdf p143"
replace schoolid=735 if schoolid==733
replace schoolid=735 if schoolid==738
replace schoolid=719 if schoolid==745
replace schoolid=716 if schoolid==776
replace schoolid=735 if schoolid==785
	*fixing Mbozi
replace schoolid=905 if schoolid==9
replace schoolid=915 if name=="0915_1_STD.pdf p283"
replace schoolid=931 if schoolid==913
replace schoolid=905 if schoolid==6905
replace schoolid=908 if schoolid==6908
replace schoolid=915 if schoolid==6915
replace schoolid=919 if schoolid==6919
replace schoolid=929 if schoolid==6929
replace schoolid=931 if schoolid==6931
replace schoolid=915 if schoolid==9915
	*fixing Kigoma
drop if name=="1101_2_STD-page12.pdf"
	
*Check all schools are Stadi or Control schools
rename schoolid SchoolID
merge m:1 SchoolID using "X:\Box.net\Twaweza Schools Data\2015\4 Endline\Final Deliverables 1214\Final Data\School Data\R8School.dta", keepus(SchoolID treatarm2)
rename SchoolID schoolid
drop if _merge==2
tab treatarm2
drop _merge

*Add in Excel info that can't be merged (spans multiple pages)
gen pos_space=strpos(name," ")+2
	replace pos_space=strpos(name,"page")+4 if pos_space==2
	gen name_numb=substr(name,pos_space,.) 
	replace name_numb=subinstr(name_numb,".png","",.)
	replace name_numb=subinstr(name_numb,"png","",.)
	replace name_numb=subinstr(name_numb,".pdf","",.)
	replace name_numb=subinstr(name_numb,"pdf","",.)
	destring name_numb, replace
	
forvalues i=1/37{
	replace stream="A" if districtid==1 & schoolid==104 & grade==2 & name_numb==`i'
	}	
	
forvalues i=38/77{
	replace stream="B" if districtid==1 & schoolid==104 & grade==2 & name_numb==`i'
	}
	
forvalues i=25/29{
	replace stream="C" if districtid==1 & schoolid==129 & grade==2 & name_numb==`i'
	}
	
forvalues i=207/215{
	replace stream="A" if districtid==1 & schoolid==134 & grade==1 & name_numb==`i'
	}
	
forvalues i=217/220{
	replace stream="C" if districtid==1 & schoolid==134 & grade==1 & name_numb==`i'
	}	
	
forvalues i=71/75{
	replace stream="B" if districtid==1 & schoolid==134 & grade==3 & name_numb==`i'
	}
	
replace studentid="-999" if schoolid==802 & grade==1
replace studentid="-999" if schoolid==804 & grade==1
replace studentid="-999" if schoolid==808 & grade==1
replace studentid="-999" if schoolid==815 & grade==1
replace studentid="-999" if (schoolid==819 & grade==1) & (studentid=="--blank--")
replace studentid="-999" if schoolid==820 & grade==1

replace stream="A" if schoolid==808

*fix duplicate 'names' (something weird Kinondoni did in school 417 with rescans)
bys name: gen dup=_n
tostring dup, replace
replace name=name+dup if dup=="2"
isid name

*Merge in Excel Data
merge 1:m name using "C:\Users\jmahoney\Dropbox (TWAWEZA)\Dropbox KiuFunza II Intervention\7 Data\Intervention data\TwaEL_2015\0 raw data\9_Excel Files for Merging Student Data\Student Data Form Changes for Merge_STADICONTROL_withPDFname.dta", keepus (name studentid_merge gender_merge stream_merge set_merge)
drop _merge
replace stream_merge="--blank--" if stream_merge==""
replace gender_merge="--blank--" if gender_merge==""
replace studentid_merge="--blank--" if studentid_merge==""

	*checking to see if some of the stream information is not missing, and if so, if it matches the rescans
list name districtid schoolid stream stream_merge if stream_merge!="--blank--"
	*status: RESOLVED (all Captricity are correct). Two issues:
		* 0134_3_STD.pdf p74 (Captricity: B; Excel: E)
		* 0514_2_STD.pdf p82 (Captricity: D; Excel: A)
replace stream=stream_merge if stream_merge!="--blank--"

	*checking the same with gender
list name districtid schoolid gender gender_merge if gender_merge!="--blank--"
	*status: RESOLVED (all Captricity are correct). Four issues:
		* 0417_1_STD.pdf p193 (Captricity: ME; Excel: KE)
		* 0909_2_STD.pdf p155 (Catricity: KE; Excel: ME)
		* 0909_2_STD.pdf p178 (Catricity: KE; Excel: ME)
		* 0929_2_STD.pdf p66 (Catricity: KE; Excel: ME)
replace gender=gender_merge if stream_merge!="--blank--"
	
	*checking same with set number
list name districtid schoolid set set_merge if set_merge!=.
	*status: RESOLVED (all Captricity are correct): One issue:
		* 0827_2_STD.pdf p1 (Captricity: 7; Excel: 8)
replace set=set_merge if set_merge!=.

	*checking same with studentid
list name districtid schoolid studentid studentid_merge if studentid_merge!="--blank--"
	*status: RESOLVED: One Issue:
		* 0134_2_STD.pdf p41 (Captricity: 11; Excel: -999)
replace studentid=studentid_merge if studentid_merge!="--blank--"
replace studentid=studentid_merge if name=="0134_2_STD.pdf p41"

*Clean Stream Information
	*make sure all letters (no numbers)
	*make sure letters make sense -- no X or R, etc.
replace stream="A and B" if stream=="A  and  B" | stream=="A and  B" | stream=="A AND B" | stream=="A andB" | stream=="a and b" | stream=="A	and B" | stream=="A and	B" | stream=="A   and  B" | stream=="A  and   B"
replace stream="C and D" if stream=="C  and  D" | stream=="C AND D" | stream=="C and d" | stream=="c and d" | stream=="C	and	D" | stream=="C   and   D"
replace stream="A" if stream==" A"
replace stream="C" if stream==" C"
replace stream="A" if stream=="a"
replace stream="B" if stream=="b"
replace stream="C" if stream=="c"
replace stream="D" if stream=="d"
replace stream="E" if stream=="e"
replace stream="F" if stream=="f"
replace stream="G" if stream=="g"
replace stream="H" if stream=="h"
replace stream="I" if stream=="i"	
replace stream="B" if stream=="BO"
replace stream="C" if stream=="L"
replace stream="A" if stream=="X"

replace stream="A" if name=="0122_3_STD.pdf p297"
replace stream="A" if schoolid==826
replace stream="A" if schoolid==326 & grade==3

*import final missing stream info
merge m:1 name using "C:\Users\jmahoney\Dropbox (TWAWEZA)\Dropbox KiuFunza II Intervention\7 Data\Intervention data\TwaEL_2015\0 raw data\9_Excel Files for Merging Student Data\Stadi Control Final Data_MK.dta", keepus (name stream_missing)
drop if _merge==2
drop _merge
replace stream=stream_missing if stream=="--blank--"
replace stream="B" if name=="0417_2_STD.pdf p212"
replace stream="B" if name=="0417_2_STD.pdf p29"

merge m:1 name using "C:\Users\jmahoney\Dropbox (TWAWEZA)\Dropbox KiuFunza II Intervention\7 Data\Intervention data\TwaEL_2015\0 raw data\9_Excel Files for Merging Student Data\Stadi Control Final Data_MK_Sumbawanga.dta", keepus (name stream_missing2)
replace stream=stream_missing2 if stream=="--blank--"
drop _merge

tab stream, missing

*Clean Student Names
* standardize blanks
replace stuname_corr="--blank--" if stuname_corr=="Blank" | stuname_corr=="--BLANK--" | stuname_corr=="BLANK" | stuname_corr=="blank"
replace stuname="--blank--" if stuname=="Blank" | stuname=="--BLANK--" | stuname=="BLANK" | stuname=="blank"
	*check that there are no blank student names if student name corrected variable is also blank
list districtid schoolid if stuname=="--blank--" & stuname_corr=="--blank--"
	replace stuname="BILUTE ZAKAYO" if studentid=="11031028"
	*two are blank and -999s, so can't fix them
		* 1009_1_STD.pdf p22 and 1009_2_STD.pdf p12
	
	*fix student name corrected variable if the corrected name is only one name (sometimes DCs only rewrote the name with a misspelling, instead of copying down the whole name again
replace stuname_corr="--blank--" if stuname_corr=="b"
replace stuname_corr="PRISCA S MLENGA" if name=="0408_2_STD.pdf p97"
replace stuname_corr="JANETH E MWANISAWA" if name=="1011_1_STD.pdf p69"
replace stuname_corr="FERDICK S. SIKANDA" if name=="1011_3_STD.pdf p39"
replace stuname_corr="IRENE KUSOMGWA" if name=="1032_2_STD.pdf p50"
replace stuname_corr="SAMSON LUFUNGULO" if name=="1032_2_STD.pdf p63"

	*replace stuname with stuname_corr
replace stuname=stuname_corr if stuname_corr!="--blank--"

*Filling in/identifying missing information:
	*district id (can add in myself)
replace districtid=1 if name=="0104_2_STD.pdf p27"
replace districtid=1 if name=="0104_2_STD.pdf p38"
replace districtid=1 if name=="0104_2_STD.pdf p70"
replace districtid=1 if name=="0104_2_STD.pdf p74"
replace districtid=1 if name=="0104_3_STD.pdf p27"
replace districtid=1 if name=="0104_3_STD.pdf p53"
replace districtid=1 if name=="0104_3_STD.pdf p57"
replace districtid=1 if name=="0135_2_STD.pdf p113"
replace districtid=1 if name=="0135_3_STD.pdf p2"
replace districtid=1 if name=="0135_3_STD.pdf p49"
replace districtid=1 if name=="0135_3_STD.pdf p72"
replace districtid=4 if name=="0402_1_STD.pdf p158"
replace districtid=4 if name=="0414_1_STD.pdf p4"
replace districtid=4 if name=="0417_2_STD.pdf p144"
replace districtid=4 if name=="0417_2_STD.pdf p212"
replace districtid=4 if name=="0417_2_STD.pdf p29"
replace districtid=4 if name=="0432_2_STD.pdf p187"
replace districtid=5 if name=="0514_2_STD.pdf p17"
replace districtid=7 if name=="0708_2_STD.pdf p116"
replace districtid=7 if name=="0708_2_STD.pdf p152"
replace districtid=7 if name=="0716_2_STD.pdf p124"
replace districtid=8 if name=="0815_2_STD.pdf p5"
replace districtid=9 if name=="0933_2_STD.pdf p17"
replace districtid=9 if name=="0933_2_STD.pdf p27"
replace districtid=10 if name=="1008_2_STD.pdf p145"
replace districtid=10 if name=="1019_2_STD.pdf p41"
replace districtid=10 if name=="1028_2_STD.pdf p58"
replace districtid=10 if name=="1119_2_STD.pdf p23"
	*school id (can add in myself)
replace schoolid=104 if name=="0104_2_STD.pdf p27"
replace schoolid=104 if name=="0104_2_STD.pdf p38"
replace schoolid=104 if name=="0104_2_STD.pdf p70"
replace schoolid=104 if name=="0104_2_STD.pdf p74"
replace schoolid=104 if name=="0104_3_STD.pdf p27"
replace schoolid=104 if name=="0104_3_STD.pdf p53"
replace schoolid=104 if name=="0104_3_STD.pdf p57"
replace schoolid=122 if name=="0122_1_STD.pdf p548"
replace schoolid=134 if name=="0134_1_STD.pdf p120"
replace schoolid=135 if name=="0135_2_STD.pdf p113"
replace schoolid=135 if name=="0135_2_STD.pdf p94"
replace schoolid=135 if name=="0135_3_STD.pdf p2"
replace schoolid=135 if name=="0135_3_STD.pdf p49"
replace schoolid=135 if name=="0135_3_STD.pdf p72"
replace schoolid=408 if name=="0408_1_STD.pdf p113"
replace schoolid=412 if name=="0412_1_STD.pdf p117"
replace schoolid=414 if name=="0414_1_STD.pdf p4"
replace schoolid=416 if name=="0416_2_STD.pdf p23"
replace schoolid=417 if name=="0417_2_STD.pdf p144"
replace schoolid=417 if name=="0417_2_STD.pdf p212"
replace schoolid=417 if name=="0417_2_STD.pdf p29"
replace schoolid=419 if name=="0419_1_STD.pdf p118"
replace schoolid=441 if name=="0441_2_STD.pdf p48"
replace schoolid=514 if name=="0514_2_STD.pdf p17"
replace schoolid=611 if name=="0611_1_STD.pdf p7"
replace schoolid=708 if name=="0708_2_STD.pdf p116"
replace schoolid=708 if name=="0708_2_STD.pdf p152"
replace schoolid=809 if name=="0809_3_STD.pdf p11"
replace schoolid=909 if name=="0909_1_STD.pdf p84"
replace schoolid=915 if name=="0915_3_STD.pdf p5"
replace schoolid=933 if name=="0933_2_STD.pdf p17"
replace schoolid=933 if name=="0933_2_STD.pdf p27"
replace schoolid=1001 if name=="1001_2_STD.pdf p57"
replace schoolid=1008 if name=="1008_2_STD.pdf p145"
replace schoolid=1009 if name=="1009_1_STD.pdf p10"
replace schoolid=1009 if name=="1009_1_STD.pdf p63"
replace schoolid=1011 if name=="1011_2_STD.pdf p20"
replace schoolid=1019 if name=="1019_2_STD.pdf p41"
replace schoolid=1028 if name=="1028_2_STD.pdf p58"
replace schoolid=1119 if name=="1119_2_STD.pdf p23"
	*grade (should already have been added in, otherwise can add in myself)
		*none missing
	*gender (check with DCs)
	*stream (check with DCs)
	
*Clean Student IDs
	*checking blank IDs -- making sure we don't have that ID on file
replace studentid="01220501" if name=="0122_1_STD.pdf p265"
replace studentid="06101051" if name=="0610_2_STD.pdf p60"
replace studentid="10111023" if name=="1011_1_STD.pdf p47"
replace studentid="-999" if studentid=="--blank--"
	*check with Youdi that this is okay

	*check to see that IDs are all 8 digits, aside from -999
gen IDLength=strlen( studentid)
tab IDLength
	*Manually fixing all incorrect IDs:
replace studentid="-999" if name=="0108_1_STD.pdf p32"
replace studentid="-999" if name=="0108_1_STD.pdf p69"
replace studentid="-999" if name=="0123_3_STD.pdf p35"
replace studentid="02011025" if name=="0201_2_STD.pdf p62"
replace studentid="02032031" if name=="0203_2_STD.pdf p95"
replace studentid="03011055" if name=="0301_2_STD.pdf p67"
replace studentid="03151063" if name=="0315_1_STD.pdf p68"
replace studentid="04191231" if name=="0419_2_STD.pdf p255"
replace studentid="07161044" if name=="0716_2_STD.pdf p124"
replace studentid="07161147" if name=="0716_2_STD.pdf p126"
replace studentid="-999" if name=="0915_1_STD.pdf p31"
replace studentid="-999" if name=="0108_1_STD.pdf p32"
replace studentid="01350108" if studentid=="013500108"
replace studentid="01350102" if studentid=="013500102"
replace studentid="01350100" if studentid=="013500100"
replace studentid="02340004" if studentid=="023420004"
replace studentid="08191024" if studentid=="089191024"
replace studentid="10110123" if studentid=="100110123"
replace studentid="10350060" if studentid=="010350060"
replace studentid="01082003" if studentid=="0108200"
replace studentid="01021029" if studentid=="1021029"
replace studentid="01292004" if studentid=="0129004"
replace studentid="01311026" if studentid=="1211026"
replace studentid="01350009" if studentid=="0135009"
replace studentid="08012078" if studentid=="7012078"
replace studentid="09311065" if studentid=="0911065"
replace studentid="10200019" if studentid=="1020019"
replace studentid="10200049" if studentid=="1020049"
replace studentid="10242028" if studentid=="1024208"
replace studentid="11020066" if studentid=="1102006"

gen updatedIDLength=strlen( studentid)
tab updatedIDLength

replace studentid="0"+studentid if updatedIDLength==7

	*check that the 5th digit of student ID is a 0, 1, 2, or 3 (no other options)
gen FifthID=substr(studentid,5,1)
tab FifthID if (updatedIDLength!=4 | updatedIDLength!=9)
	*fixing issues:
replace studentid="04190006" if studentid=="00419006"
replace studentid="06101046" if studentid=="06404046"
replace studentid="06101081" if studentid=="06407081"
replace studentid="06101002" if studentid=="06404002"
replace studentid="06101047" if studentid=="06404047"
replace studentid="06101062" if studentid=="06103062"
replace studentid="09141045" if studentid=="00914045"

	*Create Dummy Variable for -999 Students
gen newstudent=1 if studentid=="-999"
replace newstudent=0 if studentid!="-999"

	*create student IDs for newly tested students
/*gen grade2014=0 if grade==1
replace grade2014=1 if grade==2
replace grade2014=2 if grade==3
replace studentid="99999999" if studentid=="-999"
sort schoolid grade2014 studentid
bys schoolid grade2014: gen studnum=_n
tostring studnum, format(%03.0f) generate(studnum_s)
tostring grade2014, generate(grade2014_s)
tostring schoolid, generate (schoolid_s)
gen newstudentid=schoolid_s+grade2014_s+studnum_s
isid newstudentid
replace studentid=newstudentid if studentid=="99999999"*/
	***WHAT ABOUT THE TWO STUDENTS AT 1009 WITH NO NAME?

	*check that student IDs are unique
		*dropping duplicates (scanned twice)
drop if name=="0104_1_STD.pdf p34"
drop if name=="0106_2_STD.pdf p27"
drop if name=="0108_2_STD.pdf p61"
drop if name=="0108_2_STD.pdf p48"
drop if name=="0122_1_STD.pdf p320"
drop if name=="0122_1_STD.pdf p277"
drop if name=="0122_1_STD.pdf p274"
drop if name=="0122_1_STD.pdf p285"
drop if name=="0122_1_STD.pdf p278"
drop if name=="0122_1_STD.pdf p284"
drop if name=="0122_1_STD.pdf p275"
drop if name=="0122_1_STD.pdf p270"
drop if name=="0122_1_STD.pdf p301"
drop if name=="0122_1_STD.pdf p283"
drop if name=="0122_1_STD.pdf p282"
drop if name=="0122_1_STD.pdf p232"
drop if name=="0122_1_STD.pdf p315"
drop if name=="0122_1_STD.pdf p273"
drop if name=="0122_1_STD.pdf p297"
drop if name=="0122_1_STD.pdf p325"
drop if name=="0122_1_STD.pdf p316"
drop if name=="0122_1_STD.pdf p313"
drop if name=="0122_1_STD.pdf p309"
drop if name=="0122_1_STD.pdf p317"
drop if name=="0122_1_STD.pdf p281"
drop if name=="0122_1_STD.pdf p318"
drop if name=="0122_1_STD.pdf p288"
drop if name=="0122_1_STD.pdf p321"
drop if name=="0122_1_STD.pdf p314"
drop if name=="0122_1_STD.pdf p302"
drop if name=="0122_1_STD.pdf p312"
drop if name=="0122_1_STD.pdf p289"
drop if name=="0122_1_STD.pdf p293"
drop if name=="0122_1_STD.pdf p300"
drop if name=="0122_1_STD.pdf p290"
drop if name=="0122_1_STD.pdf p299"
drop if name=="0122_1_STD.pdf p324"
drop if name=="0122_1_STD.pdf p326"
drop if name=="0122_1_STD.pdf p319"
drop if name=="0122_1_STD.pdf p322"
drop if name=="0122_1_STD.pdf p308"
drop if name=="0122_1_STD.pdf p294"
drop if name=="0122_1_STD.pdf p264"
drop if name=="0122_1_STD.pdf p272"
drop if name=="0122_1_STD.pdf p271"
drop if name=="0122_1_STD.pdf p287"
drop if name=="0122_1_STD.pdf p296"
drop if name=="0122_1_STD.pdf p452"
drop if name=="0122_1_STD.pdf p323"
drop if name=="0122_1_STD.pdf p310"
drop if name=="0122_1_STD.pdf p311"
drop if name=="0122_1_STD.pdf p291"
drop if name=="0122_1_STD.pdf p286"
drop if name=="0122_1_STD.pdf p292"
drop if name=="0122_1_STD.pdf p298"
drop if name=="0122_2_STD.pdf p14"
drop if name=="0122_2_STD.pdf p184"
drop if name=="0122_3_STD.pdf p3"
drop if name=="0122_3_STD.pdf p166"
drop if name=="0131_1_STD.pdf p101"
drop if name=="0131_2_STD.pdf p53"
drop if name=="0131_2_STD.pdf p107"
drop if name=="0134_3_STD.pdf p17"
replace studentid="01350076" if name=="0135_1_STD.pdf p78"
replace studentid="01350021" if name=="0135_1_STD.pdf p32"
replace studentid="01350036" if name=="0135_1_STD.pdf p117"
drop if name=="0135_1_STD.pdf p41"
drop if name=="0135_1_STD.pdf p43"
drop if name=="0135_1_STD.pdf p44"
replace studentid="01350131" if name=="0135_1_STD.pdf p57"
drop if name=="0135_3_STD.pdf p49"
replace studentid="02032110" if name=="0203_3_STD.pdf p7"
drop if name=="0402_2_STD.pdf p17"
drop if name=="0402_2_STD.pdf p18"
drop if name=="0402_2_STD.pdf p67"
drop if name=="0416_2_STD.pdf p142"
drop if name=="0416_3_STD.pdf p164"
drop if name=="0416_3_STD.pdf p61"
drop if name=="0417_2_STD.pdf p158"
replace studentid="04172066" if name=="0417_2_STD.pdf p140"
drop if name=="0419_2_STD.pdf p191"
drop if name=="0441_1_STD.pdf p31"
drop if name=="0514_2_STD.pdf p157"
replace studentid="06102084" if name=="0610_2_STD.pdf p112"
drop if name=="0702_2_STD.pdf p41"
drop if name=="0706_1_STD.pdf p79"
drop if name=="0706_1_STD.pdf p77"
drop if name=="0706_1_STD.pdf p76"
drop if name=="0706_1_STD.pdf p80"
drop if name=="0706_1_STD.pdf p78"
drop if name=="0706_1_STD.pdf p74"
drop if name=="0706_2_STD.pdf p84"
drop if name=="0708_1_STD.pdf p17"
drop if name=="0708_2_STD.pdf p147"
drop if name=="0708_2_STD.pdf p144"
drop if name=="0708_2_STD.pdf p136"
drop if name=="0708_2_STD.pdf p159"
drop if name=="0708_2_STD.pdf p129"
drop if name=="0708_2_STD.pdf p154"
drop if name=="0708_2_STD.pdf p131"
drop if name=="0708_2_STD.pdf p140"
drop if name=="0708_2_STD.pdf p125"
drop if name=="0708_2_STD.pdf p3"
drop if name=="0708_2_STD.pdf p132"
drop if name=="0708_2_STD.pdf p155"
drop if name=="0708_2_STD.pdf p139"
drop if name=="0708_2_STD.pdf p135"
drop if name=="0708_2_STD.pdf p151"
drop if name=="0708_2_STD.pdf p124"
drop if name=="0708_2_STD.pdf p150"
drop if name=="0708_2_STD.pdf p156"
drop if name=="0708_2_STD.pdf p127"
drop if name=="0708_2_STD.pdf p142"
drop if name=="0708_2_STD.pdf p148"
drop if name=="0708_2_STD.pdf p138"
drop if name=="0708_2_STD.pdf p157"
drop if name=="0708_2_STD.pdf p160"
drop if name=="0708_2_STD.pdf p143"
drop if name=="0708_2_STD.pdf p128"
drop if name=="0708_2_STD.pdf p158"
drop if name=="0708_3_STD.pdf p179"
drop if name=="0708_3_STD.pdf p13"
drop if name=="0713_2_STD.pdf p117"
drop if name=="0713_2_STD.pdf p110"
drop if name=="0713_2_STD.pdf p116"
drop if name=="0713_2_STD.pdf p109"
drop if name=="0713_2_STD.pdf p111"
drop if name=="0713_2_STD.pdf p115"
drop if name=="0713_2_STD.pdf p112"
drop if name=="0713_2_STD.pdf p114"
drop if name=="0713_2_STD.pdf p113"
drop if name=="0713_3_STD.pdf p33"
drop if name=="0716_2_STD.pdf p2"
drop if name=="0719_2_STD.pdf p79"
drop if name=="0719_2_STD.pdf p65"
drop if name=="0719_2_STD.pdf p98"
drop if name=="0719_2_STD.pdf p68"
drop if name=="0719_2_STD.pdf p52"
replace studentid="07232134" if name=="0723_2_STD.pdf p73"
replace studentid="07232086" if name=="0723_2_STD.pdf p61"
drop if name=="0732_1_STD.pdf p153"
drop if name=="0732_1_STD.pdf p138"
replace studentid="07232032" if name=="0723_2_STD.pdf p46"
replace studentid="08092016" if name=="0809_3_STD.pdf p1"
drop if name=="0908_2_STD.pdf p4"
drop if name=="0908_2_STD.pdf p10"
drop if name=="0908_2_STD.pdf p2"
drop if name=="0908_2_STD.pdf p19"
drop if name=="0908_2_STD.pdf p18"
drop if name=="0908_2_STD.pdf p20"
drop if name=="0908_2_STD.pdf p21"
drop if name=="0931_2_STD.pdf p27"
drop if name=="1009_3_STD.pdf p74"
drop if name=="1009_3_STD.pdf p75"
drop if name=="1032_2_STD.pdf p6"

drop if name=="0412_3_STD.pdf p47"
replace studentid="-999" if name=="0732_3_STD.pdf p50"

drop if name=="0228_2_STD.pdf p60"
drop if name=="0610_3_STD.pdf p47"
drop if name=="0610_3_STD.pdf p42"
drop if name=="0628_2_STD.pdf p33"
drop if name=="0723_3_STD.pdf p63"
drop if name=="0732_3_STD.pdf p16"
drop if name=="1005_2_STD.pdf p23"

	*creating duplicate types
	*type 1
gen type1duplicate=1 if name=="0106_3_STD.pdf p2"
replace type1duplicate=1 if name=="0106_3_STD.pdf p87"
replace type1duplicate=1 if name=="0108_2_STD.pdf p28"
replace type1duplicate=1 if name=="0108_2_STD.pdf p37"
replace type1duplicate=1 if name=="0719_2_STD.pdf p110"
replace type1duplicate=1 if name=="0719_2_STD.pdf p111"
replace type1duplicate=1 if name=="0915_2_STD.pdf p150"
replace type1duplicate=1 if name=="0915_2_STD.pdf p90"
replace type1duplicate=1 if name=="1011_3_STD.pdf p37"
replace type1duplicate=1 if name=="1011_3_STD.pdf p50"
replace type1duplicate=0 if type1duplicate!=1

**MR code to fix student IDS duplicates type1duplicate==1***
bys studentid: gen rand = runiform() if type1duplicate==1
sort studentid rand 
egen lrank = rank(_n) if type1duplicate==1, by(studentid)
drop if lrank>=2 & !missing(lrank)
drop lrank rand

	*type 2
gen type2duplicate=1 if name=="0102_1_STD.pdf p44"
replace type2duplicate=1 if name=="0102_1_STD.pdf p89"
replace type2duplicate=1 if name=="0106_2_STD.pdf p32"
replace type2duplicate=1 if name=="0106_2_STD.pdf p72"
replace type2duplicate=1 if name=="0106_3_STD.pdf p65"
replace type2duplicate=1 if name=="0106_3_STD.pdf p67"
replace type2duplicate=1 if name=="0107_1_STD.pdf p112"
replace type2duplicate=1 if name=="0107_1_STD.pdf p79"
replace type2duplicate=1 if name=="0108_3_STD.pdf p2"
replace type2duplicate=1 if name=="0108_3_STD.pdf p43"
replace type2duplicate=1 if name=="0129_3_STD.pdf p1"
replace type2duplicate=1 if name=="0129_3_STD.pdf p21"
replace type2duplicate=1 if name=="0326_2_STD.pdf p30"
replace type2duplicate=1 if name=="0326_2_STD.pdf p45"
replace type2duplicate=1 if name=="0414_1_STD.pdf p4"
replace type2duplicate=1 if name=="0414_1_STD.pdf p86"
replace type2duplicate=1 if name=="0525_1_STD.pdf p12"
replace type2duplicate=1 if name=="0525_1_STD.pdf p14"
replace type2duplicate=1 if name=="0603_2_STD.pdf p55"
replace type2duplicate=1 if name=="0603_2_STD.pdf p96"
replace type2duplicate=1 if name=="0719_2_STD.pdf p109"
replace type2duplicate=1 if name=="0719_2_STD.pdf p99"
replace type2duplicate=1 if name=="0723_2_STD.pdf p67"
replace type2duplicate=1 if name=="0723_2_STD.pdf p92"
replace type2duplicate=1 if name=="0723_2_STD.pdf p38"
replace type2duplicate=1 if name=="0723_3_STD.pdf p69"
replace type2duplicate=1 if name=="0723_2_STD.pdf p20"
replace type2duplicate=1 if name=="0723_3_STD.pdf p4"
replace type2duplicate=1 if name=="0917_1_STD.pdf p119"
replace type2duplicate=1 if name=="0917_1_STD.pdf p70"
replace type2duplicate=1 if name=="0933_3_STD.pdf p5"
replace type2duplicate=1 if name=="0933_3_STD.pdf p63"
replace type2duplicate=1 if name=="1120_1_STD.pdf p115"
replace type2duplicate=1 if name=="1120_1_STD.pdf p116"
replace type2duplicate=0 if type2duplicate!=1
	**MR code to fix student IDS duplicates type2duplicate==2***
bys studentid: gen rand = runiform() if type2duplicate==1
sort studentid rand 
egen lrank = rank(_n) if type2duplicate==1, by(studentid)
gen sub1=substr(studentid,1,5)
gen sub2=substr(studentid,6,1)
destring sub2, replace
replace sub2=sub2+8 if lrank==2 
replace sub2=sub2+9 if lrank==3 
tostring sub2, replace
gen sub3=substr(studentid,7,.)
replace studentid=sub1+sub2+sub3 if lrank==2
replace stuname=stuname+" 2" if lrank==2
replace studentid=sub1+sub2+sub3 if lrank==3
replace stuname=stuname+" 3" if lrank==3
drop lrank rand sub1 sub2 sub3	

*merge in missing 2014 students
merge m:m stuname schoolid using "C:\Users\jmahoney\Desktop\Stata\Intervention Reclink\Reclink Missing 2014 Students_STADICONTROL_linked.dta", keepus (stuname stuname_2014 studentid_reclink)
drop if _merge==2
replace studentid=studentid_reclink if _merge==3
drop _merge

*Create Dummy Variable if student was held back this year
egen MissingGrd1=rownonmiss(grd1_*)
egen MissingGrd2=rownonmiss(grd2_*)
egen MissingGrd3=rownonmiss(grd3_*)

gen heldback=1 if ((FifthID=="0" & MissingGrd1==0) | (FifthID=="1" & MissingGrd2==0) | (FifthID=="2" & MissingGrd3==0)) & studentid!="-999"
replace heldback=0 if heldback!=1 & studentid!="-999"

*Clean Volunteer Names (so that MR can run Volunteer Fixed Effects Checks)
	*(no)
	
gen tested2015=1

save "EL2015_Student_FinalRun_StadiandControl_CLEAN.dta", replace
