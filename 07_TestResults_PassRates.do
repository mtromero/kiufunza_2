**********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
fvset base default treatarm 
global treatmentlist TreatmentLevels TreatmentGains
gen TreatmentLevels2=TreatmentLevels
gen TreatmentGains2=TreatmentGains
global treatmentlist2 TreatmentLevels2 TreatmentGains2
label var TreatmentLevels "Levels (\$\beta_1\$)"
label var TreatmentGains "P4Pctile (\$\beta_2\$)"
keep ${schoolcontrol} *T3 *T6  SchoolID DistrictID StrataScore treat* Treat* upid studentid
keep ${schoolcontrol} *Pass* Grade* SchoolID DistrictID StrataScore treat* Treat* upid studentid
gen ID=_n

global student2 shoes_T2 socks_T2 dirty_T2 uniformdirty_T2 uniformtorn_T2  ringworm_T2 CloseToeShoe_T2


 

eststo clear
foreach var in Kis_Silabi_Pass_T3 Kis_Maneno_Pass_T3 Kis_Sentenci_Pass_T3 Kis_Aya_Pass_T3 Kis_Story_Pass_T3 Kis_Comp_Pass_T3 {
	eststo: reghdfe `var' $treatmentlist i.Grade_T3 ${schoolcontrol}, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
	estadd ysumm
	sum `var' if treatment2=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
}
esttab  using "$latexcodes/Pass_Kis_T3.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc %9.2gc) labels("N. of obs." "Control mean" "\$\beta_3 = \beta_2-\beta_1\$" "p-value (\$H_0:\beta_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)


eststo clear
foreach var in Eng_Letter_Pass_T3 Eng_Word_Pass_T3 Eng_Sentences_Pass_T3 Eng_Paragraph_Pass_T3 Eng_Story_Pass_T3 Eng_Comp_Pass_T3 {
	eststo: reghdfe `var' $treatmentlist i.Grade_T3 ${schoolcontrol}, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
	estadd ysumm
	sum `var' if treatment2=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
}
esttab  using "$latexcodes/Pass_Eng_T3.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc %9.2gc) labels("N. of obs." "Control mean" "\$\beta_3 = \beta_2-\beta_1\$" "p-value (\$H_0:\beta_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

eststo clear
foreach var in Math_id_Pass_T3 Math_uta_Pass_T3 Math_bwa_Pass_T3 Math_j_Pass_T3 Math_t_Pass_T3 Math_z_Pass_T3 Math_g_Pass_T3 {
	eststo: reghdfe `var' $treatmentlist i.Grade_T3 ${schoolcontrol}, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
	estadd ysumm
	sum `var' if treatment2=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
}
esttab  using "$latexcodes/Pass_Math_T3.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc %9.2gc) labels("N. of obs." "Control mean" "\$\beta_3 = \beta_2-\beta_1\$" "p-value (\$H_0:\beta_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)







eststo clear
foreach var in Kis_Silabi_Pass_T6 Kis_Maneno_Pass_T6 Kis_Sentenci_Pass_T6 Kis_Aya_Pass_T6 Kis_Story_Pass_T6 Kis_Comp_Pass_T6 {
	eststo: reghdfe `var' $treatmentlist i.Grade_T6 ${schoolcontrol}, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
	estadd ysumm
	sum `var' if treatment2=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
}
esttab  using "$latexcodes/Pass_Kis_T6.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc %9.2gc) labels("N. of obs." "Control mean" "\$\beta_3 = \beta_2-\beta_1\$" "p-value (\$H_0:\beta_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

foreach var in Eng_Letter_Pass_T6 Eng_Word_Pass_T6 Eng_Sentences_Pass_T6 Eng_Paragraph_Pass_T6{
	gen `var'=0
}
eststo clear
foreach var in Eng_Letter_Pass_T6 Eng_Word_Pass_T6 Eng_Sentences_Pass_T6 Eng_Paragraph_Pass_T6  {
	eststo: reghdfe `var'  i.Grade_T3 ${schoolcontrol}, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
	estadd ysumm
	estadd scalar ymean2=.
	estadd scalar N=0, replace
}

foreach var in  Eng_Story_Pass_T6 Eng_Comp_Pass_T6 {
	eststo: reghdfe `var' $treatmentlist i.Grade_T3 ${schoolcontrol}, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
	estadd ysumm
	sum `var' if treatment2=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
}
esttab  using "$latexcodes/Pass_Eng_T6.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc %9.2gc) labels("N. of obs." "Control mean" "\$\beta_3 = \beta_2-\beta_1\$" "p-value (\$H_0:\beta_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

eststo clear
foreach var in Math_id_Pass_T6 Math_uta_Pass_T6 Math_bwa_Pass_T6 Math_j_Pass_T6 Math_t_Pass_T6 Math_z_Pass_T6 Math_g_Pass_T6 {
	eststo: reghdfe `var' $treatmentlist i.Grade_T6 ${schoolcontrol}, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
	estadd ysumm
	sum `var' if treatment2=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
}
esttab  using "$latexcodes/Pass_Math_T6.tex", se ar2 booktabs label b(%9.3gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc %9.2gc) labels("N. of obs." "Control mean" "\$\beta_3 = \beta_2-\beta_1\$" "p-value (\$H_0:\beta_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)



foreach var of varlist Eng_Letter_Pass_T6 Eng_Word_Pass_T6 Eng_Sentences_Pass_T6 Eng_Paragraph_Pass_T6{
	replace `var'=.
}

reshape long Kis_Silabi_Pass_@ Kis_Maneno_Pass_@ Kis_Sentenci_Pass_@ Kis_Aya_Pass_@ Kis_Story_Pass_@ Kis_Comp_Pass_@ Eng_Letter_Pass_@ Eng_Word_Pass_@ Eng_Sentences_Pass_@ Eng_Paragraph_Pass_@ Eng_Story_Pass_@ Eng_Comp_Pass_@ Math_id_Pass_@ Math_uta_Pass_@ Math_bwa_Pass_@ Math_j_Pass_@ Math_t_Pass_@ Math_z_Pass_@ Math_g_Pass_@, i(ID) j(time) string
reshape long Kis_@_Pass_ Eng_@_Pass_ Math_@_Pass_ , i(ID time) j(skill) string

encode skill, gen(skill2)
encode time, gen(time2)

eststo clear
foreach time in T3 T6{
	foreach var in Math__Pass_ Kis__Pass_ Eng__Pass_{
		eststo: reghdfe `var' $treatmentlist i.Grade_`time' ${schoolcontrol} if time=="`time'", vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm##skill2)
		estadd ysumm
		sum `var' if treatment2=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	}
}

esttab  using "$latexcodes/Pass_Pooled.tex", se ar2 booktabs label b(%9.3gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc %9.2gc) labels("N. of obs." "Control mean" "\$\beta_3 = \beta_2-\beta_1\$" "p-value (\$H_0:\beta_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)
