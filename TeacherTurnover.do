use "$basein/9 Baseline 2015/Final Data/Teacher/R7TGrdSubGrp_noPII.dta", clear
keep if tchgrpyn==1
keep SchoolID R7TeacherID R7TGradeID R7TGrdSub2ID tchgrpyn
collapse (max) tchgrpyn, by(SchoolID R7TeacherID)
rename tchgrpyn R7Focal
tempfile R7file
save `R7file', replace

use "$basein/11 Endline 2015/Final Data/Teacher Data/R8TGrdSubGrp_nopii.dta", clear
keep if tchgrpyn==1
keep SchoolID R8TeacherID R8TGradeID R8TGrdSub2ID tchgrpyn
collapse (max) tchgrpyn, by(SchoolID R8TeacherID)
rename tchgrpyn R8Focal
tempfile R8file
save `R8file', replace
 
use "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10TGrdSubGrp_nopii.dta", clear
keep if tchgrpyn==1
keep SchoolID R10TeacherID R10TGradeID R10TGrdSub2ID tchgrpyn
collapse (max) tchgrpyn, by(SchoolID R10TeacherID)
rename tchgrpyn R10Focal
tempfile R10file
save `R10file', replace

   
use "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", clear
mmerge  SchoolID R7TeacherID using `R7file', type(n:1)  missing(nomatch) 
drop if _merge==2 
drop _merge
mmerge  SchoolID R8TeacherID using `R8file', type(n:1)  missing(nomatch) 
drop if _merge==2 
drop _merge
mmerge  SchoolID R10TeacherID using `R10file', type(n:1)  missing(nomatch) 
drop if _merge==2 
drop _merge
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3

gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "Gains" 

recode R8Focal R10Focal R7Focal (.=0) 
drop if R7Focal==0 & R10Focal==0 & R8Focal==0
drop if R7Focal==0

eststo clear
eststo: reghdfe R8Focal $treatmentlist, vce(cluster SchoolID) ab(DistrictID#StrataScore#treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
sum R8Focal if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] =_b[TreatmentLevels]=0)

eststo: reghdfe R10Focal $treatmentlist, vce(cluster SchoolID) ab(DistrictID#StrataScore#treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
sum R10Focal if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] =_b[TreatmentLevels]=0)

label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

esttab  using "$latexcodes/TeacherTurnover.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)




*************************************
***** TAKE AWAY ENGLISH *************
*************************************

use "$basein/9 Baseline 2015/Final Data/Teacher/R7TGrdSubGrp_noPII.dta", clear
keep if tchgrpyn==1
keep SchoolID R7TeacherID R7TGradeID R7TGrdSub2ID tchgrpyn
drop if R7TGrdSub2ID==3
collapse (max) tchgrpyn, by(SchoolID R7TeacherID)
rename tchgrpyn R7Focal
tempfile R7file
save `R7file', replace

use "$basein/11 Endline 2015/Final Data/Teacher Data/R8TGrdSubGrp_nopii.dta", clear
keep if tchgrpyn==1
keep SchoolID R8TeacherID R8TGradeID R8TGrdSub2ID tchgrpyn
drop if R8TGrdSub2ID==3
collapse (max) tchgrpyn, by(SchoolID R8TeacherID)
rename tchgrpyn R8Focal
tempfile R8file
save `R8file', replace
 
use "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10TGrdSubGrp_nopii.dta", clear
keep if tchgrpyn==1
keep SchoolID R10TeacherID R10TGradeID R10TGrdSub2ID tchgrpyn
drop if R10TGrdSub2ID==3
collapse (max) tchgrpyn, by(SchoolID R10TeacherID)
rename tchgrpyn R10Focal
tempfile R10file
save `R10file', replace

   
use "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", clear
mmerge  SchoolID R7TeacherID using `R7file', type(n:1)  missing(nomatch) 
drop if _merge==2 
drop _merge
mmerge  SchoolID R8TeacherID using `R8file', type(n:1)  missing(nomatch) 
drop if _merge==2 
drop _merge
mmerge  SchoolID R10TeacherID using `R10file', type(n:1)  missing(nomatch) 
drop if _merge==2 
drop _merge
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3

gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "Gains" 

recode R8Focal R10Focal R7Focal (.=0) 
drop if R7Focal==0 & R10Focal==0 & R8Focal==0
drop if R7Focal==0

eststo clear
eststo: reghdfe R8Focal $treatmentlist, vce(cluster SchoolID) ab(DistrictID#StrataScore#treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
sum R8Focal if treatment2=="Control"
estadd scalar ymean2=r(mean)

eststo: reghdfe R10Focal $treatmentlist, vce(cluster SchoolID) ab(DistrictID#StrataScore#treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
sum R10Focal if treatment2=="Control"
estadd scalar ymean2=r(mean)

label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

esttab  using "$latexcodes/TeacherTurnover_NoEnglish.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "Gains-Levels \$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)
