
use using "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd3_Payments.dta", clear
gen v=1
append using "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd3_Payments_v2.dta"
replace v=2 if v==.

reshape wide Sudents Payment_Total_Kis- Rank_Math, i(SchoolID grade stream) j(v)


gen DiffKis=Payment_Total_Kis2-Payment_Total_Kis1
gen DiffEng=Payment_Total_Eng2-Payment_Total_Eng1
gen DiffMath=Payment_Total_Math2-Payment_Total_Math1


save "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd3_DiffPayments.dta", replace
