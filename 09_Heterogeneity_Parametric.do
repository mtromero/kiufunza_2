**********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear


global AggregateDep 	Z_hisabati Z_kiswahili Z_kiingereza Z_sayansi Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal /*this should be added in the future...*/
global AggregateDep_Karthik 	Z_hisabati Z_kiswahili Z_kiingereza   /*Z_kiingereza  Z_ScoreFocal this should be added in the future...*/
global AggregateDep_int 	Z_hisabati Z_kiswahili Z_kiingereza
 
global AggregateDep_NonEng 	Z_hisabati Z_kiswahili 
global AggregateDep_Eng 	Z_kiingereza

global treatmentlist TreatmentLevels TreatmentGains
gen TreatmentLevels2=TreatmentLevels
gen TreatmentGains2=TreatmentGains
global treatmentlist2 TreatmentLevels2 TreatmentGains2
*enrollment2015_T1 Rural_T1 ClassesOutside_T1 Electricity_T1
global schoolcontrol  PTR_T1  SingleShift_T1 DistanceIndex_T1 InfrastructureIndex_T1
global studentcontrol c.LagseenUwezoTests#c.MissingseenUwezoTests MissingseenUwezoTests c.LagpreSchoolYN#MissingpreSchoolYN MissingpreSchoolYN ///
c.LagGender#MissingGender MissingGender c.LagAge#c.MissingAge MissingAge ///
c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza)##c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza) ///
MissingZ_kiswahili MissingZ_hisabati MissingZ_kiingereza
 
      
*c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza)##c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza) ///




global student2 shoes_T2 socks_T2 dirty_T2 uniformdirty_T2 uniformtorn_T2  ringworm_T2 CloseToeShoe_T2

recode bookshome_T2 (-98=.)
global studentoutcomes studentsfight_T2 sing_T2 bookshome_T2

bys SchoolID: egen Z_kiswahili_T0_G=mean(Z_kiswahili_T0)
bys SchoolID: egen Z_kiingereza_T0_G=mean(Z_kiingereza_T0)
bys SchoolID: egen Z_hisabati_T0_G=mean(Z_hisabati_T0)
pca Z_hisabati_T0  Z_kiingereza_T0 Z_kiswahili_T0
predict Z_ScoreFocal_T0, score
*First lets create the tables Karthik Wants

label var Z_kiswahili_T2 Swahili
label var Z_kiswahili_T5 Swahili
label var Z_kiingereza_T2 English
label var Z_kiingereza_T5 English
label var Z_hisabati_T2 Math
label var Z_hisabati_T5 Math
label var Z_ScoreFocal_T2 "Focal Subjects"
label var Z_ScoreFocal_T5 "Focal Subjects"

gen MissingZ_ScoreKisawMath=!missing(LagZ_ScoreKisawMath)

foreach var in Z_hisabati Z_kiswahili Z_ScoreKisawMath Z_ScoreFocal{
	egen Decile_`var' = fastxtile(Lag`var') if Missing`var'==1, by(LagGrade) nq(5)
	replace Decile_`var'=0 if Missing`var'==0
}

reghdfe Z_hisabati_T2 c.(${treatmentlist})#i.Decile_Z_hisabati i.Decile_Z_hisabati i.LagGrade $studentcontrol $schoolcontrol if GradeID_T2<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)


foreach time in T2 T5{

	foreach var in $AggregateDep_NonEng Z_ScoreKisawMath Z_ScoreFocal {
		capture drop Decile
	    gen Decile=Decile_`var'			
		if "`var'"=="Z_kiingereza" & "`time'"=="T2"{
			reghdfe `var'_`time' c.(${treatmentlist})#i.Decile i.Decile i.LagGrade $studentcontrol $schoolcontrol if GradeID_`time'==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
		}
		if "`var'"=="Z_kiingereza" & "`time'"=="T5"{
			reghdfe `var'_`time' c.(${treatmentlist})#i.Decile i.Decile i.LagGrade $studentcontrol $schoolcontrol if GradeID_`time'==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
		}
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_`time' c.(${treatmentlist})#i.Decile i.Decile i.LagGrade $studentcontrol $schoolcontrol if GradeID_`time'<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
		}
		test (_b[1.Decile#TreatmentGains] - _b[5.Decile#TreatmentGains]=0)
		local pGains=string(r(p), "%9.2gc")
		test (_b[1.Decile#TreatmentLevels] - _b[5.Decile#TreatmentLevels]=0)
		local pLevels=string(r(p), "%9.2gc")
		
		
		if "`var'"=="Z_kiingereza" & "`time'"=="T5"{ 
			test (_b[1.Decile#c.TreatmentLevels]=_b[2.Decile#c.TreatmentLevels]= _b[4.Decile#c.TreatmentLevels]= _b[5.Decile#c.TreatmentLevels])
		}
		else{
			test (_b[1.Decile#c.TreatmentLevels]=_b[2.Decile#c.TreatmentLevels]= _b[3.Decile#c.TreatmentLevels]= _b[4.Decile#c.TreatmentLevels]= _b[5.Decile#c.TreatmentLevels])
		}
		local pLevels_all=string(r(p), "%9.2gc")
		
		if "`var'"=="Z_kiingereza" & "`time'"=="T5"{
			test (_b[1.Decile#c.TreatmentGains]=_b[2.Decile#c.TreatmentGains]= _b[4.Decile#c.TreatmentGains]= _b[5.Decile#c.TreatmentGains])
		}
		else{
			test (_b[1.Decile#c.TreatmentGains]=_b[2.Decile#c.TreatmentGains]= _b[3.Decile#c.TreatmentGains]= _b[4.Decile#c.TreatmentGains]= _b[5.Decile#c.TreatmentGains])
			}
		local pGains_all=string(r(p), "%9.2gc")
		
		if "`var'"=="Z_kiingereza" & "`time'"=="T5"{
		coefplot, graphregion(color(white)) baselevels keep(*c.TreatmentLevels*) drop(0.Decile#c.TreatmentLevels) ci ///
				rename(0.Decile#c.TreatmentLevels="NA" 1.Decile#c.TreatmentLevels="1" 2.Decile#c.TreatmentLevels="2" 3.Decile#c.TreatmentLevels="3" 4.Decile#c.TreatmentLevels="4" ///
				5.Decile#c.TreatmentLevels="5"  /* 6.Decile#c.TreatmentLevels="6" 7.Decile#c.TreatmentLevels="7" 8.Decile#c.TreatmentLevels="8" 9.Decile#c.TreatmentLevels="9" 10.Decile#c.TreatmentLevels="10" */ ) ///
				levels(95 90) ciopts(recast(rcap)) graphregion(color(white)) ///
				vertical yline(0) ylab(, ang(h) nogrid labsize(large) labgap(-6)) xlab(,labsize(large)) xtitle("Quintile",size(large)) ytitle("Treatment effect",size(large)) title("Levels",size(large)) saving(Levels,replace) ///
				note("p-value(H{sub:0}:Q{sub:1}=Q{sub:5})= `pLevels'" "p-value(H{sub:0}:Q{sub:1}=Q{sub:2}=Q{sub:4}=Q{sub:5})= `pLevels_all'",size(large)) yscale(range(-0.5 0.5)) ylabel(-0.5(0.25)0.5) ///
				
				*graph export "$graphs/HeteroBaseline_`kind'_`subject'_`time'.pdf", replace
				
		coefplot, graphregion(color(white)) baselevels keep(*c.TreatmentGains*) ci drop(0.Decile#c.TreatmentGains) ///
				rename(0.Decile#c.TreatmentGains="NA" 1.Decile#c.TreatmentGains="1" 2.Decile#c.TreatmentGains="2" 3.Decile#c.TreatmentGains="3" 4.Decile#c.TreatmentGains="4" ///
				5.Decile#c.TreatmentGains="5" /*6.Decile#c.TreatmentGains="6" 7.Decile#c.TreatmentGains="7" 8.Decile#c.TreatmentGains="8" 9.Decile#c.TreatmentGains="9" 10.Decile#c.TreatmentGains="10"*/ )   ///
				levels(95 90) ciopts(recast(rcap)) graphregion(color(white)) ///
				vertical yline(0) ylab(, ang(h) nogrid labsize(large) labgap(-6)) xlab(,labsize(large)) xtitle("Quintile",size(large)) ytitle("Treatment effect",size(large)) title("P4Pctile",size(large)) saving(Gains,replace) ///
				note("p-value(H{sub:0}:Q{sub:1}=Q{sub:5})= `pGains'" "p-value(H{sub:0}:Q{sub:1}=Q{sub:2}=Q{sub:4}=Q{sub:5})= `pGains_all'",size(large)) yscale(range(-0.5 0.5)) ylabel(-0.5(0.25)0.5) 
				*graph export "$graphs/HeteroBaseline_`kind'_`subject'_`time'.pdf", replace
		}
		else{
		coefplot, graphregion(color(white)) baselevels keep(*c.TreatmentLevels*) drop(0.Decile#c.TreatmentLevels) ci ///
				rename(0.Decile#c.TreatmentLevels="NA" 1.Decile#c.TreatmentLevels="1" 2.Decile#c.TreatmentLevels="2" 3.Decile#c.TreatmentLevels="3" 4.Decile#c.TreatmentLevels="4" ///
				5.Decile#c.TreatmentLevels="5"  /* 6.Decile#c.TreatmentLevels="6" 7.Decile#c.TreatmentLevels="7" 8.Decile#c.TreatmentLevels="8" 9.Decile#c.TreatmentLevels="9" 10.Decile#c.TreatmentLevels="10" */ ) ///
				levels(95 90) ciopts(recast(rcap)) graphregion(color(white)) ///
				vertical yline(0) ylab(, ang(h) nogrid labsize(large) labgap(-6)) xlab(,labsize(large)) xtitle("Quintile",size(large)) ytitle("Treatment effect",size(large)) title("Levels",size(large)) saving(Levels,replace) ///
				note("p-value(H{sub:0}:Q{sub:1}=Q{sub:5})= `pLevels'" "p-value(H{sub:0}:Q{sub:1}=Q{sub:2}=Q{sub:3}=Q{sub:4}=Q{sub:5})= `pLevels_all'",size(large)) yscale(range(-0.5 0.5)) ylabel(-0.5(0.25)0.5) ///
				
				*graph export "$graphs/HeteroBaseline_`kind'_`subject'_`time'.pdf", replace
				
		coefplot, graphregion(color(white)) baselevels keep(*c.TreatmentGains*) ci drop(0.Decile#c.TreatmentGains) ///
				rename(0.Decile#c.TreatmentGains="NA" 1.Decile#c.TreatmentGains="1" 2.Decile#c.TreatmentGains="2" 3.Decile#c.TreatmentGains="3" 4.Decile#c.TreatmentGains="4" ///
				5.Decile#c.TreatmentGains="5" /*6.Decile#c.TreatmentGains="6" 7.Decile#c.TreatmentGains="7" 8.Decile#c.TreatmentGains="8" 9.Decile#c.TreatmentGains="9" 10.Decile#c.TreatmentGains="10"*/ )   ///
				levels(95 90) ciopts(recast(rcap)) graphregion(color(white)) ///
				vertical yline(0) ylab(, ang(h) nogrid labsize(large) labgap(-6)) xlab(,labsize(large)) xtitle("Quintile",size(large)) ytitle("Treatment effect",size(large)) title("P4Pctile",size(large)) saving(Gains,replace) ///
				note("p-value(H{sub:0}:Q{sub:1}=Q{sub:5})= `pGains'" "p-value(H{sub:0}:Q{sub:1}=Q{sub:2}=Q{sub:3}=Q{sub:4}=Q{sub:5})= `pGains_all'",size(large)) yscale(range(-0.5 0.5)) ylabel(-0.5(0.25)0.5) 
				*graph export "$graphs/HeteroBaseline_`kind'_`subject'_`time'.pdf", replace
		}
				
		gr combine Levels.gph Gains.gph, graphregion(color(white))
		graph export "$graphs/HeteroQuintile_`var'_`time'.pdf", replace
			
		
	}  
}

keep $treatmentlist LagGrade LagseenUwezoTests MissingseenUwezoTests LagpreSchoolYN MissingpreSchoolYN LagGender MissingGender LagAge MissingAge MissingZ_ScoreKisawMath LagZ_ScoreKisawMath LagZ_kiswahili LagZ_hisabati LagZ_kiingereza MissingZ_kiswahili MissingZ_hisabati MissingZ_kiingereza $schoolcontrol GradeID_* Decile_*  SchoolID DistrictID StrataScore treatarm Z_ScoreKisawMath* Z_hisabati* Z_kiswahili* Z_kiingereza* upid
drop if upid==""
reshape long Z_hisabati@ Z_kiswahili@ Z_kiingereza@ Z_ScoreKisawMath@ GradeID@, i(upid) j(time) string
replace Z_kiingereza=. if GradeID<=2
encode time, gen(time2)
	
foreach var in Z_hisabati Z_kiswahili Z_ScoreKisawMath {
	capture drop Decile
	gen Decile=Decile_`var'			
	reghdfe `var' c.(${treatmentlist})#i.Decile i.Decile##i.time2 time2##(i.LagGrade c.($studentcontrol ${schoolcontrol})) if GradeID<=3 & (time=="_T2" | time=="_T5"), vce(cluster SchoolID) ab(time2##DistrictID##StrataScore##treatarm)
	test (_b[1.Decile#TreatmentGains] - _b[5.Decile#TreatmentGains]=0)
	local pGains=string(r(p), "%9.2gc")
	test (_b[1.Decile#TreatmentLevels] - _b[5.Decile#TreatmentLevels]=0)
	local pLevels=string(r(p), "%9.2gc")
	
	
	coefplot, graphregion(color(white)) baselevels keep(*c.TreatmentLevels*) drop(0.Decile#c.TreatmentLevels) ci ///
			rename(0.Decile#c.TreatmentLevels="NA" 1.Decile#c.TreatmentLevels="1" 2.Decile#c.TreatmentLevels="2" 3.Decile#c.TreatmentLevels="3" 4.Decile#c.TreatmentLevels="4" ///
			5.Decile#c.TreatmentLevels="5"  /* 6.Decile#c.TreatmentLevels="6" 7.Decile#c.TreatmentLevels="7" 8.Decile#c.TreatmentLevels="8" 9.Decile#c.TreatmentLevels="9" 10.Decile#c.TreatmentLevels="10" */ ) ///
			levels(95 90) ciopts(recast(rcap)) graphregion(color(white)) ///
			vertical yline(0) ylab(, ang(h) nogrid labsize(large)) xlab(,labsize(large)) xtitle("Quintile") ytitle("Treatment effect") title("Levels") saving(Levels,replace) ///
			note("p-value(H{sub:0}:Q{sub:1}=Q{sub:5})= `pLevels'",size(large)) yscale(range(-0.4 0.4)) ylabel(-0.4(0.2)0.4) 
			*graph export "$graphs/HeteroBaseline_`kind'_`subject'_`time'.pdf", replace
			
	coefplot, graphregion(color(white)) baselevels keep(*c.TreatmentGains*) drop(0.Decile#c.TreatmentGains) ci ///
			rename(0.Decile#c.TreatmentGains="NA" 1.Decile#c.TreatmentGains="1" 2.Decile#c.TreatmentGains="2" 3.Decile#c.TreatmentGains="3" 4.Decile#c.TreatmentGains="4" ///
			5.Decile#c.TreatmentGains="5" /*6.Decile#c.TreatmentGains="6" 7.Decile#c.TreatmentGains="7" 8.Decile#c.TreatmentGains="8" 9.Decile#c.TreatmentGains="9" 10.Decile#c.TreatmentGains="10"*/ )   ///
			levels(95 90) ciopts(recast(rcap)) graphregion(color(white)) ///
			vertical yline(0) ylab(, ang(h) nogrid labsize(large)) xlab(,labsize(large))  xtitle("Quintile") ytitle("Treatment effect") title("P4Pctile") saving(Gains,replace) ///
			note("p-value(H{sub:0}:Q{sub:1}=Q{sub:5})= `pGains'",size(large)) yscale(range(-0.4 0.4)) ylabel(-0.4(0.2)0.4) 
			*graph export "$graphs/HeteroBaseline_`kind'_`subject'_`time'.pdf", replace

	gr combine Levels.gph Gains.gph, graphregion(color(white))
	graph export "$graphs/HeteroQuintile_`var'.pdf", replace
}  
