*****************************************************************
************* SABOTAGE OR HELPING OTHERS         ****************
*****************************************************************
use "$basein/11 Endline 2015/Final Data/Teacher Data/R8Teacher_nopii.dta", clear
keep  SchoolID R8TeacherID ETeach_pp upid whooth  trtght  trtgot trtgas dictatorgame  codoif

rename * =_T2
foreach var in SchoolID R8TeacherID ETeach_pp upid{
rename `var'_T2 `var'
}

tempfile R2
save `R2'

use "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10Teacher_nopii.dta", clear
keep SchoolID R10TeacherID ETeach_pp upid whooth  trtght  trtgot trtgas dictatorgame  stu_rep* tch_rep* codoif
recode stu_rep tch_rep (2=0)

foreach var of varlist stu_rep_how1- stu_rep_how5{
	replace `var'=1 if `var'!=0 & !missing(`var')
	replace `var'=0 if stu_rep==0
}
foreach var of varlist tch_rep_how1- tch_rep_how4{
	replace `var'=1 if `var'!=0 & !missing(`var')
	replace `var'=0 if tch_rep==0
}

rename * =_T5
foreach var in SchoolID R10TeacherID ETeach_pp upid{
rename `var'_T5 `var'
}

merge 1:1 SchoolID upid using `R2'

drop _merge



reshape long whooth_T@  trtght_T@  trtgot_T@ trtgas_T@ codoif_T@, i(upid) j(time)

replace whooth_T=. if whooth_T<0
gen D_whooth_T=(whooth_T>0) if !missing(whooth_T)
recode trtgot_T trtght_T trtgas_T (1/2=1) (3/6=0)  
recode codoif_T (8=0) (6/7=1)  (-99=.)

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "P4Pctile"

label var whooth_T "Help from other teachers last month"
label var D_whooth_T "Help from other teachers last month (yes/no)"
label var trtgas_T "Support from volunteers (good or very good)"
label var trtght_T "Help/advice from head teacher (good or very good)"
label var trtgot_T "Help/advice from other teachers (good or very good)"

label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

reghdfe codoif_T c.(${treatmentlist})  , vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm i.time)

eststo clear
foreach var of varlist whooth_T D_whooth_T trtgot_T  trtght_T  {
	eststo: reghdfe `var' c.(${treatmentlist})  , vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm i.time)
	estadd ysumm
	sum `var' if treatment2=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
}
esttab  using "$latexcodes/TeachersSabotage_Collaboration.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value(\$\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

esttab  using "$latexcodes/TeachersSabotage_Collaboration.csv", se ar2 label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01)  ///
replace m nogaps nolines ///
keep(TreatmentGains TreatmentLevels) ///
stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value(\$\alpha_3=0\$)" "") star(suma)) 


eststo clear
foreach var of varlist stu_rep_T5 stu_rep_how1_T5- stu_rep_how5_T5 {
	eststo: reghdfe `var' c.(${treatmentlist})  , vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm i.time)
	estadd ysumm
	sum `var' if treatment2=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
}
esttab  using "$latexcodes/TeachersUsageReports.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value(\$\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

 
eststo clear
foreach var of varlist tch_rep_T5 tch_rep_how1_T5- tch_rep_how4_T5 {
	eststo: reghdfe `var' TreatmentGains  , vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm i.time)
	estadd ysumm
	sum `var' if treatment2=="Levels"
	estadd scalar ymean2=r(mean)
}
esttab  using "$latexcodes/TeachersUsageReports_Teacher.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains) stats(N ymean2, fmt(%9.0gc %9.2gc) labels("N. of obs." "Mean control")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

 

