

*******************************************************
*************END LINE YR1
********************************************************

use "$basein/11 Endline 2015/Final Data/Student Data/R8Student_nopii.dta", clear
gen date_edi=dofc(time)
format date_edi %td
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
merge m:1 SchoolID using "$base_out/ConsolidatedYr34/TWA_Date_T2.dta"
drop if _merge==2
drop _merge


gen attgrade=1 if stdgrd==1 & atrgrd==.
replace attgrade=1 if atrgrd==1
replace attgrade=2 if stdgrd==2 & atrgrd==.
replace attgrade=2 if atrgrd==2
replace attgrade=3 if stdgrd==3 & atrgrd==.
replace attgrade=3 if atrgrd==3



gen student_present=0
replace student_present=1 if  consentChild==1
rename student_present attendance


gen date=dofc(timeStartTest)
format date %dN/D/Y
gen WeekIntvTest=week(date)


foreach var of varlist kispicl1_1- othsub1_8{
	recode `var' (.=0) if GradeID==1 & attendance==1
}
foreach var of varlist kispic2_1- othsub2_15{
	recode `var' (.=0) if GradeID==2 & attendance==1
}
foreach var of varlist kissyl3_1 - othsub3_13{
	recode `var' (.=0) if GradeID==3 & attendance==1
}
foreach var of varlist kissyl4_1- hisdiv4_9{
	recode `var' (.=0) if GradeID==4 & attendance==1
}


foreach var of varlist kispicl1_1-hisdiv4_9 {
	recode `var' (2=0) (3=0) 
}

foreach i of varlist kispicl1_1-hisdiv4_9 {
local a : variable label `i'
local a: subinstr local a " correctly" ""
label var `i' "`a'"
}





egen hiscou_1=rowtotal(hiscou1_2 hiscou2_1 hiscou3_1 hiscou4_1),missing

egen hisidn_1=rowtotal(hisidn1_2 hisidn3_1 hisidn4_1),missing 
egen hisidn_2=rowtotal(hisidn1_1 hisidn2_1 ),missing
egen hisidn_3=rowtotal(hisidn1_3 hisidn2_2),missing

egen hisinq_1=rowtotal(hisinq1_2 hisinq2_1 hisinq3_1 ),missing
egen hisinq_2=rowtotal(hisinq1_4 hisinq3_2 hisinq4_1),missing

egen hisadd_1=rowtotal(hisadd3_2 hisadd4_2 hisadd1_3 hisadd2_2),missing
egen hisadd_2=rowtotal(hisadd2_6 hisadd3_6 hisadd4_4),missing 
egen hisadd_3=rowtotal(hisadd1_4 hisadd2_4 hisadd4_3),missing
egen hisadd_4=rowtotal(hisadd1_1 hisadd2_1 hisadd3_1 hisadd4_1),missing
egen hisadd_5=rowtotal(hisadd2_3 hisadd3_3),missing
egen hisadd_6=rowtotal(hisadd2_5 hisadd3_4),missing

egen hissub_1=rowtotal(hissub1_3 hissub2_2 hissub3_2 hissub4_2 ),missing
egen hissub_2=rowtotal(hissub1_4 hissub2_4),missing
egen hissub_3=rowtotal(hissub2_5 hissub3_4),missing
egen hissub_4=rowtotal(hissub2_3 hissub3_3 hissub4_3),missing 
egen hissub_5=rowtotal(hissub2_6 hissub3_6 hissub4_4),missing
egen hissub_6=rowtotal(hissub1_1 hissub2_1 hissub3_1 hissub4_1),missing

egen hismul_1=rowtotal(hismul1_1 hismul2_1 hismul3_1 hismul4_1),missing
egen hismul_2=rowtotal(hismul2_3 hismul3_2 hismul4_2),missing
egen hismul_3=rowtotal(hismul2_4 hismul3_4 hismul4_3),missing

egen hisdiv_1=rowtotal(hisdiv3_5 hisdiv4_4),missing
egen hisdiv_2=rowtotal(hisdiv3_3 hisdiv4_3),missing
egen hisdiv_3=rowtotal(hisdiv2_2 hisdiv3_2 hisdiv4_2),missing
egen hisdiv_4=rowtotal(hisdiv2_1 hisdiv3_1 hisdiv4_1),missing

egen othsub_1=rowtotal(othsub1_5 othsub2_8 othsub3_6),missing
egen othsub_2=rowtotal(othsub2_3 othsub3_1),missing
egen othsub_3=rowtotal(othsub1_7 othsub2_10 othsub3_8),missing
egen othsub_4=rowtotal(othsub2_2 othsub3_2),missing
egen othsub_5=rowtotal(othsub2_1 othsub3_3),missing
egen othsub_6=rowtotal(othsub1_6 othsub2_9 othsub3_7),missing
egen othsub_7=rowtotal(othsub2_4 othsub3_4),missing
egen othsub_8=rowtotal(othsub2_7 othsub3_5), missing
egen othsub_9=rowtotal(othsub2_11 othsub3_9), missing

egen kispic_1=rowtotal(kispic1_1 kispic2_1),missing
egen kissyl_1=rowtotal(kissyl1_1 kissyl2_1 kissyl3_1 kissyl4_1 ),missing
egen kissyl_2=rowtotal(kissyl1_2 kissyl2_2 kissyl3_2 ),missing
egen kissyl_3=rowtotal(kissyl1_3 kissyl2_3 kissyl3_3 kissyl4_2),missing

egen kiswrd_1=rowtotal(kiswrd1_3 kiswrd2_3 kiswrd3_3),missing
egen kiswrd_2=rowtotal(kiswrd1_6 kiswrd2_7 kiswrd3_8 kiswrd4_4 ),missing
egen kiswrd_3=rowtotal(kiswrd1_2 kiswrd2_2 kiswrd3_2 ),missing
egen kiswrd_4=rowtotal(kiswrd1_5 kiswrd2_5 kiswrd3_6 ),missing
egen kiswrd_5=rowtotal(kiswrd1_4 kiswrd2_4 kiswrd3_5 kiswrd4_3 ),missing
egen kiswrd_6=rowtotal(kiswrd1_1 kiswrd2_1 kiswrd3_1 kiswrd4_1 ),missing
egen kiswrd_7=rowtotal(kiswrd2_6 kiswrd3_7),missing
egen kiswrd_8=rowtotal(kiswrd3_4 kiswrd4_2 ),missing

egen kissen_1=rowtotal(kissen1_1 kissen2_1 kissen3_1 kissen4_1),missing
egen kissen_2=rowtotal(kissen1_2 kissen2_2 kissen3_2),missing
egen kissen_3=rowtotal(kissen1_3 kissen2_3 kissen3_3),missing
egen kissen_4=rowtotal(kissen1_4 kissen2_4 kissen3_5),missing
egen kissen_5=rowtotal(kissen1_5 kissen2_5 kissen3_6),missing
egen kissen_6=rowtotal(kissen1_6 kissen2_7 kissen3_8 kissen4_4),missing
egen kissen_7=rowtotal(kissen2_6 kissen3_7 kissen4_3),missing
egen kissen_8=rowtotal(kissen3_4 kissen4_2),missing

egen kiswri_1=rowtotal(kiswri2_3 kiswri3_3 kiswri4_3),missing 
egen kiswri_2=rowtotal(kiswri3_5 kiswri4_5),missing
egen kiswri_3=rowtotal(kiswri1_1 kiswri2_1 kiswri3_1 kiswri4_1 ),missing
egen kiswri_4=rowtotal(kiswri1_2 kiswri2_2 kiswri3_2 kiswri4_2 ),missing
egen kiswri_5=rowtotal(kiswri2_4 kiswri3_4 kiswri4_4),missing
egen kiswri_6=rowtotal(kiswri3_6 kiswri4_6),missing

egen kispar_1=rowtotal(kispar1_1 kispar2_1 kispar3_1 )
egen kispar_2=rowtotal(kispar1_2 kispar2_2 kispar3_2)
egen kispar_3=rowtotal(kispar2_3  kispar3_3)
egen kispar_4=rowtotal(kispar2_5 kispar3_4)

egen kissto_1=rowtotal(kissto3_1 kissto4_1) 
egen kiscom_1=rowtotal(kiscom3_2 kissto4_2)
egen kiscom_2=rowtotal(kiscom3_3 kissto4_3)

egen engpic_1=rowtotal(engpic1_2 engpic2_1 engpic3_1 ),missing
egen engpic_2=rowtotal(engpic1_3 engpic2_2 engpic3_2 engpic4_1),missing

egen engpicl_1=rowtotal(engpicl1_1 engpicl2_1 engpicl3_1 engpicl4_1 ),missing
egen engpicl_2=rowtotal(engpicl1_2 engpicl2_2 engpicl3_2),missing

egen englet_1=rowtotal(englet1_1 englet2_1 englet3_1 englet4_1 ),missing
egen englet_2=rowtotal(englet1_2 englet2_2 englet3_2 ),missing
egen englet_3=rowtotal(englet1_3 englet2_3 englet3_3 englet4_2),missing

egen engwrd_1=rowtotal(engwrd2_5 engwrd3_4 ),missing
egen engwrd_2=rowtotal(engwrd1_3 engwrd2_4 engwrd3_3 engwrd4_2),missing
egen engwrd_3=rowtotal(engwrd1_2 engwrd2_3 engwrd3_2 ),missing
egen engwrd_4=rowtotal(engwrd2_8 engwrd3_7 engwrd4_4 ),missing
egen engwrd_5=rowtotal(engwrd1_1 engwrd2_2 engwrd3_1 engwrd4_1),missing
egen engwrd_6=rowtotal(engwrd2_7 engwrd3_6 ),missing
egen engwrd_7=rowtotal(engwrd1_5 engwrd2_6 engwrd3_5 engwrd4_3),missing

egen engsen_1=rowtotal(engsen1_3 engsen2_3 engsen3_3),missing
egen engsen_2=rowtotal(engsen2_8 engsen3_6 engsen4_3),missing
egen engsen_3=rowtotal(engsen1_4 engsen2_4 engsen3_4),missing
egen engsen_4=rowtotal(engsen1_2 engsen2_2 engsen3_2 engsen4_1),missing
egen engsen_5=rowtotal(engsen1_1 engsen2_1 engsen3_1),missing
egen engsen_6=rowtotal(engsen2_6 engsen3_5 engsen4_2),missing

egen engwri_1=rowtotal(engwri2_4 engwri3_4 engwri4_4 ),missing
egen engwri_2=rowtotal(engwri1_1 engwri2_1 engwri3_1 engwri4_1 ),missing
egen engwri_3=rowtotal(engwri3_6 engwri4_6),missing
egen engwri_4=rowtotal(engwri2_3 engwri3_3 engwri4_3 ),missing
egen engwri_5=rowtotal(engwri3_5 engwri4_5 ),missing
egen engwri_6=rowtotal(engwri1_2 engwri2_2 engwri3_2 engwri4_2),missing 

egen engpar_1=rowtotal(engpar2_1 engpar3_1 engpar4_1)

egen engsto_1=rowtotal(engsto3_1 engsto4_1)
egen engcom_1=rowtotal(engcom3_1 engsto4_2)
egen engcom_2=rowtotal(engcom3_3 engsto4_3)

drop othsub2_7 othsub3_5 othsub2_11 othsub3_9
drop engsto3_1 engsto4_1 engcom3_1 engsto4_2 engcom3_3 engsto4_3
drop kissto3_1 kissto4_1 kiscom3_2 kissto4_2 kiscom3_3 kissto4_3 engsen3_1
drop kispar2_1 kispar3_1 kispar4_1 kispar1_2 kispar2_2 kispar3_2 kispar2_3  kispar3_3 kispar2_5 kispar3_4
drop engpar2_1 engpar3_1 engpar4_1 hiscou1_2 engsen2_1 engwri2_1 engwri2_3 engwri2_4 engwri2_2
drop kissen1_1 kissen2_1 kissen3_1 kissen4_1 kissen1_2 kissen2_2 kissen3_2 kissen1_3 kissen2_3 kissen3_3 kissen1_4 kissen2_4 kissen3_5 kissen1_5 kissen2_5 kissen3_6
drop kissen1_6 kissen2_7 kissen3_8 kissen4_4 kissen2_6 kissen3_7 kissen4_3 kissen3_4 kissen4_2
drop engsen1_1 engsen1_2 engsen1_3 engsen1_4 hisdiv2_2

drop hisadd3_2 hisadd4_2 hisadd1_3 hisadd2_2 hisadd2_6 hisadd3_6 hisadd4_4 hisadd1_4 hisadd2_4 hisadd4_3 hisadd1_1 hisadd2_1 hisadd3_1 hisadd4_1 
drop hisadd2_3 hisadd3_3 hisadd2_5 hisadd3_4 hiscou2_1 hiscou3_1 hiscou4_1 hisdiv3_5 hisdiv4_4 hisdiv3_3 hisdiv4_3 hisdiv3_2 hisdiv4_2 hisdiv2_1 hisdiv3_1 hisdiv4_1
drop othsub1_5 othsub2_8 othsub3_6 othsub2_3 othsub3_1 othsub1_7 othsub2_10 othsub3_8 othsub2_2 othsub3_2 othsub2_1 othsub3_3 othsub1_6 othsub2_9 othsub3_7
drop hisidn1_2 hisidn3_1 hisidn4_1  hisidn1_1 hisidn2_1 hisidn1_3 hisidn2_2 hisinq1_2 hisinq2_1 hisinq3_1  hisinq1_4 hisinq3_2 hisinq4_1
drop hismul1_1 hismul2_1 hismul3_1 hismul4_1 hismul2_3 hismul3_2 hismul4_2 hismul2_4 hismul3_4 hismul4_3 othsub2_4 othsub3_4 kissyl1_1 kissyl2_1 kissyl3_1 kissyl4_1 
drop kissyl1_2 kissyl2_2 kissyl3_2 kissyl1_3 kissyl2_3 kissyl3_3 kissyl4_2 engsen2_3 engsen3_3 engsen2_8 engsen3_6 engsen4_3 engsen2_4 engsen3_4 engsen2_2 engsen3_2 engsen4_1
drop engsen2_6 engsen3_5 engsen4_2 engwrd2_5 engwrd3_4 kiswrd1_3 kiswrd2_3 kiswrd3_3
drop kiswrd1_6 kiswrd2_7 kiswrd3_8 kiswrd4_4 kiswrd1_2 kiswrd2_2 kiswrd3_2 engwrd1_3 engwrd2_4 engwrd3_3 engwrd4_2 engwrd1_2 engwrd2_3 engwrd3_2 engwrd2_8 engwrd3_7 engwrd4_4 
drop kiswrd1_5 kiswrd2_5 kiswrd3_6 engwrd1_1 engwrd2_2 engwrd3_1 engwrd4_1 kiswrd1_4 kiswrd2_4 kiswrd3_5 kiswrd4_3 kiswrd1_1 kiswrd2_1 kiswrd3_1 kiswrd4_1 kiswrd2_6 kiswrd3_7
drop kiswrd3_4 kiswrd4_2 engwrd2_7 engwrd3_6 engwrd1_5 engwrd2_6 engwrd3_5 engwrd4_3 engpic1_2 engpic2_1 engpic3_1 engpicl1_1 engpicl2_1 engpicl3_1 engpicl4_1 kispic1_1 kispic2_1
drop engpic1_3 engpic2_2 engpic3_2 engpic4_1 engpicl1_2 engpicl2_2 engpicl3_2 hissub1_3 hissub2_2 hissub3_2 hissub4_2 hissub1_4 hissub2_4 hissub2_5 hissub3_4 hissub2_3 hissub3_3 hissub4_3 
drop hissub2_6 hissub3_6 hissub4_4 hissub1_1 hissub2_1 hissub3_1 hissub4_1 englet1_1 englet2_1 englet3_1 englet4_1  englet1_2 englet2_2 englet3_2 englet1_3 englet2_3 englet3_3 englet4_2
drop engwri3_4 engwri4_4 kiswri2_3 kiswri3_3 kiswri4_3 engwri1_1 engwri3_1 engwri4_1 engwri3_6 engwri4_6 kiswri3_5 kiswri4_5 kiswri1_1 kiswri2_1 kiswri3_1 kiswri4_1
drop kiswri1_2 kiswri2_2 kiswri3_2 kiswri4_2  kiswri2_4 kiswri3_4 kiswri4_4 engwri3_3 engwri4_3 engwri3_5 engwri4_5 engwri1_2 engwri3_2 engwri4_2 kiswri3_6 kiswri4_6




forvalues val=1/3{
	preserve
	keep if GradeID==`val'
	dropmiss, force
	findname kis* eng* his*, all(@ == @[1])
	if ("`r(varlist)'"!="") drop  `r(varlist)'
	irt (2pl kis*) if GradeID==`val', intpoints(20) difficult
	irtgraph tif, graphregion(color(white)) se
	graph export "$graphs/TIF_IRT_kiswahili_`val'_T2.pdf", replace

	irt (2pl eng*) if GradeID==`val', intpoints(20) difficult
	irtgraph tif, graphregion(color(white)) se
	graph export "$graphs/TIF_IRT_english_`val'_T2.pdf", replace

	irt (2pl his*) if GradeID==`val', intpoints(20) difficult
	irtgraph tif, graphregion(color(white)) se
	graph export "$graphs/TIF_IRT_math_`val'_T2.pdf", replace


	irt (1pl kis* his*) if GradeID==`val', intpoints(20) difficult
	irtgraph tif, graphregion(color(white)) se
	graph export "$graphs/TIF_IRT_ScoreKisawMath_`val'_T2.pdf", replace
	
	restore
}


*******************************************************
*************END LINE YR2
********************************************************



use "$basein/15 Endline 2016/Final Deliverable/Final Data/Student Data/R10Student_nopii.dta", clear
gen date_edi=dofc(time)
format date_edi %td
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
merge m:1 SchoolID using "$base_out/ConsolidatedYr34/TWA_Date_T5.dta"
drop if _merge==2
drop _merge

gen attgrade=1 if stdgrd==1 & atrgrd==.
replace attgrade=1 if atrgrd==1
replace attgrade=2 if stdgrd==2 & atrgrd==.
replace attgrade=2 if atrgrd==2
replace attgrade=3 if stdgrd==3 & atrgrd==.
replace attgrade=3 if atrgrd==3



gen student_present=0
replace student_present=1 if  consentChild==1
rename student_present attendance


gen date=dofc(timeStartTest)
format date %dN/D/Y
gen WeekIntvTest=week(date)


drop kiswri4_7_er  kiswri4_8_er engwri4_8_er /*for now not sure what to do with these */
foreach var of varlist kispicl1_1- othsub1_8{
	recode `var' (.=0) if GradeID==1 & attendance==1
}
foreach var of varlist kispic2_1- othsub2_14{
	recode `var' (.=0) if GradeID==2 & attendance==1
}
foreach var of varlist kissyl3_1 - othsub3_17{
	recode `var' (.=0) if GradeID==3 & attendance==1
}
foreach var of varlist kissyl4_1- hisdiv4_9{
	recode `var' (.=0) if GradeID==4 & attendance==1
}


foreach var of varlist kispicl1_1-hisdiv4_9 {
	recode `var' (2=0) (3=0) (-99=0)
}
foreach i of varlist kispicl1_1-hisdiv4_9 {
local a : variable label `i'
local a: subinstr local a " correctly" ""
label var `i' "`a'"
}


egen engwri_1=rowtotal(engwri3_6 engwri4_6),missing
egen engwri_2=rowtotal(engwri2_4 engwri3_4 engwri4_4 ),missing
egen engwri_3=rowtotal(engwri3_5 engwri4_5),missing
egen engwri_4=rowtotal(engwri1_2 engwri2_2 engwri3_2 engwri4_2),missing
egen engwri_5=rowtotal(engwri2_3 engwri3_3 engwri4_3 ),missing
egen engwri_6=rowtotal(engwri1_1 engwri2_1 engwri3_1 engwri4_1),missing

egen kiswri_1=rowtotal(kiswri1_2 kiswri2_2 kiswri3_2 kiswri4_2 ),missing
egen kiswri_2=rowtotal(kiswri1_4 kiswri2_4 kiswri3_4 kiswri4_4),missing
egen kiswri_3=rowtotal(kiswri3_5 kiswri4_5 ),missing
egen kiswri_4=rowtotal(kiswri1_1 kiswri2_1 kiswri3_1 kiswri4_1 ),missing
egen kiswri_5=rowtotal(kiswri3_6 kiswri4_6 ),missing
egen kiswri_6=rowtotal(kiswri1_3 kiswri2_3 kiswri3_3 kiswri4_3),missing

egen englet_1=rowtotal(englet1_3 englet2_3 englet3_3 englet4_2 ),missing
egen englet_2=rowtotal(englet1_1 englet2_1 englet3_1 englet4_1 ),missing
egen englet_3=rowtotal(englet1_2 englet2_2 englet3_2),missing

egen hissub_1=rowtotal(hissub1_1 hissub2_1 hissub3_1 hissub4_1),missing
egen hissub_2=rowtotal(hissub2_6 hissub3_6 hissub4_4),missing
egen hissub_3=rowtotal(hissub2_3 hissub3_3 hissub4_3 ),missing
egen hissub_4=rowtotal(hissub2_5 hissub3_4 ),missing
egen hissub_5=rowtotal(hissub1_4 hissub2_4),missing
egen hissub_6=rowtotal(hissub1_3 hissub2_2 hissub3_2 hissub4_2),missing

egen engpicl_1=rowtotal(engpicl1_3 engpicl4_2),missing
egen engpicl_2=rowtotal(engpicl1_2 engpicl2_2 engpicl3_2 ),missing
egen engpicl_3=rowtotal(engpicl4_1 engpicl1_1 engpicl2_1 engpicl3_1),missing

egen engpic_1=rowtotal(engpic1_3 engpic2_2 engpic3_2 ),missing
egen engpic_2=rowtotal(engpic1_2 engpic2_1 engpic3_1),missing

egen kispic_1=rowtotal(kispic1_1 kispic2_1 ),missing

egen engwrd_1=rowtotal(engwrd1_3 engwrd2_4 engwrd3_3 engwrd4_2),missing
egen engwrd_2=rowtotal(engwrd1_4 engwrd2_5 engwrd3_4),missing
egen engwrd_3=rowtotal(engwrd2_8 engwrd3_7),missing
egen engwrd_4=rowtotal(engwrd2_2 engwrd3_1 engwrd4_1 ),missing
egen engwrd_5=rowtotal(engwrd1_1 engwrd2_1 ),missing
egen engwrd_6=rowtotal(engwrd2_7 engwrd3_6 engwrd4_4),missing 
egen engwrd_7=rowtotal(engwrd1_2 engwrd2_3 engwrd3_2),missing
egen engwrd_8=rowtotal(engwrd1_5 engwrd2_6 engwrd3_5 engwrd4_3),missing

egen kiswrd_1=rowtotal(kiswrd2_6 kiswrd3_7),missing
egen kiswrd_2=rowtotal(kiswrd1_2 kiswrd2_2 kiswrd3_2),missing
egen kiswrd_3=rowtotal(kiswrd1_5 kiswrd2_5 kiswrd3_6),missing
egen kiswrd_4=rowtotal(kiswrd1_1 kiswrd2_1 kiswrd3_1 kiswrd4_1 ),missing
egen kiswrd_5=rowtotal(kiswrd1_6 kiswrd2_7 kiswrd3_8 kiswrd4_4),missing
egen kiswrd_6=rowtotal(kiswrd1_3 kiswrd2_3 kiswrd3_3 kiswrd4_2 ),missing
egen kiswrd_7=rowtotal(kiswrd1_4 kiswrd2_4 kiswrd3_5 kiswrd4_3),missing


egen kissyl_1=rowtotal(kissyl1_3 kissyl2_3 kissyl3_3 kissyl4_2 ),missing
egen kissyl_2=rowtotal(kissyl1_1 kissyl2_1 kissyl3_1 kissyl4_1 ),missing
egen kissyl_3=rowtotal(kissyl3_2 kissyl1_2 kissyl2_2),missing

egen hismul_1=rowtotal(hismul2_3 hismul3_2 hismul4_2 ),missing
egen hismul_2=rowtotal(hismul2_4 hismul3_4 hismul4_3),missing
egen hismul_3=rowtotal(hismul1_1 hismul2_1 hismul3_1 hismul4_1 ),missing

egen hisinq_1=rowtotal(hisinq1_4 hisinq3_2 hisinq4_1 ),missing
egen hisinq_2=rowtotal(hisinq1_2 hisinq2_1 hisinq3_1),missing

egen hisidn_1=rowtotal(hisidn1_3 hisidn2_2 ),missing
egen hisidn_2=rowtotal(hisidn1_1 hisidn2_1 ),missing
egen hisidn_3=rowtotal(hisidn1_2 hisidn3_1 hisidn4_1),missing

*egen othsub_1=rowtotal(othsub1_4 othsub1_8),missing
egen othsub_2=rowtotal(othsub1_6 othsub2_9 othsub3_9 ),missing
egen othsub_3=rowtotal(othsub1_3 othsub2_3 othsub3_3 ),missing
egen othsub_4=rowtotal(othsub1_7 othsub2_10 othsub3_10),missing
egen othsub_5=rowtotal(othsub1_5 othsub2_8 othsub3_8 ),missing
egen othsub_6=rowtotal(othsub1_2 othsub2_2 othsub3_2 ),missing
egen othsub_7=rowtotal(othsub1_1 othsub2_1 othsub3_1),missing
egen othsub_8=rowtotal(othsub2_35 othsub3_35),missing
egen othsub_9=rowtotal(othsub3_14 othsub2_14),missing
egen othsub_10=rowtotal(othsub2_34 othsub3_34),missing
egen othsub_11=rowtotal(othsub33_4 othsub10_4),missing
egen othsub_12=rowtotal(othsub2_7 othsub3_7),missing



egen hisdiv_1=rowtotal(hisdiv2_2 hisdiv3_2 hisdiv4_2),missing
egen hisdiv_2=rowtotal(hisdiv2_1 hisdiv3_1 hisdiv4_1),missing
egen hisdiv_3=rowtotal(hisdiv3_5 hisdiv4_4 ),missing
egen hisdiv_4=rowtotal(hisdiv3_3 hisdiv4_3),missing

egen hiscou_1=rowtotal(hiscou2_1 hiscou4_1 hiscou1_2 hiscou3_1),missing


egen hisadd_1=rowtotal(hisadd2_5 hisadd3_4),missing
egen hisadd_2=rowtotal(hisadd2_1 hisadd3_1 hisadd4_1 hisadd1_1),missing
egen hisadd_3=rowtotal(hisadd2_3 hisadd3_3),missing
egen hisadd_4=rowtotal(hisadd1_4 hisadd2_4 hisadd4_3),missing
egen hisadd_5=rowtotal(hisadd2_6 hisadd3_6 hisadd4_4 ),missing
egen hisadd_6=rowtotal(hisadd1_3 hisadd2_2 hisadd3_2 hisadd4_2),missing


egen engsen_1=rowtotal(engsen1_1 engsen2_1 engsen3_1 ),missing
egen engsen_2=rowtotal(engsen1_2 engsen2_2 engsen3_2  engsen4_1),missing
egen engsen_3=rowtotal(engsen1_3 engsen2_3 engsen3_3),missing
egen engsen_4=rowtotal(engsen1_4 engsen2_4 engsen3_4 ),missing
egen engsen_5=rowtotal(engsen2_6 engsen3_5 engsen4_2 ),missing
egen engsen_6=rowtotal(engsen2_8 engsen3_6 engsen4_3 ),missing




egen kissen_1=rowtotal(kissen1_1 kissen2_1 kissen3_1 kissen4_1),missing
egen kissen_2=rowtotal(kissen1_2 kissen2_2 kissen3_2),missing
egen kissen_3=rowtotal(kissen1_3 kissen2_3 kissen3_3 kissen4_2),missing
egen kissen_4=rowtotal(kissen1_4 kissen2_4 kissen3_5),missing
egen kissen_5=rowtotal(kissen1_5 kissen2_5 kissen3_6 kissen4_3),missing
egen kissen_6=rowtotal(kissen1_6 kissen2_7 kissen3_8 kissen4_4 ),missing
egen kissen_7=rowtotal(kissen2_6 kissen3_7),missing

egen kispar_1=rowtotal(kispar2_1 kispar3_1 kispar4_1),missing
egen kispar_2=rowtotal(kispar2_2 kispar3_2 kispar4_2),missing
egen kispar_3=rowtotal(kispar3_3 kispar4_3), missing

egen engpar_1=rowtotal(engpar2_1 engpar3_1 engpar4_1), missing

egen kissto_1=rowtotal(kissto3_1 kissto4_1), missing
egen kissto_2=rowtotal(kissto3_2 kissto4_2), missing
egen kissto_3=rowtotal(kissto3_3 kissto4_3), missing
egen kissto_4=rowtotal(kissto3_4 kissto4_4), missing

egen engsto_1=rowtotal(engsto3_1 engsto4_1), missing
egen engsto_2=rowtotal(engcom3_1 engsto4_2), missing
egen engsto_3=rowtotal(engcom3_2 engsto4_3), missing
egen engsto_4=rowtotal(engcom3_3 engsto4_9), missing



  

drop engsto3_1 engsto4_1 engsto4_2 engsto4_3 engsto4_9 engcom3_1 engcom3_2 engcom3_3
drop kissto3_1 kissto4_1 kissto3_2 kissto4_2 kissto3_3 kissto4_3 kissto3_4 kissto4_4
drop othsub2_7 othsub3_7 kispar3_3 kispar4_3
drop engwri2_1 engwri2_2 engwri2_3 engwri2_4 engpar2_1 engpar3_1 engpar4_1
drop kispar2_1 kispar3_1 kispar4_1 kispar2_2 kispar3_2 kispar4_2
drop engsen1_1 engsen1_2 engsen1_3 engsen1_4 
drop kissen1_1 kissen2_1 kissen3_1 kissen4_1 kissen1_2 kissen2_2 kissen3_2 kissen1_3 kissen2_3 kissen3_3 kissen4_2 kissen1_4 kissen2_4 kissen3_5
drop kissen1_5 kissen2_5 kissen3_6 kissen4_3 kissen1_6 kissen2_7 kissen3_8 kissen4_4  kissen2_6 kissen3_7
drop engwri3_6 engwri4_6 engwri3_4 engwri4_4 kiswri1_2 kiswri2_2 kiswri3_2 kiswri4_2 kiswri1_4 kiswri2_4 kiswri3_4 kiswri4_4 kiswri3_5 kiswri4_5 kiswri1_1 kiswri2_1 kiswri3_1 kiswri4_1 
drop engwri3_5 engwri4_5 kiswri3_6 kiswri4_6 engwri1_2 engwri3_2 engwri4_2 kiswri1_3 kiswri2_3 kiswri3_3 kiswri4_3 engwri3_3 engwri4_3 engwri1_1 engwri3_1 engwri4_1
drop englet1_3 englet2_3 englet3_3 englet4_2 englet1_1 englet2_1 englet3_1 englet4_1 englet1_2 englet2_2 englet3_2 hissub1_1 hissub2_1 hissub3_1 hissub4_1 hissub2_6 hissub3_6 hissub4_4
drop hissub2_3 hissub3_3 hissub4_3 hissub2_5 hissub3_4 hissub1_4 hissub2_4 hissub1_3 hissub2_2 hissub3_2 hissub4_2 engpic1_3 engpic2_2 engpic3_2 engpicl1_3 engpicl4_2
drop engpicl1_2 engpicl2_2 engpicl3_2 kispic1_1 kispic2_1 engpic1_2 engpic2_1 engpic3_1 engpicl4_1 engpicl1_1 engpicl2_1 engpicl3_1 kiswrd2_6 kiswrd3_7 engwrd1_3 engwrd2_4 engwrd3_3 engwrd4_2
drop engwrd1_4 engwrd2_5 engwrd3_4 kiswrd1_2 kiswrd2_2 kiswrd3_2 engwrd2_8 engwrd3_7 kiswrd1_5 kiswrd2_5 kiswrd3_6 engwrd2_2 engwrd3_1 engwrd4_1 kiswrd1_1 kiswrd2_1 kiswrd3_1 kiswrd4_1 
drop kiswrd1_6 kiswrd2_7 kiswrd3_8 kiswrd4_4 engwrd1_1 engwrd2_1 kiswrd1_3 kiswrd2_3 kiswrd3_3 kiswrd4_2 kiswrd1_4 kiswrd2_4 kiswrd3_5 kiswrd4_3 engwrd2_7 engwrd3_6 engwrd4_4 
drop engwrd1_2 engwrd2_3 engwrd3_2 engwrd1_5 engwrd2_6 engwrd3_5 engwrd4_3 kissyl1_3 kissyl2_3 kissyl3_3 kissyl4_2 kissyl1_1 kissyl2_1 kissyl3_1 kissyl4_1 kissyl3_2 kissyl1_2 kissyl2_2
drop hismul2_3 hismul3_2 hismul4_2 hismul2_4 hismul3_4 hismul4_3 hismul1_1 hismul2_1 hismul3_1 hismul4_1 othsub1_4 othsub1_8 hisinq1_4 hisinq3_2 hisinq4_1 hisinq1_2 hisinq2_1 hisinq3_1
drop hisidn1_3 hisidn2_2 hisidn1_1 hisidn2_1 hisidn1_2 hisidn3_1 hisidn4_1 othsub1_6 othsub2_9 othsub3_9 othsub1_3 othsub2_3 othsub3_3 othsub1_7 othsub2_10 othsub3_10 othsub1_5 othsub2_8 othsub3_8 
drop othsub1_2 othsub2_2 othsub3_2 othsub1_1 othsub2_1 othsub3_1 hisdiv2_2 hisdiv3_2 hisdiv4_2 hisdiv2_1 hisdiv3_1 hisdiv4_1 hisdiv3_5 hisdiv4_4 hisdiv3_3 hisdiv4_3 hiscou2_1 hiscou4_1 hiscou1_2 hiscou3_1
drop othsub2_35 othsub3_35 othsub3_14 othsub2_14 othsub2_34 othsub3_34 othsub33_4 othsub10_4 hisadd2_5 hisadd3_4 hisadd2_1 hisadd3_1 hisadd4_1 hisadd1_1 hisadd2_3 hisadd3_3
drop hisadd1_4 hisadd2_4 hisadd4_3 hisadd2_6 hisadd3_6 hisadd4_4 hisadd1_3 hisadd2_2 hisadd3_2 hisadd4_2

drop engsen2_3 engsen3_3 engsen3_6 engsen4_3 engsen2_8 engsen3_2 engsen4_1 engsen2_2 engsen3_4 engsen2_4 
drop engsen3_5 engsen4_2 engsen2_6 engsen3_1 engsen2_1 


forvalues val=1/3{
	preserve
	keep if GradeID==`val'
	dropmiss, force
	findname kis* eng* his*, all(@ == @[1])
	if ("`r(varlist)'"!="") drop  `r(varlist)'
	irt (1pl kis*) if GradeID==`val', intpoints(20) difficult
	irtgraph tif, graphregion(color(white))  se
	graph export "$graphs/TIF_IRT_kiswahili_`val'_T5.pdf", replace

	irt (1pl eng*) if GradeID==`val', intpoints(20) difficult
	irtgraph tif, graphregion(color(white)) se
	graph export "$graphs/TIF_IRT_english_`val'_T5.pdf", replace

	irt (1pl his*) if GradeID==`val', intpoints(20) difficult
	irtgraph tif, graphregion(color(white)) se
	graph export "$graphs/TIF_IRT_math_`val'_T5.pdf", replace


	irt (1pl kis* his*) if GradeID==`val', intpoints(20) difficult
	irtgraph tif, graphregion(color(white)) se
	graph export "$graphs/TIF_IRT_ScoreKisawMath_`val'_T5.pdf", replace

	restore
}
