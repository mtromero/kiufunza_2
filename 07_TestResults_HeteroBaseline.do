**********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
drop if studentid==""
merge 1:1 studentid using "$basein/4 Intervention/TwaEL_2014/StudentRaw_T0.dta", keepus(Kis_SI- Math_G)
drop if _merge==2
drop _merge

reghdfe Z_hisabati_T3 $treatmentlist i.Grade_T3 ${schoolcontrol} , vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
reghdfe Z_hisabati_T3 c.($treatmentlist)#i.Kis_SI i.Grade_T3 ${schoolcontrol} , vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)

rename Kis_SI Kis_Baseline_Silabi
rename Kis_MA Kis_Baseline_Maneno
rename Kis_SE Kis_Baseline_Sentenci

rename Math_ID Math_Baseline_id
rename Math_UTA Math_Baseline_uta
rename Math_BWA Math_Baseline_bwa
rename Math_J  Math_Baseline_j
rename Math_T Math_Baseline_t
rename Math_Z  Math_Baseline_z
rename Math_G Math_Baseline_g

keep *_Baseline* Kis_Silabi* Kis_Maneno* Kis_Sentenci* Math_id* Math_uta* Math_bwa* Math_j* Math_t* Math_z* Math_g* $treatmentlist Grade_T* ${schoolcontrol} SchoolID DistrictID StrataScore  treatarm treatment2

gen ID=_n
 
reshape long Kis_Silabi_Pass_@ Kis_Maneno_Pass_@ Kis_Sentenci_Pass_@ Math_id_Pass_@ Math_uta_Pass_@ Math_bwa_Pass_@ Math_j_Pass_@ Math_t_Pass_@ Math_z_Pass_@ Math_g_Pass_@, i(ID) j(time) string


reshape long Kis_@_Pass_ Math_@_Pass_ Math_Baseline_@ Kis_Baseline_@, i(ID time) j(skill) string
encode skill, gen(skill2)
encode time, gen(time2)

reghdfe Math__Pass_ c.($treatmentlist)#i.Math_Baseline_ i.Grade_T3 ${schoolcontrol} if time=="T3", vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm##skill2)

label var Math_Baseline_ "Baseline skill level"
label var Kis_Baseline_ "Baseline skill level"



foreach var in Math Kis {
	eststo clear
	foreach time in T3 T6{

		eststo: reghdfe `var'__Pass_ c.($treatmentlist )#i.(`var'_Baseline_) i.Grade_`time' ${schoolcontrol} if time=="`time'", vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm##skill2)
		estadd ysumm
		sum `var'__Pass_ if treatment2=="Control"
		estadd scalar ymean2=r(mean)
	}
esttab  using "$latexcodes/Pass_Pooled_HeteroBaseline_`var'.tex", se ar2 booktabs label b(%9.3gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(*TreatmentGains* *TreatmentLevels*) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc %9.2gc) labels("N. of obs." "Control mean" "\$\beta_3 = \beta_2-\beta_1\$" "p-value (\$H_0:\beta_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

}

