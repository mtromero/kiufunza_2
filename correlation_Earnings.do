use "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10Teacher_nopii.dta", clear
keep SchoolID R10TeacherID ETeach_pp upid risk1- risk10  t66 set_goals set_goals_what- toppex knwtwa2- codsyn2016

gen stop_rule=0 if risk10==2
replace stop_rule=1 if risk9==2 & risk10==1
replace stop_rule=2 if risk8==2 & risk9==1
replace stop_rule=3 if risk7==2 & risk8==1
replace stop_rule=4 if risk6==2 & risk7==1
replace stop_rule=5 if risk5==2 & risk6==1
replace stop_rule=6 if risk4==2 & risk5==1
replace stop_rule=7 if risk3==2 & risk4==1
replace stop_rule=8 if risk2==2 & risk3==1
replace stop_rule=9 if risk1==2 & risk2==1

replace stop_rule=.  if risk1==1
replace stop_rule=.  if risk10==2
replace stop_rule=.  if risk9==2 & risk8==1
replace stop_rule=.  if risk8==2 & risk7==1
replace stop_rule=.  if risk7==2 & risk6==1
replace stop_rule=.  if risk6==2 & risk5==1
replace stop_rule=.  if risk5==2 & risk4==1
replace stop_rule=.  if risk4==2 & risk3==1
replace stop_rule=.  if risk3==2 & risk2==1
replace stop_rule=.  if risk2==2 & risk1==1

gen risk_averse=(stop_rule<5) if !missing(stop_rule)
gen risk_neutral=(stop_rule==5) if !missing(stop_rule)
gen risk_loving=(stop_rule>5) if !missing(stop_rule)

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "P4Pctile"

gen GoalGeneral=(twa_goals==1) if !missing(twa_goals)

gen numericGoal=.
replace numericGoal=1 if twa_goals_what==1
replace numericGoal=0 if twa_goals_what==2 | twa_goals_what==-96 | twa_goals==2
gen higherPassRate=(toppex==1) if !missing(toppex)
destring top_earnl mid_earnl low_earnl bonus_mong, replace
destring top_earng mid_earng low_earng top_earng, replace

replace bonus_monl=. if bonus_monl==-99
replace bonus_mong=. if bonus_mong==-99

recode low_earn_worryl (2=1) (3/5=0) 
recode low_earn_worryg (2=1) (3/5=0) 


recode top_earnl (2=1) (3/5=0) 
recode top_earng (2=1) (3/5=0) 

recode mid_earnl (2=1) (3/5=0) 
recode mid_earng (2=1) (3/5=0) 

recode low_earnl (2=1) (3/5=0) 
recode low_earng (2=1) (3/5=0) 


egen bonus_mon=rowtotal(bonus_mong bonus_monl), missing
egen mid_earn=rowtotal(mid_earng mid_earnl), missing
egen low_earn=rowtotal(low_earng low_earnl), missing
egen top_earn=rowtotal(top_earng top_earnl), missing
egen low_earn_worry=rowtotal(low_earn_worryg low_earn_worryl), missing

foreach var of varlist set_goals_what1-set_goals_what11 {
	qui replace `var'=1 if `var'!=.
	qui replace `var'=0 if `var'==. & set_goals==2
	qui replace `var'=0 if `var'==. & set_goals==1
}


label var risk_averse "Risk averse"
pca set_goals_what1-set_goals_what11
predict Goals_Score,score
label var Goals_Score "Goal (PCA)"

label var set_goals_what1 "Goals (National exam)"
label var set_goals_what2 "Goals (School exam)"
label var set_goals_what3 "Goals (Twaweza exam)"
label var set_goals_what6 "Goals (Love learning)"
label var set_goals_what9 "Goals (Improve teaching)"
label var set_goals_what10 "Goals (Improve own knowledge)"

label var GoalGeneral "General goal for Twaweza exam"
label var numericGoal "Clear numeric goal"
label var higherPassRate "Expects higher pass rate"
label var bonus_mon "Expected bonus"

merge 1:m R10TeacherID SchoolID using "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10TGrdSubGrp_nopii.dta", keepus(R10TGradeID R10TGrdSub2ID)
drop if _merge!=3

collapse (mean) TreatmentLevels TreatmentGains bonus_mon low_earn mid_earn top_earn low_earn_worry set_goals_what2 set_goals_what3 set_goals_what9 set_goals_what10  GoalGeneral numericGoal, by( SchoolID R10TGradeID R10TGrdSub2ID)
tempfile temp1
save `temp1', replace



use "$base_out/4 Intervention/TwaEL_2016/Stadi_Grd1_Payments.dta", clear
gen treatment="Levels"
append using "$base_out/4 Intervention/TwaEL_2016/Stadi_Grd2_Payments.dta"
replace treatment="Levels" if treatment==""
append using "$base_out/4 Intervention/TwaEL_2016/Stadi_Grd3_Payments.dta"
replace treatment="Levels" if treatment==""
append using "$base_out/4 Intervention/TwaEL_2016/Mash_Grd1_Payments_v2.dta"
replace treatment="Gains" if treatment==""
append using "$base_out/4 Intervention/TwaEL_2016/Mash_Grd2_Payments_v2.dta"
replace treatment="Gains" if treatment==""
append using "$base_out/4 Intervention/TwaEL_2016/Mash_Grd3_Payments_v2.dta"
replace treatment="Gains" if treatment==""
keep SchoolID grade treatment Payment_Total_Kis Payment_Total_Math Payment_Total_Eng
collapse (mean) Payment_Total_Kis Payment_Total_Math Payment_Total_Eng, by( SchoolID grade)
reshape long Payment_Total_@, i(SchoolID grade) j(subject) string
replace subject="1" if subject=="Math"
replace subject="2" if subject=="Kis"
replace subject="3" if subject=="Eng"
destring subject, replace
rename subject R10TGrdSub2ID
rename grade R10TGradeID
rename Payment_Total_ PaymentReal

merge 1:1 SchoolID R10TGrdSub2ID R10TGradeID using `temp1'
drop if _merge!=3


