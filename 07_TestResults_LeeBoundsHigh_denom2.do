************************************************************
************************************************************
************************************************************
************************************************************
*This should really be the list of students I expect to test in 2016
use "$basein/9 Baseline 2015/Final Data/School/R7Grade_noPII.dta",clear
egen Sutdents=rowtotal( s184 s185), missing
rename R7GradeID grade2015
keep Sutdents grade2015 SchoolID
drop if grade2015>3
sort  grade2015 SchoolID
rename grade2015 Grade_T3
saveold "$base_out/4 Intervention/TwaEL_2015/CountTested_TWA_2015_Control_EDI.dta", replace

************************************************************
************************************************************
************************************************************
************************************************************

*This should really be the list of students I expect to test in 2015
use "$basein/13 Baseline 2016/Final Data/School Data_nopii/R9Grade_nopii",clear
egen Sutdents=rowtotal( s184 s185), missing
rename R9GradeID grade2016
keep Sutdents grade2016 SchoolID
drop if grade2016>3
sort  grade2016 SchoolID
rename grade2016 Grade_T6
saveold "$base_out/4 Intervention/TwaEL_2016/CountTested_TWA_2016_Control_EDI.dta", replace

/*
use "$base_out/4 Intervention/TwaEL_2015/CountTested_TWA_2015_Control_EDI.dta", clear
mmerge Grade_T3 SchoolID using "$base_out/4 Intervention/TwaEL_2015/CountTested_TWA_2015_Control.dta", uname(TWA)
drop if _merge!=3

use "$base_out/4 Intervention/TwaEL_2016/CountTested_TWA_2016_Control_EDI.dta", clear
mmerge Grade_T6 SchoolID using "$base_out/4 Intervention/TwaEL_2016/CountTested_TWA_2016_Control.dta", uname(TWA)
drop if _merge!=3
*/
************************************************************
************************************************************
************************************************************
************************************************************


*********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
preserve
drop if Grade_T3==.
collapse (count)  TestTakes=Z_hisabati_T3, by(Grade_T3 SchoolID treatment treatarm DistrictID treatment2 treatarm2 StrataScore TreatmentLevels TreatmentGains)
merge 1:1 SchoolID Grade_T3 using "$base_out/4 Intervention/TwaEL_2015/CountTested_TWA_2015_Control_EDI.dta"
drop if _merge!=3
drop _merge
drop if TestTakes==0
replace TestTakes=TestTakes/Sutdents
replace TestTakes=1 if TestTakes>1 & !missing(TestTakes)
eststo clear
eststo test_takers_Y1: reghdfe TestTakes TreatmentLevels TreatmentGains, ab(Grade_T3 DistrictID##StrataScore##treatarm) 
estadd ysumm
sum TestTakes if TreatmentLevels==0 & TreatmentGains==0
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
matrix M=e(b)
local cutLevels_T3=M[1,1]
local cutGains_T3=M[1,2]

/*
di `cutLevels_T3'
di `cutGains_T3'
reghdfe Z_hisabati_T3 $treatmentlist i.Grade_T3 ${schoolcontrol} if Grade_T3<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
preserve
gsort SchoolID Grade_T3 -Z_hisabati_T3
by SchoolID Grade_T3: gen conteo=_n
by SchoolID Grade_T3: gen denom=_N
gen prop_conteo=conteo/denom
drop if prop_conteo<.02020723 & treatment2=="Levels"
drop if prop_conteo<-.00296011 & treatment2=="P4Pctile"
reghdfe Z_hisabati_T3 $treatmentlist i.Grade_T3 ${schoolcontrol} if Grade_T3<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
lincom _b[TreatmentLevels]
restore
*/		
			
			
restore
preserve
drop if Grade_T6==.
collapse (count)  TestTakes=Z_hisabati_T6, by(Grade_T6 SchoolID treatment treatarm DistrictID treatment2 treatarm2 StrataScore TreatmentLevels TreatmentGains)
merge 1:1 SchoolID Grade_T6 using "$base_out/4 Intervention/TwaEL_2016/CountTested_TWA_2016_Control_EDI.dta"
drop if _merge!=3
drop _merge
drop if TestTakes==0
replace TestTakes=TestTakes/Sutdents
replace TestTakes=1 if TestTakes>1 & !missing(TestTakes)
eststo test_takers_Y2: reghdfe TestTakes TreatmentLevels TreatmentGains, ab(Grade_T6 DistrictID##StrataScore##treatarm) 
estadd ysumm
sum TestTakes if TreatmentLevels==0 & TreatmentGains==0
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
matrix M=e(b)
local cutLevels_T6=M[1,1]
local cutGains_T6=M[1,2]

label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"
esttab  using "$latexcodes/RegTestScores_highstakes_TestTakers.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
keep(TreatmentLevels TreatmentGains) stats(N ymean2 suma p, fmt(%9.0fc %9.2fc %9.2fc %9.2fc) labels("N. of obs." "Mean control group" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value(\$\alpha_3=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)
restore






eststo clear
foreach time in T3 T6{
	foreach var in Z_hisabati Z_kiswahili{
		eststo m_`var'_`time':  reghdfe `var'_`time' $treatmentlist i.Grade_`time' ${schoolcontrol} if Grade_`time'<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
		estadd ysumm
		test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		preserve
			gsort SchoolID Grade_`time' -`var'_`time'
			by SchoolID Grade_`time': gen conteo=_n
			by SchoolID Grade_`time': gen denom=_N
			gen prop_conteo=conteo/denom
			drop if prop_conteo<`cutLevels_`time'' & treatment2=="Levels"
			drop if prop_conteo<`cutGains_`time'' & treatment2=="P4Pctile"
			reghdfe `var'_`time' $treatmentlist i.Grade_`time' ${schoolcontrol} if Grade_`time'<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
			lincom _b[TreatmentLevels]
			scalar CI_Levels_1=r(estimate)+1.96*r(se)
			scalar CI_Levels_2=r(estimate)-1.96*r(se)
			lincom _b[TreatmentGains]
			scalar CI_Gains_1=r(estimate)+1.96*r(se)
			scalar CI_Gains_2=r(estimate)-1.96*r(se)
			lincom (_b[TreatmentGains]-_b[TreatmentLevels] )
			scalar CI_Diff_1=r(estimate)+1.96*r(se)
			scalar CI_Diff_2=r(estimate)-1.96*r(se)
			
		restore
		preserve
			gsort SchoolID Grade_`time' `var'_`time'
			by SchoolID Grade_`time': gen conteo=_n
			by SchoolID Grade_`time': gen denom=_N
			gen prop_conteo=conteo/denom
			drop if prop_conteo<`cutLevels_`time'' & treatment2=="Levels"
			drop if prop_conteo<`cutGains_`time'' & treatment2=="P4Pctile"
			reghdfe `var'_`time' $treatmentlist i.Grade_`time' ${schoolcontrol} if Grade_`time'<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
			lincom _b[TreatmentLevels]
			scalar CI_Levels_3=r(estimate)+1.96*r(se)
			scalar CI_Levels_4=r(estimate)-1.96*r(se)
			lincom _b[TreatmentGains]
			scalar CI_Gains_3=r(estimate)+1.96*r(se)
			scalar CI_Gains_4=r(estimate)-1.96*r(se)
			lincom (_b[TreatmentGains]-_b[TreatmentLevels] )
			scalar CI_Diff_3=r(estimate)+1.96*r(se)
			scalar CI_Diff_4=r(estimate)-1.96*r(se)
		restore
		estadd scalar Lee_Levels_1=min(`=scalar(CI_Levels_1)',`=scalar(CI_Levels_2)',`=scalar(CI_Levels_3)',`=scalar(CI_Levels_4)'): m_`var'_`time'
		estadd scalar Lee_Levels_2=max(`=scalar(CI_Levels_1)',`=scalar(CI_Levels_2)',`=scalar(CI_Levels_3)',`=scalar(CI_Levels_4)'): m_`var'_`time'
		estadd scalar Lee_Gains_1=min(`=scalar(CI_Gains_1)',`=scalar(CI_Gains_2)',`=scalar(CI_Gains_3)',`=scalar(CI_Gains_4)'): m_`var'_`time'
		estadd scalar Lee_Gains_2=max(`=scalar(CI_Gains_1)',`=scalar(CI_Gains_2)',`=scalar(CI_Gains_3)',`=scalar(CI_Gains_4)'): m_`var'_`time'
		estadd scalar Lee_Diff_1=min(`=scalar(CI_Diff_1)',`=scalar(CI_Diff_2)',`=scalar(CI_Diff_3)',`=scalar(CI_Diff_4)'): m_`var'_`time'
		estadd scalar Lee_Diff_2=max(`=scalar(CI_Diff_1)',`=scalar(CI_Diff_2)',`=scalar(CI_Diff_3)',`=scalar(CI_Diff_4)'): m_`var'_`time'
	}
}


label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

esttab  using "$latexcodes/RegTestScores_highstakes_Lee.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
keep(TreatmentLevels TreatmentGains) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value(\$\alpha_3=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)

esttab  using "$latexcodes/RegTestScores_highstakes_Lee_two.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines nogaps /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
drop(*) stats(Lee_Levels_1 Lee_Levels_2, fmt(a2 a2) labels("Lower 95\% CI (\$\alpha_1\$)" "Higher 95\% CI (\$\alpha_1\$)" )) ///
nonotes substitute(\_ _)

esttab  using "$latexcodes/RegTestScores_highstakes_Lee_three.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines nogaps /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
drop(*) stats(Lee_Gains_1 Lee_Gains_2, fmt(a2 a2) labels("Lower 95\% CI (\$\alpha_2\$)" "Higher 95\% CI (\$\alpha_2\$)" )) ///
nonotes substitute(\_ _)

esttab  using "$latexcodes/RegTestScores_highstakes_Lee_four.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines nogaps /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
drop(*) stats(Lee_Diff_1 Lee_Diff_2, fmt(a2 a2) labels("Lower 95\% CI (\$\alpha_3\$)" "Higher 95\% CI (\$\alpha_3\$)" )) ///
nonotes substitute(\_ _)





foreach time in T3 T6{
	foreach var in Z_hisabati Z_kiswahili{
		eststo clear
		preserve
			gsort SchoolID Grade_`time' -`var'_`time'
			by SchoolID Grade_`time': gen conteo=_n
			by SchoolID Grade_`time': gen denom=_N
			gen prop_conteo=conteo/denom
			drop if prop_conteo<`cutLevels_`time'' & treatment2=="Levels"
			drop if prop_conteo<`cutGains_`time'' & treatment2=="P4Pctile"
			eststo m_`var'_`time'_low: reghdfe `var'_`time' $treatmentlist i.Grade_`time' ${schoolcontrol} if Grade_`time'<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
			lincom _b[TreatmentLevels]
			scalar CI_Levels_1=r(estimate)+1.96*r(se)
			scalar CI_Levels_2=r(estimate)-1.96*r(se)
			lincom _b[TreatmentGains]
			scalar CI_Gains_1=r(estimate)+1.96*r(se)
			scalar CI_Gains_2=r(estimate)-1.96*r(se)
			lincom (_b[TreatmentGains]-_b[TreatmentLevels] )
			scalar CI_Diff_1=r(estimate)+1.96*r(se)
			scalar CI_Diff_2=r(estimate)-1.96*r(se)
			
		restore
		preserve
			gsort SchoolID Grade_`time' `var'_`time'
			by SchoolID Grade_`time': gen conteo=_n
			by SchoolID Grade_`time': gen denom=_N
			gen prop_conteo=conteo/denom
			drop if prop_conteo<`cutLevels_`time'' & treatment2=="Levels"
			drop if prop_conteo<`cutGains_`time'' & treatment2=="P4Pctile"
			eststo m_`var'_`time'_high: reghdfe `var'_`time' $treatmentlist i.Grade_`time' ${schoolcontrol} if Grade_`time'<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
			lincom _b[TreatmentLevels]
			scalar CI_Levels_3=r(estimate)+1.96*r(se)
			scalar CI_Levels_4=r(estimate)-1.96*r(se)
			lincom _b[TreatmentGains]
			scalar CI_Gains_3=r(estimate)+1.96*r(se)
			scalar CI_Gains_4=r(estimate)-1.96*r(se)
			lincom (_b[TreatmentGains]-_b[TreatmentLevels] )
			scalar CI_Diff_3=r(estimate)+1.96*r(se)
			scalar CI_Diff_4=r(estimate)-1.96*r(se)
		restore
		
		esttab  using "$latexcodes/RegTestScores_highstakes_PointLee_`var'_`time'.csv", fragment se ar2 label b(%9.2fc)se(%9.2fc)nocon nonumber nolines /// 
		star(* 0.10 ** 0.05 *** 0.01) ///
		replace  nomtitles ///
		keep(TreatmentLevels TreatmentGains) stats(N suma p, fmt(%9.0g %9.0g %9.0g) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value(\$\alpha_3=0\$)" "") star(suma)) ///
		nonotes substitute(\_ _)
	}
}

