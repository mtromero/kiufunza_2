**********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
fvset base default treatarm 
gen Diff_T2=date_twa_T2-date_edi_T2
gen Diff_T5=date_twa_T5-date_edi_T5
gen Week_T2=week(date_edi_T2)
gen Week_T5=week(date_edi_T5)


global AggregateDep 	Z_hisabati Z_kiswahili Z_kiingereza Z_sayansi Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal /*this should be added in the future...*/
global AggregateDep_Karthik 	Z_hisabati Z_kiswahili Z_kiingereza   /*Z_kiingereza  Z_ScoreFocal this should be added in the future...*/
global AggregateDep_int 	Z_hisabati Z_kiswahili Z_kiingereza
 
global AggregateDep_NonEng 	Z_hisabati Z_kiswahili 
global AggregateDep_Eng 	Z_kiingereza

global treatmentlist TreatmentLevels TreatmentGains
gen TreatmentLevels2=TreatmentLevels
gen TreatmentGains2=TreatmentGains
global treatmentlist2 TreatmentLevels2 TreatmentGains2
*enrollment2015_T1 Rural_T1 ClassesOutside_T1 Electricity_T1

      
*c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza)##c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza) ///


global student2 shoes_T2 socks_T2 dirty_T2 uniformdirty_T2 uniformtorn_T2  ringworm_T2 CloseToeShoe_T2

recode bookshome_T2 (-98=.)
global studentoutcomes studentsfight_T2 sing_T2 bookshome_T2

bys SchoolID: egen Z_kiswahili_T0_G=mean(Z_kiswahili_T0)
bys SchoolID: egen Z_kiingereza_T0_G=mean(Z_kiingereza_T0)
bys SchoolID: egen Z_hisabati_T0_G=mean(Z_hisabati_T0)
pca Z_hisabati_T0  Z_kiingereza_T0 Z_kiswahili_T0
predict Z_ScoreFocal_T0, score
*First lets create the tables Karthik Wants

label var Z_kiswahili_T2 Swahili
label var Z_kiswahili_T5 Swahili
label var Z_kiingereza_T2 English
label var Z_kiingereza_T5 English
label var Z_hisabati_T2 Math
label var Z_hisabati_T5 Math
label var Z_sayansi_T2 Science
label var Z_sayansi_T5 Science
label var Z_ScoreFocal_T2 "Focal Subjects"
label var Z_ScoreFocal_T5 "Focal Subjects" 
drop if GradeID_T2==. & GradeID_T5==.

		
keep $treatmentlist LagGrade LagseenUwezoTests MissingseenUwezoTests LagpreSchoolYN MissingpreSchoolYN LagGender MissingGender LagAge MissingAge LagZ_kiswahili LagZ_hisabati LagZ_kiingereza MissingZ_kiswahili MissingZ_hisabati MissingZ_kiingereza $schoolcontrol GradeID_*  SchoolID DistrictID StrataScore treatarm Z_hisabati* Z_kiswahili* Z_kiingereza* upid  IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement MissingIndexKowledge IndexKowledge 
drop if upid==""
drop *_T0
drop *_T0_G
*drop *_T1
drop *_T3
drop *_T4
drop *_T6

reshape long Z_hisabati@ Z_kiswahili@ Z_kiingereza@  GradeID@  ///
, i(upid) j(time) string

replace Z_kiingereza=. if time=="_T5" & GradeID<=2
encode time, gen(time2)


gen Combo=(treatarm==1)
gen COD=(treatarm==3)
gen Incentives=(treatarm==3 | treatarm==1)

reghdfe Z_hisabati $treatmentlist c.(${treatmentlist})#c.(Combo COD) i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3 & time2!=1, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2)
reghdfe Z_hisabati $treatmentlist c.(${treatmentlist})#c.Incentives  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3 & time2!=1, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2)


eststo clear
foreach var in  hisabati kiswahili   {
	foreach het in Combo COD Incentives{
		capture drop covar
		capture drop miss
		gen covar=`het'

			eststo:  reghdfe Z_`var' $treatmentlist c.(${treatmentlist})#c.covar i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3 & time2!=1, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2)
			estadd ysumm
		
	}  
}
esttab  using "$latexcodes/Hetero_PreviousT.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Math" "Swahili", pattern(1 0 0  1 0 0 ) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mtitles("Combo" "COD" "Incentives" ///
"Combo" "COD" "Incentives" ///
"Combo" "COD" "Incentives") ///
keep(c.TreatmentGains#c.covar c.TreatmentLevels#c.covar) stats(N, labels("N. of obs.") fmt(%9.0fc)) ///
varlabels(c.TreatmentGains#c.covar Gains*Covariate c.TreatmentLevels#c.covar Levels*Covariate  ) ///
fragment ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

