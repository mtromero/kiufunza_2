*******************************************************
*************BASE LINE YR1
********************************************************

use "$basein/1 Baseline/Student/Sample_noPII.dta", clear
drop _merge
compress
drop if usid==""
replace usid="R1STU"+usid
rename usid upidst
keep SchoolID upidst GradeID Age Gender preSchoolYN seenUwezoTests
drop if GradeID!=2
merge 1:1 upidst using "$basein/8 Endline 2014/Final Data/Student/R6Student_noPII.dta", keepus(consentChild kissyl1_1-othsub3_24)
drop if _merge==2
rename upidst upid
keep SchoolID GradeID upid Age Gender preSchoolYN seenUwezoTests kissyl1_1 -othsub3_24 consentChild
replace Age=Age+2
replace GradeID=4 if GradeID==2
save "$base_out/9 Baseline 2015/StudentGrade4.dta", replace


use "$basein/1 Baseline/Student/Sample_noPII.dta", clear
drop _merge
compress
drop if usid==""
replace usid="R1STU"+usid
rename usid upidst
keep SchoolID upidst GradeID Age Gender preSchoolYN seenUwezoTests
drop if GradeID!=1
merge 1:1 upidst using "$basein/8 Endline 2014/Final Data/Student/R6Student_noPII.dta", keepus(consentChild kissyl1_1-othsub3_24)
drop if _merge==2
rename upidst upid
keep SchoolID GradeID upid Age Gender preSchoolYN seenUwezoTests kissyl1_1 -othsub3_24 consentChild
replace Age=Age+2
replace GradeID=3 if GradeID==1
save "$base_out/9 Baseline 2015/StudentGrade3.dta", replace

use "$basein/6 Baseline 2014/Data/student/R4Student_noPII.dta",clear
gen GradeID=2
drop if consentChild==2 | consentChild==.
rename upid upidst
keep SchoolID upidst GradeID Age Gender preSchoolYN seenUwezoTests
merge 1:1 upidst using "$basein/8 Endline 2014/Final Data/Student/R6Student_noPII.dta", keepus(consentChild kissyl1_1-othsub3_24)
drop if _merge!=3
rename upidst upid
keep SchoolID GradeID upid Age Gender preSchoolYN seenUwezoTests kissyl1_1 -othsub3_24 consentChild
replace Age=Age+1
save "$base_out/9 Baseline 2015/StudentGrade2.dta", replace


use "$basein/9 Baseline 2015/Final Data/Student/R7Student_noPII.dta", clear
gen GradeID=1
drop if consentChild==.
keep SchoolID GradeID upid Age Gender preSchoolYN seenUwezoTests kissyl1_1 -othsub1_8 consentChild
save "$base_out/9 Baseline 2015/StudentGrade1.dta", replace

***************************
***************************
use "$base_out/9 Baseline 2015/StudentGrade4.dta", clear
append using "$base_out/9 Baseline 2015/StudentGrade3.dta"
append using "$base_out/9 Baseline 2015/StudentGrade2.dta"
append using "$base_out/9 Baseline 2015/StudentGrade1.dta"
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge

gen student_present=0
replace student_present=1 if  consentChild==1
rename student_present attendance

foreach var of varlist kissyl1_1 -othsub1_8{
	recode `var' (.=0) if (GradeID==1 | GradeID==2)  & attendance==1
}
foreach var of varlist kissyl2_1- othsub2_15{
	recode `var' (.=0) if GradeID==3  & attendance==1
}
foreach var of varlist kissyl3_1- othsub3_24{
	recode `var' (.=0) if GradeID==4  & attendance==1
}

foreach var of varlist kissyl1_1-othsub3_24 {
	recode `var' (2=0) (3=0) if  attendance==1
}



egen kissyl_mi=rowtotal(kissyl1_1 kissyl2_1 kissyl3_1),missing
egen kissyl_ra=rowtotal(kissyl1_2 kissyl2_4 kissyl3_4),missing
egen kissyl_fo=rowtotal(kissyl1_3 kissyl2_2 kissyl3_2),missing
egen kissyl_se=rowtotal(kissyl1_4 kissyl2_3 kissyl3_3),missing

egen kiswrd_sifuri=rowtotal(kiswrd1_2 kiswrd2_4 kiswrd3_4),missing
egen kiswrd_kiazi=rowtotal(kiswrd1_3 kiswrd2_2 kiswrd3_2),missing
egen kiswrd_duka=rowtotal(kiswrd1_4 kiswrd2_3 kiswrd3_3),missing
egen kiswrd_chuma=rowtotal(kiswrd1_5 kiswrd2_1 kiswrd3_1),missing
egen kiswrd_chandarua=rowtotal(kiswrd1_6 kiswrd2_9 kiswrd3_8),missing

egen kissen_1=rowtotal(kissen1_1 kissen2_2 kissen3_2),missing
egen kissen_2=rowtotal(kissen1_2 kissen2_1 kissen3_1),missing
egen kissen_3=rowtotal(kissen1_3 kissen2_3 kissen3_3),missing
egen kissen_4=rowtotal(kissen1_5 kissen2_4 kissen3_4),missing
egen kissen_5=rowtotal(kissen1_6 kissen2_7 kissen3_8),missing

egen kiswri_maji=rowtotal(kiswri1_1 kiswri2_1 kiswri3_1),missing
egen kiswri_bata=rowtotal(kiswri1_2 kiswri2_2 kiswri3_2),missing

egen englet_o=rowtotal(englet1_1 englet2_1 englet3_1),missing
egen englet_v=rowtotal(englet1_2 englet2_3 englet3_3),missing
egen englet_f=rowtotal(englet1_4 englet2_4 englet3_4),missing
egen englet_r=rowtotal(englet1_5 englet2_2 englet3_2),missing

egen engwrd_pot=rowtotal(engwrd1_1 engwrd2_1 engwrd3_1),missing
egen engwrd_red=rowtotal(engwrd1_2 engwrd2_2 engwrd3_2),missing
egen engwrd_two=rowtotal(engwrd1_4 engwrd2_3 engwrd3_3),missing
egen engwrd_dust=rowtotal(engwrd1_5 engwrd2_4 engwrd3_4),missing
egen engwrd_lion=rowtotal(engwrd1_6 engwrd2_5 engwrd3_8),missing

egen engsen_1=rowtotal(engsen1_1 engsen2_1 engsen3_1),missing
egen engsen_2=rowtotal(engsen1_2 engsen2_2 engsen3_2),missing
egen engsen_3=rowtotal(engsen1_4 engsen2_3 engsen3_3),missing
egen engsen_4=rowtotal(engsen1_5 engsen2_4 engsen3_4),missing
egen engsen_5=rowtotal(engsen1_6 engsen2_6 engsen3_6),missing

egen engwri_man=rowtotal(engwri1_1 engwri2_1 engwri3_1),missing
egen engwri_pot=rowtotal(engwri1_2 engwri2_2 engwri3_2),missing

egen hiscou_nine=rowtotal(hiscou1_2 hiscou2_2 hiscou3_2), missing
egen hiscou_five=rowtotal(hiscou1_3 hiscou2_1 hiscou3_1), missing

egen hisidn_7=rowtotal(hisidn1_1 hisidn2_1 hisidn3_1), missing
egen hisidn_58=rowtotal(hisidn1_2 hisidn2_2 hisidn3_2), missing

egen hisinq_1=rowtotal(hisinq1_1 hisinq2_1 hisinq3_1), missing
egen hisinq_2=rowtotal(hisinq1_2 hisinq2_2 hisinq3_2), missing
egen hisinq_3=rowtotal(hisinq1_3 hisinq2_4 hisinq3_4), missing

egen hisadd_1=rowtotal(hisadd1_1 hisadd2_1 hisadd3_1), missing
egen hisadd_2=rowtotal(hisadd1_2 hisadd2_2 hisadd3_2), missing
egen hisadd_3=rowtotal(hisadd1_4 hisadd2_3 hisadd3_3), missing

egen hissub_1=rowtotal(hissub1_1 hissub2_1 hissub3_1), missing
egen hissub_2=rowtotal(hissub1_2 hissub2_2 hissub3_2), missing
egen hissub_3=rowtotal(hissub1_4 hissub2_3 hissub3_3), missing

egen othsub_1=rowtotal(othsub1_1 othsub2_1 othsub3_1), missing
egen othsub_2=rowtotal(othsub1_2 othsub2_2 othsub3_2), missing
egen othsub_3=rowtotal(othsub1_3 othsub2_3 othsub3_3), missing
egen othsub_4=rowtotal(othsub1_4 othsub2_4 othsub3_4), missing
egen othsub_5=rowtotal(othsub1_5 othsub2_8 othsub3_8), missing
egen othsub_6=rowtotal(othsub1_6 othsub2_9 othsub3_9), missing
egen othsub_7=rowtotal(othsub1_7 othsub2_10 othsub3_10), missing
egen othsub_8=rowtotal(othsub1_8 othsub2_11 othsub3_11), missing


drop kissyl1_1 kissyl2_1 kissyl3_1 kissyl1_2 kissyl2_4 kissyl3_4 kissyl1_3 kissyl2_2 kissyl3_2 kissyl1_4 kissyl2_3 kissyl3_3 
drop kiswrd1_2 kiswrd2_4 kiswrd3_4 kiswrd1_3 kiswrd2_2 kiswrd3_2 kiswrd1_4 kiswrd2_3 kiswrd3_3 kiswrd1_5 kiswrd2_1 kiswrd3_1 kiswrd1_6 kiswrd2_9 kiswrd3_8
drop kissen1_1 kissen2_2 kissen3_2 kissen1_2 kissen2_1 kissen3_1 kissen1_3 kissen2_3 kissen3_3 kissen1_5 kissen2_4 kissen3_4 kissen1_6 kissen2_7 kissen3_8
drop kiswri1_1 kiswri2_1 kiswri3_1 kiswri1_2 kiswri2_2 kiswri3_2
drop englet1_1 englet2_1 englet3_1 englet1_2 englet2_3 englet3_3 englet1_4 englet2_4 englet3_4 englet1_5 englet2_2 englet3_2
drop engwrd1_1 engwrd2_1 engwrd3_1 engwrd1_2 engwrd2_2 engwrd3_2 engwrd1_4 engwrd2_3 engwrd3_3 engwrd1_5 engwrd2_4 engwrd3_4 engwrd1_6 engwrd2_5 engwrd3_8
drop engsen1_1 engsen2_1 engsen3_1 engsen1_2 engsen2_2 engsen3_2 engsen1_4 engsen2_3 engsen3_3 engsen1_5 engsen2_4 engsen3_4 engsen1_6 engsen2_6 engsen3_6
drop engwri1_1 engwri2_1 engwri3_1 engwri1_2 engwri2_2 engwri3_2
drop hiscou1_2 hiscou2_2 hiscou3_2 hiscou1_3 hiscou2_1 hiscou3_1
drop hisidn1_1 hisidn2_1 hisidn3_1 hisidn1_2 hisidn2_2 hisidn3_2
drop hisinq1_1 hisinq2_1 hisinq3_1 hisinq1_2 hisinq2_2 hisinq3_2 hisinq1_3 hisinq2_4 hisinq3_4
drop hisadd1_1 hisadd2_1 hisadd3_1 hisadd1_2 hisadd2_2 hisadd3_3 hisadd1_4 hisadd2_3 hisadd3_3
drop hissub1_1 hissub2_1 hissub3_1 hissub1_2 hissub2_2 hissub3_3 hissub1_4 hissub2_3 hissub3_3
drop othsub1_1 othsub2_1 othsub3_1 othsub1_2 othsub2_2 othsub3_2 othsub1_3 othsub2_3 othsub3_3 othsub1_4 othsub2_4 othsub3_4
drop othsub1_5 othsub2_8 othsub3_8 othsub1_6 othsub2_9 othsub3_9 othsub1_7 othsub2_10 othsub3_10 othsub1_8 othsub2_11 othsub3_11
 
egen kiswrd_kamalu=rowtotal(kiswrd2_5 kiswrd3_7),missing
egen kiswrd_mlango=rowtotal(kiswrd2_6 kiswrd3_6),missing
egen kiswrd_jicho=rowtotal(kiswrd2_7 kiswrd3_5),missing
egen kissen_6=rowtotal(kissen2_5 kissen3_6),missing
egen kissen_7=rowtotal(kissen2_6 kissen3_5),missing
egen kissen_8=rowtotal(kissen2_8 kissen3_7),missing
egen kispar_1=rowtotal(kispar3_1 kispar2_1), missing
egen kiswri_bustani=rowtotal(kiswri2_3 kiswri3_3), missing
egen kiswri_nyumba=rowtotal(kiswri2_4 kiswri3_4), missing
egen engwrd_maize=rowtotal(engwrd2_6 engwrd3_7),missing
egen engwrd_neck=rowtotal(engwrd2_7 engwrd3_6),missing
egen engwrd_hide=rowtotal(engwrd2_9 engwrd3_5),missing

egen engsen_6=rowtotal(engsen2_5 engsen3_5),missing
egen engsen_7=rowtotal(engsen2_7 engsen3_7),missing
egen engsen_8=rowtotal(engsen2_9 engsen3_8),missing
egen engwri_farm=rowtotal(engwri2_3 engwri3_3), missing
egen engwri_cake=rowtotal(engwri2_4 engwri3_4), missing

egen hisinq_4=rowtotal(hisinq2_3 hisinq3_3), missing

egen hisadd_4=rowtotal(hisadd2_5 hisadd3_4), missing
egen hisadd_5=rowtotal(hisadd2_6 hisadd3_6), missing

egen hissub_4=rowtotal(hissub2_4 hissub3_4), missing
egen hissub_5=rowtotal(hissub2_6 hissub3_6), missing

egen hismul_1=rowtotal(hismul2_1 hismul3_1), missing
egen hismul_2=rowtotal(hismul2_3 hismul3_2), missing
egen hismul_3=rowtotal(hismul2_4 hismul3_3), missing

egen othsub_9=rowtotal(othsub2_5 othsub3_5), missing
egen othsub_10=rowtotal(othsub2_6 othsub3_6), missing
egen othsub_11=rowtotal(othsub2_12 othsub3_12), missing
egen othsub_12=rowtotal(othsub2_13 othsub3_13), missing
egen othsub_13=rowtotal(othsub2_14 othsub3_14), missing
egen othsub_14=rowtotal(othsub2_7 othsub3_7), missing



drop kiswrd2_5 kiswrd3_7 kiswrd2_6 kiswrd3_6 kiswrd2_7 kiswrd3_5
drop kissen2_5 kissen3_6 kissen2_6 kissen3_5 kissen2_8 kissen3_7
drop kispar3_1 kispar2_1 
drop kiswri2_3 kiswri3_3 kiswri2_4 kiswri3_4
drop engwrd2_6 engwrd3_7 engwrd2_7 engwrd3_6 engwrd2_9 engwrd3_5
drop engsen2_5 engsen3_5 engsen2_7 engsen3_7 engsen2_9 engsen3_8
drop engwri2_3 engwri3_3 engwri2_4 engwri3_4
drop hisinq2_3 hisinq3_3 
drop hisadd2_5 hisadd3_4 hisadd2_6 hisadd3_6
drop hissub2_4 hissub3_4 hissub2_6 hissub3_6
drop hismul2_1 hismul3_1 hismul2_3 hismul3_2 hismul2_4 hismul3_3
drop othsub2_5 othsub3_5 othsub2_6 othsub3_6 othsub2_7 othsub2_7
drop othsub2_12 othsub3_12 othsub2_13 othsub3_13 othsub2_14 othsub3_14

irt (2pl kis*), intpoints(20) difficult
predict Z_kiswahili, latent 


irt (2pl eng*), intpoints(20) difficult
predict Z_kiingereza, latent 

irt (2pl his*), intpoints(20) difficult
predict Z_hisabati, latent 

irt (1pl oth*), intpoints(20) difficult
predict Z_sayansi, latent 

irt (1pl kis* his*), intpoints(20) difficult
predict Z_ScoreKisawMath, latent 

irt (1pl kis* eng* his*),intpoints(20)  difficult
predict Z_ScoreFocal, latent 




foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi Z_ScoreFocal Z_ScoreKisawMath {
	replace `var'=. if attendance==0
	sum `var' if  treatarm2==3
	replace `var'=(`var'-r(mean))/r(sd)
}

drop kis* eng* his* oth*


********************
fvset base 4 treatarm
tabstat Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi, by(treatment2)
reg Z_kiswahili i.(treatarm2 DistrictID StrataScore treatarm GradeID), vce(cluster SchoolID)
test (_b[1.treatarm2]-_b[2.treatarm2]-_b[3o.treatarm2]==0)
reg Z_hisabati i.(treatarm2 DistrictID StrataScore treatarm GradeID), vce(cluster SchoolID)
test (_b[1.treatarm2]-_b[2.treatarm2]-_b[3o.treatarm2]==0)
reg Z_kiingereza i.(treatarm2 DistrictID StrataScore treatarm GradeID), vce(cluster SchoolID)
test (_b[1.treatarm2]-_b[2.treatarm2]-_b[3o.treatarm2]==0)


rename * =_T1
foreach var in upid SchoolID DistrictID StrataScore treatment2 treatarm2 treatment treatarm{
	rename `var'_T1 `var'
}



foreach var in  preSchoolYN_T1 seenUwezoTests_T1 Gender_T1{
recode `var' (2=0) 
}
compress 
save "$base_out/9 Baseline 2015/Student_T1.dta", replace

/*
fvset base 3 treatarm2   
reg Z_kiswahili_T1 i.(treatarm2 DistrictID#StrataScore#treatarm GradeID), vce(cluster SchoolID)
reg Z_hisabati_T1 i.(treatarm2 DistrictID#StrataScore#treatarm GradeID), vce(cluster SchoolID)
reg Z_kiingereza_T1 i.(treatarm2 DistrictID#StrataScore#treatarm GradeID), vce(cluster SchoolID)
reg Age_T1 i.(treatarm2 DistrictID#StrataScore#treatarm GradeID), vce(cluster SchoolID)
reg Gender_T1 i.(treatarm2 DistrictID#StrataScore#treatarm GradeID), vce(cluster SchoolID)
reg preSchoolYN_T1 i.(treatarm2 DistrictID#StrataScore#treatarm GradeID), vce(cluster SchoolID)
reg seenUwezoTests_T1 i.(treatarm2 DistrictID#StrataScore#treatarm GradeID), vce(cluster SchoolID)
*/

*******************************************************
*************END LINE YR1
********************************************************
use "$basein/12 Intervention_KFII/1 Endline 2015/10 FINAL School Data/EL2015_School_CLEAN_nopii.dta", clear
gen date_twa=date(date,"DMY")
format date_twa %td
rename schoolid SchoolID
collapse (mean) date_twa, by(SchoolID)
save "$base_out/ConsolidatedYr34/TWA_Date_T2.dta", replace

import excel "$basein/12 Intervention_KFII/1 Endline 2015/10 FINAL School Data/Missing School Dates_EL2015.xlsx", clear firstrow 
gen date_twa=date(date,"DMY")
format date_twa %td
drop date 
rename schoolid SchoolID
merge 1:1 SchoolID using "$base_out/ConsolidatedYr34/TWA_Date_T2.dta", update replace
drop _merge
save "$base_out/ConsolidatedYr34/TWA_Date_T2.dta", replace

use "$basein/11 Endline 2015/Final Data/Student Data/R8Student_nopii.dta", clear
gen date_edi=dofc(time)
format date_edi %td
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
merge m:1 SchoolID using "$base_out/ConsolidatedYr34/TWA_Date_T2.dta"
drop if _merge==2
drop _merge


gen attgrade=1 if stdgrd==1 & atrgrd==.
replace attgrade=1 if atrgrd==1
replace attgrade=2 if stdgrd==2 & atrgrd==.
replace attgrade=2 if atrgrd==2
replace attgrade=3 if stdgrd==3 & atrgrd==.
replace attgrade=3 if atrgrd==3



gen student_present=0
replace student_present=1 if  consentChild==1
rename student_present attendance


gen date=dofc(timeStartTest)
format date %dN/D/Y
gen WeekIntvTest=week(date)


foreach var of varlist kispicl1_1- othsub1_8{
	recode `var' (.=0) if GradeID==1 & attendance==1
}
foreach var of varlist kispic2_1- othsub2_15{
	recode `var' (.=0) if GradeID==2 & attendance==1
}
foreach var of varlist kissyl3_1 - othsub3_13{
	recode `var' (.=0) if GradeID==3 & attendance==1
}
foreach var of varlist kissyl4_1- hisdiv4_9{
	recode `var' (.=0) if GradeID==4 & attendance==1
}


foreach var of varlist kispicl1_1-hisdiv4_9 {
	recode `var' (2=0) (3=0) 
}

foreach i of varlist kispicl1_1-hisdiv4_9 {
local a : variable label `i'
local a: subinstr local a " correctly" ""
label var `i' "`a'"
}





egen hiscou_1=rowtotal(hiscou1_2 hiscou2_1 hiscou3_1 hiscou4_1),missing

egen hisidn_1=rowtotal(hisidn1_2 hisidn3_1 hisidn4_1),missing 
egen hisidn_2=rowtotal(hisidn1_1 hisidn2_1 ),missing
egen hisidn_3=rowtotal(hisidn1_3 hisidn2_2),missing

egen hisinq_1=rowtotal(hisinq1_2 hisinq2_1 hisinq3_1 ),missing
egen hisinq_2=rowtotal(hisinq1_4 hisinq3_2 hisinq4_1),missing

egen hisadd_1=rowtotal(hisadd3_2 hisadd4_2 hisadd1_3 hisadd2_2),missing
egen hisadd_2=rowtotal(hisadd2_6 hisadd3_6 hisadd4_4),missing 
egen hisadd_3=rowtotal(hisadd1_4 hisadd2_4 hisadd4_3),missing
egen hisadd_4=rowtotal(hisadd1_1 hisadd2_1 hisadd3_1 hisadd4_1),missing
egen hisadd_5=rowtotal(hisadd2_3 hisadd3_3),missing
egen hisadd_6=rowtotal(hisadd2_5 hisadd3_4),missing

egen hissub_1=rowtotal(hissub1_3 hissub2_2 hissub3_2 hissub4_2 ),missing
egen hissub_2=rowtotal(hissub1_4 hissub2_4),missing
egen hissub_3=rowtotal(hissub2_5 hissub3_4),missing
egen hissub_4=rowtotal(hissub2_3 hissub3_3 hissub4_3),missing 
egen hissub_5=rowtotal(hissub2_6 hissub3_6 hissub4_4),missing
egen hissub_6=rowtotal(hissub1_1 hissub2_1 hissub3_1 hissub4_1),missing

egen hismul_1=rowtotal(hismul1_1 hismul2_1 hismul3_1 hismul4_1),missing
egen hismul_2=rowtotal(hismul2_3 hismul3_2 hismul4_2),missing
egen hismul_3=rowtotal(hismul2_4 hismul3_4 hismul4_3),missing

egen hisdiv_1=rowtotal(hisdiv3_5 hisdiv4_4),missing
egen hisdiv_2=rowtotal(hisdiv3_3 hisdiv4_3),missing
egen hisdiv_3=rowtotal(hisdiv2_2 hisdiv3_2 hisdiv4_2),missing
egen hisdiv_4=rowtotal(hisdiv2_1 hisdiv3_1 hisdiv4_1),missing

egen othsub_1=rowtotal(othsub1_5 othsub2_8 othsub3_6),missing
egen othsub_2=rowtotal(othsub2_3 othsub3_1),missing
egen othsub_3=rowtotal(othsub1_7 othsub2_10 othsub3_8),missing
egen othsub_4=rowtotal(othsub2_2 othsub3_2),missing
egen othsub_5=rowtotal(othsub2_1 othsub3_3),missing
egen othsub_6=rowtotal(othsub1_6 othsub2_9 othsub3_7),missing
egen othsub_7=rowtotal(othsub2_4 othsub3_4),missing
egen othsub_8=rowtotal(othsub2_7 othsub3_5), missing
egen othsub_9=rowtotal(othsub2_11 othsub3_9), missing

egen kispic_1=rowtotal(kispic1_1 kispic2_1),missing
egen kissyl_1=rowtotal(kissyl1_1 kissyl2_1 kissyl3_1 kissyl4_1 ),missing
egen kissyl_2=rowtotal(kissyl1_2 kissyl2_2 kissyl3_2 ),missing
egen kissyl_3=rowtotal(kissyl1_3 kissyl2_3 kissyl3_3 kissyl4_2),missing

egen kiswrd_1=rowtotal(kiswrd1_3 kiswrd2_3 kiswrd3_3),missing
egen kiswrd_2=rowtotal(kiswrd1_6 kiswrd2_7 kiswrd3_8 kiswrd4_4 ),missing
egen kiswrd_3=rowtotal(kiswrd1_2 kiswrd2_2 kiswrd3_2 ),missing
egen kiswrd_4=rowtotal(kiswrd1_5 kiswrd2_5 kiswrd3_6 ),missing
egen kiswrd_5=rowtotal(kiswrd1_4 kiswrd2_4 kiswrd3_5 kiswrd4_3 ),missing
egen kiswrd_6=rowtotal(kiswrd1_1 kiswrd2_1 kiswrd3_1 kiswrd4_1 ),missing
egen kiswrd_7=rowtotal(kiswrd2_6 kiswrd3_7),missing
egen kiswrd_8=rowtotal(kiswrd3_4 kiswrd4_2 ),missing

egen kissen_1=rowtotal(kissen1_1 kissen2_1 kissen3_1 kissen4_1),missing
egen kissen_2=rowtotal(kissen1_2 kissen2_2 kissen3_2),missing
egen kissen_3=rowtotal(kissen1_3 kissen2_3 kissen3_3),missing
egen kissen_4=rowtotal(kissen1_4 kissen2_4 kissen3_5),missing
egen kissen_5=rowtotal(kissen1_5 kissen2_5 kissen3_6),missing
egen kissen_6=rowtotal(kissen1_6 kissen2_7 kissen3_8 kissen4_4),missing
egen kissen_7=rowtotal(kissen2_6 kissen3_7 kissen4_3),missing
egen kissen_8=rowtotal(kissen3_4 kissen4_2),missing

egen kiswri_1=rowtotal(kiswri2_3 kiswri3_3 kiswri4_3),missing 
egen kiswri_2=rowtotal(kiswri3_5 kiswri4_5),missing
egen kiswri_3=rowtotal(kiswri1_1 kiswri2_1 kiswri3_1 kiswri4_1 ),missing
egen kiswri_4=rowtotal(kiswri1_2 kiswri2_2 kiswri3_2 kiswri4_2 ),missing
egen kiswri_5=rowtotal(kiswri2_4 kiswri3_4 kiswri4_4),missing
egen kiswri_6=rowtotal(kiswri3_6 kiswri4_6),missing

egen kispar_1=rowtotal(kispar1_1 kispar2_1 kispar3_1 )
egen kispar_2=rowtotal(kispar1_2 kispar2_2 kispar3_2)
egen kispar_3=rowtotal(kispar2_3  kispar3_3)
egen kispar_4=rowtotal(kispar2_5 kispar3_4)

egen kissto_1=rowtotal(kissto3_1 kissto4_1) 
egen kiscom_1=rowtotal(kiscom3_2 kissto4_2)
egen kiscom_2=rowtotal(kiscom3_3 kissto4_3)

egen engpic_1=rowtotal(engpic1_2 engpic2_1 engpic3_1 ),missing
egen engpic_2=rowtotal(engpic1_3 engpic2_2 engpic3_2 engpic4_1),missing

egen engpicl_1=rowtotal(engpicl1_1 engpicl2_1 engpicl3_1 engpicl4_1 ),missing
egen engpicl_2=rowtotal(engpicl1_2 engpicl2_2 engpicl3_2),missing

egen englet_1=rowtotal(englet1_1 englet2_1 englet3_1 englet4_1 ),missing
egen englet_2=rowtotal(englet1_2 englet2_2 englet3_2 ),missing
egen englet_3=rowtotal(englet1_3 englet2_3 englet3_3 englet4_2),missing

egen engwrd_1=rowtotal(engwrd2_5 engwrd3_4 ),missing
egen engwrd_2=rowtotal(engwrd1_3 engwrd2_4 engwrd3_3 engwrd4_2),missing
egen engwrd_3=rowtotal(engwrd1_2 engwrd2_3 engwrd3_2 ),missing
egen engwrd_4=rowtotal(engwrd2_8 engwrd3_7 engwrd4_4 ),missing
egen engwrd_5=rowtotal(engwrd1_1 engwrd2_2 engwrd3_1 engwrd4_1),missing
egen engwrd_6=rowtotal(engwrd2_7 engwrd3_6 ),missing
egen engwrd_7=rowtotal(engwrd1_5 engwrd2_6 engwrd3_5 engwrd4_3),missing

egen engsen_1=rowtotal(engsen1_3 engsen2_3 engsen3_3),missing
egen engsen_2=rowtotal(engsen2_8 engsen3_6 engsen4_3),missing
egen engsen_3=rowtotal(engsen1_4 engsen2_4 engsen3_4),missing
egen engsen_4=rowtotal(engsen1_2 engsen2_2 engsen3_2 engsen4_1),missing
egen engsen_5=rowtotal(engsen1_1 engsen2_1 engsen3_1),missing
egen engsen_6=rowtotal(engsen2_6 engsen3_5 engsen4_2),missing

egen engwri_1=rowtotal(engwri2_4 engwri3_4 engwri4_4 ),missing
egen engwri_2=rowtotal(engwri1_1 engwri2_1 engwri3_1 engwri4_1 ),missing
egen engwri_3=rowtotal(engwri3_6 engwri4_6),missing
egen engwri_4=rowtotal(engwri2_3 engwri3_3 engwri4_3 ),missing
egen engwri_5=rowtotal(engwri3_5 engwri4_5 ),missing
egen engwri_6=rowtotal(engwri1_2 engwri2_2 engwri3_2 engwri4_2),missing 

egen engpar_1=rowtotal(engpar2_1 engpar3_1 engpar4_1)

egen engsto_1=rowtotal(engsto3_1 engsto4_1)
egen engcom_1=rowtotal(engcom3_1 engsto4_2)
egen engcom_2=rowtotal(engcom3_3 engsto4_3)

drop othsub2_7 othsub3_5 othsub2_11 othsub3_9
drop engsto3_1 engsto4_1 engcom3_1 engsto4_2 engcom3_3 engsto4_3
drop kissto3_1 kissto4_1 kiscom3_2 kissto4_2 kiscom3_3 kissto4_3 engsen3_1
drop kispar2_1 kispar3_1 kispar4_1 kispar1_2 kispar2_2 kispar3_2 kispar2_3  kispar3_3 kispar2_5 kispar3_4
drop engpar2_1 engpar3_1 engpar4_1 hiscou1_2 engsen2_1 engwri2_1 engwri2_3 engwri2_4 engwri2_2
drop kissen1_1 kissen2_1 kissen3_1 kissen4_1 kissen1_2 kissen2_2 kissen3_2 kissen1_3 kissen2_3 kissen3_3 kissen1_4 kissen2_4 kissen3_5 kissen1_5 kissen2_5 kissen3_6
drop kissen1_6 kissen2_7 kissen3_8 kissen4_4 kissen2_6 kissen3_7 kissen4_3 kissen3_4 kissen4_2
drop engsen1_1 engsen1_2 engsen1_3 engsen1_4 hisdiv2_2

drop hisadd3_2 hisadd4_2 hisadd1_3 hisadd2_2 hisadd2_6 hisadd3_6 hisadd4_4 hisadd1_4 hisadd2_4 hisadd4_3 hisadd1_1 hisadd2_1 hisadd3_1 hisadd4_1 
drop hisadd2_3 hisadd3_3 hisadd2_5 hisadd3_4 hiscou2_1 hiscou3_1 hiscou4_1 hisdiv3_5 hisdiv4_4 hisdiv3_3 hisdiv4_3 hisdiv3_2 hisdiv4_2 hisdiv2_1 hisdiv3_1 hisdiv4_1
drop othsub1_5 othsub2_8 othsub3_6 othsub2_3 othsub3_1 othsub1_7 othsub2_10 othsub3_8 othsub2_2 othsub3_2 othsub2_1 othsub3_3 othsub1_6 othsub2_9 othsub3_7
drop hisidn1_2 hisidn3_1 hisidn4_1  hisidn1_1 hisidn2_1 hisidn1_3 hisidn2_2 hisinq1_2 hisinq2_1 hisinq3_1  hisinq1_4 hisinq3_2 hisinq4_1
drop hismul1_1 hismul2_1 hismul3_1 hismul4_1 hismul2_3 hismul3_2 hismul4_2 hismul2_4 hismul3_4 hismul4_3 othsub2_4 othsub3_4 kissyl1_1 kissyl2_1 kissyl3_1 kissyl4_1 
drop kissyl1_2 kissyl2_2 kissyl3_2 kissyl1_3 kissyl2_3 kissyl3_3 kissyl4_2 engsen2_3 engsen3_3 engsen2_8 engsen3_6 engsen4_3 engsen2_4 engsen3_4 engsen2_2 engsen3_2 engsen4_1
drop engsen2_6 engsen3_5 engsen4_2 engwrd2_5 engwrd3_4 kiswrd1_3 kiswrd2_3 kiswrd3_3
drop kiswrd1_6 kiswrd2_7 kiswrd3_8 kiswrd4_4 kiswrd1_2 kiswrd2_2 kiswrd3_2 engwrd1_3 engwrd2_4 engwrd3_3 engwrd4_2 engwrd1_2 engwrd2_3 engwrd3_2 engwrd2_8 engwrd3_7 engwrd4_4 
drop kiswrd1_5 kiswrd2_5 kiswrd3_6 engwrd1_1 engwrd2_2 engwrd3_1 engwrd4_1 kiswrd1_4 kiswrd2_4 kiswrd3_5 kiswrd4_3 kiswrd1_1 kiswrd2_1 kiswrd3_1 kiswrd4_1 kiswrd2_6 kiswrd3_7
drop kiswrd3_4 kiswrd4_2 engwrd2_7 engwrd3_6 engwrd1_5 engwrd2_6 engwrd3_5 engwrd4_3 engpic1_2 engpic2_1 engpic3_1 engpicl1_1 engpicl2_1 engpicl3_1 engpicl4_1 kispic1_1 kispic2_1
drop engpic1_3 engpic2_2 engpic3_2 engpic4_1 engpicl1_2 engpicl2_2 engpicl3_2 hissub1_3 hissub2_2 hissub3_2 hissub4_2 hissub1_4 hissub2_4 hissub2_5 hissub3_4 hissub2_3 hissub3_3 hissub4_3 
drop hissub2_6 hissub3_6 hissub4_4 hissub1_1 hissub2_1 hissub3_1 hissub4_1 englet1_1 englet2_1 englet3_1 englet4_1  englet1_2 englet2_2 englet3_2 englet1_3 englet2_3 englet3_3 englet4_2
drop engwri3_4 engwri4_4 kiswri2_3 kiswri3_3 kiswri4_3 engwri1_1 engwri3_1 engwri4_1 engwri3_6 engwri4_6 kiswri3_5 kiswri4_5 kiswri1_1 kiswri2_1 kiswri3_1 kiswri4_1
drop kiswri1_2 kiswri2_2 kiswri3_2 kiswri4_2  kiswri2_4 kiswri3_4 kiswri4_4 engwri3_3 engwri4_3 engwri3_5 engwri4_5 engwri1_2 engwri3_2 engwri4_2 kiswri3_6 kiswri4_6


/*
*Calculcate number of questions per skill
egen Kis_Silabi=rowtotal(kissyl*), missing
egen Kis_Maneno=rowtotal(kiswrd*), missing
egen Kis_Sentenci=rowtotal(kissen*), missing
egen Kis_Aya=rowtotal(kispar*), missing
egen Kis_Story=rowtotal(kissto*), missing
egen Kis_Comp=rowtotal(kiscom*) , missing 

egen Eng_Letter=rowtotal(englet_*), missing
egen Eng_Word=rowtotal(engwrd*), missing
egen Eng_Sentences=rowtotal(engsen*), missing
egen Eng_Paragraph=rowtotal(engpar*), missing
egen Eng_Story=rowtotal(engsto*), missing
egen Eng_Comp=rowtotal(engcom*), missing

egen Math_id=rowtotal(hisidn*), missing
egen Math_uta=rowtotal(hiscou*), missing
egen Math_bwa=rowtotal(hisidn*), missing
egen Math_j=rowtotal(hisadd*), missing
egen Math_t=rowtotal(hissub*), missing
egen Math_z=rowtotal(hismul*), missing
egen Math_g=rowtotal(hisdiv*), missing



 



*generate passing dummies. Have to be careful since stata thinks missing is infinity
foreach var of varlist Kis_Silabi   Eng_Letter   {
	gen `var'_Pass=`var'>=3 if !missing(`var')
}

foreach var of varlist  Kis_Maneno Kis_Sentenci  Eng_Word Eng_Sentences {
	gen `var'_Pass=`var'>=4 if !missing(`var')
}

foreach var of varlist Math_id Math_uta Math_bwa Math_j Math_t Math_z Math_g {
	gen `var'_Pass=`var'>=2 if !missing(`var')
}

foreach var of varlist  Kis_Aya Eng_Paragraph Kis_Story Eng_Story{
	gen `var'_Pass=`var'>=1 if !missing(`var')
}

foreach var of varlist  Kis_Comp Eng_Comp {
	gen `var'_Pass=`var'>=2 if !missing(`var')
}
*/





irt (2pl kis*), intpoints(20) difficult
predict Z_kiswahili, latent 

irtgraph tif
graph export "$graphs/TIF_IRT_kiswahili_T2.pdf", replace

irt (2pl eng*), intpoints(20) difficult
predict Z_kiingereza, latent 

irtgraph tif
graph export "$graphs/TIF_IRT_english_T2.pdf", replace

irt (2pl his*), intpoints(20) difficult
predict Z_hisabati, latent 

irtgraph tif
graph export "$graphs/TIF_IRT_math_T2.pdf", replace

irt (1pl oth*), intpoints(20) difficult
predict Z_sayansi, latent 
replace Z_sayansi=. if GradeID==4


irt (1pl kis* his*), intpoints(20) difficult
predict Z_ScoreKisawMath, latent 

irtgraph tif
graph export "$graphs/TIF_IRT_ScoreKisawMath_T2.pdf", replace

irt (1pl kis* his* eng*), intpoints(20) difficult
predict Z_ScoreFocal, latent 



foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi Z_ScoreKisawMath Z_ScoreFocal {
	replace `var'=. if attendance==0
	sum `var' if  treatarm2==3
	replace `var'=(`var'-r(mean))/r(sd)
}


drop kis* eng* his* oth*

recode shoes socks dirty uniformdirty uniformtorn  ringworm bookshome sing missschool (2=0)
recode studentsfight (3=0) (4=0) (2=1)
replace sing=0 if singfreq==3 | singfreq==4
gen CloseToeShoe=(shoetype==2) if !missing(shoetype)



rename * =_T2
foreach var in upid SchoolID DistrictID StrataScore treatment2 treatarm2 treatment treatarm{
	rename `var'_T2 `var'
}







compress 
save "$base_out/11 Endline 2015/Student_T2.dta", replace






*********** ERNDLINE 2015***********
******* OTHER THINGS **********8
*****************************

use "$basein/11 Endline 2015/Final Data/Student Data/R8Subject_nopii.dta", clear
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
merge m:1 SchoolID GradeID  R8StudentID using "$basein/11 Endline 2015/Final Data/Student Data/R8Student_nopii.dta", keepus(upid)
drop if _merge!=3
drop _merge

drop if subnottaught==1
drop if subnottaught==.
recode teacherhelp knowname callname harsh missdays presents homework assignmentgraded (2=0)
recode callname numdays (.=0)
recode teacherhit teacherleave (1/2=1) (3/5=0)
recode assignmentlength (-95=.)
replace assignmentlength=assignmentlength*0.71 if  booklarge==2 /*use ratio of area */
gen Nofeedback  =(bookfeedback_95!=.) & !missing(bookfeedback)
keep upid SchoolID GradeID R8StudentID R8SubjectID teacherhelp -treatarm
rename R8StudentID StudentID
rename R8SubjectID SubjectID
rename * =_T2
foreach var in upid SchoolID StudentID SubjectID DistrictID StrataScore treatment2 treatarm2 treatment treatarm{
rename `var'_T2 `var'
}
save "$base_out/11 Endline 2015/StudentSubject_T2.dta", replace



use "$basein/11 Endline 2015/Final Data/Student Data/R8TeacherHelp_nopii.dta", clear
recode schoolhelp weekendhelp (2=0)
collapse (max) schoolhelp weekendhelp , by(SchoolID GradeID R8StudentID R8SubjectID)
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
merge m:1 SchoolID GradeID  R8StudentID using "$basein/11 Endline 2015/Final Data/Student Data/R8Student_nopii.dta", keepus(upid)
drop if _merge!=3
drop _merge
rename schoolhelp schoolhelp_T2
rename weekendhelp weekendhelp_T2
rename GradeID GradeID_T2
rename R8StudentID StudentID
rename R8SubjectID SubjectID
save "$base_out/11 Endline 2015/StudentHelp_T2.dta", replace



*********** ERNDLINE 2015***********
******* twaweza test **********8
*****************************
use "$basein/12 Intervention_KFII/1 Endline 2015/AllStudentsTested_2015.dta", clear
*use "$basein/12 Intervention_KFII/1 Endline 2015/AllStudentsTested_2015.dta", clear
drop  studentid_merge set_merge stream_merge gender_merge FifthID updatedIDLength IDLength stream_missing2 name_numb pos_space 

rename districtid DistrictID
rename grade Grade
rename stream Stream
rename tested2015 TestedEL2015
replace TestedEL2015=0 if TestedEL2015==.
keep DistrictID SchoolID Grade Stream studentid gender TestedEL2015 grd1_kis_a_si_1-grd3_his_c_g_3 treatarm2 heldback set
drop *his_b_* *his_c_* *kis_b_* *kis_c_* *eng_b_* *eng_c_*
drop if TestedEL2015!=1
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge==1
drop _merge

foreach var of varlist grd1*{
	replace `var'=. if Grade!=1 | TestedEL2015==0
	replace `var'=0 if `var'==. & Grade==1 & TestedEL2015==1
}

foreach var of varlist grd2*{
	replace `var'=. if Grade!=2 | TestedEL2015==0
	replace `var'=0 if `var'==. & Grade==2 & TestedEL2015==1
}

foreach var of varlist grd3*{
	replace `var'=. if Grade!=3 | TestedEL2015==0
	replace `var'=0 if `var'==. & Grade==3 & TestedEL2015==1
}


*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in si ma se{
	 forvalues i=3/6{
		replace grd1_kis_a_`skill'_`i'=0 if grd1_kis_a_`skill'_1==0 & grd1_kis_a_`skill'_2==0 & Grade==1 & TestedEL2015==1
	}
}

foreach skill in l w se{
	forvalues i=3/6{
		replace grd1_eng_a_`skill'_`i'=0 if grd1_eng_a_`skill'_1==0 & grd1_eng_a_`skill'_2==0 & Grade==1 & TestedEL2015==1
	}
}

foreach skill in id uta bwa j t{
	replace grd1_his_a_`skill'_3=0 if grd1_his_a_`skill'_1==0 & grd1_his_a_`skill'_2==0 & Grade==1 & TestedEL2015==1
} 



*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in ma se{
	 forvalues i=3/6{
		replace grd2_kis_a_`skill'_`i'=0 if grd2_kis_a_`skill'_1==0 & grd2_kis_a_`skill'_2==0 & Grade==2 & TestedEL2015==1
	} 
}

foreach skill in w se{
	forvalues i=3/6{
		replace grd2_eng_a_`skill'_`i'=0 if grd2_eng_a_`skill'_1==0 & grd2_eng_a_`skill'_2==0 & Grade==2 & TestedEL2015==1
	}
}

foreach skill in  bwa j t z{
	replace grd2_his_a_`skill'_3=0 if grd2_his_a_`skill'_1==0 & grd2_his_a_`skill'_2==0 & Grade==2 & TestedEL2015==1
} 




*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_kis_a_m_1=0 if grd3_kis_a_h==0  & Grade==3 & TestedEL2015==1
replace grd3_kis_a_m_2=0 if grd3_kis_a_h==0  & Grade==3 & TestedEL2015==1


*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_eng_a_c_1=0 if grd3_eng_a_s==0 & Grade==3 & TestedEL2015==1
replace grd3_eng_a_c_2=0 if grd3_eng_a_s==0 & Grade==3 & TestedEL2015==1

*First data cleaning process for math is as before...
foreach skill in  j t z g{
	replace grd3_his_a_`skill'_3=0 if grd3_his_a_`skill'_1==0 & grd3_his_a_`skill'_2==0 & Grade==3 & TestedEL2015==1
} 

drop grd2_eng_a_se_5 /*March 3rd, 2017: for some reason this appears only for gains students... WTF?*/



forvalues val=1/3{
	foreach var of varlist grd`val'_* {
			forvalues set_num =1/10 {
				qui sum `var' if Grade==`val' & treatarm2==3 & set==`set_num'
				qui capture gen Z_`var'=.
				qui replace Z_`var'= (`var'-r(mean))/r(sd) if Grade==`val' & set==`set_num'
			}
	}
}



gen Z_kiswahili=.
gen Z_kiingereza=.
gen Z_hisabati=.
forvalues val=1/3{
	pca Z_grd`val'_kis_*
	predict temp, score
	replace Z_kiswahili=temp if Grade==`val'
	drop temp
	pca Z_grd`val'_his_*
	predict temp, score
	replace Z_hisabati=temp if Grade==`val'
	drop temp
	pca Z_grd`val'_eng_*
	predict temp, score
	replace Z_kiingereza=temp if Grade==`val'
	drop temp
}


***Now we standarize by grade subject
forvalues val=1/3{
	foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati  {
		sum `var' if Grade==`val' & treatarm2==3
		replace `var'=(`var'-r(mean))/r(sd) if Grade==`val'
	}
}


foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati {
sum `var' if  treatarm2==3
replace `var'=(`var'-r(mean))/r(sd)
}

pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score

pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score
drop Z_grd1_kis_a_si_1-Z_grd1_his_a_t_3 Z_grd2_kis_a_ma_1-Z_grd3_his_a_g_3


*Calculcate number of questions per skill
egen Kis_Silabi=rowtotal(grd*_kis_a_si_*), missing
egen Kis_Maneno=rowtotal(grd*_kis_a_ma_*), missing
egen Kis_Sentenci=rowtotal(grd*_kis_a_se_*), missing
egen Eng_Letter=rowtotal(grd*_eng_a_l_*), missing
egen Eng_Word=rowtotal(grd*_eng_a_w_*), missing
egen Eng_Sentences=rowtotal(grd*_eng_a_se_*), missing
egen Math_id=rowtotal(grd*_his_a_id_*), missing
egen Math_uta=rowtotal(grd*_his_a_uta_*), missing
egen Math_bwa=rowtotal(grd*_his_a_bwa_*), missing
egen Math_j=rowtotal(grd*_his_a_j_*), missing
egen Math_t=rowtotal(grd*_his_a_t_*), missing
egen Math_z=rowtotal(grd*_his_a_z_*), missing
egen Math_g=rowtotal(grd*_his_a_g_*), missing
gen Kis_Aya=grd2_kis_a_a
gen Eng_Paragraph=grd2_eng_a_p
gen Kis_Story=grd3_kis_a_h
egen Kis_Comp=rowtotal(grd*_kis_a_m_*) , missing  
gen Eng_Story=grd3_eng_a_s
egen Eng_Comp=rowtotal(grd*_eng_a_c_*), missing
 



*generate passing dummies. Have to be careful since stata thinks missing is infinity
foreach var of varlist Kis_Silabi Kis_Maneno Kis_Sentenci Eng_Letter Eng_Word Eng_Sentences {
	gen `var'_Pass=`var'>=4 if !missing(`var')
	
	
	twoway (histogram `var' if treatment2=="Control", percent fcolor(red) fintensity(100) lcolor(red) barwidth(0.4)) ///
	(histogram `var' if treatment2=="Levels", percent fcolor(ltblue) fintensity(100) lcolor(ltblue) barwidth(0.25)) ///
	(histogram `var' if treatment2=="Gains", percent fcolor(gray) fintensity(100) lcolor(gray) barwidth(0.1)), ///
	ytitle(% of students) ylabel(, angle(horizontal) nogrid) xtitle(Correct answers) xline(4, lwidth(thick) lpattern(dash) lcolor(black)) ///
	legend(order(1 "Control" 2 "Levels" 3 "Gains") rows(1) region(fcolor(none) margin(zero) lcolor(none)) bmargin(zero)) ///
	graphregion(fcolor(white)) plotregion(fcolor(white))
	graph export "$graphs/histogram`var'_T3.pdf", replace
	
	
	
}

foreach var of varlist Math_id Math_uta Math_bwa Math_j Math_t Math_z Math_g {
	gen `var'_Pass=`var'>=2 if !missing(`var')
	
	twoway (histogram `var' if treatment2=="Control", percent fcolor(red) fintensity(100) lcolor(red) barwidth(0.4)) ///
	(histogram `var' if treatment2=="Levels", percent fcolor(ltblue) fintensity(100) lcolor(ltblue) barwidth(0.25)) ///
	(histogram `var' if treatment2=="Gains", percent fcolor(gray) fintensity(100) lcolor(gray) barwidth(0.1)), ///
	ytitle(% of students) ylabel(, angle(horizontal) nogrid) xtitle(Correct answers) xline(2, lwidth(thick) lpattern(dash) lcolor(black)) ///
	legend(order(1 "Control" 2 "Levels" 3 "Gains") rows(1) region(fcolor(none) margin(zero) lcolor(none)) bmargin(zero)) ///
	graphregion(fcolor(white)) plotregion(fcolor(white))
	graph export "$graphs/histogram`var'_T3.pdf", replace
}

foreach var of varlist  Kis_Aya Eng_Paragraph Kis_Story Eng_Story{
	gen `var'_Pass=`var'==3 if !missing(`var')
}

foreach var of varlist  Kis_Comp Eng_Comp {
	gen `var'_Pass=`var'==2 if !missing(`var')
}

drop grd1_kis_a_si_1-grd1_his_a_t_3 grd2_kis_a_ma_1-grd3_his_a_g_3

drop Stream volsign volname end endampm 
drop if treatarm2==.
drop Kis_Silabi- Eng_Comp
rename * =_T3
foreach var in DistrictID SchoolID studentid  StrataScore treatment2 treatarm2 treatment treatarm{
rename `var'_T3 `var'
}

replace gender_T3="" if gender_T3=="--blank--"
encode gender_T3, gen(gender_T32)
drop gender_T3
gen gender_T3=gender_T32
drop gender_T32

save "$base_out/11 Endline 2015/Student_T3.dta", replace



*********** ERNDLINE 2014***********
******* twaweza baseline **********8
*****************************

use  "$basein/4 Intervention/TwaEL_2014/TwaTestData_2014_allstudents.dta", clear
*drop _merge
drop SchoolID_15_s Grade_15_s USchoolID UGrade UStuID StuName_s StuName_sound UStuName_sound uniqid2 dupmergscore uniqid1 studnum studnum_s Darasa_s SchoolID_s SchGrd SchGrd_s SchGrd_group v10 StuID_el schgrdnum schgrdnum_15 schgrdnum_15_s StuID  number stuid_dup
foreach var in DistrictID SchoolID Grade Stream{
	replace `var'_el=`var' if missing(`var'_el)
}
gen NoInfoBL=(DistrictID==.)
drop DistrictID SchoolID Grade Stream
foreach var in DistrictID SchoolID Grade Stream date StuTest Kis_SI Kis_MA Kis_SE Kis_A Kis_H Kis_M Kis_Pass Eng_L Eng_W Eng_SE Eng_P Eng_S Eng_C Eng_Pass Math_ID Math_UTA Math_BWA Math_J Math_T Math_Z Math_G Math_Pass VolName start end time_session stud_session time_pertest treatment{
	rename `var'_el `var'
}
drop treatment
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge==1
drop _merge


 
 
 *First we need to clean the data a bit
 replace Kis_SI=. if Kis_SI==8
 replace Kis_MA=. if Kis_MA==6
 replace Kis_SE=. if Kis_SE==6
 replace Kis_A=. if Kis_A>1
 replace Kis_H=. if Kis_H>1
 replace Eng_SE=. if Eng_SE==6
 replace Eng_P=. if Eng_P>1
 replace Eng_S=. if Eng_S>1
 replace Math_J=. if Math_J==14
 replace Math_T=. if Math_T==6
 replace Math_G=. if Math_G==6
 replace  Kis_H=. if Grade==2
 replace  Kis_M=. if Grade==2
 replace  Kis_MA=. if Grade==3
 replace  Kis_SE=. if Grade==3
 replace  Kis_A=. if Grade==3
 replace  Eng_W=. if Grade==3
 replace  Eng_SE=. if Grade==3
 replace  Eng_P=. if Grade==3
 replace  Eng_S=. if Grade==2
 replace  Eng_C=. if Grade==2
 replace  Math_G=. if Grade==2
 replace Math_BWA=. if Grade==3
     

 drop Kis_Pass Eng_Pass Math_Pass
 preserve
 keep Kis_SI- Math_G StuID_15
 rename StuID_15 studentid 
 save  "$basein/4 Intervention/TwaEL_2014/StudentRaw_T0.dta", replace
 restore
 

irt  (2pl Eng_P Eng_S Kis_A Kis_H) (pcm Math* Kis_SI Kis_MA Kis_SE Kis_M Eng_L Eng_W Eng_SE Eng_C), difficult  intpoints(20)	
predict IRT, latent 	
	
forvalues val=1/3{
	foreach var of varlist Kis_SI-Math_G  {
		sum `var' if Grade==`val' & treatarm2==3
		capture gen Z_`var'=.
		replace Z_`var'= (`var'-r(mean))/r(sd) if Grade==`val'
	}
}

egen Z_kiswahili=rowtotal(Z_Kis*),missing
egen Z_kiingereza=rowtotal(Z_Eng*),missing
egen Z_hisabati=rowtotal(Z_Math*),missing





***Now we standarize by grade subject
forvalues val=1/3{
	foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati  {
		sum `var' if Grade==`val' & treatarm2==3
		replace `var'=(`var'-r(mean))/r(sd) if Grade==`val'
	}
}


foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati {
	sum `var' if  treatarm2==3
	replace `var'=(`var'-r(mean))/r(sd)
}


keep Z_kiswahili Z_kiingereza Z_hisabati StuID_15 SchoolID Grade IRT
drop if Z_kiswahili==. & Z_kiingereza==. & Z_hisabati==.
rename StuID_15 studentid

rename * =_T0
foreach var in studentid{
	rename `var'_T0 `var'
}

save  "$basein/4 Intervention/TwaEL_2014/Student_T0.dta", replace




*******************************************************
*************BASE LINE YR2
********************************************************
use "$basein/13 Baseline 2016/Final Data/Student Data_nopii/R9Student_nopii.dta", clear
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge



gen student_present=0
replace student_present=1 if  consentChild==1
rename student_present attendance


gen date=dofc(timeStartTest)
format date %dN/D/Y
gen WeekIntvTest=week(date)


foreach var of varlist kispicl1_1- othsub1_8{
	recode `var' (.=0) if  attendance==1
}


foreach var of varlist kispicl1_1-othsub1_8 {
	recode `var' (2=0) (3=0) 
}

drop engwrd1_3 engsen1_2  engsen1_3

irt (2pl kis*), intpoints(20) difficult
predict Z_kiswahili, latent 

irt (1pl eng*), intpoints(20) difficult
predict Z_kiingereza, latent 

irt (2pl his*), intpoints(20) difficult
predict Z_hisabati, latent 

irt (1pl oth*), intpoints(20) difficult
predict Z_sayansi, latent 

irt (1pl kis* his*),  intpoints(20) difficult
predict Z_ScoreKisawMath, latent 

irt (1pl kis* eng* his*),  intpoints(20) difficult
predict Z_ScoreFocal, latent 





foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi Z_ScoreKisawMath Z_ScoreFocal {
	replace `var'=. if attendance==0
	sum `var' if  treatarm2==3
	replace `var'=(`var'-r(mean))/r(sd)
}




drop kis* eng* his* oth*

gen GradeID=1

rename * =_T4
foreach var in upid SchoolID DistrictID StrataScore treatment2 treatarm2 treatment treatarm{
	rename `var'_T4 `var'
}

compress 
save "$base_out/13 Baseline 2016/Student_T4.dta", replace




*******************************************************
*************END LINE YR2
********************************************************
use "$basein/12 Intervention_KFII/4 Endline 2016/6_TwaEL2016_School_Control_FINAL_nopii.dta", clear
gen date_twa=date(date,"DMY")
format date_twa %td
rename schoolid SchoolID
collapse (mean) date_twa, by(SchoolID)
save "$base_out/ConsolidatedYr34/TWA_Date_T5.dta", replace

use "$basein/12 Intervention_KFII/4 Endline 2016/7_TwaEL2016_School_Intervention_FINAL_nopii.dta", clear
gen date_twa=date(date,"DMY")
format date_twa %td
rename schoolid SchoolID
collapse (mean) date_twa, by(SchoolID)
append using "$base_out/ConsolidatedYr34/TWA_Date_T5.dta"
save "$base_out/ConsolidatedYr34/TWA_Date_T5.dta", replace



use "$basein/15 Endline 2016/Final Deliverable/Final Data/Student Data/R10Student_nopii.dta", clear
gen date_edi=dofc(time)
format date_edi %td
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
merge m:1 SchoolID using "$base_out/ConsolidatedYr34/TWA_Date_T5.dta"
drop if _merge==2
drop _merge

gen attgrade=1 if stdgrd==1 & atrgrd==.
replace attgrade=1 if atrgrd==1
replace attgrade=2 if stdgrd==2 & atrgrd==.
replace attgrade=2 if atrgrd==2
replace attgrade=3 if stdgrd==3 & atrgrd==.
replace attgrade=3 if atrgrd==3



gen student_present=0
replace student_present=1 if  consentChild==1
rename student_present attendance


gen date=dofc(timeStartTest)
format date %dN/D/Y
gen WeekIntvTest=week(date)


drop kiswri4_7_er  kiswri4_8_er engwri4_8_er /*for now not sure what to do with these */
foreach var of varlist kispicl1_1- othsub1_8{
	recode `var' (.=0) if GradeID==1 & attendance==1
}
foreach var of varlist kispic2_1- othsub2_14{
	recode `var' (.=0) if GradeID==2 & attendance==1
}
foreach var of varlist kissyl3_1 - othsub3_17{
	recode `var' (.=0) if GradeID==3 & attendance==1
}
foreach var of varlist kissyl4_1- hisdiv4_9{
	recode `var' (.=0) if GradeID==4 & attendance==1
}


foreach var of varlist kispicl1_1-hisdiv4_9 {
	recode `var' (2=0) (3=0) (-99=0)
}
foreach i of varlist kispicl1_1-hisdiv4_9 {
local a : variable label `i'
local a: subinstr local a " correctly" ""
label var `i' "`a'"
}


egen engwri_1=rowtotal(engwri3_6 engwri4_6),missing
egen engwri_2=rowtotal(engwri2_4 engwri3_4 engwri4_4 ),missing
egen engwri_3=rowtotal(engwri3_5 engwri4_5),missing
egen engwri_4=rowtotal(engwri1_2 engwri2_2 engwri3_2 engwri4_2),missing
egen engwri_5=rowtotal(engwri2_3 engwri3_3 engwri4_3 ),missing
egen engwri_6=rowtotal(engwri1_1 engwri2_1 engwri3_1 engwri4_1),missing

egen kiswri_1=rowtotal(kiswri1_2 kiswri2_2 kiswri3_2 kiswri4_2 ),missing
egen kiswri_2=rowtotal(kiswri1_4 kiswri2_4 kiswri3_4 kiswri4_4),missing
egen kiswri_3=rowtotal(kiswri3_5 kiswri4_5 ),missing
egen kiswri_4=rowtotal(kiswri1_1 kiswri2_1 kiswri3_1 kiswri4_1 ),missing
egen kiswri_5=rowtotal(kiswri3_6 kiswri4_6 ),missing
egen kiswri_6=rowtotal(kiswri1_3 kiswri2_3 kiswri3_3 kiswri4_3),missing

egen englet_1=rowtotal(englet1_3 englet2_3 englet3_3 englet4_2 ),missing
egen englet_2=rowtotal(englet1_1 englet2_1 englet3_1 englet4_1 ),missing
egen englet_3=rowtotal(englet1_2 englet2_2 englet3_2),missing

egen hissub_1=rowtotal(hissub1_1 hissub2_1 hissub3_1 hissub4_1),missing
egen hissub_2=rowtotal(hissub2_6 hissub3_6 hissub4_4),missing
egen hissub_3=rowtotal(hissub2_3 hissub3_3 hissub4_3 ),missing
egen hissub_4=rowtotal(hissub2_5 hissub3_4 ),missing
egen hissub_5=rowtotal(hissub1_4 hissub2_4),missing
egen hissub_6=rowtotal(hissub1_3 hissub2_2 hissub3_2 hissub4_2),missing

egen engpicl_1=rowtotal(engpicl1_3 engpicl4_2),missing
egen engpicl_2=rowtotal(engpicl1_2 engpicl2_2 engpicl3_2 ),missing
egen engpicl_3=rowtotal(engpicl4_1 engpicl1_1 engpicl2_1 engpicl3_1),missing

egen engpic_1=rowtotal(engpic1_3 engpic2_2 engpic3_2 ),missing
egen engpic_2=rowtotal(engpic1_2 engpic2_1 engpic3_1),missing

egen kispic_1=rowtotal(kispic1_1 kispic2_1 ),missing

egen engwrd_1=rowtotal(engwrd1_3 engwrd2_4 engwrd3_3 engwrd4_2),missing
egen engwrd_2=rowtotal(engwrd1_4 engwrd2_5 engwrd3_4),missing
egen engwrd_3=rowtotal(engwrd2_8 engwrd3_7),missing
egen engwrd_4=rowtotal(engwrd2_2 engwrd3_1 engwrd4_1 ),missing
egen engwrd_5=rowtotal(engwrd1_1 engwrd2_1 ),missing
egen engwrd_6=rowtotal(engwrd2_7 engwrd3_6 engwrd4_4),missing 
egen engwrd_7=rowtotal(engwrd1_2 engwrd2_3 engwrd3_2),missing
egen engwrd_8=rowtotal(engwrd1_5 engwrd2_6 engwrd3_5 engwrd4_3),missing

egen kiswrd_1=rowtotal(kiswrd2_6 kiswrd3_7),missing
egen kiswrd_2=rowtotal(kiswrd1_2 kiswrd2_2 kiswrd3_2),missing
egen kiswrd_3=rowtotal(kiswrd1_5 kiswrd2_5 kiswrd3_6),missing
egen kiswrd_4=rowtotal(kiswrd1_1 kiswrd2_1 kiswrd3_1 kiswrd4_1 ),missing
egen kiswrd_5=rowtotal(kiswrd1_6 kiswrd2_7 kiswrd3_8 kiswrd4_4),missing
egen kiswrd_6=rowtotal(kiswrd1_3 kiswrd2_3 kiswrd3_3 kiswrd4_2 ),missing
egen kiswrd_7=rowtotal(kiswrd1_4 kiswrd2_4 kiswrd3_5 kiswrd4_3),missing


egen kissyl_1=rowtotal(kissyl1_3 kissyl2_3 kissyl3_3 kissyl4_2 ),missing
egen kissyl_2=rowtotal(kissyl1_1 kissyl2_1 kissyl3_1 kissyl4_1 ),missing
egen kissyl_3=rowtotal(kissyl3_2 kissyl1_2 kissyl2_2),missing

egen hismul_1=rowtotal(hismul2_3 hismul3_2 hismul4_2 ),missing
egen hismul_2=rowtotal(hismul2_4 hismul3_4 hismul4_3),missing
egen hismul_3=rowtotal(hismul1_1 hismul2_1 hismul3_1 hismul4_1 ),missing

egen hisinq_1=rowtotal(hisinq1_4 hisinq3_2 hisinq4_1 ),missing
egen hisinq_2=rowtotal(hisinq1_2 hisinq2_1 hisinq3_1),missing

egen hisidn_1=rowtotal(hisidn1_3 hisidn2_2 ),missing
egen hisidn_2=rowtotal(hisidn1_1 hisidn2_1 ),missing
egen hisidn_3=rowtotal(hisidn1_2 hisidn3_1 hisidn4_1),missing

*egen othsub_1=rowtotal(othsub1_4 othsub1_8),missing
egen othsub_2=rowtotal(othsub1_6 othsub2_9 othsub3_9 ),missing
egen othsub_3=rowtotal(othsub1_3 othsub2_3 othsub3_3 ),missing
egen othsub_4=rowtotal(othsub1_7 othsub2_10 othsub3_10),missing
egen othsub_5=rowtotal(othsub1_5 othsub2_8 othsub3_8 ),missing
egen othsub_6=rowtotal(othsub1_2 othsub2_2 othsub3_2 ),missing
egen othsub_7=rowtotal(othsub1_1 othsub2_1 othsub3_1),missing
egen othsub_8=rowtotal(othsub2_35 othsub3_35),missing
egen othsub_9=rowtotal(othsub3_14 othsub2_14),missing
egen othsub_10=rowtotal(othsub2_34 othsub3_34),missing
egen othsub_11=rowtotal(othsub33_4 othsub10_4),missing
egen othsub_12=rowtotal(othsub2_7 othsub3_7),missing



egen hisdiv_1=rowtotal(hisdiv2_2 hisdiv3_2 hisdiv4_2),missing
egen hisdiv_2=rowtotal(hisdiv2_1 hisdiv3_1 hisdiv4_1),missing
egen hisdiv_3=rowtotal(hisdiv3_5 hisdiv4_4 ),missing
egen hisdiv_4=rowtotal(hisdiv3_3 hisdiv4_3),missing

egen hiscou_1=rowtotal(hiscou2_1 hiscou4_1 hiscou1_2 hiscou3_1),missing


egen hisadd_1=rowtotal(hisadd2_5 hisadd3_4),missing
egen hisadd_2=rowtotal(hisadd2_1 hisadd3_1 hisadd4_1 hisadd1_1),missing
egen hisadd_3=rowtotal(hisadd2_3 hisadd3_3),missing
egen hisadd_4=rowtotal(hisadd1_4 hisadd2_4 hisadd4_3),missing
egen hisadd_5=rowtotal(hisadd2_6 hisadd3_6 hisadd4_4 ),missing
egen hisadd_6=rowtotal(hisadd1_3 hisadd2_2 hisadd3_2 hisadd4_2),missing


egen engsen_1=rowtotal(engsen1_1 engsen2_1 engsen3_1 ),missing
egen engsen_2=rowtotal(engsen1_2 engsen2_2 engsen3_2  engsen4_1),missing
egen engsen_3=rowtotal(engsen1_3 engsen2_3 engsen3_3),missing
egen engsen_4=rowtotal(engsen1_4 engsen2_4 engsen3_4 ),missing
egen engsen_5=rowtotal(engsen2_6 engsen3_5 engsen4_2 ),missing
egen engsen_6=rowtotal(engsen2_8 engsen3_6 engsen4_3 ),missing




egen kissen_1=rowtotal(kissen1_1 kissen2_1 kissen3_1 kissen4_1),missing
egen kissen_2=rowtotal(kissen1_2 kissen2_2 kissen3_2),missing
egen kissen_3=rowtotal(kissen1_3 kissen2_3 kissen3_3 kissen4_2),missing
egen kissen_4=rowtotal(kissen1_4 kissen2_4 kissen3_5),missing
egen kissen_5=rowtotal(kissen1_5 kissen2_5 kissen3_6 kissen4_3),missing
egen kissen_6=rowtotal(kissen1_6 kissen2_7 kissen3_8 kissen4_4 ),missing
egen kissen_7=rowtotal(kissen2_6 kissen3_7),missing

egen kispar_1=rowtotal(kispar2_1 kispar3_1 kispar4_1),missing
egen kispar_2=rowtotal(kispar2_2 kispar3_2 kispar4_2),missing
egen kispar_3=rowtotal(kispar3_3 kispar4_3), missing

egen engpar_1=rowtotal(engpar2_1 engpar3_1 engpar4_1), missing

egen kissto_1=rowtotal(kissto3_1 kissto4_1), missing
egen kissto_2=rowtotal(kissto3_2 kissto4_2), missing
egen kissto_3=rowtotal(kissto3_3 kissto4_3), missing
egen kissto_4=rowtotal(kissto3_4 kissto4_4), missing

egen engsto_1=rowtotal(engsto3_1 engsto4_1), missing
egen engsto_2=rowtotal(engcom3_1 engsto4_2), missing
egen engsto_3=rowtotal(engcom3_2 engsto4_3), missing
egen engsto_4=rowtotal(engcom3_3 engsto4_9), missing



  

drop engsto3_1 engsto4_1 engsto4_2 engsto4_3 engsto4_9 engcom3_1 engcom3_2 engcom3_3
drop kissto3_1 kissto4_1 kissto3_2 kissto4_2 kissto3_3 kissto4_3 kissto3_4 kissto4_4
drop othsub2_7 othsub3_7 kispar3_3 kispar4_3
drop engwri2_1 engwri2_2 engwri2_3 engwri2_4 engpar2_1 engpar3_1 engpar4_1
drop kispar2_1 kispar3_1 kispar4_1 kispar2_2 kispar3_2 kispar4_2
drop engsen1_1 engsen1_2 engsen1_3 engsen1_4 
drop kissen1_1 kissen2_1 kissen3_1 kissen4_1 kissen1_2 kissen2_2 kissen3_2 kissen1_3 kissen2_3 kissen3_3 kissen4_2 kissen1_4 kissen2_4 kissen3_5
drop kissen1_5 kissen2_5 kissen3_6 kissen4_3 kissen1_6 kissen2_7 kissen3_8 kissen4_4  kissen2_6 kissen3_7
drop engwri3_6 engwri4_6 engwri3_4 engwri4_4 kiswri1_2 kiswri2_2 kiswri3_2 kiswri4_2 kiswri1_4 kiswri2_4 kiswri3_4 kiswri4_4 kiswri3_5 kiswri4_5 kiswri1_1 kiswri2_1 kiswri3_1 kiswri4_1 
drop engwri3_5 engwri4_5 kiswri3_6 kiswri4_6 engwri1_2 engwri3_2 engwri4_2 kiswri1_3 kiswri2_3 kiswri3_3 kiswri4_3 engwri3_3 engwri4_3 engwri1_1 engwri3_1 engwri4_1
drop englet1_3 englet2_3 englet3_3 englet4_2 englet1_1 englet2_1 englet3_1 englet4_1 englet1_2 englet2_2 englet3_2 hissub1_1 hissub2_1 hissub3_1 hissub4_1 hissub2_6 hissub3_6 hissub4_4
drop hissub2_3 hissub3_3 hissub4_3 hissub2_5 hissub3_4 hissub1_4 hissub2_4 hissub1_3 hissub2_2 hissub3_2 hissub4_2 engpic1_3 engpic2_2 engpic3_2 engpicl1_3 engpicl4_2
drop engpicl1_2 engpicl2_2 engpicl3_2 kispic1_1 kispic2_1 engpic1_2 engpic2_1 engpic3_1 engpicl4_1 engpicl1_1 engpicl2_1 engpicl3_1 kiswrd2_6 kiswrd3_7 engwrd1_3 engwrd2_4 engwrd3_3 engwrd4_2
drop engwrd1_4 engwrd2_5 engwrd3_4 kiswrd1_2 kiswrd2_2 kiswrd3_2 engwrd2_8 engwrd3_7 kiswrd1_5 kiswrd2_5 kiswrd3_6 engwrd2_2 engwrd3_1 engwrd4_1 kiswrd1_1 kiswrd2_1 kiswrd3_1 kiswrd4_1 
drop kiswrd1_6 kiswrd2_7 kiswrd3_8 kiswrd4_4 engwrd1_1 engwrd2_1 kiswrd1_3 kiswrd2_3 kiswrd3_3 kiswrd4_2 kiswrd1_4 kiswrd2_4 kiswrd3_5 kiswrd4_3 engwrd2_7 engwrd3_6 engwrd4_4 
drop engwrd1_2 engwrd2_3 engwrd3_2 engwrd1_5 engwrd2_6 engwrd3_5 engwrd4_3 kissyl1_3 kissyl2_3 kissyl3_3 kissyl4_2 kissyl1_1 kissyl2_1 kissyl3_1 kissyl4_1 kissyl3_2 kissyl1_2 kissyl2_2
drop hismul2_3 hismul3_2 hismul4_2 hismul2_4 hismul3_4 hismul4_3 hismul1_1 hismul2_1 hismul3_1 hismul4_1 othsub1_4 othsub1_8 hisinq1_4 hisinq3_2 hisinq4_1 hisinq1_2 hisinq2_1 hisinq3_1
drop hisidn1_3 hisidn2_2 hisidn1_1 hisidn2_1 hisidn1_2 hisidn3_1 hisidn4_1 othsub1_6 othsub2_9 othsub3_9 othsub1_3 othsub2_3 othsub3_3 othsub1_7 othsub2_10 othsub3_10 othsub1_5 othsub2_8 othsub3_8 
drop othsub1_2 othsub2_2 othsub3_2 othsub1_1 othsub2_1 othsub3_1 hisdiv2_2 hisdiv3_2 hisdiv4_2 hisdiv2_1 hisdiv3_1 hisdiv4_1 hisdiv3_5 hisdiv4_4 hisdiv3_3 hisdiv4_3 hiscou2_1 hiscou4_1 hiscou1_2 hiscou3_1
drop othsub2_35 othsub3_35 othsub3_14 othsub2_14 othsub2_34 othsub3_34 othsub33_4 othsub10_4 hisadd2_5 hisadd3_4 hisadd2_1 hisadd3_1 hisadd4_1 hisadd1_1 hisadd2_3 hisadd3_3
drop hisadd1_4 hisadd2_4 hisadd4_3 hisadd2_6 hisadd3_6 hisadd4_4 hisadd1_3 hisadd2_2 hisadd3_2 hisadd4_2

drop engsen2_3 engsen3_3 engsen3_6 engsen4_3 engsen2_8 engsen3_2 engsen4_1 engsen2_2 engsen3_4 engsen2_4 
drop engsen3_5 engsen4_2 engsen2_6 engsen3_1 engsen2_1 



irt (1pl kis*), intpoints(20) difficult
predict Z_kiswahili, latent 

irtgraph tif
graph export "$graphs/TIF_IRT_kiswahili_T5.pdf", replace

irt (1pl eng*), intpoints(20) difficult
predict Z_kiingereza, latent 

irtgraph tif
graph export "$graphs/TIF_IRT_English_T5.pdf", replace

irt  (2pl hisadd* hissub* hisidn* hisinq* hismul* hiscou* hisdiv*), intpoints(20) difficult
predict Z_hisabati, latent 

irtgraph tif
graph export "$graphs/TIF_IRT_Math_T5.pdf", replace

irt (1pl oth*), intpoints(20) difficult
predict Z_sayansi, latent 
replace Z_sayansi=. if GradeID==4

irt (1pl kis* hisadd* hissub* hisidn* hisinq* hismul* hiscou* hisdiv*), intpoints(20) difficult
predict Z_ScoreKisawMath, latent 

irtgraph tif
graph export "$graphs/TIF_IRT_ScoreKisawMath_T5.pdf", replace

irt (1pl kis* hisadd* hissub* hisidn* hisinq* hismul* hiscou* hisdiv* eng*), intpoints(20) difficult
predict Z_ScoreFocal, latent 


foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi Z_ScoreKisawMath Z_ScoreFocal {
	replace `var'=. if attendance==0
	sum `var' if  treatarm2==3
	replace `var'=(`var'-r(mean))/r(sd)
}



drop kis* eng* his* oth*


rename upidst upid
rename * =_T5
foreach var in upid SchoolID DistrictID StrataScore treatment2 treatarm2 treatment treatarm{
	rename `var'_T5 `var'
}



compress 
save "$base_out/15 Endline 2016/Student_T5.dta", replace




*********** ENDLINE 2016***********
******* OTHER THINGS **********8
*****************************

use "$basein/15 Endline 2016/Final Deliverable/Final Data/Student Data/R10Subject_nopii.dta", clear
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
merge m:1 SchoolID GradeID  R10StudentID using "$basein/15 Endline 2016/Final Deliverable/Final Data/Student Data/R10Student_nopii.dta", keepus(upid)
drop if _merge!=3
drop _merge
drop if subnottaught==1
drop if subnottaught==.
recode teacherhelp knowname callname harsh missdays presents homework assignmentgraded (2=0)
recode callname numdays (.=0)
recode teacherhit teacherleave (1/2=1) (3/5=0)
recode assignmentlength (-95=.)
replace assignmentlength=assignmentlength*0.71 if  booklarge==2 /*use ratio of area */
gen Nofeedback  =(bookfeedback_95!=.) & !missing(bookfeedback)
keep upid SchoolID GradeID R10StudentID R10SubjectID teacherhelp -treatarm 
rename R10StudentID StudentID
rename R10SubjectID SubjectID
rename * =_T5
foreach var in upidst SchoolID StudentID SubjectID DistrictID StrataScore treatment2 treatarm2 treatment treatarm{
rename `var'_T5 `var'
}
rename upidst upid
save "$base_out/11 Endline 2015/StudentSubject_T5.dta", replace


use "$basein/15 Endline 2016/Final Deliverable/Final Data/Student Data/R10TeacherHelp_nopii.dta", clear
recode schoolhelp weekendhelp (2=0)
collapse (max) schoolhelp weekendhelp , by(SchoolID GradeID R10StudentID R10SubjectID)
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
merge m:1 SchoolID GradeID  R10StudentID using "$basein/15 Endline 2016/Final Deliverable/Final Data/Student Data/R10Student_nopii.dta", keepus(upid)
drop if _merge!=3
drop _merge
rename schoolhelp schoolhelp_T5
rename weekendhelp weekendhelp_T5
rename GradeID GradeID_T5
rename R10StudentID StudentID
rename R10SubjectID SubjectID
rename upidst upid
save "$base_out/11 Endline 2015/StudentHelp_T5.dta", replace


***********************************************************
********** Twaweza endline 2016***********
***********************************************************


use "$base_out/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_Mashindano_CLEAN.dta", clear
append using "$base_out/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_StadiandControl_CLEAN.dta"
keep studentid districtid schoolid grade gender grd1* grd2* grd3*  newstudentEL2016  testedEL2016  set

rename schoolid SchoolID
rename districtid DistrictID
rename grade Grade

rename testedEL2016 TestedEL2016
replace TestedEL2016=0 if TestedEL2016==.
drop *his_b_* *his_c_* *kis_b_* *kis_c_* *eng_b_* *eng_c_*

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge==1
drop _merge


foreach var of varlist grd1*{
replace `var'=. if Grade!=1 | TestedEL2016==0
replace `var'=0 if `var'==. & Grade==1 & TestedEL2016==1
}

foreach var of varlist grd2*{
replace `var'=. if Grade!=2 | TestedEL2016==0
replace `var'=0 if `var'==. & Grade==2 & TestedEL2016==1
}

foreach var of varlist grd3*{
replace `var'=. if Grade!=3 | TestedEL2016==0
replace `var'=0 if `var'==. & Grade==3 & TestedEL2016==1
}
*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in si ma se{
	 forvalues i=3/6{
		replace grd1_kis_a_`skill'_`i'=0 if grd1_kis_a_`skill'_1==0 & grd1_kis_a_`skill'_2==0 & Grade==1
	}
}

foreach skill in id uta bwa j t{
	replace grd1_his_a_`skill'_3=0 if grd1_his_a_`skill'_1==0 & grd1_his_a_`skill'_2==0 & Grade==1
} 

replace grd2_kis_a_h=0 if grd2_kis_a_h==. & Grade==2

*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd2_kis_a_m_1=0 if grd2_kis_a_h==0 & Grade==2
replace grd2_kis_a_m_2=0 if grd2_kis_a_h==0 & Grade==2

*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in ma se{
	 forvalues i=3/6{
		replace grd2_kis_a_`skill'_`i'=0 if grd2_kis_a_`skill'_1==0 & grd2_kis_a_`skill'_2==0 & Grade==2
	}
}
foreach skill in  bwa j t{
	replace grd2_his_a_`skill'_3=0 if grd2_his_a_`skill'_1==0 & grd2_his_a_`skill'_2==0 & Grade==2
} 


replace grd3_kis_a_h=0 if grd3_kis_a_h==. & Grade==3
*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_kis_a_m_1=0 if grd3_kis_a_h==0 & Grade==3
replace grd3_kis_a_m_2=0 if grd3_kis_a_h==0 & Grade==3


*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_eng_a_c_1=0 if grd3_eng_a_s==0 & Grade==3
replace grd3_eng_a_c_2=0 if grd3_eng_a_s==0 & Grade==3

*First data cleaning process for math is as before...
foreach skill in  j t z g{
	replace grd3_his_a_`skill'_3=0 if grd3_his_a_`skill'_1==0 & grd3_his_a_`skill'_2==0 & Grade==3

} 


forvalues val=1/3{
	foreach var of varlist grd`val'_* {
			forvalues set_num =1/10 {
				qui sum `var' if Grade==`val' & treatarm2==3 & set==`set_num'
				qui capture gen Z_`var'=.
				qui replace Z_`var'= (`var'-r(mean))/r(sd) if Grade==`val' & set==`set_num'
			}
	}
}

/*
forvalues val=1/3{
foreach var of varlist grd1* grd2* grd3* {
sum `var' if Grade==`val' & treatarm2==3
capture gen Z_`var'=.
replace Z_`var'= (`var'-r(mean))/r(sd) if Grade==`val'
}
}
*/

gen Z_kiswahili=.
gen Z_kiingereza=.
gen Z_hisabati=.
forvalues val=1/3{
	pca Z_grd`val'_kis_*
	predict temp, score
	replace Z_kiswahili=temp if Grade==`val'
	drop temp
	pca Z_grd`val'_his_*
	predict temp, score
	replace Z_hisabati=temp if Grade==`val'
	drop temp
}
pca Z_grd3_eng_*
predict temp, score
replace Z_kiingereza=temp if Grade==3
drop temp




***Now we standarize by grade subject
forvalues val=1/3{
	foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati  {
		sum `var' if Grade==`val' & treatarm2==3
		replace `var'=(`var'-r(mean))/r(sd) if Grade==`val'
	}
}


foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati {
	sum `var' if  treatarm2==3
	replace `var'=(`var'-r(mean))/r(sd)
}

pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score

pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score



*Calculcate number of questions per skill
egen Kis_Silabi=rowtotal(grd*_kis_a_si_*), missing
egen Kis_Maneno=rowtotal(grd*_kis_a_ma_*), missing
egen Kis_Sentenci=rowtotal(grd*_kis_a_se_*), missing
gen Kis_Aya=grd2_kis_a_h
egen Kis_Comp=rowtotal(grd*_kis_a_m_*), missing
egen Math_id=rowtotal(grd*_his_a_id_*), missing
egen Math_uta=rowtotal(grd*_his_a_uta_*), missing
egen Math_bwa=rowtotal(grd*_his_a_bwa_*), missing
egen Math_j=rowtotal(grd*_his_a_j_*), missing
egen Math_t=rowtotal(grd*_his_a_t_*), missing
egen Math_z=rowtotal(grd*_his_a_z_*), missing
egen Math_g=rowtotal(grd*_his_a_g_*), missing
gen Kis_Story=grd3_kis_a_h
gen Eng_Story=grd3_eng_a_s
egen Eng_Comp=rowtotal(grd*_eng_a_c_*), missing
 




*generate passing dummies. Have to be careful since stata thinks missing is infinity
foreach var of varlist Kis_Silabi Kis_Maneno   {
	gen `var'_Pass=`var'>=4 if !missing(`var')
	
		twoway (histogram `var' if treatment2=="Control", percent fcolor(red) fintensity(100) lcolor(red) barwidth(0.4)) ///
	(histogram `var' if treatment2=="Levels", percent fcolor(ltblue) fintensity(100) lcolor(ltblue) barwidth(0.25)) ///
	(histogram `var' if treatment2=="Gains", percent fcolor(gray) fintensity(100) lcolor(gray) barwidth(0.1)), ///
	ytitle(% of students) ylabel(, angle(horizontal) nogrid) xtitle(Correct answers) xline(4, lwidth(thick) lpattern(dash) lcolor(black)) ///
	legend(order(1 "Control" 2 "Levels" 3 "Gains") rows(1) region(fcolor(none) margin(zero) lcolor(none)) bmargin(zero)) ///
	graphregion(fcolor(white)) plotregion(fcolor(white))
	graph export "$graphs/histogram`var'_T6.pdf", replace
}

foreach var of varlist grd*_kis_a_se_*   {
	gen `var'_Pass=(`var'>=2) if !missing(`var')
	
}

egen temp=rowtotal(grd*_kis_a_se_*_Pass), missing

foreach var of varlist Kis_Sentenci    {
	gen `var'_Pass=(temp>=4) if !missing(temp)
	
		twoway (histogram `var' if treatment2=="Control", percent fcolor(red) fintensity(100) lcolor(red) barwidth(0.4)) ///
	(histogram `var' if treatment2=="Levels", percent fcolor(ltblue) fintensity(100) lcolor(ltblue) barwidth(0.25)) ///
	(histogram `var' if treatment2=="Gains", percent fcolor(gray) fintensity(100) lcolor(gray) barwidth(0.1)), ///
	ytitle(% of students) ylabel(, angle(horizontal) nogrid) xtitle(Correct answers) xline(4, lwidth(thick) lpattern(dash) lcolor(black)) ///
	legend(order(1 "Control" 2 "Levels" 3 "Gains") rows(1) region(fcolor(none) margin(zero) lcolor(none)) bmargin(zero)) ///
	graphregion(fcolor(white)) plotregion(fcolor(white))
	graph export "$graphs/histogram`var'_T6.pdf", replace
}
drop temp

foreach var of varlist Math_id Math_uta Math_bwa Math_j Math_t Math_z Math_g {
	gen `var'_Pass=`var'>=2 if !missing(`var')
	
		twoway (histogram `var' if treatment2=="Control", percent fcolor(red) fintensity(100) lcolor(red) barwidth(0.4)) ///
	(histogram `var' if treatment2=="Levels", percent fcolor(ltblue) fintensity(100) lcolor(ltblue) barwidth(0.25)) ///
	(histogram `var' if treatment2=="Gains", percent fcolor(gray) fintensity(100) lcolor(gray) barwidth(0.1)), ///
	ytitle(% of students) ylabel(, angle(horizontal) nogrid) xtitle(Correct answers) xline(2, lwidth(thick) lpattern(dash) lcolor(black)) ///
	legend(order(1 "Control" 2 "Levels" 3 "Gains") rows(1) region(fcolor(none) margin(zero) lcolor(none)) bmargin(zero)) ///
	graphregion(fcolor(white)) plotregion(fcolor(white))
	graph export "$graphs/histogram`var'_T6.pdf", replace
}

foreach var of varlist  Kis_Aya Kis_Story Eng_Story{
	gen `var'_Pass=`var'==3 if !missing(`var')
}

foreach var of varlist  Kis_Comp Eng_Comp{
	gen `var'_Pass=`var'==2 if !missing(`var')
}

drop Kis_Silabi-Eng_Comp  grd1_kis_a_se_1_Pass-grd2_kis_a_se_6_Pass
drop grd1* grd2* grd3* Z_grd1* Z_grd2* Z_grd3*
drop if treatarm2==.

rename * =_T6
foreach var in DistrictID SchoolID studentid  StrataScore treatment2 treatarm2 treatment treatarm{
	rename `var'_T6 `var'
}

replace gender_T6="" if gender_T6=="--blank--"
encode gender_T6, gen(gender_T62)
drop gender_T6
gen gender_T6=gender_T62
drop gender_T62
drop if studentid=="-999"
save "$base_out/15 Endline 2016/Student_T6.dta", replace





************ MERGE**********

***********************************************************
********** Twaweza endline 2016***********
***********************************************************
use "$base_out/15 Endline 2016/R10 Linked Students_draft_nopii.dta", clear
replace studentid="" if studentid=="999"
replace studentid="" if studentid=="-999"
replace studentid="" if studentid=="99"
replace studentid="" if studentid=="-99"
replace studentid="" if studentid=="-  999"
replace studentid="" if studentid=="--BLANK--"
replace studentid="" if studentid=="-.999"
replace studentid="" if studentid=="-99.9"
replace studentid="" if studentid=="9.99"
merge m:1 name using "$base_out/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_Mashindano_CLEAN.dta", keepus(studentid) update 
drop _merge
merge m:1 name using "$base_out/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_StadiandControl_CLEAN.dta", keepus(studentid) update 
drop _merge
bys id_name: replace studentid=studentid[_n-1] if studentid==""
bys id_name: replace studentid=studentid[_n-1] if studentid==""
bys id_name: replace studentid=studentid[_n+1] if studentid==""
bys id_name: replace studentid=studentid[_n+1] if studentid==""
bys id_name: replace upidst=upidst[_n-1] if upidst==""
bys id_name: replace upidst=upidst[_n-1] if upidst==""
bys id_name: replace upidst=upidst[_n+1] if upidst==""
bys id_name: replace upidst=upidst[_n+1] if upidst==""
drop if id_name==.
keep studentid upidst id_name
bys id_name:gen n=_n
drop if n>1
drop n 
rename upidst upid
drop id_name
save "$base_out/15 Endline 2016/CrossOverEDI_TWA.dta", replace

use "$basein/12 Intervention_KFII/2 Endline Name Merge 2015/FuzzyMerge_NameMatch_nopii.dta", clear
keep studentid upid 
drop if studentid=="-999"
append using "$base_out/15 Endline 2016/CrossOverEDI_TWA.dta"
bys upid studentid: gen N=_n
drop if N>1
drop N
bys studentid: gen N=_n
drop if N==2
drop N
bys upid: gen N=_n
drop if N==2
drop N
save "$base_out/15 Endline 2016/CrossOverEDI_TWA_FINAL.dta", replace

use "$base_out/9 Baseline 2015/Student_T1.dta", clear
merge 1:1 upid using "$base_out/11 Endline 2015/Student_T2.dta"
drop _merge
merge m:1 upid using "$base_out/13 Baseline 2016/Student_T4.dta"
drop _merge
merge m:1 upid using "$base_out/15 Endline 2016/Student_T5.dta"
drop _merge

merge m:1 upid using "$base_out/15 Endline 2016/CrossOverEDI_TWA_FINAL.dta", update
drop _merge


merge m:1 studentid using  "$basein/4 Intervention/TwaEL_2014/Student_T0.dta"
drop _merge
merge m:1 studentid using "$base_out/11 Endline 2015/Student_T3.dta", update keepus(SchoolID Z_kiswahili_T3 Z_kiingereza_T3 Z_hisabati_T3 Z_ScoreFocal_T3 Z_ScoreKisawMath_T3 gender_T3 Grade_T3 TestedEL2015 *Pass*)
drop _merge
merge m:1 studentid using "$base_out/15 Endline 2016/Student_T6.dta", update keepus(SchoolID Z_kiswahili_T6 Z_kiingereza_T6 Z_hisabati_T6 Z_ScoreFocal_T6 Z_ScoreKisawMath_T6 gender_T6 Grade_T6 TestedEL2016 *Pass*)
drop _merge

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta", update replace
drop if _merge==1
drop _merge
save "$base_out/ConsolidatedYr34/Student.dta", replace


************ MERGE OTHER**********
use "$base_out/11 Endline 2015/StudentSubject_T2.dta", clear
merge 1:1 upid  SubjectID using "$base_out/11 Endline 2015/StudentSubject_T5.dta"
drop _merge
save "$base_out/ConsolidatedYr34/StudentSubject.dta", replace


use "$base_out/11 Endline 2015/StudentHelp_T2.dta", clear
merge 1:1 upid  SubjectID  using "$base_out/11 Endline 2015/StudentHelp_T5.dta"
drop _merge
save "$base_out/ConsolidatedYr34/StudentHelp.dta", replace

/*
fvset base 3 treatarm2   
reghdfe Z_kiswahili_T2 i.(treatarm2 GradeID_T2) if GradeID_T2<4, vce(cluster SchoolID) ab( DistrictID#StrataScore#treatarm)
reghdfe Z_kiswahili_T2 i.(treatarm2 GradeID_T2) Z_kiswahili_T1 Z_kiingereza_T1 Z_hisabati_T1 Z_sayansi_T1 if GradeID_T2<4, vce(cluster SchoolID)  ab( DistrictID#StrataScore#treatarm)

reghdfe Z_hisabati_T2 i.(treatarm2 GradeID_T2) if GradeID_T2<4, vce(cluster SchoolID)  ab( DistrictID#StrataScore#treatarm)
reghdfe Z_hisabati_T2 i.(treatarm2 GradeID_T2) Z_kiswahili_T1 Z_kiingereza_T1 Z_hisabati_T1 Z_sayansi_T1 if GradeID_T2<4, vce(cluster SchoolID)  ab( DistrictID#StrataScore#treatarm)

reghdfe Z_kiingereza_T2 i.(treatarm2 GradeID_T2) if GradeID_T2<4, vce(cluster SchoolID)  ab( DistrictID#StrataScore#treatarm)
reghdfe Z_kiingereza_T2 i.(treatarm2 GradeID_T2) Z_kiswahili_T1 Z_kiingereza_T1 Z_hisabati_T1 Z_sayansi_T1 if GradeID_T2<4, vce(cluster SchoolID)  ab( DistrictID#StrataScore#treatarm)
*/



