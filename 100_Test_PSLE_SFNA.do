use "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2015.dta", clear
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
mmerge school_codePSLE using "$basein/5 National Exams/ResultadosSchoolTotal_SFNA_2015.dta", uname(SFNA) umatch(school_codeSFNA)





use "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2016.dta", clear
mmerge school_codePSLE using "$basein/5 National Exams/ResultadosSchoolTotal_SFNA_2016.dta", uname(SFNA) umatch(school_codeSFNA)



use "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2017.dta", clear
mmerge school_codePSLE using "$basein/5 National Exams/ResultadosSchoolTotal_SFNA_2017.dta", uname(SFNA) umatch(school_codeSFNA)




use "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2013.dta", clear
keep school_name district_name region_name school_codePSLE
rename region_name Y2013region_name
rename school_name Y2013school_name
rename district_name Y2013district_name

mmerge school_codePSLE using "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2014.dta", uname(Y2014)  ukeep(school_name district_name region_name)
drop if _merge!=3
drop _merge
mmerge school_codePSLE using "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2015.dta", uname(Y2015)  ukeep(school_name district_name region_name)
drop if _merge!=3
drop _merge
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
mmerge school_codePSLE using "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2016.dta", uname(Y2016)  ukeep(school_name district_name region_name)
drop if _merge!=3
drop _merge
mmerge school_codePSLE using "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2017.dta", uname(Y2017)  ukeep(school_name district_name region_name)
drop if _merge!=3
drop _merge

drop if Y2013region_name!= Y2014region_name
drop if Y2013region_name!= Y2015region_name
drop if Y2013region_name!= Y2016region_name
drop if Y2013region_name!= Y2017region_name

drop if Y2014region_name!= Y2015region_name
drop if Y2014region_name!= Y2016region_name
drop if Y2014region_name!= Y2017region_name

drop if Y2015region_name!= Y2016region_name
drop if Y2015region_name!= Y2017region_name

drop if Y2016region_name!= Y2017region_name

/*
I THINK KAHAMA MJI BECAME	KISHAPU
AND KISHAPU BECAME	MSALALA
*/

replace Y2014district_name="KAHAMA" if Y2014district_name=="KAHAMA MJI"
replace Y2015district_name="KAHAMA" if Y2015district_name=="KAHAMA MJI"
replace Y2016district_name="KAHAMA" if Y2016district_name=="KAHAMA MJI"
replace Y2016district_name="MBINGA" if Y2016district_name=="MBINGA(V)"
replace Y2016district_name="MPANDA MJI" if Y2016district_name=="MPANDA MANISPAA"
replace Y2013district_name="MSALALA" if Y2013district_name=="KISHAPU"
replace Y2013district_name="KISHAPU" if Y2013district_name=="KAHAMA MJI"

replace Y2017district_name="KINONDONI(V)" if Y2017district_name=="KINONDONI"
replace Y2017district_name="KINONDONI(M)" if Y2017district_name=="UBUNGO"

replace Y2017district_name="TEMEKE(V)" if Y2017district_name=="KIGAMBONI"
replace Y2017district_name="TEMEKE(M)" if Y2017district_name=="TEMEKE"

replace Y2017district_name="TARIME" if Y2017district_name=="TARIME(V)"
replace Y2017district_name="MBINGA" if Y2017district_name=="MBINGA (DC)"

replace Y2017district_name="KAHAMA" if Y2017district_name=="KAHAMA MJI"
replace Y2017district_name="MPANDA MJI" if Y2017district_name=="MPANDA MANISPAA"



	

	



drop if Y2013district_name!= Y2014district_name
drop if Y2013district_name!= Y2015district_name
drop if Y2013district_name!= Y2016district_name
drop if Y2013district_name!= Y2017district_name

drop if Y2014district_name!= Y2015district_name
drop if Y2014district_name!= Y2016district_name
drop if Y2014district_name!= Y2017district_name

drop if Y2015district_name!= Y2016district_name
drop if Y2015district_name!= Y2017district_name

drop if Y2016district_name!= Y2017district_name

qui forvalues i=1/5{
	qui foreach var of varlist *school_name{
		replace `var'=regexr(`var',"SHULE YA MSINGI","")
		replace `var'=regexr(`var',"PRIMARY$"," ")
		replace `var'=regexr(`var',"PPIMARY$"," ")
		replace `var'=regexr(`var',"PRIIMARY$"," ")
		replace `var'=regexr(`var',"PRINARY$"," ")
		replace `var'=regexr(`var',"PRMARY$"," ")
		replace `var'=regexr(`var',"P/RIMARY$"," ")
		replace `var'=regexr(`var',"PRIMAMARY$"," ")
		replace `var'=regexr(`var',"-PRIMA$"," ")
		replace `var'=regexr(`var',"ACADPRYMARY$"," ")
		replace `var'=regexr(`var',"PR$"," ")
		replace `var'=regexr(`var',"PRIMAEY$"," ")
		replace `var'=regexr(`var',"PRIMMARY$"," ")
		replace `var'=regexr(`var',"PRIMARI$"," ")
		replace `var'=regexr(`var',"PSCHOOL$"," ")
		replace `var'=regexr(`var'," PRM$"," ")
		replace `var'=regexr(`var',"SCHOOL$"," ")
		replace `var'=regexr(`var',"SHCOOL$"," ")
		replace `var'=regexr(`var',"CHOOL$"," ")
		replace `var'=regexr(`var',"PRIMARYA$"," ")
		
		
		replace `var'=regexr(`var',"ENGLISH MEDIUM$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUM$"," ")
		replace `var'=regexr(`var',"ENGL MEDIUM$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTRE$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTER$"," ")
		replace `var'=regexr(`var',"ACADEMY$"," ")
		replace `var'=regexr(`var',"CENTRE$"," ")
		replace `var'=regexr(`var',"CENTER$"," ")
		replace `var'=regexr(`var',"SCHOO$"," ")
		replace `var'=regexr(`var',"SHOOL$"," ")
		replace `var'=regexr(`var',"SCHOLL$"," ")
		replace `var'=regexr(`var'," EMPS$"," ")
		replace `var'=regexr(`var'," SCH$"," ")
		replace `var'=regexr(`var'," PS$"," ")
		replace `var'=regexr(`var'," P/S$"," ")
		replace `var'=regexr(`var',"ENG MED$"," ")
		replace `var'=regexr(`var',"ENG MEDIUM$"," ")
		replace `var'=regexr(`var',"ENG MD$"," ")
		replace `var'=regexr(`var',"PRIMSCHOOL$"," ")
		replace `var'=regexr(`var',"PRSCHOOL$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUMSCHOOL$"," ")
		replace `var'=regexr(`var',"\(ENGLISHMEDIUM\)$"," ")
		replace `var'=regexr(`var',"P /$"," ")
		 
		replace `var'=regexr(`var',"\.","")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strupper(`var')
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var'," - ","-")
		replace `var'=regexr(`var'," -","-")
		replace `var'=regexr(`var',"- ","-")
		replace `var'=regexr(`var',"-"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"  "," ")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strtrim(`var')
		replace `var'=stritrim(`var')	
	}
}


forvalues i=2013/2017{
	 forvalues j=`i'/2017{
		if(`i'!=`j'){
			ustrdist Y`i'school_name Y`j'school_name, generate(Diff_`i'_`j')
			drop if Diff_`i'_`j'>=2 & !missing(Diff_`i'_`j')
		}
	}
}
/*
egen Diff=rowmean(Diff_2013_2014- Diff_2016_2017)
drop if  Diff>3

egen Diff2=rowmedian(Diff_2013_2014- Diff_2016_2017)
drop if  Diff2>3

bro Y*school_name  if Y2013school_name!= Y2014school_name
bro Y*school_name  if Y2013school_name!= Y2016school_name


drop if Y2013school_name!= Y2014school_name
drop if Y2013school_name!= Y2015school_name
drop if Y2013school_name!= Y2016school_name
drop if Y2013school_name!= Y2017school_name

drop if Y2014school_name!= Y2015school_name
drop if Y2014school_name!= Y2016school_name
drop if Y2014school_name!= Y2017school_name

drop if Y2015school_name!= Y2016school_name
drop if Y2015school_name!= Y2017school_name

drop if Y2016school_name!= Y2017school_name
*/

