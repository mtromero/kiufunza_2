**********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
fvset base default treatarm 
gen Diff_T2=date_twa_T2-date_edi_T2
gen Diff_T5=date_twa_T5-date_edi_T5
gen Week_T2=week(date_edi_T2)
gen Week_T5=week(date_edi_T5)


global AggregateDep 	Z_hisabati Z_kiswahili Z_kiingereza Z_sayansi Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal /*this should be added in the future...*/
global AggregateDep_Karthik 	Z_hisabati Z_kiswahili Z_kiingereza   /*Z_kiingereza  Z_ScoreFocal this should be added in the future...*/
global AggregateDep_int 	Z_hisabati Z_kiswahili Z_kiingereza
 
global AggregateDep_NonEng 	Z_hisabati Z_kiswahili 
global AggregateDep_Eng 	Z_kiingereza

global treatmentlist TreatmentLevels TreatmentGains
gen TreatmentLevels2=TreatmentLevels
gen TreatmentGains2=TreatmentGains
global treatmentlist2 TreatmentLevels2 TreatmentGains2
*enrollment2015_T1 Rural_T1 ClassesOutside_T1 Electricity_T1

      
*c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza)##c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza) ///


global student2 shoes_T2 socks_T2 dirty_T2 uniformdirty_T2 uniformtorn_T2  ringworm_T2 CloseToeShoe_T2

recode bookshome_T2 (-98=.)
global studentoutcomes studentsfight_T2 sing_T2 bookshome_T2

bys SchoolID: egen Z_kiswahili_T0_G=mean(Z_kiswahili_T0)
bys SchoolID: egen Z_kiingereza_T0_G=mean(Z_kiingereza_T0)
bys SchoolID: egen Z_hisabati_T0_G=mean(Z_hisabati_T0)
pca Z_hisabati_T0  Z_kiingereza_T0 Z_kiswahili_T0
predict Z_ScoreFocal_T0, score
*First lets create the tables Karthik Wants

label var Z_kiswahili_T2 Swahili
label var Z_kiswahili_T5 Swahili
label var Z_kiingereza_T2 English
label var Z_kiingereza_T5 English
label var Z_hisabati_T2 Math
label var Z_hisabati_T5 Math
label var Z_sayansi_T2 Science
label var Z_sayansi_T5 Science
label var Z_ScoreFocal_T2 "Focal Subjects"
label var Z_ScoreFocal_T5 "Focal Subjects"
rename Grade_T3 GradeID_T3
rename Grade_T6 GradeID_T6
rename Grade_T0 GradeID_T0
drop if GradeID_T0==3

foreach subject in kiswahili  hisabati {
	bys SchoolID : egen Variance_`subject'_T0=sd(Z_`subject'_T0)
	bys SchoolID : egen Variance_`subject'_T1=sd(Z_`subject'_T1)
	bys SchoolID : egen Variance_`subject'_T2=sd(Z_`subject'_T2)
	bys SchoolID : egen Variance_`subject'_T3=sd(Z_`subject'_T3)
	bys SchoolID : egen Variance_`subject'_T4=sd(Z_`subject'_T4)
	bys SchoolID : egen Variance_`subject'_T5=sd(Z_`subject'_T5)
	bys SchoolID : egen Variance_`subject'_T6=sd(Z_`subject'_T6)	
}
 


***********************************************
***********************************************
********** TEST SCORES ********************
***********************************************
***********************************************
*Z_kiswahili_T0_G Z_kiingereza_T0_G Z_hisabati_T0_G 
eststo clear
foreach time in T2 T3 T5 T6{
	foreach var in kiswahili  hisabati{
			eststo:  reghdfe Z_`var'_`time' $treatmentlist i.GradeID_`time' $schoolcontrol if GradeID_`time'<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		
	}  
}

esttab  using "$latexcodes/RegResumen.tex", se ar2 booktabs label b(%9.2fc) se(%9.2fc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0fc %9.2fc %9.2fc) labels("N. of obs." "Gains-Levels \$(\alpha_3) = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)




eststo clear
foreach time in T2 {
	foreach var in kiswahili  hisabati{
			eststo:  reghdfe Z_`var'_`time' c.(${treatmentlist})##c.(Variance_`var'_T1) i.GradeID_`time' $schoolcontrol if GradeID_`time'<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		
	}  
}

esttab  using "$latexcodes/RegHetVarianza.tex", se ar2 booktabs label b(%9.2fc) se(%9.2fc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(*TreatmentGains* *TreatmentLevels* Variance*) stats(N suma p, fmt(%9.0fc %9.2fc %9.2fc) labels("N. of obs." "Gains-Levels \$(\alpha_3) = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

eststo clear
foreach time in T2 T5 {
	foreach var in kiswahili  hisabati{
			eststo:  reghdfe Z_`var'_`time' c.(${treatmentlist})##c.(Variance_`var'_T0) i.GradeID_`time' $schoolcontrol if GradeID_`time'<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		
	}  
}

esttab  using "$latexcodes/RegHetVarianza_High.tex", se ar2 booktabs label b(%9.2fc) se(%9.2fc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(*TreatmentGains* *TreatmentLevels* Variance*) stats(N suma p, fmt(%9.0fc %9.2fc %9.2fc) labels("N. of obs." "Gains-Levels \$(\alpha_3) = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)
