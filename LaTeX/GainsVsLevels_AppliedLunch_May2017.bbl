\begin{thebibliography}{}

\bibitem [\protect \citeauthoryear {%
Balch%
\ \BBA {} Springer%
}{%
Balch%
\ \BBA {} Springer%
}{%
{\protect \APACyear {2015}}%
}]{%
Balch2015}
\APACinsertmetastar {%
Balch2015}%
\begin{APACrefauthors}%
Balch, R.%
\BCBT {}\ \BBA {} Springer, M\BPBI G.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Performance pay, test scores, and student learning
  objectives} {Performance pay, test scores, and student learning
  objectives}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Economics of Education Review}{44}{0}{114 - 125}.
\newblock
\begin{APACrefURL}
  \url{http://www.sciencedirect.com/science/article/pii/S0272775714001034}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{http://dx.doi.org/10.1016/j.econedurev.2014.11.002}
  \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Barlevy%
\ \BBA {} Neal%
}{%
Barlevy%
\ \BBA {} Neal%
}{%
{\protect \APACyear {2012}}%
}]{%
Barlevy2012}
\APACinsertmetastar {%
Barlevy2012}%
\begin{APACrefauthors}%
Barlevy, G.%
\BCBT {}\ \BBA {} Neal, D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Pay for Percentile} {Pay for percentile}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Review}{102}{5}{1805-31}.
\newblock
\begin{APACrefURL}
  \url{http://www.aeaweb.org/articles.php?doi=10.1257/aer.102.5.1805}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1257/aer.102.5.1805} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Bettinger%
\ \BBA {} Long%
}{%
Bettinger%
\ \BBA {} Long%
}{%
{\protect \APACyear {2004}}%
}]{%
bettinger2004}
\APACinsertmetastar {%
bettinger2004}%
\begin{APACrefauthors}%
Bettinger, E.%
\BCBT {}\ \BBA {} Long, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2004}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Do college instructors matter? The effects of adjuncts
  on students’ interests and success} {Do college instructors matter? the
  effects of adjuncts on students’ interests and success}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Review of Economics and Statistics}{92}{3}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Glewwe%
, Ilias%
\BCBL {}\ \BBA {} Kremer%
}{%
Glewwe%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2010}}%
}]{%
Glewwe2010}
\APACinsertmetastar {%
Glewwe2010}%
\begin{APACrefauthors}%
Glewwe, P.%
, Ilias, N.%
\BCBL {}\ \BBA {} Kremer, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2010}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Teacher Incentives} {Teacher incentives}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Journal: Applied
  Economics}{2}{3}{205-27}.
\newblock
\begin{APACrefURL}
  \url{http://www.aeaweb.org/articles.php?doi=10.1257/app.2.3.205}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1257/app.2.3.205} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Goldhaber%
\ \BBA {} Walch%
}{%
Goldhaber%
\ \BBA {} Walch%
}{%
{\protect \APACyear {2012}}%
}]{%
Goldhaber2012}
\APACinsertmetastar {%
Goldhaber2012}%
\begin{APACrefauthors}%
Goldhaber, D.%
\BCBT {}\ \BBA {} Walch, J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Strategic pay reform: A student outcomes-based
  evaluation of {Denver's} ProComp teacher pay initiative} {Strategic pay
  reform: A student outcomes-based evaluation of {Denver's} procomp teacher pay
  initiative}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Economics of Education Review}{31}{6}{1067 - 1083}.
\newblock
\begin{APACrefURL}
  \url{http://www.sciencedirect.com/science/article/pii/S0272775712000751}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{http://dx.doi.org/10.1016/j.econedurev.2012.06.007}
  \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Goodman%
\ \BBA {} Turner%
}{%
Goodman%
\ \BBA {} Turner%
}{%
{\protect \APACyear {2013}}%
}]{%
Goodman2013}
\APACinsertmetastar {%
Goodman2013}%
\begin{APACrefauthors}%
Goodman, S\BPBI F.%
\BCBT {}\ \BBA {} Turner, L\BPBI J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {{The Design of Teacher Incentive Pay and Educational
  Outcomes: Evidence from the New York City Bonus Program}} {{The Design of
  Teacher Incentive Pay and Educational Outcomes: Evidence from the New York
  City Bonus Program}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Labor Economics}{31}{2}{409 - 420}.
\newblock
\begin{APACrefURL}
  \url{http://ideas.repec.org/a/ucp/jlabec/doi10.1086-668676.html}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Kane%
, Rockoff%
\BCBL {}\ \BBA {} Staiger%
}{%
Kane%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2008}}%
}]{%
Kane2008}
\APACinsertmetastar {%
Kane2008}%
\begin{APACrefauthors}%
Kane, T\BPBI J.%
, Rockoff, J\BPBI E.%
\BCBL {}\ \BBA {} Staiger, D\BPBI O.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2008}{December}{}.
\newblock
{\BBOQ}\APACrefatitle {What does certification tell us about teacher
  effectiveness? Evidence from New York City} {What does certification tell us
  about teacher effectiveness? evidence from new york city}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Economics of Education Review}{27}{6}{615-631}.
\newblock
\begin{APACrefURL}
  \url{http://ideas.repec.org/a/eee/ecoedu/v27y2008i6p615-631.html}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Lavy%
}{%
Lavy%
}{%
{\protect \APACyear {2002}}%
}]{%
Lavy2002}
\APACinsertmetastar {%
Lavy2002}%
\begin{APACrefauthors}%
Lavy, V.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2002}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Evaluating the Effect of Teachers' Group Performance
  Incentives on Pupil Achievement} {Evaluating the effect of teachers' group
  performance incentives on pupil achievement}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Political Economy}{110}{6}{pp. 1286-1317}.
\newblock
\begin{APACrefURL} \url{http://www.jstor.org/stable/10.1086/342810}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Lavy%
}{%
Lavy%
}{%
{\protect \APACyear {2009}}%
}]{%
Lavy2009}
\APACinsertmetastar {%
Lavy2009}%
\begin{APACrefauthors}%
Lavy, V.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Performance Pay and Teachers' Effort, Productivity, and
  Grading Ethics} {Performance pay and teachers' effort, productivity, and
  grading ethics}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Review}{99}{5}{1979-2011}.
\newblock
\begin{APACrefURL}
  \url{http://www.aeaweb.org/articles.php?doi=10.1257/aer.99.5.1979}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1257/aer.99.5.1979} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Muralidharan%
\ \BBA {} Sundararaman%
}{%
Muralidharan%
\ \BBA {} Sundararaman%
}{%
{\protect \APACyear {2011}}%
}]{%
Muralidharan2011}
\APACinsertmetastar {%
Muralidharan2011}%
\begin{APACrefauthors}%
Muralidharan, K.%
\BCBT {}\ \BBA {} Sundararaman, V.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Teacher Performance Pay: Experimental Evidence from
  India} {Teacher performance pay: Experimental evidence from india}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Political Economy}{119}{1}{pp. 39-77}.
\newblock
\begin{APACrefURL} \url{http://www.jstor.org/stable/10.1086/659655}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Springer%
\ \protect \BOthers {.}}{%
Springer%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2011}}%
}]{%
Springer2011}
\APACinsertmetastar {%
Springer2011}%
\begin{APACrefauthors}%
Springer, M\BPBI G.%
, Ballou, D.%
, Hamilton, L.%
, Le, V\BHBI N.%
, Lockwood, J.%
, McCaffrey, D\BPBI F.%
\BDBL {}Stecher, B\BPBI M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Teacher Pay for Performance: Experimental Evidence from
  the Project on Incentives in Teaching (POINT).} {Teacher pay for performance:
  Experimental evidence from the project on incentives in teaching
  (point).}{\BBCQ}
\newblock
\APACjournalVolNumPages{Society for Research on Educational
  Effectiveness}{}{}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Woessmann%
}{%
Woessmann%
}{%
{\protect \APACyear {2011}}%
}]{%
Woessmann2011}
\APACinsertmetastar {%
Woessmann2011}%
\begin{APACrefauthors}%
Woessmann, L.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Cross-country evidence on teacher performance pay}
  {Cross-country evidence on teacher performance pay}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Economics of Education Review}{30}{3}{404 - 418}.
\newblock
\begin{APACrefURL}
  \url{http://www.sciencedirect.com/science/article/pii/S0272775710001731}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{http://dx.doi.org/10.1016/j.econedurev.2010.12.008}
  \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\end{thebibliography}
