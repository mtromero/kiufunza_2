use "CreatedData/4 Intervention/TwaEL_2015/EL2015_Student_FinalRun_Mashindano_CLEAN_quality.dta", clear
append using "CreatedData/4 Intervention/TwaEL_2015/EL2015_Student_FinalRun_StadiandControl_CLEAN_quality.dta"
tostring studentID, format(%08.0f) generate(StuID_15)
merge m:1 StuID_15 using "RawData/12 Intervention_KFII/1 Endline 2015/1 Student Names List/2015_Student_Name_List_nopii"
replace tested2015=0 if tested2015==.
drop _merge
**************** get randomization status
merge m:1 SchoolID using "RawData/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2) update replace
drop if _merge==2
drop _merge
merge m:1 SchoolID using "CommScripts/Kigoma Sample/KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update replace
drop if _merge==2
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440
tab treatarm2, gen(TD) 

*ONYL SHEMU A
drop *eng_b_*
drop *eng_c_*

drop *kis_b_*
drop *kis_c_*

drop *his_b_*
drop *his_c_*

******************************************************************************
******************************************************************************
*... if first two in skills are wrong, then don't ask the rest!
foreach skill in si ma se{
	 forvalues i=3/6{
		replace grd1_kis_a_`skill'_`i'=0 if grd1_kis_a_`skill'_1==0 & grd1_kis_a_`skill'_2==0 & grade==1
	}
}

foreach skill in l w se{
	forvalues i=3/6{
		replace grd1_eng_a_`skill'_`i'=0 if grd1_eng_a_`skill'_1==0 & grd1_eng_a_`skill'_2==0 & grade==1
	}
}

foreach skill in id uta bwa j t{
	replace grd1_his_a_`skill'_3=0 if grd1_his_a_`skill'_1==0 & grd1_his_a_`skill'_2==0 & grade==1
} 

*Calculcate number of questions per skill
egen Kis_Silabi_Grd1=rowtotal(grd1_kis_a_si_1 grd1_kis_a_si_2 grd1_kis_a_si_3 grd1_kis_a_si_4 grd1_kis_a_si_5 grd1_kis_a_si_6) if grade==1
egen Kis_Maneno_Grd1=rowtotal(grd1_kis_a_ma_1 grd1_kis_a_ma_2 grd1_kis_a_ma_3 grd1_kis_a_ma_4 grd1_kis_a_ma_5 grd1_kis_a_ma_6) if grade==1
egen Kis_Sentenci_Grd1=rowtotal(grd1_kis_a_se_1 grd1_kis_a_se_2 grd1_kis_a_se_3 grd1_kis_a_se_4 grd1_kis_a_se_5 grd1_kis_a_se_6) if grade==1

egen Eng_Letter_Grd1=rowtotal(grd1_eng_a_l_1 grd1_eng_a_l_2 grd1_eng_a_l_3 grd1_eng_a_l_4 grd1_eng_a_l_5 grd1_eng_a_l_6) if grade==1
egen Eng_Word_Grd1=rowtotal(grd1_eng_a_w_1 grd1_eng_a_w_2 grd1_eng_a_w_3 grd1_eng_a_w_4 grd1_eng_a_w_5 grd1_eng_a_w_6) if grade==1
egen Eng_Sentences_Grd1=rowtotal(grd1_eng_a_se_1 grd1_eng_a_se_2 grd1_eng_a_se_3 grd1_eng_a_se_4 grd1_eng_a_se_5 grd1_eng_a_se_6) if grade==1

egen Math_id_Grd1=rowtotal(grd1_his_a_id_1 grd1_his_a_id_2 grd1_his_a_id_3) if grade==1
egen Math_uta_Grd1=rowtotal(grd1_his_a_uta_1 grd1_his_a_uta_2 grd1_his_a_uta_3) if grade==1
egen Math_bwa_Grd1=rowtotal(grd1_his_a_bwa_1 grd1_his_a_bwa_2 grd1_his_a_bwa_3) if grade==1
egen Math_j_Grd1=rowtotal(grd1_his_a_j_1 grd1_his_a_j_2 grd1_his_a_j_3) if grade==1
egen Math_t_Grd1=rowtotal(grd1_his_a_t_1 grd1_his_a_t_2 grd1_his_a_t_3) if grade==1

*generate passing dummies. Have to be careful since stata thinks missing is infinity
foreach var in Kis_Silabi Kis_Maneno Kis_Sentenci Eng_Letter Eng_Word Eng_Sentences {
	gen `var'_Pass_Grd1=`var'_Grd1>=4 & !missing(`var'_Grd1)
}

foreach var in Math_id Math_uta Math_bwa Math_j Math_t {
	gen `var'_Pass_Grd1=`var'_Grd1>=2 & !missing(`var'_Grd1)
}




capture rename grd1_eng_a_w_5 grd2_eng_a_se_5


*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in ma se{
	 forvalues i=3/6{
		replace grd2_kis_a_`skill'_`i'=0 if grd2_kis_a_`skill'_1==0 & grd2_kis_a_`skill'_2==0 & grade==2
	}
}

foreach skill in w se{
	forvalues i=3/6{
		replace grd2_eng_a_`skill'_`i'=0 if grd2_eng_a_`skill'_1==0 & grd2_eng_a_`skill'_2==0 & grade==2
	}
}

foreach skill in  bwa j t z{
	replace grd2_his_a_`skill'_3=0 if grd2_his_a_`skill'_1==0 & grd2_his_a_`skill'_2==0 & grade==2
} 



egen Kis_Maneno_Grd2=rowtotal(grd2_kis_a_ma_1 grd2_kis_a_ma_2 grd2_kis_a_ma_3 grd2_kis_a_ma_4 grd2_kis_a_ma_5 grd2_kis_a_ma_6) if grade==2
egen Kis_Sentenci_Grd2=rowtotal(grd2_kis_a_se_1 grd2_kis_a_se_2 grd2_kis_a_se_3 grd2_kis_a_se_4 grd2_kis_a_se_5 grd2_kis_a_se_6) if grade==2
gen Kis_Aya_Grd2=grd2_kis_a_a


egen Eng_Word_Grd2=rowtotal(grd2_eng_a_w_1 grd2_eng_a_w_2 grd2_eng_a_w_3 grd2_eng_a_w_4 grd2_eng_a_w_5 grd2_eng_a_w_6) if grade==2
egen Eng_Sentences_Grd2=rowtotal(grd2_eng_a_se_1 grd2_eng_a_se_2 grd2_eng_a_se_3 grd2_eng_a_se_4 grd2_eng_a_se_5 grd2_eng_a_se_6) if grade==2
gen Eng_Paragraph_Grd2=grd2_eng_a_p

egen Math_bwa_Grd2=rowtotal(grd2_his_a_bwa_1 grd2_his_a_bwa_2 grd2_his_a_bwa_3) if grade==2
egen Math_j_Grd2=rowtotal(grd2_his_a_j_1 grd2_his_a_j_2 grd2_his_a_j_3) if grade==2
egen Math_t_Grd2=rowtotal(grd2_his_a_t_1 grd2_his_a_t_2 grd2_his_a_t_3) if grade==2
egen Math_z_Grd2=rowtotal(grd2_his_a_z_1 grd2_his_a_z_2 grd2_his_a_z_3) if grade==2

foreach var in  Kis_Maneno Kis_Sentenci Eng_Word Eng_Sentences{
	gen `var'_Pass_Grd2=`var'_Grd2>=4 & !missing(`var'_Grd2)
}

foreach var in  Math_bwa Math_j Math_t Math_z{
	gen `var'_Pass_Grd2=`var'_Grd2>=2 & !missing(`var'_Grd2)
}

foreach var in  Kis_Aya Eng_Paragraph{
	gen `var'_Pass_Grd2=`var'_Grd2==3 & !missing(`var'_Grd2)
}



*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_kis_a_m_1=0 if grd3_kis_a_h==0 & grade==3
replace grd3_kis_a_m_2=0 if grd3_kis_a_h==0 & grade==3


*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_eng_a_c_1=0 if grd3_eng_a_s==0 & grade==3
replace grd3_eng_a_c_2=0 if grd3_eng_a_s==0 & grade==3

*First data cleaning process for math is as before...
foreach skill in  j t z g{
	replace grd3_his_a_`skill'_3=0 if grd3_his_a_`skill'_1==0 & grd3_his_a_`skill'_2==0 & grade==3

} 



gen Kis_Story_Grd3=grd3_kis_a_h if grade==3
egen Kis_Comp_Grd3=rowtotal(grd3_kis_a_m_1 grd3_kis_a_m_2) if grade==3
     
gen Eng_Story_Grd3=grd3_eng_a_s if grade==3
egen Eng_Comp_Grd3=rowtotal(grd3_eng_a_c_1 grd3_eng_a_c_2) if grade==3
 

egen Math_j_Grd3=rowtotal(grd3_his_a_j_1 grd3_his_a_j_2 grd3_his_a_j_3) if grade==3
egen Math_t_Grd3=rowtotal(grd3_his_a_t_1 grd3_his_a_t_2 grd3_his_a_t_3) if grade==3
egen Math_z_Grd3=rowtotal(grd3_his_a_z_1 grd3_his_a_z_2 grd3_his_a_z_3) if grade==3
egen Math_g_Grd3=rowtotal(grd3_his_a_g_1 grd3_his_a_g_2 grd3_his_a_g_3) if grade==3


foreach var in  Math_j Math_t Math_z Math_g{
	gen `var'_Pass_Grd3=`var'_Grd3>=2 & !missing(`var'_Grd3)
}

foreach var in  Kis_Story Eng_Story{
	gen `var'_Pass_Grd3=`var'_Grd3==3 & !missing(`var'_Grd3)
}

foreach var in  Kis_Comp Eng_Comp {
	gen `var'_Pass_Grd3=`var'_Grd3==2 & !missing(`var'_Grd3)
}


label var Kis_Silabi_Pass_Grd1 "Silbai"
label var Kis_Maneno_Pass_Grd1 "Maneno"
label var Kis_Sentenci_Pass_Grd1 "Sentenci"
label var Eng_Letter_Pass_Grd1 "Letter"
label var Eng_Word_Pass_Grd1 "Word"
label var Eng_Sentences_Pass_Grd1 "Sentences"
label var Math_id_Pass_Grd1 "ID"
label var Math_uta_Pass_Grd1 "Counting"
label var Math_bwa_Pass_Grd1 "Inequality"
label var Math_j_Pass_Grd1 "Addition"
label var Math_t_Pass_Grd1 "Subtraction"
label var Kis_Maneno_Pass_Grd2 "Maneno"
label var Kis_Sentenci_Pass_Grd2 "Sentenci"
label var Eng_Word_Pass_Grd2  "Word"
label var Eng_Sentences_Pass_Grd2 "Sentences"
label var Math_bwa_Pass_Grd2 "Inequality"
label var Math_j_Pass_Grd2 "Addition"
label var Math_t_Pass_Grd2 "Subtraction"
label var Math_z_Pass_Grd2 "Multiplication"
label var Kis_Aya_Pass_Grd2 "Aya"
label var Eng_Paragraph_Pass_Grd2 "Paragraph"
label var Math_j_Pass_Grd3 "Addition"
label var Math_t_Pass_Grd3 "Subtraction"
label var Math_z_Pass_Grd3 "Multiplication"
label var Math_g_Pass_Grd3 "Division"
label var Kis_Story_Pass_Grd3 "Hadithi"
label var Eng_Story_Pass_Grd3 "Story"
label var Kis_Comp_Pass_Grd3 "Ufahamu"
label var Eng_Comp_Pass_Grd3 "Comprehension"

forvalues val=1/3{
foreach var of varlist grd1_kis_a_si_1-grd1_his_a_t_3 grd2_kis_a_ma_1-grd3_his_a_g_3 {
sum `var' if Grade==`val' & treatarm2==3
capture gen Z_`var'=.
replace Z_`var'= (`var'-r(mean))/r(sd) if Grade==`val'
}
}


egen Z_kiswahili=rowtotal(Z_*kis*),missing
egen Z_kiingereza=rowtotal(Z_*eng*),missing
egen Z_hisabati=rowtotal(Z_*his*),missing



***Now we standarize by grade subject
forvalues val=1/3{
foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati  {
sum `var' if Grade==`val' & treatarm2==3
replace `var'=(`var'-r(mean))/r(sd) if Grade==`val'
}
}


foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati {
sum `var' if  treatarm2==3
replace `var'=(`var'-r(mean))/r(sd)
}

pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score

pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score


keep name districtid district SchoolID grade gender treatarm2 Kis_Silabi_Grd1- Eng_Comp_Pass_Grd3 treatment2 Z_kiswahili Z_kiingereza Z_hisabati Z_ScoreFocal


foreach i in 1 2 3{
estpost tabstat *Pass_Grd`i', by(treatment2) statistics(mean) columns(statistics)
esttab using "Results/ExcelTables/PassRate_ByTreatment_Grade`i'.csv", replace  main(mean)  nostar unstack  nonote nomtitle nonumber label
estpost tabstat *Pass_Grd`i', by(districtid) statistics(mean) columns(statistics)
esttab using "Results/ExcelTables/PassRate_ByDistrict_Grade`i'.csv", replace  main(mean)  nostar unstack  nonote nomtitle nonumber label

estpost tabstat Z_*, by(treatment2) statistics(mean) columns(statistics)
esttab using "Results/ExcelTables/ZScore_ByTreatment_Grade`i'.csv", replace  main(mean)  nostar unstack  nonote nomtitle nonumber label
estpost tabstat Z_*, by(districtid) statistics(mean) columns(statistics)
esttab using "Results/ExcelTables/ZScore_ByDistrict_Grade`i'.csv", replace  main(mean)  nostar unstack  nonote nomtitle nonumber label


}
