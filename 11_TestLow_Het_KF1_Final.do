**********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
fvset base default treatarm 
gen Diff_T2=date_twa_T2-date_edi_T2
gen Diff_T5=date_twa_T5-date_edi_T5
gen Week_T2=week(date_edi_T2)
gen Week_T5=week(date_edi_T5)


global AggregateDep 	Z_hisabati Z_kiswahili Z_kiingereza /*this should be added in the future... Z_sayansi Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal*/ 
global AggregateDep_Karthik 	Z_hisabati Z_kiswahili Z_kiingereza   /*Z_kiingereza  Z_ScoreFocal this should be added in the future...*/
global AggregateDep_int 	Z_hisabati Z_kiswahili Z_kiingereza
 
global AggregateDep_NonEng 	Z_hisabati Z_kiswahili 
global AggregateDep_Eng 	Z_kiingereza

global treatmentlist TreatmentLevels TreatmentGains
gen TreatmentLevels2=TreatmentLevels
gen TreatmentGains2=TreatmentGains
global treatmentlist2 TreatmentLevels2 TreatmentGains2
*enrollment2015_T1 Rural_T1 ClassesOutside_T1 Electricity_T1

label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"      
*c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza)##c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza) ///


global student2 shoes_T2 socks_T2 dirty_T2 uniformdirty_T2 uniformtorn_T2  ringworm_T2 CloseToeShoe_T2

recode bookshome_T2 (-98=.)
global studentoutcomes studentsfight_T2 sing_T2 bookshome_T2

bys SchoolID: egen Z_kiswahili_T0_G=mean(Z_kiswahili_T0)
bys SchoolID: egen Z_kiingereza_T0_G=mean(Z_kiingereza_T0)
bys SchoolID: egen Z_hisabati_T0_G=mean(Z_hisabati_T0)
pca Z_hisabati_T0  Z_kiingereza_T0 Z_kiswahili_T0
predict Z_ScoreFocal_T0, score
*First lets create the tables Karthik Wants

label var Z_kiswahili_T2 Swahili
label var Z_kiswahili_T5 Swahili
label var Z_kiingereza_T2 English
label var Z_kiingereza_T5 English
label var Z_hisabati_T2 Math
label var Z_hisabati_T5 Math
label var Z_sayansi_T2 Science
label var Z_sayansi_T5 Science
label var Z_ScoreFocal_T2 "Focal Subjects"
label var Z_ScoreFocal_T5 "Focal Subjects"
recode missschool_T5 (2=0) 

gen COD_KF1=(treatarm==1 | treatarm==3)

***********************************************
***********************************************
 

label var TreatmentLevels "Levels"
label var TreatmentGains "P4Pctile"

label var COD_KF1 "Incentives in previous RCT"
label var COD_KF1 "Incentives in previous RCT"

gen TLev0=TreatmentLevels*(1-COD_KF1)
gen TP4P0=TreatmentGains*(1-COD_KF1)
gen TLev1=TreatmentLevels*(COD_KF1)
gen TP4P1=TreatmentGains*(COD_KF1)

label var TLev0 "Levels \$\times\$ Control in previous RCT (\$\alpha_1$)"
label var TLev1 "Levels \$\times\$ Incentives in previous RCT (\$\alpha_2$)"
label var TP4P0 "P4Pctile \$\times\$ Control in previous RCT (\$\beta_1$)"
label var TP4P1 "P4Pctile \$\times\$ Incentives in previous RCT (\$\beta_2$)"


eststo clear
eststo: reghdfe Z_ScoreKisawMath_T2  TLev*  TP4P* i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T2<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
lincom TLev0-TLev1
estadd scalar p1=r(p)
lincom TP4P0-TP4P1
estadd scalar p2=r(p)
lincom TLev0-TP4P0
estadd scalar p3=r(p)
lincom TLev1-TP4P1
estadd scalar p4=r(p)
lincom (TLev1-TP4P1)-(TLev0-TP4P0)
estadd scalar p5=r(p)

		
eststo: reghdfe Z_ScoreKisawMath_T5  TLev*  TP4P* i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T5<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
lincom TLev0-TLev1
estadd scalar p1=r(p)
lincom TP4P0-TP4P1
estadd scalar p2=r(p)
lincom TLev0-TP4P0
estadd scalar p3=r(p)
lincom TLev1-TP4P1
estadd scalar p4=r(p)
lincom (TLev1-TP4P1)-(TLev0-TP4P0)
estadd scalar p5=r(p)

	
	


esttab  using "$latexcodes/Het_Previous_LS.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TLev*  TP4P*) stats(N p1 p2 p3 p4 p5, fmt(%9.0gc %9.2gc  %9.2gc  %9.2gc  %9.2gc  %9.2gc) ///
labels("N. of obs." "p-value(\$H_0:\alpha_1=\alpha_2\$)" "p-value(\$H_0:\beta_1=\beta_2\$)" "p-value(\$H_0:\alpha_1=\beta_1\$)" "p-value(\$H_0:\alpha_2=\beta_2\$)" "p-value(\$H_0:(\alpha_1-\beta_1)=(\alpha_2-\beta_2)\$)" )) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)


eststo clear
eststo: reghdfe Z_ScoreKisawMath_T3  TLev*  TP4P* i.Grade_T3  $schoolcontrol  if Grade_T3<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
estadd ysumm
lincom TLev0-TLev1
estadd scalar p1=r(p)
lincom TP4P0-TP4P1
estadd scalar p2=r(p)
lincom TLev0-TP4P0
estadd scalar p3=r(p)
lincom TLev1-TP4P1
estadd scalar p4=r(p)
lincom (TLev1-TP4P1)-(TLev0-TP4P0)
estadd scalar p5=r(p)

	
eststo: reghdfe Z_ScoreKisawMath_T6  TLev*  TP4P* i.Grade_T6  $schoolcontrol  if Grade_T6<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
estadd ysumm
lincom TLev0-TLev1
estadd scalar p1=r(p)
lincom TP4P0-TP4P1
estadd scalar p2=r(p)
lincom TLev0-TP4P0
estadd scalar p3=r(p)
lincom TLev1-TP4P1
estadd scalar p4=r(p)
lincom (TLev1-TP4P1)-(TLev0-TP4P0)
estadd scalar p5=r(p)



esttab  using "$latexcodes/Het_Previous_HS.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TLev*  TP4P*) stats(N p1 p2 p3 p4 p5, fmt(%9.0gc %9.2gc  %9.2gc  %9.2gc  %9.2gc  %9.2gc) ///
labels("N. of obs." "p-value(\$H_0:\alpha_1=\alpha_2\$)" "p-value(\$H_0:\beta_1=\beta_2\$)" "p-value(\$H_0:\alpha_1=\beta_1\$)" "p-value(\$H_0:\alpha_2=\beta_2\$)" "p-value(\$H_0:(\alpha_1-\beta_1)=(\alpha_2-\beta_2)\$)" )) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)


