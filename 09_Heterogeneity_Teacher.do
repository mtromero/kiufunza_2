**********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
fvset base default treatarm 
gen Diff_T2=date_twa_T2-date_edi_T2
gen Diff_T5=date_twa_T5-date_edi_T5
gen Week_T2=week(date_edi_T2)
gen Week_T5=week(date_edi_T5)


global AggregateDep 	Z_hisabati Z_kiswahili Z_kiingereza Z_sayansi Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal /*this should be added in the future...*/
global AggregateDep_Karthik 	Z_hisabati Z_kiswahili Z_kiingereza   /*Z_kiingereza  Z_ScoreFocal this should be added in the future...*/
global AggregateDep_int 	Z_hisabati Z_kiswahili Z_kiingereza
 
global AggregateDep_NonEng 	Z_hisabati Z_kiswahili 
global AggregateDep_Eng 	Z_kiingereza

global treatmentlist TreatmentLevels TreatmentGains
gen TreatmentLevels2=TreatmentLevels
gen TreatmentGains2=TreatmentGains
global treatmentlist2 TreatmentLevels2 TreatmentGains2
*enrollment2015_T1 Rural_T1 ClassesOutside_T1 Electricity_T1

      
*c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza)##c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza) ///


global student2 shoes_T2 socks_T2 dirty_T2 uniformdirty_T2 uniformtorn_T2  ringworm_T2 CloseToeShoe_T2

recode bookshome_T2 (-98=.)
global studentoutcomes studentsfight_T2 sing_T2 bookshome_T2

bys SchoolID: egen Z_kiswahili_T0_G=mean(Z_kiswahili_T0)
bys SchoolID: egen Z_kiingereza_T0_G=mean(Z_kiingereza_T0)
bys SchoolID: egen Z_hisabati_T0_G=mean(Z_hisabati_T0)
pca Z_hisabati_T0  Z_kiingereza_T0 Z_kiswahili_T0
predict Z_ScoreFocal_T0, score
*First lets create the tables Karthik Wants

label var Z_kiswahili_T2 Swahili
label var Z_kiswahili_T5 Swahili
label var Z_kiingereza_T2 English
label var Z_kiingereza_T5 English
label var Z_hisabati_T2 Math
label var Z_hisabati_T5 Math
label var Z_sayansi_T2 Science
label var Z_sayansi_T5 Science
label var Z_ScoreFocal_T2 "Focal Subjects"
label var Z_ScoreFocal_T5 "Focal Subjects" 
drop if GradeID_T2==. & GradeID_T5==.
mmerge SchoolID GradeID_T2 using "$base_out/ConsolidatedYr34/AverageTeacher_T2.dta", ukeep(t05* t10* Index* MaleTeacher* IRTAbility* dictatorgame*)
drop if _merge==2
drop _merge
mmerge SchoolID GradeID_T5 using "$base_out/ConsolidatedYr34/AverageTeacher_T5.dta", ukeep(t05* t10* Index* MaleTeacher* IRTAbility* HT_Rating* dictatorgame*)
drop if _merge==2
drop _merge
		
keep $treatmentlist LagGrade LagseenUwezoTests MissingseenUwezoTests LagpreSchoolYN MissingpreSchoolYN LagGender MissingGender LagAge MissingAge LagZ_ScoreKisawMath LagZ_kiswahili LagZ_hisabati LagZ_kiingereza MissingZ_kiswahili MissingZ_hisabati MissingZ_kiingereza $schoolcontrol GradeID_*  SchoolID DistrictID StrataScore treatarm Z_hisabati* Z_kiswahili* Z_kiingereza* upid  IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement MissingIndexKowledge IndexKowledge ///
IRTAbility* IndexRecall* IndexFatal* IndexTurst* IndexSelfAssesment* IndexRaven* IndexWord* MaleTeacher* t05* t10* HT_Rating* dictatorgame*
drop if upid==""
drop *_T0
drop *_T0_G
*drop *_T1
drop *_T3
drop *_T4
drop *_T6

reshape long Z_hisabati@ Z_kiswahili@ Z_kiingereza@  GradeID@ ///
HT_Rating_hisabati@ HT_Rating_kiingereza@ HT_Rating_kiswahili@ ///
dictatorgame_hisabati@ IRTAbility_hisabati@  IndexSelfAssesment_hisabati@ IndexRecall_hisabati@ IndexFatal_hisabati@ IndexTurst_hisabati@  IndexRaven_hisabati@ IndexWord_hisabati@ MaleTeacher_hisabati@ t05_hisabati@ t10_hisabati@ ///
dictatorgame_kiswahili@ IRTAbility_kiswahili@ IndexSelfAssesment_kiswahili@ IndexRecall_kiswahili@ IndexFatal_kiswahili@ IndexTurst_kiswahili@  IndexRaven_kiswahili@ IndexWord_kiswahili@ MaleTeacher_kiswahili@ t05_kiswahili@ t10_kiswahili@ ///
dictatorgame_kiingereza@ IRTAbility_kiingereza@ IndexSelfAssesment_kiingereza@ IndexRecall_kiingereza@ IndexFatal_kiingereza@ IndexTurst_kiingereza@ IndexRaven_kiingereza@ IndexWord_kiingereza@ MaleTeacher_kiingereza@ t05_kiingereza@ t10_kiingereza@ ///
, i(upid) j(time) string

replace Z_kiingereza=. if time=="_T5" & GradeID<=2
encode time, gen(time2)
drop if time!="_T2" & time !="_T5"

foreach var of varlist t05*{
 replace `var'=2014-`var'
}

replace HT_Rating_kiingereza=. /*just not enough english obs since we didn't ask about them*/
*gen Age=2014-t05
*gen ExperiencePrivate=(t10==1) & !missing(t10)

eststo clear
foreach var in  hisabati kiswahili  kiingereza  {
	foreach het in MaleTeacher t05  IRTAbility HT_Rating  IndexSelfAssesment   {
		preserve
		capture drop covar
		capture drop miss
		gen covar=`het'_`var'
		gen miss=!missing(`het'_`var')
		replace covar=0 if miss==0
		if("`het'"=="HT_Rating") drop if time=="_T2"
			eststo:  reghdfe Z_`var' $treatmentlist c.(${treatmentlist})#c.covar covar miss i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2)
			estadd ysumm
			estadd scalar suma=_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]
			test (_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]=0)
			estadd scalar p=r(p)
			
		restore
			
		
	}  
}
esttab  using "$latexcodes/Hetero_Teacher_Ability.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Math" "Swahili" "English", pattern(1 0 0 0 0  1 0 0 0 0 1 0 0 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mtitles("Male" "Age" "IRT" "HT Rating" "Self Rating" ///
"Male" "Age" "IRT" "HT Rating" "Self Rating" ///
"Male" "Age" "IRT" "HT Rating" "Self Rating" ) ///
keep(c.TreatmentGains#c.covar c.TreatmentLevels#c.covar) stats(N, labels("N. of obs.") fmt(%9.0fc)) ///
varlabels(c.TreatmentGains#c.covar P4Pctile*Covariate c.TreatmentLevels#c.covar Levels*Covariate  ) ///
fragment substitute(\_ _) nolines ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")


reshape long Z_@ HT_Rating_@ t05_@ t10_@ IndexRecall_@ IndexFatal_@ IndexTurst_@ IndexSelfAssesment_@ IndexRaven_@ IndexWord_@ MaleTeacher_@ IRTAbility_@ dictatorgame_@, i(upid time) j(subject) string
drop if subject=="kiingereza"
encode subject, gen(subject2)

eststo clear
	foreach het in MaleTeacher t05  IRTAbility HT_Rating  IndexSelfAssesment   {
		preserve
		capture drop covar
		capture drop miss
		gen covar=`het'_
		gen miss=!missing(`het'_)
		replace covar=0 if miss==0
		if("`het'"=="HT_Rating") drop if time=="_T2"
			eststo:  reghdfe Z_ $treatmentlist c.(${treatmentlist})#c.covar covar miss i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2##subject2)
			estadd ysumm
			estadd scalar suma=_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]
			test (_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]=0)
			estadd scalar p=r(p)
			sum covar if miss==1
			estadd scalar mean_covar=r(mean)
		restore
}
esttab  using "$latexcodes/Hetero_Teacher_Ability_stacked.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace   ///
mtitles("Male" "Age" "IRT" "HT Rating" "Self Rating") ///
keep(TreatmentGains TreatmentLevels c.TreatmentGains#c.covar c.TreatmentLevels#c.covar covar) stats(mean_covar N, labels("Covariate mean" "N. of obs.") fmt(%9.2gc %9.0fc)) ///
varlabels(c.TreatmentGains#c.covar P4Pctile*Covariate c.TreatmentLevels#c.covar Levels*Covariate covar Covariate  ) ///
fragment substitute(\_ _) nolines ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")



/*
foreach var in  hisabati kiswahili  kiingereza  {
	eststo clear
	foreach het in MaleTeacher t05  IRTAbility HT_Rating  IndexSelfAssesment   {
		preserve
		capture drop covar
		capture drop miss
		gen covar=`het'_`var'
		gen miss=!missing(`het'_`var')
		replace covar=0 if miss==0
		if("`het'"=="HT_Rating") drop if time=="_T2"
		eststo:  reghdfe Z_`var' $treatmentlist c.(${treatmentlist})#c.covar covar miss i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2)
		estadd ysumm
		if("`het'"=="HT_Rating" & "`var'"=="kiingereza") estadd scalar N=0, replace
		estadd scalar suma=_b[c.TreatmentGains#c.covar]-_b[c.TreatmentLevels#c.covar]
		test (_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]=0)
		estadd scalar p=r(p)	
		restore
	}
	
	esttab  using "$latexcodes/Hetero_Teacher_Ability_`var'.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace    ///
	mtitles("Male" "Age" "IRT" "HT Rating" "Self Rating" ) ///
	keep(c.TreatmentGains#c.covar c.TreatmentLevels#c.covar) ///
	stats(N  suma p, labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") fmt(%9.0fc %9.2gc %9.2gc) star(suma)) ///
	varlabels(c.TreatmentGains#c.covar "P4Pctile*Covariate (\$\alpha_1\$)" c.TreatmentLevels#c.covar "Levels*Covariate (\$\alpha_2\$)"  ) ///
	fragment substitute(\_ _) nolines ///
	nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

}
*/


*********VALIDATION OF THESE RATINGS *********************
/*
reshape long Z_@  ///
HT_Rating_@  ///
dictatorgame_@ IRTAbility_@  IndexSelfAssesment_@ IndexRecall_@ IndexFatal_@ IndexTurst_@  IndexRaven_@ IndexWord_@ MaleTeacher_@ t05_@ t10_@ ///
, i(upid time) j(subject) string

encode subject, gen(subject2)


reghdfe  Z_ IRTAbility_  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3 & TreatmentLevels==0 &  TreatmentGains==0 & subject!="kiingereza", vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2##subject2)
reghdfe  Z_ HT_Rating_ i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3 & TreatmentLevels==0 &  TreatmentGains==0 & subject!="kiingereza", vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2##subject2)
reghdfe  Z_ IndexSelfAssesment_  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3 & TreatmentLevels==0 &  TreatmentGains==0 & subject!="kiingereza", vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2##subject2)
*/

 
/*
eststo clear
foreach var in  hisabati kiswahili   {
	foreach het in  IndexFatal IndexTurst   IndexSelfAssesment{
		capture drop covar
		capture drop miss
		gen covar=`het'_`var'
		gen miss=!missing(`het'_`var')
		replace covar=0 if miss==0
		
			eststo:  reghdfe Z_`var' $treatmentlist c.(${treatmentlist})#c.covar covar miss i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2)
			estadd ysumm
		
	}  
}
esttab  using "$latexcodes/Hetero_Teacher_Index.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Math" "Swahili", pattern(1 0 0   1 0 0 ) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mtitles( "Fatal" "Trust"  "Self Assessment" ///
"Fatal" "Trust"  "Self Assessment" ///
"Fatal" "Trust"  "Self Assessment") ///
keep(c.TreatmentGains#c.covar c.TreatmentLevels#c.covar) stats(N, labels("N. of obs.") fmt(%9.0fc)) ///
varlabels(c.TreatmentGains#c.covar Gains*Covariate c.TreatmentLevels#c.covar Levels*Covariate  ) ///
fragment ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")



eststo clear
foreach var in  hisabati kiswahili   {
	foreach het in  dictatorgame {
		capture drop covar
		capture drop miss
		gen covar=`het'_`var'
		gen miss=!missing(`het'_`var')
		replace covar=0 if miss==0
		
			eststo:  reghdfe Z_`var' $treatmentlist c.(${treatmentlist})#c.covar covar miss i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2)
			estadd ysumm
		
	}  
}
esttab  using "$latexcodes/Hetero_Teacher_dictatorgame.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Math" "Swahili", pattern(1 0 0   1 0 0 ) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mtitles( "Fatal" "Trust"  "Self Assessment" ///
"Fatal" "Trust"  "Self Assessment" ///
"Fatal" "Trust"  "Self Assessment") ///
keep(c.TreatmentGains#c.covar c.TreatmentLevels#c.covar) stats(N, labels("N. of obs.") fmt(%9.0fc)) ///
varlabels(c.TreatmentGains#c.covar Gains*Covariate c.TreatmentLevels#c.covar Levels*Covariate  ) ///
fragment ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")
*/

/*

eststo clear
foreach var in  hisabati kiswahili   {
	foreach het in HT_Rating{
		capture drop covar
		capture drop miss
		gen covar=`het'_`var'
		gen miss=!missing(`het'_`var')
		replace covar=0 if miss==0
		
			eststo:  reghdfe Z_`var' $treatmentlist c.(${treatmentlist})#c.covar covar miss i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm##time2)
			estadd ysumm
		
	}  
}
esttab  using "$latexcodes/Hetero_Teacher_Ability_ByPrincipal.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Math" "Swahili", pattern(1 0 0  1 0 0 ) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mtitles("IRT" "Male" "Age" ///
"IRT" "Male" "Age" ///
"IRT" "Male" "Age") ///
keep(c.TreatmentGains#c.covar c.TreatmentLevels#c.covar) stats(N, labels("N. of obs.") fmt(%9.0fc)) ///
varlabels(c.TreatmentGains#c.covar Gains*Covariate c.TreatmentLevels#c.covar Levels*Covariate  ) ///
fragment ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")
