**********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
fvset base default treatarm 

global AggregateDep 	Z_hisabati Z_kiswahili Z_kiingereza Z_sayansi Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal /*this should be added in the future...*/
global AggregateDep_Karthik 	Z_hisabati Z_kiswahili Z_kiingereza   /*Z_kiingereza  Z_ScoreFocal this should be added in the future...*/
global AggregateDep_int 	Z_hisabati Z_kiswahili Z_kiingereza
 
global AggregateDep_NonEng 	Z_hisabati Z_kiswahili 
global AggregateDep_Eng 	Z_kiingereza

global treatmentlist TreatmentLevels TreatmentGains
gen TreatmentLevels2=TreatmentLevels
gen TreatmentGains2=TreatmentGains
global treatmentlist2 TreatmentLevels2 TreatmentGains2
*enrollment2015_T1 Rural_T1 ClassesOutside_T1 Electricity_T1
global schoolcontrol  PTR_T1  SingleShift_T1 DistanceIndex_T1 InfrastructureIndex_T1
global studentcontrol c.LagseenUwezoTests#c.MissingseenUwezoTests MissingseenUwezoTests c.LagpreSchoolYN#MissingpreSchoolYN MissingpreSchoolYN ///
c.LagGender#MissingGender MissingGender c.LagAge#c.MissingAge MissingAge ///
c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza)##c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza) ///
MissingZ_kiswahili MissingZ_hisabati MissingZ_kiingereza
 
      
global student2 shoes_T2 socks_T2 dirty_T2 uniformdirty_T2 uniformtorn_T2  ringworm_T2 CloseToeShoe_T2

recode bookshome_T2 (-98=.)
global studentoutcomes studentsfight_T2 sing_T2 bookshome_T2

bys SchoolID: egen Z_kiswahili_T0_G=mean(Z_kiswahili_T0)
bys SchoolID: egen Z_kiingereza_T0_G=mean(Z_kiingereza_T0)
bys SchoolID: egen Z_hisabati_T0_G=mean(Z_hisabati_T0)
pca Z_hisabati_T0  Z_kiingereza_T0 Z_kiswahili_T0
predict Z_ScoreFocal_T0, score
*First lets create the tables Karthik Wants

label var Z_kiswahili_T2 Swahili
label var Z_kiswahili_T5 Swahili
label var Z_kiingereza_T2 English
label var Z_kiingereza_T5 English
label var Z_hisabati_T2 Math
label var Z_hisabati_T5 Math
label var Z_ScoreFocal_T2 "Focal Subjects"
label var Z_ScoreFocal_T5 "Focal Subjects"


keep $treatmentlist LagGrade LagseenUwezoTests MissingseenUwezoTests LagpreSchoolYN MissingpreSchoolYN LagGender MissingGender LagAge MissingAge LagZ_kiswahili LagZ_hisabati LagZ_kiingereza MissingZ_kiswahili MissingZ_hisabati MissingZ_kiingereza $schoolcontrol GradeID_*  SchoolID DistrictID StrataScore treatarm Z_hisabati* Z_kiswahili*  upid  IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement MissingIndexKowledge IndexKowledge
*gen covar=LagAge
*reghdfe Z_hisabati_T5 $treatmentlist c.(${treatmentlist})#c.covar covar i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T5<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
*reghdfe Z_hisabati_T2 $treatmentlist c.(${treatmentlist})#c.covar covar i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T2<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
*drop covar

drop if upid==""
reshape long Z_hisabati@ Z_kiswahili@  GradeID@, i(upid) j(time) string
*replace Z_kiingereza=. if time=="_T5" & GradeID<=2
encode time, gen(time2)
drop if time!="_T2" & time !="_T5"

label var Z_kiswahili "Swahili" 
*label var Z_kiingereza "English" 
label var Z_hisabati "Math" 

*gen covar=LagAge
*reghdfe Z_kiingereza $treatmentlist c.(${treatmentlist})#c.covar covar i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm time2)
	
eststo clear
foreach var in $AggregateDep_NonEng {
	foreach het in LagGender LagAge Lag`var'{
		capture drop covar
		gen covar=`het'
		if "`var'"=="Z_kiingereza"{
			eststo:  reghdfe `var' $treatmentlist c.(${treatmentlist})#c.covar covar i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm time2)
			estadd ysumm
			estadd scalar suma=_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]
			test (_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]=0)
			estadd scalar p=r(p)	
		}
		if "`var'"!="Z_kiingereza"{
			eststo:  reghdfe `var' $treatmentlist c.(${treatmentlist})#c.covar covar i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm time2)
			estadd ysumm
			estadd scalar suma=_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]
			test (_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]=0)
			estadd scalar p=r(p)	
		}
	}  
}


esttab  using "$latexcodes/Hetero_Student.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Math" "Swahili", pattern(1 0 0  1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mtitles("Male" "Age" "Test(Yr0)" ///
"Male" "Age" "Test(Yr0)" ///
"Male" "Age" "Test(Yr0)") ///
keep(TreatmentGains TreatmentLevels c.TreatmentGains#c.covar c.TreatmentLevels#c.covar covar) stats(N  suma p, labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") fmt(%9.0fc %9.2gc %9.2gc) star(suma)) ///
varlabels(c.TreatmentGains#c.covar "P4Pctile*Covariate (\$\alpha_1\$)" c.TreatmentLevels#c.covar "Levels*Covariate (\$\alpha_2\$)" covar Covariate ) ///
fragment substitute(\_ _) nolines ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")


foreach var in $AggregateDep_NonEng {
	gen Weak`var'=.
	foreach gr in 0 1 2 3 4 5{
		sum Lag`var' if LagGrade==`gr',d
		replace Weak`var'=(Lag`var'<=r(p50)) if LagGrade==`gr' & !missing(Lag`var')
	}
}

foreach var in $AggregateDep_NonEng {
	bys SchoolID: egen FracWeak`var'=mean(Weak`var')
}

*DistanceIndex_T1		
eststo clear
foreach var in $AggregateDep_NonEng {
	foreach het in InfrastructureIndex_T1 PTR_T1  {
		capture drop covar
		gen covar=`het'
		if "`var'"=="Z_kiingereza" {
			eststo:  reghdfe `var' $treatmentlist c.(${treatmentlist})#c.covar covar i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm time2)
			estadd ysumm
			estadd scalar suma=_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]
			test (_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]=0)
			estadd scalar p=r(p)	
		}
		if "`var'"!="Z_kiingereza"{
			eststo:  reghdfe `var' $treatmentlist c.(${treatmentlist})#c.covar covar i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm time2)
			estadd ysumm
			estadd scalar suma=_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]
			test (_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]=0)
			estadd scalar p=r(p)	
		}
	}
	foreach het in FracWeak  {
		capture drop covar
		gen covar=`het'`var'
		if "`var'"=="Z_kiingereza" {
			eststo:  reghdfe `var' $treatmentlist c.(${treatmentlist})#c.covar covar i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm time2)
			estadd ysumm
			estadd scalar suma=_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]
			test (_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]=0)
			estadd scalar p=r(p)	
			
		}
		if "`var'"!="Z_kiingereza"{
			eststo:  reghdfe `var' $treatmentlist c.(${treatmentlist})#c.covar covar i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm time2)
			estadd ysumm
			estadd scalar suma=_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]
			test (_b[c.TreatmentGains#c.covar] - _b[c.TreatmentLevels#c.covar]=0)
			estadd scalar p=r(p)	
		}
	}
	
}


esttab  using "$latexcodes/Hetero_School.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Math" "Swahili", pattern(1 0 0  1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mtitles("Facilities" "PTR" "Fraction Weak" ///
"Facilities" "PTR" "Fraction Weak" ///
"Facilities" "PTR" "Fraction Weak")  ///
keep(TreatmentGains TreatmentLevels c.TreatmentGains#c.covar c.TreatmentLevels#c.covar covar) stats(N  suma p, labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)") fmt(%9.0fc %9.2gc %9.2gc) star(suma)) ///
varlabels(c.TreatmentGains#c.covar "P4Pctile*Covariate (\$\alpha_1\$)" c.TreatmentLevels#c.covar "Levels*Covariate (\$\alpha_2\$)"  covar Covariate ) ///
fragment substitute(\_ _) nolines ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")
