**********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
fvset base default treatarm 
gen Diff_T2=date_twa_T2-date_edi_T2
gen Diff_T5=date_twa_T5-date_edi_T5
gen Week_T2=week(date_edi_T2)
gen Week_T5=week(date_edi_T5)


global AggregateDep 	Z_hisabati Z_kiswahili Z_kiingereza /*this should be added in the future... Z_sayansi Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal*/ 
global AggregateDep_Karthik 	Z_hisabati Z_kiswahili Z_kiingereza   /*Z_kiingereza  Z_ScoreFocal this should be added in the future...*/
global AggregateDep_int 	Z_hisabati Z_kiswahili Z_kiingereza
 
global AggregateDep_NonEng 	Z_hisabati Z_kiswahili 
global AggregateDep_Eng 	Z_kiingereza

global treatmentlist TreatmentLevels TreatmentGains
gen TreatmentLevels2=TreatmentLevels
gen TreatmentGains2=TreatmentGains
global treatmentlist2 TreatmentLevels2 TreatmentGains2
*enrollment2015_T1 Rural_T1 ClassesOutside_T1 Electricity_T1

label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"      
*c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza)##c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza) ///


global student2 shoes_T2 socks_T2 dirty_T2 uniformdirty_T2 uniformtorn_T2  ringworm_T2 CloseToeShoe_T2

recode bookshome_T2 (-98=.)
global studentoutcomes studentsfight_T2 sing_T2 bookshome_T2

bys SchoolID: egen Z_kiswahili_T0_G=mean(Z_kiswahili_T0)
bys SchoolID: egen Z_kiingereza_T0_G=mean(Z_kiingereza_T0)
bys SchoolID: egen Z_hisabati_T0_G=mean(Z_hisabati_T0)
pca Z_hisabati_T0  Z_kiingereza_T0 Z_kiswahili_T0
predict Z_ScoreFocal_T0, score
*First lets create the tables Karthik Wants

label var Z_kiswahili_T2 Swahili
label var Z_kiswahili_T5 Swahili
label var Z_kiingereza_T2 English
label var Z_kiingereza_T5 English
label var Z_hisabati_T2 Math
label var Z_hisabati_T5 Math
label var Z_sayansi_T2 Science
label var Z_sayansi_T5 Science
label var Z_ScoreFocal_T2 "Focal Subjects"
label var Z_ScoreFocal_T5 "Focal Subjects"
recode missschool_T5 (2=0) 


gen retained_T2=0 if !missing(stdgrd_T2) & attendance_T2==1
replace retained_T2=1 if stdgrd_T2>atrgrd_T2 & !missing(stdgrd_T2) & attendance_T2==1
replace retained_T2=1 if stdgrd_T2>attgrade_T2 & !missing(stdgrd_T2) & attendance_T2==1

gen retained_T5=0 if !missing(stdgrd_T5) & attendance_T5==1
replace retained_T5=1 if stdgrd_T5>atrgrd_T5 & !missing(stdgrd_T5) & attendance_T5==1
replace retained_T5=1 if stdgrd_T5>attgrade_T5 & !missing(stdgrd_T5) & attendance_T5==1


gen retained_T3=0 if !missing(Grade_T0)  & !missing(Grade_T3) & Grade_T0<=2
replace retained_T3=1 if Grade_T3<(Grade_T0+1) & !missing(Grade_T0) & !missing(Grade_T3) & Grade_T0<=2


gen retained_T6=0 if !missing(Grade_T0)  & !missing(Grade_T6) & Grade_T0<=1
replace retained_T6=1 if Grade_T6<(Grade_T0+1) & !missing(Grade_T0) & !missing(Grade_T6) & Grade_T0<=1
 

eststo clear
foreach time in T2 T5{
	foreach var in retained {
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol $schoolcontrol if GradeID_`time'<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			sum `var'_`time' if treatment2=="Control" & GradeID_`time'<=3
			estadd scalar ymean2=r(mean)
			
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]		
			local tempm=string(abs(_b[TreatmentGains] - _b[TreatmentLevels]), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_coef_diff.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			
			if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			}
			if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			}
			
	    matrix tempm=e(b)
		local tempm=string(abs(tempm[1,1]*100), "%9.2gc")
		local tempm_effect=tempm[1,1]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_levels.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		local tempm=string(abs(tempm[1,2]*100), "%9.2gc")
		local tempm_effect=tempm[1,2]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_gains.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		
		test TreatmentLevels
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		test TreatmentGains
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		
	}  
}

eststo:  reghdfe retained_T5 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol if GradeID_T5<=3 & retained_T2!=1, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]		

sum retained_T5 if treatment2=="Control" & GradeID_T5<=3
estadd scalar ymean2=r(mean)

/*
eststo:  reghdfe retained_T2 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol if GradeID_T2==1, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]		

sum retained_T2 if treatment2=="Control" & GradeID_T2==1
estadd scalar ymean2=r(mean)


eststo:  reghdfe retained_T5 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol if GradeID_T5==1, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]		

sum retained_T2 if treatment2=="Control" & GradeID_T5==1
estadd scalar ymean2=r(mean)
*/

esttab  using "$latexcodes/RegRetention.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)






eststo clear
foreach time in T3 T6{
	foreach var in retained {
			eststo:  reghdfe `var'_`time' $treatmentlist $schoolcontrol i.Grade_T0, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]		
			
			sum `var'_`time' if treatment2=="Control"
			estadd scalar ymean2=r(mean)
	}  
}


esttab  using "$latexcodes/RegRetention_high.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)



eststo clear
foreach time in T2 T5{
	foreach var in retained {
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol $schoolcontrol if GradeID_`time'==4, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]		
			
			sum `var'_`time' if treatment2=="Control"  & GradeID_`time'==4
			estadd scalar ymean2=r(mean)
	}  
}


esttab  using "$latexcodes/RegRetention_Grd4.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)
