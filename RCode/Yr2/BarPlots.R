rm(list=ls())
library(gplots)
library(ggplot2)
setwd("C:/Users/Mauricio/Dropbox/KF_Draft/KFII/LaTeX/")
function_clean=function(Tabla){
  Tabla=apply(Tabla,2,function(x) gsub("\\(","",x))
  Tabla=apply(Tabla,2,function(x) gsub("\\)","",x))
  Tabla=apply(Tabla,2,function(x) gsub("\\\\sym\\{\\*\\*\\*\\}","",x))
  Tabla=apply(Tabla,2,function(x) gsub("\\\\sym\\{\\*\\*\\}","",x))
  Tabla=apply(Tabla,2,function(x) gsub("\\\\sym\\{\\*\\}","",x))
  Tabla=apply(Tabla,2,function(x) gsub("\\\\","",x))
  Tabla=apply(Tabla,2,function(x) trimws(x))
  Tabla=data.frame(Tabla[,1],apply(Tabla[,-1],2,function(x) as.numeric(x)))
  
}
CI=0.95



TablaTestScores=read.delim("tables/RegKarthik_HHControls.tex",sep="&",stringsAsFactors = F,header=F)
TablaTestScores=function_clean(TablaTestScores)


pdf("figures/LowStakes.pdf")
barplot2(as.matrix(TablaTestScores[c(1,3),4:5]),names.arg=c("Math","Kiswahili"),                                                 
         xlab="",ylab="Learning gains in standard deviations",plot.ci=TRUE,ylim=c(0,0.3),main="",
         ci.u=as.matrix(rbind(TablaTestScores[c(1, 3),4:5]))+qnorm(CI)*as.matrix(rbind(TablaTestScores[c(1, 3)+1,4:5])),
         ci.l=as.matrix(rbind(TablaTestScores[c(1, 3),4:5]))-qnorm(CI)*as.matrix(rbind(TablaTestScores[c(1, 3)+1,4:5])),
         beside=T,las=1,col=c("#006EB9","#81B53C"),axes=F,cex=1.2,cex.names=2,cex.lab=1.5)
axis(side=2,las=1,cex.axis=1.2,lwd=0,cex=2)       
legend("top", c("Levels","Gains"), fill=c("#006EB9","#81B53C"),ncol=2,bty="n",inset=0,cex=2)
dev.off()


TablaTestScores=read.delim("tables/RegKarthikTWA.tex",sep="&",stringsAsFactors = F,header=F)
TablaTestScores=function_clean(TablaTestScores)


pdf("figures/HighStakes.pdf")
barplot2(as.matrix(TablaTestScores[c(1,3),4:5]),names.arg=c("Math","Kiswahili"),                                                 
         xlab="",ylab="Learning gains in standard deviations",plot.ci=TRUE,ylim=c(0,0.3),main="",
         ci.u=as.matrix(rbind(TablaTestScores[c(1, 3),4:5]))+qnorm(CI)*as.matrix(rbind(TablaTestScores[c(1, 3)+1,4:5])),
         ci.l=as.matrix(rbind(TablaTestScores[c(1, 3),4:5]))-qnorm(CI)*as.matrix(rbind(TablaTestScores[c(1, 3)+1,4:5])),
         beside=T,las=1,col=c("#006EB9","#81B53C"),axes=F,cex=1.2,cex.names=2,cex.lab=1.5)
axis(side=2,las=1,cex.axis=1.2,lwd=0,cex=2)       
legend("top", c("Levels","Gains"), fill=c("#006EB9","#81B53C"),ncol=2,bty="n",inset=0,cex=2)
dev.off()
## add default y-axis (ticks+labels)

TablaTestScores=read.delim("tables/RegKarthik_HHControls_EYOS.tex",sep="&",stringsAsFactors = F,header=F)
TablaTestScores=function_clean(TablaTestScores)


pdf("figures/LowStakes_EYOS.pdf")
barplot2(as.matrix(TablaTestScores[c(1,3),4:5]),names.arg=c("Math","Kiswahili"),                                                 
         xlab="",ylab="Equivalent years of schooling",ylim=c(0,0.7),plot.ci=TRUE,main="",
         ci.u=as.matrix(rbind(TablaTestScores[c(1, 3),4:5]))+qnorm(CI)*as.matrix(rbind(TablaTestScores[c(1, 3)+1,4:5])),
         ci.l=as.matrix(rbind(TablaTestScores[c(1, 3),4:5]))-qnorm(CI)*as.matrix(rbind(TablaTestScores[c(1, 3)+1,4:5])),
         beside=T,las=1,col=c("#006EB9","#81B53C"),axes=F,cex=1.2,cex.names=2,cex.lab=1.5)
axis(side=2,las=1,cex.axis=1.2,lwd=0,cex=2)       
legend("top", c("Levels","Gains"), fill=c("#006EB9","#81B53C"),ncol=2,bty="n",inset=0,cex=2)
dev.off()
