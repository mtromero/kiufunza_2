*Document folder where all KF things are saved
clear all
cd "C:/Users/Mauri/Box Sync/01_KiuFunza/"
global dirdo "C:/Users/Mauri/Documents/git/kiufunza_2/Payments"
*Install tabmiss for data checks
*findit tabmiss
scalar drop _all
scalar define TotalBudget=2*75000*2100*5/6

do "$dirdo/01_Budget_Calc.do"
do "$dirdo/02_Mash_Yr1.do"
do "$dirdo/03_Stadi_Yr1.do"
do "$dirdo/04_CheckBudget.do"
do "$dirdo/05_RandomChecks.do"

do "$dirdo/07_ComparePayments.do"

