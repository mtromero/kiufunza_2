use "$basein/5 National Exams/Grade 7, PSLE/PSLE1213", clear
keep DistID District SchoolID School tzschlcode13 tzschlname13 tzdistname13 tzregname13

gen school_codePSLE=regexr(tzschlcode13,"-","")
drop tzschlcode13
rename tzschlname13 SchoolNamePSLE
rename tzdistname13 DistrictPSLE
rename tzregname13 RegionPSLE

/*
replace school_codePSLE="PS1703021" if SchoolID==211
replace school_codePSLE="PS1706038" if SchoolID==215
replace school_codePSLE="PS0507077" if SchoolID==331
replace school_codePSLE="PS0303033" if SchoolID==511
replace school_codePSLE="PS0307049" if SchoolID==521
replace school_codePSLE="PS2003073" if SchoolID==717
replace school_codePSLE="PS1010058" if SchoolID==927
replace school_codePSLE="PS1501048" if SchoolID==1018
replace school_codePSLE="PS1503099" if SchoolID==1034
*/
keep DistID SchoolID school_codePSLE School District
replace School=strupper(School)
replace District=strupper(District)
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013_ERIN.dta", replace

keep DistID SchoolID  School District
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"  "," ")


 forvalues i=1/5{
	 foreach var of varlist School{
		replace `var'=regexr(`var',".*/","")
		replace `var'=regexr(`var',"SHULE YA MSINGI","")
		replace `var'=regexr(`var',"PRIMARY$"," ")
		replace `var'=regexr(`var',"PPIMARY$"," ")
		replace `var'=regexr(`var',"PRIIMARY$"," ")
		replace `var'=regexr(`var',"PRINARY$"," ")
		replace `var'=regexr(`var',"PRMARY$"," ")
		replace `var'=regexr(`var',"P/RIMARY$"," ")
		replace `var'=regexr(`var',"PRIMAMARY$"," ")
		replace `var'=regexr(`var',"-PRIMA$"," ")
		replace `var'=regexr(`var',"ACADPRYMARY$"," ")
		replace `var'=regexr(`var',"PR$"," ")
		replace `var'=regexr(`var',"PRIMAEY$"," ")
		replace `var'=regexr(`var',"PRIMMARY$"," ")
		replace `var'=regexr(`var',"PRIMARI$"," ")
		replace `var'=regexr(`var',"PSCHOOL$"," ")
		replace `var'=regexr(`var'," PRM$"," ")
		replace `var'=regexr(`var',"SCHOOL$"," ")
		replace `var'=regexr(`var',"SHCOOL$"," ")
		replace `var'=regexr(`var',"CHOOL$"," ")
		replace `var'=regexr(`var',"PRIMARYA$"," ")
		
		
		replace `var'=regexr(`var',"ENGLISH MEDIUM$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUM$"," ")
		replace `var'=regexr(`var',"ENGL MEDIUM$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTRE$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTER$"," ")
		replace `var'=regexr(`var',"ACADEMY$"," ")
		replace `var'=regexr(`var',"CENTRE$"," ")
		replace `var'=regexr(`var',"CENTER$"," ")
		replace `var'=regexr(`var',"SCHOO$"," ")
		replace `var'=regexr(`var',"SHOOL$"," ")
		replace `var'=regexr(`var',"SCHOLL$"," ")
		replace `var'=regexr(`var'," EMPS$"," ")
		replace `var'=regexr(`var'," SCH$"," ")
		replace `var'=regexr(`var'," PS$"," ")
		replace `var'=regexr(`var'," P/S$"," ")
		replace `var'=regexr(`var',"ENG MED$"," ")
		replace `var'=regexr(`var',"ENG MEDIUM$"," ")
		replace `var'=regexr(`var',"ENG MD$"," ")
		replace `var'=regexr(`var',"PRIMSCHOOL$"," ")
		replace `var'=regexr(`var',"PRSCHOOL$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUMSCHOOL$"," ")
		replace `var'=regexr(`var',"\(ENGLISHMEDIUM\)$"," ")
		replace `var'=regexr(`var',"P /$"," ")
		 
		replace `var'=regexr(`var',"\.","")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strupper(`var')
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var'," - ","-")
		replace `var'=regexr(`var'," -","-")
		replace `var'=regexr(`var',"- ","-")
		replace `var'=regexr(`var',"-"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"  "," ")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strtrim(`var')
		replace `var'=stritrim(`var')	
	}
}

replace District=regexr(District,"'"," ")
replace District=regexr(District,"  "," ")
replace District=strltrim(District)
replace District=strrtrim(District)
replace School=strltrim(School)
replace School=strrtrim(School)

gen region_name=""
replace region_name="GEITA" if District=="GEITA"
replace region_name="SHINYANGA" if District=="KAHAMA"
replace region_name="KAGERA" if District=="KARAGWE"
replace region_name="DAR ES SALAAM" if District=="KINONDONI"
replace region_name="DODOMA" if District=="KONDOA"
replace region_name="TANGA" if District=="KOROGWE RURAL"
replace region_name="TANGA" if District=="LUSHOTO"
replace region_name="RUVUMA" if District=="MBINGA"
replace region_name="MBEYA" if District=="MBOZI"
replace region_name="RUKWA" if District=="SUMBAWANGA RURAL"



replace District="KOROGWE" if District=="KOROGWE RURAL"
replace District="SUMBAWANGA" if District=="SUMBAWANGA RURAL"

save "$base_out/CrossWalk_PSLE/SchoolNames.dta", replace



/*
use "$basein/ResultadosSchoolTotal_PSLE_2013.dta", clear
keep school_name district_name region_name school_codePSLE
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
merge 1:m school_codePSLE using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013_ERIN.dta"

use "$basein/ResultadosSchoolTotal_PSLE_2014.dta", clear
keep school_name district_name region_name school_codePSLE
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
merge 1:m school_codePSLE using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013_ERIN.dta"

use "$basein/ResultadosSchoolTotal_PSLE_2015.dta", clear
keep school_name district_name region_name school_codePSLE
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
merge 1:m school_codePSLE using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013_ERIN.dta"

*/

****************************************
****************************************
************* 2013 ***************
********************************
****************************************
****************************************

use "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2013.dta", clear
keep school_name district_name school_codePSLE region_name
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
rename  school_name School
rename  district_name District

replace District="MSALALA" if District=="KISHAPU"
replace District="KISHAPU" if District=="KAHAMA MJI"

qui forvalues i=1/5{
	qui foreach var of varlist School{
		replace `var'=regexr(`var',"SHULE YA MSINGI","")
		replace `var'=regexr(`var',"PRIMARY$"," ")
		replace `var'=regexr(`var',"PPIMARY$"," ")
		replace `var'=regexr(`var',"PRIIMARY$"," ")
		replace `var'=regexr(`var',"PRINARY$"," ")
		replace `var'=regexr(`var',"PRMARY$"," ")
		replace `var'=regexr(`var',"P/RIMARY$"," ")
		replace `var'=regexr(`var',"PRIMAMARY$"," ")
		replace `var'=regexr(`var',"-PRIMA$"," ")
		replace `var'=regexr(`var',"ACADPRYMARY$"," ")
		replace `var'=regexr(`var',"PR$"," ")
		replace `var'=regexr(`var',"PRIMAEY$"," ")
		replace `var'=regexr(`var',"PRIMMARY$"," ")
		replace `var'=regexr(`var',"PRIMARI$"," ")
		replace `var'=regexr(`var',"PSCHOOL$"," ")
		replace `var'=regexr(`var'," PRM$"," ")
		replace `var'=regexr(`var',"SCHOOL$"," ")
		replace `var'=regexr(`var',"SHCOOL$"," ")
		replace `var'=regexr(`var',"CHOOL$"," ")
		replace `var'=regexr(`var',"PRIMARYA$"," ")
		
		
		replace `var'=regexr(`var',"ENGLISH MEDIUM$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUM$"," ")
		replace `var'=regexr(`var',"ENGL MEDIUM$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTRE$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTER$"," ")
		replace `var'=regexr(`var',"ACADEMY$"," ")
		replace `var'=regexr(`var',"CENTRE$"," ")
		replace `var'=regexr(`var',"CENTER$"," ")
		replace `var'=regexr(`var',"SCHOO$"," ")
		replace `var'=regexr(`var',"SHOOL$"," ")
		replace `var'=regexr(`var',"SCHOLL$"," ")
		replace `var'=regexr(`var'," EMPS$"," ")
		replace `var'=regexr(`var'," SCH$"," ")
		replace `var'=regexr(`var'," PS$"," ")
		replace `var'=regexr(`var'," P/S$"," ")
		replace `var'=regexr(`var',"ENG MED$"," ")
		replace `var'=regexr(`var',"ENG MEDIUM$"," ")
		replace `var'=regexr(`var',"ENG MD$"," ")
		replace `var'=regexr(`var',"PRIMSCHOOL$"," ")
		replace `var'=regexr(`var',"PRSCHOOL$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUMSCHOOL$"," ")
		replace `var'=regexr(`var',"\(ENGLISHMEDIUM\)$"," ")
		replace `var'=regexr(`var',"P /$"," ")
		 
		replace `var'=regexr(`var',"\.","")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strupper(`var')
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var'," - ","-")
		replace `var'=regexr(`var'," -","-")
		replace `var'=regexr(`var',"- ","-")
		replace `var'=regexr(`var',"-"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"  "," ")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strtrim(`var')
		replace `var'=stritrim(`var')	
	}
}
*replace School=stritrim(School)
replace School=strltrim(School)
replace School=strrtrim(School)
replace School=strupper(School)
replace District=strupper(District)
replace District=regexr(District,"\(V\)","")
replace District=regexr(District,"\(M\)","")
replace District=regexr(District,"\(DC\)","")
replace District=regexr(District,"\(TC\)","")
replace District=regexr(District,"VIJIJINI","")
replace District=regexr(District,"MJI","")
replace District=regexr(District," DC","")
replace District=regexr(District,"-DC","")
replace District=regexr(District," NI","")
replace District=regexr(District," D\.C","")
replace District=regexr(District," TC","")
replace District=regexr(District," D\.C\.","")
replace District=regexr(District,"MANISPAA","")
replace District=regexr(District,"JIJI","")

replace District=strltrim(District)
replace District=strrtrim(District)
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"  "," ")
replace District=regexr(District,"'"," ")
replace District=regexr(District,"  "," ")
replace District=strltrim(District)
replace District=strrtrim(District)
replace School=strltrim(School)
replace School=strrtrim(School)
replace school_codePSLE=regexr(school_codePSLE,"PS","") 
destring school_codePSLE, replace



preserve
merge m:1 School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", keepus(SchoolID)
keep if _merge==3
drop _merge
drop if SchoolID==616 & school_codePSLE!=2002061
drop if SchoolID==407 & school_codePSLE!=0204074
gen LevelCertanty=1
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta", replace
restore

preserve
reclink School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(District) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=2
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=3
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta")
drop if _merge!=3
keep if District=="GEITA" | District=="KAHAMA" | District=="KARAGWE" | District=="KINONDONI" | District=="KONDOA" | ///
		District=="KOROGWE" | District=="LUSHOTO" | District=="MBINGA" | District=="MBOZI" | District=="SUMBAWANGA"
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=4
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta", replace
restore

use "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta", clear
rename school_codePSLE school_codePSLE2013 
tostring school_codePSLE2013, replace
replace school_codePSLE2013="PS"+school_codePSLE2013
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013_ERIN.dta"
replace school_codePSLE2013=regexr(school_codePSLE2013,"PS","")
destring school_codePSLE2013, replace
gen school_codePSLE2013_2= string(school_codePSLE2013,"%07.0f")
replace school_codePSLE2013_2="PS"+school_codePSLE2013_2 if school_codePSLE2013!=.
drop school_codePSLE2013
rename school_codePSLE2013_2 school_codePSLE2013

replace school_codePSLE=regexr(school_codePSLE,"PS","")
destring school_codePSLE, replace
gen school_codePSLE_2= string(school_codePSLE,"%07.0f")
replace school_codePSLE_2="PS"+school_codePSLE_2 if school_codePSLE!=.
drop school_codePSLE
rename school_codePSLE_2 school_codePSLE
replace school_codePSLE="" if school_codePSLE=="."
replace school_codePSLE2013="" if school_codePSLE2013=="."

bro if school_codePSLE2013!= school_codePSLE & school_codePSLE2013!="" & school_codePSLE!=""

gen school_codePSL_Final=school_codePSLE2013 if school_codePSLE2013!=""
replace  school_codePSL_Final=school_codePSLE if school_codePSLE!="" & school_codePSL_Final==""




replace school_codePSL_Final="PS0303033" if SchoolID==511
replace school_codePSL_Final="PS0307049" if SchoolID==521
replace school_codePSL_Final="PS2003073" if SchoolID==717
replace school_codePSL_Final="PS1501048" if SchoolID==1018
replace school_codePSL_Final="PS1503099" if SchoolID==1034
replace school_codePSL_Final="PS1010082" if SchoolID==923
drop _merge
drop if school_codePSL_Final==""
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta", replace



****************************************
****************************************
************* 2014 ***************
********************************
****************************************
****************************************

use "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2014.dta", clear
keep school_name district_name school_codePSLE region_name
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
rename  school_name School
rename  district_name District

replace District="KAHAMA" if District=="KAHAMA MJI"




qui forvalues i=1/5{
	qui foreach var of varlist School{
		replace `var'=regexr(`var',"SHULE YA MSINGI","")
		replace `var'=regexr(`var',"PRIMARY$"," ")
		replace `var'=regexr(`var',"PPIMARY$"," ")
		replace `var'=regexr(`var',"PRIIMARY$"," ")
		replace `var'=regexr(`var',"PRINARY$"," ")
		replace `var'=regexr(`var',"PRMARY$"," ")
		replace `var'=regexr(`var',"P/RIMARY$"," ")
		replace `var'=regexr(`var',"PRIMAMARY$"," ")
		replace `var'=regexr(`var',"-PRIMA$"," ")
		replace `var'=regexr(`var',"ACADPRYMARY$"," ")
		replace `var'=regexr(`var',"PR$"," ")
		replace `var'=regexr(`var',"PRIMAEY$"," ")
		replace `var'=regexr(`var',"PRIMMARY$"," ")
		replace `var'=regexr(`var',"PRIMARI$"," ")
		replace `var'=regexr(`var',"PSCHOOL$"," ")
		replace `var'=regexr(`var'," PRM$"," ")
		replace `var'=regexr(`var',"SCHOOL$"," ")
		replace `var'=regexr(`var',"SHCOOL$"," ")
		replace `var'=regexr(`var',"CHOOL$"," ")
		replace `var'=regexr(`var',"PRIMARYA$"," ")
		
		
		replace `var'=regexr(`var',"ENGLISH MEDIUM$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUM$"," ")
		replace `var'=regexr(`var',"ENGL MEDIUM$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTRE$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTER$"," ")
		replace `var'=regexr(`var',"ACADEMY$"," ")
		replace `var'=regexr(`var',"CENTRE$"," ")
		replace `var'=regexr(`var',"CENTER$"," ")
		replace `var'=regexr(`var',"SCHOO$"," ")
		replace `var'=regexr(`var',"SHOOL$"," ")
		replace `var'=regexr(`var',"SCHOLL$"," ")
		replace `var'=regexr(`var'," EMPS$"," ")
		replace `var'=regexr(`var'," SCH$"," ")
		replace `var'=regexr(`var'," PS$"," ")
		replace `var'=regexr(`var'," P/S$"," ")
		replace `var'=regexr(`var',"ENG MED$"," ")
		replace `var'=regexr(`var',"ENG MEDIUM$"," ")
		replace `var'=regexr(`var',"ENG MD$"," ")
		replace `var'=regexr(`var',"PRIMSCHOOL$"," ")
		replace `var'=regexr(`var',"PRSCHOOL$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUMSCHOOL$"," ")
		replace `var'=regexr(`var',"\(ENGLISHMEDIUM\)$"," ")
		replace `var'=regexr(`var',"P /$"," ")
		 
		replace `var'=regexr(`var',"\.","")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strupper(`var')
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var'," - ","-")
		replace `var'=regexr(`var'," -","-")
		replace `var'=regexr(`var',"- ","-")
		replace `var'=regexr(`var',"-"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"  "," ")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strtrim(`var')
		replace `var'=stritrim(`var')	
	}
}
*replace School=stritrim(School)
replace School=strltrim(School)
replace School=strrtrim(School)
replace School=strupper(School)
replace District=strupper(District)
replace District=regexr(District,"\(V\)","")
replace District=regexr(District,"\(M\)","")
replace District=regexr(District,"\(DC\)","")
replace District=regexr(District,"\(TC\)","")
replace District=regexr(District,"VIJIJINI","")
replace District=regexr(District,"MJI","")
replace District=regexr(District," DC","")
replace District=regexr(District,"-DC","")
replace District=regexr(District," NI","")
replace District=regexr(District," D\.C","")
replace District=regexr(District," TC","")
replace District=regexr(District," D\.C\.","")
replace District=regexr(District,"MANISPAA","")
replace District=regexr(District,"JIJI","")

replace District=strltrim(District)
replace District=strrtrim(District)
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"  "," ")
replace District=regexr(District,"'"," ")
replace District=regexr(District,"  "," ")
replace District=strltrim(District)
replace District=strrtrim(District)
replace School=strltrim(School)
replace School=strrtrim(School)
replace school_codePSLE=regexr(school_codePSLE,"PS","") 
destring school_codePSLE, replace

preserve
merge m:1 School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", keepus(SchoolID)
keep if _merge==3
drop _merge
drop if SchoolID==616 & school_codePSLE!=2002061
drop if SchoolID==407 & school_codePSLE!=0204074
gen LevelCertanty=1
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta", replace
restore

preserve
reclink School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(District) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=2
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=3
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta")
drop if _merge!=3
keep if District=="GEITA" | District=="KAHAMA" | District=="KARAGWE" | District=="KINONDONI" | District=="KONDOA" | ///
		District=="KOROGWE" | District=="LUSHOTO" | District=="MBINGA" | District=="MBOZI" | District=="SUMBAWANGA"
		
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=4
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta", replace
restore

use "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta", clear
rename school_codePSLE school_codePSLE2014 
tostring school_codePSLE2014, replace
replace school_codePSLE2014="PS"+school_codePSLE2014
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013_ERIN.dta"
replace school_codePSLE2014=regexr(school_codePSLE2014,"PS","")
destring school_codePSLE2014, replace
gen school_codePSLE2014_2= string(school_codePSLE2014,"%07.0f")
replace school_codePSLE2014_2="PS"+school_codePSLE2014_2 if school_codePSLE2014!=.
drop school_codePSLE2014
rename school_codePSLE2014_2 school_codePSLE2014

replace school_codePSLE=regexr(school_codePSLE,"PS","")
destring school_codePSLE, replace
gen school_codePSLE_2= string(school_codePSLE,"%07.0f")
replace school_codePSLE_2="PS"+school_codePSLE_2 if school_codePSLE!=.
drop school_codePSLE
rename school_codePSLE_2 school_codePSLE
replace school_codePSLE="" if school_codePSLE=="."
replace school_codePSLE2014="" if school_codePSLE2014=="."

bro if school_codePSLE2014!= school_codePSLE & school_codePSLE2014!="" & school_codePSLE!=""

gen school_codePSL_Final=school_codePSLE2014 if school_codePSLE2014!=""
replace  school_codePSL_Final=school_codePSLE if school_codePSLE!="" & school_codePSL_Final==""

replace school_codePSL_Final="PS0303033" if SchoolID==511
replace school_codePSL_Final="PS1501048" if SchoolID==1018
replace school_codePSL_Final="PS1503099" if SchoolID==1034
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta", update keepus( school_codePSL_Final)
drop _merge
drop if school_codePSL_Final==""
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta", replace

****************************************
****************************************
************** 2015 ********************
****************************************
****************************************

use "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2015.dta", clear
keep school_name district_name school_codePSLE region_name
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
rename  school_name School
rename  district_name District

replace District="KAHAMA" if District=="KAHAMA MJI"



qui forvalues i=1/5{
	qui foreach var of varlist School{
		replace `var'=regexr(`var',"SHULE YA MSINGI","")
		replace `var'=regexr(`var',"PRIMARY$"," ")
		replace `var'=regexr(`var',"PPIMARY$"," ")
		replace `var'=regexr(`var',"PRIIMARY$"," ")
		replace `var'=regexr(`var',"PRINARY$"," ")
		replace `var'=regexr(`var',"PRMARY$"," ")
		replace `var'=regexr(`var',"P/RIMARY$"," ")
		replace `var'=regexr(`var',"PRIMAMARY$"," ")
		replace `var'=regexr(`var',"-PRIMA$"," ")
		replace `var'=regexr(`var',"ACADPRYMARY$"," ")
		replace `var'=regexr(`var',"PR$"," ")
		replace `var'=regexr(`var',"PRIMAEY$"," ")
		replace `var'=regexr(`var',"PRIMMARY$"," ")
		replace `var'=regexr(`var',"PRIMARI$"," ")
		replace `var'=regexr(`var',"PSCHOOL$"," ")
		replace `var'=regexr(`var'," PRM$"," ")
		replace `var'=regexr(`var',"SCHOOL$"," ")
		replace `var'=regexr(`var',"SHCOOL$"," ")
		replace `var'=regexr(`var',"CHOOL$"," ")
		replace `var'=regexr(`var',"PRIMARYA$"," ")
		
		
		replace `var'=regexr(`var',"ENGLISH MEDIUM$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUM$"," ")
		replace `var'=regexr(`var',"ENGL MEDIUM$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTRE$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTER$"," ")
		replace `var'=regexr(`var',"ACADEMY$"," ")
		replace `var'=regexr(`var',"CENTRE$"," ")
		replace `var'=regexr(`var',"CENTER$"," ")
		replace `var'=regexr(`var',"SCHOO$"," ")
		replace `var'=regexr(`var',"SHOOL$"," ")
		replace `var'=regexr(`var',"SCHOLL$"," ")
		replace `var'=regexr(`var'," EMPS$"," ")
		replace `var'=regexr(`var'," SCH$"," ")
		replace `var'=regexr(`var'," PS$"," ")
		replace `var'=regexr(`var'," P/S$"," ")
		replace `var'=regexr(`var',"ENG MED$"," ")
		replace `var'=regexr(`var',"ENG MEDIUM$"," ")
		replace `var'=regexr(`var',"ENG MD$"," ")
		replace `var'=regexr(`var',"PRIMSCHOOL$"," ")
		replace `var'=regexr(`var',"PRSCHOOL$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUMSCHOOL$"," ")
		replace `var'=regexr(`var',"\(ENGLISHMEDIUM\)$"," ")
		replace `var'=regexr(`var',"P /$"," ")
		 
		replace `var'=regexr(`var',"\.","")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strupper(`var')
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var'," - ","-")
		replace `var'=regexr(`var'," -","-")
		replace `var'=regexr(`var',"- ","-")
		replace `var'=regexr(`var',"-"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"  "," ")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strtrim(`var')
		replace `var'=stritrim(`var')	
	}
}
*replace School=stritrim(School)
replace School=strltrim(School)
replace School=strrtrim(School)
replace School=strupper(School)
replace District=strupper(District)
replace District=regexr(District,"\(V\)","")
replace District=regexr(District,"\(M\)","")
replace District=regexr(District,"\(DC\)","")
replace District=regexr(District,"\(TC\)","")
replace District=regexr(District,"VIJIJINI","")
replace District=regexr(District,"MJI","")
replace District=regexr(District," DC","")
replace District=regexr(District,"-DC","")
replace District=regexr(District," NI","")
replace District=regexr(District," D\.C","")
replace District=regexr(District," TC","")
replace District=regexr(District," D\.C\.","")
replace District=regexr(District,"MANISPAA","")
replace District=regexr(District,"JIJI","")

replace District=strltrim(District)
replace District=strrtrim(District)
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"  "," ")
replace District=regexr(District,"'"," ")
replace District=regexr(District,"  "," ")
replace District=strltrim(District)
replace District=strrtrim(District)
replace School=strltrim(School)
replace School=strrtrim(School)
replace school_codePSLE=regexr(school_codePSLE,"PS","") 
destring school_codePSLE, replace

preserve
merge m:1 School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", keepus(SchoolID)
keep if _merge==3
drop _merge
drop if SchoolID==616 & school_codePSLE!=2002061
drop if SchoolID==407 & school_codePSLE!=0204074
gen LevelCertanty=1
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta", replace
restore

preserve
reclink School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(District) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=2
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=3
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta")
drop if _merge!=3
keep if District=="GEITA" | District=="KAHAMA" | District=="KARAGWE" | District=="KINONDONI" | District=="KONDOA" | ///
		District=="KOROGWE" | District=="LUSHOTO" | District=="MBINGA" | District=="MBOZI" | District=="SUMBAWANGA"
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=4
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta", replace
restore

use "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta", clear
rename school_codePSLE school_codePSLE2015 
tostring school_codePSLE2015, replace
replace school_codePSLE2015="PS"+school_codePSLE2015
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013_ERIN.dta"
replace school_codePSLE2015=regexr(school_codePSLE2015,"PS","")
destring school_codePSLE2015, replace
gen school_codePSLE2015_2= string(school_codePSLE2015,"%07.0f")
replace school_codePSLE2015_2="PS"+school_codePSLE2015_2 if school_codePSLE2015!=.
drop school_codePSLE2015
rename school_codePSLE2015_2 school_codePSLE2015

replace school_codePSLE=regexr(school_codePSLE,"PS","")
destring school_codePSLE, replace
gen school_codePSLE_2= string(school_codePSLE,"%07.0f")
replace school_codePSLE_2="PS"+school_codePSLE_2 if school_codePSLE!=.
drop school_codePSLE
rename school_codePSLE_2 school_codePSLE
replace school_codePSLE="" if school_codePSLE=="."
replace school_codePSLE2015="" if school_codePSLE2015=="."

bro if school_codePSLE2015!= school_codePSLE & school_codePSLE2015!="" & school_codePSLE!=""



gen school_codePSL_Final=school_codePSLE2015 if school_codePSLE2015!=""
replace  school_codePSL_Final=school_codePSLE if school_codePSLE!="" & school_codePSL_Final==""

replace school_codePSL_Final="PS0303033" if SchoolID==511 
replace school_codePSL_Final="PS1501048" if SchoolID==1018
replace school_codePSL_Final="PS1010082" if SchoolID==923



save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta", replace
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta", update keepus( school_codePSL_Final)
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta", update keepus( school_codePSL_Final)
drop _merge
drop if school_codePSL_Final==""
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta", replace



****************************************
****************************************
************** 2016 ********************
****************************************
****************************************

use "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2016.dta", clear
keep school_name district_name school_codePSLE region_name
rename  school_name School
rename  district_name District


replace District="KAHAMA" if District=="KAHAMA MJI"
replace District="MBINGA" if District=="MBINGA(V)"
replace District="MPANDA MJI" if District=="MPANDA MANISPAA"





qui forvalues i=1/5{
	qui foreach var of varlist School{
		replace `var'=regexr(`var',"SHULE YA MSINGI","")
		replace `var'=regexr(`var',"PRIMARY$"," ")
		replace `var'=regexr(`var',"PPIMARY$"," ")
		replace `var'=regexr(`var',"PRIIMARY$"," ")
		replace `var'=regexr(`var',"PRINARY$"," ")
		replace `var'=regexr(`var',"PRMARY$"," ")
		replace `var'=regexr(`var',"P/RIMARY$"," ")
		replace `var'=regexr(`var',"PRIMAMARY$"," ")
		replace `var'=regexr(`var',"-PRIMA$"," ")
		replace `var'=regexr(`var',"ACADPRYMARY$"," ")
		replace `var'=regexr(`var',"PR$"," ")
		replace `var'=regexr(`var',"PRIMAEY$"," ")
		replace `var'=regexr(`var',"PRIMMARY$"," ")
		replace `var'=regexr(`var',"PRIMARI$"," ")
		replace `var'=regexr(`var',"PSCHOOL$"," ")
		replace `var'=regexr(`var'," PRM$"," ")
		replace `var'=regexr(`var',"SCHOOL$"," ")
		replace `var'=regexr(`var',"SHCOOL$"," ")
		replace `var'=regexr(`var',"CHOOL$"," ")
		replace `var'=regexr(`var',"PRIMARYA$"," ")
		
		
		replace `var'=regexr(`var',"ENGLISH MEDIUM$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUM$"," ")
		replace `var'=regexr(`var',"ENGL MEDIUM$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTRE$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTER$"," ")
		replace `var'=regexr(`var',"ACADEMY$"," ")
		replace `var'=regexr(`var',"CENTRE$"," ")
		replace `var'=regexr(`var',"CENTER$"," ")
		replace `var'=regexr(`var',"SCHOO$"," ")
		replace `var'=regexr(`var',"SHOOL$"," ")
		replace `var'=regexr(`var',"SCHOLL$"," ")
		replace `var'=regexr(`var'," EMPS$"," ")
		replace `var'=regexr(`var'," SCH$"," ")
		replace `var'=regexr(`var'," PS$"," ")
		replace `var'=regexr(`var'," P/S$"," ")
		replace `var'=regexr(`var',"ENG MED$"," ")
		replace `var'=regexr(`var',"ENG MEDIUM$"," ")
		replace `var'=regexr(`var',"ENG MD$"," ")
		replace `var'=regexr(`var',"PRIMSCHOOL$"," ")
		replace `var'=regexr(`var',"PRSCHOOL$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUMSCHOOL$"," ")
		replace `var'=regexr(`var',"\(ENGLISHMEDIUM\)$"," ")
		replace `var'=regexr(`var',"P /$"," ")
		 
		replace `var'=regexr(`var',"\.","")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strupper(`var')
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var'," - ","-")
		replace `var'=regexr(`var'," -","-")
		replace `var'=regexr(`var',"- ","-")
		replace `var'=regexr(`var',"-"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"  "," ")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strtrim(`var')
		replace `var'=stritrim(`var')	
	}
}
*replace School=stritrim(School)
replace School=strltrim(School)
replace School=strrtrim(School)
replace School=strupper(School)
replace District=strupper(District)
replace District=regexr(District,"\(V\)","")
replace District=regexr(District,"\(M\)","")
replace District=regexr(District,"\(DC\)","")
replace District=regexr(District,"\(TC\)","")
replace District=regexr(District,"VIJIJINI","")
replace District=regexr(District,"MJI","")
replace District=regexr(District," DC","")
replace District=regexr(District,"-DC","")
replace District=regexr(District," NI","")
replace District=regexr(District," D\.C","")
replace District=regexr(District," TC","")
replace District=regexr(District," D\.C\.","")
replace District=regexr(District,"MANISPAA","")
replace District=regexr(District,"JIJI","")

replace District=strltrim(District)
replace District=strrtrim(District)
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"  "," ")
replace District=regexr(District,"'"," ")
replace District=regexr(District,"  "," ")
replace District=strltrim(District)
replace District=strrtrim(District)
replace School=strltrim(School)
replace School=strrtrim(School)
replace school_codePSLE=regexr(school_codePSLE,"PS","") 
destring school_codePSLE, replace

preserve
merge m:1 School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", keepus(SchoolID)
keep if _merge==3
drop _merge
drop if SchoolID==616 & school_codePSLE!=2002061
drop if SchoolID==407 & school_codePSLE!=0204074
gen LevelCertanty=1
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2016.dta", replace
restore

preserve
reclink School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(District) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2016.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=2
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2016.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2016.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2016.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=3
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2016.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2016.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2016.dta")
drop if _merge!=3
keep if District=="GEITA" | District=="KAHAMA" | District=="KARAGWE" | District=="KINONDONI" | District=="KONDOA" | ///
		District=="KOROGWE" | District=="LUSHOTO" | District=="MBINGA" | District=="MBOZI" | District=="SUMBAWANGA"
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=4
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2016.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2016.dta", replace
restore

use "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2016.dta", clear
rename school_codePSLE school_codePSLE2016 
tostring school_codePSLE2016, replace
replace school_codePSLE2016="PS"+school_codePSLE2016
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013_ERIN.dta"
replace school_codePSLE2016=regexr(school_codePSLE2016,"PS","")
destring school_codePSLE2016, replace
gen school_codePSLE2016_2= string(school_codePSLE2016,"%07.0f")
replace school_codePSLE2016_2="PS"+school_codePSLE2016_2 if school_codePSLE2016!=.
drop school_codePSLE2016
rename school_codePSLE2016_2 school_codePSLE2016

replace school_codePSLE=regexr(school_codePSLE,"PS","")
destring school_codePSLE, replace
gen school_codePSLE_2= string(school_codePSLE,"%07.0f")
replace school_codePSLE_2="PS"+school_codePSLE_2 if school_codePSLE!=.
drop school_codePSLE
rename school_codePSLE_2 school_codePSLE
replace school_codePSLE="" if school_codePSLE=="."
replace school_codePSLE2016="" if school_codePSLE2016=="."

bro if school_codePSLE2016!= school_codePSLE & school_codePSLE2016!="" & school_codePSLE!=""



gen school_codePSL_Final=school_codePSLE2016 if school_codePSLE2016!=""
replace  school_codePSL_Final=school_codePSLE if school_codePSLE!="" & school_codePSL_Final==""

replace school_codePSL_Final="PS0303033" if SchoolID==511 & school_codePSL_Final==""
replace school_codePSL_Final="PS1501048" if SchoolID==1018 & school_codePSL_Final==""

drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta", update keepus( school_codePSL_Final)
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta", update keepus( school_codePSL_Final)
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta", update keepus( school_codePSL_Final)
drop _merge
drop if school_codePSL_Final==""
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2016.dta", replace




****************************************
****************************************
************** 2017 ********************
****************************************
****************************************

use "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2017.dta", clear
keep school_name district_name school_codePSLE region_name
rename  school_name School
rename  district_name District


replace District="KINONDONI(V)" if District=="KINONDONI"
replace District="KINONDONI(M)" if District=="UBUNGO"

replace District="TEMEKE(V)" if District=="KIGAMBONI"
replace District="TEMEKE(M)" if District=="TEMEKE"

replace District="TARIME" if District=="TARIME(V)"
replace District="MBINGA" if District=="MBINGA (DC)"

replace District="KAHAMA" if District=="KAHAMA MJI"
replace District="MPANDA MJI" if District=="MPANDA MANISPAA"


qui forvalues i=1/5{
	qui foreach var of varlist School{
		replace `var'=regexr(`var',"SHULE YA MSINGI","")
		replace `var'=regexr(`var',"PRIMARY$"," ")
		replace `var'=regexr(`var',"PPIMARY$"," ")
		replace `var'=regexr(`var',"PRIIMARY$"," ")
		replace `var'=regexr(`var',"PRINARY$"," ")
		replace `var'=regexr(`var',"PRMARY$"," ")
		replace `var'=regexr(`var',"P/RIMARY$"," ")
		replace `var'=regexr(`var',"PRIMAMARY$"," ")
		replace `var'=regexr(`var',"-PRIMA$"," ")
		replace `var'=regexr(`var',"ACADPRYMARY$"," ")
		replace `var'=regexr(`var',"PR$"," ")
		replace `var'=regexr(`var',"PRIMAEY$"," ")
		replace `var'=regexr(`var',"PRIMMARY$"," ")
		replace `var'=regexr(`var',"PRIMARI$"," ")
		replace `var'=regexr(`var',"PSCHOOL$"," ")
		replace `var'=regexr(`var'," PRM$"," ")
		replace `var'=regexr(`var',"SCHOOL$"," ")
		replace `var'=regexr(`var',"SHCOOL$"," ")
		replace `var'=regexr(`var',"CHOOL$"," ")
		replace `var'=regexr(`var',"PRIMARYA$"," ")
		
		
		replace `var'=regexr(`var',"ENGLISH MEDIUM$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUM$"," ")
		replace `var'=regexr(`var',"ENGL MEDIUM$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTRE$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTER$"," ")
		replace `var'=regexr(`var',"ACADEMY$"," ")
		replace `var'=regexr(`var',"CENTRE$"," ")
		replace `var'=regexr(`var',"CENTER$"," ")
		replace `var'=regexr(`var',"SCHOO$"," ")
		replace `var'=regexr(`var',"SHOOL$"," ")
		replace `var'=regexr(`var',"SCHOLL$"," ")
		replace `var'=regexr(`var'," EMPS$"," ")
		replace `var'=regexr(`var'," SCH$"," ")
		replace `var'=regexr(`var'," PS$"," ")
		replace `var'=regexr(`var'," P/S$"," ")
		replace `var'=regexr(`var',"ENG MED$"," ")
		replace `var'=regexr(`var',"ENG MEDIUM$"," ")
		replace `var'=regexr(`var',"ENG MD$"," ")
		replace `var'=regexr(`var',"PRIMSCHOOL$"," ")
		replace `var'=regexr(`var',"PRSCHOOL$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUMSCHOOL$"," ")
		replace `var'=regexr(`var',"\(ENGLISHMEDIUM\)$"," ")
		replace `var'=regexr(`var',"P /$"," ")
		 
		replace `var'=regexr(`var',"\.","")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strupper(`var')
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var'," - ","-")
		replace `var'=regexr(`var'," -","-")
		replace `var'=regexr(`var',"- ","-")
		replace `var'=regexr(`var',"-"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"  "," ")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strtrim(`var')
		replace `var'=stritrim(`var')	
	}
}
*replace School=stritrim(School)
replace School=strltrim(School)
replace School=strrtrim(School)
replace School=strupper(School)
replace District=strupper(District)
replace District=regexr(District,"\(V\)","")
replace District=regexr(District,"\(M\)","")
replace District=regexr(District,"\(DC\)","")
replace District=regexr(District,"\(TC\)","")
replace District=regexr(District,"VIJIJINI","")
replace District=regexr(District,"MJI","")
replace District=regexr(District," DC","")
replace District=regexr(District,"-DC","")
replace District=regexr(District," NI","")
replace District=regexr(District," D\.C","")
replace District=regexr(District," TC","")
replace District=regexr(District," D\.C\.","")
replace District=regexr(District,"MANISPAA","")
replace District=regexr(District,"JIJI","")


replace District=strltrim(District)
replace District=strrtrim(District)
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"  "," ")
replace District=regexr(District,"'"," ")
replace District=regexr(District,"  "," ")
replace District=strltrim(District)
replace District=strrtrim(District)
replace School=strltrim(School)
replace School=strrtrim(School)
replace school_codePSLE=regexr(school_codePSLE,"PS","") 
destring school_codePSLE, replace

preserve
merge m:1 School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", keepus(SchoolID)
keep if _merge==3
drop _merge
drop if SchoolID==616 & school_codePSLE!=2002061
drop if SchoolID==407 & school_codePSLE!=0204074
gen LevelCertanty=1
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2017.dta", replace
restore

preserve
reclink School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(District) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2017.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=2
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2017.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2017.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2017.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=3
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2017.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2017.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codePSLE) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2017.dta")
drop if _merge!=3
keep if District=="GEITA" | District=="KAHAMA" | District=="KARAGWE" | District=="KINONDONI" | District=="KONDOA" | ///
		District=="KOROGWE" | District=="LUSHOTO" | District=="MBINGA" | District=="MBOZI" | District=="SUMBAWANGA"
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=4
append using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2017.dta"
keep School District school_codePSLE SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2017.dta", replace
restore

use "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2017.dta", clear
rename school_codePSLE school_codePSLE2017 
tostring school_codePSLE2017, replace
replace school_codePSLE2017="PS"+school_codePSLE2017
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013_ERIN.dta"
replace school_codePSLE2017=regexr(school_codePSLE2017,"PS","")
destring school_codePSLE2017, replace
gen school_codePSLE2017_2= string(school_codePSLE2017,"%07.0f")
replace school_codePSLE2017_2="PS"+school_codePSLE2017_2 if school_codePSLE2017!=.
drop school_codePSLE2017
rename school_codePSLE2017_2 school_codePSLE2017

replace school_codePSLE=regexr(school_codePSLE,"PS","")
destring school_codePSLE, replace
gen school_codePSLE_2= string(school_codePSLE,"%07.0f")
replace school_codePSLE_2="PS"+school_codePSLE_2 if school_codePSLE!=.
drop school_codePSLE
rename school_codePSLE_2 school_codePSLE
replace school_codePSLE="" if school_codePSLE=="."
replace school_codePSLE2017="" if school_codePSLE2017=="."

bro if school_codePSLE2017!= school_codePSLE & school_codePSLE2017!="" & school_codePSLE!=""



gen school_codePSL_Final=school_codePSLE2017 if school_codePSLE2017!=""
replace  school_codePSL_Final=school_codePSLE if school_codePSLE!="" & school_codePSL_Final==""

replace school_codePSL_Final="PS0303033" if SchoolID==511
replace school_codePSL_Final="PS1501048" if SchoolID==1018
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2016.dta", update keepus( school_codePSL_Final)
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2015.dta", update keepus( school_codePSL_Final)
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta", update keepus( school_codePSL_Final)
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta", update keepus( school_codePSL_Final)
drop _merge
drop if school_codePSL_Final==""

replace school_codePSL_Final="PS0204023" if SchoolID==415
replace school_codePSL_Final="PS0204030" if SchoolID==421




save "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2017.dta", replace
exit
