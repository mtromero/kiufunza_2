use "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_StadiandControl_CLEAN.dta", clear
append using "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_Mashindano_CLEAN.dta"
drop  gender set start startampm volname end endampm
replace stream = lower(stream)
rename schoolid SchoolID
rename studentid studentID
merge m:1 SchoolID using "RawData/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2)
drop if _merge==2
drop _merge
merge m:1 SchoolID using "CommScripts/Kigoma Sample/KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update
drop if _merge==2
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440

*136 is a split school
replace treatarm2=1 if SchoolID==136
replace treatment2="Levels" if SchoolID==136
replace SchoolID=1136 if SchoolID==1104 & stream=="b"
replace treatarm2=1 if SchoolID==1136
replace treatment2="Levels" if SchoolID==1136

ds, has(type string)
foreach var of varlist `r(varlist)'{
replace `var'="" if `var'=="--blank--" | `var'=="X"
}



destring , replace
drop if grade!=1


foreach var of varlist SchoolID grade stream{
	qui tabmiss `var'
	display "`r(sum)' missing in variable `var'"
	display "They are dropped"
	drop if	missing(`var')
}







*Some missings are "weird" but I'm gonna assume students just didin't answer the question, i.e. it was wrong... but I do some more cleaning below
drop grd2* grd3*
foreach var of varlist grd*{
replace `var'=0 if `var'==.
}

capture drop grd1_kis_b* grd1_kis_c* grd1_his_b* grd1_his_c*



*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in si ma se{
	 forvalues i=3/6{
		replace grd1_kis_a_`skill'_`i'=0 if grd1_kis_a_`skill'_1==0 & grd1_kis_a_`skill'_2==0
	}
}
/*
foreach skill in l w se{
	forvalues i=3/6{
		replace grd1_eng_a_`skill'_`i'=0 if grd1_eng_a_`skill'_1==0 & grd1_eng_a_`skill'_2==0
	}
}
*/
foreach skill in id uta bwa j t{
	replace grd1_his_a_`skill'_3=0 if grd1_his_a_`skill'_1==0 & grd1_his_a_`skill'_2==0
} 

*Calculcate number of questions per skill
egen Kis_Silabi=rowtotal(grd1_kis_a_si_1 grd1_kis_a_si_2 grd1_kis_a_si_3 grd1_kis_a_si_4 grd1_kis_a_si_5 grd1_kis_a_si_6)
egen Kis_Maneno=rowtotal(grd1_kis_a_ma_1 grd1_kis_a_ma_2 grd1_kis_a_ma_3 grd1_kis_a_ma_4 grd1_kis_a_ma_5 grd1_kis_a_ma_6)
egen Kis_Sentenci=rowtotal(grd1_kis_a_se_1 grd1_kis_a_se_2 grd1_kis_a_se_3 grd1_kis_a_se_4 grd1_kis_a_se_5 grd1_kis_a_se_6)
/*
egen Eng_Letter=rowtotal(grd1_eng_a_l_1 grd1_eng_a_l_2 grd1_eng_a_l_3 grd1_eng_a_l_4 grd1_eng_a_l_5 grd1_eng_a_l_6)
egen Eng_Word=rowtotal(grd1_eng_a_w_1 grd1_eng_a_w_2 grd1_eng_a_w_3 grd1_eng_a_w_4 grd1_eng_a_w_5 grd1_eng_a_w_6)
egen Eng_Sentences=rowtotal(grd1_eng_a_se_1 grd1_eng_a_se_2 grd1_eng_a_se_3 grd1_eng_a_se_4 grd1_eng_a_se_5 grd1_eng_a_se_6)
*/
egen Math_id=rowtotal(grd1_his_a_id_1 grd1_his_a_id_2 grd1_his_a_id_3)
egen Math_uta=rowtotal(grd1_his_a_uta_1 grd1_his_a_uta_2 grd1_his_a_uta_3)
egen Math_bwa=rowtotal(grd1_his_a_bwa_1 grd1_his_a_bwa_2 grd1_his_a_bwa_3)
egen Math_j=rowtotal(grd1_his_a_j_1 grd1_his_a_j_2 grd1_his_a_j_3)
egen Math_t=rowtotal(grd1_his_a_t_1 grd1_his_a_t_2 grd1_his_a_t_3)

*generate passing dummies. Have to be careful since stata thinks missing is infinity
foreach var of varlist Kis_Silabi Kis_Maneno   {
	gen `var'_Pass=`var'>=4 & !missing(`var')
}

foreach var of varlist grd1_kis_a_se_*   {
	gen `var'_Pass=(`var'>=2) & !missing(`var')
}
foreach var of varlist Kis_Sentenci    {
	gen `var'_Pass=((grd1_kis_a_se_1_Pass+ grd1_kis_a_se_2_Pass+ grd1_kis_a_se_3_Pass +grd1_kis_a_se_4_Pass+ grd1_kis_a_se_5_Pass+ grd1_kis_a_se_6_Pass)>=4)
}
drop grd1_kis_a_se_1_Pass-grd1_kis_a_se_6_Pass


foreach var of varlist Math_id Math_uta Math_bwa Math_j Math_t {
	gen `var'_Pass=`var'>=2 & !missing(`var')
}

 
keep name- studentID  *Pass treatment2 treatarm2
saveold "CreatedData/4 Intervention/TwaEL_2016/All_Grd1Pass_Students.dta",replace version(12)



use "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_StadiandControl_CLEAN.dta", clear
append using "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_Mashindano_CLEAN.dta"
drop  gender set start startampm volname end endampm
replace stream = lower(stream)
rename schoolid SchoolID
rename studentid studentID
gen studentid=studentID
merge m:1 SchoolID using "RawData/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2)
drop if _merge==2
drop _merge
merge m:1 SchoolID using "CommScripts/Kigoma Sample/KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update
drop if _merge==2
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440
*136 is a split school
replace treatarm2=1 if SchoolID==136
replace treatment2="Levels" if SchoolID==136
replace SchoolID=1136 if SchoolID==1104 & stream=="b"
replace treatarm2=1 if SchoolID==1136
replace treatment2="Levels" if SchoolID==1136

ds, has(type string)


foreach var of varlist `r(varlist)'{
replace `var'="" if `var'=="--blank--" | `var'=="X"
}

destring , replace
drop if grade!=2

foreach var of varlist SchoolID grade stream{
	qui tabmiss `var'
	display "`r(sum)' missing in variable `var'"
	display "They are dropped"
	drop if	missing(`var')
}






drop grd1* grd3*
foreach var of varlist grd*{
	replace `var'=0 if `var'==.
}

capture drop grd2_kis_b* grd2_kis_c* grd2_his_b* grd2_his_c*

*capture rename grd1_eng_a_w_5 grd2_eng_a_se_5

replace grd2_kis_a_h=0 if grd2_kis_a_h==.

*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd2_kis_a_m_1=0 if grd2_kis_a_h==0
replace grd2_kis_a_m_2=0 if grd2_kis_a_h==0

*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in ma se{
	 forvalues i=3/6{
		replace grd2_kis_a_`skill'_`i'=0 if grd2_kis_a_`skill'_1==0 & grd2_kis_a_`skill'_2==0
	}
}
/*
foreach skill in w se{
	forvalues i=3/6{
		replace grd2_eng_a_`skill'_`i'=0 if grd2_eng_a_`skill'_1==0 & grd2_eng_a_`skill'_2==0
	}
}
*/
foreach skill in  bwa j t{
	replace grd2_his_a_`skill'_3=0 if grd2_his_a_`skill'_1==0 & grd2_his_a_`skill'_2==0
} 



egen Kis_Maneno=rowtotal(grd2_kis_a_ma_1 grd2_kis_a_ma_2 grd2_kis_a_ma_3 grd2_kis_a_ma_4 grd2_kis_a_ma_5 grd2_kis_a_ma_6)
egen Kis_Sentenci=rowtotal(grd2_kis_a_se_1 grd2_kis_a_se_2 grd2_kis_a_se_3 grd2_kis_a_se_4 grd2_kis_a_se_5 grd2_kis_a_se_6)
gen Kis_Aya=grd2_kis_a_h
egen Kis_Comp=rowtotal(grd2_kis_a_m_1 grd2_kis_a_m_2)
 
/*
egen Eng_Word=rowtotal(grd2_eng_a_w_1 grd2_eng_a_w_2 grd2_eng_a_w_3 grd2_eng_a_w_4 grd2_eng_a_w_5 grd2_eng_a_w_6)
egen Eng_Sentences=rowtotal(grd2_eng_a_se_1 grd2_eng_a_se_2 grd2_eng_a_se_3 grd2_eng_a_se_4 grd2_eng_a_se_5 grd2_eng_a_se_6)
gen Eng_Paragraph=grd2_eng_a_p
*/
egen Math_bwa=rowtotal(grd2_his_a_bwa_1 grd2_his_a_bwa_2 grd2_his_a_bwa_3)
egen Math_j=rowtotal(grd2_his_a_j_1 grd2_his_a_j_2 grd2_his_a_j_3)
egen Math_t=rowtotal(grd2_his_a_t_1 grd2_his_a_t_2 grd2_his_a_t_3)
*egen Math_z=rowtotal(grd2_his_a_z_1 grd2_his_a_z_2 grd2_his_a_z_3)

foreach var of varlist  Kis_Maneno{
	gen `var'_Pass=`var'>=4 & !missing(`var')
}
foreach var of varlist grd2_kis_a_se_*   {
	gen `var'_Pass=(`var'>=2) & !missing(`var')
}
foreach var of varlist Kis_Sentenci    {
	gen `var'_Pass=((grd2_kis_a_se_1_Pass+ grd2_kis_a_se_2_Pass+ grd2_kis_a_se_3_Pass +grd2_kis_a_se_4_Pass+ grd2_kis_a_se_5_Pass+ grd2_kis_a_se_6_Pass)>=4)
}
drop grd2_kis_a_se_1_Pass-grd2_kis_a_se_6_Pass

foreach var of varlist  Math_bwa Math_j Math_t {
	gen `var'_Pass=`var'>=2 & !missing(`var')
}

foreach var of varlist  Kis_Aya{
	gen `var'_Pass=`var'==3 & !missing(`var')
}

foreach var of varlist  Kis_Comp{
	gen `var'_Pass=`var'==2 & !missing(`var')
}



keep name- studentID *Pass treatment2 treatarm2
saveold "CreatedData/4 Intervention/TwaEL_2016/All_Grd2Pass_Students.dta",replace version(12)




use "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_StadiandControl_CLEAN.dta", clear
append using "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_Mashindano_CLEAN.dta"
drop  gender set start startampm volname end endampm
replace stream = lower(stream)
rename schoolid SchoolID
rename studentid studentID
gen studentid=studentID
merge m:1 SchoolID using "RawData/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2)
drop if _merge==2
drop _merge
merge m:1 SchoolID using "CommScripts/Kigoma Sample/KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update
drop if _merge==2
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440
*136 is a split school
replace treatarm2=1 if SchoolID==136
replace treatment2="Levels" if SchoolID==136
replace SchoolID=1136 if SchoolID==1104 & stream=="b"
replace treatarm2=1 if SchoolID==1136
replace treatment2="Levels" if SchoolID==1136


ds, has(type string)
foreach var of varlist `r(varlist)'{
replace `var'="" if `var'=="--blank--" | `var'=="X"
}

destring , replace
drop if grade!=3

foreach var of varlist SchoolID grade stream{
	qui tabmiss `var'
	display "`r(sum)' missing in variable `var'"
	display "They are dropped"
	drop if	missing(`var')
}


qui tabmiss grd3_kis_a_h
display "Skills Reading in Swahili/Qs1 has `r(sum)' missing"
qui tabmiss grd3_eng_a_s
display "Skills English in Swahili/Qs1 has `r(sum)' missing"



foreach skill in  j t z g{
	qui tabmiss grd3_his_a_`skill'_1
	display "Skills `skill' in Math/Qs1 has `r(sum)' missing"
	qui tabmiss grd3_his_a_`skill'_1
	display "Skills `skill' in Math/Qs2 has `r(sum)' missing"
}



foreach var of varlist grd*{
	replace `var'=0 if `var'==.
}

replace grd3_kis_a_h=0 if grd3_kis_a_h==.

*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_kis_a_m_1=0 if grd3_kis_a_h==0
replace grd3_kis_a_m_2=0 if grd3_kis_a_h==0


*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_eng_a_c_1=0 if grd3_eng_a_s==0
replace grd3_eng_a_c_2=0 if grd3_eng_a_s==0

*First data cleaning process for math is as before...
foreach skill in  j t z g{
	replace grd3_his_a_`skill'_3=0 if grd3_his_a_`skill'_1==0 & grd3_his_a_`skill'_2==0

} 


capture drop grd3_kis_b* grd3_kis_c* grd3_eng_b* grd3_eng_c* grd3_his_b* grd3_his_c*


gen Kis_Story=grd3_kis_a_h
egen Kis_Comp=rowtotal(grd3_kis_a_m_1 grd3_kis_a_m_2)
     
gen Eng_Story=grd3_eng_a_s
egen Eng_Comp=rowtotal(grd3_eng_a_c_1 grd3_eng_a_c_2)
 

egen Math_j=rowtotal(grd3_his_a_j_1 grd3_his_a_j_2 grd3_his_a_j_3)
egen Math_t=rowtotal(grd3_his_a_t_1 grd3_his_a_t_2 grd3_his_a_t_3)
egen Math_z=rowtotal(grd3_his_a_z_1 grd3_his_a_z_2 grd3_his_a_z_3)
egen Math_g=rowtotal(grd3_his_a_g_1 grd3_his_a_g_2 grd3_his_a_g_3)


foreach var of varlist  Math_j Math_t Math_z Math_g{
	gen `var'_Pass=`var'>=2 & !missing(`var')
}

foreach var of varlist  Kis_Story Eng_Story{
	gen `var'_Pass=`var'==3 & !missing(`var')
}

foreach var of varlist  Kis_Comp Eng_Comp {
	gen `var'_Pass=`var'==2 & !missing(`var')
}


keep name- studentID  *Pass treatment2 treatarm2
saveold "CreatedData/4 Intervention/TwaEL_2016/All_Grd3Pass_Students.dta",replace version(12)



use "CreatedData/4 Intervention/TwaEL_2016/All_Grd1Pass_Students.dta", clear
append using "CreatedData/4 Intervention/TwaEL_2016/All_Grd2Pass_Students.dta"
append using "CreatedData/4 Intervention/TwaEL_2016/All_Grd3Pass_Students.dta"

preserve
collapse (count) Kis_Silabi_Pass- Eng_Comp_Pass, by(districtid grade treatarm2 treatment2) 
save "CreatedData/4 Intervention/TwaEL_2016/All_Tested.dta", replace
restore
preserve
collapse (mean) Kis_Silabi_Pass- Eng_Comp_Pass, by(districtid grade treatarm2 treatment2) 
save "CreatedData/4 Intervention/TwaEL_2016/All_PassRate.dta", replace
restore
preserve
collapse (sum) Kis_Silabi_Pass- Eng_Comp_Pass, by(districtid grade treatarm2 treatment2) 
save "CreatedData/4 Intervention/TwaEL_2016/All_PassRaw.dta", replace
restore
