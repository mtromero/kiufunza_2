use "C:\Users\Mauricio\Box Sync\01_KiuFunza\RawData\11 Endline 2015\Final Data\Teacher Data\R8TGrdSub2_nopii.dta", clear
keep if R8TGradeID>=1 & R8TGradeID<=3
keep if R8TGrdSub2ID>=1 & R8TGrdSub2ID<=3 
keep if tgrdyn==1
mmerge R8TeacherID SchoolID  using "$basein/11 Endline 2015/Final Data/Teacher Data/R8Teacher_nopii.dta", uif(R8TeacherID!=.) type( n:1 ) ukeep(SchoolID R8TeacherID upid tcodhttpay tcodfar codort codops codoif codopt knwtwa2 trstwa sgnpet wchprg programprefer topbpm toppal passAll bonusSkills SkillsWorth HTEarn1 elgsub_levels1 elgsub_levels2 elgsub_levels3 GroupsAbility EarnFail StudentAB HTEarn2 elgsub_gain1 elgsub_gain2 elgsub_gain3)
drop if _merge!=3
drop _merge


gen PreferLevels=(programprefer==1) & !missing(programprefer)
label var PreferLevels "Single test"

label define topbm_l 1 "Very favorable" 2 "Somewhat favorable" 3 "Neutral" 4 "Somewhat unfavorable" 5 "Very unfavorable"
label var topbpm topbm_l

gen FavorableBonusPayments=(topbpm==1 | topbpm==2) & !missing(topbpm)
label var PreferLevels "Bonus payments"

recode knwtwa2 (2=0)
recode sgnpet trstwa tcodhttpay (1/2=1) (3/5=0)

recode SkillsWorth 1=0 2=1 3=0
recode GroupsAbility 2=0
recode EarnFail 2=0
recode StudentAB 2=0
recode passAll 2=1 1=0
recode bonusSkills 2=0 
recode HTEarn1 HTEarn2 (2=1) (-99=0) (1=0) (3=0) (4=0)
recode elgsub_levels1 elgsub_levels3 elgsub_gain1  elgsub_gain3 (2=1) (1=0)
recode elgsub_levels2 elgsub_gain2 (2=0)
*irt (2pl bonusSkills SkillsWorth), intpoints(20) difficult
*predict IRT_Stadi, latent 

egen FracStadi=rowtotal(bonusSkills SkillsWorth elgsub_levels1 elgsub_levels2 elgsub_levels3), missing
egen FracMash=rowtotal(GroupsAbility StudentAB elgsub_gain1 elgsub_gain2 elgsub_gain3), missing
gen FracCorrect=.
replace FracCorrect=FracStadi/5 if FracStadi!=.
replace FracCorrect=FracMash/5 if FracMash!=.

drop FracStadi FracMash
egen FracStadi=rowtotal(bonusSkills SkillsWorth), missing
egen FracMash=rowtotal(GroupsAbility StudentAB), missing
gen FracCorrectCore=.
replace FracCorrectCore=FracStadi/2 if FracStadi!=.
replace FracCorrectCore=FracMash/2 if FracMash!=.
drop FracStadi FracMash

recode codort codops codoif codopt (-99=.) (8=0) (6/7=1)
egen MoneyEvenifFail=rowtotal(EarnFail passAll), missing
drop if FracCorrectCore==.
collapse (count) NumTeachers=R8TeacherID (mean) PreferLevels FavorableBonusPayments knwtwa2 sgnpet trstwa tcodhttpay FracCorrect FracCorrectCore codort codops codoif codopt MoneyEvenifFail, by(SchoolID R8TGradeID)
label var NumTeachers "Number of teachers"
label var PreferLevels "Single test"
label var FavorableBonusPayments "Bonus payments"
label var knwtwa2 "Know Twaweza"
label var sgnpet "Support Twaweza"
label var trstwa "Trust Twaweza"
label var tcodhttpay "Trust payments"
label var FracCorrect "Understanding"
label var FracCorrectCore "Understanding (core)"
label var codort "Resources (taken away)"
label var codops "Share"
label var codoif "Interfered"
label var codopt "Parent Support"
label var MoneyEvenifFail "Can earn bonus even if student fails"

rename R8TGradeID GradeID_T2
rename FracCorrectCore FracCorrectCore_T2
keep SchoolID  GradeID_T2 FracCorrectCore
save "$base_out/ConsolidatedYr34/AverageTeacherUnderstanding_T2.dta", replace





use "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10TGrdSubGrp_nopii.dta", clear
keep if R10TGradeID>=1 & R10TGradeID<=3
keep if R10TGrdSub2ID>=1 & R10TGrdSub2ID<=3 
keep if tchgrpyn==1

mmerge R10TeacherID SchoolID  using "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10Teacher_nopii.dta", uif(R10TeacherID!=.) type( n:1 ) ukeep(SchoolID R10TeacherID ETeach_pp upid tcodhttpay tcodfar codort codops codoif codopt knwtwa2 trstwa sgnpet wchprg programprefer topbpm toppal passAll bonusSkills SkillsWorth tearnl elgsub_levels1 elgsub_levels2 elgsub_levels3 GroupsAbility EarnFail StudentAB tearng elgsub_gain1 elgsub_gain2 elgsub_gain3)
drop if _merge!=3
drop _merge


gen PreferLevels=(programprefer==1) & !missing(programprefer)
label var PreferLevels "Single test"

gen FavorableBonusPayments=(topbpm==1 | topbpm==2) & !missing(topbpm)
label var PreferLevels "Bonus payments"

recode knwtwa2 (2=0)
recode sgnpet trstwa tcodhttpay (1/2=1) (3/5=0)

recode SkillsWorth 1=0 2=1 3=0
recode GroupsAbility 2=0
recode EarnFail 2=0
recode StudentAB 2=0
recode passAll 2=1 1=0
recode bonusSkills 2=0 
recode tearnl tearng (2=0) 
recode elgsub_levels1 elgsub_levels3 elgsub_gain1  elgsub_gain3 (2=1) (1=0)
recode elgsub_levels2 elgsub_gain2 (2=0)
egen FracStadi=rowtotal(bonusSkills SkillsWorth elgsub_levels1 elgsub_levels2 elgsub_levels3), missing
egen FracMash=rowtotal(GroupsAbility StudentAB elgsub_gain1 elgsub_gain2 elgsub_gain3), missing
gen FracCorrect=.
replace FracCorrect=FracStadi/5 if FracStadi!=.
replace FracCorrect=FracMash/5 if FracMash!=.

drop FracStadi FracMash
egen FracStadi=rowtotal(bonusSkills SkillsWorth), missing
egen FracMash=rowtotal(GroupsAbility StudentAB), missing
gen FracCorrectCore=.
replace FracCorrectCore=FracStadi/2 if FracStadi!=.
replace FracCorrectCore=FracMash/2 if FracMash!=.
drop FracStadi FracMash
egen MoneyEvenifFail=rowtotal(EarnFail passAll), missing

recode codort codops codoif codopt (-99=.) (8=0) (6/7=1)
drop if FracCorrectCore==.
collapse (count) NumTeachers=R10TeacherID (mean) PreferLevels FavorableBonusPayments knwtwa2 sgnpet trstwa tcodhttpay FracCorrect FracCorrectCore codort codops codoif codopt MoneyEvenifFail, by(SchoolID R10TGradeID)

label var PreferLevels "Single test"
label var FavorableBonusPayments "Bonus payments"
label var knwtwa2 "Know Twaweza"
label var sgnpet "Support Twaweza"
label var trstwa "Trust Twaweza"
label var tcodhttpay "Trust payments"
label var FracCorrect "Understanding"
label var FracCorrectCore "Understanding (core)"
label var codort "Resources (taken away)"
label var codops "Share"
label var codoif "Interfered"
label var codopt "Parent Support"
label var MoneyEvenifFail "Can earn bonus even if student fails"


rename R10TGradeID GradeID_T5
rename FracCorrectCore FracCorrectCore_T5
keep SchoolID  GradeID_T5 FracCorrectCore

save "$base_out/ConsolidatedYr34/AverageTeacherUnderstanding_T5.dta", replace
