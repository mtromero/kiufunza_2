
	*** Mauricio's laptop
	if "`c(username)'" == "Mauricio"  {
		global mipath "C:/Users/Mauricio/Box Sync/01_KiuFunza"
		global dir_do     "C:/Users/Mauricio/Documents/git/kiufunza_2"
		global paper "C:/Users/Mauricio//Dropbox/KF_Draft/KFII/LaTeX"
	}
	*** Mauricio's desktop
	if "`c(username)'" == "mauri"  {
		global mipath "C:/Users/mauri/Box Sync/01_KiuFunza"
		global dir_do     "C:/Users/mauri/Documents/git/kiufunza_2"
		global paper "C:/Users/mauri//Dropbox/KF_Draft/KFII/LaTeX"
	}
	
	*** ITAM's desktop
	if "`c(username)'" == "MROMEROLO"  {
		global mipath "D:/Box Sync/01_KiuFunza"
		global dir_do     "D:/git/kiufunza_2"
		global paper "D:/Dropbox/KF_Draft/KFII/LaTeX"
	}


* Path Tree
  global basein 	"$mipath/RawData"
  global base_out   "$mipath/CreatedData"

  global results    "$mipath/Results/Year34"
  global exceltables  "$mipath/Results/Year34/ExcelTables"
  global graphs     "$paper/figures"
  global latexcodes     "$paper/tables"
	
	
	
global schoolcontrol  PTR_T1  SingleShift_T1 DistanceIndex_T1 InfrastructureIndex_T1
global studentcontrol c.LagpreSchoolYN#MissingpreSchoolYN MissingpreSchoolYN ///
c.LagGender#MissingGender c.MissingGender c.LagAge#c.MissingAge c.MissingAge ///
c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza)##c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza) ///
c.MissingZ_kiswahili c.MissingZ_hisabati c.MissingZ_kiingereza
  
global HHcontrol c.IndexPoverty#c.MissingIndexPoverty c.MissingIndexPoverty c.IndexEngagement#MissingIndexEngagement c.MissingIndexEngagement ///
c.IndexKowledge#MissingIndexKowledge c.MissingIndexKowledge 

 
global UnObs UnObsTeaching  UnObsClassManagement UnObsOffTask UnObsStudentOffTask UnObsIndex
global PhysicalClassroom crowding studentsfloor dirtfloor blackboardandchalk electricity  ClasroomIndex
global AmbientClassroom workdisplay chartsdisplay trash  AmbientIndex
global TeachingActivities hitting teachersmile teacherthreaten teacherkind teacherhit teacherphone teacherleave TeachingIndex
global Inputs proptext proppen InputsIndex

global treatmentlist TreatmentLevels TreatmentGains

