**********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
fvset base default treatarm 
gen Diff_T2=date_twa_T2-date_edi_T2
gen Diff_T5=date_twa_T5-date_edi_T5
gen Week_T2=week(date_edi_T2)
gen Week_T5=week(date_edi_T5)


global AggregateDep 	Z_hisabati Z_kiswahili Z_kiingereza /*this should be added in the future... Z_sayansi Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal*/ 
global AggregateDep_Karthik 	Z_hisabati Z_kiswahili Z_kiingereza   /*Z_kiingereza  Z_ScoreFocal this should be added in the future...*/
global AggregateDep_int 	Z_hisabati Z_kiswahili Z_kiingereza
 
global AggregateDep_NonEng 	Z_hisabati Z_kiswahili 
global AggregateDep_Eng 	Z_kiingereza

global treatmentlist TreatmentLevels TreatmentGains
gen TreatmentLevels2=TreatmentLevels
gen TreatmentGains2=TreatmentGains
global treatmentlist2 TreatmentLevels2 TreatmentGains2
*enrollment2015_T1 Rural_T1 ClassesOutside_T1 Electricity_T1

label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"      
*c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza)##c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza) ///


global student2 shoes_T2 socks_T2 dirty_T2 uniformdirty_T2 uniformtorn_T2  ringworm_T2 CloseToeShoe_T2

recode bookshome_T2 (-98=.)
global studentoutcomes studentsfight_T2 sing_T2 bookshome_T2

bys SchoolID: egen Z_kiswahili_T0_G=mean(Z_kiswahili_T0)
bys SchoolID: egen Z_kiingereza_T0_G=mean(Z_kiingereza_T0)
bys SchoolID: egen Z_hisabati_T0_G=mean(Z_hisabati_T0)
pca Z_hisabati_T0  Z_kiingereza_T0 Z_kiswahili_T0
predict Z_ScoreFocal_T0, score
*First lets create the tables Karthik Wants

label var Z_kiswahili_T2 Swahili
label var Z_kiswahili_T5 Swahili
label var Z_kiingereza_T2 English
label var Z_kiingereza_T5 English
label var Z_hisabati_T2 Math
label var Z_hisabati_T5 Math
label var Z_sayansi_T2 Science
label var Z_sayansi_T5 Science
label var Z_ScoreFocal_T2 "Focal Subjects"
label var Z_ScoreFocal_T5 "Focal Subjects"
recode missschool_T5 (2=0) 
***********************************************
***********************************************
********** EDI TEST SCORES ********************
***********************************************
***********************************************
 reghdfe Z_kiswahili_T2 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol if GradeID_T2<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
 			
eststo clear
foreach time in T2 T5{
	foreach var in $AggregateDep_Karthik {
		if "`var'"=="Z_kiingereza" & "`time'"=="T2"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol $schoolcontrol if GradeID_`time'==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
		if "`var'"=="Z_kiingereza" & "`time'"=="T5"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol $schoolcontrol if GradeID_`time'==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
		if "`var'"!="Z_kiingereza"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol $schoolcontrol if GradeID_`time'<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
	}  
}


esttab  using "$latexcodes/RegKarthik.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

eststo clear
eststo:  reghdfe Z_sayansi_T2 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol if GradeID_T2<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
eststo:  reghdfe Z_sayansi_T5 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol if GradeID_T5<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
esttab  using "$latexcodes/RegScience.tex", booktabs  se ar2 label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace  mtitles("Year 1" "Year 2")  ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)
		
			

eststo clear
foreach time in T2 T5{
	foreach var in $AggregateDep_Karthik {
		if "`var'"=="Z_kiingereza" & "`time'"=="T2"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol  if GradeID_`time'==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
		if "`var'"=="Z_kiingereza" & "`time'"=="T5"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol  if GradeID_`time'==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
		if "`var'"!="Z_kiingereza"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol  if GradeID_`time'<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
	}  
}


esttab  using "$latexcodes/RegKarthik_nocontrol.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)


eststo clear
eststo:  reghdfe Z_sayansi_T2 $treatmentlist i.LagGrade $studentcontrol  if GradeID_T2<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
eststo:  reghdfe Z_sayansi_T5 $treatmentlist i.LagGrade $studentcontrol  if GradeID_T5<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
esttab  using "$latexcodes/RegScience_nocontrol.tex", booktabs  se ar2 label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace  collabels(none) nogaps nolines  ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)
/*		
gen outcome_T2=Z_ScoreFocal_T2 if  GradeID_T2==3  & Z_ScoreFocal_T2!=.
replace outcome_T2=Z_ScoreKisawMath_T2 if GradeID_T2<3 & Z_ScoreKisawMath_T2!=.

gen outcome_T5=Z_ScoreFocal_T5 if  GradeID_T5==3  & Z_ScoreFocal_T5!=.
replace outcome_T5=Z_ScoreKisawMath_T5 if GradeID_T5<3 & Z_ScoreKisawMath_T5!=.			
*/

eststo clear
foreach time in T2 T5{
	foreach var in $AggregateDep_NonEng Z_ScoreKisawMath {
		if "`var'"=="Z_kiingereza" & "`time'"=="T2"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_`time'==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
			
			local tempm=string(abs(_b[TreatmentGains] - _b[TreatmentLevels]), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_coef_diff.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			
			if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			}
			if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			}
		}
		if "`var'"=="Z_kiingereza" & "`time'"=="T5"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_`time'==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
			
			local tempm=string(abs(_b[TreatmentGains] - _b[TreatmentLevels]), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_coef_diff.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			
			if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			}
			if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			}
		}
		if "`var'"!="Z_kiingereza"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_`time'<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
			
			local tempm=string(abs(_b[TreatmentGains] - _b[TreatmentLevels]), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_coef_diff.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		
			if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			}
			if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
			file write newfile "`tempm'"
			file close newfile
			}
		
		
		}
		
		matrix tempm=e(b)
		local tempm=string(tempm[1,1], "%9.2gc")
		local tempm_effect=tempm[1,1]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_levels.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		local tempm=string(tempm[1,2], "%9.2gc")
		local tempm_effect=tempm[1,2]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_gains.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		
		test TreatmentLevels
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		test TreatmentGains
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		
		
	}  
}

label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

esttab  using "$latexcodes/RegKarthik_HHControls.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

/*
gen outcome_T2=Z_ScoreFocal_T2 if  GradeID_T2==3  & Z_ScoreFocal_T2!=.
replace outcome_T2=Z_ScoreKisawMath_T2 if GradeID_T2<3 & Z_ScoreKisawMath_T2!=.

gen outcome_T5=Z_ScoreFocal_T5 if  GradeID_T5==3  & Z_ScoreFocal_T5!=.
replace outcome_T5=Z_ScoreKisawMath_T5 if GradeID_T5<3 & Z_ScoreKisawMath_T5!=.
*/
/*
eststo clear
eststo: reghdfe outcome_T2 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T2<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
eststo: reghdfe outcome_T5 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T5<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
			
esttab  using "$latexcodes/RegKarthik_HHControls_Index.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)
*/
	

************* WITHOUT GRADE 1, NOT PROPERLY SEEDED **************



eststo clear
foreach time in T2 T5{
	foreach var in $AggregateDep_NonEng Z_ScoreKisawMath {
		if "`var'"=="Z_kiingereza" & "`time'"=="T2"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_`time'==3 & GradeID_`time'!=1, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
		if "`var'"=="Z_kiingereza" & "`time'"=="T5"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_`time'==3 & GradeID_`time'!=1, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]	
		}
		if "`var'"!="Z_kiingereza"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_`time'<=3 & GradeID_`time'!=1, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
	
	}  
}

label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

esttab  using "$latexcodes/RegKarthik_HHControls_NOGRD1.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

**************************8		


eststo clear
eststo:  reghdfe Z_sayansi_T2 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol  if GradeID_T2<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
eststo:  reghdfe Z_sayansi_T5 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol  if GradeID_T5<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]

esttab  using "$latexcodes/RegScience_HHControls.tex", booktabs  se ar2 label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none) collabels(none) nogaps nolines  ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)
	
eststo clear
eststo:  reghdfe Z_kiingereza_T2 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol  if GradeID_T2==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
eststo:  reghdfe Z_kiingereza_T5 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol  if GradeID_T5==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]

esttab  using "$latexcodes/RegEnglish_HHControls.tex", booktabs  se ar2 label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)
	

	
***********************************************
***********************************************
********** EYOS + HH Controls ********************
***********************************************
***********************************************

		
eststo clear
foreach time in T2 T5{
	foreach var in $AggregateDep_Karthik {
		preserve
		if "`var'"=="Z_kiingereza" & "`time'"=="T2"{
			reghdfe `var'_`time'  LagGrade  c.LagseenUwezoTests#c.MissingseenUwezoTests MissingseenUwezoTests c.LagpreSchoolYN#MissingpreSchoolYN MissingpreSchoolYN c.LagGender#MissingGender c.MissingGender c.LagAge#c.MissingAge c.MissingAge $schoolcontrol $HHcontrol  if GradeID_`time'<=3 & treatment2=="Control", vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			lincom _b[c.LagAge#c.MissingAge]+_b[LagGrade]
			local tempm_progression=r(estimate)
			replace `var'_`time'=`var'_`time'/`tempm_progression'
			
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_`time'==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
			
			
			
		}
		if "`var'"=="Z_kiingereza" & "`time'"=="T5"{
			reghdfe `var'_`time'  LagGrade  c.LagseenUwezoTests#c.MissingseenUwezoTests MissingseenUwezoTests c.LagpreSchoolYN#MissingpreSchoolYN MissingpreSchoolYN c.LagGender#MissingGender c.MissingGender c.LagAge#c.MissingAge c.MissingAge $schoolcontrol $HHcontrol  if GradeID_`time'<=3 & treatment2=="Control", vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			matrix tempm=e(b)
			local tempm_progression=tempm[1,1]
			replace `var'_`time'=`var'_`time'/`tempm_progression'
			
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_`time'==3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_`time'  LagGrade  c.LagseenUwezoTests#c.MissingseenUwezoTests MissingseenUwezoTests c.LagpreSchoolYN#MissingpreSchoolYN MissingpreSchoolYN c.LagGender#MissingGender c.MissingGender c.LagAge#c.MissingAge c.MissingAge $schoolcontrol $HHcontrol  if GradeID_`time'<=3 & treatment2=="Control", vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			matrix tempm=e(b)
			local tempm_progression=tempm[1,1]
			replace `var'_`time'=`var'_`time'/`tempm_progression'
			
			eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_`time'<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
		restore
	}
}


esttab  using "$latexcodes/RegKarthik_HHControls_EYOS.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)



preserve
eststo clear
reghdfe Z_sayansi_T2  LagGrade  c.LagseenUwezoTests#c.MissingseenUwezoTests MissingseenUwezoTests c.LagpreSchoolYN#MissingpreSchoolYN MissingpreSchoolYN c.LagGender#MissingGender c.MissingGender c.LagAge#c.MissingAge c.MissingAge $schoolcontrol $HHcontrol  if GradeID_T2<=3 & treatment2=="Control", vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
lincom _b[c.LagAge#c.MissingAge]+_b[LagGrade]
local tempm_progression=r(estimate)
replace Z_sayansi_T2=Z_sayansi_T2/`tempm_progression'

reghdfe Z_sayansi_T5  LagGrade  c.LagseenUwezoTests#c.MissingseenUwezoTests MissingseenUwezoTests c.LagpreSchoolYN#MissingpreSchoolYN MissingpreSchoolYN c.LagGender#MissingGender c.MissingGender c.LagAge#c.MissingAge c.MissingAge $schoolcontrol $HHcontrol  if GradeID_T5<=3 & treatment2=="Control", vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
lincom _b[c.LagAge#c.MissingAge]+_b[LagGrade]
local tempm_progression=r(estimate)
replace Z_sayansi_T5=Z_sayansi_T5/`tempm_progression'	
eststo:  reghdfe Z_sayansi_T2 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol  if GradeID_T2<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
eststo:  reghdfe Z_sayansi_T5 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol  if GradeID_T5<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
esttab  using "$latexcodes/RegScience_HHControls_EYOS.tex", booktabs se ar2 label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)
restore
	



***********************************************
***********************************************
********** GRADE 4 EDI ********************
***********************************************
***********************************************


eststo clear
foreach time in T2 T5{
foreach var in $AggregateDep_NonEng Z_ScoreKisawMath {
eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade  $studentcontrol ${schoolcontrol} ${HHcontrol}  if GradeID_`time'==4, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
}  
}


esttab  using "$latexcodes/RegKarthik_Grd4.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)


eststo clear
foreach time in T2 T5{
	foreach var in $AggregateDep_NonEng Z_ScoreKisawMath {
	eststo:  reghdfe `var'_`time' $treatmentlist i.LagGrade  $studentcontrol ${schoolcontrol} ${HHcontrol}  if GradeID_`time'==4, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
	estadd ysumm
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	
	
			
		local tempm=string(abs(_b[TreatmentGains] - _b[TreatmentLevels]), "%9.2gc")
		file open newfile using "$latexcodes/`var'_`time'_coef_diff_grd4.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		if r(p)<0.01 {
		di "peque"
		local tempm ="$<0.01$"
		file open newfile using "$latexcodes/`var'_`time'_pvalue_diff_grd4.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		}
		if r(p)>0.01 {
		di "grande"
		local tempm=string(r(p), "%9.2gc")
		file open newfile using "$latexcodes/`var'_`time'_pvalue_diff_grd4.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		}
		
		matrix tempm=e(b)
		local tempm=string(tempm[1,1], "%9.2gc")
		local tempm_effect=tempm[1,1]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_levels_grd4.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		local tempm=string(tempm[1,2], "%9.2gc")
		local tempm_effect=tempm[1,2]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_gains_grd4.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		
		test TreatmentLevels
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels_grd4.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels_grd4.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		test TreatmentGains
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains_grd4.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains_grd4.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}

			
	}  
}


esttab  using "$latexcodes/RegKarthik_Grd4_Index.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)


***********************************************
***********************************************
********** TWA TEST SCORES ********************
***********************************************
***********************************************
label var Z_kiswahili_T3 "Swahili" 
label var Z_kiingereza_T3 "English" 
label var Z_hisabati_T3 "Math" 
label var Z_kiswahili_T6 "Swahili" 
label var Z_kiingereza_T6 "English" 
label var Z_hisabati_T6 "Math" 
gen outcome_T3=Z_ScoreFocal_T3 if  Grade_T3==3  & Z_ScoreFocal_T3!=.
replace outcome_T3=Z_ScoreKisawMath_T3 if Grade_T3<3 & Z_ScoreKisawMath_T3!=.

gen outcome_T6=Z_ScoreFocal_T6 if  Grade_T6==3  & Z_ScoreFocal_T6!=.
replace outcome_T6=Z_ScoreKisawMath_T6 if Grade_T6<3 & Z_ScoreKisawMath_T6!=.

eststo clear
foreach time in T3 T6{
	foreach var in $AggregateDep_NonEng Z_ScoreKisawMath {
		if "`var'"=="Z_kiingereza"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.Grade_`time' ${schoolcontrol} if Grade_`time'==3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
		if "`var'"!="Z_kiingereza"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.Grade_`time' ${schoolcontrol} if Grade_`time'<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
		
		local tempm=string(abs(_b[TreatmentGains] - _b[TreatmentLevels]), "%9.2gc")
		file open newfile using "$latexcodes/`var'_`time'_coef_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	
		if r(p)<0.01 {
		di "peque"
		local tempm ="$<0.01$"
		file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		}
		if r(p)>0.01 {
		di "grande"
		local tempm=string(r(p), "%9.2gc")
		file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		}
		
		matrix tempm=e(b)
		local tempm=string(tempm[1,1], "%9.2gc")
		local tempm_effect=tempm[1,1]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_levels.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		local tempm=string(tempm[1,2], "%9.2gc")
		local tempm_effect=tempm[1,2]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_gains.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		
		test TreatmentLevels
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		test TreatmentGains
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		
	}  
}


label var TreatmentLevels "Levels (\$\beta_1\$)"
label var TreatmentGains "P4Pctile (\$\beta_2\$)"

esttab  using "$latexcodes/RegKarthikTWA.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc a2) labels("N. of obs." "\$\beta_3 = \beta_2-\beta_1\$" "p-value (\$H_0:\beta_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)



eststo clear
eststo: reghdfe Z_kiingereza_T3 $treatmentlist  i.Grade_T3 ${schoolcontrol} if Grade_T3<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
eststo: reghdfe Z_kiingereza_T6 $treatmentlist  i.Grade_T6 ${schoolcontrol} if Grade_T6<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
			
esttab  using "$latexcodes/RegKarthikTWA_English.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

eststo clear
eststo: reghdfe outcome_T3 $treatmentlist  i.Grade_T3 ${schoolcontrol} if Grade_T3<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
eststo: reghdfe outcome_T6 $treatmentlist  i.Grade_T6 ${schoolcontrol} if Grade_T6<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
			
esttab  using "$latexcodes/RegKarthikTWA_Index.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)



***********************************************
***********************************************
********** TWA TEST SCORES No controls ********************
***********************************************
***********************************************


eststo clear
foreach time in T3 T6{
	foreach var in $AggregateDep_NonEng Z_ScoreKisawMath {
		if "`var'"=="Z_kiingereza"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.Grade_`time'  if Grade_`time'==3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
		if "`var'"!="Z_kiingereza"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.Grade_`time'  if Grade_`time'<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
		
		local tempm=string(abs(_b[TreatmentGains] - _b[TreatmentLevels]), "%9.2gc")
		file open newfile using "$latexcodes/`var'_`time'_coef_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	
		if r(p)<0.01 {
		di "peque"
		local tempm ="$<0.01$"
		file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		}
		if r(p)>0.01 {
		di "grande"
		local tempm=string(r(p), "%9.2gc")
		file open newfile using "$latexcodes/`var'_`time'_pvalue_diff.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		}
		
		matrix tempm=e(b)
		local tempm=string(tempm[1,1], "%9.2gc")
		local tempm_effect=tempm[1,1]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_levels.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		local tempm=string(tempm[1,2], "%9.2gc")
		local tempm_effect=tempm[1,2]
		
		file open newfile using "$latexcodes/`var'_`time'_coef_gains.tex", write replace
		file write newfile "`tempm'"
		file close newfile
		
		
		test TreatmentLevels
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_levels.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		test TreatmentGains
		if r(p)<0.01 {
			di "peque"
			local tempm ="$<0.01$"
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		if r(p)>0.01 {
			di "grande"
			local tempm=string(r(p), "%9.2gc")
			file open newfile using "$latexcodes/`var'_`time'_pvalue_gains.tex", write replace
			file write newfile "`tempm'"
			file close newfile
		}
		
	}  
}


label var TreatmentLevels "Levels (\$\beta_1\$)"
label var TreatmentGains "P4Pctile (\$\beta_2\$)"

esttab  using "$latexcodes/RegKarthikTWA_nocontrol.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc a2) labels("N. of obs." "\$\beta_3 = \beta_2-\beta_1\$" "p-value (\$H_0:\beta_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)



eststo clear
eststo: reghdfe Z_kiingereza_T3 $treatmentlist  i.Grade_T3 ${schoolcontrol} if Grade_T3<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
eststo: reghdfe Z_kiingereza_T6 $treatmentlist  i.Grade_T6 ${schoolcontrol} if Grade_T6<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
			
esttab  using "$latexcodes/RegKarthikTWA_English.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

eststo clear
eststo: reghdfe outcome_T3 $treatmentlist  i.Grade_T3 ${schoolcontrol} if Grade_T3<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
eststo: reghdfe outcome_T6 $treatmentlist  i.Grade_T6 ${schoolcontrol} if Grade_T6<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
			
esttab  using "$latexcodes/RegKarthikTWA_Index.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)



***********************************************
***********************************************
********** TWA TEST SCORES WITHOUT GRADE 1 ********************
***********************************************
***********************************************
label var Z_kiswahili_T3 "Swahili" 
label var Z_kiingereza_T3 "English" 
label var Z_hisabati_T3 "Math" 
label var Z_kiswahili_T6 "Swahili" 
label var Z_kiingereza_T6 "English" 
label var Z_hisabati_T6 "Math" 

eststo clear
foreach time in T3 T6{
	foreach var in $AggregateDep_NonEng Z_ScoreKisawMath{
		if "`var'"=="Z_kiingereza"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.Grade_`time' ${schoolcontrol} if Grade_`time'==3 & Grade_`time'!=1, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}
		if "`var'"!="Z_kiingereza"{
			eststo:  reghdfe `var'_`time' $treatmentlist i.Grade_`time' ${schoolcontrol} if Grade_`time'<=3 & Grade_`time'!=1, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
			estadd ysumm
			test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
			estadd scalar p=r(p)
			estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
		}

	}  
}


label var TreatmentLevels "Levels (\$\beta_1\$)"
label var TreatmentGains "P4Pctile (\$\beta_2\$)"

esttab  using "$latexcodes/RegKarthikTWA_NOGRD1.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc a2) labels("N. of obs." "\$\beta_3 = \beta_2-\beta_1\$" "p-value (\$H_0:\beta_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)



*********** HIGH -STAKES number of test takers

eststo clear
capture drop taken*
preserve
gen taken_T3=!missing(Z_hisabati_T3)
gen taken_T6=!missing(Z_hisabati_T6)
collapse (sum) taken_T3 taken_T6 (mean) $treatmentlist, by(SchoolID DistrictID StrataScore treatarm )


foreach time in T3 T6{
	eststo:  reghdfe taken_`time' $treatmentlist , vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
	estadd ysumm
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	sum  taken_`time' if TreatmentGains==0 & TreatmentLevels==0
	estadd scalar ymean2=r(mean)
}


label var TreatmentLevels "Levels (\$\beta_1\$)"
label var TreatmentGains "P4Pctile (\$\beta_2\$)"

esttab  using "$latexcodes/RegTestTakersTWA.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc a2) labels("N. of obs." "Mean control" "Gains-Levels \$(\beta_3) = \beta_2-\beta_1\$" "p-value (\$H_0:\beta_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)
restore

eststo clear
capture drop taken*
gen taken_T3=!missing(Z_hisabati_T3)
gen taken_T6=!missing(Z_hisabati_T6)

foreach time in T3 T6{
	eststo:  reghdfe taken_`time' $treatmentlist , vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
	estadd ysumm
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	sum  taken_`time' if TreatmentGains==0 & TreatmentLevels==0
	estadd scalar ymean2=r(mean)
}


label var TreatmentLevels "Levels (\$\beta_1\$)"
label var TreatmentGains "P4Pctile (\$\beta_2\$)"

esttab  using "$latexcodes/RegTestTakersPropTWA.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc a2) labels("N. of obs." "Mean control" "Gains-Levels \$(\beta_3) = \beta_2-\beta_1\$" "p-value (\$H_0:\beta_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)


***********************************************
***********************************************
********** Difference High Low ********************
***********************************************
***********************************************

reghdfe Z_hisabati_T2 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T2<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) 
reghdfe Z_hisabati_T3 $treatmentlist i.Grade_T3 ${schoolcontrol} , vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm) 	

eststo clear
foreach var in $AggregateDep_NonEng Z_ScoreKisawMath{
		capture drop resid_T2
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T2  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T2==3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T2)
		}
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T2  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T2<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T2)
		}
		replace resid_T2=. if e(sample)==0
		*predict resid_T2 if e(sample)==1,resid
		preserve
		tempfile file1
		drop if e(sample)==0
		gen time=0
		rename resid_T2 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file1'
		restore
		
		capture drop resid_T3
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T3 i.Grade_T3 ${schoolcontrol} if Grade_T3<=3 , vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm) resid(resid_T3)
		}
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T3 i.Grade_T3 ${schoolcontrol} if Grade_T3==3, vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm) resid(resid_T3)
		}
		replace resid_T3=. if e(sample)==0
		*predict resid_T3 if e(sample)==1,resid
		
		preserve
		tempfile file2
		drop if e(sample)==0
		gen time=1
		rename resid_T3 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file2'
		restore
		
		preserve
		clear
		use `file1'
		append using `file2'
		rename TreatmentLevels TreatmentLevels2
		rename TreatmentGains TreatmentGains2

		
		eststo `var'_yr1:  reghdfe resid c.(TreatmentLevels2 TreatmentGains2)##c.time, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm##time)
		test (_b[c.TreatmentLevels2#c.time]=0)
		estadd scalar std_err1=r(p)
		estadd scalar suma1=_b[c.TreatmentLevels2#c.time]
		
		test (_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err2=r(p)
		estadd scalar suma2=_b[c.TreatmentGains2#c.time]
		
		test (_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err3=r(p)
		estadd scalar suma3=_b[c.TreatmentGains2#c.time]- _b[c.TreatmentLevels2#c.time]
		
		
		test (_b[c.TreatmentLevels2#c.time]=_b[c.TreatmentGains2#c.time])
		estadd scalar std_err4=r(p)
		estadd scalar suma4=_b[c.TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]
		
		test (_b[c.TreatmentLevels2#c.time]=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err5=r(p)
		estadd scalar suma5=_b[c.TreatmentLevels2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err6=r(p)
		estadd scalar suma6=_b[c.TreatmentGains2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentLevels2#c.time=c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err7=r(p)

		
		
		restore
		
		
		capture drop resid_T5
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T5  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T5<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T5)
		}
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T5  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T5==3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T5)
		}
		replace resid_T5=. if e(sample)==0
		*predict resid_T5 if e(sample)==1,resid 
		preserve
		tempfile file1
		drop if e(sample)==0
		gen time=0
		rename resid_T5 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file1'
		restore
		
		capture drop resid_T6
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T6 i.Grade_T6 ${schoolcontrol} if Grade_T6<=3 , vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T6)
		}
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T6 i.Grade_T6 ${schoolcontrol} if Grade_T6==3 , vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T6)
		}
		replace resid_T6=. if e(sample)==0
		*predict resid_T6 if e(sample)==1,resid
		
		preserve
		tempfile file2
		drop if e(sample)==0
		gen time=1
		rename resid_T6 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file2'
		restore
		
		preserve
		clear
		use `file1'
		append using `file2'
		rename TreatmentLevels TreatmentLevels2
		rename TreatmentGains TreatmentGains2

		
		eststo `var'_yr2:  reghdfe resid c.(TreatmentLevels2 TreatmentGains2)##c.time, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm##time)
		test (_b[c.TreatmentLevels2#c.time]=0)
		estadd scalar std_err1=r(p)
		estadd scalar suma1=_b[c.TreatmentLevels2#c.time]
		
		test (_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err2=r(p)
		estadd scalar suma2=_b[c.TreatmentGains2#c.time]
		
		test (_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err3=r(p)
		estadd scalar suma3=_b[c.TreatmentGains2#c.time]- _b[c.TreatmentLevels2#c.time]
		
		
		test (_b[c.TreatmentLevels2#c.time]=_b[c.TreatmentGains2#c.time])
		estadd scalar std_err4=r(p)
		estadd scalar suma4=_b[c.TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]
		
		test (_b[c.TreatmentLevels2#c.time]=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err5=r(p)
		estadd scalar suma5=_b[c.TreatmentLevels2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err6=r(p)
		estadd scalar suma6=_b[c.TreatmentGains2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentLevels2#c.time=c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err7=r(p)

		restore
		
}


esttab *_yr1 *_yr2 using "$latexcodes/RegTestScores_Difference.tex", se ar2 label nonumb /// 
replace  b(%9.2gc)se(%9.2gc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentLevels2#c.time TreatmentLevels  c.TreatmentGains2#c.time TreatmentGains) keep( )  ///
coeflabel(TreatmentLevels "Levels \$(\beta_1-\alpha_1\$)"  TreatmentGains "Gains \$(\beta_2-\alpha_2\$)") ///
stats(suma1 std_err1 suma2 std_err2  suma3 std_err3 std_err7, ///
fmt(%9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc) ///
labels("\$\gamma_1=\beta_1-\alpha_1\$" "p-value(\$\gamma_1=0 \$)" ///
"\$\gamma_2=\beta_2-\alpha_2\$" "p-value(\$\gamma_2=0\$)" ///
"\$\gamma_3=\beta_3-\alpha_3\$" "p-value(\$\gamma_3=0 \$)" ///
"p-value(\$\gamma_1=\gamma_2=\gamma_3\$)" ///
)) ///
nonotes substitute(\_ _)


esttab *_yr1 *_yr2 using "$latexcodes/RegTestScores_Difference_More.tex", se ar2 label nonumb /// 
replace  b(%9.2gc)se(%9.2gc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentLevels2#c.time TreatmentLevels  c.TreatmentGains2#c.time TreatmentGains) keep( )  ///
coeflabel(TreatmentLevels "Levels \$(\beta_1-\alpha_1\$)"  TreatmentGains "Gains \$(\beta_2-\alpha_2\$)") ///
stats(suma1 std_err1 suma2 std_err2  suma3 std_err3 suma4 std_err4 suma5 std_err5  suma6 std_err6 std_err7, ///
fmt(%9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc) labels("\$\gamma_1=\beta_1-\alpha_1\$" "p-value(\$\gamma_1=0 \$)" ///
"\$\gamma_2=\beta_2-\alpha_2\$" "p-value(\$\gamma_2=0\$)" "\$\gamma_3=\beta_3-\alpha_3\$" "p-value(\$\gamma_3=0 \$)" ///
"\$\gamma_1-\gamma_2\$" "p-value(\$\gamma_1-\gamma_2=0 \$)" ///
"\$\gamma_1-\gamma_3\$" "p-value(\$\gamma_1-\gamma_3=0 \$)" ///
"\$\gamma_2-\gamma_3\$" "p-value(\$\gamma_2-\gamma_3=0 \$)" ///
"p-value(\$\gamma_1=\gamma_2=\gamma_3\$)" ///
)) ///
nonotes substitute(\_ _)







***********************************************
***********************************************
********** Difference High Low -- no controls ********************
***********************************************
***********************************************


eststo clear
foreach var in $AggregateDep_NonEng Z_ScoreKisawMath{
		capture drop resid_T2
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T2  i.LagGrade $studentcontrol  if GradeID_T2==3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T2)
		}
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T2  i.LagGrade $studentcontrol  if GradeID_T2<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T2)
		}
		replace resid_T2=. if e(sample)==0
		*predict resid_T2 if e(sample)==1,resid
		preserve
		tempfile file1
		drop if e(sample)==0
		gen time=0
		rename resid_T2 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file1'
		restore
		
		capture drop resid_T3
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T3 i.Grade_T3  if Grade_T3<=3 , vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm) resid(resid_T3)
		}
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T3 i.Grade_T3  if Grade_T3==3, vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm) resid(resid_T3)
		}
		replace resid_T3=. if e(sample)==0
		*predict resid_T3 if e(sample)==1,resid
		
		preserve
		tempfile file2
		drop if e(sample)==0
		gen time=1
		rename resid_T3 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file2'
		restore
		
		preserve
		clear
		use `file1'
		append using `file2'
		rename TreatmentLevels TreatmentLevels2
		rename TreatmentGains TreatmentGains2

		
		eststo `var'_yr1:  reghdfe resid c.(TreatmentLevels2 TreatmentGains2)##c.time, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm##time)
		test (_b[c.TreatmentLevels2#c.time]=0)
		estadd scalar std_err1=r(p)
		estadd scalar suma1=_b[c.TreatmentLevels2#c.time]
		
		test (_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err2=r(p)
		estadd scalar suma2=_b[c.TreatmentGains2#c.time]
		
		test (_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err3=r(p)
		estadd scalar suma3=_b[c.TreatmentGains2#c.time]- _b[c.TreatmentLevels2#c.time]
		
		
		test (_b[c.TreatmentLevels2#c.time]=_b[c.TreatmentGains2#c.time])
		estadd scalar std_err4=r(p)
		estadd scalar suma4=_b[c.TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]
		
		test (_b[c.TreatmentLevels2#c.time]=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err5=r(p)
		estadd scalar suma5=_b[c.TreatmentLevels2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err6=r(p)
		estadd scalar suma6=_b[c.TreatmentGains2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentLevels2#c.time=c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err7=r(p)

		
		
		restore
		
		
		capture drop resid_T5
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T5  i.LagGrade $studentcontrol  if GradeID_T5<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T5)
		}
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T5  i.LagGrade $studentcontrol if GradeID_T5==3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T5)
		}
		replace resid_T5=. if e(sample)==0
		*predict resid_T5 if e(sample)==1,resid 
		preserve
		tempfile file1
		drop if e(sample)==0
		gen time=0
		rename resid_T5 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file1'
		restore
		
		capture drop resid_T6
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T6 i.Grade_T6 if Grade_T6<=3 , vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T6)
		}
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T6 i.Grade_T6 if Grade_T6==3 , vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T6)
		}
		replace resid_T6=. if e(sample)==0
		*predict resid_T6 if e(sample)==1,resid
		
		preserve
		tempfile file2
		drop if e(sample)==0
		gen time=1
		rename resid_T6 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file2'
		restore
		
		preserve
		clear
		use `file1'
		append using `file2'
		rename TreatmentLevels TreatmentLevels2
		rename TreatmentGains TreatmentGains2

		
		eststo `var'_yr2:  reghdfe resid c.(TreatmentLevels2 TreatmentGains2)##c.time, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm##time)
		test (_b[c.TreatmentLevels2#c.time]=0)
		estadd scalar std_err1=r(p)
		estadd scalar suma1=_b[c.TreatmentLevels2#c.time]
		
		test (_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err2=r(p)
		estadd scalar suma2=_b[c.TreatmentGains2#c.time]
		
		test (_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err3=r(p)
		estadd scalar suma3=_b[c.TreatmentGains2#c.time]- _b[c.TreatmentLevels2#c.time]
		
		
		test (_b[c.TreatmentLevels2#c.time]=_b[c.TreatmentGains2#c.time])
		estadd scalar std_err4=r(p)
		estadd scalar suma4=_b[c.TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]
		
		test (_b[c.TreatmentLevels2#c.time]=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err5=r(p)
		estadd scalar suma5=_b[c.TreatmentLevels2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err6=r(p)
		estadd scalar suma6=_b[c.TreatmentGains2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentLevels2#c.time=c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err7=r(p)

		restore
		
}


esttab *_yr1 *_yr2 using "$latexcodes/RegTestScores_Difference_nocontrol.tex", se ar2 label nonumb /// 
replace  b(%9.2gc)se(%9.2gc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentLevels2#c.time TreatmentLevels  c.TreatmentGains2#c.time TreatmentGains) keep( )  ///
coeflabel(TreatmentLevels "Levels \$(\beta_1-\alpha_1\$)"  TreatmentGains "Gains \$(\beta_2-\alpha_2\$)") ///
stats(suma1 std_err1 suma2 std_err2  suma3 std_err3 std_err7, ///
fmt(%9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc) ///
labels("\$\gamma_1=\beta_1-\alpha_1\$" "p-value(\$\gamma_1=0 \$)" ///
"\$\gamma_2=\beta_2-\alpha_2\$" "p-value(\$\gamma_2=0\$)" ///
"\$\gamma_3=\beta_3-\alpha_3\$" "p-value(\$\gamma_3=0 \$)" ///
"p-value(\$\gamma_1=\gamma_2=\gamma_3\$)" ///
)) ///
nonotes substitute(\_ _)


esttab *_yr1 *_yr2 using "$latexcodes/RegTestScores_Difference_More_nocontrol.tex", se ar2 label nonumb /// 
replace  b(%9.2gc)se(%9.2gc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentLevels2#c.time TreatmentLevels  c.TreatmentGains2#c.time TreatmentGains) keep( )  ///
coeflabel(TreatmentLevels "Levels \$(\beta_1-\alpha_1\$)"  TreatmentGains "Gains \$(\beta_2-\alpha_2\$)") ///
stats(suma1 std_err1 suma2 std_err2  suma3 std_err3 suma4 std_err4 suma5 std_err5  suma6 std_err6 std_err7, ///
fmt(%9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc) labels("\$\gamma_1=\beta_1-\alpha_1\$" "p-value(\$\gamma_1=0 \$)" ///
"\$\gamma_2=\beta_2-\alpha_2\$" "p-value(\$\gamma_2=0\$)" "\$\gamma_3=\beta_3-\alpha_3\$" "p-value(\$\gamma_3=0 \$)" ///
"\$\gamma_1-\gamma_2\$" "p-value(\$\gamma_1-\gamma_2=0 \$)" ///
"\$\gamma_1-\gamma_3\$" "p-value(\$\gamma_1-\gamma_3=0 \$)" ///
"\$\gamma_2-\gamma_3\$" "p-value(\$\gamma_2-\gamma_3=0 \$)" ///
"p-value(\$\gamma_1=\gamma_2=\gamma_3\$)" ///
)) ///
nonotes substitute(\_ _)

***********************************************
***********************************************
********** Difference High Low ---English ********************
***********************************************
***********************************************


eststo clear
foreach var in Z_kiingereza{
		capture drop resid_T2
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T2  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T2==3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T2)
		}
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T2  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T2<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T2)
		}
		replace resid_T2=. if e(sample)==0
		*predict resid_T2 if e(sample)==1,resid
		preserve
		tempfile file1
		drop if e(sample)==0
		gen time=0
		rename resid_T2 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file1'
		restore
		
		capture drop resid_T3
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T3 i.Grade_T3 ${schoolcontrol} if Grade_T3<=3 , vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm) resid(resid_T3)
		}
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T3 i.Grade_T3 ${schoolcontrol} if Grade_T3==3, vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm) resid(resid_T3)
		}
		replace resid_T3=. if e(sample)==0
		*predict resid_T3 if e(sample)==1,resid
		
		preserve
		tempfile file2
		drop if e(sample)==0
		gen time=1
		rename resid_T3 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file2'
		restore
		
		preserve
		clear
		use `file1'
		append using `file2'
		rename TreatmentLevels TreatmentLevels2
		rename TreatmentGains TreatmentGains2

		
		eststo `var'_yr1:  reghdfe resid c.(TreatmentLevels2 TreatmentGains2)##c.time, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm##time)
		test (_b[c.TreatmentLevels2#c.time]=0)
		estadd scalar std_err1=r(p)
		estadd scalar suma1=_b[c.TreatmentLevels2#c.time]
		
		test (_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err2=r(p)
		estadd scalar suma2=_b[c.TreatmentGains2#c.time]
		
		test (_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err3=r(p)
		estadd scalar suma3=_b[c.TreatmentGains2#c.time]- _b[c.TreatmentLevels2#c.time]
		
		test (_b[c.TreatmentLevels2#c.time]=_b[c.TreatmentGains2#c.time])
		estadd scalar std_err4=r(p)
		estadd scalar suma4=_b[c.TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]
		
		test (_b[c.TreatmentLevels2#c.time]=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err5=r(p)
		estadd scalar suma5=_b[c.TreatmentLevels2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err6=r(p)
		estadd scalar suma6=_b[c.TreatmentGains2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentLevels2#c.time=c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err7=r(p)
		
		
		restore
		
		
		capture drop resid_T5
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T5  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T5<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T5)
		}
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T5  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T5==3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T5)
		}
		replace resid_T5=. if e(sample)==0
		*predict resid_T5 if e(sample)==1,resid 
		preserve
		tempfile file1
		drop if e(sample)==0
		gen time=0
		rename resid_T5 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file1'
		restore
		
		capture drop resid_T6
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T6 i.Grade_T6 ${schoolcontrol} if Grade_T6<=3 , vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T6)
		}
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T6 i.Grade_T6 ${schoolcontrol} if Grade_T6==3 , vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T6)
		}
		replace resid_T6=. if e(sample)==0
		*predict resid_T6 if e(sample)==1,resid
		
		preserve
		tempfile file2
		drop if e(sample)==0
		gen time=1
		rename resid_T6 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file2'
		restore
		
		preserve
		clear
		use `file1'
		append using `file2'
		rename TreatmentLevels TreatmentLevels2
		rename TreatmentGains TreatmentGains2

		
		eststo `var'_yr2:  reghdfe resid c.(TreatmentLevels2 TreatmentGains2)##c.time, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm##time)
		test (_b[c.TreatmentLevels2#c.time]=0)
		estadd scalar std_err1=r(p)
		estadd scalar suma1=_b[c.TreatmentLevels2#c.time]
		
		test (_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err2=r(p)
		estadd scalar suma2=_b[c.TreatmentGains2#c.time]
		
		test (_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err3=r(p)
		estadd scalar suma3=_b[c.TreatmentGains2#c.time]- _b[c.TreatmentLevels2#c.time]
		
		test (_b[c.TreatmentLevels2#c.time]=_b[c.TreatmentGains2#c.time])
		estadd scalar std_err4=r(p)
		estadd scalar suma4=_b[c.TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]
		
		test (_b[c.TreatmentLevels2#c.time]=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err5=r(p)
		estadd scalar suma5=_b[c.TreatmentLevels2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err6=r(p)
		estadd scalar suma6=_b[c.TreatmentGains2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentLevels2#c.time=c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err7=r(p)
		
		restore
		
}


esttab *_yr1 *_yr2 using "$latexcodes/RegTestScores_Difference_English.tex", se ar2 label nonumb /// 
replace  b(%9.2gc)se(%9.2gc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentLevels2#c.time TreatmentLevels  c.TreatmentGains2#c.time TreatmentGains) keep( )  ///
coeflabel(TreatmentLevels "Levels \$(\beta_1-\alpha_1\$)"  TreatmentGains "Gains \$(\beta_2-\alpha_2\$)") ///
stats(suma1 std_err1 suma2 std_err2  suma3 std_err3, fmt(%9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc) labels("\$\beta_1-\alpha_1\$" "p-value(\$\beta_1-\alpha_1=0 \$)" "\$\beta_2-\alpha_2\$" "p-value(\$\beta_2-\alpha_2=0\$)" "\$\beta_3-\alpha_3\$" "p-value( \$\beta_3-\alpha_3=0 \$)")) ///
nonotes substitute(\_ _)


esttab *_yr1 *_yr2 using "$latexcodes/RegTestScores_Difference_English_More.tex", se ar2 label nonumb /// 
replace  b(%9.2gc)se(%9.2gc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentLevels2#c.time TreatmentLevels  c.TreatmentGains2#c.time TreatmentGains) keep( )  ///
coeflabel(TreatmentLevels "Levels \$(\beta_1-\alpha_1\$)"  TreatmentGains "Gains \$(\beta_2-\alpha_2\$)") ///
stats(suma1 std_err1 suma2 std_err2  suma3 std_err3 suma4 std_err4 suma5 std_err5  suma6 std_err6 std_err7, ///
fmt(%9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc) labels("\$\gamma_1=\beta_1-\alpha_1\$" "p-value(\$\gamma_1=0 \$)" ///
"\$\gamma_2=\beta_2-\alpha_2\$" "p-value(\$\gamma_2=0\$)" "\$\gamma_3=\beta_3-\alpha_3\$" "p-value(\$\gamma_3=0 \$)" ///
"\$\gamma_1-\gamma_2\$" "p-value(\$\gamma_1-\gamma_2=0 \$)" ///
"\$\gamma_1-\gamma_3\$" "p-value(\$\gamma_1-\gamma_3=0 \$)" ///
"\$\gamma_2-\gamma_3\$" "p-value(\$\gamma_2-\gamma_3=0 \$)" ///
"p-value(\$\gamma_1=\gamma_2=\gamma_3\$)" ///
)) ///
nonotes substitute(\_ _)
***********************************************
***********************************************
********** Difference High Low --  NO GRD 1 ********************
***********************************************
***********************************************

reghdfe Z_hisabati_T2 $treatmentlist i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T2<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) 
reghdfe Z_hisabati_T3 $treatmentlist i.Grade_T3 ${schoolcontrol} , vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm) 	

eststo clear
foreach var in $AggregateDep_NonEng Z_ScoreKisawMath{
		capture drop resid_T2
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T2  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T2==3 & GradeID_T2!=1, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T2)
		}
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T2  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T2<=3 & GradeID_T2!=1, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T2)
		}
		replace resid_T2=. if e(sample)==0
		*predict resid_T2 if e(sample)==1,resid
		preserve
		tempfile file1
		drop if e(sample)==0
		gen time=0
		rename resid_T2 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file1'
		restore
		
		capture drop resid_T3
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T3 i.Grade_T3 ${schoolcontrol} if Grade_T3<=3 & Grade_T3!=1, vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm) resid(resid_T3)
		}
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T3 i.Grade_T3 ${schoolcontrol} if Grade_T3==3 & Grade_T3!=1, vce(cluster SchoolID)  a(DistrictID##StrataScore##treatarm) resid(resid_T3)
		}
		replace resid_T3=. if e(sample)==0
		*predict resid_T3 if e(sample)==1,resid
		
		preserve
		tempfile file2
		drop if e(sample)==0
		gen time=1
		rename resid_T3 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file2'
		restore
		
		preserve
		clear
		use `file1'
		append using `file2'
		rename TreatmentLevels TreatmentLevels2
		rename TreatmentGains TreatmentGains2

		
		eststo `var'_yr1:  reghdfe resid c.(TreatmentLevels2 TreatmentGains2)##c.time, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm##time)
		test (_b[c.TreatmentLevels2#c.time]=0)
		estadd scalar std_err1=r(p)
		estadd scalar suma1=_b[c.TreatmentLevels2#c.time]
		
		test (_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err2=r(p)
		estadd scalar suma2=_b[c.TreatmentGains2#c.time]
		
		test (_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err3=r(p)
		estadd scalar suma3=_b[c.TreatmentGains2#c.time]- _b[c.TreatmentLevels2#c.time]
		
		test (_b[c.TreatmentLevels2#c.time]=_b[c.TreatmentGains2#c.time])
		estadd scalar std_err4=r(p)
		estadd scalar suma4=_b[c.TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]
		
		test (_b[c.TreatmentLevels2#c.time]=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err5=r(p)
		estadd scalar suma5=_b[c.TreatmentLevels2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err6=r(p)
		estadd scalar suma6=_b[c.TreatmentGains2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentLevels2#c.time=c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err7=r(p)
		
		restore
		
		
		capture drop resid_T5
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T5  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T5<=3 & GradeID_T5!=1, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T5)
		}
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T5  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T5==3 & GradeID_T5!=1, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T5)
		}
		replace resid_T5=. if e(sample)==0
		*predict resid_T5 if e(sample)==1,resid 
		preserve
		tempfile file1
		drop if e(sample)==0
		gen time=0
		rename resid_T5 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file1'
		restore
		
		capture drop resid_T6
		if "`var'"!="Z_kiingereza"{
			reghdfe `var'_T6 i.Grade_T6 ${schoolcontrol} if Grade_T6<=3 & Grade_T6!=1, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T6)
		}
		if "`var'"=="Z_kiingereza"{
			reghdfe `var'_T6 i.Grade_T6 ${schoolcontrol} if Grade_T6==3 & Grade_T6!=1, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm) resid(resid_T6)
		}
		replace resid_T6=. if e(sample)==0
		*predict resid_T6 if e(sample)==1,resid
		
		preserve
		tempfile file2
		drop if e(sample)==0
		gen time=1
		rename resid_T6 resid
		keep resid SchoolID  $treatmentlist time DistrictID StrataScore treatarm
		save `file2'
		restore
		
		preserve
		clear
		use `file1'
		append using `file2'
		rename TreatmentLevels TreatmentLevels2
		rename TreatmentGains TreatmentGains2

		
		eststo `var'_yr2:  reghdfe resid c.(TreatmentLevels2 TreatmentGains2)##c.time, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm##time)
		test (_b[c.TreatmentLevels2#c.time]=0)
		estadd scalar std_err1=r(p)
		estadd scalar suma1=_b[c.TreatmentLevels2#c.time]
		
		test (_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err2=r(p)
		estadd scalar suma2=_b[c.TreatmentGains2#c.time]
		
		test (_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]=0)
		estadd scalar std_err3=r(p)
		estadd scalar suma3=_b[c.TreatmentGains2#c.time]- _b[c.TreatmentLevels2#c.time]
		
		test (_b[c.TreatmentLevels2#c.time]=_b[c.TreatmentGains2#c.time])
		estadd scalar std_err4=r(p)
		estadd scalar suma4=_b[c.TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]
		
		test (_b[c.TreatmentLevels2#c.time]=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err5=r(p)
		estadd scalar suma5=_b[c.TreatmentLevels2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err6=r(p)
		estadd scalar suma6=_b[c.TreatmentGains2#c.time]-(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time])
		
		test (c.TreatmentLevels2#c.time=c.TreatmentGains2#c.time=(_b[TreatmentLevels2#c.time]-_b[c.TreatmentGains2#c.time]))
		estadd scalar std_err7=r(p)
		
		restore
		
}


esttab *_yr1 *_yr2 using "$latexcodes/RegTestScores_Difference_NOGRD1.tex", se ar2 label nonumb /// 
replace  b(%9.2gc)se(%9.2gc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentLevels2#c.time TreatmentLevels  c.TreatmentGains2#c.time TreatmentGains) keep( )  ///
coeflabel(TreatmentLevels "Levels \$(\beta_1-\alpha_1\$)"  TreatmentGains "Gains \$(\beta_2-\alpha_2\$)") ///
stats(suma1 std_err1 suma2 std_err2  suma3 std_err3, fmt(%9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc) labels("\$\beta_1-\alpha_1\$" "p-value(\$\beta_1-\alpha_1=0 \$)" "\$\beta_2-\alpha_2\$" "p-value(\$\beta_2-\alpha_2=0\$)" "\$\beta_3-\alpha_3\$" "p-value( \$\beta_3-\alpha_3=0 \$)")) ///
nonotes substitute(\_ _)


esttab *_yr1 *_yr2 using "$latexcodes/RegTestScores_Difference_NOGRD1_More.tex", se ar2 label nonumb /// 
replace  b(%9.2gc)se(%9.2gc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentLevels2#c.time TreatmentLevels  c.TreatmentGains2#c.time TreatmentGains) keep( )  ///
coeflabel(TreatmentLevels "Levels \$(\beta_1-\alpha_1\$)"  TreatmentGains "Gains \$(\beta_2-\alpha_2\$)") ///
stats(suma1 std_err1 suma2 std_err2  suma3 std_err3 suma4 std_err4 suma5 std_err5  suma6 std_err6 std_err7, ///
fmt(%9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc %9.2gc) labels("\$\gamma_1=\beta_1-\alpha_1\$" "p-value(\$\gamma_1=0 \$)" ///
"\$\gamma_2=\beta_2-\alpha_2\$" "p-value(\$\gamma_2=0\$)" "\$\gamma_3=\beta_3-\alpha_3\$" "p-value(\$\gamma_3=0 \$)" ///
"\$\gamma_1-\gamma_2\$" "p-value(\$\gamma_1-\gamma_2=0 \$)" ///
"\$\gamma_1-\gamma_3\$" "p-value(\$\gamma_1-\gamma_3=0 \$)" ///
"\$\gamma_2-\gamma_3\$" "p-value(\$\gamma_2-\gamma_3=0 \$)" ///
"p-value(\$\gamma_1=\gamma_2=\gamma_3\$)" ///
)) ///
nonotes substitute(\_ _)




***********************************************
***********************************************
********** other outcomes ********************
***********************************************
***********************************************

label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"

eststo clear
foreach var in $studentoutcomes {
eststo: reghdfe `var' $treatmentlist i.LagGrade  if GradeID_T2!=4, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
estadd ysumm
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
}
esttab using "$latexcodes/RegStudentOutcomes1.tex", se ar2 booktabs label  /// 
replace title("Treatment effect on standardized test scores for `name'")   b(%9.2gc) se(%9.2gc) nocon  ///
keep(TreatmentGains TreatmentLevels) stats(N suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

