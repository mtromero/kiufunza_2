*Document folder where all KF things are saved
clear all
cd "C:/Users/mauri/Box Sync/01_KiuFunza/"
global dirdo "C:/Users/mauri/Documents/git/kiufunza_2/Payments/Yr2"

*cd "/media/mauricio/TeraHDD2/Box Sync/01_KiuFunza/"
*cd "/media/mauricio/TeraHDD2/git/kiufunza_2/Payments/Yr2"

*Install tabmiss for data checks
*findit tabmiss
scalar drop _all
scalar define TotalBudget=2*75000*2200*5/6
*the 5/6 is because im calculating teachers only first

do "$dirdo/01_Budget_Calc.do"
do "$dirdo/02_Mash_Yr2.do"
do "$dirdo/03_Stadi_Yr2.do"
do "$dirdo/04_CheckBudget.do"
do "$dirdo/05_RandomChecks.do"


