use "RawData\4 Intervention\TwaEL_2014\TwaTestData_2014_allstudents.dta", clear
keep Kis_SI_el- Math_Pass_el StuID_15
destring StuID_15, replace
save "CreatedData\4 Intervention\TwaEL_2014\TwaTestData_2014_allstudents_temp.dta", replace

use "CreatedData/4 Intervention/TwaEL_2015/EL2015_Student_FinalRun_Mashindano_CLEAN.dta", clear
drop  district   gender set start startampm volname end endampm
replace stream = lower(stream)
rename schoolid SchoolID
rename studentid studentID
replace studentID=. if studentID==-999
replace studentID=. if studentID==-99
replace studentID=. if studentID==999
replace studentID=. if studentID==99

merge m:1 studentID using "CreatedData/4 Intervention/TwaEL_2015/2015_GroupsStudent_Mash_Grade23.dta", keepus( KisLevel1 EngLevel1 MathLevel1)
drop if _merge==2
drop _merge

ds, has(type string)
foreach var of varlist `r(varlist)'{
replace `var'="" if `var'=="--blank--" | `var'=="X"
}

destring , replace

drop if grade!=2

foreach var of varlist SchoolID grade stream{
	qui tabmiss `var'
	display "`r(sum)' missing in variable `var'"
	display "They are dropped"
	drop if	missing(`var')
}
merge 1:1 name using "CreatedData/4 Intervention/TwaEL_2015/CorrectStream.dta", update replace
drop if _merge==2
drop _merge

/*
bys  SchoolID grade stream: gen Rand=uniform()
replace stream="a" if  SchoolID==120 & grade==2 & stream=="c" & Rand>=0.5
replace stream="b" if  SchoolID==120 & grade==2 & stream=="c" & Rand<0.5
replace stream="a" if  SchoolID==120 & grade==2 & stream=="d" & Rand>=0.5
replace stream="b" if  SchoolID==120 & grade==2 & stream=="d" & Rand<0.5

replace stream="a" if  SchoolID==121 & grade==2 & stream=="d" & Rand<0.33
replace stream="b" if  SchoolID==121 & grade==2 & stream=="d" & Rand>=0.5 & Rand<0.66
replace stream="c" if  SchoolID==121 & grade==2 & stream=="d" & Rand>=0.66
drop Rand
*/

replace stream="a" if  SchoolID==218 & grade==2 & stream=="b"
replace stream="a" if  SchoolID==306 & grade==2 & stream=="c"
replace stream="a" if  SchoolID==623 & grade==2 & stream=="c"
replace stream="a" if  SchoolID==736 & grade==2 & stream=="c"
replace stream="a" if  SchoolID==829 & grade==2 & stream=="b"


foreach skill in ma se{
	qui tabmiss grd2_kis_a_`skill'_1
	display "Skills `skill' in Swahili/Qs1 has `r(sum)' missing"
	qui tabmiss grd2_kis_a_`skill'_1
	display "Skills `skill' in Swahili/Qs2 has `r(sum)' missing"
}

foreach skill in w se{
	qui tabmiss grd2_eng_a_`skill'_1
	display "Skills `skill' in English/Qs1 has `r(sum)' missing"
	qui tabmiss grd2_eng_a_`skill'_1
	display "Skills `skill' in English/Qs2 has `r(sum)' missing"
}

foreach skill in  bwa j t z{
	qui tabmiss grd2_his_a_`skill'_1
	display "Skills `skill' in Math/Qs1 has `r(sum)' missing"
	qui tabmiss grd2_his_a_`skill'_1
	display "Skills `skill' in Math/Qs2 has `r(sum)' missing"
}


*Some missings are "weird" but I'm gonna assume students just didin't answer the question, i.e. it was wrong... but I do some more cleaning below
foreach var of varlist grd*{
	replace `var'=0 if `var'==.
}

*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in ma se{
	 forvalues i=3/6{
		replace grd2_kis_a_`skill'_`i'=0 if grd2_kis_a_`skill'_1==0 & grd2_kis_a_`skill'_2==0
	}
}

foreach skill in w se{
	forvalues i=3/6{
		replace grd2_eng_a_`skill'_`i'=0 if grd2_eng_a_`skill'_1==0 & grd2_eng_a_`skill'_2==0
	}
}

foreach skill in  bwa j t z{
	replace grd2_his_a_`skill'_3=0 if grd2_his_a_`skill'_1==0 & grd2_his_a_`skill'_2==0
} 


*Calculcate how many correct questions per skill */
egen Kis_Maneno=rowtotal(grd2_kis_a_ma_1 grd2_kis_a_ma_2 grd2_kis_a_ma_3 grd2_kis_a_ma_4 grd2_kis_a_ma_5 grd2_kis_a_ma_6)
egen Kis_Sentenci=rowtotal(grd2_kis_a_se_1 grd2_kis_a_se_2 grd2_kis_a_se_3 grd2_kis_a_se_4 grd2_kis_a_se_5 grd2_kis_a_se_6)
gen Kis_Aya=grd2_kis_a_a


egen Eng_Word=rowtotal(grd2_eng_a_w_1 grd2_eng_a_w_2 grd2_eng_a_w_3 grd2_eng_a_w_4 grd2_eng_a_w_5 grd2_eng_a_w_6)
egen Eng_Sentences=rowtotal(grd2_eng_a_se_1 grd2_eng_a_se_2 grd2_eng_a_se_3 grd2_eng_a_se_4 grd2_eng_a_se_5 grd2_eng_a_se_6)
gen Eng_Paragraph=grd2_eng_a_p


egen Math_bwa=rowtotal(grd2_his_a_bwa_1 grd2_his_a_bwa_2 grd2_his_a_bwa_3)
egen Math_j=rowtotal(grd2_his_a_j_1 grd2_his_a_j_2 grd2_his_a_j_3)
egen Math_t=rowtotal(grd2_his_a_t_1 grd2_his_a_t_2 grd2_his_a_t_3)
egen Math_z=rowtotal(grd2_his_a_z_1 grd2_his_a_z_2 grd2_his_a_z_3)


foreach var of varlist  Kis_Maneno Kis_Sentenci Eng_Word Eng_Sentences{
	gen `var'_Pass=`var'>=4 & !missing(`var')
}

foreach var of varlist  Math_bwa Math_j Math_t Math_z{
	gen `var'_Pass=`var'>=2 & !missing(`var')
}

foreach var of varlist  Kis_Aya Eng_Paragraph{
	gen `var'_Pass=`var'==3 & !missing(`var')
}

/*You only do b if you get no more than 1 question correct per section
I assume that if you dont do b, you would have gotten a 1 in every answer!
*/
/*You only do c if you get everything right beforehand
I assume that if you dont do c, you would have gotten everything incorrect!
*/
foreach var of varlist grd2_kis_b*{
	replace `var'=1 if Kis_Maneno>1 | Kis_Sentenci>1 | Kis_Aya>1
}

foreach var of varlist grd2_kis_c*{
	replace `var'=0 if Kis_Maneno<6 | Kis_Sentenci<6 | Kis_Aya<3
}

foreach var of varlist grd2_eng_b*{
	replace `var'=1 if Eng_Word>1 | Eng_Sentences>1 | Eng_Paragraph>1
}

foreach var of varlist grd2_eng_c*{
	replace `var'=0 if Eng_Word<6 | Eng_Sentences<6 | Eng_Paragraph<3
}
foreach var of varlist grd2_his_b*{
	replace `var'=1 if Math_bwa>1 | Math_j>1 | Math_t>1 | Math_z>1
} 
foreach var of varlist grd2_his_c*{
	replace `var'=0 if Math_bwa<3 | Math_j<3 | Math_t<3 | Math_z<3
} 

*Calculate some dummies for the reading questions. dummy_i means you got a score of at least i
foreach var of varlist grd2_*{
	qui sum `var'
	if(`r(max)'==2){
		gen `var'_1=(`var'>=1) & !missing(`var')
		gen `var'_2=(`var'>=2) & !missing(`var')
		drop `var'
	}
	if(`r(max)'==3){
		gen `var'_1=(`var'>=1) & !missing(`var')
		gen `var'_2=(`var'>=2) & !missing(`var')
		gen `var'_3=(`var'>=3) & !missing(`var')
		drop `var'
	}
}



/*Drop questions with all 0 or all 1 since they provide no useful variation*/
foreach var of varlist grd2_*{
	qui sum `var'
	if(`r(sd)'==0) {
		drop `var'
	}
	if(`r(sd)'!=0) {
		egen `var'_Z=std(`var')  
	}
}

*Do PCA to rank students *
pca grd2_kis_*_Z
predict Z_kis, score 
pca grd2_eng_*_Z
predict Z_eng, score 
pca grd2_his_*_Z
predict Z_his, score 

*If  no pre_level, and therefore must assing one (which is the one for no pre-info)
replace KisLevel1=0 if KisLevel1==.
replace EngLevel1=0 if EngLevel1==.
replace MathLevel1=0 if MathLevel1==.

*Calculcate percentiles*
egen Rank_Kis = xtile(Z_kis), by(KisLevel1) nquantiles(100)
egen Rank_Eng = xtile(Z_eng), by(EngLevel1) nquantiles(100)
egen Rank_Math = xtile(Z_his), by(MathLevel1) nquantiles(100)

*Calulcate the total number of students by level
bys KisLevel1: egen StudentsInLevel_Kis=count(Rank_Kis) 
bys EngLevel1: egen StudentsInLevel_Eng=count(Rank_Eng) 
bys MathLevel1: egen StudentsInLevel_Math =count(Rank_Math ) 

*Calcuclate the number of points or rank-points per level
bys KisLevel1: egen TotalPoints_Kis=total(Rank_Kis-1) 
bys EngLevel1: egen TotalPoints_Eng=total(Rank_Eng-1) 
bys MathLevel1: egen TotalPoints_Math =total(Rank_Math-1) 

/*The budget per group is equal to the number of students divided by the total =scalar(BudgetGrd1)*(StudentsInLevel_Kis/_N)*/
/*Then we calculcate how much money we pay per point which is the the number above divided by total number of points */
/*We the calculcate how much each student gets, which is the rank of the student divided multiplied by the number above */
gen Payment_Total_Kis=(Rank_Kis-1)*(scalar(BudgetKis_Grd2_Mash)*(StudentsInLevel_Kis/_N))/TotalPoints_Kis
gen Payment_Total_Eng=(Rank_Eng-1)*(scalar(BudgetEng_Grd2_Mash)*(StudentsInLevel_Eng/_N))/TotalPoints_Eng
gen Payment_Total_Math=(Rank_Math-1)*(scalar(BudgetMath_Grd2_Mash)*(StudentsInLevel_Math/_N))/TotalPoints_Math

keep name- studentID treatarm2 KisLevel1 EngLevel1 MathLevel1 Kis_Maneno_Pass- Eng_Paragraph_Pass Payment_Total_Kis- Payment_Total_Math
gen StuID_15=studentID


merge m:1 StuID_15 using  "CreatedData\4 Intervention\TwaEL_2014\TwaTestData_2014_allstudents_temp.dta", keepus(Kis_SI_el- Math_Pass_el)
drop if _merge!=3

sort Payment_Total_Math

