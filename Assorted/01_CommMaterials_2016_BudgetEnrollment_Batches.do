

use  "RawData/12 Intervention_KFII/1 Endline 2015/AllStudentsTested_2015.dta", clear
*drop _merge
drop  studentid_merge set_merge stream_merge gender_merge FifthID updatedIDLength IDLength stream_missing2 name_numb pos_space 

rename districtid DistrictID
rename grade Grade
rename stream Stream

**Calculate enrollmente not EDI, but previous TWA
preserve
collapse (count) Enrollment=TestedEL2015, by(Grade treatment2 treatarm2)
drop if Grade==.
saveold "CreatedData/ConsolidatedYr34/EnroollmnetGradeTWA_2015.dta", replace
restore
***Back to main code
tabstat Grade, by(treatment2) statistics (count)
tabstat Grade if treatment2=="Gains", by(Grade) statistics (count)
tabstat Grade if treatment2=="Levels", by(Grade) statistics (count)


foreach var of varlist grd1*{
replace `var'=. if Grade!=1 | TestedEL2015==0
replace `var'=0 if `var'==. & Grade==1 & TestedEL2015==1
}

foreach var of varlist grd2*{
replace `var'=. if Grade!=2 | TestedEL2015==0
replace `var'=0 if `var'==. & Grade==2 & TestedEL2015==1
}

foreach var of varlist grd3*{
replace `var'=. if Grade!=3 | TestedEL2015==0
replace `var'=0 if `var'==. & Grade==3 & TestedEL2015==1
}


*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in si ma se{
	 forvalues i=3/6{
		replace grd1_kis_a_`skill'_`i'=0 if grd1_kis_a_`skill'_1==0 & grd1_kis_a_`skill'_2==0 & Grade==1 & TestedEL2015==1
	}
}

foreach skill in l w se{
	forvalues i=3/6{
		replace grd1_eng_a_`skill'_`i'=0 if grd1_eng_a_`skill'_1==0 & grd1_eng_a_`skill'_2==0 & Grade==1 & TestedEL2015==1
	}
}

foreach skill in id uta bwa j t{
	replace grd1_his_a_`skill'_3=0 if grd1_his_a_`skill'_1==0 & grd1_his_a_`skill'_2==0 & Grade==1 & TestedEL2015==1
} 


/*Total answers per skill*/
egen Kis_Silabi_Grd1=rowtotal(grd1_kis_a_si_1 grd1_kis_a_si_2 grd1_kis_a_si_3 grd1_kis_a_si_4 grd1_kis_a_si_5 grd1_kis_a_si_6) if Grade==1 & TestedEL2015==1, missing
egen Kis_Maneno_Grd1=rowtotal(grd1_kis_a_ma_1 grd1_kis_a_ma_2 grd1_kis_a_ma_3 grd1_kis_a_ma_4 grd1_kis_a_ma_5 grd1_kis_a_ma_6) if Grade==1 & TestedEL2015==1, missing
egen Kis_Sentenci_Grd1=rowtotal(grd1_kis_a_se_1 grd1_kis_a_se_2 grd1_kis_a_se_3 grd1_kis_a_se_4 grd1_kis_a_se_5 grd1_kis_a_se_6) if Grade==1 & TestedEL2015==1, missing


egen Eng_Letter_Grd1=rowtotal(grd1_eng_a_l_1 grd1_eng_a_l_2 grd1_eng_a_l_3 grd1_eng_a_l_4 grd1_eng_a_l_5 grd1_eng_a_l_6) if Grade==1 & TestedEL2015==1, missing
egen Eng_Word_Grd1=rowtotal(grd1_eng_a_w_1 grd1_eng_a_w_2 grd1_eng_a_w_3 grd1_eng_a_w_4 grd1_eng_a_w_5 grd1_eng_a_w_6) if Grade==1 & TestedEL2015==1, missing
egen Eng_Sentences_Grd1=rowtotal(grd1_eng_a_se_1 grd1_eng_a_se_2 grd1_eng_a_se_3 grd1_eng_a_se_4 grd1_eng_a_se_5 grd1_eng_a_se_6) if Grade==1 & TestedEL2015==1, missing


egen Math_id_Grd1=rowtotal(grd1_his_a_id_1 grd1_his_a_id_2 grd1_his_a_id_3) if Grade==1 & TestedEL2015==1, missing
egen Math_uta_Grd1=rowtotal(grd1_his_a_uta_1 grd1_his_a_uta_2 grd1_his_a_uta_3) if Grade==1 & TestedEL2015==1, missing
egen Math_bwa_Grd1=rowtotal(grd1_his_a_bwa_1 grd1_his_a_bwa_2 grd1_his_a_bwa_3) if Grade==1 & TestedEL2015==1, missing
egen Math_j_Grd1=rowtotal(grd1_his_a_j_1 grd1_his_a_j_2 grd1_his_a_j_3) if Grade==1 & TestedEL2015==1, missing
egen Math_t_Grd1=rowtotal(grd1_his_a_t_1 grd1_his_a_t_2 grd1_his_a_t_3) if Grade==1 & TestedEL2015==1, missing

*Only goes to b section if NO more than 1 answer right in each section */
*Only go to c if everything is perfect! */
*Therefire if you didint go to b, I'm assuming you had all of these correct */
*If you didint go to C I'm assuming you would have gotten all those wrong!
foreach var of varlist grd1_kis_b*{
	replace `var'=1 if (Kis_Silabi_Grd1>1 | Kis_Maneno_Grd1>1 | Kis_Sentenci_Grd1>1) & Grade==1 & TestedEL2015==1
}

foreach var of varlist grd1_kis_c*{
	replace `var'=0 if (Kis_Silabi_Grd1<6 | Kis_Maneno_Grd1<6 | Kis_Sentenci_Grd1<6) & Grade==1 & TestedEL2015==1
}

foreach var of varlist grd1_eng_b*{
	replace `var'=1 if (Eng_Letter_Grd1>1 | Eng_Word_Grd1>1 | Eng_Sentences_Grd1>1) & Grade==1 & TestedEL2015==1
}

foreach var of varlist grd1_eng_c*{
	replace `var'=0 if (Eng_Letter_Grd1<6 | Eng_Word_Grd1<6 | Eng_Sentences_Grd1<6) & Grade==1 & TestedEL2015==1
}

foreach var of varlist grd1_his_c*{
	replace `var'=0 if (Math_id_Grd1<3 | Math_uta_Grd1<3 | Math_bwa_Grd1<3 | Math_j_Grd1<3 | Math_t_Grd1<3) & Grade==1 & TestedEL2015==1
} 


egen Kis_Picha_Grd1=rowtotal(grd1_kis_b_pi_*) if Grade==1 & TestedEL2015==1, missing
egen Kis_PichaLetter_Grd1=rowtotal(grd1_kis_b_ph_*) if Grade==1 & TestedEL2015==1, missing
egen Kis_Aya_Grd1=rowtotal(grd1_kis_c_a* ) if Grade==1 & TestedEL2015==1, missing

egen Eng_Picture_Grd1=rowtotal(grd1_eng_b_pi_*) if Grade==1 & TestedEL2015==1, missing
egen Eng_PictureLetter_Grd1=rowtotal(grd1_eng_b_pl_*) if Grade==1 & TestedEL2015==1, missing
gen Eng_Paragraph_Grd1=grd1_eng_c_p if Grade==1 & TestedEL2015==1


egen Math_z_Grd1=rowtotal(grd1_his_c_z_* ) if Grade==1 & TestedEL2015==1

*I create dummies for the different levels of reading. So dummy_2 is if you got a 2 or higher. Same for other numbers
foreach var of varlist grd1_*{
	qui sum `var' if Grade==1 & TestedEL2015==1
	if(`r(max)'==2){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==1 & TestedEL2015==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==1 & TestedEL2015==1
		drop `var'
	}
	if(`r(max)'==3){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==1 & TestedEL2015==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==1 & TestedEL2015==1
		gen `var'_3=(`var'>=3) if !missing(`var') & Grade==1 & TestedEL2015==1
		drop `var' 
	}
}







*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in ma se{
	 forvalues i=3/6{
		replace grd2_kis_a_`skill'_`i'=0 if grd2_kis_a_`skill'_1==0 & grd2_kis_a_`skill'_2==0 & Grade==2 & TestedEL2015==1
	} 
}

foreach skill in w se{
	forvalues i=3/6{
		replace grd2_eng_a_`skill'_`i'=0 if grd2_eng_a_`skill'_1==0 & grd2_eng_a_`skill'_2==0 & Grade==2 & TestedEL2015==1
	}
}

foreach skill in  bwa j t z{
	replace grd2_his_a_`skill'_3=0 if grd2_his_a_`skill'_1==0 & grd2_his_a_`skill'_2==0 & Grade==2 & TestedEL2015==1
} 



*Calculcate how many correct questions per skill */
egen Kis_Maneno_Grd2=rowtotal(grd2_kis_a_ma_1 grd2_kis_a_ma_2 grd2_kis_a_ma_3 grd2_kis_a_ma_4 grd2_kis_a_ma_5 grd2_kis_a_ma_6) if Grade==2 & TestedEL2015==1, missing
egen Kis_Sentenci_Grd2=rowtotal(grd2_kis_a_se_1 grd2_kis_a_se_2 grd2_kis_a_se_3 grd2_kis_a_se_4 grd2_kis_a_se_5 grd2_kis_a_se_6) if Grade==2 & TestedEL2015==1, missing
gen Kis_Aya_Grd2=grd2_kis_a_a if Grade==2& TestedEL2015==1


egen Eng_Word_Grd2=rowtotal(grd2_eng_a_w_1 grd2_eng_a_w_2 grd2_eng_a_w_3 grd2_eng_a_w_4 grd2_eng_a_w_5 grd2_eng_a_w_6) if Grade==2 & TestedEL2015==1, missing
egen Eng_Sentences_Grd2=rowtotal(grd2_eng_a_se_1 grd2_eng_a_se_2 grd2_eng_a_se_3 grd2_eng_a_se_4 grd2_eng_a_se_5 grd2_eng_a_se_6) if Grade==2 & TestedEL2015==1, missing
gen Eng_Paragraph_Grd2=grd2_eng_a_p if Grade==2 & TestedEL2015==1


egen Math_bwa_Grd2=rowtotal(grd2_his_a_bwa_1 grd2_his_a_bwa_2 grd2_his_a_bwa_3) if Grade==2 & TestedEL2015==1, missing
egen Math_j_Grd2=rowtotal(grd2_his_a_j_1 grd2_his_a_j_2 grd2_his_a_j_3) if Grade==2 & TestedEL2015==1, missing
egen Math_t_Grd2=rowtotal(grd2_his_a_t_1 grd2_his_a_t_2 grd2_his_a_t_3) if Grade==2 & TestedEL2015==1, missing
egen Math_z_Grd2=rowtotal(grd2_his_a_z_1 grd2_his_a_z_2 grd2_his_a_z_3) if Grade==2 & TestedEL2015==1, missing


/*You only do b if you get no more than 1 question correct per section
I assume that if you dont do b, you would have gotten a 1 in every answer!
*/
/*You only do c if you get everything right beforehand
I assume that if you dont do c, you would have gotten everything incorrect!
*/
foreach var of varlist grd2_kis_b*{
	replace `var'=1 if (Kis_Maneno_Grd2>1 | Kis_Sentenci_Grd2>1 | Kis_Aya_Grd2>1) & Grade==2 & TestedEL2015==1
}

foreach var of varlist grd2_kis_c*{
	replace `var'=0 if (Kis_Maneno_Grd2<6 | Kis_Sentenci_Grd2<6 | Kis_Aya_Grd2<3) & Grade==2 & TestedEL2015==1
}

foreach var of varlist grd2_eng_b*{
	replace `var'=1 if (Eng_Word_Grd2>1 | Eng_Sentences_Grd2>1 | Eng_Paragraph_Grd2>1) & Grade==2 & TestedEL2015==1
}

foreach var of varlist grd2_eng_c*{
	replace `var'=0 if (Eng_Word_Grd2<6 | Eng_Sentences_Grd2<6 | Eng_Paragraph_Grd2<3) & Grade==2 & TestedEL2015==1
}
foreach var of varlist grd2_his_b*{
	replace `var'=1 if (Math_bwa_Grd2>1 | Math_j_Grd2>1 | Math_t_Grd2>1 | Math_z_Grd2>1) & Grade==2 & TestedEL2015==1
} 
foreach var of varlist grd2_his_c*{
	replace `var'=0 if (Math_bwa_Grd2<3 | Math_j_Grd2<3 | Math_t_Grd2<3 | Math_z_Grd2<3) & Grade==2 & TestedEL2015==1
} 


egen Kis_Picha_Grd2=rowtotal(grd2_kis_b_pi_*) if Grade==2 & TestedEL2015==1, missing
egen Kis_Silabi_Grd2=rowtotal(grd2_kis_b_si_*) if Grade==2 & TestedEL2015==1, missing
egen Kis_MaswaliAya_Grd2=rowtotal(grd2_kis_c_ma_* ) if Grade==2 & TestedEL2015==1, missing

egen Eng_Picture_Grd2=rowtotal(grd2_eng_b_pi_*) if Grade==2 & TestedEL2015==1, missing
egen Eng_Letter_Grd2=rowtotal(grd2_eng_b_l_*) if Grade==2 & TestedEL2015==1, missing
egen Eng_ParagraphComp_Grd2=rowtotal(grd2_eng_c_pc_* ) if Grade==2 & TestedEL2015==1, missing


egen Math_uta_Grd2=rowtotal(grd2_his_b_uta_* ) if Grade==2 & TestedEL2015==1, missing
egen Math_bwab_Grd2=rowtotal(grd2_his_b_bwa_* ) if Grade==2 & TestedEL2015==1, missing
egen Math_g_Grd2=rowtotal(grd2_his_c_g_* ) if Grade==2 & TestedEL2015==1, missing


*Calculate some dummies for the reading questions. dummy_i means you got a score of at least i
foreach var of varlist grd2_*{
	qui sum `var' if Grade==2 & TestedEL2015==1
	if(`r(max)'==2){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==2 & TestedEL2015==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==2 & TestedEL2015==1
		drop `var'
	}
	if(`r(max)'==3){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==2 & TestedEL2015==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==2 & TestedEL2015==1
		gen `var'_3=(`var'>=3) if !missing(`var') & Grade==2 & TestedEL2015==1
		drop `var'
	}
}




*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_kis_a_m_1=0 if grd3_kis_a_h==0  & Grade==3 & TestedEL2015==1
replace grd3_kis_a_m_2=0 if grd3_kis_a_h==0  & Grade==3 & TestedEL2015==1


*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_eng_a_c_1=0 if grd3_eng_a_s==0 & Grade==3 & TestedEL2015==1
replace grd3_eng_a_c_2=0 if grd3_eng_a_s==0 & Grade==3 & TestedEL2015==1

*First data cleaning process for math is as before...
foreach skill in  j t z g{
	replace grd3_his_a_`skill'_3=0 if grd3_his_a_`skill'_1==0 & grd3_his_a_`skill'_2==0 & Grade==3 & TestedEL2015==1
} 


gen Kis_Story_Grd3=grd3_kis_a_h if Grade==3
egen Kis_Comp_Grd3=rowtotal(grd3_kis_a_m_1 grd3_kis_a_m_2) if Grade==3 & TestedEL2015==1, missing
     
gen Eng_Story_Grd3=grd3_eng_a_s if Grade==3
egen Eng_Comp_Grd3=rowtotal(grd3_eng_a_c_1 grd3_eng_a_c_2) if Grade==3 & TestedEL2015==1, missing
 

egen Math_j_Grd3=rowtotal(grd3_his_a_j_1 grd3_his_a_j_2 grd3_his_a_j_3) if Grade==3 & TestedEL2015==1, missing
egen Math_t_Grd3=rowtotal(grd3_his_a_t_1 grd3_his_a_t_2 grd3_his_a_t_3) if Grade==3 & TestedEL2015==1, missing
egen Math_z_Grd3=rowtotal(grd3_his_a_z_1 grd3_his_a_z_2 grd3_his_a_z_3) if Grade==3 & TestedEL2015==1, missing
egen Math_g_Grd3=rowtotal(grd3_his_a_g_1 grd3_his_a_g_2 grd3_his_a_g_3) if Grade==3 & TestedEL2015==1, missing


/*You only do b if you get no more than 1 question correct per section
I assume that if you dont do b, you would have gotten a 1 in every answer!
*/
/*You only do c if you get everything right beforehand
I assume that if you dont do c, you would have gotten everything incorrect!
*/
foreach var of varlist grd3_kis_b*{
	replace `var'=1 if (Kis_Story_Grd3>1 | Kis_Comp_Grd3>1) & Grade==3 & TestedEL2015==1
}

foreach var of varlist grd3_kis_c*{
	replace `var'=0 if (Kis_Story_Grd3<3 | Kis_Comp_Grd3<2) & Grade==3 & TestedEL2015==1
}

foreach var of varlist grd3_eng_b*{
	replace `var'=1 if (Eng_Story_Grd3>1 | Eng_Comp_Grd3>1) & Grade==3 & TestedEL2015==1
}

foreach var of varlist grd3_eng_c*{
	replace `var'=0 if (Eng_Story_Grd3<3 | Eng_Comp_Grd3<2) & Grade==3 & TestedEL2015==1
}
foreach var of varlist grd3_his_b*{
	replace `var'=1 if  (Math_j_Grd3>1 | Math_t_Grd3>1 | Math_z_Grd3>1 | Math_g_Grd3>1) & Grade==3 & TestedEL2015==1
} 
foreach var of varlist grd3_his_c*{
	replace `var'=0 if  (Math_j_Grd3<3 | Math_t_Grd3<3 | Math_z_Grd3<3 | Math_g_Grd3<3) & Grade==3 & TestedEL2015==1
} 


egen Kis_Maneno_Grd3=rowtotal(grd3_kis_b_ma_*) if Grade==3 & TestedEL2015==1, missing
egen Kis_Sentenci_Grd3=rowtotal(grd3_kis_b_se_*) if Grade==3 & TestedEL2015==1, missing
gen Kis_Aya_Grd3=grd3_kis_b_a if Grade==3 & TestedEL2015==1

gen Kis_StoryAd_Grd3=grd3_kis_c_h if Grade==3 & TestedEL2015==1
egen Kis_ComPlus_Grd3=rowtotal(grd3_kis_c_m_*) if Grade==3 & TestedEL2015==1, missing


egen Eng_Word_Grd3=rowtotal(grd3_eng_b_w_*) if Grade==3 & TestedEL2015==1, missing
egen Eng_Sentences_Grd3=rowtotal(grd3_eng_b_se_*) if Grade==3 & TestedEL2015==1, missing
gen Eng_Paragraph_Grd3=grd3_eng_b_p if Grade==3 & TestedEL2015==1
gen Eng_StoryAd_Grd3=grd3_eng_c_s if Grade==3 & TestedEL2015==1
egen Eng_ComPlus_Grd3=rowtotal(grd3_eng_c_c_*) if Grade==3 & TestedEL2015==1, missing



egen Math_uta_Grd3=rowtotal(grd3_his_b_uta_*) if Grade==3 & TestedEL2015==1, missing
egen Math_bwa_Grd3=rowtotal(grd3_his_b_bwa_*) if Grade==3 & TestedEL2015==1, missing
egen Math_gAdv_Grd3=rowtotal(grd3_his_c_g_*) if Grade==3 & TestedEL2015==1, missing



*Calculate some dummies for the reading questions. dummy_i means you got a score of at least i
foreach var of varlist grd3_*{
	qui sum `var' if Grade==3 & TestedEL2015==1
	if(`r(max)'==2){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==3 & TestedEL2015==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==3 & TestedEL2015==1
		drop `var'
	}
	if(`r(max)'==3){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==3 & TestedEL2015==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==3 & TestedEL2015==1
		gen `var'_3=(`var'>=3) if !missing(`var') & Grade==3 & TestedEL2015==1
		drop `var'
	}
}




*generate passing dummies. Have to be careful since stata thinks missing is infinity
foreach var of varlist Math_gAdv_Grd3 Math_bwa_Grd3 Math_uta_Grd3 Eng_Sentences_Grd3 Eng_Word_Grd3 Kis_Sentenci_Grd3 Kis_Maneno_Grd3 Eng_Letter_Grd2 Eng_Picture_Grd2 Kis_Silabi_Grd2 Kis_Picha_Grd2 Eng_PictureLetter_Grd1 Eng_Picture_Grd1 Kis_Picha_Grd1 Kis_PichaLetter_Grd1 Kis_Silabi_Grd1 Kis_Maneno_Grd1 Kis_Sentenci_Grd1 Eng_Letter_Grd1 Eng_Word_Grd1 Eng_Sentences_Grd1 Kis_Maneno_Grd2 Kis_Sentenci_Grd2 Eng_Word_Grd2 Eng_Sentences_Grd2{
	gen `var'_Pass=`var'>=4 if !missing(`var') & TestedEL2015==1
}

foreach var of varlist Math_g_Grd2 Math_bwab_Grd2 Math_uta_Grd2 Math_z_Grd1 Math_id_Grd1 Math_uta_Grd1 Math_bwa_Grd1 Math_j_Grd1 Math_t_Grd1 Math_bwa_Grd2 Math_j_Grd2 Math_t_Grd2 Math_z_Grd2 Math_j_Grd3 Math_t_Grd3 Math_z_Grd3 Math_g_Grd3{
	gen `var'_Pass=`var'>=2 if !missing(`var') & TestedEL2015==1
}


foreach var of varlist  Eng_StoryAd_Grd3 Eng_Paragraph_Grd3 Kis_StoryAd_Grd3 Kis_Aya_Grd3 Eng_Paragraph_Grd1 Kis_Aya_Grd1 Kis_Aya_Grd2 Eng_Paragraph_Grd2 Kis_Story_Grd3 Eng_Story_Grd3{
	gen `var'_Pass=`var'==3 if !missing(`var') & TestedEL2015==1
}


foreach var of varlist  Eng_ComPlus_Grd3 Kis_ComPlus_Grd3 Eng_ParagraphComp_Grd2 Kis_MaswaliAya_Grd2 Kis_Comp_Grd3 Eng_Comp_Grd3 {
	gen `var'_Pass=`var'>=2 if !missing(`var') & TestedEL2015==1
}

preserve 
keep studentid DistrictID SchoolID Grade Stream TestedEL2015 stuname district school gender *_Grd1_Pass  *_Grd2_Pass *_Grd3_Pass treatment2 name
compress
saveold "CreatedData\ConsolidatedYr34\AllStudents_PassRates.dta", version(12) replace
restore

estpost tabstat *Grd1*_Pass if TestedEL2015==1, columns(statistics) by(treatment2)
esttab using "Results/ExcelTables/PassRateTwaY3_Grd1.csv", replace main(mean) unstack 

estpost tabstat *Grd2*_Pass if TestedEL2015==1, columns(statistics) by(treatment2)
esttab using "Results/ExcelTables/PassRateTwaY3_Grd2.csv", replace main(mean) unstack 

estpost tabstat *Grd3*_Pass if TestedEL2015==1, columns(statistics) by(treatment2)
esttab using "Results/ExcelTables/PassRateTwaY3_Grd3.csv", replace main(mean) unstack 

**Calculate pass rates STADI
preserve
collapse (sum) *_Pass TestedEL2015 if TestedEL2015==1, by(Grade treatment2 treatarm2)
saveold "CreatedData/ConsolidatedYr34/PassRateTWA_2015_EL.dta", replace
restore
***Back to main code

drop if treatment2!="Gains"


foreach var of varlist grd1_* grd2_* grd3_*{
	qui sum `var' if TestedEL2015==1
	if(`r(sd)'==0) {
		drop `var'
	}
	if(`r(sd)'!=0) {
		egen `var'_Z=std(`var')  if TestedEL2015==1
	}
}





forvalues i=1/2{

pca grd`i'_kis_*_Z
predict grd`i'_kis_Z, score
*egen grd`i'_kis_Z=rowtotal(grd`i'_kis_*_Z) if TestedEL2015==1, missing
if(`i'==1) xtile KisLevel`i' = grd`i'_kis_Z if TestedEL2015==1,  nquantiles(10)
if(`i'==2) xtile KisLevel`i' = grd`i'_kis_Z if TestedEL2015==1,  nquantiles(9)
replace KisLevel`i'=0 if TestedEL2015==0 & Grade==`i'

pca grd`i'_eng_*_Z
predict grd`i'_eng_Z, score
*egen grd`i'_eng_Z=rowtotal(grd`i'_eng_*_Z) if TestedEL2015==1, missing
if(`i'==1) xtile EngLevel`i' = grd`i'_eng_Z if TestedEL2015==1,  nquantiles(3)
if(`i'==2) xtile EngLevel`i' = grd`i'_eng_Z if TestedEL2015==1,  nquantiles(4)
replace EngLevel`i'=0 if TestedEL2015==0 & Grade==`i'

pca grd`i'_his_*_Z
predict grd`i'_his_Z, score
*egen grd`i'_his_Z=rowtotal(grd`i'_his_*_Z) if TestedEL2015==1, missing
if(`i'==1) xtile MathLevel`i' = grd`i'_his_Z if TestedEL2015==1,  nquantiles(9)
if(`i'==2) xtile MathLevel`i' = grd`i'_his_Z if TestedEL2015==1,  nquantiles(10)
replace MathLevel`i'=0 if TestedEL2015==0 & Grade==`i'

}


saveold "CreatedData\ConsolidatedYr34\TWA_StudentLevelsBatches_2016.dta", replace

drop if Grade!=1
collapse (mean) grd1_*, by(SchoolID)

pca grd1_*, components(1)
predict Total, score
xtile TotalQ = Total, nquantiles(10) 
save "CreatedData\ConsolidatedYr34\TWA_SchoolLevelsBatches_2016.dta", replace



use "CreatedData\ConsolidatedYr34\AllStudents_PassRates.dta", clear
merge 1:1 studentid using "CreatedData\ConsolidatedYr34\TWA_StudentLevelsBatches_2016.dta",keepus(MathLevel* EngLevel* KisLevel*)
drop _merge
merge m:1 SchoolID using "CreatedData\ConsolidatedYr34\TWA_SchoolLevelsBatches_2016.dta",keepus(TotalQ)
drop _merge
saveold "CreatedData\ConsolidatedYr34\AllStudents_PassRates.dta", version(12) replace

preserve
keep DistrictID- gender  *Grd1_Pass KisLevel1- MathLevel1 treatment2
drop if Grade!=1
saveold "CreatedData\ConsolidatedYr34\AllStudents_PassRates_Grd1.dta", version(12) replace
restore

preserve
keep DistrictID- gender  *Grd2_Pass KisLevel2- MathLevel2 treatment2
drop if Grade!=2
saveold "CreatedData\ConsolidatedYr34\AllStudents_PassRates_Grd2.dta", version(12) replace
restore


preserve
keep DistrictID- gender  *Grd3_Pass treatment2
drop if Grade!=3
saveold "CreatedData\ConsolidatedYr34\AllStudents_PassRates_Grd3.dta", version(12) replace
restore


use "CreatedData\ConsolidatedYr34\AllStudents_PassRates_Grd1.dta", clear
drop if treatment2!="Gains"
preserve
collapse (mean) Kis_PichaLetter_Grd1_Pass Kis_Picha_Grd1_Pass Kis_Silabi_Grd1_Pass Kis_Maneno_Grd1_Pass Kis_Sentenci_Grd1_Pass Kis_Aya_Grd1_Pass , by(KisLevel1)
drop if KisLevel1==0
saveold "CreatedData/4 Intervention/TwaEL_2015/MeanPassRate_KisLevels_Grd1.dta", replace
restore
preserve
collapse (mean) Eng_PictureLetter_Grd1_Pass Eng_Picture_Grd1_Pass Eng_Letter_Grd1_Pass Eng_Word_Grd1_Pass Eng_Sentences_Grd1_Pass Eng_Paragraph_Grd1_Pass , by(EngLevel1)
drop if EngLevel1==0
saveold "CreatedData/4 Intervention/TwaEL_2015/MeanPassRate_EngLevels_Grd1.dta", replace
restore
preserve
collapse (mean) Math_id_Grd1_Pass Math_uta_Grd1_Pass Math_bwa_Grd1_Pass Math_j_Grd1_Pass Math_t_Grd1_Pass Math_z_Grd1_Pass, by(MathLevel1)
drop if MathLevel1==0
saveold "CreatedData/4 Intervention/TwaEL_2015/MeanPassRate_MathLevels_Grd1.dta", replace
restore

use "CreatedData\ConsolidatedYr34\AllStudents_PassRates_Grd2.dta", clear
drop if treatment2!="Gains"
preserve
collapse (mean) Kis_Picha_Grd2_Pass Kis_Silabi_Grd2_Pass Kis_Maneno_Grd2_Pass Kis_Sentenci_Grd2_Pass Kis_Aya_Grd2_Pass Kis_MaswaliAya_Grd2_Pass , by(KisLevel2)
drop if KisLevel2==0
saveold "CreatedData/4 Intervention/TwaEL_2015/MeanPassRate_KisLevels_Grd2.dta", replace
restore
preserve
collapse (mean) Eng_Picture_Grd2_Pass Eng_Letter_Grd2_Pass Eng_Word_Grd2_Pass Eng_Sentences_Grd2_Pass Eng_Paragraph_Grd2_Pass Eng_ParagraphComp_Grd2_Pass , by(EngLevel2)
drop if EngLevel2==0
saveold "CreatedData/4 Intervention/TwaEL_2015/MeanPassRate_EngLevels_Grd2.dta", replace
restore
preserve
collapse (mean) Math_uta_Grd2_Pass Math_bwab_Grd2_Pass Math_bwa_Grd2_Pass Math_j_Grd2_Pass Math_t_Grd2_Pass Math_z_Grd2_Pass Math_g_Grd2_Pass, by(MathLevel2)
drop if MathLevel2==0
saveold "CreatedData/4 Intervention/TwaEL_2015/MeanPassRate_MathLevels_Grd2.dta", replace
restore



