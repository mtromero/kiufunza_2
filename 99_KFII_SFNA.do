***********************************************************
******************************** 2015 *********************
***********************************************************


use "$basein/5 National Exams/ResultadosEstudiantesTotal_SFNA_2015.dta", clear
capture renvars _all, subst("`=char(69)'" "")
capture renvars _all, subst("`=char(69)'" "")
rename AVRAG AVERAGE
qui foreach sub in AVERAGE kiswahili english jamii hisabati sayansi stadi tehama{
	replace `sub'="5" if `sub'=="A"
	replace `sub'="4" if `sub'=="B"
	replace `sub'="3" if `sub'=="C"
	replace `sub'="2" if `sub'=="D"
	replace `sub'="1" if `sub'=="E"
	replace `sub'="0" if `sub'=="F"
	replace `sub'="" if `sub'=="NA"
	replace `sub'="" if `sub'=="X"
	replace `sub'="" if `sub'=="*S"
	replace `sub'="" if `sub'=="*W"
	replace `sub'="" if `sub'=="*R"
	replace `sub'="" if `sub'=="Absent"
	replace `sub'="" if `sub'=="REFERRED"
}
qui destring AVERAGE kiswahili english jamii hisabati sayansi stadi tehama, replace
drop if AVERAGE==.
keep CAND SX AVERAGE kiswahili- sayansi school_codeSFNA SchoolID
rename SchoolID SchoolID_SFNA2015
rename school_codeSFNA school_codePSL_Final

preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_SFNA2015 status_SFNA2015
drop if status_SFNA2015=="Matched - manual R2"
drop if status_SFNA2015=="Unmatched"
tempfile temp
save `temp'
restore
merge m:1 SchoolID_SFNA2015 using  `temp',keepus(SchoolID)

drop if _merge!=3
drop _merge
save "$base_out/Student_SFNA_2015.dta", replace
gen Pass=(AVERAGE>=3)
collapse (sum) Pass=Pass (mean) PassRate=Pass AverageOwn=AVERAGE kiswahili- sayansi, by(school_codePSL_Final)
save "$base_out/School_SFNA_2015.dta", replace

use "$basein/5 National Exams/ResultadosSchoolTotal_SFNA_2015.dta", clear
rename school_codeSFNA school_codePSL_Final
keep Students Average RankingDistrict TotalDistrict RankingRegion TotalRegion RankingNation TotalNation school_codePSL_Final SchoolID
rename SchoolID SchoolID_SFNA2015

preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_SFNA2015 status_SFNA2015
drop if status_SFNA2015=="Matched - manual R2"
drop if status_SFNA2015=="Unmatched"
tempfile temp
save `temp'
restore
merge 1:1 SchoolID_SFNA2015 using  `temp',keepus(SchoolID)

drop if _merge!=3
drop _merge
merge 1:1 school_codePSL_Final using "$base_out/School_SFNA_2015.dta"
drop if _merge!=3
drop _merge
save "$base_out/School_SFNA_2015.dta", replace





***********************************************************
******************************** 2016 *********************
***********************************************************


use "$basein/5 National Exams/ResultadosEstudiantesTotal_SFNA_2016.dta", clear
capture renvars _all, subst("`=char(69)'" "")
capture renvars _all, subst("`=char(69)'" "")
rename AVRAG AVERAGE
qui foreach sub in AVERAGE kiswahili english jamii hisabati sayansi stadi tehama{
	replace `sub'="5" if `sub'=="A"
	replace `sub'="4" if `sub'=="B"
	replace `sub'="3" if `sub'=="C"
	replace `sub'="2" if `sub'=="D"
	replace `sub'="1" if `sub'=="E"
	replace `sub'="0" if `sub'=="F"
	replace `sub'="" if `sub'=="NA"
	replace `sub'="" if `sub'=="X"
	replace `sub'="" if `sub'=="*S"
	replace `sub'="" if `sub'=="*W"
	replace `sub'="" if `sub'=="*R"
	replace `sub'="" if `sub'=="Absent"
	replace `sub'="" if `sub'=="REFERRED"
	
}
qui destring AVERAGE kiswahili english jamii hisabati sayansi stadi tehama, replace
drop if AVERAGE==.
keep CAND SX AVERAGE kiswahili- sayansi school_codeSFNA SchoolID
rename SchoolID SchoolID_SFNA2016
rename school_codeSFNA school_codePSL_Final

preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_SFNA2016 status_SFNA2016
drop if status_SFNA2016=="Matched - manual R2"
drop if status_SFNA2016=="Unmatched"
tempfile temp
save `temp'
restore
merge m:1 SchoolID_SFNA2016 using  `temp',keepus(SchoolID)

drop if _merge!=3
drop _merge
save "$base_out/Student_SFNA_2016.dta", replace
gen Pass=(AVERAGE>=3)
collapse (sum) Pass=Pass (mean) PassRate=Pass AverageOwn=AVERAGE kiswahili- sayansi, by(school_codePSL_Final)
save "$base_out/School_SFNA_2016.dta", replace

use "$basein/5 National Exams/ResultadosSchoolTotal_SFNA_2016.dta", clear
rename school_codeSFNA school_codePSL_Final
keep Students Average RankingDistrict TotalDistrict RankingRegion TotalRegion RankingNation TotalNation school_codePSL_Final SchoolID
rename SchoolID SchoolID_SFNA2016

preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_SFNA2016 status_SFNA2016
drop if status_SFNA2016=="Matched - manual R2"
drop if status_SFNA2016=="Unmatched"
tempfile temp
save `temp'
restore
merge 1:1 SchoolID_SFNA2016 using  `temp',keepus(SchoolID)

drop if _merge!=3
drop _merge
merge 1:1 school_codePSL_Final using "$base_out/School_SFNA_2016.dta"
drop if _merge!=3
drop _merge
save "$base_out/School_SFNA_2016.dta", replace




***********************************************************
******************************** 2017 *********************
***********************************************************


use "$basein/5 National Exams/ResultadosEstudiantesTotal_SFNA_2017.dta", clear
rename JINA name
rename JINSI SX
capture renvars _all, subst("`=char(69)'" "")
capture renvars _all, subst("`=char(69)'" "")
rename GRDI AVERAGE
qui foreach sub in AVERAGE kiswahili english jamii hisabati sayansi stadi tehama{
	replace `sub'="5" if `sub'=="A"
	replace `sub'="4" if `sub'=="B"
	replace `sub'="3" if `sub'=="C"
	replace `sub'="2" if `sub'=="D"
	replace `sub'="1" if `sub'=="E"
	replace `sub'="0" if `sub'=="F"
	replace `sub'="" if `sub'=="NA"
	replace `sub'="" if `sub'=="X"
	replace `sub'="" if `sub'=="*S"
	replace `sub'="" if `sub'=="*W"
	replace `sub'="" if `sub'=="*R"
	replace `sub'="" if `sub'=="Absent"
	replace `sub'="" if `sub'=="REFERRED"
}
qui destring AVERAGE kiswahili english jamii hisabati sayansi stadi tehama, replace
drop if AVERAGE==.
keep CAND SX AVERAGE kiswahili- sayansi school_codeSFNA SchoolID
rename SchoolID SchoolID_SFNA2017
rename school_codeSFNA school_codePSL_Final

preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_SFNA2017 status_SFNA2017
drop if status_SFNA2017=="Matched - manual R2"
drop if status_SFNA2017=="Unmatched"
tempfile temp
save `temp'
restore
merge m:1 SchoolID_SFNA2017 using  `temp',keepus(SchoolID)


drop if _merge!=3
drop _merge
save "$base_out/Student_SFNA_2017.dta", replace
gen Pass=(AVERAGE>=3)
collapse (sum) Pass=Pass (mean) PassRate=Pass AverageOwn=AVERAGE kiswahili- sayansi, by(school_codePSL_Final)
save "$base_out/School_SFNA_2017.dta", replace

use "$basein/5 National Exams/ResultadosSchoolTotal_SFNA_2017.dta", clear
rename school_codeSFNA school_codePSL_Final
keep Students Average RankingDistrict TotalDistrict RankingRegion TotalRegion RankingNation TotalNation school_codePSL_Final SchoolID
rename SchoolID SchoolID_SFNA2017

preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_SFNA2017 status_SFNA2017
drop if status_SFNA2017=="Matched - manual R2"
drop if status_SFNA2017=="Unmatched"
tempfile temp
save `temp'
restore
merge 1:1 SchoolID_SFNA2017 using  `temp',keepus(SchoolID)

drop if _merge!=3
drop _merge
merge 1:1 school_codePSL_Final using "$base_out/School_SFNA_2017.dta"
drop if _merge!=3
drop _merge
save "$base_out/School_SFNA_2017.dta", replace



