eststo clear
use "$base_out/Student_PSLE_2013.dta",clear
encode SX, gen(SX2)
sort SchoolID
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop _merge
gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "Gains"


gen Pass=(AVERAGE>=3)
label var Pass "Pass"
label var AVERAGE "Score"



eststo m1_2013: reghdfe Pass $treatmentlist i.SX2, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum Pass if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust)

eststo m2_2013: reghdfe AVERAGE $treatmentlist i.SX2, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum AVERAGE if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust)


use "$base_out/School_PSLE_2013.dta",clear
sort SchoolID
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop _merge

gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "Gains"
  
eststo m3_2013: reghdfe Students $treatmentlist, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum Students if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust)


  
************************************
************* 2014 *****************
************************************
use "$base_out/Student_PSLE_2014.dta",clear
encode SX, gen(SX2)
sort SchoolID
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop _merge
gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "Gains"


gen Pass=(AVERAGE>=3)
label var Pass "Pass"
label var AVERAGE "Score"


eststo m1_2014: reghdfe Pass $treatmentlist i.SX2, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum Pass if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust)

eststo m2_2014: reghdfe AVERAGE $treatmentlist i.SX2, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum AVERAGE if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust)


use "$base_out/School_PSLE_2014.dta",clear
sort SchoolID
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop _merge

gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "Gains"
  
eststo m3_2014: reghdfe Students $treatmentlist, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum Students if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust) 
 
 
************************************
************* 2015 *****************
************************************
use "$base_out/Student_PSLE_2015.dta",clear
encode SX, gen(SX2)
sort SchoolID
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop _merge
gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "Gains"


gen Pass=(AVERAGE>=3)
label var Pass "Pass"
label var AVERAGE "Score"



eststo m1_2015: reghdfe Pass $treatmentlist i.SX2, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum Pass if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust)

eststo m2_2015: reghdfe AVERAGE $treatmentlist i.SX2, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum AVERAGE if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust)


use "$base_out/School_PSLE_2015.dta",clear
sort SchoolID
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop _merge

gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "Gains"
  
eststo m3_2015: reghdfe Students $treatmentlist, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum Students if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust) 
 
 
 
************************************
************* 2016 *****************
************************************
use "$base_out/Student_PSLE_2016.dta",clear
encode SX, gen(SX2)
sort SchoolID
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop _merge
gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "Gains"


gen Pass=(AVERAGE>=3)
label var Pass "Pass"
label var AVERAGE "Score"



eststo m1_2016: reghdfe Pass $treatmentlist i.SX2, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum Pass if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust)

eststo m2_2016: reghdfe AVERAGE $treatmentlist i.SX2, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum AVERAGE if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust)


use "$base_out/School_PSLE_2016.dta",clear
sort SchoolID
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop _merge

gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "Gains"
  
eststo m3_2016: reghdfe Students $treatmentlist, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum Students if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust)

************************************
************* 2017 *****************
************************************
use "$base_out/Student_PSLE_2017.dta",clear
encode SX, gen(SX2)
sort SchoolID
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop _merge
gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "Gains"


gen Pass=(AVERAGE>=3)
label var Pass "Pass"
label var AVERAGE "Score"



eststo m1_2017: reghdfe Pass $treatmentlist i.SX2, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum Pass if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust)

eststo m2_2017: reghdfe AVERAGE $treatmentlist i.SX2, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum AVERAGE if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust)


use "$base_out/School_PSLE_2017.dta",clear
sort SchoolID
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop _merge

gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "Gains"
  
eststo m3_2017: reghdfe Students $treatmentlist, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm) 
estadd ysumm
sum Students if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
estadd scalar NSchool=e(N_clust)
 
label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)" 
 
 *m1_2013 m2_2013 m3_2013 m1_2014 m2_2014 m3_2014
esttab   m1_2015 m2_2015 m3_2015 m1_2016 m2_2016 m3_2016 m1_2017 m2_2017 m3_2017 ///
using "$latexcodes/RegPSLE.tex", se ar2 fragment booktabs nolines label b(%9.2fc) se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Grade 7 PSLE 2015" "Grade 7 PSLE 2016" "Grade 7 PSLE 2017", pattern(1 0 0 1 0 0 1 0 0 1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mlabel("Pass" "Score" "Test takers" "Pass" "Score" "Test takers" "Pass" "Score" "Test takers" "Pass" "Score" "Test takers" "Pass" "Score" "Test takers") ///
keep(TreatmentLevels TreatmentGains) stats(N NSchool ymean2 suma p, fmt(%9.0fc %9.0fc a2 a2 a2)  labels("N. of obs." "N. of schools" "Mean control group" "\$\alpha_3 =\alpha_2-\alpha_1\$" "p-value (\$H_0:\alpha_3=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)





