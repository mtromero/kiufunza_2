library(foreign)
library(xtable)
library(stringr)
library(tools)


setwd("C:/Users/Mauricio/Box Sync/01_KiuFunza/CreatedData/4 Intervention/")
SchoolNames=read.dta("SchoolIDs_KFII_Intervention.dta")
StadiPayments3=read.dta("TwaEL_2016/Stadi_Grd3_Payments.dta")
StadiPayments2=read.dta("TwaEL_2016/Stadi_Grd2_Payments.dta")
StadiPayments1=read.dta("TwaEL_2016/Stadi_Grd1_Payments.dta")

National1=apply(StadiPayments1[,c(grep("Sudents",colnames(StadiPayments1)),grep("Math_",colnames(StadiPayments1)),grep("Kis_",colnames(StadiPayments1)))],2,sum)
National2=apply(StadiPayments2[,c(grep("Sudents",colnames(StadiPayments2)),grep("Math_",colnames(StadiPayments2)),grep("Kis_",colnames(StadiPayments2)))],2,sum)
National3=apply(StadiPayments3[,c(grep("Sudents",colnames(StadiPayments3)),grep("Math_",colnames(StadiPayments3)),grep("Kis_",colnames(StadiPayments3)),grep("Eng_",colnames(StadiPayments3)))],2,sum)
National1=c(National1[1],100*National1[2:length(National1)]/National1[1])
National2=c(National2[1],100*National2[2:length(National2)]/National2[1])
National3=c(National3[1],100*National3[2:length(National3)]/National3[1])

District1=aggregate(StadiPayments1[,c(grep("Sudents",colnames(StadiPayments1)),grep("Math_",colnames(StadiPayments1)),grep("Kis_",colnames(StadiPayments1)))],by=list(StadiPayments1$DistrictID),sum)
District2=aggregate(StadiPayments2[,c(grep("Sudents",colnames(StadiPayments2)),grep("Math_",colnames(StadiPayments2)),grep("Kis_",colnames(StadiPayments2)))],by=list(StadiPayments2$DistrictID),sum)
District3=aggregate(StadiPayments3[,c(grep("Sudents",colnames(StadiPayments3)),grep("Math_",colnames(StadiPayments3)),grep("Kis_",colnames(StadiPayments3)),grep("Eng_",colnames(StadiPayments3)))],by=list(StadiPayments3$DistrictID),sum)

District1=cbind(District1[,1:2],100*District1[,3:(ncol(District1))]/District1[,2])
District2=cbind(District2[,1:2],100*District2[,3:(ncol(District2))]/District2[,2])
District3=cbind(District3[,1:2],100*District3[,3:(ncol(District3))]/District3[,2])

School1=aggregate(StadiPayments1[,c(grep("Sudents",colnames(StadiPayments1)),grep("Math_",colnames(StadiPayments1)),grep("Kis_",colnames(StadiPayments1)))],by=list(StadiPayments1$SchoolID),FUN=sum)
School2=aggregate(StadiPayments2[,c(grep("Sudents",colnames(StadiPayments2)),grep("Math_",colnames(StadiPayments2)),grep("Kis_",colnames(StadiPayments2)))],by=list(StadiPayments2$SchoolID),FUN=sum)
School3=aggregate(StadiPayments3[,c(grep("Sudents",colnames(StadiPayments3)),grep("Math_",colnames(StadiPayments3)),grep("Kis_",colnames(StadiPayments3)),grep("Eng_",colnames(StadiPayments3)))],by=list(StadiPayments3$SchoolID),FUN=sum)

School1=cbind(School1[,1:2],100*School1[,3:ncol(School1)]/School1[,2])
School2=cbind(School2[,1:2],100*School2[,3:ncol(School2)]/School2[,2])
School3=cbind(School3[,1:2],100*School3[,3:ncol(School3)]/School3[,2])


National1=data.frame(National1)
National2=data.frame(National2)
National3=data.frame(National3)

rownames(National1)=c("Students","IDADI","UTAMBUZI","NAMBA IPI NI KUBWA","KUJUMLISHA","KUTOA","SILABI","MANENO","SENTENSI")
rownames(National2)=c("Students","NAMBA IPI NI KUBWA","KUJUMLISHA","KUTOA","MANENO","SENTENSI","AYA","UFAHAMU")
rownames(National3)=c("Students","KUJUMLISHA","KUTOA","KUZIDISHA","KUGAWANYA","HADITHI","UFAHAMU","STORY","COMPREHENSION")
colnames(National1)="NATIONAL"
colnames(National2)="NATIONAL"
colnames(National3)="NATIONAL"

colnames(School1)=c("School","Students","IDADI","UTAMBUZI","NAMBA IPI NI KUBWA","KUJUMLISHA","KUTOA","SILABI","MANENO","SENTENSI")
colnames(School2)=c("School","Students","NAMBA IPI NI KUBWA","KUJUMLISHA","KUTOA","MANENO","SENTENSI","AYA","UFAHAMU")
colnames(School3)=c("School","Students","KUJUMLISHA","KUTOA","KUZIDISHA","KUGAWANYA","HADITHI","UFAHAMU","STORY","COMPREHENSION")

setwd(paste0(getwd(),"/TwaEL_2016/pdf_School"))
for(school in unique(School1$School)){

schoolname=SchoolNames$SchoolName[SchoolNames$SchoolID==school]
districtname=SchoolNames$DistrictName[SchoolNames$SchoolID==school]
districtid=SchoolNames$DistrictID[SchoolNames$SchoolID==school]


  

district=as.numeric(substr(str_pad(school, 4, pad = "0"),1,2))
District1[District1[,1]==district,-1]

S1=School1[School1$School==school,-1]
S2=School2[School2$School==school,-1]
S3=School3[School3$School==school,-1]
     
S1=cbind(National1,t(District1[District1[,1]==district,-1]),t(S1))
S2=cbind(National2,t(District2[District2[,1]==district,-1]),t(S2))
S3=cbind(National3,t(District3[District3[,1]==district,-1]),t(S3))

colnames(S1)=c("Shule zote za KiuFunza Stadi","Wilaya","Shule")
colnames(S2)=c("Shule zote za KiuFunza Stadi","Wilaya","Shule")
colnames(S3)=c("Shule zote za KiuFunza Stadi","Wilaya","Shule")

print(xtable(S1[c(2:6),3:1],digits=0),file="latexTables/S1_math.tex",only.contents=T,include.colnames=F,hline.after=NULL,format.args = list(big.mark = ",", decimal.mark = "."))
print(xtable(S1[c(7:9),3:1],digits=0),file="latexTables/S1_kis.tex",only.contents=T,include.colnames=F,hline.after=NULL,format.args = list(big.mark = ",", decimal.mark = "."))
#print(xtable(S1[c(10:12),3:1],digits=0),file="latexTables/S1_eng.tex",only.contents=T,include.colnames=F,hline.after=NULL,format.args = list(big.mark = ",", decimal.mark = "."))

print(xtable(S2[c(2:4),3:1],digits=0),file="latexTables/S2_math.tex",only.contents=T,include.colnames=F,hline.after=NULL,format.args = list(big.mark = ",", decimal.mark = "."))
print(xtable(S2[c(5:8),3:1],digits=0),file="latexTables/S2_kis.tex",only.contents=T,include.colnames=F,hline.after=NULL,format.args = list(big.mark = ",", decimal.mark = "."))
#print(xtable(S2[c(9:11),3:1],digits=0),file="latexTables/S2_eng.tex",only.contents=T,include.colnames=F,hline.after=NULL,format.args = list(big.mark = ",", decimal.mark = "."))


print(xtable(S3[c(2:5),3:1],digits=0),file="latexTables/S3_math.tex",only.contents=T,include.colnames=F,hline.after=NULL,format.args = list(big.mark = ",", decimal.mark = "."))
print(xtable(S3[c(6:7),3:1],digits=0),file="latexTables/S3_kis.tex",only.contents=T,include.colnames=F,hline.after=NULL,format.args = list(big.mark = ",", decimal.mark = "."))
print(xtable(S3[c(8:9),3:1],digits=0),file="latexTables/S3_eng.tex",only.contents=T,include.colnames=F,hline.after=NULL,format.args = list(big.mark = ",", decimal.mark = "."))

for(gr in 1:3){
for(subject in c("Kis","Eng","Math")){
  if(!(gr<3 & subject=="Eng")){
tabla=read.delim(paste0("latexTables/",school,"_",subject,"_grd",gr,".csv"))
tabla=tabla[-1]
tabla[c(4:5)]=tabla[c(4:5)]*100
tabla=t(tabla)
rownames(tabla)=c("Idadi ya Wanafunzi Waliotahiniwa","Kiasi cha Juu","Kiasi Kilicholipwa","Kiwango (%)","Kiwango cha juu katika Wilaya (%)")

print(xtable(tabla,digits=0),file=paste0("latexTables/",subject,"_grd",gr,".tex"),only.contents=T,include.colnames=F,include.rownames=T,hline.after=NULL,format.args = list(big.mark = ",", decimal.mark = "."))
}
}
}

treamtnetname="Stadi"

Sweave("C:/Users/Mauricio/Documents/git/kiufunza_2/RCode/Yr2/SchoolReport_2.rnw")
texi2dvi("SchoolReport_2.tex", pdf=TRUE)
file.exists("SchoolReport_2.pdf")
Dist=substr(str_pad(school, 4, pad = "0"),1,2)
if(!dir.exists(paste0(getwd(),"/",Dist))) dir.create(paste0(getwd(),"/",Dist))
if(!dir.exists(paste0(getwd(),"/",Dist,"/",school))) dir.create(paste0(getwd(),"/",Dist,"/",school))
file.copy("SchoolReport_2.pdf", paste0(Dist,"/",school,"/","teacher_",school,".pdf"),recursive=T)
file.rename("SchoolReport_2.pdf", paste0("teacher_",school,".pdf"))



#         Sweave("C:/Users/Mauricio/Documents/git/kiufunza_2/RCode/SchoolReport.rnw",output=paste0("pdf_Stadi/",school,".tex"))
#texi2pdf(paste0("pdf_Stadi/",school,".tex"),clean=T)


}
