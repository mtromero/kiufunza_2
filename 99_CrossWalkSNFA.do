use "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013_ERIN.dta", clear
gen school_codeSFNA=school_codePSLE
drop school_codePSLE
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2013_ERIN.dta", replace

***************************************
****************************************
************** 2015 ********************
****************************************
****************************************

use "$basein/5 National Exams/ResultadosSchoolTotal_SFNA_2015.dta", clear
keep school_name district_name school_codeSFNA region_name
rename  school_name School
rename  district_name District

replace District="KAHAMA" if District=="KAHAMA MJI"



qui forvalues i=1/5{
	qui foreach var of varlist School{
		replace `var'=regexr(`var',"SHULE YA MSINGI","")
		replace `var'=regexr(`var',"PRIMARY$"," ")
		replace `var'=regexr(`var',"PPIMARY$"," ")
		replace `var'=regexr(`var',"PRIIMARY$"," ")
		replace `var'=regexr(`var',"PRINARY$"," ")
		replace `var'=regexr(`var',"PRMARY$"," ")
		replace `var'=regexr(`var',"P/RIMARY$"," ")
		replace `var'=regexr(`var',"PRIMAMARY$"," ")
		replace `var'=regexr(`var',"-PRIMA$"," ")
		replace `var'=regexr(`var',"ACADPRYMARY$"," ")
		replace `var'=regexr(`var',"PR$"," ")
		replace `var'=regexr(`var',"PRIMAEY$"," ")
		replace `var'=regexr(`var',"PRIMMARY$"," ")
		replace `var'=regexr(`var',"PRIMARI$"," ")
		replace `var'=regexr(`var',"PSCHOOL$"," ")
		replace `var'=regexr(`var'," PRM$"," ")
		replace `var'=regexr(`var',"SCHOOL$"," ")
		replace `var'=regexr(`var',"SHCOOL$"," ")
		replace `var'=regexr(`var',"CHOOL$"," ")
		replace `var'=regexr(`var',"PRIMARYA$"," ")
		
		
		replace `var'=regexr(`var',"ENGLISH MEDIUM$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUM$"," ")
		replace `var'=regexr(`var',"ENGL MEDIUM$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTRE$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTER$"," ")
		replace `var'=regexr(`var',"ACADEMY$"," ")
		replace `var'=regexr(`var',"CENTRE$"," ")
		replace `var'=regexr(`var',"CENTER$"," ")
		replace `var'=regexr(`var',"SCHOO$"," ")
		replace `var'=regexr(`var',"SHOOL$"," ")
		replace `var'=regexr(`var',"SCHOLL$"," ")
		replace `var'=regexr(`var'," EMPS$"," ")
		replace `var'=regexr(`var'," SCH$"," ")
		replace `var'=regexr(`var'," PS$"," ")
		replace `var'=regexr(`var'," P/S$"," ")
		replace `var'=regexr(`var',"ENG MED$"," ")
		replace `var'=regexr(`var',"ENG MEDIUM$"," ")
		replace `var'=regexr(`var',"ENG MD$"," ")
		replace `var'=regexr(`var',"PRIMSCHOOL$"," ")
		replace `var'=regexr(`var',"PRSCHOOL$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUMSCHOOL$"," ")
		replace `var'=regexr(`var',"\(ENGLISHMEDIUM\)$"," ")
		replace `var'=regexr(`var',"P /$"," ")
		 
		replace `var'=regexr(`var',"\.","")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strupper(`var')
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var'," - ","-")
		replace `var'=regexr(`var'," -","-")
		replace `var'=regexr(`var',"- ","-")
		replace `var'=regexr(`var',"-"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"  "," ")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strtrim(`var')
		replace `var'=stritrim(`var')	
	}
}
*replace School=stritrim(School)
replace School=strltrim(School)
replace School=strrtrim(School)
replace School=strupper(School)
replace District=strupper(District)
replace District=regexr(District,"\(V\)","")
replace District=regexr(District,"\(M\)","")
replace District=regexr(District,"\(DC\)","")
replace District=regexr(District,"\(TC\)","")
replace District=regexr(District,"VIJIJINI","")
replace District=regexr(District,"MJI","")
replace District=regexr(District," DC","")
replace District=regexr(District,"-DC","")
replace District=regexr(District," NI","")
replace District=regexr(District," D\.C","")
replace District=regexr(District," TC","")
replace District=regexr(District," D\.C\.","")
replace District=regexr(District,"MANISPAA","")
replace District=regexr(District,"JIJI","")

replace District=strltrim(District)
replace District=strrtrim(District)
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"  "," ")
replace District=regexr(District,"'"," ")
replace District=regexr(District,"  "," ")
replace District=strltrim(District)
replace District=strrtrim(District)
replace School=strltrim(School)
replace School=strrtrim(School)
replace school_codeSFNA=regexr(school_codeSFNA,"PS","") 
destring school_codeSFNA, replace

preserve
merge m:1 School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", keepus(SchoolID)
keep if _merge==3
drop _merge
drop if SchoolID==616 & school_codeSFNA!=2002061
drop if SchoolID==407 & school_codeSFNA!=0204074
gen LevelCertanty=1
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta", replace
restore

preserve
reclink School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(District) idmaster(school_codeSFNA) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=2
append using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta"
keep School District school_codeSFNA SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codeSFNA) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=3
append using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta"
keep School District school_codeSFNA SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codeSFNA) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta")
drop if _merge!=3
keep if District=="GEITA" | District=="KAHAMA" | District=="KARAGWE" | District=="KINONDONI" | District=="KONDOA" | ///
		District=="KOROGWE" | District=="LUSHOTO" | District=="MBINGA" | District=="MBOZI" | District=="SUMBAWANGA"
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=4
append using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta"
keep School District school_codeSFNA SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta", replace
restore

use "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta", clear
rename school_codeSFNA school_codeSFNA2015 
tostring school_codeSFNA2015, replace
replace school_codeSFNA2015="PS"+school_codeSFNA2015
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2013_ERIN.dta"
replace school_codeSFNA2015=regexr(school_codeSFNA2015,"PS","")
destring school_codeSFNA2015, replace
gen school_codeSFNA2015_2= string(school_codeSFNA2015,"%07.0f")
replace school_codeSFNA2015_2="PS"+school_codeSFNA2015_2 if school_codeSFNA2015!=.
drop school_codeSFNA2015
rename school_codeSFNA2015_2 school_codeSFNA2015

replace school_codeSFNA=regexr(school_codeSFNA,"PS","")
destring school_codeSFNA, replace
gen school_codeSFNA_2= string(school_codeSFNA,"%07.0f")
replace school_codeSFNA_2="PS"+school_codeSFNA_2 if school_codeSFNA!=.
drop school_codeSFNA
rename school_codeSFNA_2 school_codeSFNA
replace school_codeSFNA="" if school_codeSFNA=="."
replace school_codeSFNA2015="" if school_codeSFNA2015=="."

bro if school_codeSFNA2015!= school_codeSFNA & school_codeSFNA2015!="" & school_codeSFNA!=""



gen school_codePSL_Final=school_codeSFNA2015 if school_codeSFNA2015!=""
replace  school_codePSL_Final=school_codeSFNA if school_codeSFNA!="" & school_codePSL_Final==""

replace school_codePSL_Final="PS0303033" if SchoolID==511 
replace school_codePSL_Final="PS1501048" if SchoolID==1018
replace school_codePSL_Final="PS1010082" if SchoolID==923



save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta", replace
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta", update keepus( school_codePSL_Final)
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta", update keepus( school_codePSL_Final)
drop _merge
drop if school_codePSL_Final==""
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta", replace



****************************************
****************************************
************** 2016 ********************
****************************************
****************************************

use "$basein/5 National Exams/ResultadosSchoolTotal_SFNA_2016.dta", clear
keep school_name district_name school_codeSFNA region_name
rename  school_name School
rename  district_name District


replace District="KAHAMA" if District=="KAHAMA MJI"
replace District="MBINGA" if District=="MBINGA(V)"
replace District="MPANDA MJI" if District=="MPANDA MANISPAA"





qui forvalues i=1/5{
	qui foreach var of varlist School{
		replace `var'=regexr(`var',"SHULE YA MSINGI","")
		replace `var'=regexr(`var',"PRIMARY$"," ")
		replace `var'=regexr(`var',"PPIMARY$"," ")
		replace `var'=regexr(`var',"PRIIMARY$"," ")
		replace `var'=regexr(`var',"PRINARY$"," ")
		replace `var'=regexr(`var',"PRMARY$"," ")
		replace `var'=regexr(`var',"P/RIMARY$"," ")
		replace `var'=regexr(`var',"PRIMAMARY$"," ")
		replace `var'=regexr(`var',"-PRIMA$"," ")
		replace `var'=regexr(`var',"ACADPRYMARY$"," ")
		replace `var'=regexr(`var',"PR$"," ")
		replace `var'=regexr(`var',"PRIMAEY$"," ")
		replace `var'=regexr(`var',"PRIMMARY$"," ")
		replace `var'=regexr(`var',"PRIMARI$"," ")
		replace `var'=regexr(`var',"PSCHOOL$"," ")
		replace `var'=regexr(`var'," PRM$"," ")
		replace `var'=regexr(`var',"SCHOOL$"," ")
		replace `var'=regexr(`var',"SHCOOL$"," ")
		replace `var'=regexr(`var',"CHOOL$"," ")
		replace `var'=regexr(`var',"PRIMARYA$"," ")
		
		
		replace `var'=regexr(`var',"ENGLISH MEDIUM$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUM$"," ")
		replace `var'=regexr(`var',"ENGL MEDIUM$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTRE$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTER$"," ")
		replace `var'=regexr(`var',"ACADEMY$"," ")
		replace `var'=regexr(`var',"CENTRE$"," ")
		replace `var'=regexr(`var',"CENTER$"," ")
		replace `var'=regexr(`var',"SCHOO$"," ")
		replace `var'=regexr(`var',"SHOOL$"," ")
		replace `var'=regexr(`var',"SCHOLL$"," ")
		replace `var'=regexr(`var'," EMPS$"," ")
		replace `var'=regexr(`var'," SCH$"," ")
		replace `var'=regexr(`var'," PS$"," ")
		replace `var'=regexr(`var'," P/S$"," ")
		replace `var'=regexr(`var',"ENG MED$"," ")
		replace `var'=regexr(`var',"ENG MEDIUM$"," ")
		replace `var'=regexr(`var',"ENG MD$"," ")
		replace `var'=regexr(`var',"PRIMSCHOOL$"," ")
		replace `var'=regexr(`var',"PRSCHOOL$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUMSCHOOL$"," ")
		replace `var'=regexr(`var',"\(ENGLISHMEDIUM\)$"," ")
		replace `var'=regexr(`var',"P /$"," ")
		 
		replace `var'=regexr(`var',"\.","")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strupper(`var')
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var'," - ","-")
		replace `var'=regexr(`var'," -","-")
		replace `var'=regexr(`var',"- ","-")
		replace `var'=regexr(`var',"-"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"  "," ")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strtrim(`var')
		replace `var'=stritrim(`var')	
	}
}
*replace School=stritrim(School)
replace School=strltrim(School)
replace School=strrtrim(School)
replace School=strupper(School)
replace District=strupper(District)
replace District=regexr(District,"\(V\)","")
replace District=regexr(District,"\(M\)","")
replace District=regexr(District,"\(DC\)","")
replace District=regexr(District,"\(TC\)","")
replace District=regexr(District,"VIJIJINI","")
replace District=regexr(District,"MJI","")
replace District=regexr(District," DC","")
replace District=regexr(District,"-DC","")
replace District=regexr(District," NI","")
replace District=regexr(District," D\.C","")
replace District=regexr(District," TC","")
replace District=regexr(District," D\.C\.","")
replace District=regexr(District,"MANISPAA","")
replace District=regexr(District,"JIJI","")

replace District=strltrim(District)
replace District=strrtrim(District)
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"  "," ")
replace District=regexr(District,"'"," ")
replace District=regexr(District,"  "," ")
replace District=strltrim(District)
replace District=strrtrim(District)
replace School=strltrim(School)
replace School=strrtrim(School)
replace school_codeSFNA=regexr(school_codeSFNA,"PS","") 
destring school_codeSFNA, replace

preserve
merge m:1 School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", keepus(SchoolID)
keep if _merge==3
drop _merge
drop if SchoolID==616 & school_codeSFNA!=2002061
drop if SchoolID==407 & school_codeSFNA!=0204074
gen LevelCertanty=1
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2016.dta", replace
restore

preserve
reclink School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(District) idmaster(school_codeSFNA) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2016.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=2
append using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2016.dta"
keep School District school_codeSFNA SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2016.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codeSFNA) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2016.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=3
append using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2016.dta"
keep School District school_codeSFNA SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2016.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codeSFNA) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2016.dta")
drop if _merge!=3
keep if District=="GEITA" | District=="KAHAMA" | District=="KARAGWE" | District=="KINONDONI" | District=="KONDOA" | ///
		District=="KOROGWE" | District=="LUSHOTO" | District=="MBINGA" | District=="MBOZI" | District=="SUMBAWANGA"
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=4
append using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2016.dta"
keep School District school_codeSFNA SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2016.dta", replace
restore

use "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2016.dta", clear
rename school_codeSFNA school_codeSFNA2016 
tostring school_codeSFNA2016, replace
replace school_codeSFNA2016="PS"+school_codeSFNA2016
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2013_ERIN.dta"
replace school_codeSFNA2016=regexr(school_codeSFNA2016,"PS","")
destring school_codeSFNA2016, replace
gen school_codeSFNA2016_2= string(school_codeSFNA2016,"%07.0f")
replace school_codeSFNA2016_2="PS"+school_codeSFNA2016_2 if school_codeSFNA2016!=.
drop school_codeSFNA2016
rename school_codeSFNA2016_2 school_codeSFNA2016

replace school_codeSFNA=regexr(school_codeSFNA,"PS","")
destring school_codeSFNA, replace
gen school_codeSFNA_2= string(school_codeSFNA,"%07.0f")
replace school_codeSFNA_2="PS"+school_codeSFNA_2 if school_codeSFNA!=.
drop school_codeSFNA
rename school_codeSFNA_2 school_codeSFNA
replace school_codeSFNA="" if school_codeSFNA=="."
replace school_codeSFNA2016="" if school_codeSFNA2016=="."

bro if school_codeSFNA2016!= school_codeSFNA & school_codeSFNA2016!="" & school_codeSFNA!=""



gen school_codePSL_Final=school_codeSFNA2016 if school_codeSFNA2016!=""
replace  school_codePSL_Final=school_codeSFNA if school_codeSFNA!="" & school_codePSL_Final==""

replace school_codePSL_Final="PS0303033" if SchoolID==511 & school_codePSL_Final==""
replace school_codePSL_Final="PS1501048" if SchoolID==1018 & school_codePSL_Final==""

drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta", update keepus( school_codePSL_Final)
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta", update keepus( school_codePSL_Final)
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta", update keepus( school_codePSL_Final)
drop _merge
drop if school_codePSL_Final==""
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2016.dta", replace




****************************************
****************************************
************** 2017 ********************
****************************************
****************************************

use "$basein/5 National Exams/ResultadosSchoolTotal_SFNA_2017.dta", clear
keep school_name district_name school_codeSFNA region_name
rename  school_name School
rename  district_name District


replace District="KINONDONI(V)" if District=="KINONDONI"
replace District="KINONDONI(M)" if District=="UBUNGO"

replace District="TEMEKE(V)" if District=="KIGAMBONI"
replace District="TEMEKE(M)" if District=="TEMEKE"

replace District="TARIME" if District=="TARIME(V)"
replace District="MBINGA" if District=="MBINGA (DC)"

replace District="KAHAMA" if District=="KAHAMA MJI"
replace District="MPANDA MJI" if District=="MPANDA MANISPAA"


qui forvalues i=1/5{
	qui foreach var of varlist School{
		replace `var'=regexr(`var',"SHULE YA MSINGI","")
		replace `var'=regexr(`var',"PRIMARY$"," ")
		replace `var'=regexr(`var',"PPIMARY$"," ")
		replace `var'=regexr(`var',"PRIIMARY$"," ")
		replace `var'=regexr(`var',"PRINARY$"," ")
		replace `var'=regexr(`var',"PRMARY$"," ")
		replace `var'=regexr(`var',"P/RIMARY$"," ")
		replace `var'=regexr(`var',"PRIMAMARY$"," ")
		replace `var'=regexr(`var',"-PRIMA$"," ")
		replace `var'=regexr(`var',"ACADPRYMARY$"," ")
		replace `var'=regexr(`var',"PR$"," ")
		replace `var'=regexr(`var',"PRIMAEY$"," ")
		replace `var'=regexr(`var',"PRIMMARY$"," ")
		replace `var'=regexr(`var',"PRIMARI$"," ")
		replace `var'=regexr(`var',"PSCHOOL$"," ")
		replace `var'=regexr(`var'," PRM$"," ")
		replace `var'=regexr(`var',"SCHOOL$"," ")
		replace `var'=regexr(`var',"SHCOOL$"," ")
		replace `var'=regexr(`var',"CHOOL$"," ")
		replace `var'=regexr(`var',"PRIMARYA$"," ")
		
		
		replace `var'=regexr(`var',"ENGLISH MEDIUM$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUM$"," ")
		replace `var'=regexr(`var',"ENGL MEDIUM$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTRE$"," ")
		replace `var'=regexr(`var',"ACADEMY CENTER$"," ")
		replace `var'=regexr(`var',"ACADEMY$"," ")
		replace `var'=regexr(`var',"CENTRE$"," ")
		replace `var'=regexr(`var',"CENTER$"," ")
		replace `var'=regexr(`var',"SCHOO$"," ")
		replace `var'=regexr(`var',"SHOOL$"," ")
		replace `var'=regexr(`var',"SCHOLL$"," ")
		replace `var'=regexr(`var'," EMPS$"," ")
		replace `var'=regexr(`var'," SCH$"," ")
		replace `var'=regexr(`var'," PS$"," ")
		replace `var'=regexr(`var'," P/S$"," ")
		replace `var'=regexr(`var',"ENG MED$"," ")
		replace `var'=regexr(`var',"ENG MEDIUM$"," ")
		replace `var'=regexr(`var',"ENG MD$"," ")
		replace `var'=regexr(`var',"PRIMSCHOOL$"," ")
		replace `var'=regexr(`var',"PRSCHOOL$"," ")
		replace `var'=regexr(`var',"ENGLISHMEDIUMSCHOOL$"," ")
		replace `var'=regexr(`var',"\(ENGLISHMEDIUM\)$"," ")
		replace `var'=regexr(`var',"P /$"," ")
		 
		replace `var'=regexr(`var',"\.","")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strupper(`var')
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var'," - ","-")
		replace `var'=regexr(`var'," -","-")
		replace `var'=regexr(`var',"- ","-")
		replace `var'=regexr(`var',"-"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"'"," ")
		replace `var'=regexr(`var',"  "," ")
		replace `var'=strltrim(`var')
		replace `var'=strrtrim(`var')
		replace `var'=strtrim(`var')
		replace `var'=stritrim(`var')	
	}
}
*replace School=stritrim(School)
replace School=strltrim(School)
replace School=strrtrim(School)
replace School=strupper(School)
replace District=strupper(District)
replace District=regexr(District,"\(V\)","")
replace District=regexr(District,"\(M\)","")
replace District=regexr(District,"\(DC\)","")
replace District=regexr(District,"\(TC\)","")
replace District=regexr(District,"VIJIJINI","")
replace District=regexr(District,"MJI","")
replace District=regexr(District," DC","")
replace District=regexr(District,"-DC","")
replace District=regexr(District," NI","")
replace District=regexr(District," D\.C","")
replace District=regexr(District," TC","")
replace District=regexr(District," D\.C\.","")
replace District=regexr(District,"MANISPAA","")
replace District=regexr(District,"JIJI","")


replace District=strltrim(District)
replace District=strrtrim(District)
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"'"," ")
replace School=regexr(School,"  "," ")
replace District=regexr(District,"'"," ")
replace District=regexr(District,"  "," ")
replace District=strltrim(District)
replace District=strrtrim(District)
replace School=strltrim(School)
replace School=strrtrim(School)
replace school_codeSFNA=regexr(school_codeSFNA,"PS","") 
destring school_codeSFNA, replace

preserve
merge m:1 School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", keepus(SchoolID)
keep if _merge==3
drop _merge
drop if SchoolID==616 & school_codeSFNA!=2002061
drop if SchoolID==407 & school_codeSFNA!=0204074
gen LevelCertanty=1
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2017.dta", replace
restore

preserve
reclink School District using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(District) idmaster(school_codeSFNA) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2017.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=2
append using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2017.dta"
keep School District school_codeSFNA SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2017.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codeSFNA) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2017.dta")
drop if _merge!=3
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=3
append using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2017.dta"
keep School District school_codeSFNA SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2017.dta", replace
restore

preserve
reclink School region_name  using "$base_out/CrossWalk_PSLE/SchoolNames.dta", required(region_name) idmaster(school_codeSFNA) idusing(SchoolID) gen(prob) minscore(0.99)  exclude("$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2017.dta")
drop if _merge!=3
keep if District=="GEITA" | District=="KAHAMA" | District=="KARAGWE" | District=="KINONDONI" | District=="KONDOA" | ///
		District=="KOROGWE" | District=="LUSHOTO" | District=="MBINGA" | District=="MBOZI" | District=="SUMBAWANGA"
bys SchoolID: gen N=_N
drop if N>1
gen LevelCertanty=4
append using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2017.dta"
keep School District school_codeSFNA SchoolID LevelCertanty
save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2017.dta", replace
restore

use "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2017.dta", clear
rename school_codeSFNA school_codeSFNA2017 
tostring school_codeSFNA2017, replace
replace school_codeSFNA2017="PS"+school_codeSFNA2017
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2013_ERIN.dta"
replace school_codeSFNA2017=regexr(school_codeSFNA2017,"PS","")
destring school_codeSFNA2017, replace
gen school_codeSFNA2017_2= string(school_codeSFNA2017,"%07.0f")
replace school_codeSFNA2017_2="PS"+school_codeSFNA2017_2 if school_codeSFNA2017!=.
drop school_codeSFNA2017
rename school_codeSFNA2017_2 school_codeSFNA2017

replace school_codeSFNA=regexr(school_codeSFNA,"PS","")
destring school_codeSFNA, replace
gen school_codeSFNA_2= string(school_codeSFNA,"%07.0f")
replace school_codeSFNA_2="PS"+school_codeSFNA_2 if school_codeSFNA!=.
drop school_codeSFNA
rename school_codeSFNA_2 school_codeSFNA
replace school_codeSFNA="" if school_codeSFNA=="."
replace school_codeSFNA2017="" if school_codeSFNA2017=="."

bro if school_codeSFNA2017!= school_codeSFNA & school_codeSFNA2017!="" & school_codeSFNA!=""



gen school_codePSL_Final=school_codeSFNA2017 if school_codeSFNA2017!=""
replace  school_codePSL_Final=school_codeSFNA if school_codeSFNA!="" & school_codePSL_Final==""

replace school_codePSL_Final="PS0303033" if SchoolID==511
replace school_codePSL_Final="PS1501048" if SchoolID==1018
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2016.dta", update keepus( school_codePSL_Final)
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2015.dta", update keepus( school_codePSL_Final)
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta", update keepus( school_codePSL_Final)
drop _merge
merge 1:1 SchoolID using "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta", update keepus( school_codePSL_Final)
drop _merge
drop if school_codePSL_Final==""

replace school_codePSL_Final="PS0204023" if SchoolID==415
replace school_codePSL_Final="PS0204030" if SchoolID==421




save "$base_out/CrossWalk_PSLE/CrossWalk_SFNA_2017.dta", replace
exit
