\documentclass[11pt,letterpaper]{article}
\usepackage{float}
\usepackage{booktabs}
\usepackage[cm]{fullpage}
\usepackage{longtable}
\usepackage[final]{pdfpages}
 \setboolean{@twoside}{false}

\usepackage{caption}
\hyphenation{}


\begin{document}

\title{
\Huge{\textbf{2. Taarifa ya Matokeo ya Wanafunzi}}   \\
Wilaya: \Sexpr{districtname} \\
Namba	ya	Wilaya: \Sexpr{districtid} \\
Shule:	\Sexpr{schoolname}      \\
Namba	ya	Schule:	\Sexpr{school}     \\
Aina	ya	Majaribio:	\Sexpr{treamtnetname}    \\
}
\date{}
\maketitle

\tableofcontents

\clearpage


\section{Darasa la 1}
\subsection{Kiswahili}

%Wanafunzi wa darasa la kwanza wa Schule ya __________ wako kwenye kundi la _________ kwa masomo ya Kiswahili.
Jedwali hili linaonyesha majina ya wanafunzi wote waliofanya mtihani wa Twaweza wa 2016 katika somo la Kiswahili darasa la 1. Kama mwanafunzi alifaulu stadi husika, katika jedwali imeandikwa namba 1. Kama mwanafunzi ameshindwa stadi husika, katika jedwali imeandikwa 0. Kufaulu stadi ni pale mwanafunzi alipopata vema katika zaidi ya nusu ya maswali. Stadi katika somo la Kiswahili Darasa la 1 ni: Silabi (SI), Maneno (MA), na Sentensi (SE).
\input{latexTables/S1_Kis.tex}
\clearpage


\subsection{Hesabu}

Jedwali hili linaonyesha majina ya wanafunzi wote waliofanya mtihani wa Twaweza wa 2016 katika somo la Hesabu darasa la 1. Kama mwanafunzi alifaulu stadi husika, katika jedwali imeandikwa namba 1. Kama mwanafunzi ameshindwa stadi husika, katika jedwali imeandikwa 0. Kufaulu stadi ni pale mwanafunzi alipopata vema katika zaidi ya nusu ya maswali. Stadi katika somo la Hesabu Darasa la 1 ni: Taja Idadi (ID), Utambuzi wa Namba (UTA), Namba Ipi Ni Kubwa (BWA), Kujumlisha (J), na Kutoa (T). Kujumlisha (J), na Kutoa (T).
\input{latexTables/S1_Math.tex}
\clearpage



\section{Darasa la 2}
\subsection{Kiswahili}

Jedwali hili linaonyesha majina ya wanafunzi wote waliofanya mtihani wa Twaweza wa 2016 katika somo la Kiswahili darasa la 2. Kama mwanafunzi alifaulu stadi husika, katika jedwali imeandikwa namba 1. Kama mwanafunzi ameshindwa stadi husika, katika jedwali imeandikwa 0. Kufaulu stadi ni pale mwanafunzi alipopata vema katika zaidi ya nusu ya maswali. Stadi katika somo la Kiswahili Darasa la 2 ni: Maneno (MA), Sentensi (SE), Hadithi (H), na Maswali ya Ufahamu (U).
\input{latexTables/S2_Kis.tex}
\clearpage


\subsection{Hesabu}

Jedwali hili linaonyesha majina ya wanafunzi wote waliofanya mtihani wa Twaweza wa 2016 katika somo la Hesabu darasa la 2. Kama mwanafunzi alifaulu stadi husika, katika jedwali imeandikwa namba 1. Kama mwanafunzi ameshindwa stadi husika, katika jedwali imeandikwa 0. Kufaulu stadi ni pale mwanafunzi alipopata vema katika zaidi ya nusu ya maswali. Stadi katika somo la Hesabu Darasa la 2 ni: Namba Ipi Ni Kubwa (BWA), Kujumlisha (J), na Kutoa (T).
\input{latexTables/S2_Math.tex}
\clearpage






\section{Darasa la 3}

\subsection{Kiswahili}



Jedwali hili linaonyesha majina ya wanafunzi wote waliofanya mtihani wa Twaweza wa 2016 katika somo la Kiswahili darasa la 3. Kama mwanafunzi alifaulu stadi husika, katika jedwali imeandikwa namba 1. Kama mwanafunzi ameshindwa stadi husika, katika jedwali imeandikwa 0. Kufaulu stadi ni pale mwanafunzi alipopata vema katika zaidi ya nusu ya maswali. Stadi katika somo la Kiswahili Darasa la 3 ni: Hadithi (H), na Maswali ya Ufahamu (M).
\input{latexTables/S3_Kis.tex}

\clearpage

\subsection{Kiingereza}

Jedwali hili linaonyesha majina ya wanafunzi wote waliofanya mtihani wa Twaweza wa 2016 katika somo la Kiingereza darasa la 3. Kama mwanafunzi alifaulu stadi husika, katika jedwali imeandikwa namba 1. Kama mwanafunzi ameshindwa stadi husika, katika jedwali imeandikwa 0. Kufaulu stadi ni pale mwanafunzi alipopata vema katika zaidi ya nusu ya maswali. Stadi katika somo la Kiingereza Darasa la 3 ni: Story (S), and Comprehension Questions (C).
\input{latexTables/S3_Eng.tex}

\clearpage


\subsection{Hesabu}

Jedwali hili linaonyesha majina ya wanafunzi wote waliofanya mtihani wa Twaweza wa 2016 katika somo la Hesabu darasa la 3. Kama mwanafunzi alifaulu stadi husika, katika jedwali imeandikwa namba 1. Kama mwanafunzi ameshindwa stadi husika, katika jedwali imeandikwa 0. Kufaulu stadi ni pale mwanafunzi alipopata vema katika zaidi ya nusu ya maswali. Stadi katika somo la Hesabu Darasa la 3 ni: Kujumlisha (J), Kutoa (T), Kuzidisha (Z), na Kugawanya (G).
\input{latexTables/S3_Math.tex}





\end{document}


