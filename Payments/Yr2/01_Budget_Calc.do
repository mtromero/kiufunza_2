capture log close
log using "CreatedData/4 Intervention/TwaEL_2016/BudgetCalcs.smcl", replace

************************************************************
****** Lets read JML data and destring/compress it.  *******
*******This is the data I'll use in the future *************
************************************************************
************************************************************
use "RawData/12 Intervention_KFII/4 Endline 2016/3_TwaEL2016_StudentData_FINAL_nopii", clear
rename studentid studentID
replace studentID="" if studentID=="999"
replace studentID="" if studentID=="-999"
replace studentID="" if studentID=="99"
replace studentID="" if studentID=="-99"
replace studentID="" if studentID=="-  999"
replace studentID="" if studentID=="--BLANK--"
replace studentID="" if studentID=="-.999"
replace studentID="" if studentID=="-99.9"
replace studentID="" if studentID=="9.99"
rename studentID studentid 

gen newstudentindicator="M"
replace studentid="99999999" if studentid==""
sort schoolid grade studentid
bys schoolid grade: gen studnum=_n if (studentid=="99999999" & grade==1)
tostring studnum, format(%03.0f) generate(studnum_s)
tostring schoolid, format(%04.0f) generate(schoolid_s)
gen grade2016_s="A" if (studentid=="99999999" & grade==1)
gen studentid_newstud=schoolid_s+grade2016_s+newstudentindicator+studnum_s if (studentid=="99999999" & grade==1)
replace studentid=studentid_newstud if (studentid=="99999999" & grade==1)
drop studnum studentid_newstud grade2016_s studnum_s schoolid_s 

sort schoolid grade studentid
bys schoolid grade: gen studnum=_n if (studentid=="99999999" & (grade==2 | grade==3))
tostring studnum, format(%03.0f) generate(studnum_s)
tostring schoolid, format(%04.0f) generate(schoolid_s)
tostring grade, format(%01.0f) generate(grade2016_s)
gen studentid_new=schoolid_s+grade2016_s+newstudentindicator+studnum_s
replace studentid=studentid_new if (studentid=="99999999" & (grade==2 | grade==3))
isid studentid
drop studnum studentid_new studnum_s schoolid_s 
drop newstudentindicator
destring, replace
compress
preserve
keep if treatarm2==2
saveold "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_Mashindano_CLEAN.dta", replace
restore
preserve
keep if treatarm2!=2
saveold "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_StadiandControl_CLEAN.dta", replace
restore


************************************************************
************************************************************
****** FIRS LETS CREATE SOME BASIC DATA ********************
************************************************************
************************************************************

**This IS KEY. This are the student groups based on last year's exam!
use "CreatedData/ConsolidatedYr34/TWA_StudentLevelsBatches_2016.dta", clear
drop if treatment2!="Gains"
drop if Grade==3
drop StuID_15 
rename SchoolID, lower
keep schoolid studentid KisLevel1 EngLevel1 MathLevel1 KisLevel2 EngLevel2 MathLevel2

merge 1:1 studentid using "RawData/12 Intervention_KFII/4 Endline 2016/ID_Many_ToFinal_06122016_nopii.dta"
drop if _merge==2
drop _merge
replace studentid=studentid_final if studentid_final!=""
collapse (max) KisLevel1- MathLevel2 maxgrade2015=grade2015 (min)  mingrade2015=grade2015, by(studentid schoolid SchoolID)
saveold "CreatedData/4 Intervention/TwaEL_2016/2016_GroupsStudent_Mash_Grade23.dta", replace version(12)

*These are the school groups based on last year's exam!
use "CreatedData/ConsolidatedYr34/TWA_SchoolLevelsBatches_2016.dta", clear
keep SchoolID TotalQ
rename TotalQ LevelGrade0
keep LevelGrade0 SchoolID
rename SchoolID, lower
sort schoolid
saveold "CreatedData/4 Intervention/TwaEL_2016/2016_GroupsStudent_Mash_Grade1.dta", replace version(12)

************************************************************
************************************************************
****** Now lets get the number of students to be tested ***
************************************************************
************************************************************
*This should really be the list of students I expect to test
use "RawData/12 Intervention_KFII/4 Endline 2016/StudentNameLists_06122016_nopii", clear 
bys studentid: gen cont=_n
drop if cont>=2
drop cont
rename SchoolID, lower
ds studentid districtid schoolid, not
merge 1:1 studentid using "CreatedData/4 Intervention/TwaEL_2016/2016_GroupsStudent_Mash_Grade23.dta", keepus(schoolid KisLevel1- MathLevel2)
*Drop students not tested at EL last year 
*drop if TestedEL==0
*drop students who are in grade 4 last year
drop if grade2016==4
drop if _merge==2 /*this should be no one... */
gen Match_Original=_merge
drop _merge
merge m:1 schoolid using "CreatedData/4 Intervention/TwaEL_2016/2016_GroupsStudent_Mash_Grade1.dta"
drop _merge
rename schoolid SchoolID
merge m:1 SchoolID using "RawData/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2) update
drop _merge
merge m:1 SchoolID using "CommScripts/Kigoma Sample/KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440

*136 is a split school
replace treatarm2=1 if SchoolID==136
replace treatment2="Levels" if SchoolID==136

foreach var of varlist LevelGrade0 KisLevel1 EngLevel1 MathLevel1 KisLevel2 EngLevel2 MathLevel2{
	replace `var'=. if treatment2=="Levels"
} 
/*
foreach var of varlist LevelGrade0{
	replace `var'=. if treatment2=="Gains" & grade2016!=1
	replace `var'=0 if treatment2=="Gains" & grade2016==1 & `var'==.
} 

foreach var of varlist KisLevel1 EngLevel1 MathLevel1{
	replace `var'=. if treatment2=="Gains" & grade2016!=2
	replace `var'=0 if treatment2=="Gains" & grade2016==2 & `var'==.
} 
foreach var of varlist KisLevel2 EngLevel2 MathLevel2{
	replace `var'=. if treatment2=="Gains" & grade2016!=3
	replace `var'=0 if treatment2=="Gains" & grade2016==3 & `var'==.
} 
*/


foreach archivo in "EL2016_Student_FinalRun_Mashindano_CLEAN" "EL2016_Student_FinalRun_StadiandControl_CLEAN"{
di "`archivo'"
merge 1:m studentid using "CreatedData/4 Intervention/TwaEL_2016/`archivo'.dta", keepus(studentid schoolid grade) update
/*
replace studentid="" if studentid=="999"
replace studentid="" if studentid=="-999"
replace studentid="" if studentid=="99"
replace studentid="" if studentid=="-99"

replace studentid="" if studentid=="-  999"
replace studentid="" if studentid=="--BLANK--"
replace studentid="" if studentid=="-.999"
replace studentid="" if studentid=="-99.9"
replace studentid="" if studentid=="9.99"
replace studentid=string(_n) if studentid==""
*/
replace SchoolID=schoolid if _merge==2
drop _merge schoolid
duplicates drop studentid, force
}
replace grade2016=grade if grade2016==.
merge m:1 SchoolID using "RawData/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2) update replace
drop _merge
merge m:1 SchoolID using "CommScripts/Kigoma Sample/KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update replace
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440

*136 is a split school
replace treatarm2=1 if SchoolID==136
replace treatment2="Levels" if SchoolID==136

drop if SchoolID==.
drop if treatment2=="Control"
drop if treatment2=="" /*right now is school 1002*/
drop if grade2016==.

saveold "CreatedData/4 Intervention/TwaEL_2016/StudentsTested_TWA_2016.dta", replace
collapse (count) Sutdents= SchoolID , by( treatment2 treatarm2 grade2016)
sort treatment2 treatarm2 grade2016
saveold "CreatedData/4 Intervention/TwaEL_2016/CountTested_TWA_2016.dta", replace
gen TotalStudents = sum( Sutdents)


scalar define BudgetGrd1_Mash=2/3*(scalar(TotalBudget)*Sutdents[1]/TotalStudents[6])
scalar define BudgetGrd2_Mash=2/3*(scalar(TotalBudget)*Sutdents[2]/TotalStudents[6])
scalar define BudgetGrd3_Mash=scalar(TotalBudget)*Sutdents[3]/TotalStudents[6] 

scalar define BudgetGrd1_Stadi=2/3*(scalar(TotalBudget)*Sutdents[4]/TotalStudents[6])
scalar define BudgetGrd2_Stadi=2/3*(scalar(TotalBudget)*Sutdents[5]/TotalStudents[6])
scalar define BudgetGrd3_Stadi=scalar(TotalBudget)*Sutdents[6]/TotalStudents[6] 

**MASHINDANO BUDGET PER SUBJECT ******
foreach subject in Kis Math{
	foreach grade in 1 2 {
		scalar define Budget`subject'_Grd`grade'_Mash=scalar(BudgetGrd`grade'_Mash)/2
	}
}

foreach subject in Kis Math Eng{
	foreach grade in 3{
		scalar define Budget`subject'_Grd`grade'_Mash=scalar(BudgetGrd`grade'_Mash)/3
	}
}

**STADI BUDGET PER SUBJECT ******


foreach subject in Kis Math{
	foreach grade in 1 2 {
		scalar define Budget`subject'_Grd`grade'_Stadi=scalar(BudgetGrd`grade'_Stadi)/2
	}
}

foreach subject in Kis Eng Math{
	foreach grade in 3{
		scalar define Budget`subject'_Grd`grade'_Stadi=scalar(BudgetGrd`grade'_Stadi)/3
	}
}

**STADI BUDGET PER SKILL ******

foreach skills in Silabi Maneno Sentenci{
	scalar define BudgetKis_`skills'_Grd1_Stadi=scalar(BudgetKis_Grd1_Stadi)/3
}


foreach skills in id uta bwa j t{
	scalar define BudgetMath_`skills'_Grd1_Stadi=scalar(BudgetMath_Grd1_Stadi)/5
}

foreach skills in Maneno Sentenci Aya Comp{
	scalar define BudgetKis_`skills'_Grd2_Stadi=scalar(BudgetKis_Grd2_Stadi)/4
}


foreach skills in bwa j t {
	scalar define BudgetMath_`skills'_Grd2_Stadi=scalar(BudgetMath_Grd2_Stadi)/3
}

foreach skills in Story Comp {
	scalar define BudgetKis_`skills'_Grd3_Stadi=scalar(BudgetKis_Grd3_Stadi)/2
}

foreach skills in Story Comp{
	scalar define BudgetEng_`skills'_Grd3_Stadi=scalar(BudgetEng_Grd3_Stadi)/2
}

foreach skills in j t z g{
	scalar define BudgetMath_`skills'_Grd3_Stadi=scalar(BudgetMath_Grd3_Stadi)/4
}

log close
