use "$base_out/ConsolidatedYr34/TWA_StudentLevelsBatches_2016.dta", clear
label define groupl 0 "Unknown ability"  1 "Group 1" 2 "Group 2" 3 "Group 3" 4 "Group 4" 5 "Group 5" 6 "Group 6" 7 "Group 7" 8 "Group 8" 9 "Group 9" 10 "Group 10"
label values KisLevel1 groupl
label values EngLevel1 groupl
label values MathLevel1 groupl
label values KisLevel2 groupl
label values EngLevel2 groupl
label values MathLevel2 groupl

drop if MathLevel2==0 | MathLevel1==0

foreach var in Kis Eng Math{
	foreach num in 1 2{
		estpost tabulate `var'Level`num', nototal
		esttab  using "$latexcodes/`var'Groups`num'_Gains_Yrs1.tex", cells("b(label(Students) fmt(%9.0gc)) pct(label(%) fmt(%9.2fc))") ///
		nonumber nomtitle noobs fragment replace nogaps nolines mlabels(none)  collabels(none)

	}
}

use "$base_out/ConsolidatedYr34/TWA_StudentLevelsBatches_2017.dta", clear
label define groupl 0 "Unknown ability"  1 "Group 1" 2 "Group 2" 3 "Group 3" 4 "Group 4" 5 "Group 5" 6 "Group 6" 7 "Group 7" 8 "Group 8" 9 "Group 9" 10 "Group 10"
label values KisLevel1 groupl
label values MathLevel1 groupl
label values KisLevel2 groupl
label values MathLevel2 groupl



foreach var in Kis Math{
	foreach num in 1 2{
		estpost tabulate `var'Level`num', nototal
		esttab  using "$latexcodes/`var'Groups`num'_Gains_Yrs2.tex", cells("b(label(Students) fmt(%9.0gc)) pct(label(%) fmt(%9.2fc))") ///
		nonumber nomtitle noobs fragment replace nogaps nolines mlabels(none)  collabels(none)

	}
}




use "$base_out/ConsolidatedYr34/TWA_SchoolLevelsBatches_2016.dta", clear
merge 1:m SchoolID using "$base_out/ConsolidatedYr34/TWA_StudentLevelsBatches_2016.dta"
drop if Grade!=1

label define groupl 0 "Unknown ability"  1 "Group 1" 2 "Group 2" 3 "Group 3" 4 "Group 4" 5 "Group 5" 6 "Group 6" 7 "Group 7" 8 "Group 8" 9 "Group 9" 10 "Group 10"
label values TotalQ groupl

estpost tabulate TotalQ, nototal
		esttab  using "$latexcodes/Groups0_Gains_Yrs1.tex", cells("b(label(Students) fmt(%9.0gc)) pct(label(%) fmt(%9.2fc))") ///
		nonumber nomtitle noobs fragment replace nogaps nolines mlabels(none)  collabels(none)
		


use "$base_out/ConsolidatedYr34/TWA_SchoolLevelsBatches_2017.dta", clear
merge 1:m SchoolID using "$base_out/ConsolidatedYr34/TWA_StudentLevelsBatches_2017.dta"
drop if Grade!=1

label define groupl 0 "Unknown ability"  1 "Group 1" 2 "Group 2" 3 "Group 3" 4 "Group 4" 5 "Group 5" 6 "Group 6" 7 "Group 7" 8 "Group 8" 9 "Group 9" 10 "Group 10"
label values TotalQ groupl

estpost tabulate TotalQ, nototal
		esttab  using "$latexcodes/Groups0_Gains_Yrs2.tex", cells("b(label(Students) fmt(%9.0gc)) pct(label(%) fmt(%9.2fc))") ///
		nonumber nomtitle noobs fragment replace nogaps nolines mlabels(none)  collabels(none)
		

