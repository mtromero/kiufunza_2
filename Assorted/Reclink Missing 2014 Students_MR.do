cd "C:\Users\Mauricio\Box Sync\01_KiuFunza\"
use "RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\Reclink Missing 2014 Students_withName_STADICONTROL.dta", clear
rename StuID_15 studentid
rename StuName stuname
rename SchoolID schoolid
reclink2 stuname schoolid using "RawData\12 Intervention_KFII\1 Endline 2015\7 FINAL DATA as of 3.7\EL2015_Student_FinalRun_StadiandControl_CLEAN.dta", idm(id) idu(name) gen(Stu_Name_Fuzzy) required(schoolid) wmatch(20 1) wnomatch(1 20) minscore (1)
drop if Stu_Name_Fuzzy==.
drop dup
bys id: gen dup=_N
drop if dup==2 & Grade+1!=grade
drop dup
bys id: gen dup=_n
drop if dup>1
drop dup
bys name : gen dup=_N
drop if dup==2 & Grade+1!=grade
drop dup
bys name: gen dup=_n
drop if dup>1
drop dup
rename studentid studentid_reclink
keep  studentid_reclink stuname schoolid Ustuname  id name Stu_Name_Fuzzy grade Grade
saveold "RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\ReclinkMatch.dta", replace

use "RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\Reclink Missing 2014 Students_withName_STADICONTROL.dta", clear
rename StuID_15 studentid
rename StuName stuname
rename SchoolID schoolid
reclink2 stuname schoolid using "RawData\12 Intervention_KFII\1 Endline 2015\7 FINAL DATA as of 3.7\EL2015_Student_FinalRun_StadiandControl_CLEAN.dta", idm(id) idu(name) gen(Stu_Name_Fuzzy) required(schoolid) wmatch(20 1) wnomatch(1 20) minscore(0.999) exclude("RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\ReclinkMatch.dta")
drop if Stu_Name_Fuzzy==.
capture drop dup
gsort id -Stu_Name_Fuzzy
by id: gen dup=_N if Stu_Name_Fuzzy!=.
drop if dup==2 & Grade+1!=grade
drop dup
by id: gen dup1=_n if Stu_Name_Fuzzy!=.
drop if dup1>1
drop dup
gsort name -Stu_Name_Fuzzy
by name: gen dup=_N if Stu_Name_Fuzzy!=.
drop if dup==2 & Grade+1!=grade
drop dup
by name: gen dup1=_n if Stu_Name_Fuzzy!=.
drop if dup1>1
drop dup
rename studentid studentid_reclink
keep  studentid_reclink stuname schoolid Ustuname  id name Stu_Name_Fuzzy grade Grade
append using "RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\ReclinkMatch.dta"
save "RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\ReclinkMatch.dta", replace


use "RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\Reclink Missing 2014 Students_withName_STADICONTROL.dta", clear
rename StuID_15 studentid
rename StuName stuname
rename SchoolID schoolid
reclink2 stuname schoolid using "RawData\12 Intervention_KFII\1 Endline 2015\7 FINAL DATA as of 3.7\EL2015_Student_FinalRun_StadiandControl_CLEAN.dta", idm(id) idu(name) gen(Stu_Name_Fuzzy) required(schoolid) wmatch(20 1) wnomatch(1 20) minscore(0.9975) exclude("RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\ReclinkMatch.dta")
drop if Stu_Name_Fuzzy==.
capture drop dup
gsort id -Stu_Name_Fuzzy
by id: gen dup=_N if Stu_Name_Fuzzy!=.
drop if dup==2 & Grade+1!=grade
drop dup
by id: gen dup1=_n if Stu_Name_Fuzzy!=.
drop if dup1>1
drop dup
gsort name -Stu_Name_Fuzzy
by name: gen dup=_N if Stu_Name_Fuzzy!=.
drop if dup==2 & Grade+1!=grade
drop dup
by name: gen dup1=_n if Stu_Name_Fuzzy!=.
drop if dup1>1
drop dup
rename studentid studentid_reclink
keep  studentid_reclink stuname schoolid Ustuname  id name Stu_Name_Fuzzy grade Grade
append using "RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\ReclinkMatch.dta"
save "RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\ReclinkMatch.dta", replace

use "RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\Reclink Missing 2014 Students_withName_STADICONTROL.dta", clear
rename StuID_15 studentid
rename StuName stuname
rename SchoolID schoolid
reclink2 stuname schoolid using "RawData\12 Intervention_KFII\1 Endline 2015\7 FINAL DATA as of 3.7\EL2015_Student_FinalRun_StadiandControl_CLEAN.dta", idm(id) idu(name) gen(Stu_Name_Fuzzy) required(schoolid) wmatch(20 1) wnomatch(2 1) minscore(0.9943) exclude("RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\ReclinkMatch.dta")
drop if Stu_Name_Fuzzy==.
capture drop dup
gsort id -Stu_Name_Fuzzy
by id: gen dup=_N if Stu_Name_Fuzzy!=.
drop if dup==2 & Grade+1!=grade
drop dup
by id: gen dup1=_n if Stu_Name_Fuzzy!=.
drop if dup1>1
drop dup
gsort name -Stu_Name_Fuzzy
by name: gen dup=_N if Stu_Name_Fuzzy!=.
drop if dup==2 & Grade+1!=grade
drop dup
by name: gen dup1=_n if Stu_Name_Fuzzy!=.
drop if dup1>1
drop dup
rename studentid studentid_reclink
keep  studentid_reclink stuname schoolid Ustuname  id name Stu_Name_Fuzzy grade Grade
append using "RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\ReclinkMatch.dta"
save "RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\ReclinkMatch.dta", replace



sort Stu_Name_Fuzzy  schoolid
order Stu_Name_Fuzzy schoolid studentid_reclink stuname Ustuname 
rename stuname stuname_2014
rename Ustuname stuname
tostring studentid_reclink, replace
gen IDLength=strlen( studentid_reclink)
replace studentid_reclink="0"+studentid_reclink if IDLength==7
gen IDLength_2=strlen( studentid_reclink)
tab IDLength_2
save "RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\Reclink Missing 2014 Students_STADICONTROL_linked.dta", replace




use "RawData\12 Intervention_KFII\1 Endline 2015\7 FINAL DATA as of 3.7\EL2015_Student_FinalRun_StadiandControl_CLEAN.dta", clear
*merge in missing 2014 students
merge m:1 grade stuname schoolid using "RawData\12 Intervention_KFII\1 Endline 2015\1 Student Names List\Reclink Missing 2014 Students_STADICONTROL_linked.dta", keepus (stuname stuname_2014 studentid_reclink)
drop if _merge==2
replace studentid=studentid_reclink if _merge==3
drop _merge
drop heldback
*Create Dummy Variable if student was held back this year
egen MissingGrd1=rownonmiss(grd1_*)
egen MissingGrd2=rownonmiss(grd2_*)
egen MissingGrd3=rownonmiss(grd3_*)

gen heldback=1 if ((FifthID=="0" & MissingGrd1==0) | (FifthID=="1" & MissingGrd2==0) | (FifthID=="2" & MissingGrd3==0)) & studentid!="-999"
replace heldback=0 if heldback!=1 & studentid!="-999"


bys studentid: gen duplicated=_n if studentid!="-999"
drop if duplicated>1 & !missing(duplicated)
drop duplicated
	
gen tested2015=1

save "RawData\12 Intervention_KFII\1 Endline 2015\8 FINAL DATA as of 3.10\EL2015_Student_FinalRun_StadiandControl_CLEAN.dta", replace
