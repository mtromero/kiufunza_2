clear mata
clear all
set matsize 1100
set seed 1234
local it = 100

use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear




global studentcontrol LagseenUwezoTests LagpreSchoolYN LagGender LagAge



foreach var in Z_kiswahili Z_kiingereza Z_hisabati{
reg Lag`var' $studentcontrol  i.LagGrade  i.(DistrictID##StrataScore##treatarm) 
predict LagResid_`var', resid
reg `var'_T2 $studentcontrol  i.LagGrade  i.(DistrictID##StrataScore##treatarm) 
predict Resid_`var'_T2, resid
}


keep LagResid* Resid* SchoolID treatment2 treatarm2 LagGrade
save "$base_out/ConsolidatedYr34/Student_Pcentile_NP.dta", replace

foreach time in T2{
foreach treat in Levels Gains{
foreach subject in Z_kiswahili Z_kiingereza Z_hisabati {
foreach grade in 1 2 3 0 4 {
use "$base_out/ConsolidatedYr34/Student_Pcentile_NP.dta", clear


drop if (treatment2!="`treat'" & treatment2!="Control")
if(`grade'!=0){
drop if LagGrade!=`grade'
}
if(`grade'==0){
drop if LagGrade==4
}

keep treatment2 SchoolID LagResid_`subject' Resid_`subject'_`time'
compress
save "$base_out/temp/TempBoot_Pass", replace

qui egen kernel_range = fill(.01(.01)1)
qui replace kernel_range = . if kernel_range>1
mkmat kernel_range if kernel_range != .
matrix diff = kernel_range
matrix x = kernel_range


quietly forvalues j = 1(1)`it' {
use "$base_out/temp/TempBoot_Pass", clear
bsample, strata(treatment2) cluster(SchoolID)


bysort treatment2: egen rank`subject' = rank(LagResid_`subject'), unique
bysort treatment2: egen max_rank`subject' = max(rank`subject')
bysort treatment2: gen LagPctileResid_`subject' = rank`subject'/max_rank`subject' 



egen kernel_range = fill(.01(.01)1)
qui replace kernel_range = . if kernel_range>1

*regressing endline scores on percentile rankings
lpoly Resid_`subject'_`time' LagPctileResid_`subject' if treatment2=="Control" , gen(xcon pred_con) at (kernel_range) nograph
lpoly Resid_`subject'_`time' LagPctileResid_`subject' if treatment2=="`treat'" , gen(xtre pred_tre) at (kernel_range) nograph
	
	
mkmat pred_tre if pred_tre != . 
mkmat pred_con if pred_con != . 
matrix diff = diff, pred_tre - pred_con

}

matrix diff = diff'



*each variable is a percentile that is being estimated (can sort by column to get 2.5th and 97.5th confidence interval)
svmat diff
keep diff* 

matrix conf_int = J(100, 2, 100)
qui drop if _n == 1

*sort each column (percentile) and saving 25th and 975th place in a matrix
forvalues i = 1(1)100{
sort diff`i'
matrix conf_int[`i', 1] = diff`i'[0.025*`it']
matrix conf_int[`i', 2] = diff`i'[0.975*`it']	
}

*******************Graphs for control, treatment, and difference using actual data (BASELINE)*************************************
use "$base_out/temp/TempBoot_Pass", clear
  
bysort treatment2: egen rank`subject' = rank(LagResid_`subject'), unique
bysort treatment2: egen max_rank`subject' = max(rank`subject')
bysort treatment2: gen LagPctileResid_`subject' = rank`subject'/max_rank`subject' 


egen kernel_range = fill(.01(.01)1)
qui replace kernel_range = . if kernel_range>1


lpoly Resid_`subject'_`time' LagPctileResid_`subject' if treatment2=="Control" , gen(xcon pred_con) at (kernel_range) nograph
lpoly Resid_`subject'_`time' LagPctileResid_`subject' if treatment2=="`treat'" , gen(xtre pred_tre) at (kernel_range) nograph

gen diff = pred_tre - pred_con

*variables for confidence interval bands
svmat conf_int

else if "`time'"=="T4" local name "low-stakes year 2"
else if "`time'"=="T2" local name "low-stakes year 1"


else if "`time'"=="T4" local name2 "2"
else if "`time'"=="T2" local name2 "1"

if "`subject'"=="Z_kiswahili" local name3 Swahili
else if "`subject'"=="Z_kiingereza"  local name3 English	
else if "`subject'"=="Z_hisabati" local name3 Math
else if "`subject'"=="Z_Index" local name3 "Index (PCA)"


if "`grade'"=="1" local name4 1
else if "`grade'"=="2"  local name4 2	
else if "`grade'"=="3" local name4 3
else if "`grade'"=="0" local name4 All
else if "`grade'"=="4" local name4 4

save "$base_out/ConsolidatedYr34/Lowess_Resid_Percentile_`subject'_`treat'_`time'_`grade'.dta", replace

graph twoway (line pred_con xcon, lcolor(blue) lpattern("--.....") legend(lab(1 "Control"))) ///
(line pred_tre xtre, lcolor(red) lpattern(longdash) legend(lab(2 "Treatment"))) ///
(line diff xcon, lcolor(black) lpattern(solid) legend(lab(3 "Difference"))) ///
(line conf_int1 xcon, lcolor(black) lpattern(shortdash) legend(lab(4 "95% Confidence Band"))) ///
(line conf_int2 xcon, lcolor(black) lpattern(shortdash) legend(lab(5 "95% Confidence Band"))) ///
,yline(0, lcolor(gs10)) xtitle(Percentile of Residual Baseline Score) ytitle(Residual Year `name2' Test Score) legend(order(1 2 3 4)) ///
title("Nonparametric treatment effects of `treat' by percentile of baseline score for `name3': `name' exam, grade `name4'", size(vsmall))
graph export "$results/Graphs/Lowess_Resid_Percentile_`subject'_`treat'_`time'_`grade'.pdf", as(pdf) replace


}
}
}
}

