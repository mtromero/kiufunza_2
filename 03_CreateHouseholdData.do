use "$basein/13 Baseline 2016/Final Data/Household Data_nopii/R9HHData_nopii.dta", clear 
keep if consnt==1
keep HHID SchoolID ltcbyn upid_student wrokyn matwll matflr matrof srcwtr srclgh srccok tlttyp tltshr asset_1- asset_8 mobilno bnkacc hsowsh rmsnum rmsslp lndown lndqnt tchrno prdyyn prdylt  lstctb_1 lstctb_2 lstctb_3 shlcev adtprt hmwhlp_1 hmwhlp_2 hmwhlp_3 hrshmw hmwcmp lrntlk tutrng tutprc brkfst  twknyn twpgsu twpgcg twpgdw twpgsf twpgcd codelt_1 codelt_2 codelt_3 codelt_4 swprof rdngsw rdngen mathqn_1 mathqn_2


gen wall_mud=0
replace wall_mud=1 if matwll==1
label variable 	wall_mud "Walls made out of earth/mud"

gen floor_mud=0
replace floor_mud=1 if matflr==1 
label variable 	floor_mud "Floor made out of earth/mud"


gen roof_durable=0
replace roof_durable=1 if matrof==4 | matrof==5 | matrof==6 | matrof==7 | matrof==8
label variable 	roof_durable "Roof made out of a durable material"

gen improveWater=((srcwtr>=1 & srcwtr<=6) | srcwtr==12)
gen improveSanitation=(tlttyp>=1 & tlttyp<=3)
gen HHElectricty=(srclgh>=1 & srclgh<=3)

gen KnowsTeachers=!(tchrno==-99)


foreach var in ltcbyn prdyyn  wrokyn asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8  bnkacc lndown shlcev adtprt lrntlk tutrng hmwcmp hrshmw hmwhlp_1 hmwhlp_2 hmwhlp_3{
recode `var' (2=0) (-99=.) (3=0) (4=0) (-98=.) (-97=.) (-96=.) (-95=.)
}
foreach var in  mathqn_1 mathqn_2{
recode `var' (2=0) (-99=0) (3=0) (4=0) (-98=0) (-97=0) (-96=0) (-95=0)
}
recode brkfst (3=0) (2=1)

recode rdngsw rdngen (1=1) (2/4=0)


rename upid_student upidst
compress
save "$base_out/ConsolidatedYr34/Household_Baseline2016.dta", replace


use "$basein/13 Baseline 2016/Final Data/Household Data_nopii/R9HHExpenditure_nopii.dta", clear
replace expn15=. if R9HHMemberID!=1 
collapse (count) HHSize=R9HHMemberID (sum) expn15, by(HHID) 

merge 1:1 HHID using "$base_out/ConsolidatedYr34/Household_Baseline2016.dta"
drop _merge
compress



pca wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8 
predict IndexPoverty, score
pca  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng
predict IndexEngagement, score
pca rdngsw rdngen mathqn_1 mathqn_2
predict IndexKowledge, score
keep HHID SchoolID HHSize expn15 upidst KnowsTeachers IndexPoverty IndexEngagement IndexKowledge wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng rdngsw rdngen mathqn_1 mathqn_2

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge

save "$base_out/ConsolidatedYr34/Household_Baseline2016.dta", replace

****************************************************
****************************************************
****************************************************

use "$basein/9 Baseline 2015/Final Data/Household/R7HHData_noPII.dta", clear 
keep if consnt==1
keep HHID SchoolID ltcbyn upid_student wrokyn matwll matflr matrof srcwtr srclgh srccok tlttyp tltshr asset_1- asset_8 mobilno bnkacc hsowsh rmsnum rmsslp lndown lndqnt tchrno prdyyn prdylt  lstctb_1 lstctb_2 lstctb_3 shlcev adtprt hmwhlp_1 hmwhlp_2 hmwhlp_3 hrshmw hmwcmp lrntlk tutrng tutprc brkfst  twknyn twpgsu twpgcg twpgdw twpgsf twpgcd codelt_1 codelt_2 codelt_3 codelt_4 swprof rdngsw rdngen mathqn_1 mathqn_2


gen wall_mud=0
replace wall_mud=1 if matwll==1
label variable 	wall_mud "Walls made out of earth/mud"

gen floor_mud=0
replace floor_mud=1 if matflr==1 
label variable 	floor_mud "Floor made out of earth/mud"


gen roof_durable=0
replace roof_durable=1 if matrof==4 | matrof==5 | matrof==6 | matrof==7 | matrof==8
label variable 	roof_durable "Roof made out of a durable material"

gen improveWater=((srcwtr>=1 & srcwtr<=6) | srcwtr==12)
gen improveSanitation=(tlttyp>=1 & tlttyp<=3)
gen HHElectricty=(srclgh>=1 & srclgh<=3)

gen KnowsTeachers=!(tchrno==-99)


foreach var in ltcbyn prdyyn  wrokyn asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8  bnkacc lndown shlcev adtprt lrntlk tutrng hmwcmp hrshmw hmwhlp_1 hmwhlp_2 hmwhlp_3{
recode `var' (2=0) (-99=.) (3=0) (4=0) (-98=.) (-97=.) (-96=.) (-95=.)
}
foreach var in  mathqn_1 mathqn_2{
recode `var' (2=0) (-99=0) (3=0) (4=0) (-98=0) (-97=0) (-96=0) (-95=0)
}
recode brkfst (3=0) (2=1)

recode rdngsw rdngen (1=1) (2/4=0)


rename upid_student upidst
compress
save "$base_out/ConsolidatedYr34/Household_Baseline2015.dta", replace


use "$basein/9 Baseline 2015/Final Data/Household/R7HHExpenditure_nopii.dta", clear
replace expn15=. if R7HHMemberID!=1 
collapse (count) HHSize=R7HHMemberID (sum) expn15, by(HHID) 

merge 1:1 HHID using "$base_out/ConsolidatedYr34/Household_Baseline2015.dta"
drop _merge
compress



pca wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8 
predict IndexPoverty, score
pca  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng
predict IndexEngagement, score
pca rdngsw rdngen mathqn_1 mathqn_2
predict IndexKowledge, score
keep HHID SchoolID HHSize expn15 upidst KnowsTeachers IndexPoverty IndexEngagement IndexKowledge wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng rdngsw rdngen mathqn_1 mathqn_2

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge

save "$base_out/ConsolidatedYr34/Household_Baseline2015.dta", replace



****************************************************
****************************************************
****************************************************
import delim "$basein\8 Endline 2014\Reference Data\R_R6_HHData.csv", clear 
keep hhid upid v1
rename hhid HHID
rename v1 SchoolID
merge 1:1 SchoolID HHID using "$basein\8 Endline 2014\Final Data\Household\R6HHData_noPII.dta" 
drop if _merge==1
drop _merge
keep if consnt==1
rename ctcbyn ltcbyn
rename crtctb_1 lstctb_1
rename crtctb_2 lstctb_2
rename crtctb_3 lstctb_3
rename tutcrt tutrng
keep upid HHID SchoolID ltcbyn upid_student wrokyn matwll matflr matrof srcwtr srclgh srccok tlttyp tltshr asset_1- asset_8 mobilno bnkacc hsowsh rmsnum rmsslp lndown lndqnt tchrno prdyyn prdylt  lstctb_1 lstctb_2 lstctb_3 shlcev adtprt hmwhlp_1 hmwhlp_2 hmwhlp_3 hrshmw hmwcmp lrntlk tutrng tutprc brkfst  twknyn twpgsu twpgcg twpgdw twpgsf twpgcd codelt_1 codelt_2 codelt_3 codelt_4 swprof rdngsw rdngen mathqn_1 mathqn_2
*merge 1:m  HHID SchoolID using  "$basein\8 Endline 2014\ID Linkage\R6_Student_HH_ID_Linkage_noPII", keepus(upid)
drop if HHID==.
*drop if _merge==2
*drop _merge
drop upid_student
rename upid upid_student

gen wall_mud=0
replace wall_mud=1 if matwll==1
label variable 	wall_mud "Walls made out of earth/mud"

gen floor_mud=0
replace floor_mud=1 if matflr==1 
label variable 	floor_mud "Floor made out of earth/mud"


gen roof_durable=0
replace roof_durable=1 if matrof==4 | matrof==5 | matrof==6 | matrof==7 | matrof==8
label variable 	roof_durable "Roof made out of a durable material"

gen improveWater=((srcwtr>=1 & srcwtr<=6) | srcwtr==12)
gen improveSanitation=(tlttyp>=1 & tlttyp<=3)
gen HHElectricty=(srclgh>=1 & srclgh<=3)

gen KnowsTeachers=!(tchrno==-99)


foreach var in ltcbyn prdyyn  wrokyn asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8  bnkacc lndown shlcev adtprt lrntlk tutrng hmwcmp hrshmw hmwhlp_1 hmwhlp_2 hmwhlp_3{
recode `var' (2=0) (-99=.) (3=0) (4=0) (-98=.) (-97=.) (-96=.) (-95=.)
}
foreach var in  mathqn_1 mathqn_2{
recode `var' (2=0) (-99=0) (3=0) (4=0) (-98=0) (-97=0) (-96=0) (-95=0)
}
recode brkfst (3=0) (2=1)

recode rdngsw rdngen (1=1) (2/4=0)


rename upid_student upidst
compress
save "$base_out/ConsolidatedYr34/Household_Endline2014.dta", replace


use "$basein/8 Endline 2014\Final Data\Household\R6HHExpenditure_nopii.dta", clear
rename expn13 expn15
replace expn15=. if R6HHMemberID!=1 
collapse (count) HHSize=R6HHMemberID (sum) expn15, by(HHID) 

merge 1:1 HHID using "$base_out/ConsolidatedYr34/Household_Endline2014.dta"
drop _merge
compress



pca wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8 
predict IndexPoverty, score
pca  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng
predict IndexEngagement, score
pca rdngsw rdngen mathqn_1 mathqn_2
predict IndexKowledge, score
keep HHID SchoolID HHSize expn15 upidst KnowsTeachers IndexPoverty IndexEngagement IndexKowledge wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng rdngsw rdngen mathqn_1 mathqn_2

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
replace upidst="R1"+upidst if substr(upidst,1,3)=="STU"
save "$base_out/ConsolidatedYr34/Household_Endline2014.dta", replace




****************************************************
****************************************************
****************************************************

use "$basein\6 Baseline 2014\Data\household\R4HHData_noPII.dta", clear 
keep if consnt==1

keep HHID SchoolID ltcbyn upid_student wrokyn matwll matflr matrof srcwtr srclgh srccok tlttyp tltshr asset_1- asset_8 mobilno bnkacc hsowsh rmsnum rmsslp lndown lndqnt tchrno prdyyn prdylt  lstctb_1 lstctb_2 lstctb_3 shlcev adtprt hmwhlp_1 hmwhlp_2 hmwhlp_3 hrshmw hmwcmp lrntlk tutrng tutprc brkfst  twknyn twpgsu twpgcg twpgdw twpgsf twpgcd codelt_1 codelt_2 codelt_3 codelt_4 swprof rdngsw rdngen mathqn_1 mathqn_2
gen wall_mud=0
replace wall_mud=1 if matwll==1
label variable 	wall_mud "Walls made out of earth/mud"

gen floor_mud=0
replace floor_mud=1 if matflr==1 
label variable 	floor_mud "Floor made out of earth/mud"


gen roof_durable=0
replace roof_durable=1 if matrof==4 | matrof==5 | matrof==6 | matrof==7 | matrof==8
label variable 	roof_durable "Roof made out of a durable material"

gen improveWater=((srcwtr>=1 & srcwtr<=6) | srcwtr==12)
gen improveSanitation=(tlttyp>=1 & tlttyp<=3)
gen HHElectricty=(srclgh>=1 & srclgh<=3)

gen KnowsTeachers=!(tchrno==-99)


foreach var in ltcbyn prdyyn  wrokyn asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8  bnkacc lndown shlcev adtprt lrntlk tutrng hmwcmp hrshmw hmwhlp_1 hmwhlp_2 hmwhlp_3{
recode `var' (2=0) (-99=.) (3=0) (4=0) (-98=.) (-97=.) (-96=.) (-95=.)
}
foreach var in  mathqn_1 mathqn_2{
recode `var' (2=0) (-99=0) (3=0) (4=0) (-98=0) (-97=0) (-96=0) (-95=0)
}
recode brkfst (3=0) (2=1)

recode rdngsw rdngen (1=1) (2/4=0)


rename upid_student upidst
compress
save "$base_out/ConsolidatedYr34/Household_Baseline2014.dta", replace


use "$basein\6 Baseline 2014\Data\household\R4HHExpenditure_nopii.dta", clear
rename expn14 expn15
replace expn15=. if R4HHMemberID!=1 
collapse (count) HHSize=R4HHMemberID (sum) expn15, by(HHID) 

merge 1:1 HHID using "$base_out/ConsolidatedYr34/Household_Baseline2014.dta"
drop _merge
compress



pca wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8 
predict IndexPoverty, score
pca  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng
predict IndexEngagement, score
pca rdngsw rdngen mathqn_1 mathqn_2
predict IndexKowledge, score
keep HHID SchoolID HHSize expn15 upidst KnowsTeachers IndexPoverty IndexEngagement IndexKowledge wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng rdngsw rdngen mathqn_1 mathqn_2

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge

save "$base_out/ConsolidatedYr34/Household_Baseline2014.dta", replace



****************************************************
****************************************************
****************************************************

use "$basein\3 Endline\Household\EHHData_noPII.dta", clear 
keep if consnt==1
rename workyn wrokyn
rename ctcbyn ltcbyn
rename crtctb_1 lstctb_1
rename crtctb_2 lstctb_2
rename crtctb_3 lstctb_3
rename tutcrt tutrng
keep HHID SchoolID ltcbyn  wrokyn matwll matflr matrof srcwtr srclgh srccok tlttyp tltshr asset_1- asset_8 mobilno bnkacc hsowsh rmsnum rmsslp lndown lndqnt tchrno prdyyn prdylt  lstctb_1 lstctb_2 lstctb_3 shlcev adtprt hmwhlp_1 hmwhlp_2 hmwhlp_3 hrshmw hmwcmp lrntlk tutrng tutprc brkfst  twknyn twpgsu twpgcg twpgdw twpgsf twpgcd codelt_1 codelt_2 codelt_3 codelt_4 swprof

merge 1:m SchoolID HHID using "$basein/3 Endline/Supplementing/R_EL_rHHData_noPII.dta", keepus(upid)
drop if _merge!=3
drop _merge


gen wall_mud=0
replace wall_mud=1 if matwll==1
label variable 	wall_mud "Walls made out of earth/mud"

gen floor_mud=0
replace floor_mud=1 if matflr==1 
label variable 	floor_mud "Floor made out of earth/mud"


gen roof_durable=0
replace roof_durable=1 if matrof==4 | matrof==5 | matrof==6 | matrof==7 | matrof==8
label variable 	roof_durable "Roof made out of a durable material"

gen improveWater=((srcwtr>=1 & srcwtr<=6) | srcwtr==12)
gen improveSanitation=(tlttyp>=1 & tlttyp<=3)
gen HHElectricty=(srclgh>=1 & srclgh<=3)

gen KnowsTeachers=!(tchrno==-99)


foreach var in ltcbyn prdyyn  wrokyn asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8  bnkacc lndown shlcev adtprt lrntlk tutrng hmwcmp hrshmw hmwhlp_1 hmwhlp_2 hmwhlp_3{
recode `var' (2=0) (-99=.) (3=0) (4=0) (-98=.) (-97=.) (-96=.) (-95=.)
}


recode brkfst (3=0) (2=1)


rename upid upidst
compress
save "$base_out/ConsolidatedYr34/Household_Endline2013.dta", replace


use "$basein\3 Endline\Household\EHHExpenditure_noPII.dta", clear
rename expn13 expn15
replace expn15=. if HHMemberID!=1 
collapse (count) HHSize=HHMemberID (sum) expn15, by(HHID) 

merge 1:1 HHID using "$base_out/ConsolidatedYr34/Household_Endline2013.dta"
drop _merge
compress


pca wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8 
predict IndexPoverty, score
pca  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng
predict IndexEngagement, score

keep HHID SchoolID  upidst KnowsTeachers IndexPoverty IndexEngagement  wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge

save "$base_out/ConsolidatedYr34/Household_Endline2013.dta", replace





****************************************************
****************************************************
****************************************************

use "$basein\1 Baseline\Household\HHData_noPII.dta", clear 
keep if consnt==1
rename workyn wrokyn
keep HHID  ltcbyn  wrokyn matwll matflr matrof srcwtr srclgh srccok tlttyp tltshr asset_1- asset_8 mobilno bnkacc hsowsh rmsnum rmsslp lndown lndqnt tchrno prdyyn prdylt  lstctb_1 lstctb_2 lstctb_3 shlcev adtprt hmwhlp_1 hmwhlp_2 hmwhlp_3 hrshmw hmwcmp lrntlk tutrng tutprc brkfst swprof rdngsw rdngen mathqn_1 mathqn_2
merge 1:m HHID using "$basein/1 Baseline/Supplementing/StudentIDLinkage_noPII.dta", keepus(SchoolID upid)
drop if _merge!=3
drop _merge


gen wall_mud=0
replace wall_mud=1 if matwll==1
label variable 	wall_mud "Walls made out of earth/mud"

gen floor_mud=0
replace floor_mud=1 if matflr==1 
label variable 	floor_mud "Floor made out of earth/mud"


gen roof_durable=0
replace roof_durable=1 if matrof==4 | matrof==5 | matrof==6 | matrof==7 | matrof==8
label variable 	roof_durable "Roof made out of a durable material"

gen improveWater=((srcwtr>=1 & srcwtr<=6) | srcwtr==12)
gen improveSanitation=(tlttyp>=1 & tlttyp<=3)
gen HHElectricty=(srclgh>=1 & srclgh<=3)

gen KnowsTeachers=!(tchrno==-99)


foreach var in ltcbyn prdyyn  wrokyn asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8  bnkacc lndown shlcev adtprt lrntlk tutrng hmwcmp hrshmw hmwhlp_1 hmwhlp_2 hmwhlp_3{
recode `var' (2=0) (-99=.) (3=0) (4=0) (-98=.) (-97=.) (-96=.) (-95=.)
}
foreach var in  mathqn_1 mathqn_2{
recode `var' (2=0) (-99=0) (3=0) (4=0) (-98=0) (-97=0) (-96=0) (-95=0)
}
recode brkfst (3=0) (2=1)

recode rdngsw rdngen (1=1) (2/4=0)


rename upid upidst
compress
save "$base_out/ConsolidatedYr34/Household_Baseline2013.dta", replace


pca wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8 
predict IndexPoverty, score
pca  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng
predict IndexEngagement, score
pca rdngsw rdngen mathqn_1 mathqn_2
predict IndexKowledge, score
keep HHID SchoolID  upidst KnowsTeachers IndexPoverty IndexEngagement IndexKowledge wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng rdngsw rdngen mathqn_1 mathqn_2

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
replace upidst="R1"+upidst if substr(upidst,1,3)=="STU"
save "$base_out/ConsolidatedYr34/Household_Baseline2013.dta", replace

