
use "CreatedData/4 Intervention/TwaEL_2015/Stadi_Grd1_Payments.dta", clear
gen treatment="Levels"
append using "CreatedData/4 Intervention/TwaEL_2015/Stadi_Grd2_Payments.dta"
replace treatment="Levels" if treatment==""
append using "CreatedData/4 Intervention/TwaEL_2015/Stadi_Grd3_Payments.dta"
replace treatment="Levels" if treatment==""
append using "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd1_Payments_v2.dta"
replace treatment="Gains" if treatment==""
append using "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd2_Payments_v2.dta"
replace treatment="Gains" if treatment==""
append using "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd3_Payments_v2.dta"
replace treatment="Gains" if treatment==""

preserve
drop if treatment!="Levels"
egen Payment_Total=rowtotal(Payment_Total*)
collapse (sum) Payment_Total, by(SchoolID)
foreach var of varlist Payment_Total{
replace `var'=`var'*1/5
}
saveold "CreatedData/4 Intervention/TwaEL_2015/HT_Payments_Stadi.dta",replace
restore

preserve
drop if treatment!="Gains"
egen Payment_Total=rowtotal(Payment_Total*)
collapse (sum) Payment_Total, by(SchoolID)
foreach var of varlist Payment_Total{
replace `var'=`var'*1/5
}
saveold "CreatedData/4 Intervention/TwaEL_2015/HT_Payments_Mash_v2.dta",replace
restore

collapse (sum) Payment_Total*, by(grade treatment)

egen Payment_Total=rowtotal(Payment_Total*)

mat A=[BudgetGrd1_Mash\BudgetGrd1_Stadi\BudgetGrd2_Mash\BudgetGrd2_Stadi\BudgetGrd3_Mash\BudgetGrd3_Stadi]

svmat A
rename A1 Budget
gen Diff=Budget-Payment_Total


saveold "CreatedData/4 Intervention/TwaEL_2015/Teacher_BudgetCheck.dta",replace

collapse (sum) Payment_Total
append using "CreatedData/4 Intervention/TwaEL_2015/HT_Payments_Stadi.dta"
append using "CreatedData/4 Intervention/TwaEL_2015/HT_Payments_Mash_v2.dta"
collapse (sum) Payment_Total

mat B=[ TotalBudget*6/5]
svmat B
rename B1 TotalBudget
gen Diff=TotalBudget-Payment_Total

saveold "CreatedData/4 Intervention/TwaEL_2015/Total_BudgetCheck.dta",replace


use "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd1Rankings_Students.dta", clear
gen treatment="Gains"
append using "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd2Rankings_Students.dta"
replace treatment="Gains" if treatment==""
append using "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd3Rankings_Students.dta"
replace treatment="Gains" if treatment==""
append using "CreatedData/4 Intervention/TwaEL_2015/Stadi_Grd1Pass_Students.dta"
replace treatment="Levels" if treatment==""
append using "CreatedData/4 Intervention/TwaEL_2015/Stadi_Grd2Pass_Students.dta"
replace treatment="Levels" if treatment==""
append using "CreatedData/4 Intervention/TwaEL_2015/Stadi_Grd3Pass_Students.dta"
replace treatment="Levels" if treatment==""

