capture log close
log using "CreatedData/4 Intervention/TwaEL_2015/Shaming.smcl", replace

use "CreatedData/4 Intervention/TwaEL_2015/EL2015_Student_FinalRun_Mashindano_CLEAN_quality.dta", clear
append using "CreatedData/4 Intervention/TwaEL_2015/EL2015_Student_FinalRun_StadiandControl_CLEAN_quality.dta"


tostring studentID, format(%08.0f) generate(StuID_15)
merge m:1 StuID_15 using "RawData/12 Intervention_KFII/1 Endline 2015/1 Student Names List/2015_Student_Name_List_nopii"
replace tested2015=0 if tested2015==.
drop _merge

******************************************************************************
******************************************************************************
******************************************************************************
**************** get randomization status
merge m:1 SchoolID using "RawData/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2) update replace
drop if _merge==2
drop _merge
merge m:1 SchoolID using "CommScripts/Kigoma Sample/KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update replace
drop if _merge==2
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440
tab treatarm2, gen(TD) 

******************************************************************************
******************************************************************************
******************************************************************************
**CHECK FOR MISSINGS ****
drop stream
sum tested2015 
scalar Tested=r(sum)
drop if tested2015==0
foreach var of varlist SchoolID grade Stream district  gender set start startampm volname end endampm{
	qui tabmiss `var' 
	display "`=scalar(r(sum)/Tested)' missing in variable `var'"
	tostring `var', replace
	encode `var'  , generate(`var'2)
	drop `var' 
	gen `var'=`var'2
	drop `var'2
}
preserve
gen n=1
collapse  (count ) n SchoolID grade Stream district  gender set start startampm volname end endampm, by(districtid)
saveold "CreatedData/4 Intervention/TwaEL_2015/percent_missing_idvars.dta", replace
restore

******************************************************************************
******************************************************************************
******************************************************************************
*** CHECK FOR NOT MISSINGS IN FIRST 2 QUESTIONS OF "Sehemu  A" in grade 1
sum tested2015 if grade==1
scalar Tested=r(sum)
foreach skill in si ma se{
	qui tabmiss grd1_kis_a_`skill'_1 if grade==1
	display "Skills `skill' in Swahili/Qs1 has `=scalar(r(sum)/Tested)' missing"
	qui tabmiss grd1_kis_a_`skill'_1 if grade==1
	display "Skills `skill' in Swahili/Qs2 has `=scalar(r(sum)/Tested)' missing"
}

foreach skill in l w se{
	qui tabmiss grd1_eng_a_`skill'_1 if grade==1
	display "Skills `skill' in English/Qs1 has `=scalar(r(sum)/Tested)' missing"
	qui tabmiss grd1_eng_a_`skill'_1 if grade==1
	display "Skills `skill' in English/Qs2 has `=scalar(r(sum)/Tested)' missing"
}

foreach skill in id uta bwa j t{
	qui tabmiss grd1_his_a_`skill'_1 if grade==1
	display "Skills `skill' in Math/Qs1 has `=scalar(r(sum)/Tested)' missing"
	qui tabmiss grd1_his_a_`skill'_1 if grade==1
	display "Skills `skill' in Math/Qs2 has `=scalar(r(sum)/Tested)' missing"
}

preserve
gen n=1
drop if grade!=1
collapse (count ) n grd1_kis_a_si_1 grd1_kis_a_si_2 grd1_kis_a_ma_1 grd1_kis_a_ma_2 grd1_kis_a_se_1 grd1_kis_a_se_2 ///
 grd1_eng_a_l_1 grd1_eng_a_l_2 grd1_eng_a_w_1 grd1_eng_a_w_2 grd1_eng_a_se_1 grd1_eng_a_se_2 ///
 grd1_his_a_uta_1 grd1_his_a_uta_2 grd1_his_a_bwa_1 grd1_his_a_bwa_2 grd1_his_a_t_1 grd1_his_a_t_2 ///
 , by(districtid)
saveold "CreatedData/4 Intervention/TwaEL_2015/percent_missing_firstq_grade1.dta", replace
restore

preserve
drop if grade!=2
gen n=1
collapse (count ) n  grd2_kis_a_ma_1 grd2_kis_a_ma_2 grd2_kis_a_se_1 grd2_kis_a_se_2 ///
   grd2_eng_a_w_1 grd2_eng_a_w_2 grd2_eng_a_se_1 grd2_eng_a_se_2 ///
   grd2_his_a_bwa_1 grd2_his_a_bwa_2 grd2_his_a_t_1 grd2_his_a_t_2 grd2_his_a_z_1 grd2_his_a_z_2  , by(districtid)
saveold "CreatedData/4 Intervention/TwaEL_2015/percent_missing_firstq_grade2.dta", replace
restore

preserve
gen n=1
drop if grade!=3
collapse (count ) n  grd3_kis_a_h grd3_eng_a_s grd3_his_a_j_1 grd3_his_a_j_2 ///
 grd3_his_a_t_1 grd3_his_a_t_2 ///
 grd3_his_a_z_1 grd3_his_a_z_2 ///
 grd3_his_a_g_1 grd3_his_a_g_2 ///
 , by(districtid)
saveold "CreatedData/4 Intervention/TwaEL_2015/percent_missing_firstq_grade3.dta", replace
restore

