clear
set more off

global path			"/Users/youdischipper/Dropbox (TWAWEZA)/Dropbox KiuFunza II/7 Codes/11 Intervention"
global itests		"/Users/youdischipper/Box Sync/01_KiuFunza/CreatedData/4 Intervention/TwaEL_2016"

global data			"/Users/youdischipper/Dropbox (TWAWEZA)/Dropbox KiuFunza II/5 Data"

global do			"$path/2 dofiles"
global output 		"$path/3 tables"
global refdata		"$data/Research data cr/9 RefData"
global treat 		"/Users/youdischipper/Box Sync/01_KiuFunza/RawData/TreatmentStatusYr3-4"


** Student numbers from survey

*2015
use "$data/Research data/2015 endline/Data/School Data/R8Grade_nopii.dta", clear
gen stu_tot_15 = s184+s185 
drop s184 s185
gen R10GradeID = R8GradeID
sort SchoolID R10GradeID

*2016
merge 1:1 SchoolID R10GradeID using "$data/Research data/2016 Endline/EDI Final Deliverable/Final Data/School Data/R10Grade_nopii.dta"
gen stu_tot_16 = s184+s185
drop s184 s185 _m

** ONLY keep FG 
keep if R8GradeID<4
gen grade = R8GradeID
save "$refdata/FG_stu_nr", replace

*** 2016 Intervention data
clear
cd "$itests"

use "All_Grd1Pass_Students.dta" 
append using "All_Grd2Pass_Students.dta" 
append using "All_Grd3Pass_Students.dta" 

tab grade
sum Eng*

egen Kis_pass = rowtotal(Kis_Silabi_Pass Kis_Maneno_Pass Kis_Sentenci_Pass Kis_Aya_Pass Kis_Comp_Pass Kis_Story_Pass)
egen Math_pass = rowtotal(Math_id_Pass Math_uta_Pass Math_bwa_Pass Math_j_Pass Math_t_Pass Math_z_Pass Math_g_Pass) 
egen Eng_pass = rowtotal(Eng_Story_Pass Eng_Comp_Pass)

sum Kis_pass Math_pass Eng_pass

tab treatarm2 grade, summarize(Kis_pass)
tab treatarm2 grade, summarize(Math_pass)
tab treatarm2, summarize(Eng_pass)

*** PERCENT TESTED

* Students tested
gen tested = 0
replace tested = 1 if Kis_pass!=. | Math_pass!=.  // this means there are test results for the student

collapse (sum) nr_tested = tested, by(SchoolID grade)

merge 1:1 SchoolID grade using "$refdata/FG_stu_nr"
keep if _m==3
drop _m
cd "$treat"
merge n:1 SchoolID using RandomizeStatus.dta
drop _m

gen tested_perc = nr_tested/stu_tot_16

table treatarm2, c(m tested_perc) row col format(%12.2fc)


** REGRESSION 
* percent tested
qui tab treatarm2, gen(treat)
regress tested_perc treat1 treat2 
