\documentclass[pdf]{beamer}
\usepackage{booktabs}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{dsfont}
\usepackage{bbm}
\usepackage[flushleft]{threeparttable}      % Option: Table notes
\usepackage{floatpag}
\usepackage{wrapfig}
\usepackage{relsize}
\usepackage{setspace}  
\usepackage{apacite}                     
\mode<presentation>{}
\usetheme{Warsaw}                  % Option: 2.0 and 1.5 spacing package.
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\let\oldtabular\tabular
\renewcommand{\tabular}{\tiny\oldtabular}
\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}
\title[Levels Vs Gains]{Teacher Performance Pay: Levels Vs Gains }

\author[Mbiti, Muralidharan \& Romero]{Isaac Mbiti \and Karthik Muralidharan \and Mauricio Romero }

\normalsize
\date{}
\begin{document}
\frame{\titlepage}







\AtBeginSection[]
{
   \begin{frame}
       \frametitle{Table of Contents}
       \tableofcontents[currentsection]
   \end{frame}
}


\begin{frame}[plain]{Background/Motivation}
\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{MDG2.pdf}
\end{figure}
\end{frame}


\begin{frame}[plain]{Background/Motivation}
\begin{figure}[H]
\centering
\includegraphics[height=\textheight]{fact1.pdf}
\end{figure}
\end{frame}

\begin{frame}[plain]{Background/Motivation}
\begin{itemize}[<+->]
\vfill \item Only 33\% of first graders are able to pronounce basic syllables
\vfill \item Only 70\% can count up to 5
\vfill \item Less than 50\% can identify which number is bigger: 9 or 6
\end{itemize}
\end{frame}



\begin{frame}[plain]{Background/Motivation}
\begin{itemize}
\vfill \item Teacher performance pay has received an increasing attention in recent years
\pause
\begin{itemize}[<+->]
\vfill \item Teacher salaries are the largest component of education budgets
\vfill \item Usual determinants of teacher salaries are not correlated with student performance \cite{BettingerForthcoming,Kane2008,Woessmann2011}
\vfill \item The evidence on teacher performance pay is at best mixed
\begin{itemize}[<+->]
\vfill \item Positive effects \cite{Glewwe2010,Lavy2002,Lavy2009,Muralidharan2011,Balch2015}
\vfill \item Little or no effects at all \cite{Goldhaber2012,Goodman2013,Springer2011}
\vfill \item Studies are not directly comparable
\end{itemize}
\end{itemize}
\end{itemize}
\end{frame}




\begin{frame}[plain]{This paper}
\begin{itemize}
\vfill \item Compare two incentive payment structures in the same context and with the same budget
\pause
\begin{itemize}
\vfill \item  Performance based on levels (absolute learning levels)
\pause
\begin{itemize}
\vfill \item Easy to implement (scale up) and communicate
\vfill \item Not optimal
\end{itemize}
\pause
\vfill \item  Performance based on gains
\pause
\begin{itemize}
\vfill \item Hard to implement (scale up) and communicate
\vfill \item Optimal
\end{itemize}
\end{itemize}
\vfill \item Heterogeneity by students ability
\end{itemize}
\end{frame}


\begin{frame}[plain]{Intervention - ``levels''}
\begin{itemize}
\vfill \item Teachers are rewarded for each student that pass grade-specific skills
\vfill \item Tournament: payment depends on total number of students that pass each skill (rewards for harder skills is higher)

\begin{equation}
P_j^s=\frac{X_s}{\sum\limits_{i \in T} 1_{T_i>b_s}} \sum\limits_{k \in J} 1_{T_k>b_s} 
\end{equation}
\vfill \item Focus on students that are ``in the money''
\end{itemize}
\end{frame}


\begin{frame}[plain]{Intervention - ``levels''}
\begin{table}[H]
  \centering
  \caption{Skills tested in the ``levels'' design\label{tab:skills}}
    \begin{tabular}{lll}
    \toprule
    \textbf{Swahili} & \textbf{English} & \textbf{Math} \\
    \midrule
    \multicolumn{3}{c}{\textit{Grade 1}} \\
    \addlinespace
    Letters & Letters & Count \\
    Words & Words & Numbers \\
    Sentences & Sentences & Inequalities \\
          &       & Add \\
          &       & Subtract \\
    \addlinespace
    \multicolumn{3}{c}{\textit{Grade 2}} \\
    \addlinespace
    Words & Words & Inequalities \\
    Sentences & Sentences & Add \\
    Paragraph & Paragraph & Subtract \\
          &       & Multiply \\
    \addlinespace
    \multicolumn{3}{c}{\textit{Grade 3}} \\
    \addlinespace
    Paragraph & Paragraph & Add \\
    Story & Story & Subtract \\
    Comprehension & Comprehension & Multiply \\
          &       & Divide \\
    \bottomrule
    \end{tabular}%
\end{table}%
\end{frame}

\begin{frame}[plain]{Intervention - ``gains''}
\begin{itemize}
\vfill \item Based on the work of \citeA{Barlevy2012} (induces teachers to exert socially optimal levels of effort)
\vfill \item Place students in bins according to initial levels of learning (handicapping)
\vfill \item Rank students at the end of the year, within each bin
\vfill \item Pay teachers according to the rank of their students
\vfill \item For a student in the top $1\%$ a teacher gets 99 points, and for a student in the bottom 1\% he gets no points
\begin{itemize}
\vfill \item For a student in the top 1\% a teacher receives \$1.77
\vfill \item For a student in the top 50\% a teacher receives \$0.89
\end{itemize}
\end{itemize}
\end{frame}




\begin{frame}[plain]{Design}
\begin{figure}[H]
\centering
\caption{Districts in Tanzania from which schools are selected\label{fig:mapa}}
\includegraphics[width=0.7\textwidth]{map_rct_1.pdf}
\end{figure}
\end{frame}



\begin{frame}[plain]{Design}
\begin{itemize}
\item In each district we pick 18 government primary schools
\item The sample of 180 schools was taken from a previous RCT (needed baseline scores from students)
\item From each district 6 schools were assigned to the ``levels'' treatment, 6 schools to the ``gains'' treatment and 6 schools were left as controls.
\end{itemize}
\end{frame}
\begin{frame}[plain]{Design}
\begin{table}[H]
  \centering
  \caption{Add caption}
    \begin{tabular}{rrrrrr}
    \toprule
          &       & \multicolumn{4}{c}{Current treatment} \\
    \midrule
          &       & Levels   & Gains & Control & Total \\
    Previous RCT & C1   & 40    & 20    & 10    & 70 \\
          & C2 & 10    & 30    & 30    & 70 \\
          & C3 & 10    & 10    & 20    & 40 \\
          & Total & 60    & 60    & 60    & 180 \\
    \bottomrule
    \end{tabular}%
  \label{tab:addlabel}%
\end{table}%
\end{frame}


\begin{frame}[plain,label=balance]{Design validity}
\begin{itemize}
\vfill \item No difference in baseline characteristics by 
\begin{itemize}
\vfill \item \hyperlink{balance_student}{\beamergotobutton{student}}
\vfill \item \hyperlink{balance_school}{\beamergotobutton{household}}
\vfill \item \hyperlink{balance_school}{\beamergotobutton{school}}
\vfill \item \hyperlink{balance_teacher}{\beamergotobutton{teachers}}
\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}[plain,label=balance]{Hypothesis - main outcomes}

\begin{enumerate}
\vfill\item $H_a$ ($H_0$): `levels' treatment has (no) positive impact on test scores
\vfill\item  $H_a$ ($H_0$): `gains' treatment has (no) positive impact on test scores
\vfill\item  $H_a$ ($H_0$): `gains' treatment has (no) greater impact on test scores than the `levels' treatment
\end{enumerate}

\end{frame}


\begin{frame}[plain,label=balance]{Hypothesis - heterogeneity}
$H_a$ ($H_0$): Treatment - `gains' and `levels' - impact on knowledge is different (the same) for all students.
\begin{itemize}
\vfill\item How to measure who is ``in the money'' in the ``levels'' treatment?
\end{itemize}
\end{frame}


\begin{frame}[plain,label=balance]{Hypothesis - mechanisms}

\begin{enumerate}
\vfill\item $H_a$ ($H_0$): Treatment - `gains' and `levels' - has (no) impact on teacher's behavior (classroom presence, assignments, tutoring, etc.) in focal subjects in focal years.
\vfill\item $H_a$ ($H_0$): Treatment - `gains' and `levels' - has (no) positive impact on text book and teaching input expenditures
\vfill\item $H_a$ ($H_0$): Treatment - `gains' and `levels' - has (no) impact on the number of hours taught in different subjects
\vfill\item $H_a$ ($H_0$): Treatment - `gains' and `levels' - has (no) impact on teacher assignments across subjects and grades
\end{enumerate}

\end{frame}





\renewcommand*{\refname}{} % This will define heading of bibliography to be empty, so you can...
\begin{frame}[allowframebreaks]{Bibliography}
	\bibliographystyle{apacite}
\bibliography{TanBib}
\end{frame}

\begin{frame}[plain,label=extrapower]{Power}
\begin{table}[H]
  \centering
  \caption{Add caption}
    \begin{tabular}{rrrrr}
    \toprule
    Size  & Power & MDE COD & MDE Gains & MD Difference \\
    \midrule
    0.5   & 0.8   & 0.16 & 0.15 & 0.15 \\
    0.1   & 0.8   & 0.20 & 0.20 & 0.19 \\
    0.5   & 0.9   & 0.19 & 0.17 & 0.18 \\
    0.1   & 0.9   & 0.23 & 0.21 & 0.22 \\
    \bottomrule
    \end{tabular}%
  \label{tab:addlabel}%
\end{table}%
\end{frame}




\begin{frame}[plain,label=balance_student]{Baseline descriptive/balance: students}
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{l*{1}{cccc}}
\toprule
                    &     Control&       Gains&      Levels&     p-value         \\
\midrule
\multicolumn{5}{c}{\textbf{Panel A: Student characteristics}}\\
\addlinespace
Age                 &        8.57&        8.63&        8.62&        0.66         \\
                    &     (0.082)&     (0.084)&     (0.088)&                     \\
Gender              &        0.51&        0.48&        0.52&       0.066\sym{*}  \\
                    &     (0.025)&     (0.027)&     (0.028)&                     \\
Attend pre-school   &        0.67&        0.64&        0.68&        0.51         \\
                    &     (0.053)&     (0.049)&     (0.055)&                     \\
Swahili test score  &       -0.25&       -0.30&       -0.24&        0.64         \\
                    &     (0.095)&      (0.10)&      (0.11)&                     \\
English test score  &      -0.087&      -0.073&      -0.039&        0.78         \\
                    &     (0.079)&     (0.092)&     (0.097)&                     \\
Math test score     &       0.084&       0.022&       0.097&        0.51         \\
                    &     (0.092)&     (0.090)&      (0.10)&                     \\
Other subjects test score&      -0.040&       -0.15&      -0.056&        0.28         \\
                    &      (0.10)&      (0.11)&      (0.12)&                     \\
\bottomrule
\multicolumn{5}{c}{\footnotesize \specialcell{Standard errors, clustered at the school level, in parenthesis \\ \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{frame}

\begin{frame}[plain,label=balance_school]{Baseline descriptive/balance: schools and households}
\centering
\resizebox{!}{0.5\textheight}{%
\begin{tabular}{l*{1}{cccc}}
\toprule
                    &     Control&       Gains&      Levels&     p-value         \\
\midrule
\multicolumn{5}{l}{\textbf{Panel A: Schools}}\\
Grd 2 enrollment    &       233.3&       231.8&       241.0&        0.81         \\
                    &      (36.0)&      (36.5)&      (43.0)&                     \\
Grd 1 enrollment    &       233.9&       239.4&       251.9&        0.50         \\
                    &      (46.3)&      (47.5)&      (53.2)&                     \\
Grd 3 enrollment    &       173.2&       178.9&       194.0&        0.14         \\
                    &      (26.9)&      (26.8)&      (30.9)&                     \\
Total enrollment    &      1120.6&      1101.7&      1188.0&        0.41         \\
                    &     (170.7)&     (169.1)&     (198.4)&                     \\
Kitchen             &       0.011&      -0.040&      -0.023&        0.78         \\
                    &     (0.057)&     (0.057)&     (0.067)&                     \\
Library             &        0.42&        0.38&        0.41&        0.76         \\
                    &      (0.13)&      (0.13)&      (0.13)&                     \\
Urban               &       0.083&       0.055&       0.085&        0.82         \\
                    &     (0.086)&     (0.098)&      (0.10)&                     \\
Classes outside     &        0.12&        0.20&        0.18&        0.36         \\
                    &      (0.10)&      (0.10)&      (0.11)&                     \\
Electricity         &       0.050&      -0.040&      -0.050&        0.13         \\
                    &     (0.040)&     (0.047)&     (0.045)&                     \\
Computers           &      -0.032&      -0.013&     -0.0022&        0.61         \\
                    &     (0.022)&     (0.020)&     (0.033)&                     \\
Preschool           &        0.85&        0.79&        0.82&        0.57         \\
                    &     (0.100)&      (0.11)&     (0.096)&                     \\
\midrule
\multicolumn{5}{l}{\textbf{Panel B: Households}}\\
Breadwinner employed&        0.91&        0.94&        0.84&       0.037\sym{**} \\
                    &     (0.055)&     (0.055)&     (0.065)&                     \\
TV                  &       0.061&       0.044&       0.063&        0.71         \\
                    &     (0.038)&     (0.037)&     (0.042)&                     \\
Bicycle             &        0.63&        0.60&        0.66&        0.38         \\
                    &     (0.074)&     (0.073)&     (0.075)&                     \\
Motorbike           &       0.049&       0.034&       0.025&        0.70         \\
                    &     (0.028)&     (0.026)&     (0.028)&                     \\
Refrigerator        &     -0.0077&     -0.0018&      0.0050&        0.87         \\
                    &     (0.014)&     (0.020)&     (0.025)&                     \\
Mobile Phone        &        0.68&        0.68&        0.71&        0.72         \\
                    &     (0.069)&     (0.070)&     (0.080)&                     \\
Exp. in child's education&     23694.5&     22474.3&     25775.7&        0.31         \\
                    &    (3516.6)&    (3415.5)&    (3487.3)&                     \\
Electricity         &        0.17&        0.17&        0.16&        0.99         \\
                    &     (0.050)&     (0.056)&     (0.055)&                     \\
\bottomrule
\multicolumn{5}{c}{\footnotesize \specialcell{Standard errors, clustered at the school level, in parenthesis \\ \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{frame}

\begin{frame}[plain,label=balance_teacher]{Baseline descriptive/balance: teachers}
\centering
\resizebox{!}{0.25\textheight}{%
\begin{tabular}{l*{1}{cccc}}
\toprule
                    &     Control&       Gains&      Levels&     p-value         \\
\midrule
Male                &        0.58&        0.53&        0.47&        0.13         \\
                    &     (0.074)&     (0.073)&     (0.081)&                     \\
Yr born             &      1976.3&      1978.4&      1978.1&        0.14         \\
                    &      (1.63)&      (1.46)&      (1.70)&                     \\
Yr started teaching &      2001.1&      2003.7&      2003.3&       0.068\sym{*}  \\
                    &      (1.63)&      (1.32)&      (1.67)&                     \\
Yr started teaching at this school&      2006.1&      2007.9&      2007.2&        0.10         \\
                    &      (1.29)&      (1.24)&      (1.30)&                     \\
Private school experience&       0.037&       0.016&       0.048&       0.051\sym{*}  \\
                    &     (0.021)&     (0.022)&     (0.026)&                     \\
Travel time (mins)  &        18.9&        21.4&        20.1&        0.77         \\
                    &      (5.95)&      (7.21)&      (7.08)&                     \\
Tertiary education  &        0.67&        0.71&        0.69&        0.79         \\
                    &      (0.10)&     (0.097)&      (0.11)&                     \\
\bottomrule
\multicolumn{5}{l}{\footnotesize \specialcell{Standard errors, clustered at the school level, in parenthesis \\ \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{frame}

\end{document}

