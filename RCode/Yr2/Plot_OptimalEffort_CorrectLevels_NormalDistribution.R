rm(list=ls())
# ###Assumptions
# Cost=function(x) x^2
# #all with error terms normal N(0,1)
library(nleqslv) #to solve non linear equation systems
scale=1
setwd("C:/Users/Mauricio/Dropbox/KF_Draft/KFII")


#First order codnition ignoring own effect on pass rate, but taking into account in equilibrium pass rate depends on effort
#Note that this is correct as it incorporates the effect of all students on the pass rate
first_order_levels=function(effort,gamma,Initial_Level,PI_Levels=rep(N*CK,length(T_Levels)),T_Levels,N=1000,CK=30) {
  if(length(PI_Levels)!=length(T_Levels)) stop("Length of payoffs is not the same as thresholds")
  if(length(effort)!=length(gamma)) stop("Length of effort is not the same as productivity")
  if(length(effort)!=length(Initial_Level)) stop("Length of effort is not the same as initial levels")
  
  vector_total=rep(0,length(effort))
  for(level in 1:length(T_Levels)){
    vector_total=vector_total+gamma*dnorm(T_Levels[level]-Initial_Level-gamma*effort)*PI_Levels[level]/sum(N*CK*(1-pnorm(T_Levels[level]-Initial_Level-gamma*effort)))
  }
  return(vector_total-2*effort)
}



#The equilibrium effort for levels is:
effort_gamma_levels=function(gamma,Initial_Level,PI_Levels=rep(N*CK,length(T_Levels)),T_Levels,N=1000,CK=30) {
  effort_noslack=nleqslv(rep(0,length(Initial_Level)), first_order_levels,gamma=gamma,Initial_Level=Initial_Level,PI_Levels=PI_Levels,T_Levels=T_Levels,N=N,CK=CK)
  if(effort_noslack$termcd!=1) stop("fuck")
   return(effort_noslack$x)
}




NumTeachers=1000
ClassSize=dbinom(seq(1,17), 17, 0.5)*17
seq_intiail=seq(-4,4,0.5)
Prize=NumTeachers*sum(ClassSize)
pdf("LaTeX/figures/Levels_figure1_Samegamma_binomdistr.pdf")
plot(seq_intiail,effort_gamma_levels(gamma=rep(1,length(seq_intiail)),Initial_Level=seq_intiail,
                                     PI_Levels=Prize,T_Levels=-1,N=NumTeachers,CK=ClassSize),
     type="l",col=4,xlab="Initial levels of learning",ylab="Effort",cex.lab=1.5,cex.axis=1.5,
     lwd=3,bty="n",ylim=c(0,0.7),lty=4)
lines(seq_intiail,effort_gamma_levels(gamma=rep(1,length(seq_intiail)),Initial_Level=seq_intiail,PI_Levels=Prize,T_Levels=0,N=NumTeachers,CK=ClassSize),
     type="l",col=3,lwd=3,lty=3)
lines(seq_intiail,effort_gamma_levels(gamma=rep(1,length(seq_intiail)),Initial_Level=seq_intiail,PI_Levels=Prize,T_Levels=1,N=NumTeachers,CK=ClassSize),
      type="l",col=2,lwd=3,lty=2)
lines(seq_intiail,effort_gamma_levels(gamma=rep(1,length(seq_intiail)),Initial_Level=seq_intiail,PI_Levels=rep(Prize/3,3),T_Levels=c(-1,0,1),N=NumTeachers,CK=ClassSize),
      type="l",col=1,lwd=3,lty=1)
legend("topleft",c("Threshold=-1","Threshold=0","Threshold=1","Multiple thresholds"),
       col=4:1,lty=4:1,lwd=3,bty ="n",cex=1.5)
dev.off()



gamma_seq=seq(0,by=0.25,length.out=length(seq_intiail))
pdf("LaTeX/figures/Levels_figure1_Differentgamma_binomdistr.pdf")
plot(seq_intiail,effort_gamma_levels(gamma=gamma_seq,Initial_Level=seq_intiail,
                                     PI_Levels=Prize,T_Levels=-1,N=NumTeachers,CK=ClassSize),
     type="l",col=4,xlab="Initial levels of learning",ylab="Effort",
     cex.lab=1.5,cex.axis=1.5,lwd=3,bty="n",ylim=c(0,0.8),lty=4)
lines(seq_intiail,effort_gamma_levels(gamma=gamma_seq,Initial_Level=seq_intiail,PI_Levels=Prize,T_Levels=0,N=NumTeachers,CK=ClassSize),
      type="l",col=3,lwd=3,lty=3)
lines(seq_intiail,effort_gamma_levels(gamma=gamma_seq,Initial_Level=seq_intiail,PI_Levels=Prize,T_Levels=1,N=NumTeachers,CK=ClassSize),
      type="l",col=2,lwd=3,lty=2)
lines(seq_intiail,effort_gamma_levels(gamma=gamma_seq,Initial_Level=seq_intiail,PI_Levels=rep(Prize/3,3),T_Levels=c(-1,0,1),N=NumTeachers,CK=ClassSize),
      type="l",col=1,lwd=3,lty=1)
legend("topleft",c("Threshold=-1","Threshold=0","Threshold=1","Multiple thresholds"),
       col=4:1,lty=4:1,lwd=3,bty ="n",cex=1.5)
dev.off()

         
#Then the equilibrium effort for gains:
effort_gamma_gains=function(gamma,PI_Gains=1,N=1000) (N-1)*PI_Gains*gamma*dnorm(0,mean=0,sd=2*scale)/2
pdf("LaTeX/figures/Gains_figure1_Samegamma.pdf")
plot(seq_intiail,rep(sapply(1,effort_gamma_gains,PI_Gains=1*2/1000,N=1000),length(seq_intiail)),lwd=3,lty=1,
     type="l",col=1,xlab="Initial levels of learning",ylab="Effort",cex.lab=1.5,cex.axis=1.5,bty="n")
dev.off()

effort_gamma_gains=function(gamma,PI_Gains=1,N=1000) (N-1)*PI_Gains*gamma*dnorm(0,mean=0,sd=2*scale)/2
pdf("LaTeX/figures/Gains_figure1_Differentgamma.pdf")
plot(seq_intiail,sapply(gamma_seq,effort_gamma_gains,PI_Gains=1*2/1000,N=1000),lwd=3,lty=1,
     type="l",col=1,xlab="Initial levels of learning",ylab="Effort",cex.lab=1.5,cex.axis=1.5,bty="n")
dev.off()

# 
# pdf("LaTeX/figures/Gains_figure1_Differentgamma.pdf")
# par(xpd=T,mar=c(5, 4, 4, 3) + 0.1)
# plot(seq_intiail,rep(sapply(4,effort_gamma_gains,PI_Gains=1,N=1000),length(seq_intiail)),lwd=3,lty=1,
#      type="l",col=1,xlab="Initial levels of learning",ylab="Effort",cex.lab=1.5,cex.axis=1.5,ylim=c(0,410),bty="n")
# for(gam in seq(3,length(gamma_seq),by=2)){
#   lines(seq_intiail,rep(sapply(gamma_seq[gam],effort_gamma_gains,PI_Gains=1,N=1000),length(seq_intiail)),lwd=3,lty=(gam-1)/2,col=(gam-1)/2)
#   text(tail(seq_intiail,1)+0.2,sapply(gamma_seq[gam],effort_gamma_gains,PI_Gains=1,N=1000),
#        bquote(gamma==~.(gamma_seq[gam])),
#        pos=4,col=1,cex=1.2,offset=0)
# }
# dev.off()

