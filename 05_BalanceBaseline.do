capt prog drop my_ptest
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric)  [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_1 mu_2 mu_3  se_1 se_2 se_3 d_p
capture drop TD*
tab `by', gen(TD)
foreach var of local varlist {

sum `var' if TD1==1
 mat `mu_1' = nullmat(`mu_1'), r(mean)
 mat `se_1' = nullmat(`se_1'), r(sd)
sum `var' if TD2==1
 mat `mu_2' = nullmat(`mu_2'), r(mean)
 mat `se_2' = nullmat(`se_2'), r(sd)
sum `var' if TD3==1
 mat `mu_3' = nullmat(`mu_3'), r(mean)
 mat `se_3' = nullmat(`se_3'), r(sd)
 
reghdfe `var' TD2 TD3 `if', vce(cluster `clus_id') ab(DistrictID##StrataScore##treatarm)
 test (0 == _b[TD2]== _b[TD3])
 mat `d_p'  = nullmat(`d_p'),r(p)
}
foreach mat in mu_1 mu_2 mu_3 se_1 se_2 se_3 d_p {
 mat coln ``mat'' = `varlist'
}
eret local cmd "my_ptest"
foreach mat in mu_1 mu_2 mu_3 se_1 se_2 se_3 d_p {
 eret mat `mat' = ``mat''
}
end

use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear

label var preSchoolYN_T1 "ECE"
label var Z_kiswahili_T1 "Kiswahili test score"
label var Z_kiingereza_T1 "English test score"
label var Z_hisabati_T1 "Math test score"
label var Z_sayansi_T1 "Other subjects test score"
label var attendance_T1 "Tested in yr0"
label var attendance_T2 "Tested in yr1"
label var attendance_T5 "Tested in yr2"

label var Gender_T1 "Male"

   

cdfplot Z_kiswahili_T1, by(treatment2) legend(on order(1 "Control" 2 "Gains" 3 "Levels"))
graph export "$graphs/Z_kiswahili_T1_CDF.pdf", replace

graph export "$graphs/Z_kiingereza_T1_T1_CDF.pdf", replace

cdfplot Z_hisabati_T1, by(treatment2)  legend(on order(1 "Control" 2 "Gains" 3 "Levels"))
graph export "$graphs/Z_hisabati_T1_T1_CDF.pdf", replace


eststo clear
eststo: xi: my_ptest Age_T1 Gender_T1  Z_kiswahili_T1 Z_kiingereza_T1 Z_hisabati_T1 attendance_T1 attendance_T2 attendance_T5 , by(treatment2) clus_id(SchoolID) /*controls(DistrictID StrataScore treatarm)*/
esttab using "$latexcodes/summaryStudentBase.tex", label replace fragment nogaps nolines ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) d_p(star pvalue(d_p) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2( fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) .") ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

foreach var in  attendance_T1 attendance_T2 attendance_T5{
	replace `var'=`var'*100
}

foreach var in Age_T1 attendance_T1 attendance_T2 attendance_T5{
	sum `var'
	local tempm=string(r(mean), "%9.2gc")
	file open newfile using "$latexcodes/`var'_summary.tex", write replace
	file write newfile "`tempm'"
	file close newfile
}




use "$base_out/ConsolidatedYr34/School.dta", clear


label var s1451_T1 "Kitchen"
label var s1452_T1 "Library"
label var s1453_T1 "Playground"
label var s1454_T1 "Staff room"
label var s1455_T1 "Outer wall"
label var s1456_T1 "Newspaper"

label var computersYN_T1 "Computers"
label var s120_T1  "Electricity"
label var s118_T1 "Classes outside"
label var PipedWater_T1 "Piped Water"
label var NoWater_T1 "No Water"
label var s188_T1 "Breakfast"
label var s175_T1 "Preschool"
label var s200_T1 "Track students"
label var s108_T1 "Urban"
label var s191_T1 "Lunch"
label var SingleShift_T1 "Single shift"
label var enrollment2015_T1 "Total enrollment"
label var enrollment1_T1 "Grd 1 enrollment"
label var enrollment2_T1 "Grd 2 enrollment"
label var enrollment3_T1 "Grd 3 enrollment"


pca s1451_T1-s1455_T1 
predict IndexFacilities, score
label var IndexFacilities "Facilities index (PCA)"
sum IndexFacilities if treatarm==1
replace IndexFacilities=(IndexFacilities-r(mean))/r(sd)

eststo clear
xi: my_ptest  enrollment2015_T1  IndexFacilities s108_T1   SingleShift_T1, by(treatment2) clus_id(SchoolID) /*controls(DistrictID StrataScore treatarm)*/
esttab using "$latexcodes/summarySchoolBase.tex", label replace fragment nogaps nolines  ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) d_p(star pvalue(d_p) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2( fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) .") ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

foreach var in enrollment2015_T1 s108_T1 PipedWater_T1 SingleShift_T1{
	sum `var'
	local tempm=string(r(mean), "%9.2gc")
	file open newfile using "$latexcodes/`var'_summary.tex", write replace
	file write newfile "`tempm'"
	file close newfile
}


use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
drop if wrokyn==.
drop if LagGrade==0
drop if GradeID_T1==4
label var wrokyn "Breadwinner employed"
label var asset_1 "Radio"
label var asset_2 "TV"
label var asset_3 "Bicycle"
label var asset_4 "Car"
label var asset_5 "Motorbike"
label var asset_6 "Refrigerator"
label var asset_7 "Watch/Clock"
label var asset_8 "Mobile Phone"
label var expn15 "Exp. in child's education"
label var ltcbyn "Give to school (kind or cash)"
label var wall_mud "Wall made out of mud"
label var floor_mud "Floor made out of mud"
label var roof_durable "Roof is durable"
label var improveWater "Improved water source"
label var improveSanitation "Improved toilet"
label var HHElectricty "Electricity"

eststo clear
xi: my_ptest asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8 wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty, by(treatment2) clus_id(SchoolID) /*controls(DistrictID StrataScore treatarm)*/
esttab using "$latexcodes/summaryHHBase.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Control" "Gains" "Levels" "p-value")  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) d_p(star pvalue(d_p))" "se_1(fmt(%9.2fc) par) se_2( fmt(%9.2fc) par) se_3(fmt(%9.2fc) par)  .") ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

label var IndexPoverty "Poverty index (PCA)"



eststo clear
xi: my_ptest IndexPoverty  , by(treatment2) clus_id(SchoolID) /*controls(DistrictID StrataScore treatarm)*/
esttab using "$latexcodes/summaryHHBase_short.tex", label replace nogaps nolines ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none) fragment  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) d_p(star pvalue(d_p))" "se_1(fmt(%9.2fc) par) se_2( fmt(%9.2fc) par) se_3(fmt(%9.2fc) par)  .") ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")


use "$base_out/ConsolidatedYr34/TeacherCharacteristics.dta", clear
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
save "$base_out/ConsolidatedYr34/TSupport.dta", replace
gen Male=( tchsex==1) & !missing(tchsex)
gen HigherEducation=(t16==3 | t16==4 | t16==5) if !missing(t16)
recode t10 (2=0)

label var Male Male
replace t05=2015-t05
replace t07=2015-t07
replace t05=2015-t05

label var t05 "Age (Yrs)"
replace t05=2015-t05
label var t07 "Experience (Yrs)"
label var t08 "Yr started teaching at this school"
label var t10 "Private school experience"
label var t21 "Travel time (mins)"
label var HigherEducation "Tertiary education"
/*
collapse (count) R10TeacherID (mean) t05- StrataScore treatarm2 treatarm Male HigherEducation , by(SchoolID DistrictID)
 reghdfe Male [aw=R10TeacherID],ab(DistrictID##StrataScore##treatarm) residual(resid)
  reghdfe t05 [aw=R10TeacherID],ab(DistrictID##StrataScore##treatarm) residual(resid2)
  reghdfe t07 [aw=R10TeacherID],ab(DistrictID##StrataScore##treatarm) residual(resid3)
 */
 *t07
eststo clear
xi: my_ptest Male t05  HigherEducation, by(treatment2) clus_id(SchoolID) /*controls(DistrictID StrataScore treatarm)*/
esttab using "$latexcodes/summaryTeachersBase.tex", label replace nogaps nolines  ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none) fragment  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) d_p(star pvalue(d_p))" "se_1(fmt(%9.2fc) par) se_2( fmt(%9.2fc) par) se_3(fmt(%9.2fc) par)  .") ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")


foreach var in Male t05 t07 t10 HigherEducation{
	sum `var'
	local tempm=string(r(mean), "%9.2gc")
	file open newfile using "$latexcodes/`var'_summary.tex", write replace
	file write newfile "`tempm'"
	file close newfile
}
