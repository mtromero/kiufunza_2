***********************************************************
******************************** 2013 *********************
***********************************************************

use "$basein/5 National Exams/ResultadosEstudiantesTotal_PSLE_2013.dta", clear
capture renvars _all, subst("`=char(69)'" "")
capture renvars _all, subst("`=char(69)'" "")
rename AVRAG AVERAGE
qui foreach sub in AVERAGE kiswahili english maarifa hisabati sayansi{
	replace `sub'="5" if `sub'=="A"
	replace `sub'="4" if `sub'=="B"
	replace `sub'="3" if `sub'=="C"
	replace `sub'="2" if `sub'=="D"
	replace `sub'="1" if `sub'=="E"
	replace `sub'="0" if `sub'=="F"
	replace `sub'="" if `sub'=="NA"
	replace `sub'="" if `sub'=="X"
	replace `sub'="" if `sub'=="*S"
	replace `sub'="" if `sub'=="*W"
}
qui destring AVERAGE kiswahili english maarifa hisabati sayansi, replace
drop if AVERAGE==.
keep CAND SX AVERAGE kiswahili- sayansi school_codePSLE SchoolID
rename SchoolID SchoolID_PSLE2013
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
rename school_codePSLE school_codePSL_Final

preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_PSLE2013 status_PSLE2013
drop if status_PSLE2013=="Matched - manual R2"
drop if status_PSLE2013=="Unmatched"
tempfile temp
save `temp'
restore
merge m:1 SchoolID_PSLE2013 using  `temp',keepus(SchoolID)
drop if _merge!=3
drop _merge
save "$base_out/Student_PSLE_2013.dta", replace
gen Pass=(AVERAGE>=3)
collapse (sum) Pass=Pass (mean) PassRate=Pass AverageOwn=AVERAGE kiswahili- sayansi, by(school_codePSL_Final)
save "$base_out/School_PSLE_2013.dta", replace

use "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2013.dta", clear
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
rename school_codePSLE school_codePSL_Final
keep Students Average RankingDistrict TotalDistrict RankingRegion TotalRegion RankingNation TotalNation school_codePSL_Final SchoolID
rename SchoolID SchoolID_PSLE2013
preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_PSLE2013 status_PSLE2013
drop if status_PSLE2013=="Matched - manual R2"
drop if status_PSLE2013=="Unmatched"
tempfile temp
save `temp'
restore
merge 1:1 SchoolID_PSLE2013 using  `temp',keepus(SchoolID)
drop if _merge!=3
drop _merge
merge 1:1 school_codePSL_Final using "$base_out/School_PSLE_2013.dta"
drop if _merge!=3
drop _merge
save "$base_out/School_PSLE_2013.dta", replace

***********************************************************
******************************** 2014 *********************
***********************************************************




use "$basein/5 National Exams/ResultadosEstudiantesTotal_PSLE_2014.dta", clear
capture renvars _all, subst("`=char(69)'" "")
capture renvars _all, subst("`=char(69)'" "")
rename AVRAG AVERAGE
qui foreach sub in AVERAGE kiswahili english maarifa hisabati sayansi{
	replace `sub'="5" if `sub'=="A"
	replace `sub'="4" if `sub'=="B"
	replace `sub'="3" if `sub'=="C"
	replace `sub'="2" if `sub'=="D"
	replace `sub'="1" if `sub'=="E"
	replace `sub'="0" if `sub'=="F"
	replace `sub'="" if `sub'=="NA"
	replace `sub'="" if `sub'=="X"
	replace `sub'="" if `sub'=="*S"
	replace `sub'="" if `sub'=="*W"
}
qui destring AVERAGE kiswahili english maarifa hisabati sayansi, replace
drop if AVERAGE==.
keep CAND SX AVERAGE kiswahili- sayansi school_codePSLE SchoolID
rename SchoolID SchoolID_PSLE2014
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
rename school_codePSLE school_codePSL_Final
preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_PSLE2014 status_PSLE2014
drop if status_PSLE2014=="Matched - manual R2"
drop if status_PSLE2014=="Unmatched"
tempfile temp
save `temp'
restore
merge m:1 SchoolID_PSLE2014 using  `temp',keepus(SchoolID)
drop if _merge!=3
drop _merge
save "$base_out/Student_PSLE_2014.dta", replace
gen Pass=(AVERAGE>=3)
collapse (sum) Pass=Pass (mean) PassRate=Pass AverageOwn=AVERAGE kiswahili- sayansi, by(school_codePSL_Final)
save "$base_out/School_PSLE_2014.dta", replace

use "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2014.dta", clear
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
rename school_codePSLE school_codePSL_Final
keep Students Average RankingDistrict TotalDistrict RankingRegion TotalRegion RankingNation TotalNation school_codePSL_Final SchoolID
rename SchoolID SchoolID_PSLE2014
preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_PSLE2014 status_PSLE2014
drop if status_PSLE2014=="Matched - manual R2"
drop if status_PSLE2014=="Unmatched"
tempfile temp
save `temp'
restore
merge 1:1 SchoolID_PSLE2014 using  `temp',keepus(SchoolID)
drop if _merge!=3
drop _merge
merge 1:1 school_codePSL_Final using "$base_out/School_PSLE_2014.dta"
drop if _merge!=3
drop _merge
save "$base_out/School_PSLE_2014.dta", replace





***********************************************************
******************************** 2015 *********************
***********************************************************


use "$basein/5 National Exams/ResultadosEstudiantesTotal_PSLE_2015.dta", clear
capture renvars _all, subst("`=char(69)'" "")
capture renvars _all, subst("`=char(69)'" "")
rename AVRAG AVERAGE
qui foreach sub in AVERAGE kiswahili english maarifa hisabati sayansi{
	replace `sub'="5" if `sub'=="A"
	replace `sub'="4" if `sub'=="B"
	replace `sub'="3" if `sub'=="C"
	replace `sub'="2" if `sub'=="D"
	replace `sub'="1" if `sub'=="E"
	replace `sub'="0" if `sub'=="F"
	replace `sub'="" if `sub'=="NA"
	replace `sub'="" if `sub'=="X"
	replace `sub'="" if `sub'=="*S"
	replace `sub'="" if `sub'=="*W"
	replace `sub'="" if `sub'=="*R"
}
qui destring AVERAGE kiswahili english maarifa hisabati sayansi, replace
drop if AVERAGE==.
keep CAND SX AVERAGE kiswahili- sayansi school_codePSLE SchoolID
rename SchoolID SchoolID_PSLE2015
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
rename school_codePSLE school_codePSL_Final

preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_PSLE2015 status_PSLE2015
drop if status_PSLE2015=="Matched - manual R2"
drop if status_PSLE2015=="Unmatched"
tempfile temp
save `temp'
restore
merge m:1 SchoolID_PSLE2015 using  `temp',keepus(SchoolID)

drop if _merge!=3
drop _merge
save "$base_out/Student_PSLE_2015.dta", replace
gen Pass=(AVERAGE>=3)
collapse (sum) Pass=Pass (mean) PassRate=Pass AverageOwn=AVERAGE kiswahili- sayansi, by(school_codePSL_Final)
save "$base_out/School_PSLE_2015.dta", replace

use "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2015.dta", clear
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
rename school_codePSLE school_codePSL_Final
keep Students Average RankingDistrict TotalDistrict RankingRegion TotalRegion RankingNation TotalNation school_codePSL_Final SchoolID
rename SchoolID SchoolID_PSLE2015
/*this has some duplicates, product of the scrapping*/
bys SchoolID_PSLE2015: gen n=_n
drop if n>1
drop n
/*end of duplicate code*/
preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_PSLE2015 status_PSLE2015
drop if status_PSLE2015=="Matched - manual R2"
drop if status_PSLE2015=="Unmatched"
tempfile temp
save `temp'
restore
merge 1:1 SchoolID_PSLE2015 using  `temp',keepus(SchoolID)

drop if _merge!=3
drop _merge
merge 1:1 school_codePSL_Final using "$base_out/School_PSLE_2015.dta"
drop if _merge!=3
drop _merge
save "$base_out/School_PSLE_2015.dta", replace





***********************************************************
******************************** 2016 *********************
***********************************************************


use "$basein/5 National Exams/ResultadosEstudiantesTotal_PSLE_2016.dta", clear
capture renvars _all, subst("`=char(69)'" "")
capture renvars _all, subst("`=char(69)'" "")
rename AVRAG AVERAGE
qui foreach sub in AVERAGE kiswahili english maarifa hisabati sayansi{
	replace `sub'="5" if `sub'=="A"
	replace `sub'="4" if `sub'=="B"
	replace `sub'="3" if `sub'=="C"
	replace `sub'="2" if `sub'=="D"
	replace `sub'="1" if `sub'=="E"
	replace `sub'="0" if `sub'=="F"
	replace `sub'="" if `sub'=="NA"
	replace `sub'="" if `sub'=="X"
	replace `sub'="" if `sub'=="*S"
	replace `sub'="" if `sub'=="*W"
	replace `sub'="" if `sub'=="*R"
}
qui destring AVERAGE kiswahili english maarifa hisabati sayansi, replace
drop if AVERAGE==.
keep CAND SX AVERAGE kiswahili- sayansi school_codePSLE SchoolID
rename SchoolID SchoolID_PSLE2016
rename school_codePSLE school_codePSL_Final
preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_PSLE2016 status_PSLE2016
drop if status_PSLE2016=="Matched - manual R2"
drop if status_PSLE2016=="Unmatched"
tempfile temp
save `temp'
restore
merge m:1 SchoolID_PSLE2016 using  `temp',keepus(SchoolID)

drop if _merge!=3
drop _merge
save "$base_out/Student_PSLE_2016.dta", replace
gen Pass=(AVERAGE>=3)
collapse (sum) Pass=Pass (mean) PassRate=Pass AverageOwn=AVERAGE kiswahili- sayansi, by(school_codePSL_Final)
save "$base_out/School_PSLE_2016.dta", replace

use "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2016.dta", clear
rename school_codePSLE school_codePSL_Final
keep Students Average RankingDistrict TotalDistrict RankingRegion TotalRegion RankingNation TotalNation school_codePSL_Final SchoolID
rename SchoolID SchoolID_PSLE2016
preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_PSLE2016 status_PSLE2016
drop if status_PSLE2016=="Matched - manual R2"
drop if status_PSLE2016=="Unmatched"
tempfile temp
save `temp'
restore
merge 1:1 SchoolID_PSLE2016 using  `temp',keepus(SchoolID)

drop if _merge!=3
drop _merge
merge 1:1 school_codePSL_Final using "$base_out/School_PSLE_2016.dta"
drop if _merge!=3
drop _merge
save "$base_out/School_PSLE_2016.dta", replace




***********************************************************
******************************** 2017 *********************
***********************************************************


use "$basein/5 National Exams/ResultadosEstudiantesTotal_PSLE_2017.dta", clear
capture renvars _all, subst("`=char(69)'" "")
capture renvars _all, subst("`=char(69)'" "")
rename AVRAG AVERAGE
qui foreach sub in AVERAGE kiswahili english maarifa hisabati sayansi{
	replace `sub'="5" if `sub'=="A"
	replace `sub'="4" if `sub'=="B"
	replace `sub'="3" if `sub'=="C"
	replace `sub'="2" if `sub'=="D"
	replace `sub'="1" if `sub'=="E"
	replace `sub'="0" if `sub'=="F"
	replace `sub'="" if `sub'=="NA"
	replace `sub'="" if `sub'=="X"
	replace `sub'="" if `sub'=="*S"
	replace `sub'="" if `sub'=="*W"
	replace `sub'="" if `sub'=="*R"
}
qui destring AVERAGE kiswahili english maarifa hisabati sayansi, replace
drop if AVERAGE==.
keep CAND SX AVERAGE kiswahili- sayansi school_codePSLE SchoolID
rename SchoolID SchoolID_PSLE2017
rename school_codePSLE school_codePSL_Final

preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_PSLE2017 status_PSLE2017
drop if status_PSLE2017=="Matched - manual R2"
drop if status_PSLE2017=="Unmatched"
tempfile temp
save `temp'
restore
merge m:1 SchoolID_PSLE2017 using  `temp',keepus(SchoolID)

drop if _merge!=3
drop _merge
save "$base_out/Student_PSLE_2017.dta", replace
gen Pass=(AVERAGE>=3)
collapse (sum) Pass=Pass (mean) PassRate=Pass AverageOwn=AVERAGE kiswahili- sayansi, by(school_codePSL_Final)
save "$base_out/School_PSLE_2017.dta", replace

use "$basein/5 National Exams/ResultadosSchoolTotal_PSLE_2017.dta", clear
rename school_codePSLE school_codePSL_Final
keep Students Average RankingDistrict TotalDistrict RankingRegion TotalRegion RankingNation TotalNation school_codePSL_Final SchoolID
rename SchoolID SchoolID_PSLE2017

preserve
use "$mipath/Willie Work/school matching/results.dta", clear
keep SchoolID SchoolID_PSLE2017 status_PSLE2017
drop if status_PSLE2017=="Matched - manual R2"
drop if status_PSLE2017=="Unmatched"
tempfile temp
save `temp'
restore
merge 1:1 SchoolID_PSLE2017 using  `temp',keepus(SchoolID)

drop if _merge!=3
drop _merge
merge 1:1 school_codePSL_Final using "$base_out/School_PSLE_2017.dta"
drop if _merge!=3
drop _merge
save "$base_out/School_PSLE_2017.dta", replace



