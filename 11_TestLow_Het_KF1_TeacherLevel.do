***** IDENTIFY GRADE/SUBJECTS/SCHOOLS WITH TEACHER IN KF 2 WHO HAD TEACHERS IN FOCAL GRDES/SUBJECTS IN KF1

use "$basein/3 Endline/Teacher/ETGrdSub_noPII.dta", clear
gen TreatedKF1_Yr1=0
replace TreatedKF1_Yr1=1 if (ETGradeID==1 | ETGradeID==2 | ETGradeID==3) & (ETGrdSubID==1 | ETGrdSubID==2 | ETGrdSubID==3) & tgrdyn==1
collapse (max) TreatedKF1_Yr1, by(ETTeacherID)
tempfile temp1
save `temp1'

use "$basein/8 Endline 2014/Final Data/Teacher/R6TGrdSub2_noPII.dta", clear
gen TreatedKF1_Yr2=0
replace TreatedKF1_Yr2=1 if (R6TGradeID==1 | R6TGradeID==2 | R6TGradeID==3) & (R6TGrdSub2ID==1 | R6TGrdSub2ID==2 | R6TGrdSub2ID==3) & tgrdyn==1
collapse (max) TreatedKF1_Yr2, by(SchoolID R6TeacherID)
tempfile temp2
save `temp2'



use "$base_out/ConsolidatedYr34/Teachers.dta", clear
merge m:1 ETTeacherID using `temp1'
drop _merge
replace TreatedKF1_Yr1=0 if TreatedKF1_Yr1==.
merge m:1 SchoolID R6TeacherID using `temp2'
drop _merge
replace TreatedKF1_Yr2=0 if TreatedKF1_Yr2==.
keep SchoolID R8TeacherID R10TeacherID_T2 upid  - upidold StrataScore- TreatedKF1_Yr2 R10TeacherID
save "$base_out/ConsolidatedYr34/Teachers_ExposedKF1.dta", replace




use "$basein/11 Endline 2015/Final Data/Teacher Data/R8TGrdSub2_nopii.dta", clear
keep if (R8TGradeID==1 | R8TGradeID==2 | R8TGradeID==3) & (R8TGrdSub2ID==1 | R8TGrdSub2ID==2) & tgrdyn==1
keep SchoolID- R8TGrdSub2ID tgrdyn
reshape wide tgrdyn, i(SchoolID R8TeacherID R8TGradeID) j(R8TGrdSub2ID )
reshape wide tgrdyn*, i(SchoolID R8TeacherID ) j(R8TGradeID)
merge 1:m  SchoolID R8TeacherID using "$base_out/ConsolidatedYr34/Teachers_ExposedKF1.dta"
drop if _merge==2
drop _merge
replace TreatedKF1_Yr1=0 if TreatedKF1_Yr1==.
replace TreatedKF1_Yr2=0 if TreatedKF1_Yr2==.

reshape long tgrdyn1 tgrdyn2, i(SchoolID R8TeacherID ) j(GradeID)
drop if tgrdyn1==. & tgrdyn2==.
reshape long tgrdyn@, i(SchoolID R8TeacherID GradeID) j(Subject)
drop if tgrdyn==.
collapse (mean) TreatedKF1_Yr1 TreatedKF1_Yr2, by(SchoolID GradeID)
save "$base_out/ConsolidatedYr34/Teachers_ExposedKF1_Year1.dta", replace



  

use "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10TGrdSub2_nopii.dta", clear
keep if (R10TGradeID==1 | R10TGradeID==2 | R10TGradeID==3) & (R10TGrdSub2ID==1 | R10TGrdSub2ID==2) & tgrdyn==1
keep SchoolID- R10TGrdSub2ID tgrdyn
reshape wide tgrdyn, i(SchoolID R10TeacherID R10TGradeID) j(R10TGrdSub2ID )
reshape wide tgrdyn*, i(SchoolID R10TeacherID ) j(R10TGradeID)
rename R10TeacherID R10TeacherID_T2
merge 1:m  SchoolID R10TeacherID_T2 using "$base_out/ConsolidatedYr34/Teachers_ExposedKF1.dta"
drop if _merge==2
drop _merge
replace TreatedKF1_Yr1=0 if TreatedKF1_Yr1==.
replace TreatedKF1_Yr2=0 if TreatedKF1_Yr2==.

reshape long tgrdyn1 tgrdyn2, i(SchoolID R10TeacherID_T2 ) j(GradeID)
drop if tgrdyn1==. & tgrdyn2==.
reshape long tgrdyn@, i(SchoolID R10TeacherID_T2 GradeID) j(Subject)
drop if tgrdyn==.
collapse (mean) TreatedKF1_Yr1 TreatedKF1_Yr2, by(SchoolID GradeID)
save "$base_out/ConsolidatedYr34/Teachers_ExposedKF1_Year2.dta", replace









**********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
fvset base default treatarm 
gen Diff_T2=date_twa_T2-date_edi_T2
gen Diff_T5=date_twa_T5-date_edi_T5
gen Week_T2=week(date_edi_T2)
gen Week_T5=week(date_edi_T5)


global AggregateDep 	Z_hisabati Z_kiswahili Z_kiingereza /*this should be added in the future... Z_sayansi Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal*/ 
global AggregateDep_Karthik 	Z_hisabati Z_kiswahili Z_kiingereza   /*Z_kiingereza  Z_ScoreFocal this should be added in the future...*/
global AggregateDep_int 	Z_hisabati Z_kiswahili Z_kiingereza
 
global AggregateDep_NonEng 	Z_hisabati Z_kiswahili 
global AggregateDep_Eng 	Z_kiingereza

global treatmentlist TreatmentLevels TreatmentGains
gen TreatmentLevels2=TreatmentLevels
gen TreatmentGains2=TreatmentGains
global treatmentlist2 TreatmentLevels2 TreatmentGains2
*enrollment2015_T1 Rural_T1 ClassesOutside_T1 Electricity_T1

label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"      
*c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza)##c.(c.LagZ_kiswahili#c.MissingZ_kiswahili c.LagZ_hisabati#c.MissingZ_hisabati c.LagZ_kiingereza#c.MissingZ_kiingereza) ///


global student2 shoes_T2 socks_T2 dirty_T2 uniformdirty_T2 uniformtorn_T2  ringworm_T2 CloseToeShoe_T2

recode bookshome_T2 (-98=.)
global studentoutcomes studentsfight_T2 sing_T2 bookshome_T2

bys SchoolID: egen Z_kiswahili_T0_G=mean(Z_kiswahili_T0)
bys SchoolID: egen Z_kiingereza_T0_G=mean(Z_kiingereza_T0)
bys SchoolID: egen Z_hisabati_T0_G=mean(Z_hisabati_T0)
pca Z_hisabati_T0  Z_kiingereza_T0 Z_kiswahili_T0
predict Z_ScoreFocal_T0, score
*First lets create the tables Karthik Wants

label var Z_kiswahili_T2 Swahili
label var Z_kiswahili_T5 Swahili
label var Z_kiingereza_T2 English
label var Z_kiingereza_T5 English
label var Z_hisabati_T2 Math
label var Z_hisabati_T5 Math
label var Z_sayansi_T2 Science
label var Z_sayansi_T5 Science
label var Z_ScoreFocal_T2 "Focal Subjects"
label var Z_ScoreFocal_T5 "Focal Subjects"
recode missschool_T5 (2=0) 

gen GradeID=GradeID_T2
merge m:1 SchoolID GradeID using "$base_out/ConsolidatedYr34/Teachers_ExposedKF1_Year1.dta"
drop if _merge==2
drop _merge
drop GradeID
gen GradeID=Grade_T3
merge m:1 SchoolID GradeID using "$base_out/ConsolidatedYr34/Teachers_ExposedKF1_Year1.dta", update
drop if _merge==2
drop _merge
gen COD_KF1_Year1=(TreatedKF1_Yr1>0 | TreatedKF1_Yr2>0)*(treatarm==1 | treatarm==3)
drop GradeID



gen GradeID=GradeID_T5
merge m:1 SchoolID GradeID using "$base_out/ConsolidatedYr34/Teachers_ExposedKF1_Year2.dta"
drop if _merge==2
drop _merge
drop GradeID
gen GradeID=Grade_T6
merge m:1 SchoolID GradeID using "$base_out/ConsolidatedYr34/Teachers_ExposedKF1_Year2.dta", update
drop if _merge==2
drop _merge
gen COD_KF1_Year2=(TreatedKF1_Yr1>0 | TreatedKF1_Yr2>0)*(treatarm==1 | treatarm==3)
drop GradeID



label var TreatmentLevels "Levels"
label var TreatmentGains "P4Pctile"

gen TLev0=TreatmentLevels*(1-COD_KF1_Year1)
gen TP4P0=TreatmentGains*(1-COD_KF1_Year1)
gen TLev1=TreatmentLevels*(COD_KF1_Year1)
gen TP4P1=TreatmentGains*(COD_KF1_Year1)
gen COD_KF1=COD_KF1_Year1

eststo clear
eststo: reghdfe Z_ScoreKisawMath_T2  TLev*  TP4P* COD_KF1  i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T2<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
lincom TLev0-TLev1
estadd scalar p1=r(p)
lincom TP4P0-TP4P1
estadd scalar p2=r(p)
lincom TLev0-TP4P0
estadd scalar p3=r(p)
lincom TLev1-TP4P1
estadd scalar p4=r(p)
lincom (TLev1-TP4P1)-(TLev0-TP4P0)
estadd scalar p5=r(p)

drop TLev*  TP4P* COD_KF1
gen TLev0=TreatmentLevels*(1-COD_KF1_Year2)
gen TP4P0=TreatmentGains*(1-COD_KF1_Year2)
gen TLev1=TreatmentLevels*(COD_KF1_Year2)
gen TP4P1=TreatmentGains*(COD_KF1_Year2)
gen COD_KF1=COD_KF1_Year2
		
eststo: reghdfe Z_ScoreKisawMath_T5  TLev*  TP4P* COD_KF1 i.LagGrade $studentcontrol $schoolcontrol $HHcontrol if GradeID_T5<=3, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
lincom TLev0-TLev1
estadd scalar p1=r(p)
lincom TP4P0-TP4P1
estadd scalar p2=r(p)
lincom TLev0-TP4P0
estadd scalar p3=r(p)
lincom TLev1-TP4P1
estadd scalar p4=r(p)
lincom (TLev1-TP4P1)-(TLev0-TP4P0)
estadd scalar p5=r(p)

	
label var TLev0 "Levels \$\times\$ Teachers not incentivized in previous RCT (\$\alpha_1$)"
label var TLev1 "Levels \$\times\$ Teachers incentivized in previous RCT (\$\alpha_2$)"
label var TP4P0 "P4Pctile \$\times\$ Teachers not incentivized in previous RCT (\$\beta_1$)"
label var TP4P1 "P4Pctile \$\times\$ Teachers incentivized in previous RCT (\$\beta_2$)"
label var COD_KF1 "Teachers incentivized in previous RCT"


esttab  using "$latexcodes/Het_Previous_LS_Teacher.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TLev*  TP4P* COD_KF1) stats(N p1 p2 p3 p4 p5, fmt(%9.0gc %9.2gc  %9.2gc  %9.2gc  %9.2gc  %9.2gc) ///
labels("N. of obs." "p-value(\$H_0:\alpha_1=\alpha_2\$)" "p-value(\$H_0:\beta_1=\beta_2\$)" "p-value(\$H_0:\alpha_1=\beta_1\$)" "p-value(\$H_0:\alpha_2=\beta_2\$)" "p-value(\$H_0:(\alpha_1-\beta_1)=(\alpha_2-\beta_2)\$)" )) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)


drop TLev*  TP4P* COD_KF1
gen TLev0=TreatmentLevels*(1-COD_KF1_Year1)
gen TP4P0=TreatmentGains*(1-COD_KF1_Year1)
gen TLev1=TreatmentLevels*(COD_KF1_Year1)
gen TP4P1=TreatmentGains*(COD_KF1_Year1)
gen COD_KF1=COD_KF1_Year1


eststo clear
eststo: reghdfe Z_ScoreKisawMath_T3  TLev*  TP4P* COD_KF1 i.Grade_T3  $schoolcontrol  if Grade_T3<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
estadd ysumm
lincom TLev0-TLev1
estadd scalar p1=r(p)
lincom TP4P0-TP4P1
estadd scalar p2=r(p)
lincom TLev0-TP4P0
estadd scalar p3=r(p)
lincom TLev1-TP4P1
estadd scalar p4=r(p)
lincom (TLev1-TP4P1)-(TLev0-TP4P0)
estadd scalar p5=r(p)


drop TLev*  TP4P* COD_KF1
gen TLev0=TreatmentLevels*(1-COD_KF1_Year2)
gen TP4P0=TreatmentGains*(1-COD_KF1_Year2)
gen TLev1=TreatmentLevels*(COD_KF1_Year2)
gen TP4P1=TreatmentGains*(COD_KF1_Year2)
gen COD_KF1=COD_KF1_Year2

eststo: reghdfe Z_ScoreKisawMath_T6  TLev*  TP4P* COD_KF1 i.Grade_T6  $schoolcontrol  if Grade_T6<=3, vce(cluster SchoolID) a(DistrictID##StrataScore##treatarm)
estadd ysumm
lincom TLev0-TLev1
estadd scalar p1=r(p)
lincom TP4P0-TP4P1
estadd scalar p2=r(p)
lincom TLev0-TP4P0
estadd scalar p3=r(p)
lincom TLev1-TP4P1
estadd scalar p4=r(p)
lincom (TLev1-TP4P1)-(TLev0-TP4P0)
estadd scalar p5=r(p)


label var TLev0 "Levels \$\times\$ Teachers not incentivized in previous RCT (\$\alpha_1$)"
label var TLev1 "Levels \$\times\$ Teachers incentivized in previous RCT (\$\alpha_2$)"
label var TP4P0 "P4Pctile \$\times\$ Teachers not incentivized in previous RCT (\$\beta_1$)"
label var TP4P1 "P4Pctile \$\times\$ Teachers incentivized in previous RCT (\$\beta_2$)"
label var COD_KF1 "Teachers incentivized in previous RCT"

esttab  using "$latexcodes/Het_Previous_HS_Teacher.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TLev*  TP4P* COD_KF1) stats(N p1 p2 p3 p4 p5, fmt(%9.0gc %9.2gc  %9.2gc  %9.2gc  %9.2gc  %9.2gc) ///
labels("N. of obs." "p-value(\$H_0:\alpha_1=\alpha_2\$)" "p-value(\$H_0:\beta_1=\beta_2\$)" "p-value(\$H_0:\alpha_1=\beta_1\$)" "p-value(\$H_0:\alpha_2=\beta_2\$)" "p-value(\$H_0:(\alpha_1-\beta_1)=(\alpha_2-\beta_2)\$)" )) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)


