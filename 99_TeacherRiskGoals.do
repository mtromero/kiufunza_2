*****************************************************************
************* RISK AVERSION/SIGN POSTING/ OTHERS ****************
*****************************************************************
use "$basein/15 Endline 2016/Final Deliverable/Final Data/Teacher Data/R10Teacher_nopii.dta", clear
keep SchoolID R10TeacherID ETeach_pp upid risk1- risk10  t66 set_goals set_goals_what- toppex knwtwa2- codsyn2016 ttestc ttest

gen stop_rule=0 if risk10==2
replace stop_rule=1 if risk9==2 & risk10==1
replace stop_rule=2 if risk8==2 & risk9==1
replace stop_rule=3 if risk7==2 & risk8==1
replace stop_rule=4 if risk6==2 & risk7==1
replace stop_rule=5 if risk5==2 & risk6==1
replace stop_rule=6 if risk4==2 & risk5==1
replace stop_rule=7 if risk3==2 & risk4==1
replace stop_rule=8 if risk2==2 & risk3==1
replace stop_rule=9 if risk1==2 & risk2==1

replace stop_rule=.  if risk1==1
replace stop_rule=.  if risk10==2
replace stop_rule=.  if risk9==2 & risk8==1
replace stop_rule=.  if risk8==2 & risk7==1
replace stop_rule=.  if risk7==2 & risk6==1
replace stop_rule=.  if risk6==2 & risk5==1
replace stop_rule=.  if risk5==2 & risk4==1
replace stop_rule=.  if risk4==2 & risk3==1
replace stop_rule=.  if risk3==2 & risk2==1
replace stop_rule=.  if risk2==2 & risk1==1

gen risk_averse=(stop_rule<5) if !missing(stop_rule)
gen risk_neutral=(stop_rule==5) if !missing(stop_rule)
gen risk_loving=(stop_rule>5) if !missing(stop_rule)

merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/RandomizeStatus.dta"
drop if _merge!=3
drop _merge
gen TreatmentLevels=0 
replace TreatmentLevels=1 if treatment2=="Levels" 
label var TreatmentLevels "Levels"
gen TreatmentGains=0 
replace TreatmentGains=1 if treatment2=="Gains"
label var TreatmentGains "P4Pctile"

gen GoalGeneral=(twa_goals==1) if !missing(twa_goals)

gen numericGoal=.
replace numericGoal=1 if twa_goals_what==1
replace numericGoal=0 if twa_goals_what==2 | twa_goals_what==-96 | twa_goals==2
gen higherPassRate=(toppex==1) if !missing(toppex)
destring top_earnl mid_earnl low_earnl bonus_mong, replace
destring top_earng mid_earng low_earng top_earng, replace

replace bonus_monl=. if bonus_monl==-99
replace bonus_mong=. if bonus_mong==-99

recode low_earn_worryl (2=1) (3/5=0) 
recode low_earn_worryg (2=1) (3/5=0) 


recode top_earnl (2=1) (3/5=0) 
recode top_earng (2=1) (3/5=0) 

recode mid_earnl (2=1) (3/5=0) 
recode mid_earng (2=1) (3/5=0) 

recode low_earnl (2=1) (3/5=0) 
recode low_earng (2=1) (3/5=0) 



egen bonus_mon=rowtotal(bonus_mong bonus_monl), missing
egen mid_earn=rowtotal(mid_earng mid_earnl), missing
egen low_earn=rowtotal(low_earng low_earnl), missing
egen top_earn=rowtotal(top_earng top_earnl), missing
egen low_earn_worry=rowtotal(low_earn_worryg low_earn_worryl), missing

replace low_earn=0 if top_earn==1
replace mid_earn=0 if top_earn==1
replace low_earn=0 if mid_earn==1


foreach var of varlist set_goals_what1-set_goals_what11 {
	qui replace `var'=1 if `var'!=.
	qui replace `var'=0 if `var'==. & set_goals==2
	qui replace `var'=0 if `var'==. & set_goals==1
}


label var risk_averse "Risk averse"
pca set_goals_what1-set_goals_what11
predict Goals_Score,score
label var Goals_Score "Goal (PCA)"

label var set_goals_what1 "Goals (National exam)"
label var set_goals_what2 "Goals (School exam)"
label var set_goals_what3 "Goals (Twaweza exam)"
label var set_goals_what6 "Goals (Love learning)"
label var set_goals_what9 "Goals (Improve teaching)"
label var set_goals_what10 "Goals (Improve own knowledge)"

label var GoalGeneral "General goal for Twaweza exam"
label var numericGoal "Clear numeric goal"
label var higherPassRate "Expects higher pass rate"
label var bonus_mon "Expected bonus"

label var TreatmentLevels "Levels (\$\alpha_1\$)"
label var TreatmentGains "P4Pctile (\$\alpha_2\$)"
eststo clear
/*
eststo: reghdfe risk_averse c.(${treatmentlist})  , vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
estadd ysumm
sum Goals_Score if treatment2=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
*/

eststo clear
foreach var of varlist set_goals_what2 set_goals_what3 set_goals_what9 set_goals_what10 {
	eststo: reghdfe `var' c.(${treatmentlist})  , vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
	estadd ysumm
	sum `var' if treatment2=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	
	
	local tempm=string(abs(_b[TreatmentGains] - _b[TreatmentLevels]), "%9.2gc")
	file open newfile using "$latexcodes/`var'_T5_coef_diff.tex", write replace
	file write newfile "`tempm'"
	file close newfile
	
	if r(p)<0.01 {
	di "peque"
	local tempm ="$<0.01$"
	file open newfile using "$latexcodes/`var'_T5_pvalue_diff.tex", write replace
	file write newfile "`tempm'"
	file close newfile
	}
	if r(p)>0.01 {
	di "grande"
	local tempm=string(r(p), "%9.2gc")
	file open newfile using "$latexcodes/`var'_T5_pvalue_diff.tex", write replace
	file write newfile "`tempm'"
	file close newfile
	}
	
	
	
	matrix tempm=e(b)
	local tempm=string(tempm[1,1], "%9.2gc")
	local tempm_effect=tempm[1,1]
	
	file open newfile using "$latexcodes/`var'_T5_coef_levels.tex", write replace
	file write newfile "`tempm'"
	file close newfile
	
	local tempm=string(tempm[1,2], "%9.2gc")
	local tempm_effect=tempm[1,2]
	
	file open newfile using "$latexcodes/`var'_T5_coef_gains.tex", write replace
	file write newfile "`tempm'"
	file close newfile
	
	
	test TreatmentLevels
	if r(p)<0.01 {
		di "peque"
		local tempm ="$<0.01$"
		file open newfile using "$latexcodes/`var'_T5_pvalue_levels.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
	if r(p)>0.01 {
		di "grande"
		local tempm=string(r(p), "%9.2gc")
		file open newfile using "$latexcodes/`var'_T5_pvalue_levels.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
	test TreatmentGains
	if r(p)<0.01 {
		di "peque"
		local tempm ="$<0.01$"
		file open newfile using "$latexcodes/`var'_T5_pvalue_gains.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
	if r(p)>0.01 {
		di "grande"
		local tempm=string(r(p), "%9.2gc")
		file open newfile using "$latexcodes/`var'_T5_pvalue_gains.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
}


foreach var of varlist GoalGeneral numericGoal {
	eststo: reghdfe `var' c.(${treatmentlist})  , vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
	estadd ysumm
	sum `var' if treatment2=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentGains] - _b[TreatmentLevels]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentGains] - _b[TreatmentLevels]
	
	
	local tempm=string(abs(_b[TreatmentGains] - _b[TreatmentLevels]), "%9.2gc")
	file open newfile using "$latexcodes/`var'_T5_coef_diff.tex", write replace
	file write newfile "`tempm'"
	file close newfile
	
	if r(p)<0.01 {
	di "peque"
	local tempm ="$<0.01$"
	file open newfile using "$latexcodes/`var'_T5_pvalue_diff.tex", write replace
	file write newfile "`tempm'"
	file close newfile
	}
	if r(p)>0.01 {
	di "grande"
	local tempm=string(r(p), "%9.2gc")
	file open newfile using "$latexcodes/`var'_T5_pvalue_diff.tex", write replace
	file write newfile "`tempm'"
	file close newfile
	}
	
	
	
	matrix tempm=e(b)
	local tempm=string(tempm[1,1], "%9.2gc")
	local tempm_effect=tempm[1,1]
	
	file open newfile using "$latexcodes/`var'_T5_coef_levels.tex", write replace
	file write newfile "`tempm'"
	file close newfile
	
	local tempm=string(tempm[1,2], "%9.2gc")
	local tempm_effect=tempm[1,2]
	
	file open newfile using "$latexcodes/`var'_T5_coef_gains.tex", write replace
	file write newfile "`tempm'"
	file close newfile
	
	
	test TreatmentLevels
	if r(p)<0.01 {
		di "peque"
		local tempm ="$<0.01$"
		file open newfile using "$latexcodes/`var'_T5_pvalue_levels.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
	if r(p)>0.01 {
		di "grande"
		local tempm=string(r(p), "%9.2gc")
		file open newfile using "$latexcodes/`var'_T5_pvalue_levels.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
	test TreatmentGains
	if r(p)<0.01 {
		di "peque"
		local tempm ="$<0.01$"
		file open newfile using "$latexcodes/`var'_T5_pvalue_gains.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
	if r(p)>0.01 {
		di "grande"
		local tempm=string(r(p), "%9.2gc")
		file open newfile using "$latexcodes/`var'_T5_pvalue_gains.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
}
 
 

esttab  using "$latexcodes/TeachersMotivation_T5.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains TreatmentLevels) stats(N ymean2 suma p, fmt(%9.0gc %9.2gc %9.2gc) labels("N. of obs." "Mean control" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value(\$\alpha_3=0\$)" "") star(suma)) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

eststo clear
foreach var of varlist bonus_mon low_earn mid_earn top_earn low_earn_worry{
	eststo: reghdfe `var' TreatmentGains  , vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
	estadd ysumm
	sum `var' if treatment2=="Levels"
	estadd scalar ymean2=r(mean)
}

esttab  using "$latexcodes/TeachersExpectations_T5.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains) stats(N ymean2, fmt(%9.0gc %9.2gc) labels("N. of obs." "Mean Levels")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

eststo clear
foreach var of varlist bonus_mon low_earn mid_earn top_earn low_earn_worry{
	eststo: reghdfe `var' TreatmentGains  if ttest==1 | ttestc==1, vce(cluster SchoolID) ab(DistrictID##StrataScore##treatarm)
	estadd ysumm
	sum `var' if treatment2=="Levels"
	estadd scalar ymean2=r(mean)
}

esttab  using "$latexcodes/TeachersExpectations_T5_check.tex", se ar2 booktabs label b(%9.2gc) se(%9.2gc) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) fragment ///
replace mlabels(none)  collabels(none) nogaps nolines ///
keep(TreatmentGains) stats(N ymean2, fmt(%9.0gc %9.2gc) labels("N. of obs." "Mean Levels")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") substitute(\_ _)

sum bonus_mon,d
drop if bonus_mon>r(p99)

		twoway (kdensity bonus_mon if treatment2=="Gains", lcolor(blue) lwidth(medthick)) ///
		(kdensity bonus_mon if treatment2=="Levels", lcolor(red) lwidth(medthick)), ///
		ytitle(Density) ylabel(, angle(horizontal) nogrid) xtitle(TZS) ///
		legend(order(1 "Gains" 2 "Levels" 3) rows(1) region(fcolor(none) margin(zero) lcolor(none)) bmargin(zero)) ///
		graphregion(fcolor(white)) plotregion(fcolor(white))
	graph export "$graphs/histogram_ExpectedEarnings.pdf", replace
