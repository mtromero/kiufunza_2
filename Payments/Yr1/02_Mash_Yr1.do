capture log close
log using "CreatedData/4 Intervention/TwaEL_2015/MashYr1.smcl", replace
**********************************
**********	MASHINDANO **************
**********************************


**********************************
**********	GRADE 1 **************
**********************************


use "CreatedData/4 Intervention/TwaEL_2015/EL2015_Student_FinalRun_Mashindano_CLEAN.dta", clear
drop  district  gender set start startampm volname end endampm

replace stream = lower(stream)
rename schoolid SchoolID
rename studentid studentID
replace studentID=. if studentID==-999
replace studentID=. if studentID==-99
replace studentID=. if studentID==999
replace studentID=. if studentID==99
merge m:1 SchoolID using "CreatedData/4 Intervention/TwaEL_2015/2015_GroupsStudent_Mash_Grade1.dta"
drop _merge

ds, has(type string)
foreach var of varlist `r(varlist)'{
	replace `var'="" if `var'=="--blank--" | `var'=="X"
}
destring , replace

drop if grade!=1

foreach var of varlist SchoolID grade stream{
	qui tabmiss `var'
	display "`r(sum)' missing in variable `var'"
	display "They are dropped"
	drop if	missing(`var')
}

merge 1:1 name using "CreatedData/4 Intervention/TwaEL_2015/CorrectStream.dta", update replace
drop if _merge==2
drop _merge

/*
bys  SchoolID grade stream: gen Rand=uniform()
replace stream="a" if  SchoolID==120 & grade==1 & stream=="c" & Rand>=0.5
replace stream="b" if  SchoolID==120 & grade==1 & stream=="c" & Rand<0.5
replace stream="a" if  SchoolID==120 & grade==1 & stream=="d" & Rand>=0.5
replace stream="b" if  SchoolID==120 & grade==1 & stream=="d" & Rand<0.5
drop Rand


replace stream="a" if  SchoolID==318 & grade==1 & stream=="b"
replace stream="a" if  SchoolID==318 & grade==1 & stream=="c"
replace stream="a" if  SchoolID==322 & grade==1 & stream=="c"
replace stream="a" if  SchoolID==322 & grade==1 & stream=="f"
replace stream="a" if  SchoolID==709 & grade==1 & stream=="d"
*/

foreach skill in si ma se{
	qui tabmiss grd1_kis_a_`skill'_1
	display "Skills `skill' in Swahili/Qs1 has `r(sum)' missing"
	qui tabmiss grd1_kis_a_`skill'_1
	display "Skills `skill' in Swahili/Qs2 has `r(sum)' missing"
}

foreach skill in l w se{
	qui tabmiss grd1_eng_a_`skill'_1
	display "Skills `skill' in English/Qs1 has `r(sum)' missing"
	qui tabmiss grd1_eng_a_`skill'_1
	display "Skills `skill' in English/Qs2 has `r(sum)' missing"
}

foreach skill in id uta bwa j t{
	qui tabmiss grd1_his_a_`skill'_1
	display "Skills `skill' in Math/Qs1 has `r(sum)' missing"
	qui tabmiss grd1_his_a_`skill'_1
	display "Skills `skill' in Math/Qs2 has `r(sum)' missing"
}


*Some missings are "weird" but I'm gonna assume students just didin't answer the question, i.e. it was wrong... but I do some more cleaning below
foreach var of varlist grd*{
	replace `var'=0 if `var'==.
}

*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in si ma se{
	 forvalues i=3/6{
		replace grd1_kis_a_`skill'_`i'=0 if grd1_kis_a_`skill'_1==0 & grd1_kis_a_`skill'_2==0
	}
}

foreach skill in l w se{
	forvalues i=3/6{
		replace grd1_eng_a_`skill'_`i'=0 if grd1_eng_a_`skill'_1==0 & grd1_eng_a_`skill'_2==0
	}
}

foreach skill in id uta bwa j t{
		replace grd1_his_a_`skill'_3=0 if grd1_his_a_`skill'_1==0 & grd1_his_a_`skill'_2==0
} 

/*Total answers per skill*/
egen Kis_Silabi=rowtotal(grd1_kis_a_si_1 grd1_kis_a_si_2 grd1_kis_a_si_3 grd1_kis_a_si_4 grd1_kis_a_si_5 grd1_kis_a_si_6)
egen Kis_Maneno=rowtotal(grd1_kis_a_ma_1 grd1_kis_a_ma_2 grd1_kis_a_ma_3 grd1_kis_a_ma_4 grd1_kis_a_ma_5 grd1_kis_a_ma_6)
egen Kis_Sentenci=rowtotal(grd1_kis_a_se_1 grd1_kis_a_se_2 grd1_kis_a_se_3 grd1_kis_a_se_4 grd1_kis_a_se_5 grd1_kis_a_se_6)


egen Eng_Letter=rowtotal(grd1_eng_a_l_1 grd1_eng_a_l_2 grd1_eng_a_l_3 grd1_eng_a_l_4 grd1_eng_a_l_5 grd1_eng_a_l_6)
egen Eng_Word=rowtotal(grd1_eng_a_w_1 grd1_eng_a_w_2 grd1_eng_a_w_3 grd1_eng_a_w_4 grd1_eng_a_w_5 grd1_eng_a_w_6)
egen Eng_Sentences=rowtotal(grd1_eng_a_se_1 grd1_eng_a_se_2 grd1_eng_a_se_3 grd1_eng_a_se_4 grd1_eng_a_se_5 grd1_eng_a_se_6)


egen Math_id=rowtotal(grd1_his_a_id_1 grd1_his_a_id_2 grd1_his_a_id_3)
egen Math_uta=rowtotal(grd1_his_a_uta_1 grd1_his_a_uta_2 grd1_his_a_uta_3)
egen Math_bwa=rowtotal(grd1_his_a_bwa_1 grd1_his_a_bwa_2 grd1_his_a_bwa_3)
egen Math_j=rowtotal(grd1_his_a_j_1 grd1_his_a_j_2 grd1_his_a_j_3)
egen Math_t=rowtotal(grd1_his_a_t_1 grd1_his_a_t_2 grd1_his_a_t_3)

*generate passing dummies. Have to be careful since stata thinks missing is infinity
foreach var of varlist Kis_Silabi Kis_Maneno Kis_Sentenci Eng_Letter Eng_Word Eng_Sentences {
	gen `var'_Pass=`var'>=4 & !missing(`var')
}

foreach var of varlist Math_id Math_uta Math_bwa Math_j Math_t {
	gen `var'_Pass=`var'>=2 & !missing(`var')
}


*Only goes to b section if NO more than 1 answer right in each section */
*Only go to c if everything is perfect! */
*Therefire if you didint go to b, I'm assuming you had all of these correct */
*If you didint go to C I'm assuming you would have gotten all those wrong!
foreach var of varlist grd1_kis_b*{
	replace `var'=1 if Kis_Silabi>1 | Kis_Maneno>1 | Kis_Sentenci>1
}

foreach var of varlist grd1_kis_c*{
	replace `var'=0 if Kis_Silabi<6 | Kis_Maneno<6 | Kis_Sentenci<6
}

foreach var of varlist grd1_eng_b*{
	replace `var'=1 if Eng_Letter>1 | Eng_Word>1 | Eng_Sentences>1
}

foreach var of varlist grd1_eng_c*{
	replace `var'=0 if Eng_Letter<6 | Eng_Word<6 | Eng_Sentences<6
}

foreach var of varlist grd1_his_c*{
	replace `var'=0 if Math_id<3 | Math_uta<3 | Math_bwa<3 | Math_j<3 | Math_t<3
} 

*I create dummies for the different levels of reading. So dummy_2 is if you got a 2 or higher. Same for other numbers
foreach var of varlist grd1_*{
	qui sum `var'
	if(`r(max)'==2){
		gen `var'_1=(`var'>=1) & !missing(`var')
		gen `var'_2=(`var'>=2) & !missing(`var')
		drop `var'
	}
	if(`r(max)'==3){
		gen `var'_1=(`var'>=1) & !missing(`var')
		gen `var'_2=(`var'>=2) & !missing(`var')
		gen `var'_3=(`var'>=3) & !missing(`var')
		drop `var'
	}
}

/*Drop questions with all 0 or all 1*/
/*They provide NO useful variation */
foreach var of varlist grd1_*{
	qui sum `var'
	if(`r(sd)'==0) {
		drop `var'
	}
	if(`r(sd)'!=0) {
		egen `var'_Z=std(`var')  
	}
}
*Do PCA with each subject... this is what I used to rank students (the first principal component)
pca grd1_kis_*_Z
predict Z_kis, score 
pca grd1_eng_*_Z
predict Z_eng, score 
pca grd1_his_*_Z
predict Z_his, score 



*Rank students according to the PCA */
egen Rank_Kis = xtile(Z_kis), by(LevelGrade0) nquantiles(100)
egen Rank_Eng = xtile(Z_eng), by(LevelGrade0) nquantiles(100)
egen Rank_Math = xtile(Z_his), by(LevelGrade0) nquantiles(100)

*Calculcate how many students there are in each rank level */
bys LevelGrade0: egen StudentsInLevel_Kis=count(Rank_Kis) 
bys LevelGrade0: egen StudentsInLevel_Eng=count(Rank_Eng) 
bys LevelGrade0: egen StudentsInLevel_Math =count(Rank_Math ) 

*Calculcate how many "points" or "rank-points" there are in each level */
bys LevelGrade0: egen TotalPoints_Kis=total(Rank_Kis-1) 
bys LevelGrade0: egen TotalPoints_Eng=total(Rank_Eng-1) 
bys LevelGrade0: egen TotalPoints_Math =total(Rank_Math-1) 

/*The budget per group is equal to the number of students divided by the total =scalar(BudgetGrd1)*(StudentsInLevel_Kis/_N)*/
/*Then we calculcate how much money we pay per point which is the the number above divided by total number of points */
/*We the calculcate how much each student gets, which is the rank of the student divided multiplied by the number above */
gen Payment_Total_Kis=(Rank_Kis-1)*(scalar(BudgetKis_Grd1_Mash)*(StudentsInLevel_Kis/_N))/TotalPoints_Kis
gen Payment_Total_Eng=(Rank_Eng-1)*(scalar(BudgetEng_Grd1_Mash)*(StudentsInLevel_Eng/_N))/TotalPoints_Eng
gen Payment_Total_Math=(Rank_Math-1)*(scalar(BudgetMath_Grd1_Mash)*(StudentsInLevel_Math/_N))/TotalPoints_Math



preserve
keep name- studentID stuname_2014 Rank_Kis Rank_Eng Rank_Math LevelGrade0 tested2015 *_Pass
saveold "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd1Rankings_Students.dta",replace version(12)
restore

preserve
*Calculcate final payments by school,grade,stream
collapse (count) Sudents=LevelGrade0 (sum) Payment_Total_* Rank_*, by(SchoolID grade stream)
saveold "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd1_Payments_v2.dta",replace version(12)
restore


*Calculcate final payments by school,grade,stream
collapse (count) Sudents=grade (sum) Payment_Total_* (max) MaxKis=Rank_Kis  MaxEng=Rank_Eng MaxMath=Rank_Math (min) MinKis=Rank_Kis  MinEng=Rank_Eng MinMath=Rank_Math (median) MedianKis=Rank_Kis  MedianEng=Rank_Eng MedianMath=Rank_Math, by(SchoolID districtid LevelGrade0)
foreach subject in Kis Math Eng{
gen Payment_Total_`subject'_PS=Payment_Total_`subject'/Sudents
bys LevelGrade0: egen MaxPayment`subject'=max(Payment_Total_`subject'_PS)
replace MaxPayment`subject'=MaxPayment`subject'*Sudents
gen PropPayment`subject'=Payment_Total_`subject'/MaxPayment`subject'
bys districtid: egen MaxPropDistrict`subject'=max(PropPayment`subject')
}
saveold "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd1_TeacherReport.dta",replace version(12)


foreach subject in Kis Eng Math{
preserve
collapse (sum) Sudents Payment_Total_`subject', by(SchoolID districtid)
gen Payment_Total_`subject'_PS=Payment_Total_`subject'/Sudents
egen MaxPayment`subject'=max(Payment_Total_`subject'_PS)
replace MaxPayment`subject'=MaxPayment`subject'*Sudents
gen PropPayment`subject'=Payment_Total_`subject'/MaxPayment`subject'
bys districtid: egen MaxPropDistrict`subject'=max(PropPayment`subject')

levelsof SchoolID
foreach school in `r(levels)' {
estpost tabstat Sudents MaxPayment`subject' Payment_Total_`subject' PropPayment`subject' MaxPropDistrict`subject' if SchoolID==`school', statistics(mean) columns(variables)
esttab using "CreatedData/4 Intervention/TwaEL_2015/pdf_School/latexTables/mash_`school'_`subject'_grd1.csv", cells("Sudents MaxPayment`subject' Payment_Total_`subject' PropPayment`subject' MaxPropDistrict`subject'") replace fragment noeqlines nolines nogaps  nomti nonum nodep nonotes noobs tab
}
restore
}




**********************************
**********	GRADE 2 **************
**********************************


use "CreatedData/4 Intervention/TwaEL_2015/EL2015_Student_FinalRun_Mashindano_CLEAN.dta", clear
drop  district   gender set start startampm volname end endampm
replace stream = lower(stream)
rename schoolid SchoolID
rename studentid studentID
replace studentID=. if studentID==-999
replace studentID=. if studentID==-99
replace studentID=. if studentID==999
replace studentID=. if studentID==99

merge m:1 studentID using "CreatedData/4 Intervention/TwaEL_2015/2015_GroupsStudent_Mash_Grade23.dta", keepus( KisLevel1 EngLevel1 MathLevel1)
drop if _merge==2
drop _merge

ds, has(type string)
foreach var of varlist `r(varlist)'{
replace `var'="" if `var'=="--blank--" | `var'=="X"
}

destring , replace

drop if grade!=2

foreach var of varlist SchoolID grade stream{
	qui tabmiss `var'
	display "`r(sum)' missing in variable `var'"
	display "They are dropped"
	drop if	missing(`var')
}
merge 1:1 name using "CreatedData/4 Intervention/TwaEL_2015/CorrectStream.dta", update replace
drop if _merge==2
drop _merge

/*
bys  SchoolID grade stream: gen Rand=uniform()
replace stream="a" if  SchoolID==120 & grade==2 & stream=="c" & Rand>=0.5
replace stream="b" if  SchoolID==120 & grade==2 & stream=="c" & Rand<0.5
replace stream="a" if  SchoolID==120 & grade==2 & stream=="d" & Rand>=0.5
replace stream="b" if  SchoolID==120 & grade==2 & stream=="d" & Rand<0.5

replace stream="a" if  SchoolID==121 & grade==2 & stream=="d" & Rand<0.33
replace stream="b" if  SchoolID==121 & grade==2 & stream=="d" & Rand>=0.5 & Rand<0.66
replace stream="c" if  SchoolID==121 & grade==2 & stream=="d" & Rand>=0.66
drop Rand
*/

replace stream="a" if  SchoolID==218 & grade==2 & stream=="b"
replace stream="a" if  SchoolID==306 & grade==2 & stream=="c"
replace stream="a" if  SchoolID==623 & grade==2 & stream=="c"
replace stream="a" if  SchoolID==736 & grade==2 & stream=="c"
replace stream="a" if  SchoolID==829 & grade==2 & stream=="b"


foreach skill in ma se{
	qui tabmiss grd2_kis_a_`skill'_1
	display "Skills `skill' in Swahili/Qs1 has `r(sum)' missing"
	qui tabmiss grd2_kis_a_`skill'_1
	display "Skills `skill' in Swahili/Qs2 has `r(sum)' missing"
}

foreach skill in w se{
	qui tabmiss grd2_eng_a_`skill'_1
	display "Skills `skill' in English/Qs1 has `r(sum)' missing"
	qui tabmiss grd2_eng_a_`skill'_1
	display "Skills `skill' in English/Qs2 has `r(sum)' missing"
}

foreach skill in  bwa j t z{
	qui tabmiss grd2_his_a_`skill'_1
	display "Skills `skill' in Math/Qs1 has `r(sum)' missing"
	qui tabmiss grd2_his_a_`skill'_1
	display "Skills `skill' in Math/Qs2 has `r(sum)' missing"
}


*Some missings are "weird" but I'm gonna assume students just didin't answer the question, i.e. it was wrong... but I do some more cleaning below
foreach var of varlist grd*{
	replace `var'=0 if `var'==.
}

*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in ma se{
	 forvalues i=3/6{
		replace grd2_kis_a_`skill'_`i'=0 if grd2_kis_a_`skill'_1==0 & grd2_kis_a_`skill'_2==0
	}
}

foreach skill in w se{
	forvalues i=3/6{
		replace grd2_eng_a_`skill'_`i'=0 if grd2_eng_a_`skill'_1==0 & grd2_eng_a_`skill'_2==0
	}
}

foreach skill in  bwa j t z{
	replace grd2_his_a_`skill'_3=0 if grd2_his_a_`skill'_1==0 & grd2_his_a_`skill'_2==0
} 


*Calculcate how many correct questions per skill */
egen Kis_Maneno=rowtotal(grd2_kis_a_ma_1 grd2_kis_a_ma_2 grd2_kis_a_ma_3 grd2_kis_a_ma_4 grd2_kis_a_ma_5 grd2_kis_a_ma_6)
egen Kis_Sentenci=rowtotal(grd2_kis_a_se_1 grd2_kis_a_se_2 grd2_kis_a_se_3 grd2_kis_a_se_4 grd2_kis_a_se_5 grd2_kis_a_se_6)
gen Kis_Aya=grd2_kis_a_a


egen Eng_Word=rowtotal(grd2_eng_a_w_1 grd2_eng_a_w_2 grd2_eng_a_w_3 grd2_eng_a_w_4 grd2_eng_a_w_5 grd2_eng_a_w_6)
egen Eng_Sentences=rowtotal(grd2_eng_a_se_1 grd2_eng_a_se_2 grd2_eng_a_se_3 grd2_eng_a_se_4 grd2_eng_a_se_5 grd2_eng_a_se_6)
gen Eng_Paragraph=grd2_eng_a_p


egen Math_bwa=rowtotal(grd2_his_a_bwa_1 grd2_his_a_bwa_2 grd2_his_a_bwa_3)
egen Math_j=rowtotal(grd2_his_a_j_1 grd2_his_a_j_2 grd2_his_a_j_3)
egen Math_t=rowtotal(grd2_his_a_t_1 grd2_his_a_t_2 grd2_his_a_t_3)
egen Math_z=rowtotal(grd2_his_a_z_1 grd2_his_a_z_2 grd2_his_a_z_3)


foreach var of varlist  Kis_Maneno Kis_Sentenci Eng_Word Eng_Sentences{
	gen `var'_Pass=`var'>=4 & !missing(`var')
}

foreach var of varlist  Math_bwa Math_j Math_t Math_z{
	gen `var'_Pass=`var'>=2 & !missing(`var')
}

foreach var of varlist  Kis_Aya Eng_Paragraph{
	gen `var'_Pass=`var'==3 & !missing(`var')
}

/*You only do b if you get no more than 1 question correct per section
I assume that if you dont do b, you would have gotten a 1 in every answer!
*/
/*You only do c if you get everything right beforehand
I assume that if you dont do c, you would have gotten everything incorrect!
*/
foreach var of varlist grd2_kis_b*{
	replace `var'=1 if Kis_Maneno>1 | Kis_Sentenci>1 | Kis_Aya>1
}

foreach var of varlist grd2_kis_c*{
	replace `var'=0 if Kis_Maneno<6 | Kis_Sentenci<6 | Kis_Aya<3
}

foreach var of varlist grd2_eng_b*{
	replace `var'=1 if Eng_Word>1 | Eng_Sentences>1 | Eng_Paragraph>1
}

foreach var of varlist grd2_eng_c*{
	replace `var'=0 if Eng_Word<6 | Eng_Sentences<6 | Eng_Paragraph<3
}
foreach var of varlist grd2_his_b*{
	replace `var'=1 if Math_bwa>1 | Math_j>1 | Math_t>1 | Math_z>1
} 
foreach var of varlist grd2_his_c*{
	replace `var'=0 if Math_bwa<3 | Math_j<3 | Math_t<3 | Math_z<3
} 

*Calculate some dummies for the reading questions. dummy_i means you got a score of at least i
foreach var of varlist grd2_*{
	qui sum `var'
	if(`r(max)'==2){
		gen `var'_1=(`var'>=1) & !missing(`var')
		gen `var'_2=(`var'>=2) & !missing(`var')
		drop `var'
	}
	if(`r(max)'==3){
		gen `var'_1=(`var'>=1) & !missing(`var')
		gen `var'_2=(`var'>=2) & !missing(`var')
		gen `var'_3=(`var'>=3) & !missing(`var')
		drop `var'
	}
}



/*Drop questions with all 0 or all 1 since they provide no useful variation*/
foreach var of varlist grd2_*{
	qui sum `var'
	if(`r(sd)'==0) {
		drop `var'
	}
	if(`r(sd)'!=0) {
		egen `var'_Z=std(`var')  
	}
}

*Do PCA to rank students *
pca grd2_kis_*_Z
predict Z_kis, score 
pca grd2_eng_*_Z
predict Z_eng, score 
pca grd2_his_*_Z
predict Z_his, score 

*If  no pre_level, and therefore must assing one (which is the one for no pre-info)
replace KisLevel1=0 if KisLevel1==.
replace EngLevel1=0 if EngLevel1==.
replace MathLevel1=0 if MathLevel1==.

*Calculcate percentiles*
egen Rank_Kis = xtile(Z_kis), by(KisLevel1) nquantiles(100)
egen Rank_Eng = xtile(Z_eng), by(EngLevel1) nquantiles(100)
egen Rank_Math = xtile(Z_his), by(MathLevel1) nquantiles(100)

*Calulcate the total number of students by level
bys KisLevel1: egen StudentsInLevel_Kis=count(Rank_Kis) 
bys EngLevel1: egen StudentsInLevel_Eng=count(Rank_Eng) 
bys MathLevel1: egen StudentsInLevel_Math =count(Rank_Math ) 

*Calcuclate the number of points or rank-points per level
bys KisLevel1: egen TotalPoints_Kis=total(Rank_Kis-1) 
bys EngLevel1: egen TotalPoints_Eng=total(Rank_Eng-1) 
bys MathLevel1: egen TotalPoints_Math =total(Rank_Math-1) 

/*The budget per group is equal to the number of students divided by the total =scalar(BudgetGrd1)*(StudentsInLevel_Kis/_N)*/
/*Then we calculcate how much money we pay per point which is the the number above divided by total number of points */
/*We the calculcate how much each student gets, which is the rank of the student divided multiplied by the number above */
gen Payment_Total_Kis=(Rank_Kis-1)*(scalar(BudgetKis_Grd2_Mash)*(StudentsInLevel_Kis/_N))/TotalPoints_Kis
gen Payment_Total_Eng=(Rank_Eng-1)*(scalar(BudgetEng_Grd2_Mash)*(StudentsInLevel_Eng/_N))/TotalPoints_Eng
gen Payment_Total_Math=(Rank_Math-1)*(scalar(BudgetMath_Grd2_Mash)*(StudentsInLevel_Math/_N))/TotalPoints_Math

preserve
keep name- studentID stuname_2014 Rank_Kis Rank_Eng Rank_Math KisLevel1 EngLevel1 MathLevel1 tested2015 *_Pass
saveold "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd2Rankings_Students.dta",replace version(12)
restore

preserve
*Collapse to get the payment per school/grade/stream
collapse (count) Sudents=KisLevel1 (sum) Payment_Total_* Rank_* , by(SchoolID grade stream)
saveold "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd2_Payments_v2.dta",replace version(12)
restore

foreach subject in Kis Math Eng{
preserve
*Calculcate final payments by school,grade,stream
collapse (count) Sudents=grade (sum) Payment_Total_* (max) Max`subject'=Rank_`subject' (min) Min`subject'=Rank_`subject' (median) Median`subject'=Rank_`subject' , by(SchoolID districtid `subject'Level1)
gen Payment_Total_`subject'_PS=Payment_Total_`subject'/Sudents
bys `subject'Level1: egen MaxPayment`subject'=max(Payment_Total_`subject'_PS)
replace MaxPayment`subject'=MaxPayment`subject'*Sudents
gen PropPayment`subject'=Payment_Total_`subject'/MaxPayment`subject'
bys districtid: egen MaxPropDistrict`subject'=max(PropPayment`subject')

saveold "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd2_TeacherReport_`subject'.dta",replace version(12)

collapse (sum) Sudents Payment_Total_`subject', by(SchoolID districtid)
gen Payment_Total_`subject'_PS=Payment_Total_`subject'/Sudents
egen MaxPayment`subject'=max(Payment_Total_`subject'_PS)
replace MaxPayment`subject'=MaxPayment`subject'*Sudents
gen PropPayment`subject'=Payment_Total_`subject'/MaxPayment`subject'
bys districtid: egen MaxPropDistrict`subject'=max(PropPayment`subject')

levelsof SchoolID
foreach school in `r(levels)' {
estpost tabstat Sudents MaxPayment`subject' Payment_Total_`subject' PropPayment`subject' MaxPropDistrict`subject' if SchoolID==`school', statistics(mean) columns(variables)
esttab using "CreatedData/4 Intervention/TwaEL_2015/pdf_School/latexTables/mash_`school'_`subject'_grd2.csv", cells("Sudents MaxPayment`subject' Payment_Total_`subject' PropPayment`subject' MaxPropDistrict`subject'") replace fragment noeqlines nolines nogaps  nomti nonum nodep nonotes noobs tab
}


restore
}

**********************************
**********	GRADE 3 **************
**********************************

use "CreatedData/4 Intervention/TwaEL_2015/EL2015_Student_FinalRun_Mashindano_CLEAN.dta", clear
rename grd3_kis_c_h1 grd3_eng_c_s
rename grd3_eng_c_c_11 grd3_eng_c_c_2
drop  district   gender set start startampm volname end endampm
replace stream = lower(stream)
rename schoolid SchoolID
rename studentid studentID
replace studentID=. if studentID==-999
replace studentID=. if studentID==-99
replace studentID=. if studentID==999
replace studentID=. if studentID==99

merge m:1 studentID using "CreatedData/4 Intervention/TwaEL_2015/2015_GroupsStudent_Mash_Grade23.dta", keepus( KisLevel2 EngLevel2 MathLevel2)
drop if _merge==2
drop _merge

ds, has(type string)
foreach var of varlist `r(varlist)'{
replace `var'="" if `var'=="--blank--" | `var'=="X"
}

destring , replace

drop if grade!=3

foreach var of varlist SchoolID grade stream{
	qui tabmiss `var'
	display "`r(sum)' missing in variable `var'"
	display "They are dropped"
	drop if	missing(`var')
}


/*
replace stream="a" if  SchoolID==120 & grade==2 & stream=="c"
replace stream="a" if  SchoolID==127 & grade==2 & stream=="e"
replace stream="a" if  SchoolID==218 & grade==2 & stream=="b"
replace stream="a" if  SchoolID==219 & grade==2 & stream=="b"
*/
merge 1:1 name using "CreatedData/4 Intervention/TwaEL_2015/CorrectStream.dta", update replace
drop if _merge==2
drop _merge

qui tabmiss grd3_kis_a_h
display "Skills Reading in Swahili/Qs1 has `r(sum)' missing"
qui tabmiss grd3_eng_a_s
display "Skills English in Swahili/Qs1 has `r(sum)' missing"



foreach skill in  j t z g{
	qui tabmiss grd3_his_a_`skill'_1
	display "Skills `skill' in Math/Qs1 has `r(sum)' missing"
	qui tabmiss grd3_his_a_`skill'_1
	display "Skills `skill' in Math/Qs2 has `r(sum)' missing"
}




foreach var of varlist grd*{
	replace `var'=0 if `var'==.
}

*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_kis_a_m_1=0 if grd3_kis_a_h==0
replace grd3_kis_a_m_2=0 if grd3_kis_a_h==0


*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_eng_a_c_1=0 if grd3_eng_a_s==0
replace grd3_eng_a_c_2=0 if grd3_eng_a_s==0

*First data cleaning process for math is as before...
foreach skill in  j t z g{
	replace grd3_his_a_`skill'_3=0 if grd3_his_a_`skill'_1==0 & grd3_his_a_`skill'_2==0

} 



gen Kis_Story=grd3_kis_a_h
egen Kis_Comp=rowtotal(grd3_kis_a_m_1 grd3_kis_a_m_2)
     
gen Eng_Story=grd3_eng_a_s
egen Eng_Comp=rowtotal(grd3_eng_a_c_1 grd3_eng_a_c_2)
 

egen Math_j=rowtotal(grd3_his_a_j_1 grd3_his_a_j_2 grd3_his_a_j_3)
egen Math_t=rowtotal(grd3_his_a_t_1 grd3_his_a_t_2 grd3_his_a_t_3)
egen Math_z=rowtotal(grd3_his_a_z_1 grd3_his_a_z_2 grd3_his_a_z_3)
egen Math_g=rowtotal(grd3_his_a_g_1 grd3_his_a_g_2 grd3_his_a_g_3)


foreach var of varlist  Math_j Math_t Math_z Math_g{
	gen `var'_Pass=`var'>=2 & !missing(`var')
}

foreach var of varlist  Kis_Story Eng_Story{
	gen `var'_Pass=`var'==3 & !missing(`var')
}

foreach var of varlist  Kis_Comp Eng_Comp {
	gen `var'_Pass=`var'==2 & !missing(`var')
}

/*You only do b if you get no more than 1 question correct per section
I assume that if you dont do b, you would have gotten a 1 in every answer!
*/
/*You only do c if you get everything right beforehand
I assume that if you dont do c, you would have gotten everything incorrect!
*/
foreach var of varlist grd3_kis_b*{
	replace `var'=1 if Kis_Story>1 | Kis_Comp>1
}

foreach var of varlist grd3_kis_c*{
	replace `var'=0 if Kis_Story<3 | Kis_Comp<2
}

foreach var of varlist grd3_eng_b*{
	replace `var'=1 if Eng_Story>1 | Eng_Comp>1
}

foreach var of varlist grd3_eng_c*{
	replace `var'=0 if Eng_Story<3 | Eng_Comp<2
}
foreach var of varlist grd3_his_b*{
	replace `var'=1 if  Math_j>1 | Math_t>1 | Math_z>1 | Math_g>1
} 
foreach var of varlist grd3_his_c*{
	replace `var'=0 if  Math_j<3 | Math_t<3 | Math_z<3 | Math_g<3
} 


*Calculate some dummies for the reading questions. dummy_i means you got a score of at least i
foreach var of varlist grd3_*{
	qui sum `var'
	if(`r(max)'==2){
		gen `var'_1=(`var'>=1) & !missing(`var')
		gen `var'_2=(`var'>=2) & !missing(`var')
		drop `var'
	}
	if(`r(max)'==3){
		gen `var'_1=(`var'>=1) & !missing(`var')
		gen `var'_2=(`var'>=2) & !missing(`var')
		gen `var'_3=(`var'>=3) & !missing(`var')
		drop `var'
	}
}

/*Drop questions with all 0 or all 1 since they provide no useful variation*/
foreach var of varlist grd3_*{
	qui sum `var'
	if(`r(sd)'==0) {
		drop `var'
	}
	if(`r(sd)'!=0) {
		egen `var'_Z=std(`var')  
	}
}

*Do PCA to rank students accoridng to first component
pca grd3_kis_*_Z
predict Z_kis, score 
pca grd3_eng_*_Z
predict Z_eng, score 
pca grd3_his_*_Z
predict Z_his, score 

*If  no pre_level, and therefore must assing one (which is the one for no pre-info)
replace KisLevel2=0 if KisLevel2==.
replace EngLevel2=0 if EngLevel2==.
replace MathLevel2=0 if MathLevel2==.

*Rank students within groups
egen Rank_Kis = xtile(Z_kis), by(KisLevel2) nquantiles(100)
egen Rank_Eng = xtile(Z_eng), by(EngLevel2) nquantiles(100)
egen Rank_Math = xtile(Z_his), by(MathLevel2) nquantiles(100)

*Count the number of students in each group
bys KisLevel2: egen StudentsInLevel_Kis=count(Rank_Kis) 
bys EngLevel2: egen StudentsInLevel_Eng=count(Rank_Eng) 
bys MathLevel2: egen StudentsInLevel_Math =count(Rank_Math ) 

*Calculcate the number of points or rank-points.
bys KisLevel2: egen TotalPoints_Kis=total(Rank_Kis-1) 
bys EngLevel2: egen TotalPoints_Eng=total(Rank_Eng-1) 
bys MathLevel2: egen TotalPoints_Math =total(Rank_Math-1) 

/*The budget per group is equal to the number of students divided by the total =scalar(BudgetGrd1)*(StudentsInLevel_Kis/_N)*/
/*Then we calculcate how much money we pay per point which is the the number above divided by total number of points */
/*We the calculcate how much each student gets, which is the rank of the student divided multiplied by the number above */
gen Payment_Total_Kis=(Rank_Kis-1)*(scalar(BudgetKis_Grd3_Mash)*(StudentsInLevel_Kis/_N))/TotalPoints_Kis
gen Payment_Total_Eng=(Rank_Eng-1)*(scalar(BudgetEng_Grd3_Mash)*(StudentsInLevel_Eng/_N))/TotalPoints_Eng
gen Payment_Total_Math=(Rank_Math-1)*(scalar(BudgetMath_Grd3_Mash)*(StudentsInLevel_Math/_N))/TotalPoints_Math

preserve
keep name- studentID stuname_2014 Rank_Kis Rank_Eng Rank_Math KisLevel2 EngLevel2 MathLevel2 tested2015 *_Pass
saveold "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd3Rankings_Students.dta",replace version(12)
restore

preserve
*Calculcate the payments per school/grade/subject
collapse (count) Sudents=KisLevel2 (sum) Payment_Total_* Rank_* , by(SchoolID grade stream)
saveold "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd3_Payments_v2.dta",replace version(12)
restore

foreach subject in Kis Math Eng{
preserve
*Calculcate final payments by school,grade,stream
collapse (count) Sudents=grade (sum) Payment_Total_* (max) Max`subject'=Rank_`subject' (min) Min`subject'=Rank_`subject' (median) Median`subject'=Rank_`subject' , by(SchoolID districtid `subject'Level2)
gen Payment_Total_`subject'_PS=Payment_Total_`subject'/Sudents
bys `subject'Level2: egen MaxPayment`subject'=max(Payment_Total_`subject'_PS)
replace MaxPayment`subject'=MaxPayment`subject'*Sudents
gen PropPayment`subject'=Payment_Total_`subject'/MaxPayment`subject'
bys districtid: egen MaxPropDistrict`subject'=max(PropPayment`subject')

saveold "CreatedData/4 Intervention/TwaEL_2015/Mash_Grd3_TeacherReport_`subject'.dta",replace version(12)

collapse (sum) Sudents Payment_Total_`subject', by(SchoolID districtid)
gen Payment_Total_`subject'_PS=Payment_Total_`subject'/Sudents
egen MaxPayment`subject'=max(Payment_Total_`subject'_PS)
replace MaxPayment`subject'=MaxPayment`subject'*Sudents
gen PropPayment`subject'=Payment_Total_`subject'/MaxPayment`subject'
bys districtid: egen MaxPropDistrict`subject'=max(PropPayment`subject')

levelsof SchoolID
foreach school in `r(levels)' {
estpost tabstat Sudents MaxPayment`subject' Payment_Total_`subject' PropPayment`subject' MaxPropDistrict`subject' if SchoolID==`school', statistics(mean) columns(variables)
esttab using "CreatedData/4 Intervention/TwaEL_2015/pdf_School/latexTables/mash_`school'_`subject'_grd3.csv", cells("Sudents MaxPayment`subject' Payment_Total_`subject' PropPayment`subject' MaxPropDistrict`subject'") replace fragment noeqlines nolines nogaps  nomti nonum nodep nonotes noobs tab
}

restore
}
log close


