**********************
**************Student
************************
use "$base_out/ConsolidatedYr34/Student_School_House_Teacher_Char.dta", clear
fvset base default treatarm 
global treatmentlist TreatmentLevels TreatmentGains
gen TreatmentLevels2=TreatmentLevels
gen TreatmentGains2=TreatmentGains
global treatmentlist2 TreatmentLevels2 TreatmentGains2
label var TreatmentLevels "Levels (\$\beta_1\$)"
label var TreatmentGains "P4Pctile (\$\beta_2\$)"
keep ${schoolcontrol} *T3 *T6  SchoolID DistrictID StrataScore treat* Treat* upid studentid 
keep ${schoolcontrol} *Pass* Grade* SchoolID DistrictID StrataScore treat* Treat* upid studentid Z_kiswahili* Z_hisabati*
gen ID=_n

global student2 shoes_T2 socks_T2 dirty_T2 uniformdirty_T2 uniformtorn_T2  ringworm_T2 CloseToeShoe_T2

sum Z_hisabati_T6 if Math_id_Pass_T6==1 & Grade_T6==1,d
count if Z_hisabati_T6 < r(p1) & Grade_T6==1
local num=r(N)
count if Grade_T6==1
local denom=r(N)
di (`num'/`denom')*100
		
sum Z_hisabati_T6 if Math_t_Pass_T6==1 & Grade_T6==1,d
count if Z_hisabati_T6 < r(p1) & Grade_T6==1
local num=r(N)
count if Grade_T6==1
local denom=r(N)
di (`num'/`denom')*100



sum Z_kiswahili_T6 if Kis_Silabi_Pass_T6==1 & Grade_T6==1,d
count if Z_kiswahili_T6 < r(p1) & Grade_T6==1
local num=r(N)
count if Grade_T6==1
local denom=r(N)
di (`num'/`denom')*100
		
sum Z_kiswahili_T6 if Kis_Sentenci_Pass_T6==1 & Grade_T6==1,d
count if Z_kiswahili_T6 < r(p1) & Grade_T6==1
local num=r(N)
count if Grade_T6==1
local denom=r(N)
di (`num'/`denom')*100



foreach time in T3 T6{
	foreach var in Kis_Silabi_Pass Kis_Maneno_Pass Kis_Sentenci_Pass{ 
		sum Z_kiswahili_`time' if `var'_`time'==1 & Grade_`time'==1,d
		count if Z_kiswahili_`time' < r(p1) & Grade_`time'==1
		local num=r(N)
		count if Grade_`time'==1
		local denom=r(N)
		local tempm=string((`num'/`denom')*100, "%9.2fc")			
		file open newfile using "$latexcodes/Percentile_`var'_`time'_Grd1.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
	
	foreach var in Kis_Maneno_Pass Kis_Sentenci_Pass Kis_Aya_Pass{ 
		sum Z_kiswahili_`time' if `var'_`time'==1 & Grade_`time'==2,d
		count if Z_kiswahili_`time' < r(p1) & Grade_`time'==2
		local num=r(N)
		count if Grade_`time'==2
		local denom=r(N)
		local tempm=string((`num'/`denom')*100, "%9.2fc")			
		file open newfile using "$latexcodes/Percentile_`var'_`time'_Grd2.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
	
	foreach var in Kis_Story_Pass Kis_Comp_Pass{ 
		sum Z_kiswahili_`time' if `var'_`time'==1 & Grade_`time'==3,d
		count if Z_kiswahili_`time' < r(p1) & Grade_`time'==3
		local num=r(N)
		count if Grade_`time'==3
		local denom=r(N)
		local tempm=string((`num'/`denom')*100, "%9.2fc")			
		file open newfile using "$latexcodes/Percentile_`var'_`time'_Grd3.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
	
	
	foreach var in Math_id_Pass Math_uta_Pass Math_bwa_Pass Math_j_Pass Math_t_Pass{ 
		sum Z_hisabati_`time' if `var'_`time'==1 & Grade_`time'==1,d
		count if Z_hisabati_`time' < r(p1) & Grade_`time'==1
		local num=r(N)
		count if Grade_`time'==1
		local denom=r(N)
		local tempm=string((`num'/`denom')*100, "%9.2fc")			
		file open newfile using "$latexcodes/Percentile_`var'_`time'_Grd1.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
	
	foreach var in Math_uta_Pass Math_bwa_Pass Math_j_Pass Math_t_Pass Math_z_Pass{ 
		sum Z_hisabati_`time' if `var'_`time'==1 & Grade_`time'==2,d
		count if Z_hisabati_`time' < r(p1) & Grade_`time'==2
		local num=r(N)
		count if Grade_`time'==2
		local denom=r(N)
		local tempm=string((`num'/`denom')*100, "%9.2fc")		
		file open newfile using "$latexcodes/Percentile_`var'_`time'_Grd2.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
	
	foreach var in Math_j_Pass Math_t_Pass Math_z_Pass Math_g_Pass{ 
		sum Z_hisabati_`time' if `var'_`time'==1 & Grade_`time'==3,d
		count if Z_hisabati_`time' < r(p1) & Grade_`time'==3
		local num=r(N)
		count if Grade_`time'==3
		local denom=r(N)
		local tempm=string((`num'/`denom')*100, "%9.2fc")		
		file open newfile using "$latexcodes/Percentile_`var'_`time'_Grd3.tex", write replace
		file write newfile "`tempm'"
		file close newfile
	}
}