

*use  "RawData/12 Intervention_KFII/4 Endline 2016/3_TwaEL2016_StudentData_FINAL_nopii.dta", clear
use "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_Mashindano_CLEAN.dta", clear
append using "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_StadiandControl_CLEAN.dta"

rename districtid DistrictID
rename grade Grade
rename stream Stream
rename testedEL2016 TestedEL2016
rename schoolid SchoolID

**Calculate enrollmente not EDI, but previous TWA
preserve
collapse (count) Enrollment=TestedEL2016, by(Grade treatarm2)
drop if Grade==.
saveold "CreatedData/ConsolidatedYr34/EnroollmnetGradeTWA_2016.dta", replace
restore
***Back to main code
tabstat Grade, by(treatarm2) statistics (count)
tabstat Grade if treatarm2==2, by(Grade) statistics (count)
tabstat Grade if treatarm2==1, by(Grade) statistics (count)


foreach var of varlist grd1*{
replace `var'=. if Grade!=1 | TestedEL2016==0
replace `var'=0 if `var'==. & Grade==1 & TestedEL2016==1
}

foreach var of varlist grd2*{
replace `var'=. if Grade!=2 | TestedEL2016==0
replace `var'=0 if `var'==. & Grade==2 & TestedEL2016==1
}

foreach var of varlist grd3*{
replace `var'=. if Grade!=3 | TestedEL2016==0
replace `var'=0 if `var'==. & Grade==3 & TestedEL2016==1
}



*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in si ma se{
	 forvalues i=3/6{
		replace grd1_kis_a_`skill'_`i'=0 if grd1_kis_a_`skill'_1==0 & grd1_kis_a_`skill'_2==0 & Grade==1 & TestedEL2016==1
	}
}


foreach skill in id uta bwa j t{
	replace grd1_his_a_`skill'_3=0 if grd1_his_a_`skill'_1==0 & grd1_his_a_`skill'_2==0 & Grade==1 & TestedEL2016==1
} 


/*Total answers per skill*/
egen Kis_Silabi_Grd1=rowtotal(grd1_kis_a_si_1 grd1_kis_a_si_2 grd1_kis_a_si_3 grd1_kis_a_si_4 grd1_kis_a_si_5 grd1_kis_a_si_6) if Grade==1 & TestedEL2016==1, missing
egen Kis_Maneno_Grd1=rowtotal(grd1_kis_a_ma_1 grd1_kis_a_ma_2 grd1_kis_a_ma_3 grd1_kis_a_ma_4 grd1_kis_a_ma_5 grd1_kis_a_ma_6) if Grade==1 & TestedEL2016==1, missing
foreach var of varlist grd1_kis_a_se*{
	gen Pass_`var'=(`var'>=2) if !missing(`var')
}
egen Kis_Sentenci_Grd1=rowtotal(Pass_grd1_kis_a_se*) if Grade==1 & TestedEL2016==1, missing
drop Pass_grd1_kis_a_se*

egen Math_id_Grd1=rowtotal(grd1_his_a_id_1 grd1_his_a_id_2 grd1_his_a_id_3) if Grade==1 & TestedEL2016==1, missing
egen Math_uta_Grd1=rowtotal(grd1_his_a_uta_1 grd1_his_a_uta_2 grd1_his_a_uta_3) if Grade==1 & TestedEL2016==1, missing
egen Math_bwa_Grd1=rowtotal(grd1_his_a_bwa_1 grd1_his_a_bwa_2 grd1_his_a_bwa_3) if Grade==1 & TestedEL2016==1, missing
egen Math_j_Grd1=rowtotal(grd1_his_a_j_1 grd1_his_a_j_2 grd1_his_a_j_3) if Grade==1 & TestedEL2016==1, missing
egen Math_t_Grd1=rowtotal(grd1_his_a_t_1 grd1_his_a_t_2 grd1_his_a_t_3) if Grade==1 & TestedEL2016==1, missing

*Only goes to b section if NO more than 1 answer right in each section */
*Only go to c if everything is perfect! */
*Therefire if you didint go to b, I'm assuming you had all of these correct */
*If you didint go to C I'm assuming you would have gotten all those wrong!
foreach var of varlist grd1_kis_b*{
	replace `var'=1 if (Kis_Silabi_Grd1>1 | Kis_Maneno_Grd1>1 | Kis_Sentenci_Grd1>1) & Grade==1 & TestedEL2016==1
}

foreach var of varlist grd1_kis_c*{
	replace `var'=0 if (Kis_Silabi_Grd1<6 | Kis_Maneno_Grd1<6 | Kis_Sentenci_Grd1<18) & Grade==1 & TestedEL2016==1
}


foreach var of varlist grd1_his_c*{
	replace `var'=0 if (Math_id_Grd1<3 | Math_uta_Grd1<3 | Math_bwa_Grd1<3 | Math_j_Grd1<3 | Math_t_Grd1<3) & Grade==1 & TestedEL2016==1
} 


egen Kis_Picha_Grd1=rowtotal(grd1_kis_b_pi_*) if Grade==1 & TestedEL2016==1, missing
egen Kis_PichaLetter_Grd1=rowtotal(grd1_kis_b_ph_*) if Grade==1 & TestedEL2016==1, missing
egen Kis_Aya_Grd1=rowtotal(grd1_kis_c_h* ) if Grade==1 & TestedEL2016==1, missing
egen Math_z_Grd1=rowtotal(grd1_his_c_z_* ) if Grade==1 & TestedEL2016==1

*I create dummies for the different levels of reading. So dummy_2 is if you got a 2 or higher. Same for other numbers
foreach var of varlist grd1_*{
	qui sum `var' if Grade==1 & TestedEL2016==1
	if(`r(max)'==2){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==1 & TestedEL2016==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==1 & TestedEL2016==1
		drop `var'
	}
	else if(`r(max)'==3){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==1 & TestedEL2016==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==1 & TestedEL2016==1
		gen `var'_3=(`var'>=3) if !missing(`var') & Grade==1 & TestedEL2016==1
		drop `var' 
	}
	else if(`r(max)'==4){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==1 & TestedEL2016==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==1 & TestedEL2016==1
		gen `var'_3=(`var'>=3) if !missing(`var') & Grade==1 & TestedEL2016==1
		gen `var'_4=(`var'>=4) if !missing(`var') & Grade==1 & TestedEL2016==1
		drop `var'
	}
}




*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in ma se{
	 forvalues i=3/6{
		replace grd2_kis_a_`skill'_`i'=0 if grd2_kis_a_`skill'_1==0 & grd2_kis_a_`skill'_2==0 & Grade==2 & TestedEL2016==1
	} 
}



foreach skill in  bwa j t {
	replace grd2_his_a_`skill'_3=0 if grd2_his_a_`skill'_1==0 & grd2_his_a_`skill'_2==0 & Grade==2 & TestedEL2016==1
} 



*Calculcate how many correct questions per skill */
egen Kis_Maneno_Grd2=rowtotal(grd2_kis_a_ma_1 grd2_kis_a_ma_2 grd2_kis_a_ma_3 grd2_kis_a_ma_4 grd2_kis_a_ma_5 grd2_kis_a_ma_6) if Grade==2 & TestedEL2016==1, missing
foreach var of varlist grd2_kis_a_se*{
	gen Pass_`var'=(`var'>=2) if !missing(`var')
}
egen Kis_Sentenci_Grd2=rowtotal(Pass_grd2_kis_a_se*) if Grade==2 & TestedEL2016==1, missing
drop Pass_grd2_kis_a_se*
gen Kis_Aya_Grd2=grd2_kis_a_h if Grade==2 & TestedEL2016==1
egen Kis_Comp_Grd2=rowtotal(grd2_kis_a_m_1 grd2_kis_a_m_2) if Grade==2 & TestedEL2016==1
 

egen Math_bwa_Grd2=rowtotal(grd2_his_a_bwa_1 grd2_his_a_bwa_2 grd2_his_a_bwa_3) if Grade==2 & TestedEL2016==1, missing
egen Math_j_Grd2=rowtotal(grd2_his_a_j_1 grd2_his_a_j_2 grd2_his_a_j_3) if Grade==2 & TestedEL2016==1, missing
egen Math_t_Grd2=rowtotal(grd2_his_a_t_1 grd2_his_a_t_2 grd2_his_a_t_3) if Grade==2 & TestedEL2016==1, missing


/*You only do b if you get no more than 1 question correct per section
I assume that if you dont do b, you would have gotten a 1 in every answer!
*/
/*You only do c if you get everything right beforehand
I assume that if you dont do c, you would have gotten everything incorrect!
*/


foreach var of varlist grd2_kis_b*{
	replace `var'=1 if (Kis_Maneno_Grd2>1 | Kis_Sentenci_Grd2>1 | Kis_Aya_Grd2>1) & Grade==2 & TestedEL2016==1
}

foreach var of varlist grd2_kis_c*{
	replace `var'=0 if (Kis_Maneno_Grd2<6 | Kis_Sentenci_Grd2<18 | Kis_Aya_Grd2<3) & Grade==2 & TestedEL2016==1
}



foreach var of varlist grd2_his_b*{
	replace `var'=1 if (Math_bwa_Grd2>1 | Math_j_Grd2>1 | Math_t_Grd2>1 ) & Grade==2 & TestedEL2016==1
} 
foreach var of varlist grd2_his_c*{
	replace `var'=0 if (Math_bwa_Grd2<3 | Math_j_Grd2<3 | Math_t_Grd2<3 ) & Grade==2 & TestedEL2016==1
} 


egen Kis_Picha_Grd2=rowtotal(grd2_kis_b_pi_*) if Grade==2 & TestedEL2016==1, missing
egen Kis_Silabi_Grd2=rowtotal(grd2_kis_b_si_*) if Grade==2 & TestedEL2016==1, missing
egen Kis_MaswaliAya_Grd2=rowtotal(grd2_kis_c_mh_* ) if Grade==2 & TestedEL2016==1, missing


egen Math_uta_Grd2=rowtotal(grd2_his_b_uta_* ) if Grade==2 & TestedEL2016==1, missing
egen Math_bwab_Grd2=rowtotal(grd2_his_b_bwa_* ) if Grade==2 & TestedEL2016==1, missing
egen Math_z_Grd2=rowtotal(grd2_his_c_z_* ) if Grade==2 & TestedEL2016==1, missing
egen Math_g_Grd2=rowtotal(grd2_his_c_g_* ) if Grade==2 & TestedEL2016==1, missing



*Calculate some dummies for the reading questions. dummy_i means you got a score of at least i
foreach var of varlist grd2_*{
	qui sum `var' if Grade==2 & TestedEL2016==1
	if(`r(max)'==2){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==2 & TestedEL2016==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==2 & TestedEL2016==1
		drop `var'
	}
	else if(`r(max)'==3){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==2 & TestedEL2016==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==2 & TestedEL2016==1
		gen `var'_3=(`var'>=3) if !missing(`var') & Grade==2 & TestedEL2016==1
		drop `var'
	}
	else if(`r(max)'==4){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==2 & TestedEL2016==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==2 & TestedEL2016==1
		gen `var'_3=(`var'>=3) if !missing(`var') & Grade==2 & TestedEL2016==1
		gen `var'_4=(`var'>=4) if !missing(`var') & Grade==2 & TestedEL2016==1
		drop `var'
	}
}




*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_kis_a_m_1=0 if grd3_kis_a_h==0  & Grade==3 & TestedEL2016==1
replace grd3_kis_a_m_2=0 if grd3_kis_a_h==0  & Grade==3 & TestedEL2016==1


*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_eng_a_c_1=0 if grd3_eng_a_s==0 & Grade==3 & TestedEL2016==1
replace grd3_eng_a_c_2=0 if grd3_eng_a_s==0 & Grade==3 & TestedEL2016==1

*First data cleaning process for math is as before...
foreach skill in  j t z g{
	replace grd3_his_a_`skill'_3=0 if grd3_his_a_`skill'_1==0 & grd3_his_a_`skill'_2==0 & Grade==3 & TestedEL2016==1
} 


gen Kis_Story_Grd3=grd3_kis_a_h if Grade==3
egen Kis_Comp_Grd3=rowtotal(grd3_kis_a_m_1 grd3_kis_a_m_2) if Grade==3 & TestedEL2016==1, missing
     
gen Eng_Story_Grd3=grd3_eng_a_s if Grade==3
egen Eng_Comp_Grd3=rowtotal(grd3_eng_a_c_1 grd3_eng_a_c_2) if Grade==3 & TestedEL2016==1, missing
 

egen Math_j_Grd3=rowtotal(grd3_his_a_j_1 grd3_his_a_j_2 grd3_his_a_j_3) if Grade==3 & TestedEL2016==1, missing
egen Math_t_Grd3=rowtotal(grd3_his_a_t_1 grd3_his_a_t_2 grd3_his_a_t_3) if Grade==3 & TestedEL2016==1, missing
egen Math_z_Grd3=rowtotal(grd3_his_a_z_1 grd3_his_a_z_2 grd3_his_a_z_3) if Grade==3 & TestedEL2016==1, missing
egen Math_g_Grd3=rowtotal(grd3_his_a_g_1 grd3_his_a_g_2 grd3_his_a_g_3) if Grade==3 & TestedEL2016==1, missing


/*You only do b if you get no more than 1 question correct per section
I assume that if you dont do b, you would have gotten a 1 in every answer!
*/
/*You only do c if you get everything right beforehand
I assume that if you dont do c, you would have gotten everything incorrect!
*/
foreach var of varlist grd3_kis_b*{
	replace `var'=1 if (Kis_Story_Grd3>1 | Kis_Comp_Grd3>1) & Grade==3 & TestedEL2016==1
}

foreach var of varlist grd3_kis_c*{
	replace `var'=0 if (Kis_Story_Grd3<3 | Kis_Comp_Grd3<2) & Grade==3 & TestedEL2016==1
}

foreach var of varlist grd3_eng_b*{
	replace `var'=1 if (Eng_Story_Grd3>1 | Eng_Comp_Grd3>1) & Grade==3 & TestedEL2016==1
}

foreach var of varlist grd3_eng_c*{
	replace `var'=0 if (Eng_Story_Grd3<3 | Eng_Comp_Grd3<2) & Grade==3 & TestedEL2016==1
}
foreach var of varlist grd3_his_b*{
	replace `var'=1 if  (Math_j_Grd3>1 | Math_t_Grd3>1 | Math_z_Grd3>1 | Math_g_Grd3>1) & Grade==3 & TestedEL2016==1
} 
foreach var of varlist grd3_his_c*{
	replace `var'=0 if  (Math_j_Grd3<3 | Math_t_Grd3<3 | Math_z_Grd3<3 | Math_g_Grd3<3) & Grade==3 & TestedEL2016==1
} 


egen Kis_Maneno_Grd3=rowtotal(grd3_kis_b_ma_*) if Grade==3 & TestedEL2016==1, missing

foreach var of varlist grd3_kis_b_se_*{
	gen Pass_`var'=(`var'>=2) if !missing(`var')
}
egen Kis_Sentenci_Grd3=rowtotal(Pass_grd3_kis_b_se*) if Grade==3 & TestedEL2016==1, missing
drop Pass_grd3_kis_b_se*

gen Kis_Aya_Grd3=grd3_kis_b_h if Grade==3 & TestedEL2016==1

gen Kis_StoryAd_Grd3=grd3_kis_c_h if Grade==3 & TestedEL2016==1
egen Kis_ComPlus_Grd3=rowtotal(grd3_kis_c_m_*) if Grade==3 & TestedEL2016==1, missing


egen Eng_Word_Grd3=rowtotal(grd3_eng_b_w_*) if Grade==3 & TestedEL2016==1, missing

foreach var of varlist grd3_eng_b_se_*{
	gen Pass_`var'=(`var'>=2) if !missing(`var')
}
egen Eng_Sentences_Grd3=rowtotal(Pass_grd3_eng_b_se_*) if Grade==3 & TestedEL2016==1, missing
drop Pass_grd3_eng_b_se_*
gen Eng_Paragraph_Grd3=grd3_eng_b_p if Grade==3 & TestedEL2016==1
gen Eng_StoryAd_Grd3=grd3_eng_c_s if Grade==3 & TestedEL2016==1
egen Eng_ComPlus_Grd3=rowtotal(grd3_eng_c_c_*) if Grade==3 & TestedEL2016==1, missing



egen Math_uta_Grd3=rowtotal(grd3_his_b_uta_*) if Grade==3 & TestedEL2016==1, missing
egen Math_bwa_Grd3=rowtotal(grd3_his_b_bwa_*) if Grade==3 & TestedEL2016==1, missing
egen Math_gAdv_Grd3=rowtotal(grd3_his_c_g_*) if Grade==3 & TestedEL2016==1, missing



*Calculate some dummies for the reading questions. dummy_i means you got a score of at least i
foreach var of varlist grd3_*{
	qui sum `var' if Grade==3 & TestedEL2016==1
	if(`r(max)'==2){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==3 & TestedEL2016==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==3 & TestedEL2016==1
		drop `var'
	}
	else if(`r(max)'==3){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==3 & TestedEL2016==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==3 & TestedEL2016==1
		gen `var'_3=(`var'>=3) if !missing(`var') & Grade==3 & TestedEL2016==1
		drop `var'
	}
	else if(`r(max)'==4){
		gen `var'_1=(`var'>=1) if !missing(`var') & Grade==3 & TestedEL2016==1
		gen `var'_2=(`var'>=2) if !missing(`var') & Grade==3 & TestedEL2016==1
		gen `var'_3=(`var'>=3) if !missing(`var') & Grade==3 & TestedEL2016==1
		gen `var'_4=(`var'>=4) if !missing(`var') & Grade==3 & TestedEL2016==1
		drop `var'
	}
}




*generate passing dummies. Have to be careful since stata thinks missing is infinity
foreach var of varlist  Eng_Sentences_Grd3 Eng_Word_Grd3 Kis_Sentenci_Grd3 Kis_Maneno_Grd3  Kis_Silabi_Grd2 Kis_Picha_Grd2  Kis_Picha_Grd1 Kis_PichaLetter_Grd1 Kis_Silabi_Grd1 Kis_Maneno_Grd1 Kis_Sentenci_Grd1   Kis_Maneno_Grd2 Kis_Sentenci_Grd2  {
	gen `var'_Pass=`var'>=4 if !missing(`var') & TestedEL2016==1
}

foreach var of varlist Math_gAdv_Grd3  Math_bwa_Grd3 Math_uta_Grd3 Math_g_Grd2 Math_bwab_Grd2 Math_uta_Grd2 Math_z_Grd1 Math_id_Grd1 Math_uta_Grd1 Math_bwa_Grd1 Math_j_Grd1 Math_t_Grd1 Math_bwa_Grd2 Math_j_Grd2 Math_t_Grd2 Math_z_Grd2 Math_j_Grd3 Math_t_Grd3 Math_z_Grd3 Math_g_Grd3{
	gen `var'_Pass=`var'>=2 if !missing(`var') & TestedEL2016==1
}


foreach var of varlist  Eng_StoryAd_Grd3 Eng_Paragraph_Grd3 Kis_StoryAd_Grd3 Kis_Aya_Grd3  Kis_Aya_Grd1 Kis_Aya_Grd2  Kis_Story_Grd3 Eng_Story_Grd3 {
	gen `var'_Pass=`var'==3 if !missing(`var') & TestedEL2016==1
}


foreach var of varlist  Kis_Comp_Grd2 Eng_ComPlus_Grd3 Kis_ComPlus_Grd3  Kis_MaswaliAya_Grd2 Kis_Comp_Grd3 Eng_Comp_Grd3  {
	gen `var'_Pass=`var'>=2 if !missing(`var') & TestedEL2016==1
}

preserve 
keep studentid DistrictID SchoolID Grade Stream TestedEL2016 DistrictID gender *_Grd1_Pass  *_Grd2_Pass *_Grd3_Pass treatarm2 name stuname
compress
saveold "CreatedData\ConsolidatedYr34\2016_AllStudents_PassRates.dta", version(12) replace
restore

estpost tabstat *Grd1*_Pass if TestedEL2016==1, columns(statistics) by(treatarm2)
esttab using "Results/ExcelTables/2016_PassRateTwaY3_Grd1.csv", replace main(mean) unstack 

estpost tabstat *Grd2*_Pass if TestedEL2016==1, columns(statistics) by(treatarm2)
esttab using "Results/ExcelTables/2016_PassRateTwaY3_Grd2.csv", replace main(mean) unstack 

estpost tabstat *Grd3*_Pass if TestedEL2016==1, columns(statistics) by(treatarm2)
esttab using "Results/ExcelTables/2016_PassRateTwaY3_Grd3.csv", replace main(mean) unstack 

**Calculate pass rates STADI
preserve
collapse (sum) *_Pass TestedEL2016 if TestedEL2016==1, by(Grade treatarm2)
saveold "CreatedData/ConsolidatedYr34/PassRateTWA_2016_EL.dta", replace
restore
***Back to main code

drop if treatarm2!=2 /*"Gains"*/


foreach var of varlist grd1_* grd2_* grd3_*{
	qui sum `var' if TestedEL2016==1
	if(`r(sd)'==0) {
		drop `var'
	}
	if(`r(sd)'!=0) {
		egen `var'_Z=std(`var')  if TestedEL2016==1
	}
}





forvalues i=1/2{

pca grd`i'_kis_*_Z
predict grd`i'_kis_Z, score
*egen grd`i'_kis_Z=rowtotal(grd`i'_kis_*_Z) if TestedEL2016==1, missing
if(`i'==1) xtile KisLevel`i' = grd`i'_kis_Z if TestedEL2016==1,  nquantiles(10)
if(`i'==2) xtile KisLevel`i' = grd`i'_kis_Z if TestedEL2016==1,  nquantiles(9)
replace KisLevel`i'=0 if TestedEL2016==0 & Grade==`i'


pca grd`i'_his_*_Z
predict grd`i'_his_Z, score
*egen grd`i'_his_Z=rowtotal(grd`i'_his_*_Z) if TestedEL2016==1, missing
if(`i'==1) xtile MathLevel`i' = grd`i'_his_Z if TestedEL2016==1,  nquantiles(9)
if(`i'==2) xtile MathLevel`i' = grd`i'_his_Z if TestedEL2016==1,  nquantiles(10)
replace MathLevel`i'=0 if TestedEL2016==0 & Grade==`i'

}


saveold "CreatedData\ConsolidatedYr34\TWA_StudentLevelsBatches_2017.dta", replace

drop if Grade!=1
collapse (mean) grd1_*, by(SchoolID)

pca grd1_*, components(1)
predict Total, score
xtile TotalQ = Total, nquantiles(10) 
save "CreatedData\ConsolidatedYr34\TWA_SchoolLevelsBatches_2017.dta", replace



use "CreatedData\ConsolidatedYr34\2016_AllStudents_PassRates.dta", clear
merge 1:1 studentid using "CreatedData\ConsolidatedYr34\TWA_StudentLevelsBatches_2017.dta",keepus(MathLevel* KisLevel*)
drop _merge
merge m:1 SchoolID using "CreatedData\ConsolidatedYr34\TWA_SchoolLevelsBatches_2017.dta",keepus(TotalQ)
drop _merge
saveold "CreatedData\ConsolidatedYr34\2016_AllStudents_PassRates.dta", version(12) replace

preserve
keep DistrictID- TestedEL2016  *Grd1_Pass KisLevel1- MathLevel1 treatarm2 name
drop if Grade!=1
saveold "CreatedData\ConsolidatedYr34\2016_AllStudents_PassRates_Grd1.dta", version(12) replace
restore

preserve
keep DistrictID- TestedEL2016  *Grd2_Pass KisLevel2- MathLevel2 treatarm2 name
drop if Grade!=2
saveold "CreatedData\ConsolidatedYr34\2016_AllStudents_PassRates_Grd2.dta", version(12) replace
restore


preserve
keep DistrictID- TestedEL2016  *Grd3_Pass treatarm2 name
drop if Grade!=3
saveold "CreatedData\ConsolidatedYr34\2016_AllStudents_PassRates_Grd3.dta", version(12) replace
restore


use "CreatedData\ConsolidatedYr34\2016_AllStudents_PassRates_Grd1.dta", clear
drop if treatarm2!=2
preserve
collapse (mean) Kis_PichaLetter_Grd1_Pass Kis_Picha_Grd1_Pass Kis_Silabi_Grd1_Pass Kis_Maneno_Grd1_Pass Kis_Sentenci_Grd1_Pass Kis_Aya_Grd1_Pass , by(KisLevel1)
drop if KisLevel1==0
saveold "CreatedData/4 Intervention/TwaEL_2016/MeanPassRate_KisLevels_Grd1.dta", replace
restore
preserve
collapse (mean) Math_id_Grd1_Pass Math_uta_Grd1_Pass Math_bwa_Grd1_Pass Math_j_Grd1_Pass Math_t_Grd1_Pass Math_z_Grd1_Pass, by(MathLevel1)
drop if MathLevel1==0
saveold "CreatedData/4 Intervention/TwaEL_2016/MeanPassRate_MathLevels_Grd1.dta", replace
restore

use "CreatedData\ConsolidatedYr34\2016_AllStudents_PassRates_Grd2.dta", clear
drop if treatarm2!=2
preserve
collapse (mean) Kis_Picha_Grd2_Pass Kis_Silabi_Grd2_Pass Kis_Maneno_Grd2_Pass Kis_Sentenci_Grd2_Pass Kis_Aya_Grd2_Pass Kis_Comp_Grd2 , by(KisLevel2)
drop if KisLevel2==0
saveold "CreatedData/4 Intervention/TwaEL_2016/MeanPassRate_KisLevels_Grd2.dta", replace
restore
preserve
collapse (mean) Math_uta_Grd2_Pass Math_bwab_Grd2_Pass Math_bwa_Grd2_Pass Math_j_Grd2_Pass Math_t_Grd2_Pass Math_z_Grd2_Pass Math_g_Grd2_Pass, by(MathLevel2)
drop if MathLevel2==0
saveold "CreatedData/4 Intervention/TwaEL_2016/MeanPassRate_MathLevels_Grd2.dta", replace
restore



