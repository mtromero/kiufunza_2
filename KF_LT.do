

*******************************************************
*************BASE LINE 2013
********************************************************
use "$basein/1 Baseline/Student/Sample_noPII.dta", clear
drop _merge
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/SchoolLTStudy.dta"
drop if SchoolLTStudy==0
replace treatment2="Control" if treatment2==""
replace treatarm2=1 if treatarm2==.
drop _merge
compress


drop  Stream_ID StreamID totSampleStreams_ID streamSize_ID StudentID
*child did not answer the base test
drop if consentChild==.

gen student_present=0
replace student_present=1 if consentChild==1
rename student_present attendance
drop  consentChild result childAvailable timeEndTest timeStartTest


*Now if put the children in different levels depending on how far they went in the Test
gen KiswahiliLevel=.
replace KiswahiliLevel=4 if kiswahili_4==1 & kiswahili_4!=.
replace KiswahiliLevel=3 if kiswahili_3==1 & kiswahili_3!=. & KiswahiliLevel==.
replace KiswahiliLevel=2 if kiswahili_2>=6 & kiswahili_2!=. & KiswahiliLevel==.
replace KiswahiliLevel=1 if kiswahili_1>=4 & kiswahili_1!=. & KiswahiliLevel==.
replace KiswahiliLevel=0 if KiswahiliLevel==.

label define reading 0 "Nothing"  1 "Syllable" 2 "Words"       3 "Paragraph" 4 "Read"
label values KiswahiliLevel reading
label variable KiswahiliLevel "Kiswahili reading level"
	   
gen EnglishLevel=.
replace EnglishLevel=4 if   kiingereza_4==1 & kiingereza_4!=.
replace EnglishLevel=3 if   kiingereza_3==1 & kiingereza_3!=. & EnglishLevel==.
replace EnglishLevel=2 if   kiingereza_2>=6 & kiingereza_2!=. & EnglishLevel==.
replace EnglishLevel=1 if   kiingereza_1>=4 & kiingereza_1!=. & EnglishLevel==.
replace EnglishLevel=0 if EnglishLevel==.

label values EnglishLevel reading
label variable EnglishLevel "English reading level"
		   
gen MathLevel=.
replace MathLevel=6 if  hisabati_6>=4 & hisabati_6!=.
replace MathLevel=5 if  hisabati_5>=4 & hisabati_5!=. &  MathLevel==.
replace MathLevel=4 if  hisabati_4>=4 & hisabati_4!=. &  MathLevel==.
replace MathLevel=3 if  hisabati_3>=4 & hisabati_3!=. &  MathLevel==.
replace MathLevel=2 if  hisabati_2>=4 & hisabati_2!=. &  MathLevel==.
replace MathLevel=1 if  hisabati_1>=4 & hisabati_1!=. & MathLevel==. 
replace MathLevel=0 if  MathLevel==. 

label define math 0 "Nothing"  1 "Counting" 2 "Numbers"  3 "Values" 4 "Addition" 5 "Subtraction" 6 "Multiplication"
label values MathLevel  math
label variable MathLevel "Math level"


********************************8
******************************** THIS SECTION CALCULATES THE Z-SCORE

replace kiswahili_3=0 if kiswahili_3==2
replace kiswahili_4=0 if kiswahili_4==2
replace kiswahili_5=0 if kiswahili_5==2
replace kiswahili_6=0 if kiswahili_6==2
replace kiingereza_3=0 if kiingereza_3==2
replace kiingereza_4=0 if kiingereza_4==2
replace kiingereza_5=0 if kiingereza_5==2
replace kiingereza_6=0 if kiingereza_6==2
recode bonasi_1 bonasi_2 bonasi_3 (2=0)

*For grade 1 is the easiest as students started in the easier level and stopped when they couldnt go any further. Thus its "safe" to assume they would have score zero in the next sections
*Thus the first step is to replace the missing values for zeros (since they did not get to it, it must be because they couldn't answer easier stuff so we assume they score zero)
foreach var of varlist kiswahili_1 kiswahili_2 kiswahili_3 kiswahili_4 kiswahili_5 kiswahili_6  kiingereza_1 kiingereza_2 kiingereza_3 kiingereza_4 kiingereza_5 kiingereza_6 hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7 {
replace `var'=0 if `var'==. & GradeID==1 & attendance==1
}


*For grade  2 and 3 is the same in math... sooo... first we replace with zero the levels the student did not get to answer: 
foreach var of varlist hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7{
replace `var'=0 if `var'==. & GradeID==2 & attendance==1
}
foreach var of varlist hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7{
replace `var'=0 if `var'==. & GradeID==3 & attendance==1
}


*For grade  2 and 3 is harder in lenguages: It started in question 3, if the student go it, then he moved on, if he didn't it moved to question 2 and if he scored zero then it moved to question 1. 
*So first, if the student got it, then lets assume he could read syllabus and letters, i.e. that he could achieve perfect score.
replace kiingereza_1 =5 if kiingereza_3==1 & (GradeID==3 | GradeID==2) & kiingereza_1==. & attendance==1
replace kiingereza_2 =8 if kiingereza_3==1 & (GradeID==3 | GradeID==2) & kiingereza_2==. & attendance==1
replace kiswahili_1 =5 if kiswahili_3==1 & (GradeID==3 | GradeID==2) & kiswahili_1==. & attendance==1
replace kiswahili_2 =8 if kiswahili_3==1 & (GradeID==3 | GradeID==2) & kiswahili_2==. & attendance==1
*If the student did not get question 4 (after he got question 3), then assume he would have gotten zero in questions 5 and 6
replace kiingereza_5 =0 if kiingereza_3==1 & kiingereza_4==0 & (GradeID==3 | GradeID==2) & kiingereza_5==. & attendance==1
replace kiingereza_6 =0 if kiingereza_3==1 & kiingereza_4==0 & (GradeID==3 | GradeID==2) & kiingereza_6==. & attendance==1
replace kiswahili_5 =0 if kiswahili_3==1 & kiswahili_4==0 & (GradeID==3 | GradeID==2) & kiswahili_5==. & attendance==1
replace kiswahili_6 =0 if kiswahili_3==1 & kiswahili_4==0 & (GradeID==3 | GradeID==2) & kiswahili_6==. & attendance==1
*If the student did not get it, then lets assume he couldnt read the story or answer the questions i.e. that he could achieve perfect score. 
replace kiingereza_4 =0 if kiingereza_3==0 & (GradeID==3 | GradeID==2) & kiingereza_4==. & attendance==1
replace kiingereza_5 =0 if kiingereza_3==0 & (GradeID==3 | GradeID==2) & kiingereza_5==. & attendance==1
replace kiingereza_6 =0 if kiingereza_3==0 & (GradeID==3 | GradeID==2) & kiingereza_6==. & attendance==1
replace kiswahili_4 =0 if kiswahili_3==0 & (GradeID==3 | GradeID==2) & kiswahili_4==. & attendance==1
replace kiswahili_5 =0 if kiswahili_3==0 & (GradeID==3 | GradeID==2) & kiswahili_5==. & attendance==1
replace kiswahili_6 =0 if kiswahili_3==0 & (GradeID==3 | GradeID==2) & kiswahili_6==. & attendance==1
*If the student did not get it, and got over 0 in the word section, then we need to estimate the number of letters he would have gotten
* to do this we run an OLS regression using Grade1 data (they all answer these two questions) 
poisson kiswahili_1 kiswahili_2 i.Gender  Age c.Age#c.Age i.DistrictID if GradeID==1 & kiswahili_2>0 & attendance==1
*reg kiswahili_1 kiswahili_2 i.Gender  Age c.Age#c.Age i.DistrictID if GradeID==1 & kiswahili_2>0
predict input_kis1 if attendance==1, n 
*reg kiingereza_1 kiingereza_2 i.Gender  Age c.Age#c.Age i.DistrictID if GradeID==1 & kiswahili_2>0
poisson kiingereza_1 kiingereza_2 i.Gender  Age c.Age#c.Age i.DistrictID i.SchoolID if GradeID==1 & kiswahili_2>0 & attendance==1
predict input_kiin1 if attendance==1, n 

replace kiswahili_1=max(round(input_kis1),5) if kiswahili_1==. & kiswahili_2>0 & kiswahili_3==0 & (GradeID==3 | GradeID==2) & attendance==1
replace kiingereza_1=max(round(input_kiin1),5)  if kiingereza_1==. & kiingereza_2>0 & kiingereza_3==0 & (GradeID==3 | GradeID==2) & attendance==1
replace kiswahili_1=5 if kiswahili_1>5 & attendance==1
replace kiingereza_1=5 if kiingereza_1>5 & attendance==1
sum kiswahili_1- hisabati_7
sum kiswahili_1- hisabati_7 if GradeID==1
sum kiswahili_1- hisabati_7 if GradeID==2
sum kiswahili_1- hisabati_7 if GradeID==3



 
********************************
drop input_kis1 input_kiin1


*Now in order to calculate what the Z-score for the just passing is... I'm gonna create a series of variables that will be constant across students and will have the exact number needed to pass
gen kiswahili_1_pass=4 if GradeID==1 & attendance==1
gen kiswahili_2_pass=4 if GradeID==1 & attendance==1

gen kiingereza_1_pass=4 if GradeID==1 & attendance==1
gen kiingereza_2_pass=4 if GradeID==1 & attendance==1

gen hisabati_1_pass=4 if GradeID==1 & attendance==1
gen hisabati_2_pass=4 if GradeID==1 & attendance==1
gen hisabati_3_pass=4 if GradeID==1 & attendance==1
gen hisabati_4_pass=4 if GradeID==1 & attendance==1
gen hisabati_5_pass=4 if GradeID==1 & attendance==1


replace kiswahili_2_pass=4 if GradeID==2 & attendance==1
gen kiswahili_3_pass=1 if GradeID==2 & attendance==1

replace kiingereza_2_pass=4 if GradeID==2 & attendance==1
gen kiingereza_3_pass=1 if GradeID==2 & attendance==1


replace hisabati_3_pass=4 if GradeID==2 & attendance==1
replace hisabati_4_pass=4 if GradeID==2 & attendance==1
replace hisabati_5_pass=4 if GradeID==2 & attendance==1
gen hisabati_6_pass=4 if GradeID==2 & attendance==1


gen kiswahili_4_pass=1 if GradeID==3 & attendance==1
gen kiswahili_5_pass=1 if GradeID==3 & attendance==1
gen kiswahili_6_pass=1 if GradeID==3 & attendance==1

gen kiingereza_4_pass=1 if GradeID==3 & attendance==1
gen kiingereza_5_pass=1 if GradeID==3 & attendance==1
gen kiingereza_6_pass=1 if GradeID==3 & attendance==1


replace hisabati_4_pass=4 if GradeID==3 & attendance==1
replace hisabati_5_pass=4 if GradeID==3 & attendance==1
replace hisabati_6_pass=4 if GradeID==3 & attendance==1




*Now we can standarize for each question
foreach var of varlist bonasi_1 bonasi_2 bonasi_3 kiswahili_1 kiswahili_2 kiswahili_3 kiswahili_4 kiswahili_5 kiswahili_6  kiingereza_1 kiingereza_2 kiingereza_3 kiingereza_4 kiingereza_5 kiingereza_6 hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7 {
forvalues grade=1/3{
sum `var' if GradeID==`grade' & treatarm==4
capture gen Z_`var'=(`var'-r(mean))/r(sd) if GradeID==`grade' & attendance==1
capture replace Z_`var'=(`var'-r(mean))/r(sd) if GradeID==`grade' & attendance==1
capture replace Z_`var'=0 if r(sd)==0 & GradeID==`grade' & attendance==1
}
}


gen Z_kiswahili=.
gen Z_kiingereza=.
gen Z_hisabati=.
gen Z_bonasi=.
*****now we combine by subject
forvalues val=1/3{
	foreach subject in kiswahili kiingereza hisabati bonasi{
		pca Z_`subject'_* if GradeID==`val'
		predict Z_`subject'_temp, score
		replace Z_`subject'=  Z_`subject'_temp if GradeID==`val'
		drop Z_`subject'_temp
	}
}



***Now we standarize by grade subject
forvalues val=1/3{
	foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_bonasi{
		sum `var' if GradeID==`val' & treatarm==4
		replace `var'=(`var'-r(mean))/r(sd) if GradeID==`val' & attendance==1
	*if("`var'"!="Z_bonasi") replace `var'_pass=(`var'_pass-r(mean))/r(sd) if GradeID==`val' & attendance==1
	}
}

***Now we standarize by subject
foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_bonasi{
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
	*if("`var'"!="Z_bonasi") replace `var'_pass=(`var'_pass-r(mean))/r(sd)
}

***Now we create a few Z scores
pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score 
pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score 

foreach var of varlist Z_ScoreKisawMath Z_ScoreFocal {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}



compress

gen male=0 & attendance==1
replace male=1 if Gender==1 & attendance==1
compress
replace seenUwezoTests=0 if seenUwezoTests==2 & attendance==1
replace preSchoolYN=0 if preSchoolYN==2 & attendance==1

save "$base_out/1 Baseline/Student/Student_LT.dta", replace

rename * =_20131
rename usid_20131 usid
rename DistrictID_20131 DistrictID
rename treatment_20131 treatment
rename treatarm_20131 treatarm
rename SchoolID_20131 SchoolID
rename treatment2_20131 treatment2
rename treatarm2_20131 treatarm2

save "$base_out/LT/Student_20131_LT.dta", replace



*******************************************************
*************Endline 2013
**********************************************************

use "$basein/3 Endline/Student/ESTudent_noPII.dta", clear
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/SchoolLTStudy.dta"
drop if SchoolLTStudy==0
replace treatment2="Control" if treatment2==""
replace treatarm2=1 if treatarm2==.
drop _merge

gen grade=stdgrd
rename grade GradeID



*Now i create standarized test scores for each question
foreach var of varlist S1_kiswahili_1 S1_kiswahili_2 S1_kiswahili_3 S1_kiswahili_4  S1_kiingereza_1 S1_kiingereza_2 S1_kiingereza_3 S1_kiingereza_4 S1_hisabati_1 S1_hisabati_2 S1_hisabati_3 S1_hisabati_4 S1_hisabati_5 S1_sayansi_1 {
	sum `var' if GradeID==1 & treatarm==4
	gen Z_`var'=(`var'-r(mean))/r(sd)
}

foreach subject in kiswahili kiingereza hisabati {
	pca Z_S1_`subject'*
	predict Z_S1_`subject', score
}

gen Z_S1_sayansi=Z_S1_sayansi_1


foreach var of varlist Z_S1_kiswahili Z_S1_kiingereza Z_S1_hisabati  Z_S1_sayansi{
	sum `var' if GradeID==1 & treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}


replace S2_kiswahili_3=0 if S2_kiswahili_3==2
replace S2_kiingereza_3=0 if S2_kiingereza_3==2

foreach var of varlist S2_kiswahili_1 S2_kiswahili_2 S2_kiswahili_3 S2_kiswahili_4 S2_kiingereza_1 S2_kiingereza_2 S2_kiingereza_3 S2_kiingereza_4 S2_hisabati_1 S2_hisabati_2 S2_hisabati_3 S2_hisabati_4 S2_sayansi_1 {
	sum `var' if GradeID==2 & treatarm==4
	gen Z_`var'=(`var'-r(mean))/r(sd)
}

foreach subject in kiswahili kiingereza hisabati {
	pca Z_S2_`subject'*
	predict Z_S2_`subject', score
}
gen Z_S2_sayansi=Z_S2_sayansi_1

foreach var of varlist Z_S2_kiswahili Z_S2_kiingereza Z_S2_hisabati Z_S2_sayansi {
	sum `var' if GradeID==2 & treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}



replace S3_kiswahili_1=0 if S3_kiswahili_1==2
replace S3_kiingereza_1=0 if S3_kiingereza_1==2

foreach var of varlist S3_kiswahili_1 S3_kiswahili_2 S3_kiswahili_3 S3_kiingereza_1 S3_kiingereza_2 S3_kiingereza_3 S3_hisabati_1 S3_hisabati_2 S3_hisabati_3 S3_hisabati_4 S3_hisabati_5 S3_sayansi_1 {
	sum `var' if GradeID==3 & treatarm==4
	gen Z_`var'=(`var'-r(mean))/r(sd)
}


foreach subject in kiswahili kiingereza hisabati {
	pca Z_S3_`subject'*
	predict Z_S3_`subject', score
}
gen Z_S3_sayansi=Z_S3_sayansi_1




foreach var of varlist Z_S3_kiswahili Z_S3_kiingereza Z_S3_hisabati Z_S3_sayansi {
	sum `var' if GradeID==3 & treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}


gen IRT_kiswahili=.
gen IRT_kiingereza=.
gen IRT_hisabati=.
gen IRT_sayansi=.
drop S1_*_time
drop S2_*_time
drop S3_*_time

forvalues grd = 1/3{
	capture drop IRT_kiswahili_temp
	capture drop IRT_kiingereza_temp
	capture drop IRT_hisabati_temp
	capture drop IRT_sayansi_temp
	
	irt  (grm S`grd'_kiswahili_*) if stdgrd==`grd', intpoints(7) difficult  intmethod(ghermite) listwise
	predict IRT_kiswahili_temp if  e(sample)==1, latent 
	replace IRT_kiswahili=IRT_kiswahili_temp if IRT_kiswahili==.
	
	irt (grm S`grd'_kiingereza_*) if stdgrd==`grd', intpoints(7) difficult  intmethod(ghermite) listwise
	predict IRT_kiingereza_temp if e(sample)==1, latent 
	replace IRT_kiingereza=IRT_kiingereza_temp if IRT_kiingereza==.
	
	irt  (grm  S`grd'_hisabati_*) if stdgrd==`grd',   intpoints(7) difficult  intmethod(ghermite) listwise
	predict IRT_hisabati_temp if e(sample)==1, latent 
	replace IRT_hisabati=IRT_hisabati_temp if IRT_hisabati==.
	
	irt  (grm  S`grd'_sayansi_*) if stdgrd==`grd',   intpoints(7) difficult  intmethod(ghermite) listwise
	predict IRT_sayansi_temp if e(sample)==1, latent 
	replace IRT_sayansi=IRT_sayansi_temp if IRT_sayansi==.
}

drop IRT_kiswahili_temp IRT_kiingereza_temp IRT_hisabati_temp
forvalues val=1/3{
	foreach var of varlist IRT_kiswahili IRT_kiingereza IRT_hisabati IRT_sayansi {
		sum `var' if stdgrd==`val' & treatarm==4
		replace `var'=(`var'-r(mean))/r(sd) if GradeID==`val'
	}
}

foreach var of varlist IRT_kiswahili IRT_kiingereza IRT_hisabati IRT_sayansi {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}

pca IRT_kiswahili IRT_hisabati
predict IRT_ScoreKisawMath, score 
pca IRT_kiswahili IRT_kiingereza IRT_hisabati
predict IRT_ScoreFocal, score 

foreach var of varlist IRT_ScoreKisawMath IRT_ScoreFocal  {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}

*******************Total Z score
****************************
*****************************
egen Z_kiswahili=rowtotal(Z_S1_kiswahili Z_S2_kiswahili Z_S3_kiswahili), missing
egen Z_kiingereza=rowtotal(Z_S1_kiingereza Z_S2_kiingereza Z_S3_kiingereza), missing
egen Z_hisabati=rowtotal(Z_S1_hisabati Z_S2_hisabati Z_S3_hisabati), missing
egen Z_sayansi=rowtotal(Z_S1_sayansi Z_S2_sayansi Z_S3_sayansi), missing

foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}

pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score 
pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score 


foreach var of varlist Z_ScoreKisawMath Z_ScoreFocal {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}






gen unavailable=.
replace unavailable=1 if status==4 
replace unavailable=0 if status==1 | status==2 | status==3 
label variable 	unavailable "Student is unavailable"


gen change_school=.
replace change_school=1 if status==3 
replace change_school=0 if status==1 | status==2 | status==4
label variable 	change_school "Student changed schools"

*rename grade GradeID
compress

gen usid=subinstr(upid,"R1STU","",.)
drop if usid==""
gen date=dofc(time)
format date %dN/D/Y
gen WeekIntvTest=week(date)

drop timeStartTest S1_kiswahili_1- timeEndTest
drop Z_S1_kiswahili_1- Z_S3_sayansi


gen student_present=0
replace student_present=1 if consentChild==1
rename student_present attendance


rename * =_20132
rename usid_20132 usid
rename upid_20132 upid
rename SchoolID_20132 SchoolID
rename DistrictID_20132 DistrictID
rename treatment_20132 treatment
rename treatarm_20132 treatarm
rename treatment2_20132 treatment2
rename treatarm2_20132 treatarm2

save "$base_out/LT/Student_20132_LT.dta", replace



********************
********************
*************** BASELINE YEAR 2
********************
********************

use "$basein/6 Baseline 2014/Data/student/R4Student_noPII.dta",clear
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/SchoolLTStudy.dta"
drop if SchoolLTStudy==0
replace treatment2="Control" if treatment2==""
replace treatarm2=1 if treatarm2==.
drop _merge
gen GradeID=1
drop if consentChild==. | consentChild==2

gen male=0
replace male=1 if Gender==1
compress
replace seenUwezoTests=0 if seenUwezoTests==2
replace preSchoolYN=0 if preSchoolYN==2

gen student_present=0
replace student_present=1 if consentChild==1
rename student_present attendance

*Now in order to calculate what the Z-score for the just passing is... I'm gonna create a series of variables that will be constant across students and will have the exact number needed to pass
gen Akiswahili_1_pass=4
gen Akiswahili_2_pass=4 
gen Akiswahili_3_pass=4 


gen Akiingereza_1_pass=4 
gen Akiingereza_2_pass=4
gen Akiingereza_3_pass=4

gen Ahisabati_1_pass=4
gen Ahisabati_2_pass=4 
gen Ahisabati_3_pass=4
gen Ahisabati_4_pass=4 
gen Ahisabati_5_pass=4


*Now I create standarized test scores for each question
foreach var in kiswahili_1 kiswahili_2 kiswahili_3 kiswahili_4 kiingereza_1 kiingereza_2 kiingereza_3 kiingereza_4 hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 sayansi_1{
sum A`var' if treatarm==4
gen Z_`var'=(A`var'-r(mean))/r(sd)
*capture gen Z_`var'_pass=(A`var'_pass-r(mean))/r(sd)
}

foreach subject in kiswahili kiingereza hisabati{
	pca Z_`subject'_* 
	predict Z_`subject', score
}
	

egen Z_sayansi=rowtotal(Z_sayansi_1),missing


foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi {
	sum `var' if  treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
	capture replace `var'_pass=(`var'_pass-r(mean))/r(sd)
}



pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score 
pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score 


foreach var of varlist Z_ScoreKisawMath Z_ScoreFocal {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}


keep attendance treatment2 treatarm2 DistrictID SchoolID groupSize R4StudentID Age male preSchoolYN seenUwezoTests upid treatment treatarm GradeID Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi Z_ScoreKisawMath Z_ScoreFocal 

rename * =_20141
rename SchoolID_20141 SchoolID
rename DistrictID_20141 DistrictID
rename R4StudentID_20141 R4StudentID
rename upid_20141 upid
rename treatment_20141 treatment
rename treatarm_20141 treatarm
rename treatment2_20141 treatment2
rename treatarm2_20141 treatarm2

save "$base_out/LT/Student_20141_LT.dta", replace


*******************************************************
************* ENDLINE 2014
********************************************************


** WE NEED A BREAK TO CREATE HOME TESTED DATA
use "$basein/8 Endline 2014/Final Data/Household/R6HHData_noPII.dta",clear
drop if consentChild!=1
keep HHID consentChild- edtstm
merge 1:m HHID using "$basein/8 Endline 2014/ID Linkage/R6_Student_HH_ID_Linkage_noPII.dta",keepus(upid HHID)
drop if _merge!=3
rename upid upidst 
drop if upidst==""
drop HHID _merge
save "$base_out/8 Endline 2014/Household/HomeTests.dta", replace 
****
***
*Now lets loading the data
use "$basein/8 Endline 2014/Final Data/Student/R6Student_noPII.dta",clear
merge 1:1 upidst using "$base_out/8 Endline 2014/Household/HomeTests.dta", update
gen TestedAtHome=(_merge==4)
drop _merge
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/SchoolLTStudy.dta"
drop if SchoolLTStudy==0
replace treatment2="Control" if treatment2==""
replace treatarm2=1 if treatarm2==.
drop _merge


drop GradeID
gen GradeID=stdgrd

gen attgrade=1 if stdgrd==1 & atrgrd==.
replace attgrade=1 if atrgrd==1
replace attgrade=2 if stdgrd==2 & atrgrd==.
replace attgrade=2 if atrgrd==2
replace attgrade=3 if stdgrd==3 & atrgrd==.
replace attgrade=3 if atrgrd==3



gen student_present=0
replace student_present=1 if  consentChild==1
rename student_present attendance



gen date=dofc(time)
format date %dN/D/Y
gen WeekIntvTest=week(date)


foreach var of varlist kissyl1_1 -othsub1_8{
	recode `var' (.=0) if GradeID==1 & consentChild!=2 & consentChild!=.
}
foreach var of varlist kissyl2_1- othsub2_15{
	recode `var' (.=0) if GradeID==2 & consentChild!=2 & consentChild!=.
}
foreach var of varlist kissyl3_1- othsub3_24{
	recode `var' (.=0) if GradeID==3 & consentChild!=2 & consentChild!=.
}

foreach var of varlist kissyl1_1-othsub3_24 {
	recode `var' (2=0) (3=0) if consentChild!=2 & consentChild!=.
}



gen IRT_kiswahili=.
gen IRT_kiingereza=.
gen IRT_hisabati=.
gen IRT_sayansi=.

forvalues grd = 1/3{
	capture drop IRT_kiswahili_temp
	capture drop IRT_kiingereza_temp
	capture drop IRT_hisabati_temp
	capture drop IRT_sayansi_temp
	
	irt  (1pl kis*`grd'_*) if stdgrd==`grd', intpoints(50) difficult  intmethod(ghermite) listwise
	predict IRT_kiswahili_temp if e(sample)==1, latent 
	replace IRT_kiswahili=IRT_kiswahili_temp if IRT_kiswahili==.
	
	irt (1pl eng*`grd'_*) if stdgrd==`grd', intpoints(50) difficult  intmethod(ghermite) listwise
	predict IRT_kiingereza_temp if e(sample)==1, latent 
	replace IRT_kiingereza=IRT_kiingereza_temp if IRT_kiingereza==.
	
	irt  (1pl  his*`grd'_*) if stdgrd==`grd',   intpoints(10) difficult  intmethod(ghermite) listwise
	predict IRT_hisabati_temp if e(sample)==1, latent 
	replace IRT_hisabati=IRT_hisabati_temp if IRT_hisabati==.
	
	irt  (1pl  othsub*`grd'_*) if stdgrd==`grd',   intpoints(10) difficult  intmethod(ghermite) listwise
	predict IRT_sayansi_temp if e(sample)==1, latent 
	replace IRT_sayansi=IRT_sayansi_temp if IRT_sayansi==.
	
}
drop IRT_kiswahili_temp IRT_kiingereza_temp IRT_hisabati_temp IRT_sayansi_temp

forvalues val=1/3{
	foreach var of varlist IRT_kiswahili IRT_kiingereza IRT_hisabati IRT_sayansi {
		sum `var' if stdgrd==`val' & treatarm==4
		replace `var'=(`var'-r(mean))/r(sd) if GradeID==`val'
	}
}




foreach var of varlist IRT_kiswahili IRT_kiingereza IRT_hisabati IRT_sayansi {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}


pca IRT_kiswahili IRT_hisabati
predict IRT_ScoreKisawMath, score 
pca IRT_kiswahili IRT_kiingereza IRT_hisabati
predict IRT_ScoreFocal, score 

foreach var of varlist IRT_ScoreKisawMath IRT_ScoreFocal  {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}



gen Z_kiswahili=.
gen Z_kiingereza=.
gen Z_hisabati=.
gen Z_sayansi=.

forvalues grd = 1/3{
	pca kis*`grd'_* if stdgrd==`grd'
	predict Z_kiswahili_temp, score
	replace Z_kiswahili=Z_kiswahili_temp  if stdgrd==`grd'
	
	pca eng*`grd'_* if stdgrd==`grd'
	predict Z_kiingereza_temp, score
	replace Z_kiingereza=Z_kiingereza_temp  if stdgrd==`grd'
	
	pca his*`grd'_* if stdgrd==`grd'
	predict Z_hisabati_temp, score
	replace Z_hisabati=Z_hisabati_temp  if stdgrd==`grd'
	
	pca othsub*`grd'_* if stdgrd==`grd'
	predict Z_sayansi_temp, score
	replace Z_sayansi=Z_sayansi_temp  if stdgrd==`grd'
	
	drop Z_hisabati_temp  Z_kiingereza_temp Z_kiswahili_temp Z_sayansi_temp
}


***Now we standarize by grade subject
forvalues val=1/3{
	foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi {
		sum `var' if GradeID==`val' & treatarm==4
		replace `var'=(`var'-r(mean))/r(sd) if GradeID==`val'
	}
}


foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi {
	sum `var' if  treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}


pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score 
pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score 

/*

egen Z_ScoreKisawMath=rowtotal(Z_kiswahili Z_hisabati), missing
egen Z_ScoreFocal=rowtotal(Z_kiswahili Z_kiingereza Z_hisabati), missing
egen Z_ScoreTotal=rowtotal(Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi), missing
*/

foreach var of varlist Z_ScoreKisawMath Z_ScoreFocal  {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}




keep IRT* SchoolID R6StudentID upidst stdgrp stdage stdsex time revisit attstd atrgrd stdstrel attstd2 timeStartTest consentChild treatment treatarm TestedAtHome  GradeID attendance date WeekIntvTest Z_kiswahili* Z_kiingereza* Z_hisabati* Z_sayansi* Z_ScoreKisawMath* Z_ScoreFocal* attgrade stdgrd


rename * =_20142
rename upidst_20142 upid
rename SchoolID_20142 SchoolID
rename R6StudentID_20142 R6StudentID
compress

save "$base_out/LT/Student_20142_LT.dta", replace

*******************************************************
************* BASELINE 2015
********************************************************

use "$basein/9 Baseline 2015/Final Data/Student/R7Student_noPII.dta", clear
gen GradeID=1
drop if consentChild==.
keep SchoolID GradeID upid Age Gender preSchoolYN seenUwezoTests kissyl1_1 -othsub1_8 consentChild
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/SchoolLTStudy.dta"
drop if SchoolLTStudy==0
replace treatment2="Control" if treatment2==""
replace treatarm2=1 if treatarm2==.
drop _merge

gen student_present=0
replace student_present=1 if  consentChild==1
rename student_present attendance

foreach var of varlist kissyl1_1 -othsub1_8{
	recode `var' (.=0) if (GradeID==1 | GradeID==2)  & attendance==1
}

foreach var of varlist kissyl1_1-othsub1_8 {
	recode `var' (2=0) (3=0) if  attendance==1
}



irt (1pl kis*), intpoints(20) difficult
predict Z_kiswahili, latent 

irt (1pl eng*), intpoints(100) intmethod(ghermite) difficult
predict Z_kiingereza, latent 

irt (1pl his*), intpoints(20) difficult
predict Z_hisabati, latent 

irt (1pl oth*), intpoints(20) difficult
predict Z_sayansi, latent 


foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi {
replace `var'=. if attendance==0
sum `var' if  treatarm2==3
replace `var'=(`var'-r(mean))/r(sd)
}

pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score

pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score

pca Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi
predict Z_ScoreTotal, score

foreach var of varlist Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal {
sum `var' if treatarm2==3
replace `var'=(`var'-r(mean))/r(sd)
}
drop kis* eng* his* oth*

********************


rename * =_20151
foreach var in upid SchoolID DistrictID  treatment2 treatarm2 treatment treatarm{
rename `var'_20151 `var'
}



foreach var in  preSchoolYN_20151 seenUwezoTests_20151 Gender_20151{
recode `var' (2=0) 
}
compress 
save "$base_out/LT/Student_20151_LT.dta", replace



*******************************************************
*************END LINE 2015
********************************************************
use "$basein/11 Endline 2015/Final Data/Student Data/R8Student_nopii.dta", clear
gen date_edi=dofc(time)
format date_edi %td
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/SchoolLTStudy.dta"
drop if SchoolLTStudy==0
replace treatment2="Control" if treatment2==""
replace treatarm2=1 if treatarm2==.
drop _merge
merge m:1 SchoolID using "$base_out/ConsolidatedYr34/TWA_Date_T2.dta"
drop if _merge==2
drop _merge


gen attgrade=1 if stdgrd==1 & atrgrd==.
replace attgrade=1 if atrgrd==1
replace attgrade=2 if stdgrd==2 & atrgrd==.
replace attgrade=2 if atrgrd==2
replace attgrade=3 if stdgrd==3 & atrgrd==.
replace attgrade=3 if atrgrd==3



gen student_present=0
replace student_present=1 if  consentChild==1
rename student_present attendance


gen date=dofc(timeStartTest)
format date %dN/D/Y
gen WeekIntvTest=week(date)


foreach var of varlist kispicl1_1- othsub1_8{
	recode `var' (.=0) if GradeID==1 & attendance==1
}
foreach var of varlist kispic2_1- othsub2_15{
	recode `var' (.=0) if GradeID==2 & attendance==1
}
foreach var of varlist kissyl3_1 - othsub3_13{
	recode `var' (.=0) if GradeID==3 & attendance==1
}
foreach var of varlist kissyl4_1- hisdiv4_9{
	recode `var' (.=0) if GradeID==4 & attendance==1
}


foreach var of varlist kispicl1_1-hisdiv4_9 {
	recode `var' (2=0) (3=0) 
}

foreach i of varlist kispicl1_1-hisdiv4_9 {
local a : variable label `i'
local a: subinstr local a " correctly" ""
label var `i' "`a'"
}



egen hiscou_1=rowtotal(hiscou1_2 hiscou2_1 hiscou3_1 hiscou4_1),missing

egen hisidn_1=rowtotal(hisidn1_2 hisidn3_1 hisidn4_1),missing 
egen hisidn_2=rowtotal(hisidn1_1 hisidn2_1 ),missing
egen hisidn_3=rowtotal(hisidn1_3 hisidn2_2),missing

egen hisinq_1=rowtotal(hisinq1_2 hisinq2_1 hisinq3_1 ),missing
egen hisinq_2=rowtotal(hisinq1_4 hisinq3_2 hisinq4_1),missing

egen hisadd_1=rowtotal(hisadd3_2 hisadd4_2 hisadd1_3 hisadd2_2),missing
egen hisadd_2=rowtotal(hisadd2_6 hisadd3_6 hisadd4_4),missing 
egen hisadd_3=rowtotal(hisadd1_4 hisadd2_4 hisadd4_3),missing
egen hisadd_4=rowtotal(hisadd1_1 hisadd2_1 hisadd3_1 hisadd4_1),missing
egen hisadd_5=rowtotal(hisadd2_3 hisadd3_3),missing
egen hisadd_6=rowtotal(hisadd2_5 hisadd3_4),missing

egen hissub_1=rowtotal(hissub1_3 hissub2_2 hissub3_2 hissub4_2 ),missing
egen hissub_2=rowtotal(hissub1_4 hissub2_4),missing
egen hissub_3=rowtotal(hissub2_5 hissub3_4),missing
egen hissub_4=rowtotal(hissub2_3 hissub3_3 hissub4_3),missing 
egen hissub_5=rowtotal(hissub2_6 hissub3_6 hissub4_4),missing
egen hissub_6=rowtotal(hissub1_1 hissub2_1 hissub3_1 hissub4_1),missing

egen hismul_1=rowtotal(hismul1_1 hismul2_1 hismul3_1 hismul4_1),missing
egen hismul_2=rowtotal(hismul2_3 hismul3_2 hismul4_2),missing
egen hismul_3=rowtotal(hismul2_4 hismul3_4 hismul4_3),missing

egen hisdiv_1=rowtotal(hisdiv3_5 hisdiv4_4),missing
egen hisdiv_2=rowtotal(hisdiv3_3 hisdiv4_3),missing
egen hisdiv_3=rowtotal(hisdiv2_2 hisdiv3_2 hisdiv4_2),missing
egen hisdiv_4=rowtotal(hisdiv2_1 hisdiv3_1 hisdiv4_1),missing

egen othsub_1=rowtotal(othsub1_5 othsub2_8 othsub3_6),missing
egen othsub_2=rowtotal(othsub2_3 othsub3_1),missing
egen othsub_3=rowtotal(othsub1_7 othsub2_10 othsub3_8),missing
egen othsub_4=rowtotal(othsub2_2 othsub3_2),missing
egen othsub_5=rowtotal(othsub2_1 othsub3_3),missing
egen othsub_6=rowtotal(othsub1_6 othsub2_9 othsub3_7),missing
egen othsub_7=rowtotal(othsub2_4 othsub3_4),missing
egen othsub_8=rowtotal(othsub2_7 othsub3_5), missing
egen othsub_9=rowtotal(othsub2_11 othsub3_9), missing

egen kispic_1=rowtotal(kispic1_1 kispic2_1),missing
egen kissyl_1=rowtotal(kissyl1_1 kissyl2_1 kissyl3_1 kissyl4_1 ),missing
egen kissyl_2=rowtotal(kissyl1_2 kissyl2_2 kissyl3_2 ),missing
egen kissyl_3=rowtotal(kissyl1_3 kissyl2_3 kissyl3_3 kissyl4_2),missing

egen kiswrd_1=rowtotal(kiswrd1_3 kiswrd2_3 kiswrd3_3),missing
egen kiswrd_2=rowtotal(kiswrd1_6 kiswrd2_7 kiswrd3_8 kiswrd4_4 ),missing
egen kiswrd_3=rowtotal(kiswrd1_2 kiswrd2_2 kiswrd3_2 ),missing
egen kiswrd_4=rowtotal(kiswrd1_5 kiswrd2_5 kiswrd3_6 ),missing
egen kiswrd_5=rowtotal(kiswrd1_4 kiswrd2_4 kiswrd3_5 kiswrd4_3 ),missing
egen kiswrd_6=rowtotal(kiswrd1_1 kiswrd2_1 kiswrd3_1 kiswrd4_1 ),missing
egen kiswrd_7=rowtotal(kiswrd2_6 kiswrd3_7),missing
egen kiswrd_8=rowtotal(kiswrd3_4 kiswrd4_2 ),missing

egen kissen_1=rowtotal(kissen1_1 kissen2_1 kissen3_1 kissen4_1),missing
egen kissen_2=rowtotal(kissen1_2 kissen2_2 kissen3_2),missing
egen kissen_3=rowtotal(kissen1_3 kissen2_3 kissen3_3),missing
egen kissen_4=rowtotal(kissen1_4 kissen2_4 kissen3_5),missing
egen kissen_5=rowtotal(kissen1_5 kissen2_5 kissen3_6),missing
egen kissen_6=rowtotal(kissen1_6 kissen2_7 kissen3_8 kissen4_4),missing
egen kissen_7=rowtotal(kissen2_6 kissen3_7 kissen4_3),missing
egen kissen_8=rowtotal(kissen3_4 kissen4_2),missing

egen kiswri_1=rowtotal(kiswri2_3 kiswri3_3 kiswri4_3),missing 
egen kiswri_2=rowtotal(kiswri3_5 kiswri4_5),missing
egen kiswri_3=rowtotal(kiswri1_1 kiswri2_1 kiswri3_1 kiswri4_1 ),missing
egen kiswri_4=rowtotal(kiswri1_2 kiswri2_2 kiswri3_2 kiswri4_2 ),missing
egen kiswri_5=rowtotal(kiswri2_4 kiswri3_4 kiswri4_4),missing
egen kiswri_6=rowtotal(kiswri3_6 kiswri4_6),missing

egen kispar_1=rowtotal(kispar1_1 kispar2_1 kispar3_1 )
egen kispar_2=rowtotal(kispar1_2 kispar2_2 kispar3_2)
egen kispar_3=rowtotal(kispar2_3  kispar3_3)
egen kispar_4=rowtotal(kispar2_5 kispar3_4)

egen kissto_1=rowtotal(kissto3_1 kissto4_1) 
egen kiscom_1=rowtotal(kiscom3_2 kissto4_2)
egen kiscom_2=rowtotal(kiscom3_3 kissto4_3)

egen engpic_1=rowtotal(engpic1_2 engpic2_1 engpic3_1 ),missing
egen engpic_2=rowtotal(engpic1_3 engpic2_2 engpic3_2 engpic4_1),missing

egen engpicl_1=rowtotal(engpicl1_1 engpicl2_1 engpicl3_1 engpicl4_1 ),missing
egen engpicl_2=rowtotal(engpicl1_2 engpicl2_2 engpicl3_2),missing

egen englet_1=rowtotal(englet1_1 englet2_1 englet3_1 englet4_1 ),missing
egen englet_2=rowtotal(englet1_2 englet2_2 englet3_2 ),missing
egen englet_3=rowtotal(englet1_3 englet2_3 englet3_3 englet4_2),missing

egen engwrd_1=rowtotal(engwrd2_5 engwrd3_4 ),missing
egen engwrd_2=rowtotal(engwrd1_3 engwrd2_4 engwrd3_3 engwrd4_2),missing
egen engwrd_3=rowtotal(engwrd1_2 engwrd2_3 engwrd3_2 ),missing
egen engwrd_4=rowtotal(engwrd2_8 engwrd3_7 engwrd4_4 ),missing
egen engwrd_5=rowtotal(engwrd1_1 engwrd2_2 engwrd3_1 engwrd4_1),missing
egen engwrd_6=rowtotal(engwrd2_7 engwrd3_6 ),missing
egen engwrd_7=rowtotal(engwrd1_5 engwrd2_6 engwrd3_5 engwrd4_3),missing

egen engsen_1=rowtotal(engsen1_3 engsen2_3 engsen3_3),missing
egen engsen_2=rowtotal(engsen2_8 engsen3_6 engsen4_3),missing
egen engsen_3=rowtotal(engsen1_4 engsen2_4 engsen3_4),missing
egen engsen_4=rowtotal(engsen1_2 engsen2_2 engsen3_2 engsen4_1),missing
egen engsen_5=rowtotal(engsen1_1 engsen2_1 engsen3_1),missing
egen engsen_6=rowtotal(engsen2_6 engsen3_5 engsen4_2),missing

egen engwri_1=rowtotal(engwri2_4 engwri3_4 engwri4_4 ),missing
egen engwri_2=rowtotal(engwri1_1 engwri2_1 engwri3_1 engwri4_1 ),missing
egen engwri_3=rowtotal(engwri3_6 engwri4_6),missing
egen engwri_4=rowtotal(engwri2_3 engwri3_3 engwri4_3 ),missing
egen engwri_5=rowtotal(engwri3_5 engwri4_5 ),missing
egen engwri_6=rowtotal(engwri1_2 engwri2_2 engwri3_2 engwri4_2),missing 

egen engpar_1=rowtotal(engpar2_1 engpar3_1 engpar4_1)

egen engsto_1=rowtotal(engsto3_1 engsto4_1)
egen engcom_1=rowtotal(engcom3_1 engsto4_2)
egen engcom_2=rowtotal(engcom3_3 engsto4_3)

drop othsub2_7 othsub3_5 othsub2_11 othsub3_9
drop engsto3_1 engsto4_1 engcom3_1 engsto4_2 engcom3_3 engsto4_3
drop kissto3_1 kissto4_1 kiscom3_2 kissto4_2 kiscom3_3 kissto4_3 engsen3_1
drop kispar2_1 kispar3_1 kispar4_1 kispar1_2 kispar2_2 kispar3_2 kispar2_3  kispar3_3 kispar2_5 kispar3_4
drop engpar2_1 engpar3_1 engpar4_1 hiscou1_2 engsen2_1 engwri2_1 engwri2_3 engwri2_4 engwri2_2
drop kissen1_1 kissen2_1 kissen3_1 kissen4_1 kissen1_2 kissen2_2 kissen3_2 kissen1_3 kissen2_3 kissen3_3 kissen1_4 kissen2_4 kissen3_5 kissen1_5 kissen2_5 kissen3_6
drop kissen1_6 kissen2_7 kissen3_8 kissen4_4 kissen2_6 kissen3_7 kissen4_3 kissen3_4 kissen4_2
drop engsen1_1 engsen1_2 engsen1_3 engsen1_4 hisdiv2_2

drop hisadd3_2 hisadd4_2 hisadd1_3 hisadd2_2 hisadd2_6 hisadd3_6 hisadd4_4 hisadd1_4 hisadd2_4 hisadd4_3 hisadd1_1 hisadd2_1 hisadd3_1 hisadd4_1 
drop hisadd2_3 hisadd3_3 hisadd2_5 hisadd3_4 hiscou2_1 hiscou3_1 hiscou4_1 hisdiv3_5 hisdiv4_4 hisdiv3_3 hisdiv4_3 hisdiv3_2 hisdiv4_2 hisdiv2_1 hisdiv3_1 hisdiv4_1
drop othsub1_5 othsub2_8 othsub3_6 othsub2_3 othsub3_1 othsub1_7 othsub2_10 othsub3_8 othsub2_2 othsub3_2 othsub2_1 othsub3_3 othsub1_6 othsub2_9 othsub3_7
drop hisidn1_2 hisidn3_1 hisidn4_1  hisidn1_1 hisidn2_1 hisidn1_3 hisidn2_2 hisinq1_2 hisinq2_1 hisinq3_1  hisinq1_4 hisinq3_2 hisinq4_1
drop hismul1_1 hismul2_1 hismul3_1 hismul4_1 hismul2_3 hismul3_2 hismul4_2 hismul2_4 hismul3_4 hismul4_3 othsub2_4 othsub3_4 kissyl1_1 kissyl2_1 kissyl3_1 kissyl4_1 
drop kissyl1_2 kissyl2_2 kissyl3_2 kissyl1_3 kissyl2_3 kissyl3_3 kissyl4_2 engsen2_3 engsen3_3 engsen2_8 engsen3_6 engsen4_3 engsen2_4 engsen3_4 engsen2_2 engsen3_2 engsen4_1
drop engsen2_6 engsen3_5 engsen4_2 engwrd2_5 engwrd3_4 kiswrd1_3 kiswrd2_3 kiswrd3_3
drop kiswrd1_6 kiswrd2_7 kiswrd3_8 kiswrd4_4 kiswrd1_2 kiswrd2_2 kiswrd3_2 engwrd1_3 engwrd2_4 engwrd3_3 engwrd4_2 engwrd1_2 engwrd2_3 engwrd3_2 engwrd2_8 engwrd3_7 engwrd4_4 
drop kiswrd1_5 kiswrd2_5 kiswrd3_6 engwrd1_1 engwrd2_2 engwrd3_1 engwrd4_1 kiswrd1_4 kiswrd2_4 kiswrd3_5 kiswrd4_3 kiswrd1_1 kiswrd2_1 kiswrd3_1 kiswrd4_1 kiswrd2_6 kiswrd3_7
drop kiswrd3_4 kiswrd4_2 engwrd2_7 engwrd3_6 engwrd1_5 engwrd2_6 engwrd3_5 engwrd4_3 engpic1_2 engpic2_1 engpic3_1 engpicl1_1 engpicl2_1 engpicl3_1 engpicl4_1 kispic1_1 kispic2_1
drop engpic1_3 engpic2_2 engpic3_2 engpic4_1 engpicl1_2 engpicl2_2 engpicl3_2 hissub1_3 hissub2_2 hissub3_2 hissub4_2 hissub1_4 hissub2_4 hissub2_5 hissub3_4 hissub2_3 hissub3_3 hissub4_3 
drop hissub2_6 hissub3_6 hissub4_4 hissub1_1 hissub2_1 hissub3_1 hissub4_1 englet1_1 englet2_1 englet3_1 englet4_1  englet1_2 englet2_2 englet3_2 englet1_3 englet2_3 englet3_3 englet4_2
drop engwri3_4 engwri4_4 kiswri2_3 kiswri3_3 kiswri4_3 engwri1_1 engwri3_1 engwri4_1 engwri3_6 engwri4_6 kiswri3_5 kiswri4_5 kiswri1_1 kiswri2_1 kiswri3_1 kiswri4_1
drop kiswri1_2 kiswri2_2 kiswri3_2 kiswri4_2  kiswri2_4 kiswri3_4 kiswri4_4 engwri3_3 engwri4_3 engwri3_5 engwri4_5 engwri1_2 engwri3_2 engwri4_2 kiswri3_6 kiswri4_6


irt (1pl kis*), intpoints(20) difficult
predict Z_kiswahili, latent 

irt (1pl eng*), intpoints(20) difficult
predict Z_kiingereza, latent 

irt (1pl his*), intpoints(20) difficult
predict Z_hisabati, latent 

irt (1pl oth*), intpoints(20) difficult
predict Z_sayansi, latent 
replace Z_sayansi=. if GradeID==4



foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi {
replace `var'=. if attendance==0
sum `var' if  treatarm2==3
replace `var'=(`var'-r(mean))/r(sd)
}

pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score

pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score

pca Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi
predict Z_ScoreTotal, score

foreach var of varlist Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal {
sum `var' if treatarm2==3
replace `var'=(`var'-r(mean))/r(sd)
}
drop kis* eng* his* oth*

recode shoes socks dirty uniformdirty uniformtorn  ringworm bookshome sing missschool (2=0)
recode studentsfight (3=0) (4=0) (2=1)
replace sing=0 if singfreq==3 | singfreq==4
gen CloseToeShoe=(shoetype==2) if !missing(shoetype)



rename * =_20152
foreach var in upid SchoolID DistrictID treatment2 treatarm2 treatment treatarm{
rename `var'_20152 `var'
}



compress 
save "$base_out/LT/Student_20152_LT.dta", replace







*******************************************************
*************BASE LINE YR2
********************************************************
use "$basein/13 Baseline 2016/Final Data/Student Data_nopii/R9Student_nopii.dta", clear
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/SchoolLTStudy.dta"
drop if SchoolLTStudy==0
replace treatment2="Control" if treatment2==""
replace treatarm2=1 if treatarm2==.
drop _merge



gen student_present=0
replace student_present=1 if  consentChild==1
rename student_present attendance


gen date=dofc(timeStartTest)
format date %dN/D/Y
gen WeekIntvTest=week(date)


foreach var of varlist kispicl1_1- othsub1_8{
	recode `var' (.=0) if  attendance==1
}


foreach var of varlist kispicl1_1-othsub1_8 {
	recode `var' (2=0) (3=0) 
}

drop engwrd1_3 engsen1_2  engsen1_3 engwri1_2

irt (1pl kis*), intpoints(20) difficult
predict Z_kiswahili, latent 

irt (1pl eng*), intpoints(20) difficult
predict Z_kiingereza, latent 

irt (1pl his*), intpoints(20) difficult
predict Z_hisabati, latent 

irt (1pl oth*), intpoints(20) difficult
predict Z_sayansi, latent 




foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi {
replace `var'=. if attendance==0
sum `var' if  treatarm2==3
replace `var'=(`var'-r(mean))/r(sd)
}

pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score

pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score

pca Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi
predict Z_ScoreTotal, score

foreach var of varlist Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal {
sum `var' if treatarm2==3
replace `var'=(`var'-r(mean))/r(sd)
}
drop kis* eng* his* oth*



rename * =_20161
foreach var in upid SchoolID DistrictID treatment2 treatarm2 treatment treatarm{
rename `var'_20161 `var'
}

gen GradeID_20161=1

compress 
save "$base_out/LT/Student_20161_LT.dta", replace


*******************************************************
*************END LINE YR2
********************************************************


use "$basein/15 Endline 2016/Final Deliverable/Final Data/Student Data/R10Student_nopii.dta", clear
gen date_edi=dofc(time)
format date_edi %td
merge m:1 SchoolID using "$basein/TreatmentStatusYr3-4/SchoolLTStudy.dta"
drop if SchoolLTStudy==0
replace treatment2="Control" if treatment2==""
replace treatarm2=1 if treatarm2==.
drop _merge
merge m:1 SchoolID using "$base_out/ConsolidatedYr34/TWA_Date_T5.dta"
drop if _merge==2
drop _merge

gen attgrade=1 if stdgrd==1 & atrgrd==.
replace attgrade=1 if atrgrd==1
replace attgrade=2 if stdgrd==2 & atrgrd==.
replace attgrade=2 if atrgrd==2
replace attgrade=3 if stdgrd==3 & atrgrd==.
replace attgrade=3 if atrgrd==3



gen student_present=0
replace student_present=1 if  consentChild==1
rename student_present attendance


gen date=dofc(timeStartTest)
format date %dN/D/Y
gen WeekIntvTest=week(date)


drop kiswri4_7_er  kiswri4_8_er engwri4_8_er /*for now not sure what to do with these */
foreach var of varlist kispicl1_1- othsub1_8{
	recode `var' (.=0) if GradeID==1 & attendance==1
}
foreach var of varlist kispic2_1- othsub2_14{
	recode `var' (.=0) if GradeID==2 & attendance==1
}
foreach var of varlist kissyl3_1 - othsub3_17{
	recode `var' (.=0) if GradeID==3 & attendance==1
}
foreach var of varlist kissyl4_1- hisdiv4_9{
	recode `var' (.=0) if GradeID==4 & attendance==1
}


foreach var of varlist kispicl1_1-hisdiv4_9 {
	recode `var' (2=0) (3=0) (-99=0)
}
foreach i of varlist kispicl1_1-hisdiv4_9 {
local a : variable label `i'
local a: subinstr local a " correctly" ""
label var `i' "`a'"
}


egen engwri_1=rowtotal(engwri3_6 engwri4_6),missing
egen engwri_2=rowtotal(engwri2_4 engwri3_4 engwri4_4 ),missing
egen engwri_3=rowtotal(engwri3_5 engwri4_5),missing
egen engwri_4=rowtotal(engwri1_2 engwri2_2 engwri3_2 engwri4_2),missing
egen engwri_5=rowtotal(engwri2_3 engwri3_3 engwri4_3 ),missing
egen engwri_6=rowtotal(engwri1_1 engwri2_1 engwri3_1 engwri4_1),missing

egen kiswri_1=rowtotal(kiswri1_2 kiswri2_2 kiswri3_2 kiswri4_2 ),missing
egen kiswri_2=rowtotal(kiswri1_4 kiswri2_4 kiswri3_4 kiswri4_4),missing
egen kiswri_3=rowtotal(kiswri3_5 kiswri4_5 ),missing
egen kiswri_4=rowtotal(kiswri1_1 kiswri2_1 kiswri3_1 kiswri4_1 ),missing
egen kiswri_5=rowtotal(kiswri3_6 kiswri4_6 ),missing
egen kiswri_6=rowtotal(kiswri1_3 kiswri2_3 kiswri3_3 kiswri4_3),missing

egen englet_1=rowtotal(englet1_3 englet2_3 englet3_3 englet4_2 ),missing
egen englet_2=rowtotal(englet1_1 englet2_1 englet3_1 englet4_1 ),missing
egen englet_3=rowtotal(englet1_2 englet2_2 englet3_2),missing

egen hissub_1=rowtotal(hissub1_1 hissub2_1 hissub3_1 hissub4_1),missing
egen hissub_2=rowtotal(hissub2_6 hissub3_6 hissub4_4),missing
egen hissub_3=rowtotal(hissub2_3 hissub3_3 hissub4_3 ),missing
egen hissub_4=rowtotal(hissub2_5 hissub3_4 ),missing
egen hissub_5=rowtotal(hissub1_4 hissub2_4),missing
egen hissub_6=rowtotal(hissub1_3 hissub2_2 hissub3_2 hissub4_2),missing

egen engpicl_1=rowtotal(engpicl1_3 engpicl4_2),missing
egen engpicl_2=rowtotal(engpicl1_2 engpicl2_2 engpicl3_2 ),missing
egen engpicl_3=rowtotal(engpicl4_1 engpicl1_1 engpicl2_1 engpicl3_1),missing

egen engpic_1=rowtotal(engpic1_3 engpic2_2 engpic3_2 ),missing
egen engpic_2=rowtotal(engpic1_2 engpic2_1 engpic3_1),missing

egen kispic_1=rowtotal(kispic1_1 kispic2_1 ),missing

egen engwrd_1=rowtotal(engwrd1_3 engwrd2_4 engwrd3_3 engwrd4_2),missing
egen engwrd_2=rowtotal(engwrd1_4 engwrd2_5 engwrd3_4),missing
egen engwrd_3=rowtotal(engwrd2_8 engwrd3_7),missing
egen engwrd_4=rowtotal(engwrd2_2 engwrd3_1 engwrd4_1 ),missing
egen engwrd_5=rowtotal(engwrd1_1 engwrd2_1 ),missing
egen engwrd_6=rowtotal(engwrd2_7 engwrd3_6 engwrd4_4),missing 
egen engwrd_7=rowtotal(engwrd1_2 engwrd2_3 engwrd3_2),missing
egen engwrd_8=rowtotal(engwrd1_5 engwrd2_6 engwrd3_5 engwrd4_3),missing

egen kiswrd_1=rowtotal(kiswrd2_6 kiswrd3_7),missing
egen kiswrd_2=rowtotal(kiswrd1_2 kiswrd2_2 kiswrd3_2),missing
egen kiswrd_3=rowtotal(kiswrd1_5 kiswrd2_5 kiswrd3_6),missing
egen kiswrd_4=rowtotal(kiswrd1_1 kiswrd2_1 kiswrd3_1 kiswrd4_1 ),missing
egen kiswrd_5=rowtotal(kiswrd1_6 kiswrd2_7 kiswrd3_8 kiswrd4_4),missing
egen kiswrd_6=rowtotal(kiswrd1_3 kiswrd2_3 kiswrd3_3 kiswrd4_2 ),missing
egen kiswrd_7=rowtotal(kiswrd1_4 kiswrd2_4 kiswrd3_5 kiswrd4_3),missing


egen kissyl_1=rowtotal(kissyl1_3 kissyl2_3 kissyl3_3 kissyl4_2 ),missing
egen kissyl_2=rowtotal(kissyl1_1 kissyl2_1 kissyl3_1 kissyl4_1 ),missing
egen kissyl_3=rowtotal(kissyl3_2 kissyl1_2 kissyl2_2),missing

egen hismul_1=rowtotal(hismul2_3 hismul3_2 hismul4_2 ),missing
egen hismul_2=rowtotal(hismul2_4 hismul3_4 hismul4_3),missing
egen hismul_3=rowtotal(hismul1_1 hismul2_1 hismul3_1 hismul4_1 ),missing

egen hisinq_1=rowtotal(hisinq1_4 hisinq3_2 hisinq4_1 ),missing
egen hisinq_2=rowtotal(hisinq1_2 hisinq2_1 hisinq3_1),missing

egen hisidn_1=rowtotal(hisidn1_3 hisidn2_2 ),missing
egen hisidn_2=rowtotal(hisidn1_1 hisidn2_1 ),missing
egen hisidn_3=rowtotal(hisidn1_2 hisidn3_1 hisidn4_1),missing

*egen othsub_1=rowtotal(othsub1_4 othsub1_8),missing
egen othsub_2=rowtotal(othsub1_6 othsub2_9 othsub3_9 ),missing
egen othsub_3=rowtotal(othsub1_3 othsub2_3 othsub3_3 ),missing
egen othsub_4=rowtotal(othsub1_7 othsub2_10 othsub3_10),missing
egen othsub_5=rowtotal(othsub1_5 othsub2_8 othsub3_8 ),missing
egen othsub_6=rowtotal(othsub1_2 othsub2_2 othsub3_2 ),missing
egen othsub_7=rowtotal(othsub1_1 othsub2_1 othsub3_1),missing
egen othsub_8=rowtotal(othsub2_35 othsub3_35),missing
egen othsub_9=rowtotal(othsub3_14 othsub2_14),missing
egen othsub_10=rowtotal(othsub2_34 othsub3_34),missing
egen othsub_11=rowtotal(othsub33_4 othsub10_4),missing
egen othsub_12=rowtotal(othsub2_7 othsub3_7),missing



egen hisdiv_1=rowtotal(hisdiv2_2 hisdiv3_2 hisdiv4_2),missing
egen hisdiv_2=rowtotal(hisdiv2_1 hisdiv3_1 hisdiv4_1),missing
egen hisdiv_3=rowtotal(hisdiv3_5 hisdiv4_4 ),missing
egen hisdiv_4=rowtotal(hisdiv3_3 hisdiv4_3),missing

egen hiscou_1=rowtotal(hiscou2_1 hiscou4_1 hiscou1_2 hiscou3_1),missing


egen hisadd_1=rowtotal(hisadd2_5 hisadd3_4),missing
egen hisadd_2=rowtotal(hisadd2_1 hisadd3_1 hisadd4_1 hisadd1_1),missing
egen hisadd_3=rowtotal(hisadd2_3 hisadd3_3),missing
egen hisadd_4=rowtotal(hisadd1_4 hisadd2_4 hisadd4_3),missing
egen hisadd_5=rowtotal(hisadd2_6 hisadd3_6 hisadd4_4 ),missing
egen hisadd_6=rowtotal(hisadd1_3 hisadd2_2 hisadd3_2 hisadd4_2),missing


egen engsen_1=rowtotal(engsen1_1 engsen2_1 engsen3_1 ),missing
egen engsen_2=rowtotal(engsen1_2 engsen2_2 engsen3_2  engsen4_1),missing
egen engsen_3=rowtotal(engsen1_3 engsen2_3 engsen3_3),missing
egen engsen_4=rowtotal(engsen1_4 engsen2_4 engsen3_4 ),missing
egen engsen_5=rowtotal(engsen2_6 engsen3_5 engsen4_2 ),missing
egen engsen_6=rowtotal(engsen2_8 engsen3_6 engsen4_3 ),missing




egen kissen_1=rowtotal(kissen1_1 kissen2_1 kissen3_1 kissen4_1),missing
egen kissen_2=rowtotal(kissen1_2 kissen2_2 kissen3_2),missing
egen kissen_3=rowtotal(kissen1_3 kissen2_3 kissen3_3 kissen4_2),missing
egen kissen_4=rowtotal(kissen1_4 kissen2_4 kissen3_5),missing
egen kissen_5=rowtotal(kissen1_5 kissen2_5 kissen3_6 kissen4_3),missing
egen kissen_6=rowtotal(kissen1_6 kissen2_7 kissen3_8 kissen4_4 ),missing
egen kissen_7=rowtotal(kissen2_6 kissen3_7),missing

egen kispar_1=rowtotal(kispar2_1 kispar3_1 kispar4_1),missing
egen kispar_2=rowtotal(kispar2_2 kispar3_2 kispar4_2),missing
egen kispar_3=rowtotal(kispar3_3 kispar4_3), missing

egen engpar_1=rowtotal(engpar2_1 engpar3_1 engpar4_1), missing

egen kissto_1=rowtotal(kissto3_1 kissto4_1), missing
egen kissto_2=rowtotal(kissto3_2 kissto4_2), missing
egen kissto_3=rowtotal(kissto3_3 kissto4_3), missing
egen kissto_4=rowtotal(kissto3_4 kissto4_4), missing

egen engsto_1=rowtotal(engsto3_1 engsto4_1), missing
egen engsto_2=rowtotal(engcom3_1 engsto4_2), missing
egen engsto_3=rowtotal(engcom3_2 engsto4_3), missing
egen engsto_4=rowtotal(engcom3_3 engsto4_9), missing



  

drop engsto3_1 engsto4_1 engsto4_2 engsto4_3 engsto4_9 engcom3_1 engcom3_2 engcom3_3
drop kissto3_1 kissto4_1 kissto3_2 kissto4_2 kissto3_3 kissto4_3 kissto3_4 kissto4_4
drop othsub2_7 othsub3_7 kispar3_3 kispar4_3
drop engwri2_1 engwri2_2 engwri2_3 engwri2_4 engpar2_1 engpar3_1 engpar4_1
drop kispar2_1 kispar3_1 kispar4_1 kispar2_2 kispar3_2 kispar4_2
drop engsen1_1 engsen1_2 engsen1_3 engsen1_4 
drop kissen1_1 kissen2_1 kissen3_1 kissen4_1 kissen1_2 kissen2_2 kissen3_2 kissen1_3 kissen2_3 kissen3_3 kissen4_2 kissen1_4 kissen2_4 kissen3_5
drop kissen1_5 kissen2_5 kissen3_6 kissen4_3 kissen1_6 kissen2_7 kissen3_8 kissen4_4  kissen2_6 kissen3_7
drop engwri3_6 engwri4_6 engwri3_4 engwri4_4 kiswri1_2 kiswri2_2 kiswri3_2 kiswri4_2 kiswri1_4 kiswri2_4 kiswri3_4 kiswri4_4 kiswri3_5 kiswri4_5 kiswri1_1 kiswri2_1 kiswri3_1 kiswri4_1 
drop engwri3_5 engwri4_5 kiswri3_6 kiswri4_6 engwri1_2 engwri3_2 engwri4_2 kiswri1_3 kiswri2_3 kiswri3_3 kiswri4_3 engwri3_3 engwri4_3 engwri1_1 engwri3_1 engwri4_1
drop englet1_3 englet2_3 englet3_3 englet4_2 englet1_1 englet2_1 englet3_1 englet4_1 englet1_2 englet2_2 englet3_2 hissub1_1 hissub2_1 hissub3_1 hissub4_1 hissub2_6 hissub3_6 hissub4_4
drop hissub2_3 hissub3_3 hissub4_3 hissub2_5 hissub3_4 hissub1_4 hissub2_4 hissub1_3 hissub2_2 hissub3_2 hissub4_2 engpic1_3 engpic2_2 engpic3_2 engpicl1_3 engpicl4_2
drop engpicl1_2 engpicl2_2 engpicl3_2 kispic1_1 kispic2_1 engpic1_2 engpic2_1 engpic3_1 engpicl4_1 engpicl1_1 engpicl2_1 engpicl3_1 kiswrd2_6 kiswrd3_7 engwrd1_3 engwrd2_4 engwrd3_3 engwrd4_2
drop engwrd1_4 engwrd2_5 engwrd3_4 kiswrd1_2 kiswrd2_2 kiswrd3_2 engwrd2_8 engwrd3_7 kiswrd1_5 kiswrd2_5 kiswrd3_6 engwrd2_2 engwrd3_1 engwrd4_1 kiswrd1_1 kiswrd2_1 kiswrd3_1 kiswrd4_1 
drop kiswrd1_6 kiswrd2_7 kiswrd3_8 kiswrd4_4 engwrd1_1 engwrd2_1 kiswrd1_3 kiswrd2_3 kiswrd3_3 kiswrd4_2 kiswrd1_4 kiswrd2_4 kiswrd3_5 kiswrd4_3 engwrd2_7 engwrd3_6 engwrd4_4 
drop engwrd1_2 engwrd2_3 engwrd3_2 engwrd1_5 engwrd2_6 engwrd3_5 engwrd4_3 kissyl1_3 kissyl2_3 kissyl3_3 kissyl4_2 kissyl1_1 kissyl2_1 kissyl3_1 kissyl4_1 kissyl3_2 kissyl1_2 kissyl2_2
drop hismul2_3 hismul3_2 hismul4_2 hismul2_4 hismul3_4 hismul4_3 hismul1_1 hismul2_1 hismul3_1 hismul4_1 othsub1_4 othsub1_8 hisinq1_4 hisinq3_2 hisinq4_1 hisinq1_2 hisinq2_1 hisinq3_1
drop hisidn1_3 hisidn2_2 hisidn1_1 hisidn2_1 hisidn1_2 hisidn3_1 hisidn4_1 othsub1_6 othsub2_9 othsub3_9 othsub1_3 othsub2_3 othsub3_3 othsub1_7 othsub2_10 othsub3_10 othsub1_5 othsub2_8 othsub3_8 
drop othsub1_2 othsub2_2 othsub3_2 othsub1_1 othsub2_1 othsub3_1 hisdiv2_2 hisdiv3_2 hisdiv4_2 hisdiv2_1 hisdiv3_1 hisdiv4_1 hisdiv3_5 hisdiv4_4 hisdiv3_3 hisdiv4_3 hiscou2_1 hiscou4_1 hiscou1_2 hiscou3_1
drop othsub2_35 othsub3_35 othsub3_14 othsub2_14 othsub2_34 othsub3_34 othsub33_4 othsub10_4 hisadd2_5 hisadd3_4 hisadd2_1 hisadd3_1 hisadd4_1 hisadd1_1 hisadd2_3 hisadd3_3
drop hisadd1_4 hisadd2_4 hisadd4_3 hisadd2_6 hisadd3_6 hisadd4_4 hisadd1_3 hisadd2_2 hisadd3_2 hisadd4_2

drop engsen2_3 engsen3_3 engsen3_6 engsen4_3 engsen2_8 engsen3_2 engsen4_1 engsen2_2 engsen3_4 engsen2_4 
drop engsen3_5 engsen4_2 engsen2_6 engsen3_1 engsen2_1 



irt (1pl kis*), intpoints(4) difficult
predict Z_kiswahili, latent 

irt (1pl eng*), intpoints(20) difficult
predict Z_kiingereza, latent 

irt  (1pl hisadd* hissub* hisidn* hisinq* hismul* hiscou* hisdiv*), intpoints(20) difficult
predict Z_hisabati, latent 


irt (1pl oth*), intpoints(20) difficult
predict Z_sayansi, latent 
replace Z_sayansi=. if GradeID==4




foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi {
replace `var'=. if attendance==0
sum `var' if  treatarm2==3
replace `var'=(`var'-r(mean))/r(sd)
}



pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score

pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score

pca Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi
predict Z_ScoreTotal, score

foreach var of varlist Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal {
sum `var' if treatarm2==3
replace `var'=(`var'-r(mean))/r(sd)
}
drop kis* eng* his* oth*


rename upidst upid
rename * =_20162
foreach var in upid SchoolID DistrictID treatment2 treatarm2 treatment treatarm{
rename `var'_20162 `var'
}



compress 
save "$base_out/LT/Student_20162_LT.dta", replace




exit

************ MERGE**********


use "$base_out/LT/Student_20131_LT.dta", clear
merge 1:1 usid using "$base_out/LT/Student_20132_LT.dta", update 
drop _merge
merge 1:1 upid using "$base_out/LT/Student_20141_LT.dta", update 
drop _merge
merge 1:1 upid using "$base_out/LT/Student_20142_LT.dta", update 
drop _merge
merge 1:1 upid using "$base_out/LT/Student_20151_LT.dta", update 
drop _merge
merge 1:1 upid using "$base_out/LT/Student_20152_LT.dta", update 
drop _merge
merge 1:1 upid using "$base_out/LT/Student_20161_LT.dta", update 
drop _merge
merge 1:1 upid using "$base_out/LT/Student_20162_LT.dta", update 
drop _merge

gen COD=(treatarm2==3)

save "$base_out/LT/Student_LT.dta", replace

gen Z_kiswahili_BL=.
gen Z_kiingereza_BL=.
gen Z_hisabati_BL=.

foreach subject in Z_kiswahili Z_kiingereza Z_hisabati{
	replace `subject'_BL=`subject'_20131 if !missing(`subject'_20131) & missing(`subject'_BL)
	replace `subject'_BL=`subject'_20141 if !missing(`subject'_20141) & missing(`subject'_BL)
	replace `subject'_BL=`subject'_20151 if !missing(`subject'_20151) & missing(`subject'_BL)
	replace `subject'_BL=`subject'_20161 if !missing(`subject'_20161) & missing(`subject'_BL)
}

foreach year in  20132  20142  20152  20162{
	label var Z_kiswahili_`year' "Swahili"
	label var Z_kiingereza_`year' "English"
	label var Z_hisabati_`year' "Math"
}

eststo clear
foreach year in  20132  20142  20152  20162{
	foreach subject in Z_kiswahili  Z_hisabati Z_kiingereza{
		eststo: reghdfe `subject'_`year' COD stdage_`year' i.stdsex_`year'  if GradeID_`year'<=3,  vce(cluster SchoolID) ab(i.DistrictID i.GradeID_`year' i.GradeID_`year'#c.*_BL )
	}
}

esttab  using "$base_out/LT/RegLT.tex", se ar2 booktabs label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Year 1" "Year 2" "Year 3" "Year 4", pattern(1 0 0  1 0 0 1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
keep(COD ) stats(N , labels("N. of obs." )) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")



esttab  using "$base_out/LT/RegLT.csv", se ar2  label b(a2) se(a2) nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Year 1" "Year 2" "Year 3" "Year 4", pattern(1 0 0  1 0 0 1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
keep(COD ) stats(N , labels("N. of obs." )) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

gen Cohorte=.
replace Cohorte=1 if Z_kiswahili_20131!=.
replace Cohorte=2 if Z_kiswahili_20141!=.
replace Cohorte=3 if Z_kiswahili_20151!=.
replace Cohorte=4 if Z_kiswahili_20161!=.

eststo clear
foreach subject in Z_kiswahili  Z_hisabati Z_kiingereza{
	reghdfe `subject'_BL COD i.Cohorte,  vce(cluster SchoolID) ab(i.DistrictID )
}
