*****************************
******* mauricio's code *****
*****************************

use "C:\Users\Mauricio\Downloads\R8_STUDENT_summary_1130.dta" , clear
merge m:1 SchoolID using "C:\Users\Mauricio\Box Sync\01_KiuFunza\RawData\TreatmentStatusYr3-4\RandomizeStatus.dta", keepus(SchoolID)
drop if _merge==2
gen KF_II=(_merge==3)
gen KF_I_LT=(_merge==1)
drop _merge
drop if STU_attstd==5


gen Tested=1 if STU_result==1
replace Tested=0 if STU_result!=1

tab treatarm2, gen(TD)
reg Tested TD* if TestedR6R7==1 & KF_II==1 & GradeID!=4, vce(cluster SchoolID) nocons
test (_b[TD1] == _b[TD2]== _b[TD3])
reg Tested TD* if TestedR6R7==1 & KF_II==1 & GradeID!=4,  nocons
test (_b[TD1] == _b[TD2]== _b[TD3])

forvalues i=1/10{
reg Tested TD* if TestedR6R7==1 & DistrictID==`i' & KF_II==1 & GradeID!=4, nocons
test (_b[TD1] == _b[TD2]== _b[TD3])
}


*****************************
******* JM's code *****
*****************************

use "C:\Users\Mauricio\Downloads\R8_STUDENT_summary_1130.dta" , clear
merge m:1 SchoolID using "C:\Users\Mauricio\Box Sync\01_KiuFunza\RawData\TreatmentStatusYr3-4\RandomizeStatus.dta", keepus(SchoolID)
drop if _merge==2
gen KF_II=(_merge==3)
gen KF_I_LT=(_merge==1)
drop _merge
tab treatarm2, gen(TD)

*dropping children who have passed away from the sample
drop if STU_attstd==5

gen TestedR8=1 if STU_result==1
replace TestedR8=0 if STU_result!=1

**Overall Testing Rates**
sum TestedR8
di "****************"
di "KFII Schools Only"
di "*****************"
sum TestedR8 if KF_II==1
sum TestedR8 if GradeID==1 & KF_II==1
sum TestedR8 if GradeID==2 & KF_II==1
sum TestedR8 if GradeID==3 & KF_II==1
sum TestedR8 if GradeID==4 & KF_II==1

sum TestedR8 if KF_II==1 & treatarm2==1
sum TestedR8 if KF_II==1 & treatarm2==2
sum TestedR8 if KF_II==1 & treatarm2==3

di "****************"
di "KFII Schools Only, Only R6/R7 Tested Students"
di "*****************"
sum TestedR8 if KF_II==1 & TestedR6R7==1
sum TestedR8 if GradeID==1 & KF_II==1 & TestedR6R7==1
sum TestedR8 if GradeID==2 & KF_II==1 & TestedR6R7==1
sum TestedR8 if GradeID==3 & KF_II==1 & TestedR6R7==1
sum TestedR8 if GradeID==4 & KF_II==1 & TestedR6R7==1

sum TestedR8 if KF_II==1 & treatarm2==1 & TestedR6R7==1
sum TestedR8 if KF_II==1 & treatarm2==2 & TestedR6R7==1
sum TestedR8 if KF_II==1 & treatarm2==3 & TestedR6R7==1

di "****************"
di "KFII Schools Only, Only R6/R7 Tested Students, Only Focal Grades"
di "*****************"
sum TestedR8 if KF_II==1 & TestedR6R7==1 & ( GradeID!=4)
sum TestedR8 if KF_II==1 & TestedR6R7==1 & ( GradeID!=4) & treatarm2==1
sum TestedR8 if KF_II==1 & TestedR6R7==1 & ( GradeID!=4) & treatarm2==2
sum TestedR8 if KF_II==1 & TestedR6R7==1 & ( GradeID!=4) & treatarm2==3

*Overall by district (by grade and by treatment group -- KFII only)*
levelsof DistrictID, local(dist_test)
foreach district of local dist_test{
di "********************************************"
di "This is district `district' in KFII"
di "********************************************"
	sum TestedR8 if DistrictID==`district' & KF_II==1
	di "****Grade1****"
	sum TestedR8 if DistrictID==`district' & GradeID==1 & KF_II==1
	di "****Grade2****"
	sum TestedR8 if DistrictID==`district' & GradeID==2 & KF_II==1
	di "****Grade3****"
	sum TestedR8 if DistrictID==`district' & GradeID==3 & KF_II==1
	di "****Grade4****"
	sum TestedR8 if DistrictID==`district' & GradeID==4 & KF_II==1
}


**only KFII schools in focal grades if tested R6R7**
capture drop TD*
tab treatarm2, gen(TD)
regress TestedR8 TD* if KF_II==1 & (GradeID==1 | GradeID==2 | GradeID==3) & TestedR6R7, nocons vce(cluster SchoolID)
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD1 TD3, equal
test (_b[TD1] == _b[TD2] == _b[TD3])
drop TD1 TD2 TD3

**only KFII schools in focal grades if tested R6R7**
capture drop TD*
tab treatarm2, gen(TD)
regress TestedR8 TD* if KF_II==1 & (GradeID==1 | GradeID==2 | GradeID==3) & TestedR6R7, nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD1 TD3, equal
test (_b[TD1] == _b[TD2] == _b[TD3])
drop TD1 TD2 TD3
