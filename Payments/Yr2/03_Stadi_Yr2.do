capture log close
log using "CreatedData/4 Intervention/TwaEL_2016/StadiYr1.smcl", replace

**********************************
**********	STADI **************
**********************************


**********************************
**********	GRADE 1 **************
**********************************


use "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_StadiandControl_CLEAN.dta", clear
drop  gender set start startampm volname end endampm
replace stream = lower(stream)
rename schoolid SchoolID
rename studentid studentID
merge m:1 SchoolID using "RawData/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2)
drop if _merge==2
drop _merge
merge m:1 SchoolID using "CommScripts/Kigoma Sample/KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update
drop if _merge==2
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440

*136 is a split school
replace treatarm2=1 if SchoolID==136
replace treatment2="Levels" if SchoolID==136
replace SchoolID=1136 if SchoolID==1104 & stream=="b"
replace treatarm2=1 if SchoolID==1136
replace treatment2="Levels" if SchoolID==1136

drop if treatment2=="Control"
drop if treatment2=="Gains"
drop if treatment2==""

ds, has(type string)


foreach var of varlist `r(varlist)'{
replace `var'="" if `var'=="--blank--" | `var'=="X"
}



destring , replace
drop if grade!=1


foreach var of varlist SchoolID grade stream{
	qui tabmiss `var'
	display "`r(sum)' missing in variable `var'"
	display "They are dropped"
	drop if	missing(`var')
}





foreach skill in si ma se{
	qui tabmiss grd1_kis_a_`skill'_1
	display "Skills `skill' in Swahili/Qs1 has `r(sum)' missing"
	qui tabmiss grd1_kis_a_`skill'_1
	display "Skills `skill' in Swahili/Qs2 has `r(sum)' missing"
}
/*
foreach skill in l w se{
	qui tabmiss grd1_eng_a_`skill'_1
	display "Skills `skill' in English/Qs1 has `r(sum)' missing"
	qui tabmiss grd1_eng_a_`skill'_1
	display "Skills `skill' in English/Qs2 has `r(sum)' missing"
}
*/
foreach skill in id uta bwa j t{
	qui tabmiss grd1_his_a_`skill'_1
	display "Skills `skill' in Math/Qs1 has `r(sum)' missing"
	qui tabmiss grd1_his_a_`skill'_1
	display "Skills `skill' in Math/Qs2 has `r(sum)' missing"
}

*Some missings are "weird" but I'm gonna assume students just didin't answer the question, i.e. it was wrong... but I do some more cleaning below
drop grd2* grd3*
foreach var of varlist grd*{
replace `var'=0 if `var'==.
}

capture drop grd1_kis_b* grd1_kis_c* grd1_his_b* grd1_his_c*



*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in si ma se{
	 forvalues i=3/6{
		replace grd1_kis_a_`skill'_`i'=0 if grd1_kis_a_`skill'_1==0 & grd1_kis_a_`skill'_2==0
	}
}
/*
foreach skill in l w se{
	forvalues i=3/6{
		replace grd1_eng_a_`skill'_`i'=0 if grd1_eng_a_`skill'_1==0 & grd1_eng_a_`skill'_2==0
	}
}
*/
foreach skill in id uta bwa j t{
	replace grd1_his_a_`skill'_3=0 if grd1_his_a_`skill'_1==0 & grd1_his_a_`skill'_2==0
} 

*Calculcate number of questions per skill
egen Kis_Silabi=rowtotal(grd1_kis_a_si_1 grd1_kis_a_si_2 grd1_kis_a_si_3 grd1_kis_a_si_4 grd1_kis_a_si_5 grd1_kis_a_si_6)
egen Kis_Maneno=rowtotal(grd1_kis_a_ma_1 grd1_kis_a_ma_2 grd1_kis_a_ma_3 grd1_kis_a_ma_4 grd1_kis_a_ma_5 grd1_kis_a_ma_6)
egen Kis_Sentenci=rowtotal(grd1_kis_a_se_1 grd1_kis_a_se_2 grd1_kis_a_se_3 grd1_kis_a_se_4 grd1_kis_a_se_5 grd1_kis_a_se_6)
/*
egen Eng_Letter=rowtotal(grd1_eng_a_l_1 grd1_eng_a_l_2 grd1_eng_a_l_3 grd1_eng_a_l_4 grd1_eng_a_l_5 grd1_eng_a_l_6)
egen Eng_Word=rowtotal(grd1_eng_a_w_1 grd1_eng_a_w_2 grd1_eng_a_w_3 grd1_eng_a_w_4 grd1_eng_a_w_5 grd1_eng_a_w_6)
egen Eng_Sentences=rowtotal(grd1_eng_a_se_1 grd1_eng_a_se_2 grd1_eng_a_se_3 grd1_eng_a_se_4 grd1_eng_a_se_5 grd1_eng_a_se_6)
*/
egen Math_id=rowtotal(grd1_his_a_id_1 grd1_his_a_id_2 grd1_his_a_id_3)
egen Math_uta=rowtotal(grd1_his_a_uta_1 grd1_his_a_uta_2 grd1_his_a_uta_3)
egen Math_bwa=rowtotal(grd1_his_a_bwa_1 grd1_his_a_bwa_2 grd1_his_a_bwa_3)
egen Math_j=rowtotal(grd1_his_a_j_1 grd1_his_a_j_2 grd1_his_a_j_3)
egen Math_t=rowtotal(grd1_his_a_t_1 grd1_his_a_t_2 grd1_his_a_t_3)

*generate passing dummies. Have to be careful since stata thinks missing is infinity
foreach var of varlist Kis_Silabi Kis_Maneno   {
	gen `var'_Pass=`var'>=4 & !missing(`var')
}

foreach var of varlist grd1_kis_a_se_*   {
	gen `var'_Pass=(`var'>=2) & !missing(`var')
}
foreach var of varlist Kis_Sentenci    {
	gen `var'_Pass=((grd1_kis_a_se_1_Pass+ grd1_kis_a_se_2_Pass+ grd1_kis_a_se_3_Pass +grd1_kis_a_se_4_Pass+ grd1_kis_a_se_5_Pass+ grd1_kis_a_se_6_Pass)>=4)
}
drop grd1_kis_a_se_1_Pass-grd1_kis_a_se_6_Pass


foreach var of varlist Math_id Math_uta Math_bwa Math_j Math_t {
	gen `var'_Pass=`var'>=2 & !missing(`var')
}

preserve
keep name- studentID  *Pass
saveold "CreatedData/4 Intervention/TwaEL_2016/Stadi_Grd1Pass_Students.dta",replace version(12)
restore

*Calculate number of passing per school/grade/stream
collapse (count) Sudents=districtid (sum) *_Pass , by(SchoolID grade stream)

*Calculcate total number of passes
foreach var in Kis_Silabi Kis_Maneno Kis_Sentenci  Math_id Math_uta Math_bwa Math_j Math_t{
	bys grade: egen `var'_Total=total(`var'_Pass)
}

/*Rember the payment here is the budget divided by the total number of pases, so you get the payment per pass, and the multiplity that by the number of passes */
foreach var in Kis_Silabi Kis_Maneno Kis_Sentenci Math_id Math_uta Math_bwa Math_j Math_t{
	gen Payment_PerPass`var'=scalar(Budget`var'_Grd1_Stadi)/`var'_Total
	gen Payment_`var'=scalar(Budget`var'_Grd1_Stadi)*`var'_Pass/`var'_Total
	gen Payment_Posible`var'=Payment_PerPass`var'*Sudents
}
*Now calculcate the total payment per subject
egen Payment_Total_Kis=rowtotal(Payment_Kis*)
*egen Payment_Total_Eng=rowtotal(Payment_Eng*)
egen Payment_Total_Math=rowtotal(Payment_Math*)

egen Payment_Posible_Kis=rowtotal(Payment_PosibleKis*)
*egen Payment_Posible_Eng=rowtotal(Payment_PosibleEng*)
egen Payment_Posible_Math=rowtotal(Payment_PosibleMath*)


preserve
keep *PerPass*
collapse (mean) *PerPass*
saveold "CreatedData/4 Intervention/TwaEL_2016/PerPassGrd1.dta",replace version(12)
restore
preserve
keep Sudents SchoolID grade stream Payment_Total_Kis Payment_Total_Math *_Pass
gen DistrictID=string(SchoolID,"%04.0f")
replace DistrictID=substr(DistrictID,1,2)
destring DistrictID, replace
saveold "CreatedData/4 Intervention/TwaEL_2016/Stadi_Grd1_Payments.dta",replace version(12)
restore


keep SchoolID Sudents Payment_Posible* Payment_Total*  Payment_Kis* Payment_Math*
collapse (sum) Sudents Payment_Posible* Payment_Total* Payment_Kis*  Payment_Math*, by(SchoolID)
gen DistrictID=string(SchoolID,"%04.0f")
replace DistrictID=substr(DistrictID,1,2)
destring DistrictID, replace
foreach subject in Kis Math{
gen PercentageEarned_`subject'=Payment_Total_`subject'/Payment_Posible_`subject'
}
foreach subject in Kis_Silabi Kis_Maneno Kis_Sentenci Math_id Math_uta Math_bwa Math_j Math_t{
gen PercentageEarned_`subject'=Payment_`subject'/Payment_Posible`subject'
}
foreach subject in Kis Math Kis_Silabi Kis_Maneno Kis_Sentenci Math_id Math_uta Math_bwa Math_j Math_t{
egen MaxNationalProp_`subject'=max(PercentageEarned_`subject')
bys DistrictID: egen MaxDistrictProp_`subject'=max(PercentageEarned_`subject')
}

foreach subject in Sudents{
egen National_`subject'=total(Sudents)
bys DistrictID: egen District_`subject'=total(Sudents)
}
levelsof SchoolID
foreach school in `r(levels)' {
foreach subject in Kis Math{
estpost tabstat Sudents Payment_Posible_`subject' Payment_Total_`subject' PercentageEarned_`subject' MaxDistrictProp_`subject' if SchoolID==`school', statistics(mean) columns(variables)
esttab using "CreatedData/4 Intervention/TwaEL_2016/pdf_School/latexTables/`school'_`subject'_grd1.csv", cells("Sudents Payment_Posible_`subject' Payment_Total_`subject' PercentageEarned_`subject' MaxDistrictProp_`subject'") replace fragment noeqlines nolines nogaps  nomti nonum nodep nonotes noobs tab
}
}


saveold "CreatedData/4 Intervention/TwaEL_2016/ReportsInfoGrd1__grd1.dta",replace version(12)


**********************************
**********	GRADE 2 **************
**********************************


use "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_StadiandControl_CLEAN.dta", clear
drop  gender set start startampm volname end endampm
replace stream = lower(stream)
rename schoolid SchoolID
rename studentid studentID
gen studentid=studentID
merge m:1 SchoolID using "RawData/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2)
drop if _merge==2
drop _merge
merge m:1 SchoolID using "CommScripts/Kigoma Sample/KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update
drop if _merge==2
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440
*136 is a split school
replace treatarm2=1 if SchoolID==136
replace treatment2="Levels" if SchoolID==136
replace SchoolID=1136 if SchoolID==1104 & stream=="b"
replace treatarm2=1 if SchoolID==1136
replace treatment2="Levels" if SchoolID==1136

drop if treatment2=="Control"
drop if treatment2=="Gains"
drop if treatment2==""



ds, has(type string)
foreach var of varlist `r(varlist)'{
replace `var'="" if `var'=="--blank--" | `var'=="X"
}

destring , replace
drop if grade!=2

foreach var of varlist SchoolID grade stream{
	qui tabmiss `var'
	display "`r(sum)' missing in variable `var'"
	display "They are dropped"
	drop if	missing(`var')
}



foreach skill in ma se{
	qui tabmiss grd2_kis_a_`skill'_1
	display "Skills `skill' in Swahili/Qs1 has `r(sum)' missing"
	qui tabmiss grd2_kis_a_`skill'_1
	display "Skills `skill' in Swahili/Qs2 has `r(sum)' missing"
}

/*
foreach skill in w se{
	qui tabmiss grd2_eng_a_`skill'_1
	display "Skills `skill' in English/Qs1 has `r(sum)' missing"
	qui tabmiss grd2_eng_a_`skill'_1
	display "Skills `skill' in English/Qs2 has `r(sum)' missing"
}
*/

foreach skill in  bwa j t{
	qui tabmiss grd2_his_a_`skill'_1
	display "Skills `skill' in Math/Qs1 has `r(sum)' missing"
	qui tabmiss grd2_his_a_`skill'_1
	display "Skills `skill' in Math/Qs2 has `r(sum)' missing"
}



drop grd1* grd3*
foreach var of varlist grd*{
	replace `var'=0 if `var'==.
}

capture drop grd2_kis_b* grd2_kis_c* grd2_his_b* grd2_his_c*

*capture rename grd1_eng_a_w_5 grd2_eng_a_se_5

replace grd2_kis_a_h=0 if grd2_kis_a_h==.

*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd2_kis_a_m_1=0 if grd2_kis_a_h==0
replace grd2_kis_a_m_2=0 if grd2_kis_a_h==0

*First data cleaning proces... if first two in skills are wrong, then don't ask the rest!
foreach skill in ma se{
	 forvalues i=3/6{
		replace grd2_kis_a_`skill'_`i'=0 if grd2_kis_a_`skill'_1==0 & grd2_kis_a_`skill'_2==0
	}
}
/*
foreach skill in w se{
	forvalues i=3/6{
		replace grd2_eng_a_`skill'_`i'=0 if grd2_eng_a_`skill'_1==0 & grd2_eng_a_`skill'_2==0
	}
}
*/
foreach skill in  bwa j t{
	replace grd2_his_a_`skill'_3=0 if grd2_his_a_`skill'_1==0 & grd2_his_a_`skill'_2==0
} 



egen Kis_Maneno=rowtotal(grd2_kis_a_ma_1 grd2_kis_a_ma_2 grd2_kis_a_ma_3 grd2_kis_a_ma_4 grd2_kis_a_ma_5 grd2_kis_a_ma_6)
egen Kis_Sentenci=rowtotal(grd2_kis_a_se_1 grd2_kis_a_se_2 grd2_kis_a_se_3 grd2_kis_a_se_4 grd2_kis_a_se_5 grd2_kis_a_se_6)
gen Kis_Aya=grd2_kis_a_h
egen Kis_Comp=rowtotal(grd2_kis_a_m_1 grd2_kis_a_m_2)
 
/*
egen Eng_Word=rowtotal(grd2_eng_a_w_1 grd2_eng_a_w_2 grd2_eng_a_w_3 grd2_eng_a_w_4 grd2_eng_a_w_5 grd2_eng_a_w_6)
egen Eng_Sentences=rowtotal(grd2_eng_a_se_1 grd2_eng_a_se_2 grd2_eng_a_se_3 grd2_eng_a_se_4 grd2_eng_a_se_5 grd2_eng_a_se_6)
gen Eng_Paragraph=grd2_eng_a_p
*/
egen Math_bwa=rowtotal(grd2_his_a_bwa_1 grd2_his_a_bwa_2 grd2_his_a_bwa_3)
egen Math_j=rowtotal(grd2_his_a_j_1 grd2_his_a_j_2 grd2_his_a_j_3)
egen Math_t=rowtotal(grd2_his_a_t_1 grd2_his_a_t_2 grd2_his_a_t_3)
*egen Math_z=rowtotal(grd2_his_a_z_1 grd2_his_a_z_2 grd2_his_a_z_3)

foreach var of varlist  Kis_Maneno{
	gen `var'_Pass=`var'>=4 & !missing(`var')
}
foreach var of varlist grd2_kis_a_se_*   {
	gen `var'_Pass=(`var'>=2) & !missing(`var')
}
foreach var of varlist Kis_Sentenci    {
	gen `var'_Pass=((grd2_kis_a_se_1_Pass+ grd2_kis_a_se_2_Pass+ grd2_kis_a_se_3_Pass +grd2_kis_a_se_4_Pass+ grd2_kis_a_se_5_Pass+ grd2_kis_a_se_6_Pass)>=4)
}
drop grd2_kis_a_se_1_Pass-grd2_kis_a_se_6_Pass

foreach var of varlist  Math_bwa Math_j Math_t {
	gen `var'_Pass=`var'>=2 & !missing(`var')
}

foreach var of varlist  Kis_Aya{
	gen `var'_Pass=`var'==3 & !missing(`var')
}

foreach var of varlist  Kis_Comp{
	gen `var'_Pass=`var'==2 & !missing(`var')
}


preserve
keep name- studentID *Pass
saveold "CreatedData/4 Intervention/TwaEL_2016/Stadi_Grd2Pass_Students.dta",replace version(12)
restore

collapse (count) Sudents=districtid (sum) *_Pass , by(SchoolID grade stream)

foreach var in Kis_Maneno Kis_Sentenci Kis_Aya Kis_Comp  Math_bwa Math_j Math_t {
	bys grade: egen `var'_Total=total(`var'_Pass)
}

/*Rember the payment here is the budget divided by the total number of pases, so you get the payment per pass, and the multiplity that by the number of passes */
foreach var in Kis_Maneno Kis_Sentenci Kis_Aya Kis_Comp  Math_bwa Math_j Math_t {
	gen Payment_PerPass`var'=scalar(Budget`var'_Grd2_Stadi)/`var'_Total
	gen Payment_`var'=scalar(Budget`var'_Grd2_Stadi)*`var'_Pass/`var'_Total
	gen Payment_Posible`var'=Payment_PerPass`var'*Sudents
}


egen Payment_Total_Kis=rowtotal(Payment_Kis*)
*egen Payment_Total_Eng=rowtotal(Payment_Eng*)
egen Payment_Total_Math=rowtotal(Payment_Math*)


egen Payment_Posible_Kis=rowtotal(Payment_PosibleKis*)
*egen Payment_Posible_Eng=rowtotal(Payment_PosibleEng*)
egen Payment_Posible_Math=rowtotal(Payment_PosibleMath*)



preserve
keep *PerPass*
collapse (mean) *PerPass*
saveold "CreatedData/4 Intervention/TwaEL_2016/PerPassGrd2.dta",replace version(12)
restore

preserve
keep Sudents SchoolID grade stream Payment_Total_Kis Payment_Total_Math *_Pass 
gen DistrictID=string(SchoolID,"%04.0f")
replace DistrictID=substr(DistrictID,1,2)
destring DistrictID, replace
saveold "CreatedData/4 Intervention/TwaEL_2016/Stadi_Grd2_Payments.dta",replace version(12)
restore

keep SchoolID Sudents Payment_Posible* Payment_Total*  Payment_Kis* Payment_Math*
collapse (sum) Sudents Payment_Posible* Payment_Total* Payment_Kis* Payment_Math*, by(SchoolID)
gen DistrictID=string(SchoolID,"%04.0f")
replace DistrictID=substr(DistrictID,1,2)
destring DistrictID, replace
foreach subject in Kis Math{
gen PercentageEarned_`subject'=Payment_Total_`subject'/Payment_Posible_`subject'
}
foreach subject in  Kis_Maneno Kis_Sentenci Kis_Aya Math_bwa Math_j Math_t{
gen PercentageEarned_`subject'=Payment_`subject'/Payment_Posible`subject'
}
foreach subject in Kis Math Kis_Maneno Kis_Sentenci Kis_Aya Math_bwa Math_j Math_t {
egen MaxNationalProp_`subject'=max(PercentageEarned_`subject')
bys DistrictID: egen MaxDistrictProp_`subject'=max(PercentageEarned_`subject')
}
foreach subject in Sudents{
egen National_`subject'=total(Sudents)
bys DistrictID: egen District_`subject'=total(Sudents)
}

levelsof SchoolID
foreach school in `r(levels)' {
foreach subject in Kis Math{
estpost tabstat Sudents Payment_Posible_`subject' Payment_Total_`subject' PercentageEarned_`subject' MaxDistrictProp_`subject' if SchoolID==`school', statistics(mean) columns(variables)
esttab using "CreatedData/4 Intervention/TwaEL_2016/pdf_School/latexTables/`school'_`subject'_grd2.csv", cells("Sudents Payment_Posible_`subject' Payment_Total_`subject' PercentageEarned_`subject' MaxDistrictProp_`subject'") replace fragment noeqlines nolines nogaps  nomti nonum nodep nonotes noobs tab
}
}

saveold "CreatedData/4 Intervention/TwaEL_2016/ReportsInfoGrd2.dta",replace version(12)


**********************************
**********	GRADE 3 **************
**********************************

use "CreatedData/4 Intervention/TwaEL_2016/EL2016_Student_FinalRun_StadiandControl_CLEAN.dta", clear
drop  gender set start startampm volname end endampm
replace stream = lower(stream)
rename schoolid SchoolID
rename studentid studentID
gen studentid=studentID
merge m:1 SchoolID using "RawData/TreatmentStatusYr3-4/RandomizeStatus.dta", keepus(treatment2 treatarm2)
drop if _merge==2
drop _merge
merge m:1 SchoolID using "CommScripts/Kigoma Sample/KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update
drop if _merge==2
drop _merge
*This is the school that splitted from 724
replace treatarm2=2 if SchoolID==736
replace treatment2="Gains" if SchoolID==736
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440
*136 is a split school
replace treatarm2=1 if SchoolID==136
replace treatment2="Levels" if SchoolID==136
replace SchoolID=1136 if SchoolID==1104 & stream=="b"
replace treatarm2=1 if SchoolID==1136
replace treatment2="Levels" if SchoolID==1136

drop if treatment2=="Control"
drop if treatment2=="Gains"
drop if treatment2==""

ds, has(type string)



foreach var of varlist `r(varlist)'{
replace `var'="" if `var'=="--blank--" | `var'=="X"
}

destring , replace
drop if grade!=3

foreach var of varlist SchoolID grade stream{
	qui tabmiss `var'
	display "`r(sum)' missing in variable `var'"
	display "They are dropped"
	drop if	missing(`var')
}


qui tabmiss grd3_kis_a_h
display "Skills Reading in Swahili/Qs1 has `r(sum)' missing"
qui tabmiss grd3_eng_a_s
display "Skills English in Swahili/Qs1 has `r(sum)' missing"



foreach skill in  j t z g{
	qui tabmiss grd3_his_a_`skill'_1
	display "Skills `skill' in Math/Qs1 has `r(sum)' missing"
	qui tabmiss grd3_his_a_`skill'_1
	display "Skills `skill' in Math/Qs2 has `r(sum)' missing"
}



foreach var of varlist grd*{
	replace `var'=0 if `var'==.
}

replace grd3_kis_a_h=0 if grd3_kis_a_h==.

*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_kis_a_m_1=0 if grd3_kis_a_h==0
replace grd3_kis_a_m_2=0 if grd3_kis_a_h==0


*First data cleaning proces... if story is wrong, dont ask comprehensions
replace grd3_eng_a_c_1=0 if grd3_eng_a_s==0
replace grd3_eng_a_c_2=0 if grd3_eng_a_s==0

*First data cleaning process for math is as before...
foreach skill in  j t z g{
	replace grd3_his_a_`skill'_3=0 if grd3_his_a_`skill'_1==0 & grd3_his_a_`skill'_2==0

} 


capture drop grd3_kis_b* grd3_kis_c* grd3_eng_b* grd3_eng_c* grd3_his_b* grd3_his_c*


gen Kis_Story=grd3_kis_a_h
egen Kis_Comp=rowtotal(grd3_kis_a_m_1 grd3_kis_a_m_2)
     
gen Eng_Story=grd3_eng_a_s
egen Eng_Comp=rowtotal(grd3_eng_a_c_1 grd3_eng_a_c_2)
 

egen Math_j=rowtotal(grd3_his_a_j_1 grd3_his_a_j_2 grd3_his_a_j_3)
egen Math_t=rowtotal(grd3_his_a_t_1 grd3_his_a_t_2 grd3_his_a_t_3)
egen Math_z=rowtotal(grd3_his_a_z_1 grd3_his_a_z_2 grd3_his_a_z_3)
egen Math_g=rowtotal(grd3_his_a_g_1 grd3_his_a_g_2 grd3_his_a_g_3)


foreach var of varlist  Math_j Math_t Math_z Math_g{
	gen `var'_Pass=`var'>=2 & !missing(`var')
}

foreach var of varlist  Kis_Story Eng_Story{
	gen `var'_Pass=`var'==3 & !missing(`var')
}

foreach var of varlist  Kis_Comp Eng_Comp {
	gen `var'_Pass=`var'==2 & !missing(`var')
}


preserve
keep name- studentID  *Pass
saveold "CreatedData/4 Intervention/TwaEL_2016/Stadi_Grd3Pass_Students.dta",replace version(12)
restore

collapse  (count) Sudents=districtid  (sum) *_Pass , by(SchoolID grade stream)

foreach var in Kis_Story Kis_Comp Eng_Story Eng_Comp Math_j Math_t Math_z Math_g{
	bys grade: egen `var'_Total=total(`var'_Pass) 
}

/*Rember the payment here is the budget divided by the total number of pases, so you get the payment per pass, and the multiplity that by the number of passes */
foreach var in Kis_Story Kis_Comp Eng_Story Eng_Comp Math_j Math_t Math_z Math_g{
	gen Payment_PerPass`var'=scalar(Budget`var'_Grd3_Stadi)/`var'_Total
	gen Payment_`var'=scalar(Budget`var'_Grd3_Stadi)*`var'_Pass/`var'_Total
	gen Payment_Posible`var'=Payment_PerPass`var'*Sudents
}

egen Payment_Total_Kis=rowtotal(Payment_Kis*)
egen Payment_Total_Eng=rowtotal(Payment_Eng*)
egen Payment_Total_Math=rowtotal(Payment_Math*)


egen Payment_Posible_Kis=rowtotal(Payment_PosibleKis*)
egen Payment_Posible_Eng=rowtotal(Payment_PosibleEng*)
egen Payment_Posible_Math=rowtotal(Payment_PosibleMath*)




preserve
keep *PerPass*
collapse (mean) *PerPass*
saveold "CreatedData/4 Intervention/TwaEL_2016/PerPassGrd3.dta",replace version(12)
restore
preserve
keep Sudents SchoolID grade stream Payment_Total_Kis Payment_Total_Eng Payment_Total_Math *_Pass 
gen DistrictID=string(SchoolID,"%04.0f")
replace DistrictID=substr(DistrictID,1,2)
destring DistrictID, replace
saveold "CreatedData/4 Intervention/TwaEL_2016/Stadi_Grd3_Payments.dta",replace version(12)
restore

keep SchoolID Sudents Payment_Posible* Payment_Total*  Payment_Kis* Payment_Eng* Payment_Math*
collapse (sum) Sudents Payment_Posible* Payment_Total* Payment_Kis* Payment_Eng* Payment_Math*, by(SchoolID)
gen DistrictID=string(SchoolID,"%04.0f")
replace DistrictID=substr(DistrictID,1,2)
destring DistrictID, replace
foreach subject in Kis Eng Math{
gen PercentageEarned_`subject'=Payment_Total_`subject'/Payment_Posible_`subject'
}
foreach subject in  Kis_Story Kis_Comp Eng_Story Eng_Comp Math_j Math_t Math_z Math_g{
gen PercentageEarned_`subject'=Payment_`subject'/Payment_Posible`subject'
}
foreach subject in Kis Eng Math Kis_Story Kis_Comp Eng_Story Eng_Comp Math_j Math_t Math_z Math_g{
egen MaxNationalProp_`subject'=max(PercentageEarned_`subject')
bys DistrictID: egen MaxDistrictProp_`subject'=max(PercentageEarned_`subject')
}
foreach subject in Sudents{
egen National_`subject'=total(Sudents)
bys DistrictID: egen District_`subject'=total(Sudents)
}
levelsof SchoolID
foreach school in `r(levels)' {
foreach subject in Kis Eng Math{
estpost tabstat Sudents Payment_Posible_`subject' Payment_Total_`subject' PercentageEarned_`subject' MaxDistrictProp_`subject' if SchoolID==`school', statistics(mean) columns(variables)
esttab using "CreatedData/4 Intervention/TwaEL_2016/pdf_School/latexTables/`school'_`subject'_grd3.csv", cells("Sudents Payment_Posible_`subject' Payment_Total_`subject' PercentageEarned_`subject' MaxDistrictProp_`subject'") replace fragment noeqlines nolines nogaps  nomti nonum nodep nonotes noobs tab
}
}

saveold "CreatedData/4 Intervention/TwaEL_2016/ReportsInfoGrd3.dta",replace version(12)

log close 
